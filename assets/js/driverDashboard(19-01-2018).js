$(document).ready(function(){	
	$("#section_first").show();	
	$('#all_content').show();
	$("#sched_view").hide();
	$("#section_second").hide();
	$("#complete_view").hide();	
	$("#cancel_ride_view").hide();	
	$("#btn_continue").on("click",function(e) {
		e.preventDefault();	    
	    $("#section_first").hide();
		$("#section_second").show();						
	});

	$("#schedule_bk").on("click",function(e){
		e.preventDefault();
		$('#all_content').show();
		$("#sched_view").hide();
	});
	$("#complete_bk").on("click",function(e){
		e.preventDefault();
		$('#all_content').show();
		$("#complete_view").hide();
	});
	$("#cancel_bk").on("click",function(e){
		e.preventDefault();
		$('#all_content').show();
		$("#cancel_ride_view").hide();
	});
	$("#c_view").on("click",function(e){
		e.preventDefault();
		$('#all_content').hide();
		$("#complete_view").show();
	});
	$("#cancel_view").on("click",function(e){
		e.preventDefault();
		$('#all_content').hide();
		$("#cancel_ride_view").show();
	});

	if($("#no").is(":checked")==true)
	{
		$("#file_signature").hide();
	}
	$("input[name='signature']").click(function () {
        if ($("#yes").is(":checked")) {
            $("#file_signature").show();
        } else {
            $("#file_signature").hide();
        }
    });	
    $("#customer_complaints").click(function(){
    	window.location.href = baseURL+"cumstomer-complaint";
	});
	$("#ride_trip_details_view").click(function(){
		window.location.href= baseURL+"ride-trip-details-view";
	});
	
	$("#file-input").change(function() {	
		if (this.files && this.files[0]) {
	    var reader = new FileReader();
	    reader.onload = function(e) {
	    $('#preview_image').css('background-image', 'url(' + e.target.result + ')');
	    }
	    reader.readAsDataURL(this.files[0]);
	  }	  
	});

	$("#comp_ride .btn-view").on("click",function(e){
		e.preventDefault();
		var id = $(this).closest("tr").find("td#completed_ride_view_id").html();
		$.ajax({
			type:"POST",
			url:baseURL+"frontend/DriverController/get_schedule_ride_details",
			data:{'id':id},
			dataType:"json",
			async:false,					
			success:function(response){
				if(response.status == true){			
					$('#all_content').hide();
					$("#complete_view").show();
					$("#complete_trip_id").text(response.data[0].sr_trip_id);
					$("#complete_trip_date").text(response.data[0].sr_date+' Time: '+response.data[0].sr_pick_up_time);
					$("#complete_pick_up_point").text(response.data[0].sr_street);					
				}
			}
		});
	});
	
	$("#sched_ride .btn-track").on("click",function(e){
		e.preventDefault();
		var id = $(this).closest("tr").find("td#schedule_get_view_id").html();
		window.location.href= baseURL+"ride-trip-details-view";
	});

	$("#sched_ride .btn-view").on("click",function(e){
		e.preventDefault();
		var id = $(this).closest("tr").find("td#schedule_get_view_id").html();
		$.ajax({
			type:"POST",
			url:baseURL+"frontend/DriverController/get_schedule_ride_details",
			data:{'id':id},
			dataType:"json",
			async:false,					
			success:function(response){
				if(response.status == true){			
					$('#all_content').hide();
					$("#sched_view").show();
					$("#trip_id").text(response.data[0].sr_trip_id);
					$("#trip_date").text(response.data[0].sr_date+' Time: '+response.data[0].sr_pick_up_time);
					$("#trip_pick_up_point").text(response.data[0].sr_street);
					$("#driver_name").text(response.data[0].full_name);
					$(".cancel_btn").val(response.data[0].id);
				}
			}
		});
	});

	$(".cancel_btn").on("click",function(){
		var id = $(".cancel_btn").attr("value");
		$.ajax({
			type:"POST",
			url:baseURL+"frontend/DriverController/schedule_ride_cancelled",
			data:{'id':id},
			datType:"json",
			async:false,
			success:function(response){
				if(response.status == true){
					$("#success_message").text(response.message);
					setInterval(function(){
						location.reload(true);	
					},3000);
				}
				else
				{
					$("#error_message").text(response.message);	
				}
			}
		});
	});

	$("#cancel_ride .btn-view").on("click",function(e){
		e.preventDefault();
		var id = $(this).closest("tr").find("td#cancelled_ride_view_id").html();
		$.ajax({
			type:"POST",
			url:baseURL+"frontend/DriverController/get_schedule_ride_details",
			data:{'id':id},
			dataType:"json",
			async:false,					
			success:function(response){
				if(response.status == true){			
					$('#all_content').hide();
					$("#cancel_ride_view").show();
					$("#cancelled_trip_id").text(response.data[0].sr_trip_id);
					$("#cancelled_trip_date").text(response.data[0].sr_date+' Time: '+response.data[0].sr_pick_up_time);
					$("#cancelled_pick_up_point").text(response.data[0].sr_street);					
				}
			}
		});
	});
	$("#schedule_ride_cancelled_yes").on("click",function(e){
		e.preventDefault();		
		var id =$("#schedule_ride_cancelled_value").val();	
		$.ajax({
			type:"POST",
			url:baseURL+"frontend/DriverController/schedule_ride_cancelled",
			data:{'id':id},
			datType:"json",
			async:false,
			success:function(response){
				if(response.status == true){
					$("#success_message").text(response.message);
					setInterval(function(){
						location.reload(true);	
					},3000);
				}
				else
				{
					$("#error_message").text(response.message);	
				}
			}
		});
	});
	
	$("#sched_ride .btn-cancel").on("click",function(e){
		e.preventDefault();
		var id = $(this).closest("tr").find("td#schedule_get_view_id").html();
		$("#schedule_ride_cancelled_value").val(id);		
	});	

	$(".complaint_ok").on("click",function(e){
		e.preventDefault();
		$("#subcomp_pop").hide();
		location.reload(true);
	});

	$("#btn_customer_complaint").on("click",function(){
		var userName = $("#user_name").val();
		var driverName = $("#driver_name").val();
		var complaintType = $("#complaint_type").val();
		var dateOfService = $("#date_of_service").val();
		var nameOfTransportProv = $("#name_of_transport_prov").val();
		var description = $("#description").val();
		var dosFlag = false;
		var userNameFlag =false;		
		var driverNameFlag =false;
		var complaintTypeFlag = false;
		var descriptionFlag = false;
		var nameOfTransportProvFlag = false;
		if(nameOfTransportProv == ''){
			$(".name_of_transport_prov").remove();
			nameOfTransportProvFlag =false;	
			$("#name_of_transport_prov").parent().append("<div class='name_of_transport_prov' style='color:red;'>The name of transport provider field is required.</div>");
		}else{
			if(nameOfTransportProv.length < 8)
			{
				$(".name_of_transport_prov").remove();	
				$("#name_of_transport_prov").parent().append("<div class='name_of_transport_prov' style='color:red;'>The name of transport provider field minimum 8 characters .</div>");
				nameOfTransportProvFlag =false;
			}
			else
			{	
				$(".name_of_transport_prov").remove();
				nameOfTransportProvFlag =true;
			}					
		}

		if(description == ''){
			$(".description").remove();
			descriptionFlag =false;	
			$("#description").parent().append("<div class='description' style='color:red;'>The description field is required.</div>");
		}else{
			if(description.length < 20)
			{
				$(".description").remove();	
				$("#description").parent().append("<div class='description' style='color:red;'>The description field minimum 20 characters .</div>");
				descriptionFlag =false;
			}
			else
			{	
				$(".description").remove();
				descriptionFlag =true;
			}					
		}

		if(dateOfService == ''){
			$(".date_of_service").remove();	
			$("#date_of_service").parent().append("<div class='date_of_service' style='color:red;'>The date of service field is required.</div>");
			dosFlag =false;
		}else{
			$(".date_of_service").remove();
			dosFlag =true;			
		}

		if(complaintType == ''){
			$(".complaint_type").remove();	
			$("#complaint_type").parent().append("<div class='complaint_type' style='color:red;'>The complaint type field is required.</div>");
			complaintTypeFlag =false;
		}else{
			$(".complaint_type").remove();
			complaintTypeFlag =true;			
		}

		if(userName == ''){
			$(".user_name").remove();
			userNameFlag =false;	
			$("#user_name").parent().append("<div class='user_name' style='color:red;'>The user name field is required.</div>");
		}else{
			if(userName.length < 6)
			{
				$(".user_name").remove();	
				$("#user_name").parent().append("<div class='user_name' style='color:red;'>The user name field minimum 6 characters .</div>");
				userNameFlag =false;
			}
			else
			{	
				$(".user_name").remove();
				userNameFlag =true;
			}					
		}
		if(driverName == ''){
			$(".driver_name").remove();
			driverNameFlag =false;	
			$("#driver_name").parent().append("<div class='driver_name' style='color:red;'>The driver name field is required.</div>");
		}else{
			if(driverName.length < 6)
			{
				$(".driver_name").remove();	
				$("#driver_name").parent().append("<div class='driver_name' style='color:red;'>The driver name field minimum 6 characters .</div>");
				driverNameFlag =false;
			}
			else
			{	
				$(".driver_name").remove();
				driverNameFlag =true;
			}					
		}
		if(nameOfTransportProvFlag && descriptionFlag && complaintTypeFlag && driverNameFlag && dosFlag && userNameFlag)
		{
			$.ajax({
			type:"POST",
			url:baseURL+"cumstomer-complaint",
			data:{"userName":userName,"driverName":driverName,"complaintType":complaintType,"dateOfService":dateOfService,"nameOfTransportProv":nameOfTransportProv,"description":description},
			dataType:"json",
			async:false,					
			success:function(response){
				if(response.status == true){			
					$("#subcomp_pop").show();

				}
				else
				{
					setInterval(function(){
						$("#error_message").text(response.message);
					},3000);					
				}
			}
		});	
		}
	});
	$("#name_of_transport_prov").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});

	$("#description").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,55,37,39,188,109,32,191]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }        
    });
    $("#user_name").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});
	 $("#driver_name").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});

	$("#driver_details_bk").on("click",function(e){
		e.preventDefault();
		window.location.href= baseURL+"my-rides/schedule-ride";
	});

	$(".customer_complaints").on("click",function(e){
		e.preventDefault();
		window.location.href= baseURL+"my-rides/schedule-ride";
	});

	$("div.noti_mess").each(function(){
	    $(this).hide();
	    if($(this).attr('id') == 'id_1') {
	        $(this).show();	 	        
	        $("#delete_notification_value").val($("div.notif_val").attr('id'));      	        
	    }
	});

	$('.not_tit_wrap a').on( "click", function(e) {
	    e.preventDefault();
	    var id = $(this).attr('data-related');
	    $("#delete_notification_value").val($(this).find('.notif_val').attr('id'));
		$('.not_tit_wrap div.noti_box_inn').removeClass('act_noti');
	    $("div.noti_mess").each(function(){
	        $(this).hide();	      
	        if($(this).attr('id') == id) {
	        	$('.not_tit_wrap div.div_'+id).addClass('act_noti');
	        	$(this).show();		        	
	        }    
	    });	    
	});	

	$(".delete_notification").on("click",function(e){
		e.preventDefault();
		var notifID = $("#delete_notification_value").val();
		$.ajax({
			type:"POST",
			url:baseURL+"frontend/DriverController/delete_notification",
			data:{'id':notifID},
			datType:"json",
			async:false,
			success:function(response){
				if(response.status == true){
					$("#success_message").text(response.message);
					setInterval(function(){
						location.reload(true);	
					},3000);
				}
				else
				{
					$("#error_message").text(response.message);	
				}
			}
		});		
	});

    $( "#date_of_service" ).datepicker({		
		changeMonth: true,
	   	changeYear: true,		   	
		maxDate: 0
	});	
});