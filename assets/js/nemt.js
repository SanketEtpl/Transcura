$(document).ready(function(){
	$("#accepted_view").hide();
	// ---------- Accept request ----------
	$("#ride_request_trip .btn-ride-request-accepted").on("click",function(e){
		e.preventDefault();
		var id = $(this).closest("tr").find("td#ride_req_get_id").html();
		var tripID = $(this).closest("tr").find("td#ride_req_trip_id").html();
		if (confirm("Do you really want to accept request?"))
		{
			$.ajax({
				type:"POST",
				url:baseURL+"frontend/DriverUserTypeController/driver_accepted_request",
				data:{'driverID':id,"tripID":tripID},
				dataType:"json",
			//async:false,					
			success:function(response){
				if(response.status == true){	
					$("#success_message").text(response.message);
					setInterval(function(){
						location.reload();
					},3000);				
				}
				else
					if(response.status == false){
						$("#error_message").text(response.message);
						setInterval(function(){
							$("#error_message").text("");
						},3000);			
					}				
				}
			});	
		}			
	});

	//----------- Reject request --------------------
	$("#ride_request_trip .btn-ride-request-rejected").on("click",function(e){
		e.preventDefault();
		var id = $(this).closest("tr").find("td#ride_req_get_id").html();
		var tripID = $(this).closest("tr").find("td#ride_req_trip_id").html();
		if (confirm("Do you really want to reject request?"))
		{
			$.ajax({
				type:"POST",
				url:baseURL+"frontend/DriverUserTypeController/driver_reject_request",
				data:{'driverID':id,"tripID":tripID},
				dataType:"json",		
				success:function(response){
					if(response.status == true){	
						$("#success_message").text(response.message);
						setInterval(function(){
							location.reload();
						},3000);				
					}
					else
						if(response.status == false){
							$("#error_message").text(response.message);
							setInterval(function(){
								$("#error_message").text("");
							},3000);			
						}				
					}
				});	
		}			
	});	

	$("#accepted_trip .btn-view").on("click",function(e){
		e.preventDefault();
		var id = $(this).closest("tr").find("td#accepted_ride_view_id").html();
		//alert("hello "+ id);return false;
		$.ajax({
			type:"POST",
			url:baseURL+"frontend/DriverUserTypeController/get_accepted_ride_details",
			data:{'id':id},
			dataType:"json",
			async:false,					
			success:function(response){
				if(response.status == true){
					$('#all_content').hide();
					$("#accepted_view").show();
					$("#full_name").text(response.data[0].full_name);
					$("#source_address").text(response.data[0].source_address);
					$("#destination_address").text(response.data[0].destination_address);					
					$("#duration").text(response.data[0].duration);
					$("#sr_total_distance").text(response.data[0].sr_total_distance);
					$("#sr_date").text(response.data[0].sr_date);
					$("#sr_pick_up_time").text(response.data[0].sr_pick_up_time);						
					$("#driver_nm").text(response.data[0].driver_nm);						
				}
			}
		});
	});
	$("#accepted_view").hide();
//$("#accepted_bk").hide();

$("#accepted_bk").on("click",function(e){
	e.preventDefault();
	$('#all_content').show();
	$("#accepted_view").hide();
});
$("div.noti_mess").each(function(){
	$(this).hide();
	if($(this).attr('id') == 'id_1') {
		$(this).show();	 	        
		$("#driver_delete_notification_value").val($("div.notif_val").attr('id'));      	        
	}
});

$('.not_tit_wrap a').on( "click", function(e) {
	e.preventDefault();
	var id = $(this).attr('data-related');
	$("#driver_delete_notification_value").val($(this).find('.notif_val').attr('id'));
	$('.not_tit_wrap div.noti_box_inn').removeClass('act_noti');
	$("div.noti_mess").each(function(){
		$(this).hide();	      
		if($(this).attr('id') == id) {
			$('.not_tit_wrap div.div_'+id).addClass('act_noti');
			$(this).show();		        	
		}    
	});	    
});	

$(".driver_delete_notification").on("click",function(e){
	e.preventDefault();
	var notifID = $("#driver_delete_notification_value").val();
	$.ajax({
		type:"POST",
		url:baseURL+"frontend/DriverUserTypeController/driver_delete_notification",
		data:{'id':notifID},
		datType:"json",
		async:false,
		success:function(response){
			if(response.status == true){
				$("#success_message").text(response.message);
				setInterval(function(){
					location.reload(true);	
				},3000);
			}
			else
			{
				$("#error_message").text(response.message);	
			}
		}
	});		
});

$('#schedule_rides').on('click', '.reject_button', function(event) {
	var id = $(this).attr('data-id');
	if (confirm("Are you sure want to reject this ride ?")) {
		$.ajax({
			type:"POST",
			url:baseURL+"frontend/DriverController/rejectNEMT",
			data:{'id':id},
			dataType:"json",
			async:false,					
			success:function(response){
				if(response.status == "success"){	
					$("#success_message").css('display','block');							
					$("#success_message").text(response.message);	
					setInterval(function(){
						location.reload(true);	
					},3000);										
				}
				else
				{
					$("#error_message").css('display','block');
					$("#error_message").text(response.message);
					setInterval(function(){
						location.reload(true);	
					},3000);
				}
			}
		});		   
	}
});

$('#schedule_rides').on('click', '.confirm_button', function(event) {
	var id = $(this).attr('data-id');
	if (confirm("Are you sure want to confirm this ride ?")) {
		if( id != "")
		{
			$.ajax({
				type:"POST",
				url:baseURL+"frontend/DriverController/confirmNEMT",
				data:{'id':id},
				dataType:"json",
				async:false,					
				success:function(response){
					if(response.status == "success"){	
						$("#success_message").css('display','block');							
						$("#success_message").text(response.message);
						setInterval(function(){
							location.reload(true);	
						},3000);											
					}
					else
					{
						$("#error_message").css('display','block');
						$("#error_message").text(response.message);
						setInterval(function(){
							location.reload(true);	
						},3000);
					}
				}
			});
		}
		else
		{
			alert("No data found.");
		}
	}
});

	//-------- search healthcare -----------
	$("#search_healthcare").click(function(){
		var search_name = $("#search_name").val();
		var search_cat = $("#search_cat").val();
		$.ajax({
			type:"POST",
			url:baseURL+"frontend/HealthCareController/search_healthcare",
			data:{'search_name':search_name,'search_cat':search_cat},
			dataType:"json",
			async:false,					
			success:function(response){
					// console.log(response);
					$(".heal_wrapp").html(response.data);
				}
			});
	});

	// $(".apply_healthcare").click(function(){		
	// 	var userid = $(this).attr("data-id");
	// 	$.ajax({
	// 		type:"POST",
	// 		url:baseURL+"frontend/HealthCareController/getHealthDetails",
	// 		data:{'userid':userid},
	// 		dataType:"json",
	// 		async:false,					
	// 		success:function(response){
	// 			console.log(response);
	// 			$(".heal_wrapp").html(response.data);
	// 		}
	// 	});
	// });



	$('#return_time1').hide();
	$("#show_urgnt").hide();
	$("#req_again_ride").on("click",function(e){		
		if($("#req_again_ride").val() == 2)
			$('#return_time1').show();
		else
			$('#return_time1').hide();		
	});

	$("#urgency_type").on("click",function() {	
		if($("#urgency_type").val() == 1)
			$("#show_urgnt").show();
		else
			$("#show_urgnt").hide();
	});

	if($("#req_again_ride").val() == 2)
		$('#return_time1').show();
	else
		$('#return_time1').hide();
	
	if($("#urgency_type").val() == 1)
		$("#show_urgnt").show();
	else
		$("#show_urgnt").hide();

});
