function checkemail()
{
	var email = $("#emailaddrss").val();
	var usertype = $("#usertype").val();
	$(".emailaddrss").remove();
	$(".email").remove();
	var result = true;
	$.ajax({
		type:"POST",
		url:baseURL+"checkemailAvailability",
		data:{'email':email,'usertype':usertype},
		dataType:"json",
		async:false,					
		success:function(response){
			
			if(response.status == true)
			{
				$(".email").remove();	
				$("#emailaddrss").parent().append("<div class='email' style='color:red;'>"+ response.message +"</div>");
				result = false;
			}
			else
			{
				$(".email").remove();				
			}			
		}
	});
	return result;
}

function checkusername()
{
	//alert("hello");
	var username = $("#username").val();
	//alert(username);	
	$(".username1").remove();
	$(".username").remove();
	var result = true;
	$.ajax({
		type:"POST",
		url:baseURL+"frontend/Homecontroller/check_username_availability",
		data:{'username':username},
		dataType:"json",
		async:false,					
		success:function(response){
			
			if(response.status == true)
			{
				$(".username1").remove();	
				$("#username").parent().append("<div class='username1' style='color:red;'>"+ response.message +"</div>");
				result = false;
			}
			else
			{
				result = true;
				$(".username1").remove();				
			}			
		}
	});
	return result;
}
$(document).ready(function(){
		var emailPattern = /^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,50}$/i;
		$("#frm-second").hide();
		$(".hide_content_first").hide();
		$(".hide_content_second").hide();
		$(".hide_content_third").hide();
		$(".hide_content_four").hide();
		$("#hide_content_five").hide();
		$("#hide_content_six").hide();
		$("#hide_content_seven").hide();
		$("#hide_content_eight").hide();
		$("#hide_content_nine").hide();
		$("#hide_content_ten").hide();
		$("#hide_content_eleven").hide();
		$("#btn-back").click(function(){
					$("#frm-second").hide();
					$("#frm-first").show();
					return false;
				});		
$('#loadingDiv').hide();
		$('#country').on('change',function(){
    	var countryID = $(this).val();	       
        if(countryID){
            $.ajax({
                type:'POST',
                url:baseURL+"frontend/Homecontroller/get_state",
                data:{countryID:countryID},
                dataType:'json',
                success:function(json){                               	                    
                    $('#state').html(json.state);                     
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
    
$("#btn-reset-username").click(function(){
	var emailPattern = /^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,50}$/i;
	 	var email = $("#emailaddrss").val();

	 	emailResetFlag = false;	
	 	if(email == ''){			
			emailResetFlag = false;			
			$(".email").remove();	
			$("#emailaddrss").parent().append("<div class='email' style='color:red;'>Email field is required.</div>");
		}else{
			if( !emailPattern.test(email)){
				$(".email").remove();	
				$("#emailaddrss").parent().append("<div class='email' style='color:red;'>Please enter a valid email address.</div>");
			}else{				
				emailResetFlag = true;
				$(".email").remove();	
			}
		}
		if(emailResetFlag)
		{			
			$.ajax({
					type:"POST",
					url:baseURL+"set-reset-username",
					data:{'email':email},
					dataType:"json",					
					success:function(response){
						if(response.status == true)
						{
							$('#resetsuccess').modal('show');
						}
						else
						{
							$('.message').html('<div class="alert alert-danger">'+response.message+'</div>');	
						}
					}	
			});
		}
	 	
	 });

$("#btn-reset").click(function(){
	var emailPattern = /^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,50}$/i;
	 	var email = $("#emailaddrss").val();

	 	emailResetFlag = false;	
	 	if(email == ''){			
			emailResetFlag = false;			
			$(".email").remove();	
			$("#emailaddrss").parent().append("<div class='email' style='color:red;'>Email field is required.</div>");
		}else{
			if( !emailPattern.test(email)){
				$(".email").remove();	
				$("#emailaddrss").parent().append("<div class='email' style='color:red;'>Please enter a valid email address.</div>");
			}else{				
				emailResetFlag = true;
				$(".email").remove();	
			}
		}
		if(emailResetFlag)
		{			
			$.ajax({
					type:"POST",
					url:baseURL+"set-reset-pwd",
					data:{'email':email},
					dataType:"json",					
					success:function(response){
						if(response.status == true)
						{
							$('#resetsuccess').modal('show');
						}
						else
						{
							$('.message').html('<div class="alert alert-danger">'+response.message+'</div>');	
						}
					}	
			});
		}
	 	
	 });

$(".username").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        if ($.inArray(e.keyCode, [189,222,32,46, 8, 9, 27, 13, 110, 190,16,37,39,173,111,173,95,45,109]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        /* if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
     	 e.preventDefault();*/
     	 if($(this).val().indexOf('-') !== -1 && (e.keyCode == 109 ))
     	 e.preventDefault();
	    if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();
    });
  

    $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:baseURL+'frontend/Homecontroller/get_city',
                data:{'state_id':stateID},
                dataType:'json',
                success:function(json){
                    $('#city').html(json.city);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
    $("#zipcode").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
     /*$("#county").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || (k >= 96 && k <= 105) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});*/
$("#county").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,37,39,188,173,32,191]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90) ||( e.keyCode>=96 && e.keyCode<=105)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        
    });

	$("#phnnumber").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
   $("#eme_number").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("#insurance_id").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)||( e.keyCode>=65 && e.keyCode<=90)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

$("#email_username").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
    	/* if(this.value.indexOf('@') !== -1 && (e.keyCode == 50))
     	 e.preventDefault();*/
  		if(this.value.indexOf('_') !== -1 && (e.keyCode == 173 || e.keyCode == 95))
      	e.preventDefault();
	    if(this.value.indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();

        if ($.inArray(e.keyCode, [190,50,189,222,32,46, 8, 9, 27, 13, 110, 190,16,37,39,173,111,173,95,45,109]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
       /*  if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
     	 e.preventDefault();
     	 if($(this).val().indexOf('-') !== -1 && (e.keyCode == 109 ))
     	 e.preventDefault();
	    if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault(); */
    });

	$("#fullname").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 222 || k == 189 || k == 173 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});
	$("#eme_name").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});

	$("#street").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,55,37,39,188,109,173,111,32,191]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        
    });

	
	$("#special_instruction").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105) || k == 9 || k == 173 || k == 109 || k == 8 || k == 55 || k == 37 || k == 39 || k == 46 || k == 32 || k == 190 || k == 110 || k == 191 || k == 111) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});

	$("#username").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        if ($.inArray(e.keyCode, [189,222,32,46, 8, 9, 27, 13, 110, 190,16,37,39,173,111,173,95,45,109]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        /* if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
     	 e.preventDefault();*/
     	 if($(this).val().indexOf('-') !== -1 && (e.keyCode == 109 ))
     	 e.preventDefault();
	    if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();
    });
  
	$("#city").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 9 || k == 39 || k == 46)  {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	});

	$(".name").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 222 || k == 189 || k == 173 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});

	$(".vehicle_details").keydown(function (e) {      
        if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
	});

	$("#pass").keydown(function(e) {		
	  k = e.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105 || k >= 48 && k<=57) || k == 32 || k == 9 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        e.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    e.preventDefault();
	    return false;
	  }
	    if($(this).val().indexOf(' ') !== -1 && (e.keyCode == 8))
        e.preventDefault();
    if (k === 32 && !this.value.length)
        e.preventDefault();
	});
	$("#login-password").keydown(function(e) {		
	  k = e.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105 || k >= 48 && k<=57) || k == 32 || k == 9 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        e.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    e.preventDefault();
	    return false;
	  }
	    if($(this).val().indexOf(' ') !== -1 && (e.keyCode == 8))
        e.preventDefault();
    if (k === 32 && !this.value.length)
        e.preventDefault();
	});

	$("#conpass").keydown(function(e) {		
	  k = e.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105 || k >= 48 && k<=57) || k == 9 || k == 32 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        e.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    e.preventDefault();
	    return false;
	  }
	    if($(this).val().indexOf(' ') !== -1 && (e.keyCode == 8))
        e.preventDefault();
    if (k === 32 && !this.value.length)
        e.preventDefault();
	});

$("#emailaddrss ").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
    	 /*if(this.value.indexOf('@') !== -1 && (e.keyCode == 50))
     	 e.preventDefault(); */
if(this.value.indexOf('--') !== -1 && (e.keyCode == 189))
     	 e.preventDefault();
     	if(this.value.indexOf('__') !== -1 && (e.keyCode == 189))
     	 e.preventDefault();
  		if(this.value.indexOf('__') !== -1 && (e.keyCode == 173 || e.keyCode == 95))
      	e.preventDefault();
	    if(this.value.indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();

        if ($.inArray(e.keyCode, [190,50,189,46, 8, 9, 27, 13, 110, 190,16,37,39,173,111,173,95,45,109]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        /* if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
     	 e.preventDefault();
     	 if($(this).val().indexOf('-') !== -1 && (e.keyCode == 109 ))
     	 e.preventDefault();
	    if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault(); */
    });
	/*$("#emailaddrss").keydown(function(e) {		
	  k = e.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105) || k == 9 || k == 50 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        e.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    e.preventDefault();
	    return false;
	  }
	  if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
      e.preventDefault();
  		if($(this).val().indexOf('_') !== -1 && (e.keyCode == 173 || e.keyCode == 95))
      e.preventDefault();

	    if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();
	});
*/

 	$("#phnnumber").intlTelInput({
      allowDropdown: true,
      autoHideDialCode: false,
      autoPlaceholder: "off",     
      utilsScript: "assets/js/utils.js"
    });

 	$("#eme_number").intlTelInput({
      allowDropdown: true,
      autoHideDialCode: false,
      autoPlaceholder: "off",     
      utilsScript: "assets/js/utils.js"
    });

	$('#DRIVER').hide();  
	$('#personal_doc_id').hide();
	$('#vehicle_doc_id').hide();
	$('#vehicle_registration_div_id').hide();
	$('#own_vehicle_id').hide();
	$('#company_vehicle_id').hide();
	$("#own_bussiness_show").hide();
	$("#company_vehicle_show").hide();
	$('#usertype').change(function(){		
       	if($(this).val() === '5') {
           $('#personal_doc_id').show(); 
           $('#vehicle_doc_id').show();          
           $('#own_vehicle_id').show();
           $('#company_vehicle_id').show();
        }	     
        else
       	{
           $('#personal_doc_id').hide();     
           $('#vehicle_doc_id').hide();
           $('#own_vehicle_id').hide();  
           $('#company_vehicle_id').hide();             
       	}
    });

	$('#pers_doc_dd').on('change',function(){
    	var perDocYes = $('#pers_doc_dd option:selected').val();   
	    if(perDocYes == 1)
	    	$('#DRIVER').show();
	    else
	    	$('#DRIVER').hide();	    
    }); 

    $('#vehicle_reg_documents_id').click(function(){
		var vehRegDoc = $('#vehicle_reg_documents_id option:selected').val();    
		if(vehRegDoc == 1)
			$('#vehicle_registration_div_id').show();
		else
			$('#vehicle_registration_div_id').hide();		
    });  
    $("#own_vehicle_dd").click(function(){    	
    	var ownVehDD = $("#own_vehicle_dd option:selected").val();
    	if(ownVehDD == 1)
    		$('#own_bussiness_show').show();
    	else
    		$('#own_bussiness_show').hide();
    });
    
    $("#company_vehicle_dropdown").click(function(){    
    	var comVehDD = $("#company_vehicle_dropdown option:selected").val();
    	if(comVehDD == 1)
    		$('#company_vehicle_show').show();
    	else
    		$('#company_vehicle_show').hide();
    });

	$("#btn-proceed").click(function(e){
		var usertype = $("#usertype").val();
		var fullname = $("#fullname").val();
		var date_of_birth = $("#date_of_birth").val();
		var emailaddrss = $("#emailaddrss").val();
		var phnnumber = $("#phnnumber").val();
		var username = $('#username').val();
		var pass = $("#pass").val();
		var conpass = $("#conpass").val();		
		var passFlag =false;
		var confpwdFlag =false;
		var usrFlag =false;
		var dobFlag =false;
		var fullnmFlag =false;
		var emailFlag =false;	
		var phoneFlag =false;	
		var checkmailflg = checkemail();
		var checkUsernameflag = checkusername();

		var minlength = 6;
		if(pass == ''){
			$(".pass").remove();	
			$("#pass").parent().append("<div class='pass' style='color:red;'>Password field is required.</div>");
			passFlag =false;
		}else{	
			if(pass.length < minlength)
			{
				$(".pass").remove();	
				$("#pass").parent().append("<div class='pass' style='color:red;'>Password field must contain minimum 6 characters .</div>");
				passFlag =false;
			}
			else
			{	
				passFlag =true;
				$(".pass").remove();				
			}
		}
		if(conpass == ''){
			$(".conpass").remove();	
			$("#conpass").parent().append("<div class='conpass' style='color:red;'>Confirm password field is required.</div>");
			confpwdFlag =false;
		}else{
			if( conpass != pass){
				$(".conpass").remove();	
				confpwdFlag =false;
				$("#conpass").parent().append("<div class='conpass' style='color:red;'>Password and confirm password do not match.</div>");
			}else{
				confpwdFlag =true;
				$(".conpass").remove();	
			}	
		} 

		if(usertype == ''){
			$(".usertype").remove();	
			$("#usertype").parent().append("<div class='usertype' style='color:red;'>User type field is required.</div>");
			usrFlag =false;
		}else{
			$(".usertype").remove();
			usrFlag =true;					
		}

		if(date_of_birth == ''){
			$(".date_of_birth").remove();	
			$("#date_of_birth").parent().append("<div class='date_of_birth' style='color:red;'>Date of birth field is required.</div>");
			dobFlag =false;
		}else{
			$(".date_of_birth").remove();
			dobFlag =true;			
		}

		if(phnnumber == ''){
			$(".phnnumber").remove();
			phoneFlag =false;	
			$("#phnnumber").parent().append("<div class='phnnumber' style='color:red;'>Phone no field is required.</div>");
		}else{
			if(phnnumber.length < 8)
			{
				$(".phnnumber").remove();	
				$("#phnnumber").parent().append("<div class='phnnumber' style='color:red;'>Phone no field must contain minimum 8 digits .</div>");
				phoneFlag =false;
			}else{
				phoneFlag =true;
				$(".phnnumber").remove();	
			}				
		}


		if(username == ''){
			$(".username").remove();
			$(".username1").remove();
			usrFlag =false;	

			$("#username").parent().append("<div class='username' style='color:red;'>User name field is required.</div>");
		}else{
			$(".username").remove();	
			usrFlag =true;
		}

		if(fullname == ''){
			$(".fullname").remove();
			fullnmFlag =false;	
			$("#fullname").parent().append("<div class='fullname' style='color:red;'>Full name field is required.</div>");
		}else{
			if(fullname.length < 3)
			{
				$(".fullname").remove();	
				$("#fullname").parent().append("<div class='fullname' style='color:red;'>Full name field must contain minimum 3 characters .</div>");
				fullnmFlag =false;
			}
			else
			{	
				$(".fullname").remove();
				fullnmFlag =true;
			}					
		}

		//Ankush		
		if(emailaddrss == ''){			
			emailFlag = false;			
			$(".emailaddrss").remove();	
			$("#emailaddrss").parent().append("<div class='emailaddrss' style='color:red;'>Email field is required.</div>");
		}else{
			if( !emailPattern.test(emailaddrss)){
				emailFlag = false;				
				$(".emailaddrss").remove();	
				$("#emailaddrss").parent().append("<div class='emailaddrss' style='color:red;'>Please enter a valid email address.</div>");
			}else{				
				emailFlag = true;
				$(".emailaddrss").remove();	
			}
		}

		if(emailFlag && fullnmFlag && usrFlag && phoneFlag && dobFlag && usrFlag && confpwdFlag && passFlag && checkmailflg && checkUsernameflag)
		{
			$("#frm-second").show();
			$("#frm-first").hide();
			return false;	
		}		
	});
	
	 $('#btnSubmit').click(function(){
	 	var minlength = 6;
	 	var usertype = $("#usertype").val();
		var fullname = $("#fullname").val();
		var date_of_birth = $("#date_of_birth").val();
		var emailaddrss = $("#emailaddrss").val();
		var phnnumber = $("#phnnumber").val();
		var username = $('#username').val();
		var pass = $("#pass").val();
		var conpass = $("#conpass").val();
		var pers_doc_dd = $("#pers_doc_dd").val();
		var vehicle_reg_documents_id = $("#vehicle_reg_documents_id").val();
		var own_vehicle_dd = $("#own_vehicle_dd").val();
		var company_vehicle_dropdown = $("#company_vehicle_dropdown").val();		
		
	 	//var usertype = $("#usertype").val();
		var country = $("#country").val();
		var state = $("#state").val();
		var city = $("#city").val();
		var street = $("#street").val();
		var zipcode = $("#zipcode").val();
		var county = $("#county").val();					
		var eme_name = $("#eme_name").val();
		var eme_number = $("#eme_number").val();
		var insurance_id = $("#insurance_id").val();
		var per_doc = $("#per_doc").val();
		var signature = $('#signature').val();
		var countryFlag = false;
		var stateFlag = false;
		var cityFlag = false; 
		var streetFlag = false;
		var zipcodeFlag = false;
		var countyFlag = false;	
		var emeNameFlag = false;
		var emeNumberFlag = false;
		var insuranceIDFlag = false;
		var perDocFlag = false;
		var signatureFlag = false;
		var perDocDDFlag = false;
		var verRedFlag = false;
		var ownVehFlag = false;
		var comVehFlag = false;
		if(pers_doc_dd == ''){
			$(".pers_doc_dd").remove();	
			$("#pers_doc_dd").parent().append("<div class='pers_doc_dd' style='color:red;'>Personal document field is required.</div>");
			perDocDDFlag =false;
		}else{
			$(".pers_doc_dd").remove();
			perDocDDFlag =true;					
		} 

		if(vehicle_reg_documents_id == ''){
			$(".vehicle_reg_documents_id").remove();	
			$("#vehicle_reg_documents_id").parent().append("<div class='vehicle_reg_documents_id' style='color:red;'>Vehicle registration field is required.</div>");
			verRedFlag =false;
		}else{
			$(".vehicle_reg_documents_id").remove();
			verRedFlag =true;					
		} 

		if(own_vehicle_dd == ''){
			$(".own_vehicle_dd").remove();	
			$("#own_vehicle_dd").parent().append("<div class='own_vehicle_dd' style='color:red;'>Own business field is required.</div>");
			ownVehFlag =false;
		}else{
			$(".own_vehicle_dd").remove();
			ownVehFlag =true;					
		} 

		if(company_vehicle_dropdown == ''){
			$(".company_vehicle_dropdown").remove();	
			$("#company_vehicle_dropdown").parent().append("<div class='company_vehicle_dropdown' style='color:red;'>Company vehicle field is required.</div>");
			comVehFlag =false;
		}else{
			$(".company_vehicle_dropdown").remove();
			comVehFlag =true;					
		} 

		if(signature == ''){
			$(".signature").remove();	
			$("#signature").parent().append("<div class='signature' style='color:red;'>Signature field is required.</div>");
			signatureFlag =false;
		}else{
			$(".signature").remove();
			signatureFlag =true;					
		} 

		if(eme_number == ''){			
			emeNumberFlag = false;			
			$(".eme_number").remove();	
			$("#eme_number").parent().append("<div class='eme_number' style='color:red;'>Emergency contact no field is required.</div>");
		}
		else
		{
			if(eme_number.length < 8)
			{
				$(".eme_number").remove();	
				$("#eme_number").parent().append("<div class='eme_number' style='color:red;'>Emergency conatact field must contain minimum 8 digits .</div>");
				emeNumberFlag =false;
			}
			else
			{	
				emeNumberFlag = true;
				$(".eme_number").remove();	
			}		
		}

		if(insurance_id == ''){			
			insuranceIDFlag = false;			
			$(".insurance_id").remove();	
			$("#insurance_id").parent().append("<div class='insurance_id' style='color:red;'>Insurance id field is required.</div>");
		}
		else
		{	
			if(insurance_id.length < 12)
			{
				$(".insurance_id").remove();	
				$("#insurance_id").parent().append("<div class='insurance_id' style='color:red;'>Insurance id field must contain minimum 12 characters .</div>");
				insuranceIDFlag =false;
			}
			else
			{				
				insuranceIDFlag = true;
				$(".insurance_id").remove();
			}				
		}
		if(eme_name == ''){			
			emeNameFlag = false;			
			$(".eme_name").remove();	
			$("#eme_name").parent().append("<div class='eme_name' style='color:red;'>Emergency contact name field is required.</div>");
		}else{			
			if(eme_name.length < 3 )
			{
				$(".eme_name").remove();	
				$("#eme_name").parent().append("<div class='eme_name' style='color:red;'>Emergency contact name field must contain minimum 3 characters .</div>");
				emeNameFlag =false;
			}
			else
			{	
				emeNameFlag = true;
				$(".eme_name").remove();	
			}
		}

		if(per_doc == ''){
			$(".per_doc").remove();	
			$("#per_doc").parent().append("<div class='per_doc' style='color:red;'>Personal document field is required.</div>");
			perDocFlag =false;
		}else{
			$(".per_doc").remove();
			perDocFlag =true;					
		}
		if(country == ''){ 
			$(".country").remove();	
			$("#country").parent().append("<div class='country' style='color:red;'>Country field is required.</div>");
			countryFlag =false;
		}else{
			$(".country").remove();
			countryFlag =true;					
		}

		if(state == ''){
			$(".state").remove();	
			$("#state").parent().append("<div class='state' style='color:red;'>State field is required.</div>");
			stateFlag =false;
		}else{
			$(".state").remove();
			stateFlag =true;					
		}

		if(city == ''){
			$(".city").remove();	
			$("#city").parent().append("<div class='city' style='color:red;'>City field is required.</div>");
			cityFlag =false;
		}else{
			$(".city").remove();
			cityFlag =true;					
		}

		if(street == ''){			
			streetFlag = false;			
			$(".street").remove();	
			$("#street").parent().append("<div class='street' style='color:red;'>Street field is required.</div>");
		}else{	
			if(street.length < 3 )
			{
				$(".street").remove();	
				$("#street").parent().append("<div class='street' style='color:red;'>Street name field must contain minimum 3 characters .</div>");
				streetFlag =false;
			}
			else
			{				
				streetFlag = true;
				$(".street").remove();
			}	
		}

		if(zipcode == ''){			
			zipcodeFlag = false;			
			$(".zipcode").remove();	
			$("#zipcode").parent().append("<div class='zipcode' style='color:red;'>Zipcode field is required.</div>");
		}else{		
				if(zipcode.length < 5 )
				{
					$(".zipcode").remove();	
					$("#zipcode").parent().append("<div class='zipcode' style='color:red;'>Zipcode field must contain minimum 5 digits .</div>");
					zipcodeFlag =false;
				}
				else
				{						
					zipcodeFlag = true;
					$(".zipcode").remove();	
				}			
		}

		if(county == ''){			
			countyFlag = false;			
			$(".county").remove();	
			$("#county").parent().append("<div class='county' style='color:red;'>County field is required.</div>");
		}else{
			if(county.length < 5 )
				{
					$(".county").remove();	
					$("#county").parent().append("<div class='county' style='color:red;'>County field must contain minimum 5 digits .</div>");
					countyFlag =false;
				}
				else
				{	
					countyFlag = true;
					$(".county").remove();
				}				
		}
		//signatureFlag emeNumberFlag insuranceIDFlag emeNameFlag perDocFlag stateFlag  cityFlag streetFlag zipcodeFlag countyFlag
		
		var DLED = $('#driving_license_expiry_date').val();
		var DTED= $('#drug_test_expiry_date').val();
		var CBED = $('#criminal_bk_expiry_date').val();
		var MVRED= $('#motor_vehile_record_expiry_date').val();
		var ACED= $('#act_33_34_clearance_expiry_date').val();
		var VIED = $('#vehicle_inspection_expiry_date').val();
		var VIENSD = $('#vehicle_insurance_expiry_date').val();
		var VRED = $('#vehicle_registration_expiry_date').val();
		var DLEDFlag= false;
		var DTEDFlag = false;
		var CBEDFlag = false;
		var MVREDFlag = false;
		var ACEDFlag = false;
		var VIEDFlag = false;
		var VIENSDFlag = false;
		var VREDFlag = false;
		if($('#vehicle_registration_doc').val()!=''){
			if(VRED == ''){
				$(".vehicle_registration_expiry_date").remove();	
				$("#vehicle_registration_expiry_date").parent().append("<div class='vehicle_registration_expiry_date' style='color:red;'>Vehicle registration expiry date field is required.</div>");
				VREDFlag =false;
			}else{
				$(".vehicle_registration_expiry_date").remove();
				VREDFlag =true;			
			}
		}else{ VREDFlag =true; }
		if($('#vehicle_insurance_record_doc').val() !=''){
			if(VIENSD == ''){
				$(".vehicle_insurance_expiry_date").remove();	
				$("#vehicle_insurance_expiry_date").parent().append("<div class='vehicle_insurance_expiry_date' style='color:red;'>Vehicle insurance expiry date field is required.</div>");
				VIENSDFlag =false;
			}else{
				$(".vehicle_insurance_expiry_date").remove();
				VIENSDFlag =true;			
			}
		}else{ VIENSDFlag =true; }
		if($('#vehicle_inspection_doc').val() !=''){
			if(VIED == ''){
				$(".vehicle_inspection_expiry_date").remove();	
				$("#vehicle_inspection_expiry_date").parent().append("<div class='vehicle_inspection_expiry_date' style='color:red;'>Vehicle Inspection expiry date field is required.</div>");
				VIEDFlag =false;
			}else{
				$(".vehicle_inspection_expiry_date").remove();
				VIEDFlag =true;			
			}
		}else{ VIEDFlag =true; }	
		if($('#act_33_34_clearance_doc').val() !=''){
			if(ACED == ''){
				$(".act_33_34_clearance_expiry_date").remove();	
				$("#act_33_34_clearance_expiry_date").parent().append("<div class='act_33_34_clearance_expiry_date' style='color:red;'>Act 33 & 34 clearance expiry date field is required.</div>");
				ACEDFlag =false;
			}else{
				$(".act_33_34_clearance_expiry_date").remove();
				ACEDFlag =true;			
			}
		}else{ ACEDFlag =true; }	
		if($('#motor_vehicle_details_doc').val() != ''){
			if(MVRED == ''){
				$(".motor_vehile_record_expiry_date").remove();	
				$("#motor_vehile_record_expiry_date").parent().append("<div class='motor_vehile_record_expiry_date' style='color:red;'>Motor vehicle record expiry date field is required.</div>");
				MVREDFlag =false;
			}else{
				$(".motor_vehile_record_expiry_date").remove();
				MVREDFlag =true;			
			}
		}else{ MVREDFlag =true; }	
		if($('#driving_license_doc').val()!=''){
			if(DLED == ''){
				$(".driving_license_expiry_date").remove();	
				$("#driving_license_expiry_date").parent().append("<div class='driving_license_expiry_date' style='color:red;'>Driver license expiry date field is required.</div>");
				DLEDFlag =false;
			}else{
				$(".driving_license_expiry_date").remove();
				DLEDFlag =true;			
			}
		}else{ DLEDFlag =true; }	
		if($('#drug_test_doc').val() != ''){
			if(DTED == ''){
				$(".drug_test_expiry_date").remove();	
				$("#drug_test_expiry_date").parent().append("<div class='drug_test_expiry_date' style='color:red;'>Drug test expiry date field is required.</div>");
				DTEDFlag =false;
			}else{
				$(".drug_test_expiry_date").remove();
				DTEDFlag =true;			
			}
		}else{ DTEDFlag =true;	}	
		if($('#criminal_bk_doc').val() !=''){			
			if(CBED == ''){
				$(".criminal_bk_expiry_date").remove();	
				$("#criminal_bk_expiry_date").parent().append("<div class='criminal_bk_expiry_date' style='color:red;'>Criminal back ground expiry date field is required.</div>");
				CBEDFlag =false;
			}else{
				$(".criminal_bk_expiry_date").remove();
				CBEDFlag =true;			
			}
		}else{ CBEDFlag =true; }
		if(pers_doc_dd == 2)
			insuranceIDFlag = true;
		var userTypeCheck ='';
		if(usertype == 2)
		{
			 userTypeCheck = insuranceIDFlag;
			 DLEDFlag = DLEDFlag;
			 DTEDFlag=DTEDFlag;
			 CBEDFlag=CBEDFlag;
			 MVREDFlag=MVREDFlag;
			 ACEDFlag=ACEDFlag;
		}
		else if(usertype == 5)
		{
			userTypeCheck = insuranceIDFlag;
			DLEDFlag = DLEDFlag;
			DTEDFlag=DTEDFlag;
			CBEDFlag=CBEDFlag;
			MVREDFlag=MVREDFlag;
			ACEDFlag=ACEDFlag;
			VIEDFlag=VIEDFlag;
			VIENSDFlag=VIENSDFlag;
			VREDFlag=VREDFlag;
			perDocDDFlag=perDocDDFlag;
			verRedFlag = verRedFlag;
			ownVehFlag = ownVehFlag;
			comVehFlag = comVehFlag;
			insuranceIDFlag=insuranceIDFlag;
		}
		else
		{
			userTypeCheck =true;
			DLEDFlag = true;
			DTEDFlag=true;
			CBEDFlag=true;
			MVREDFlag=true;
			ACEDFlag=true;
			VIEDFlag=true;
			VIENSDFlag=true;
			VREDFlag=true;
			perDocDDFlag=true;
			verRedFlag = true;
			ownVehFlag = true;
			comVehFlag = true;
			insuranceIDFlag=true;
		}		
		if(MVREDFlag && ACEDFlag && VIEDFlag && VIENSDFlag && VREDFlag && CBEDFlag && DTEDFlag && DLEDFlag && signatureFlag && emeNumberFlag  && emeNameFlag  && countryFlag &&stateFlag && cityFlag && streetFlag && zipcodeFlag && countyFlag && userTypeCheck && verRedFlag && ownVehFlag && comVehFlag && insuranceIDFlag && perDocDDFlag)
		{		
			var formData = new FormData($('#signupform')[0]);			
			$('#loadingDiv').show();		
			$.ajax({
    				type:"POST",
    				url: baseURL+"set-registration",    				
    				//data:{'usertype':usertype,'fullname':fullname,'date_of_birth':date_of_birth,'emailaddrss':emailaddrss,'username':username,'phnnumber':phnnumber,'pass':pass,'country':country,'state':state,'city':city,'street':street,'zipcode':zipcode,'county':county,'eme_name':eme_name,'eme_number':eme_number,'signature':signature,'per_doc':per_doc ,'insurance_id':insurance_id,'formData':formData},
    				dataType :"json",
    				data:formData,
    				processData: false,
    				contentType: false,
    				success:function(response){ 					 					
    					if(response.status === true)
    					{
						$('#loadingDiv').hide();
	    					$('.message').html('<div class="alert alert-success">'+response.message+'</div>');
	    					document.location.href = response.redirect;   	    									
    					}
    					else
    					{
						$('#loadingDiv').hide();
    						$('.message').html('<div class="alert alert-danger">'+response.message+'</div>');
    						document.location.href = response.redirect;    					
    					}
    				}
			});	
		}			   	
    }); 
 	
 	$("#regBtn").click(function(){			    	
		window.location.href = baseURL+"registration";			    	
    });

	$("#loginBtn").click(function(){		
		window.location.href = baseURL+"frontend-login";			    	
	});

	 $('div.log_wi_btn').on("click",function() {
	
            $('div.hide_first_section_content').slideUp();
	   $(this).html('<a>VIEW MORE</a>');  
            if(!$(this).next().is(':visible')){
            $(this).next().slideDown();
	    $(this).html('<a>SHOW LESS</a>');
        }
    });
    $("div.hide_first_section_content").hide(); 
$('div.log_wi_btn a').html('<a>VIEW MORE</a>'); 
$('div.log_wi_btn_second a').html('<a>VIEW MORE</a>');  
    $('div.log_wi_btn_second').on("click",function() {
        $('div.hide_second_section_content').slideUp(); 
	    $(this).html('<a>VIEW MORE</a>');   
            if(!$(this).next().is(':visible')){
            $(this).next().slideDown();
	    $(this).html('<a>SHOW LESS</a>');
            }
        });
    $("div.hide_second_section_content").hide(); 

    $('#dledShow_date_id').hide();
	$('#driving_license_doc').change(function(){
		if($(this).val() !='')
		$('#dledShow_date_id').show();	
	});		
	$('#drug_test_date_id').hide();
	$('#drug_test_doc').change(function(){
		if($(this).val() != '')
		$('#drug_test_date_id').show();	
	});
	$('#criminal_bk_date_id').hide();
	$('#criminal_bk_doc').change(function(){
		if($(this).val() != '')
		$('#criminal_bk_date_id').show();
	});
	$('#motor_vehicle_date_id').hide();
	$('#motor_vehicle_details_doc').change(function(){
		if($(this).val() != '')
		$('#motor_vehicle_date_id').show(); 	
	});
	$('#act_33_34_clearance_date_id').hide();
	$('#act_33_34_clearance_doc').change(function(){
		if($(this).val() != '')
			$('#act_33_34_clearance_date_id').show();
	});
	$('#vehicle_inspection_date_id').hide();
	$('#vehicle_inspection_doc').change(function(){
		if($(this).val() != '')
			$('#vehicle_inspection_date_id').show();
	});

	$('#vehicle_maintaince_record_date_id').hide();
	$('#vehicle_maintainance_record_doc').change(function(){
		if($(this).val() != '')
			$('#vehicle_maintaince_record_date_id').show();
	});

	$('#vehicle_insurance_record_date_id').hide();
	$('#vehicle_insurance_record_doc').change(function(){
		if($(this).val() != '')
			$('#vehicle_insurance_record_date_id').show();
	});

	$('#vehicle_registration_date_id').hide();
	$('#vehicle_registration_doc').change(function(){
		if($(this).val() != '')
			$('#vehicle_registration_date_id').show();
	});
	
	$( ".sixMonthDate" ).datepicker({
		changeMonth:true,
		changeYear:true,
		minDate:0,
		maxDate: '+6m' 		
	});
	$( ".oneYearDate" ).datepicker({	
		changeMonth:true,
		changeYear:true,
		minDate:0,
		maxDate: '+1y' 		
	});

	$( ".fourYearDate" ).datepicker({		
		changeMonth:true,
		changeYear:true,
		minDate:0,
		maxDate: '+4y' 		
	});
	
	/*$(".view_more").click(function(){
		alert('hhelelo');
	});*/	

});
