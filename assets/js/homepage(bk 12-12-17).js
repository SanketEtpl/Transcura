function checkemail()
{
	var email = $("#emailaddrss").val();
	var usertype = $("#usertype").val();
	$(".email").remove();
	var result = true;
	$.ajax({
		type:"POST",
		url:baseURL+"checkemailAvailability",
		data:{'email':email,'usertype':usertype},
		dataType:"json",
		async:false,					
		success:function(response){
			
			if(response.status == true)
			{
				$(".email").remove();	
				$("#emailaddrss").parent().append("<div class='email' style='color:red;'>"+ response.message +"</div>");
				result = false;
			}
			else
			{
				$(".email").remove();				
			}			
		}
	});
	return result;
}
$("#btn-reset").click(function(){
	var emailPattern = /^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,50}$/i;
	 	var email = $("#emailaddrss").val();

	 	emailResetFlag = false;	
	 	if(email == ''){			
			emailResetFlag = false;			
			$(".email").remove();	
			$("#emailaddrss").parent().append("<div class='email' style='color:red;'>Email field is required.</div>");
		}else{
			if( !emailPattern.test(email)){
				$(".email").remove();	
				$("#emailaddrss").parent().append("<div class='email' style='color:red;'>Please enter a valid email address.</div>");
			}else{				
				emailResetFlag = true;
				$(".email").remove();	
			}
		}
		if(emailResetFlag)
		{			
			$.ajax({
					type:"POST",
					url:baseURL+"set-reset-pwd",
					data:{'email':email},
					dataType:"json",					
					success:function(response){
						if(response.status == true)
						{
							$('#resetsuccess').modal('show');
						}
						else
						{
							$('.message').html('<div class="alert alert-danger">'+response.message+'</div>');	
						}
					}	
			});
		}
	 	
	 });

$(document).ready(function(){
		var emailPattern = /^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,50}$/i;
		$("#frm-second").hide();
		$(".hide_content_first").hide();
		$(".hide_content_second").hide();
		$(".hide_content_third").hide();
		$(".hide_content_four").hide();
		$("#hide_content_five").hide();
		$("#hide_content_six").hide();
		$("#hide_content_seven").hide();
		$("#hide_content_eight").hide();
		$("#hide_content_nine").hide();
		$("#hide_content_ten").hide();
		$("#hide_content_eleven").hide();
		$("#btn-back").click(function(){
					$("#frm-second").hide();
					$("#frm-first").show();
					return false;
				});		

		$('#country').on('change',function(){
    	var countryID = $(this).val();	       
        if(countryID){
            $.ajax({
                type:'POST',
                url:baseURL+"frontend/Homecontroller/get_state",
                data:{countryID:countryID},
                dataType:'json',
                success:function(json){                               	                    
                    $('#state').html(json.state);                     
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
    

    $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:baseURL+'frontend/Homecontroller/get_city',
                data:{'state_id':stateID},
                dataType:'json',
                success:function(json){
                    $('#city').html(json.city);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
    $("#zipcode").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
   
	$("#county").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
	$("#phnnumber").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
   $("#eme_number").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("#insurance_id").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)||( e.keyCode>=65 && e.keyCode<=90)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

	$("#fullname").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});
	$("#eme_name").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});

	$("#street").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,55,37,39,188,109,173,111,32,191]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        
    });

	
	$("#special_instruction").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105) || k == 9 || k == 173 || k == 109 || k == 8 || k == 55 || k == 37 || k == 39 || k == 46 || k == 32 || k == 190 || k == 110 || k == 191 || k == 111) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});

	$("#username").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,50,16,37,39,173,111,173,95,45,109]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
         if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
     	 e.preventDefault();
     	 if($(this).val().indexOf('-') !== -1 && (e.keyCode == 109 ))
     	 e.preventDefault();
	    if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();
    });
  
	$("#city").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 9 || k == 39 || k == 46)  {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	});

	$(".name").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 9 || k == 39 || k == 46)  {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	});

	$("#pass").keydown(function(e) {		
	  k = e.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105 || k >= 48 && k<=57) || k == 9 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        e.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    e.preventDefault();
	    return false;
	  }
	    if($(this).val().indexOf(' ') !== -1 && (e.keyCode == 8))
        e.preventDefault();
	});
	$("#login-password").keydown(function(e) {		
	  k = e.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105 || k >= 48 && k<=57) || k == 9 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        e.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    e.preventDefault();
	    return false;
	  }
	    if($(this).val().indexOf(' ') !== -1 && (e.keyCode == 8))
        e.preventDefault();
	});

	$("#conpass").keydown(function(e) {		
	  k = e.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105 || k >= 48 && k<=57) || k == 9 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        e.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    e.preventDefault();
	    return false;
	  }
	    if($(this).val().indexOf(' ') !== -1 && (e.keyCode == 8))
        e.preventDefault();
	});

	$("#emailaddrss").keydown(function(e) {		
	  k = e.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105) || k == 9 || k == 50 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        e.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    e.preventDefault();
	    return false;
	  }
	  if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
      e.preventDefault();
  		if($(this).val().indexOf('_') !== -1 && (e.keyCode == 173 || e.keyCode == 95))
      e.preventDefault();

	    if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();
	});

 	$("#phnnumber").intlTelInput({
      allowDropdown: true,
      autoHideDialCode: false,
      autoPlaceholder: "off",     
      utilsScript: "assets/js/utils.js"
    });

 	$("#eme_number").intlTelInput({
      allowDropdown: true,
      autoHideDialCode: false,
      autoPlaceholder: "off",     
      utilsScript: "assets/js/utils.js"
    });

	$("#btn-proceed").click(function(e){
		var usertype = $("#usertype").val();
		var fullname = $("#fullname").val();
		var date_of_birth = $("#date_of_birth").val();
		var emailaddrss = $("#emailaddrss").val();
		var phnnumber = $("#phnnumber").val();
		var username = $('#username').val();
		var pass = $("#pass").val();
		var conpass = $("#conpass").val();		
		var passFlag =false;
		var confpwdFlag =false;
		var usrFlag =false;
		var dobFlag =false;
		var fullnmFlag =false;
		var emailFlag =false;	
		var phoneFlag =false;	
		var checkmailflg = checkemail();
		var minlength = 6;
		if(pass == ''){
			$(".pass").remove();	
			$("#pass").parent().append("<div class='pass' style='color:red;'>Password field is required.</div>");
			passFlag =false;
		}else{	
			if(pass.length < minlength)
			{
				$(".pass").remove();	
				$("#pass").parent().append("<div class='pass' style='color:red;'>Password field minimum 6 characters .</div>");
				passFlag =false;
			}
			else
			{	
				passFlag =true;
				$(".pass").remove();				
			}
		}
		if(conpass == ''){
			$(".conpass").remove();	
			$("#conpass").parent().append("<div class='conpass' style='color:red;'>Confirm password field is required.</div>");
			confpwdFlag =false;
		}else{
			if( conpass != pass){
				$(".conpass").remove();	
				confpwdFlag =false;
				$("#conpass").parent().append("<div class='conpass' style='color:red;'>Password and confirm password do not match.</div>");
			}else{
				confpwdFlag =true;
				$(".conpass").remove();	
			}	
		} 

		if(usertype == ''){
			$(".usertype").remove();	
			$("#usertype").parent().append("<div class='usertype' style='color:red;'>User type field is required.</div>");
			usrFlag =false;
		}else{
			$(".usertype").remove();
			usrFlag =true;					
		}

		if(date_of_birth == ''){
			$(".date_of_birth").remove();	
			$("#date_of_birth").parent().append("<div class='date_of_birth' style='color:red;'>Date of birth field is required.</div>");
			dobFlag =false;
		}else{
			$(".date_of_birth").remove();
			dobFlag =true;			
		}

		if(phnnumber == ''){
			$(".phnnumber").remove();
			phoneFlag =false;	
			$("#phnnumber").parent().append("<div class='phnnumber' style='color:red;'>Phone no field is required.</div>");
		}else{
			if(phnnumber.length < 8)
			{
				$(".phnnumber").remove();	
				$("#phnnumber").parent().append("<div class='phnnumber' style='color:red;'>Phone no field minimum 8 digits .</div>");
				phoneFlag =false;
			}else{
				phoneFlag =true;
				$(".phnnumber").remove();	
			}				
		}


		if(username == ''){
			$(".username").remove();
			usrFlag =false;	
			$("#username").parent().append("<div class='username' style='color:red;'>User name field is required.</div>");
		}else{
			$(".username").remove();	
			usrFlag =true;
		}

		if(fullname == ''){
			$(".fullname").remove();
			fullnmFlag =false;	
			$("#fullname").parent().append("<div class='fullname' style='color:red;'>Full name field is required.</div>");
		}else{
			if(fullname.length < 3)
			{
				$(".fullname").remove();	
				$("#fullname").parent().append("<div class='fullname' style='color:red;'>Full name field minimum 3 characters .</div>");
				fullnmFlag =false;
			}
			else
			{	
				$(".fullname").remove();
				fullnmFlag =true;
			}					
		}

		//Ankush		
		if(emailaddrss == ''){			
			emailFlag = false;			
			$(".emailaddrss").remove();	
			$("#emailaddrss").parent().append("<div class='emailaddrss' style='color:red;'>Email field is required.</div>");
		}else{
			if( !emailPattern.test(emailaddrss)){
				emailFlag = false;				
				$(".emailaddrss").remove();	
				$("#emailaddrss").parent().append("<div class='emailaddrss' style='color:red;'>Please enter a valid email address.</div>");
			}else{				
				emailFlag = true;
				$(".emailaddrss").remove();	
			}
		}

		if(emailFlag && fullnmFlag && usrFlag && phoneFlag && dobFlag && usrFlag && confpwdFlag && passFlag && checkmailflg)
		{
			$("#frm-second").show();
			$("#frm-first").hide();
			return false;	
		}		
	});
	
	$('#usertype').change(function(){		
	       	if($(this).val() === '2') {
	           $('#NEMT').show();           
	        }
	       else
	       	{
	           $('#NEMT').hide();           
	       	}
	    });
	 $('#btnSubmit').click(function(){
	 	var minlength = 6;
	 	var usertype = $("#usertype").val();
		var fullname = $("#fullname").val();
		var date_of_birth = $("#date_of_birth").val();
		var emailaddrss = $("#emailaddrss").val();
		var phnnumber = $("#phnnumber").val();
		var username = $('#username').val();
		var pass = $("#pass").val();
		var conpass = $("#conpass").val();		
		
	 	//var usertype = $("#usertype").val();
		var country = $("#country").val();
		var state = $("#state").val();
		var city = $("#city").val();
		var street = $("#street").val();
		var zipcode = $("#zipcode").val();
		var county = $("#county").val();					
		var eme_name = $("#eme_name").val();
		var eme_number = $("#eme_number").val();
		var insurance_id = $("#insurance_id").val();
		var per_doc = $("#per_doc").val();
		var signature = $('#signature').val();
		var countryFlag = false;
		var stateFlag = false;
		var cityFlag = false; 
		var streetFlag = false;
		var zipcodeFlag = false;
		var countyFlag = false;	
		var emeNameFlag = false;
		var emeNumberFlag = false;
		var insuranceIDFlag = false;
		var perDocFlag = false;
		var signatureFlag = false;


		if(signature == ''){
			$(".signature").remove();	
			$("#signature").parent().append("<div class='signature' style='color:red;'>Signature field is required.</div>");
			signatureFlag =false;
		}else{
			$(".signature").remove();
			signatureFlag =true;					
		} 

		if(eme_number == ''){			
			emeNumberFlag = false;			
			$(".eme_number").remove();	
			$("#eme_number").parent().append("<div class='eme_number' style='color:red;'>Emergency contact no field is required.</div>");
		}
		else
		{
			if(eme_number.length < 8)
			{
				$(".eme_number").remove();	
				$("#eme_number").parent().append("<div class='eme_number' style='color:red;'>Emergency conatact field minimum 8 digits .</div>");
				emeNumberFlag =false;
			}
			else
			{	
				emeNumberFlag = true;
				$(".eme_number").remove();	
			}		
		}

		if(insurance_id == ''){			
			insuranceIDFlag = false;			
			$(".insurance_id").remove();	
			$("#insurance_id").parent().append("<div class='insurance_id' style='color:red;'>Insurance id field is required.</div>");
		}
		else
		{	
			if(insurance_id.length < 12)
			{
				$(".insurance_id").remove();	
				$("#insurance_id").parent().append("<div class='insurance_id' style='color:red;'>Insurance id field minimum 12 characters .</div>");
				insuranceIDFlag =false;
			}
			else
			{				
				insuranceIDFlag = true;
				$(".insurance_id").remove();
			}				
		}
		if(eme_name == ''){			
			emeNameFlag = false;			
			$(".eme_name").remove();	
			$("#eme_name").parent().append("<div class='eme_name' style='color:red;'>Emergency contact name field is required.</div>");
		}else{			
			if(eme_name.length < 3 )
			{
				$(".eme_name").remove();	
				$("#eme_name").parent().append("<div class='eme_name' style='color:red;'>Emergency contact name field minimum 3 characters .</div>");
				emeNameFlag =false;
			}
			else
			{	
				emeNameFlag = true;
				$(".eme_name").remove();	
			}
		}

		if(per_doc == ''){
			$(".per_doc").remove();	
			$("#per_doc").parent().append("<div class='per_doc' style='color:red;'>Personal document field is required.</div>");
			perDocFlag =false;
		}else{
			$(".per_doc").remove();
			perDocFlag =true;					
		}
		if(country == ''){ 
			$(".country").remove();	
			$("#country").parent().append("<div class='country' style='color:red;'>Country field is required.</div>");
			countryFlag =false;
		}else{
			$(".country").remove();
			countryFlag =true;					
		}

		if(state == ''){
			$(".state").remove();	
			$("#state").parent().append("<div class='state' style='color:red;'>State field is required.</div>");
			stateFlag =false;
		}else{
			$(".state").remove();
			stateFlag =true;					
		}

		if(city == ''){
			$(".city").remove();	
			$("#city").parent().append("<div class='city' style='color:red;'>City field is required.</div>");
			cityFlag =false;
		}else{
			$(".city").remove();
			cityFlag =true;					
		}

		if(street == ''){			
			streetFlag = false;			
			$(".street").remove();	
			$("#street").parent().append("<div class='street' style='color:red;'>Street field is required.</div>");
		}else{	
			if(street.length < 3 )
			{
				$(".street").remove();	
				$("#street").parent().append("<div class='street' style='color:red;'>Street name field minimum 3 characters .</div>");
				streetFlag =false;
			}
			else
			{				
				streetFlag = true;
				$(".street").remove();
			}	
		}

		if(zipcode == ''){			
			zipcodeFlag = false;			
			$(".zipcode").remove();	
			$("#zipcode").parent().append("<div class='zipcode' style='color:red;'>Zipcode field is required.</div>");
		}else{		
				if(zipcode.length < 5 )
				{
					$(".zipcode").remove();	
					$("#zipcode").parent().append("<div class='zipcode' style='color:red;'>Zipcode field minimum 5 digits .</div>");
					zipcodeFlag =false;
				}
				else
				{						
					zipcodeFlag = true;
					$(".zipcode").remove();	
				}			
		}

		if(county == ''){			
			countyFlag = false;			
			$(".county").remove();	
			$("#county").parent().append("<div class='county' style='color:red;'>County field is required.</div>");
		}else{
			if(county.length < 5 )
				{
					$(".county").remove();	
					$("#county").parent().append("<div class='county' style='color:red;'>County field minimum 5 digits .</div>");
					countyFlag =false;
				}
				else
				{	
					countyFlag = true;
					$(".county").remove();
				}				
		}
		//signatureFlag emeNumberFlag insuranceIDFlag emeNameFlag perDocFlag stateFlag  cityFlag streetFlag zipcodeFlag countyFlag
		
		var userTypeCheck ='';
		if(usertype == 2)
		{
			 userTypeCheck = "perDocFlag && insuranceIDFlag";
		}
		else
		{
			userTypeCheck =true;
		}

		if( signatureFlag && emeNumberFlag  && emeNameFlag  && countryFlag &&stateFlag && cityFlag && streetFlag && zipcodeFlag && countyFlag && userTypeCheck)
		{		
			var formData = new FormData($('#signupform')[0]);
			//$("#loadingImage").show();			
			$.ajax({
    				type:"POST",
    				url: baseURL+"set-registration",    				
    				//data:{'usertype':usertype,'fullname':fullname,'date_of_birth':date_of_birth,'emailaddrss':emailaddrss,'username':username,'phnnumber':phnnumber,'pass':pass,'country':country,'state':state,'city':city,'street':street,'zipcode':zipcode,'county':county,'eme_name':eme_name,'eme_number':eme_number,'signature':signature,'per_doc':per_doc ,'insurance_id':insurance_id,'formData':formData},
    				dataType :"json",
    				data:formData,
    				processData: false,
    				contentType: false,
    				success:function(response){ 					 					
    					if(response.status === true)
    					{
	    					$('.message').html('<div class="alert alert-success">'+response.message+'</div>');    					
	    					document.location.href = response.redirect;   	    									
    					}
    					else
    					{
    					$('.message').html('<div class="alert alert-danger">'+response.message+'</div>');
    						document.location.href = response.redirect;    					
    					}
    				}
			});	
		}			   	
    }); 
 	
 	$("#regBtn").click(function(){			    	
		window.location.href = baseURL+"registration";			    	
    });

	$("#loginBtn").click(function(){		
		window.location.href = baseURL+"frontend-login";			    	
	});

	 $('div.log_wi_btn').on("click",function() {
            $('div.hide_first_section_content').slideUp();  
            if(!$(this).next().is(':visible')){
            $(this).next().slideDown();
        }
    });
    $("div.hide_first_section_content").hide(); 

    $('div.log_wi_btn_second').on("click",function() {
        $('div.hide_second_section_content').slideUp();  
            if(!$(this).next().is(':visible')){
            $(this).next().slideDown();
            }
        });
    $("div.hide_second_section_content").hide(); 

	//$("div.hide_first_section_content").hide();  
	//$("div.hide_second_section_content").hide();			
});
	
/*function show_more_data(id)
{	
	//$(".hide_first_section_content").hide();
	$("#show_more_id_"+id).parent().next().toggle();	
}*/

/*function show_more_view_data(id)
{	
	//$(".hide_second_section_content").hide();
	$("#show_more_view_id_"+id).parent().next().toggle();					
}*/