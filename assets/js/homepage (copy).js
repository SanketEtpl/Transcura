	$(document).ready(function(){		
		var usertype = $("#usertype").val();
		var fullname = $("#fullname").val();
		var date_of_birth = $("#date_of_birth").val();
		var emailaddrss = $("#emailaddrss").val();
		var phnnumber = $("#phnnumber").val();
		var username = $('#username').val();
		var pass = $("#pass").val();
		var country = $("#country").val();
		var state = $("#state").val();
		var city = $("#city").val();
		var street = $("#street").val();
		var zipcode = $("#zipcode").val();
		var county = $("#county").val();					
		var eme_name = $("#eme_name").val();
		var eme_number = $("#eme_number").val();
		var insurance_id = $("#insurance_id").val();
		var per_doc = $("#per_doc").val();
		var emailPattern = /^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,6}$/i;	
		//var focusSet =false;
		$("#frm-second").hide();
		$("#btn-back").click(function(){
					$("#frm-second").hide();
					$("#frm-first").show();
					return false;
				});
		$('#usertype').change(function(){		
	       	if($(this).val() === '8') {
	           $('#NEMT').show();           
	        }
	       else
	       	{
	           $('#NEMT').hide();           
	       	}
	    });

	$("#btn-proceed").click(function(e){	
		
		var focusSet = false;
		if(!$('#usertype').val())
		{			
			$(".usertype").remove();	
			$("#usertype").parent().next(".usertype").remove(); // remove it
			if($("#usertype").parent().next(".usertype").length ==0 )
			{
				$("#usertype").parent().append("<div class='usertype' style='color:red;'>The user type field is required. </div>");

			}				
			e.preventDefault(); // prevent form from POST to server
	        $('#usertype').focus();
	        focusSet = true;				
		}					
		else
		{				
			$("#usertype").parent().next(".usertype").remove(); // remove it
		}
		if(!$('#fullname').val())
		{	
			$(".fullname").remove();
			$("#fullname").parent().next(".fullname").remove(); // remove it
			if($("#fullname").parent().next(".fullname").length ==0 )
			{
				$("#fullname").parent().append("<div class='fullname' style='color:red;'>The full name field is required. </div>");
			}

			e.preventDefault(); // prevent form from POST to server
			$('#fullname').focus();
			focusSet = true;				
		}					
		else
		{
			$("#fullname").parent().next(".fullname").remove(); // remove it
		}
		if(!$('#datepicker1').val())
		{			
			$(".datepicker1").remove();	
			$("#datepicker1").parent().next(".datepicker1").remove(); // remove it
			if($("#datepicker1").parent().next(".datepicker1").length ==0 )
			{
				$("#datepicker1").parent().append("<div class='datepicker1' style='color:red;'>The date field is required.</div>");
			}
			e.preventDefault(); // prevent form from POST to server
	        $('#datepicker1').focus();
	        focusSet = true;				
		}					
		else
		{
			$("#datepicker1").parent().next(".datepicker1").remove(); // remove it
		}
		
		//Ankush
		var flag =0;
		if(emailtxt == ''){
			$(".emailaddrss").remove();	
			$("#emailaddrss").parent().append("<div class='emailaddrss' style='color:red;'>The email field is required.</div>");
		}else{
			if( !emailPattern.test(emailaddrss)){
				$(".emailaddrss").remove();	
				$("#emailaddrss").parent().append("<div class='emailaddrss' style='color:red;'>The email invalid is required.</div>");
			}else{
				flag =1;
				$(".emailaddrss").remove();	
			}
		}
			

	/*	if(!$('#emailaddrss').val())
		{	
			$(".emailaddrss").remove();		
			$("#emailaddrss").parent().next(".emailaddrss").remove(); // remove it
			if($("#emailaddrss").parent().next(".emailaddrss").length ==0 )
			{
				$("#emailaddrss").parent().append("<div class='emailaddrss' style='color:red;'>The email field is required.</div>");
			}			
			else if( !emailPattern.test(emailaddrss)) {
				alert('invalid');
				$("#emailaddrss").parent().append("<div class='emailaddrss' style='color:red;'>The email invalid is required.</div>");
			}			
			e.preventDefault(); // prevent form from POST to server
	        $('#emailaddrss').focus();
	        focusSet = true;				
		}					
		else
		{
			$("#emailaddrss").parent().next(".emailaddrss").remove(); // remove it
		}*/

		// function isValidEmailAddress(emailAddress) {
		//     var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		//     return pattern.test(emailAddress);
		// };
		if(!$('#phnnumber').val())
		{
			$(".phnnumber").remove();	
			$("#phnnumber").parent().next(".phnnumber").remove(); // remove it
			if($("#phnnumber").parent().next(".phnnumber").length ==0 )
			{
				$("#phnnumber").parent().append("<div class='phnnumber' style='color:red;'>The phone no field is required.</div>");
			}

			e.preventDefault(); // prevent form from POST to server
	        $('#phnnumber').focus();
	        focusSet = true;				
		}					
		else
		{
			$("#phnnumber").parent().next(".phnnumber").remove(); // remove it
		}
		if(!$('#pass').val())
		{		
			$(".pass").remove();	
			$("#pass").parent().next(".pass").remove(); // remove it	
			if($("#pass").parent().next(".pass").length ==0 )
			{
				$("#pass").parent().append("<div class='pass' style='color:red;'>The password field is required.</div>");
			}
			e.preventDefault(); // prevent form from POST to server
	        $('#pass').focus();
	        focusSet = true;				
		}					
		else
		{
			$("#pass").parent().next(".pass").remove(); // remove it
		}			
		if(!$('#conpass').val())
		{			
			$(".conpass").remove();
			$("#conpass").parent().next(".conpass").remove(); // remove it	
			if($("#conpass").parent().next(".conpass").length ==0 )
			{
				$("#conpass").parent().append("<div class='conpass' style='color:red;'>The confirm password field is required.</div>");
			}
			e.preventDefault(); // prevent form from POST to server
	        $('#conpass').focus();
	        focusSet = true;				
		}					
		else
		{
			$("#conpass").parent().next(".conpass").remove(); // remove it
		}
		//var intRegex = /[0-9 -()+]+$/;
		if(!$('#username').val())
		{			
			$(".username").remove();
			$("#username").parent().next(".username").remove(); // remove it	
			if($("#username").parent().next(".username").length ==0 )
			{
				$("#username").parent().append("<div class='username' style='color:red;'>The username field is required.</div>");
			}
			e.preventDefault(); // prevent form from POST to server
	        $('#username').focus();
	        focusSet = true;				
		}					
		else
		{
			$("#username").parent().next(".username").remove(); // remove it
		}
		//if( username != '' && usertype != '' && fullname != '' && datepicker1 != '' && emailaddrss != '' && phnnumber != '' && pass != '')
		if($('#usertype').val() && $('#fullname').val() && $('#username').val() && $('#date_of_birth').val() && $('#emailaddrss').val() && $('#phnnumber').val() && $('#pass').val() && $('#conpass').val())
		{
			//alert("heloo@@@@@@@@@"); return false;
			//alert("heloo");
			$("#frm-second").show();
			$("#frm-first").hide();
			return false;				
		}
	});
		 $('#btn-submit').click(function(){
    	   		
		if(!$('#insurance_id').val())
		{				
			$("#insurance_id").parent().next(".validation").remove(); // remove it
			if($("#insurance_id").parent().next(".validatition").length ==0 )
			{
				$("#insurance_id").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>The insurance id field is required.</div>");
			}				
			e.preventDefault(); // prevent form from POST to server
	        $('#insurance_id').focus();
	        focusSet = true;				
		}					
		else
		{				
			$("#insurance_id").parent().next(".validation").remove(); // remove it
		}
		if(!$('#per_doc').val())
		{				
			$("#per_doc").parent().next(".validation").remove(); // remove it
			if($("#per_doc").parent().next(".validatition").length ==0 )
			{
				$("#per_doc").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>The personal document field is required.</div>");
			}				
			e.preventDefault(); // prevent form from POST to server
	        $('#per_doc').focus();
	        focusSet = true;				
		}					
		else
		{				
			$("#per_doc").parent().next(".validation").remove(); // remove it
		}
			
		if(!$('#country').val())
		{				
			$("#country").parent().next(".validation").remove(); // remove it
			if($("#country").parent().next(".validatition").length ==0 )
			{
				$("#country").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>The country field is required.</div>");
			}				
			e.preventDefault(); // prevent form from POST to server
	        $('#country').focus();
	        focusSet = true;				
		}					
		else
		{				
			$("#country").parent().next(".validation").remove(); // remove it
		}

		if(!$('#state').val())
		{				
			$("#state").parent().next(".validation").remove(); // remove it
			if($("#state").parent().next(".validatition").length ==0 )
			{
				$("#state").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>The state field is required.</div>");
			}				
			e.preventDefault(); // prevent form from POST to server
	        $('#state').focus();
	        focusSet = true;				
		}					
		else
		{				
			$("#state").parent().next(".validation").remove(); // remove it
		}
		if(!$('#city').val())
		{				
			$("#city").parent().next(".validation").remove(); // remove it
			if($("#city").parent().next(".validatition").length ==0 )
			{
				$("#city").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>The city field is required.</div>");
			}				
			e.preventDefault(); // prevent form from POST to server
	        $('#city').focus();
	        focusSet = true;				
		}					
		else
		{				
			$("#city").parent().next(".validation").remove(); // remove it
		}
		if(!$('#street').val())
		{				
			$("#street").parent().next(".validation").remove(); // remove it
			if($("#street").parent().next(".validatition").length ==0 )
			{
				$("#street").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>The street field is required.</div>");
			}				
			e.preventDefault(); // prevent form from POST to server
	        $('#street').focus();
	        focusSet = true;				
		}					
		else
		{				
			$("#street").parent().next(".validation").remove(); // remove it
		}
		if(!$('#zipcode').val())
		{				
			$("#zipcode").parent().next(".validation").remove(); // remove it
			if($("#zipcode").parent().next(".validatition").length ==0 )
			{
				$("#zipcode").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>The zipcode field is required.</div>");
			}				
			e.preventDefault(); // prevent form from POST to server
	        $('#zipcode').focus();
	        focusSet = true;				
		}					
		else
		{				
			$("#zipcode").parent().next(".validation").remove(); // remove it
		}
		if(!$('#county').val())
		{				
			$("#county").parent().next(".validation").remove(); // remove it
			if($("#county").parent().next(".validatition").length ==0 )
			{
				$("#county").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>The county field is required.</div>");
			}				
			e.preventDefault(); // prevent form from POST to server
	        $('#county').focus();
	        focusSet = true;				
		}					
		else
		{				
			$("#county").parent().next(".validation").remove(); // remove it
		}		
		
		if(!$('#eme_name').val())
		{				
			$("#eme_name").parent().next(".validation").remove(); // remove it
			if($("#eme_name").parent().next(".validatition").length ==0 )
			{
				$("#eme_name").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>The emergency contact name field is required.</div>");
			}				
			e.preventDefault(); // prevent form from POST to server
	        $('#eme_name').focus();
	        focusSet = true;				
		}					
		else
		{				
			$("#eme_name").parent().next(".validation").remove(); // remove it
		}
		if(!$('#signature').val())
		{				
			$("#signature").parent().next(".validation").remove(); // remove it
			if($("#signature").parent().next(".validatition").length ==0 )
			{
				$("#signature").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>The signature field is required.</div>");
			}				
			e.preventDefault(); // prevent form from POST to server
	        $('#signature').focus();
	        focusSet = true;				
		}					
		else
		{				
			$("#signature").parent().next(".validation").remove(); // remove it
		}
		if(!$('#eme_number').val())
		{				
			$("#eme_number").parent().next(".validation").remove(); // remove it
			if($("#eme_number").parent().next(".validatition").length ==0 )
			{
				$("#eme_number").parent().after("<div class='validation' style='color:red;margin-bottom: 20px;'>The emergency contact no field is required.</div>");
			}				
			e.preventDefault(); // prevent form from POST to server
	        $('#eme_number').focus();
	        focusSet = true;				
		}					
		else
		{				
			$("#eme_number").parent().next(".validation").remove(); // remove it
		}

		
		//alert('sdfasdf');return false;
			$.ajax({
    				type:"POST",
    				url: "<?php echo base_url(); ?>set-registration",
    				data:{'usertype':usertype,'fullname':fullname,'datepicker1':datepicker1,'emailaddrss':emailaddrss,'phnnumber':phnnumber,'pass':pass,'country':country,'state':state,'city':city,'street':street,'zipcode':zipcode,'county':county,'eme_name':eme_name,'eme_number':eme_number},
    				dataType :"json",
    				success:function(json){
    					if(json.flag == 'success')
    					$('.message').html('<div class="alert alert-success">'+json.status+'</div>');
    					else
    					$('.message').html('<div class="alert alert-danger">'+json.status+'</div>');	
    					//alert("success ntmt"+json.status);
    				}
			});	    	   	
    });    

    $('#country').on('change',function(){
    	var countryID = $(this).val();	       
        if(countryID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>frontend/Homecontroller/get_state',
                data:{countryID:countryID},
                dataType:'json',
                success:function(json){                	                    
                     $('#state').html(json.state);
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
    
    $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>frontend/Homecontroller/get_city',
                data:{'state_id':stateID},
                dataType:'json',
                success:function(json){
                    $('#city').html(json.city);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
});
var today = new Date();
var n = today.getFullYear();
var y = n-50;
//Datepicker
$( "#date_of_birth" ).datepicker({	
	changeMonth:true,
	changeYear:true,
	maxDate:0,
	//defaultDate: "10/10/1980",
	yearRange: "1970:{ n }"	
});	
