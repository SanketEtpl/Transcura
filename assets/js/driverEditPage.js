
$("#driver_edit_profile").click(function(){
	//var fullnmFlag =false;
 	var fullname = $('#fullname').val();
	var date_of_birth = $("#date_of_birth").val();
	var phnnumber = $("#phnnumber").val();
	var country = $("#country").val();
	var state = $("#state").val();
	var city = $("#city").val();
	var zipcode = $("#zipcode").val();
	var county = $('#county').val();
	var street = $("#street").val();
	var vehicle_reg_documents_id = $("#vehicle_reg_documents_id").val();
	var pers_doc_dd = $("#pers_doc_dd").val();
	var insurance_id = $('#insurance_id').val();
	var own_vehicle_dd = $("#own_vehicle_dd").val();
	var company_vehicle_dropdown = $('#company_vehicle_dropdown').val();
	var eme_name= $("#eme_name").val();
	var eme_number = $("#eme_number").val();

  	if(fullname == ''){
		$("#fullname_error").html("Full name field is required.");
		$("#fullname_error").addClass("error").css("color", "red");
		return false;	
	}else{
		if(fullname.length < 3){				
			$("#fullname_error").html("Full name field must contain minimum 3 characters.");
			$("#fullname_error").addClass("error").css("color", "red");
			return false;	
		}else{	
			$("#fullname_error").html('');	
			$("#fullname_error").removeClass("error");		
		}					
	}

  	if(date_of_birth == ''){		
		$("#date_of_birth_error").html("Date of birth field is required.");
		$("#date_of_birth_error").addClass("error").css("color", "red");
		return false;	
	}else{
		$("#date_of_birth_error").remove();	
		$("#date_of_birth_error").removeClass("error");	
	}

	if(phnnumber == ''){
		$("#phnnumber_error").html("Phone no field is required.");
		$("#phnnumber_error").addClass("error").css("color", "red");
		return false;
	}else{
		if(phnnumber.length < 8){			
			$("#phnnumber_error").html("Phone no field must contain minimum 8 digits.");
			$("#phnnumber_error").addClass("error").css("color", "red");
			return false;
		}else{			
			$("#phnnumber_error").remove();	
			$("#phnnumber_error").removeClass("error");	
			
		}				
	}

	if(country == ''){ 		
		$("#country_error").html("Country field is required.");
		$("#country_error").addClass("error").css("color", "red");
		return false;
	}else{
		$("#country_error").remove();
		$("#country_error").removeClass("error");
						
	}

	if(state == ''){			
		$("#state_error").html("State field is required.");
		$("#state_error").addClass("error").css("color", "red");
		return false;
	}else{
		$("#state_error").remove();
		$("#state_error").removeClass("error");
						
	}

	if(city == ''){			
		$("#city_error").html("City field is required.");
		$("#city_error").addClass("error").css("color", "red");
		return false;
	}else{
		$("#city_error").remove();
		$("#city_error").removeClass("error");
							
	}

	if(street == ''){				
		$("#street_error").html("Street field is required.");
		$("#street_error").addClass("error").css("color", "red");
		return false;
	}else{	
		if(street.length < 3 )
		{				
			$("#street_error").html("Street name field must contain minimum 3 characters .");
			$("#street_error").addClass("error").css("color", "red");
			return false;
		}
		else
		{
			$("#street_error").remove();
			$("#street_error").removeClass("error");
			
		}	
	}

	if(zipcode == ''){	
		$("#zipcode_error").html("Zipcode field is required.");
		$("#zipcode_error").addClass("error").css("color", "red");
		return false;
	}else{		
			if(zipcode.length < 5 )
			{
				$("#zipcode_error").html("Zipcode field must contain minimum 5 digits .");
				$("#zipcode_error").addClass("error").css("color", "red");
				return false;
			}
			else
			{	
				$("#zipcode_error").remove();
				$("#zipcode_error").removeClass("error");	
				
			}			
	}

	if(county == ''){	
		$("#county_error").html("County field is required.");
		$("#county_error").addClass("error").css("color", "red");
		return false;
	}else{
		if(county.length < 5 )
		{
			$("#county_error").html("County field must contain minimum 5 digits .");
			$("#county_error").addClass("error").css("color", "red");
			return false;
		}
		else
		{					
			$("#county_error").remove();
			$("#county_error").removeClass("error");	
			
		}				
	}

	if(vehicle_reg_documents_id == ''){				
		$("#vehicle_reg_documents_id_error").html("Vehicle registration field is required.");
		$("#vehicle_reg_documents_id_error").addClass("error").css("color", "red");
		return false;
	}else{
		if(vehicle_reg_documents_id == 1){			
			$('#vehicle_registrat_div_id').show();			

		}else{
			$("#vehicle_reg_documents_id_error").remove();
			$("#vehicle_reg_documents_id_error").removeClass("error");
			
		}
							
	}

	if(pers_doc_dd == ''){
		
		$("#pers_doc_dd_error").html("Personal document field is required.");
		$("#pers_doc_dd_error").addClass("error").css("color", "red");
		return false;
		
	}else{
		$("#pers_doc_dd_error").remove();
		$("#pers_doc_dd_error").removeClass("error");

		if(pers_doc_dd == 1){
			if(insurance_id == ''){
				$("#insurance_id_error").html("Insurance id field is required.");
				$("#insurance_id_error").addClass("error").css("color", "red");
				return false;
			}else{	
				if(insurance_id.length < 12){
					$("#insurance_id_error").html("Insurance id field must contain minimum 12 characters .");
					$("#insurance_id_error").addClass("error").css("color", "red");
					return false;
				}else{	
					$("#insurance_id_error").remove();
					$("#insurance_id_error").removeClass("error");			
				}				
			}
		}					
	} 


	
	

	
	if(own_vehicle_dd == ''){
			
			$("#own_vehicle_dd_error").html("Own business field is required.");
			$("#own_vehicle_dd_error").addClass("error").css("color", "red");
			return false;
		}else{
			$("#own_vehicle_dd_error").remove();
			$("#own_vehicle_dd_error").removeClass("error");
				
		}

		if(company_vehicle_dropdown == ''){				
			$("#company_vehicle_dropdown_error").html("Company vehicle field is required.");
			$("#company_vehicle_dropdown_error").addClass("error").css("color", "red");
			return false;
		}else{
			$("#company_vehicle_dropdown_error").remove();
			$("#company_vehicle_dropdown_error").removeClass("error");
								
		}


		if(eme_name == ''){	
			$("#eme_name_error").html("Emergency contact name field is required.");
			$("#eme_name_error").addClass("error").css("color", "red");
			return false;
		}else{			
			if(eme_name.length < 3 )
			{
				$("#eme_name_error").html("Emergency contact name field must contain minimum 3 characters .");
				$("#eme_name_error").addClass("error").css("color", "red");
				return false;
			}
			else
			{	
				$("#eme_name_error").remove();
				$("#eme_name_error").removeClass("error");	
				
			}
		}

		if(eme_number == ''){		
			
			$("#eme_number_error").html("Emergency contact no field is required.");
			$("#eme_number_error").addClass("error").css("color", "red");
			return false;
		}
		else
		{
			if(eme_number.length < 8)
			{	
				$("#eme_number_error").html("Emergency conatact field must contain minimum 8 digits .");
				$("#eme_number_error").addClass("error").css("color", "red");
				return false;
			}
			else
			{
				$("#eme_number_error").remove();
				$("#eme_number_error").removeClass("error");	
				
			}		
		}

});
$(document).ready(function(){

	var fullname = $('#fullname').val();
	var date_of_birth = $("#date_of_birth").val();
	var phnnumber = $("#phnnumber").val();
	var country = $("#country").val();
	var state = $("#state").val();
	var city = $("#city").val();
	var zipcode = $("#zipcode").val();
	var county = $('#county').val();
	var street = $("#street").val();
	var vehicle_reg_documents_id = $("#vehicle_reg_documents_id").val();
	var pers_doc_dd = $("#pers_doc_dd").val();
	var insurance_id = $('#insurance_id').val();
	var own_vehicle_dd = $("#own_vehicle_dd").val();
	var company_vehicle_dropdown = $('#company_vehicle_dropdown').val();
	var eme_name= $("#eme_name").val();
	var eme_number = $("#eme_number").val();
	var vehicle_regi_doc_old = $("#vehicle_regi_doc_old").val();
	$('#vehicle_registrat_div_id').hide();
	$('#own_bussin_show').hide();
	$('#compa_vehicle_show').hide();
	$('#drivers').hide();
	$('#vehicle_registrat_date_id').hide();

if(vehicle_reg_documents_id == 1){			
	$('#vehicle_registrat_div_id').show();	
}

if(own_vehicle_dd == 1){
	$('#own_bussin_show').show();
}

if(company_vehicle_dropdown == 1){
	$('#compa_vehicle_show').show();
}

if(pers_doc_dd == 1){	
	$('#drivers').show();
}

if(vehicle_regi_doc_old != ''){
	$('#vehicle_registrat_date_id').show();
}
$('#vehicle_registration_doc').change(function(){
	//alert('2222');
	if($(this).val() != '')
		$('#vehicle_registrat_date_id').show();
});

$("#vehicle_reg_documents_id").change(function() {
	if($(this).val() == 1){			
	$('#vehicle_registrat_div_id').show();	
	}else{
	  $('#vehicle_registrat_div_id').hide();
	}
});

$("#own_vehicle_dd").change(function(){

	if($(this).val() == 1){			
	$('#own_bussin_show').show();	
	}else{
	  $('#own_bussin_show').hide();
	}
});


$('#company_vehicle_dropdown').change(function(){
	if($(this).val() == 1){			
	$('#compa_vehicle_show').show();	
	}else{
	  $('#compa_vehicle_show').hide();
	}
});


$('#pers_doc_dd').change(function(){
	if($(this).val() == 1){			
	$('#drivers').show();	
	}else{
	  $('#drivers').hide();
	}
});

  $('#state1').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:baseURL+'frontend/Homecontroller/get_city',
                data:{'state_id':stateID},
                dataType:'json',
                success:function(json){
                    $('#city1').html(json.city);
                }
            }); 
        }else{
            $('#city1').html('<option value="">Select state first</option>'); 
        }
    });


});


