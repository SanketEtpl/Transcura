<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Api extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("common_model");
        $this->load->library("email");
        $this->load->library("User_AuthSocial");
        $this->load->helper("email_template");
        $this->load->helper('string');
        $this->load->library('upload');
        $this->load->library("image_lib");
      
        $this->pro_profile_origin =  realpath('uploads/Patients/original');
        $this->pro_profile_thumb =  realpath('uploads/Patients/thumb');
         $this->pro_profile_origin1 =  realpath('uploads/Driver/original');


    }

      
/*   Register pateints and drivers */

 public function registerUser_post()
{

$postData = $_POST;
$reset_token = random_string('unique');
$usertype = trim($this->post('usertype'));
$base_url = $this->config->item('base_url');
$reset_url = $base_url."accountverify/".$reset_token ; 
    if(trim($postData["password"])==trim($postData["cpassword"])){

            $insertArr = array(  "user_token" => mt_rand(100000, 999999),
                        "password"     =>md5(trim($postData["password"])),
                        "first_name"   =>trim($postData["fullname"]),                 
                        "Username"   => trim($postData["username"]),  
                        "phone"        =>trim($postData["phone"]),
                        "email"        =>trim($postData["email"]),
                        "picture"   =>trim("images/default.jpg"),
                       "status"=>"inactive",
                       "token"=>$reset_token,
                       "created_at"=>date("Y-m-d H:i:s"),
                       "device_type"=>trim($postData["device_type"]),
                       "done"   =>"0",
                       "user_type"=>$usertype    
                                  );


     $emailexist = $this->common_model->select("email",TB_USERS,array("email"=>trim($this->post('email'))));
            if(count($emailexist)>0) {
            $this->response(array("status"=>200, "statusCode"=>false, "message"=> "User already exist with this email id."), 200);
            }
            else{         
                       $user = $this->common_model->insert(TB_USERS,$insertArr);
                      $userinfo= $this->common_model->select("Username,user_type,email",TB_USERS,array("email"=>trim($this->post('email'))));
                      
                        if($user)
                        {
                        $to_email = $this->post('email');
                        $hostname = $this->config->item('hostname');  
                        $config['mailtype'] ='html';
                        $config['charset'] ='iso-8859-1';

                        $this->email->initialize($config);
                        $from  = EMAIL_FROM; 
                        $this->messageBody  = email_header();
                        // $this->messageBody  .= '<tr>
                        //     <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                        //         <p>Dear '.$userinfo[0]['Username'].',</p>
                        //             <p>
                        //             You are registered successfully on Transcura.<br>
                        //             </p>
                                   
                        //             </p>

                        //         </td>
                        //     </tr>
                        //     <tr>';    


                 $this->messageBody  .= '<tr>
                <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                    <p>Dear Customer,</p>
                        <p>
                        You are registered successfully on Transcura as a patient.<br>
                        Please click the link below to verify your email address.</p>
                        <p>
                        <p>Activation Link : </p>
                        <a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href='.$reset_url.'>Click Here</a>
                        </p><p>
                        If the link is not working properly, then copy and paste the link in your 
                        browser. <br>If you did not send this request, please ignore this email.</p>
                    </td>
                </tr>';


                        $this->messageBody  .= email_footer();        //print_r( $this->messageBody);die;
                        $this->email->from($from, $from);
                        $this->email->to($to_email);
                        $this->email->subject('Welcome to Transcura');
                        $this->email->message($this->messageBody);
                        $this->email->send();
                        $this->response(array("status"=>200,"statusCode"=>true,"message"=> "Thank You! Please check your email to activate your account.","statusCode"=>"true"), 200);  
                        }

            }

    }
    else
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=> "Confirm password not match."), 200);

    }

}

/*   login pateints and drivers */

public function loginUser_post() {

            $postData = $_POST; 
$username=trim($this->post('username'));
$email=trim($this->post('username'));
$password=trim($this->post('password'));
 $base_url = $this->config->item('base_url');
//where (username='$username' or email='$email');


            if (strpos(trim($this->post('username')), '@') !== false) {
            $result = $this->common_model->select("*",TB_USERS,array("email"=>trim($this->post('username')),"password"=>md5(trim($this->post('password')))));
            }
            else {
            $result = $this->common_model->select("*",TB_USERS,array("Username"=>trim($this->post('username')),"password"=>md5(trim($this->post('password')))));
            }


//echo $this->db->last_query() ;die;
            if(count($result) > 0)  {
                if($result[0]['status']=='active'){
              //   $this->session->set_userdata("auth_user", $result[0]); 

 $userinfo = array(
                         "fullname" => $result[0]["first_name"],
                         "email" => $result[0]["email"],
                         "phone" => $result[0]["phone"],
                         "address" => $result[0]["address"],
                          "address" => $result[0]["address"],
                           "date_of_birth" => $result[0]["date_of_birth"],
                           "zipcode" => $result[0]["zipcode"],
                           "picture" =>  $base_url.$result[0]["picture"],
                            "zipcode" => $result[0]["zipcode"],
                           "Username" => $result[0]["Username"],
                            "mobile" =>$result[0]["mobile"],                          
                            "user_type" =>$result[0]["user_type"],
                            "done" =>$result[0]["done"],
                             "id" => $result[0]["id"],
                              "username" => $result[0]["Username"],
                                  "id" => $result[0]["id"],
                              "password" => $result[0]["password"]
                              


                         );




                $this->response(array("status"=>200,"statusCode"=>true,"user"=>$userinfo,"message"=> "User has been login successfully." ,"statusCode"=>"true"), 200);  
                }
                else {
                $this->response(array("status"=>200,"statusCode"=>false,"message"=> "User not activate."), 200);
                }
            }
            else {

             $this->response(array("status"=>200,"statusCode"=>false, "message"=> "Invalid username or password, Please try again."), 200);
            }

}

/*   forgotpassword */

 public function forgotPassword_post() {
        $email = $this->post('email');
        $userdata = $this->common_model->select("`id`,`first_name`,`email`,`password`,`phone`,`Username`",TB_USERS,array("email"=>trim($email),"status"=>"active"));
        if(count($userdata)==0) {
            
         $this->response(array("status"=>200,"statusCode"=>false, "message"=> "This email does not exist or active."), 200);
        }
        
        else {

            $userId = $userdata[0]['id'];
            $reset_token = random_string('unique');

            $reset_url = base_url()."frontend/login/resetPassword/".$reset_token."/".$userId ;     
           // $reset_url = base_url()."forgot-pwd/".$reset_token."/".$userId ;            
            $useremail =trim($userdata[0]["email"]) ;               
            $datareset = array('token' =>  $reset_token); 
            $cond1 = array('email' =>  $useremail); 
            $this->common_model->update(TB_USERS,$cond1,$datareset);       
            //Send Reset password Link
            $hostname = $this->config->item('hostname');
            $config['mailtype'] ='html';
            $config['charset'] ='iso-8859-1';
            $this->email->initialize($config);
            $from  = EMAIL_FROM; 
            $this->messageBody  = email_header();
            $this->messageBody  .= '<tr> 
                <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                    <p>Hello,</p>
                        <p>
                        A request has been made to reset your Transcura account password.<br>
                        Please click the link below to reset your password.</p>
                        <p>
                        <p>Activation Link : </p>
                        <a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href="'.$reset_url.'">Click Here</a>
                        </p><p>
                        If the link is not working properly, then copy and paste the link in your 
                        browser. <br>If you did not send this request, please ignore this email.</p>
                    </td>
                </tr>
                <tr>';                      
            $this->messageBody  .= email_footer();
            $this->email->from($from, $from);
            $this->email->to($email);
            $this->email->subject('Reset Password Link');
            $this->email->message($this->messageBody);
            $this->email->send();         
           $this->response(array("status"=>200,"statusCode"=>true, "message"=> "A link to reset password has been sent to your email. please check your email."), 200);
        }

}




// /*   login pateints and drivers */
// public function uploadUserPic_post(){

//    //      $this->response(array("status"=>200,"statusCode"=>true, "message"=> $_FILES), 200);
// //print_r($_FILES);exit;

//                 $postData = $this->input->post();

//              // $name = $_FILES["picture"]["name"];
//            //   print_r($_FILES);die;
//                $dateTime = date("Y-m-d H:i:s");
//                $config['upload_path']   =    $this->pro_profile_origin ;          
//                $config['allowed_types'] =   "gif|jpg|jpeg|png";  
//                $config['max_size']      =   "5000"; 
//                $config['max_width']     =   "1000"; 
//                $config['max_height']    =   "850"; 
//                $config['min_width']     = "125"; // new
//                $config['min_height']    = "75"; // new              
//                $this->load->library('upload');     
//                $this->upload->initialize($config);
//               // move_uploaded_file($_FILES['userfile']['tmp_name'],'./uploads/test.jpg');
//           $userid = $this->post('userid');
//     if($userid!='')
//        {

//             if(isset($_FILES["picture"]["name"]))   
//             {   
//                // print_r($_FILES);die;
//                     if(!$this->upload->do_upload('picture'))
//                     {         
//                     $error = $this->upload->display_errors();
//                     $this->response(array("status"=>200,"statusCode"=>false, "message"=> $error), 200);
//                     } 
//                     else
//                     {        
//                     $data=$this->upload->data();
//                     $data = $this->resize($data);              

                 
//                     $insertArr = array(                                
//                                 "picture"=>$data['new_image']                             
//                                 );
//                     $insertId = $this->common->update(TB_USERS,array('id'=>$userid),$insertArr);
//                     if($insertId){
//                         $this->response(array("status"=>200, "statusCode"=>true ,"message"=> "Your profile picture has been updated successfully.","picture"=> $data['new_image']), 200); 
//                     }else{
//                        $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload valid profile picture"), 200);
//                     }
                                    
                   
//                     }                
//             }
       
//             else
//                {

//             $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload valid profile picture"), 200);
//                } 

//    //  $this->response(array("status"=>200,"statusCode"=>true,"Userid"=>$userid, "message"=>"sucess"), 200);   

//        }



//     else
//        {

//     $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please user id required"), 200);
//        }




          
//     }



// public function resize($image_data) {
//  // $image_data['orig_name'];
//         //$img = substr($image_data['full_path'], 51);
//         $config['image_library'] = 'gd2';
//         $config['source_image'] = $image_data['full_path'];
//         $config['new_image'] ='uploads/Patients/thumb/thumb_'.$image_data['orig_name'];
//         $config['allowed_types'] = 'png|mpg|mpeg|wmv|jpg';
//         $config['width'] = 350;
//         $config['height'] = 175;
//         $this->image_lib->initialize($config);
//         $src = $config['new_image'];
//         $data['new_image'] = substr($src, 0);
//         $data['img_src'] =$data['new_image'];
//         //print_r( $data);die;
//         if (!$this->image_lib->resize())
//                     {
//                     $errror= $this->image_lib->display_errors(); 
//                     return  $error;
//                     }
//         else
//         {
//        return  $data;
        
              
//         }

                  
//     }



/*   edit profile pateints and drivers */
public function uploadUserPic_post(){

      
               $postData = $this->input->post();
               $dateTime = date("Y-m-d H:i:s");
         $base_url = $this->config->item('base_url');                
               
               //move_uploaded_file($_FILES['userfile']['tmp_name'],'./uploads/test.jpg');die;
              $userid = $this->post('userid');
    if($userid!='')
       {

                  $userlist = $this->common_model->select("user_type",TB_USERS,array("id"=>$userid));

 if( $userlist[0]['user_type']=='Driver')
 {


$config['upload_path']   =    $this->pro_profile_origin1 ; 

 }
 if( $userlist[0]['user_type']=='Patient')
 {

$config['upload_path']   =    $this->pro_profile_origin ; 


 }
$base_url = $this->config->item('base_url');
               $config['allowed_types'] =   "png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG";  
               $config['max_size']      =   "5000"; 
               $config['max_width']     =   "1000"; 
               $config['max_height']    =   "900"; 
               $config['min_width']     = "125"; // new
               $config['min_height']    = "70"; // new              
               $this->load->library('upload');     
               $this->upload->initialize($config);

            if(isset($_FILES["picture"]["name"]))   
            {   
                    if(!$this->upload->do_upload('picture'))
                    {         
                    $error = $this->upload->display_errors();
                    $this->response(array("status"=>200,"statusCode"=>false, "message"=> $error), 200);
                    } 
                    else
                    {        
                    $data=$this->upload->data();
                    $data = $this->resize($data,$userlist[0]['user_type']);              

                 
                    $insertArr = array(                                
                                "picture"=>$data['new_image']                             
                                );
                    $insertId = $this->common->update(TB_USERS,array('id'=>$userid),$insertArr);
                    if($insertId){
                        $this->response(array("status"=>200,"statusCode"=>true, "message"=> "Your profile picture has been updated successfully.","picture"=> $base_url.$data['new_image']), 200); 
                    }else{
                       $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload valid profile picture"), 200);
                    }
                                    
                   
                    }                
            }
       
            else
               {


            $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload valid profile picture"), 200);
               } 

       }
    else
       {

    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please user id required"), 200);
       }
          
    }









public function resize($image_data,$type) {

   $userlist = $this->common_model->select("user_type",TB_USERS,array("id"=>$_REQUEST['userid']));


//print_r($image_data['raw_name'].$image_data['file_ext']);die;
 if( $userlist[0]['user_type']=='Driver')
 {

$config['new_image'] ='uploads/Driver/thumb/thumb_'.$image_data['raw_name'].$image_data['file_ext'];

 }
 if( $userlist[0]['user_type']=='Patient')
 {

$config['new_image'] ='uploads/Patients/thumb/thumb_'.$image_data['raw_name'].$image_data['file_ext'];


 }
        $img = substr($image_data['full_path'], 51);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $image_data['full_path'];
        $config['allowed_types'] = 'png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG';
        $config['width'] = 350;
        $config['height'] = 175;
        $this->image_lib->initialize($config);
        $src = $config['new_image'];
        $data['new_image'] = substr($src, 0);
        $data['img_src'] =$data['new_image'];
        if (!$this->image_lib->resize())
                    {
                    $errror= $this->image_lib->display_errors(); 
                    return  $error;
                    }
        else
        {
       return  $data;
        
              
        }

                  
    }

public function insuranceproviderList_get()
{
        $postData = $this->input->post();                       
        $insurancelist = $this->common_model->select("insurance_provider,insurance_id",TB_INSURANCELIST);
        if( count($insurancelist) >0)
        {          

            foreach($insurancelist as $key => $value)
            {
            $insurance[] = array("insurance_provider" => $insurancelist[$key]["insurance_provider"],
                "insurance_id" => $insurancelist[$key]["insurance_id"]);            
            $this->response(array("status"=>200,"statusCode"=>true,"insurance_info"=>$insurance), 200); 

            }
               
        }
      else
        {
        $this->response(array("status"=>200,"statusCode"=>false, "message"=> "Data not found"), 200); 
        } 

}



public function editProfile_post()
{



    $postData = $this->input->post();
    $dateTime = date("Y-m-d H:i:s");                          
    $userid = $this->post('userid');
    $insurance_id = trim($this->post('insurance_id'));
    $signature = trim($this->post('signature')) ;
    $base_url = $this->config->item('base_url');
    if($userid=='')
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please user id required"), 200);
    }
    
    else if($insurance_id =='')
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please insurance required"), 200);
    }
   else if($signature =='')
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"select signature option"), 200);
    }
    // else if(!isset($_FILES["personal_doc"]["name"]))
    // {
    // $this->response(array("status"=>200,"statusCode"=>false, "message"=>"upload personal doc"), 200);
    // }
  
   else
       {
          if(isset($_FILES["personal_doc"]["name"]))
        {
        $file_name = $_FILES["personal_doc"]["name"];
        $original_file_name = $file_name;
        $random = rand(1, 10000000000000000);
        $makeRandom = $random;
        $file_name_rename = $makeRandom;
        $explode = explode('.', $file_name);
        if(count($explode) >= 2) {
            $new_file = $file_name_rename.'.'.$explode[1];
            //print_r($new_file);die;
            $config['upload_path'] = "./uploads/Patients/doc";
            $config['allowed_types'] = "pdf|doc|xml|docx|GIF|PDF|DOC|XML|DOCX|xls|xlsx|txt|ppt|csv";
            $config['file_name'] = $new_file;
            $config['max_size'] = '307210';
            $config['max_width'] = '300000';
            $config['max_height'] = '300000';
             $this->load->library('upload',$config);
              $this->upload->initialize($config);
            if(!$this->upload->do_upload("personal_doc")) {
                //print_r($this->upload->display_errors());
               //  $doc = 'uploads/Patients/doc/'.$new_file; 

                $this->response(array("status"=>200,"statusCode"=>false, "message"=>$this->upload->display_errors()), 200);
            } else {
                 $doc = 'uploads/Patients/doc/'.$new_file;    
                }
        } 

        else {
           $this->response(array("status"=>200,"statusCode"=>false, "message"=>"upload valid file"), 200);
        }
       }

      else
      {

       $doc = trim($this->post('personal_docold')) ;   
      }

                   $insertArr = array(
                            "date_of_birth"=>trim($this->post('date_of_birth')),
                            "email"=>trim($this->post('email')),
                                                     
                            "address"=>trim($this->post('address')),
                            "zipcode"=>trim($this->post('zipcode')),
                            "mobile"=>trim($this->post('mobile')),
                            "insurance_id"=>trim($this->post('insurance_id')),
                            "emergency_contactno"=>trim($this->post('emergency_contactno')),
                            "emergency_contactname"=>trim($this->post('emergency_contactname')),
                            "signature"=>trim($this->post('signature')),
                            "first_name"=>trim($this->post('fullname')),
                            "personal_doc" =>$doc
                          );

                    $insertId = $this->common->update(TB_USERS,array('id'=>$userid),$insertArr);
                    if($insertId){
                      $users = $this->common_model->select("*",TB_USERS,array("id"=>$userid));
    

$base_url = $this->config->item('base_url');

$key=0;

  $insurance = array(
                         "fullname" => $users[$key]["first_name"],
                         "email" => $users[$key]["email"],
                         "phone" => $users[$key]["phone"],
                         "address" => $users[$key]["address"],
                          "address" => $users[$key]["address"],
                           "date_of_birth" => $users[$key]["date_of_birth"],
                           "zipcode" => $users[$key]["zipcode"],
                           "picture" =>$base_url.$users[$key]["picture"],
                            "zipcode" => $users[$key]["zipcode"],
                           "Username" => $users[$key]["Username"],
                            "mobile" => $users[$key]["mobile"],
                            "emergency_contactno" => $users[$key]["emergency_contactno"],
                           "emergency_contactname" => $users[$key]["emergency_contactname"],
                           "signature" =>$users[$key]["signature"],
                            "insurance_provider" =>$users[$key]["insurance_provider"],
                            "personal_doc" =>$base_url.$users[$key]["personal_doc"],
                            "user_type" =>$users[$key]["user_type"],
                            "insurance_id" =>$users[$key]["insurance_id"],                            
                                  "id" => $users[$key]["id"],
                              "password" => $users[$key]["password"]
                         );








                        $this->response(array("status"=>200,"statusCode"=>true,  "message"=> "Your profile has been updated successfully.","data"=> $insurance), 200); 




                    }  
                   else
                   {

                     $this->response(array("status"=>200, "statusCode"=>false, "message"=> "Some issue in update profile."), 200); 

                   }


        

       }



}


public function editProfileDone_post()
{

    $postData = $this->input->post();
    $dateTime = date("Y-m-d H:i:s");                          
    $userid = $this->post('userid');
    $insuranceprovider = trim($this->post('insuranceprovider'));
    
    if($userid=='')
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please user id required"), 200);
    }
    
    else if($insuranceprovider =='')
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please  insurance  provider required"), 200);
    }
    else
    {
                    $insertArr = array(
                                "done"=>1,                         
                                "insurance_provider" =>$insuranceprovider,
                                "updated_at"=>$dateTime
                              );

                    $insertId = $this->common->update(TB_USERS,array('id'=>$userid),$insertArr);
                    if($insertId){
                      $userlist = $this->common_model->select("*",TB_USERS,array("id"=>$userid));
                        $this->response(array("status"=>200, "statusCode"=>true, "message"=> "Your profile has been updated successfully.","data"=> $userlist), 200); 

                    }    

                    else{
                     $this->response(array("status"=>200, "statusCode"=>false, "message"=> "Some issue in update profile."), 200); 
                    }           

    }

}





public function Userinfo_post()
{
        $postData = $this->input->post(); 
        $userid = trim($this->post('userid')); 
          if($userid=='')
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please user id required"), 200);
    }
    
    else
    {                     
        $users = $this->common_model->select("*",TB_USERS,array('id'=>$userid));            
 $base_url = $this->config->item('base_url'); 
            foreach($users as $key => $value)
            {

        if($users[$key]["user_type"]=='Patient')
          {
            $insurance = array(
                         "fullname" => $users[$key]["first_name"],
                         "email" => $users[$key]["email"],
                         "phone" => $users[$key]["phone"],
                         "address" => $users[$key]["address"],
                          "address" => $users[$key]["address"],
                           "date_of_birth" => $users[$key]["date_of_birth"],
                           "zipcode" => $users[$key]["zipcode"],
                           "picture" =>$base_url.$users[$key]["picture"],
                            "zipcode" => $users[$key]["zipcode"],
                           "Username" => $users[$key]["Username"],
                            "mobile" => $users[$key]["mobile"],
                            "emergency_contactno" => $users[$key]["emergency_contactno"],
                           "emergency_contactname" => $users[$key]["emergency_contactname"],
                           "signature" =>$users[$key]["signature"],
                            "insurance_provider" =>$users[$key]["insurance_provider"],
                            "personal_doc" =>$base_url.$users[$key]["personal_doc"],
                            "user_type" =>$users[$key]["user_type"],
                            "insurance_id" =>$users[$key]["insurance_id"],                            
                                  "id" => $users[$key]["id"],
                              "password" => $users[$key]["password"]
                         );

            }

 if($users[$key]["user_type"]=='Driver')
          {
            $insurance = array(
                         "fullname" => $users[$key]["first_name"],
                         "email" => $users[$key]["email"],
                         "phone" => $users[$key]["phone"],
                         "address" => $users[$key]["address"],
                          "address" => $users[$key]["address"],
                           "date_of_birth" => $users[$key]["date_of_birth"],
                           "zipcode" => $users[$key]["zipcode"],
                           "picture" =>$base_url.$users[$key]["picture"],
                            "zipcode" => $users[$key]["zipcode"],
                           "Username" => $users[$key]["Username"],
                            "mobile" => $users[$key]["mobile"],                          
                            "personal_doc" =>$base_url.$users[$key]["personal_doc"],
                            "user_type" =>$users[$key]["user_type"],                                                    
                            "id" => $users[$key]["id"],
                            "company_name" => $users[$key]["company_name"],
                            "company_address" =>$users[$key]["company_address"],                                                  
                            "company_contactno" => $users[$key]["company_contactno"],
                            "password" => $users[$key]["password"],
                            "vehicle_doc" =>$base_url.$users[$key]["vehicle_doc"],
                            "operate_comy_vehicle" =>$users[$key]["operate_comy_vehicle"],                                 
                            "comy_vehicle_doc" =>$base_url.$users[$key]["comy_vehicle_doc"],
                            "operate_own_vehicle_ownbusiness" => $users[$key]["operate_own_vehicle_ownbusiness"],
                            "operate_own_vehicle_ownbusiness_doc" =>$base_url.$users[$key]["operate_own_vehicle_ownbusiness_doc"],
                            "operate_own_vehicle_comy" => $users[$key]["operate_own_vehicle_comy"],                             
                            "own_vehicle_comy_doc" =>$base_url.$users[$key]["own_vehicle_comy_doc"],
                            "state"=>$users[$key]["state"],
                             "paratrasmit"=>$users[$key]["paratrasmit"],

                            

                         );


            }

            }
      $this->response(array("status"=>200,"statusCode"=>true,"Data"=>$insurance), 200);           
       
    }
}


/* register driver */

public function registerDriver_post()
{

$postData = $_POST;
$reset_token = random_string('unique');
$usertype = trim($this->post('usertype'));
    if(trim($postData["password"])==trim($postData["cpassword"])){

            $insertArr = array(  "user_token" => mt_rand(100000, 999999),
                        "password"     =>md5(trim($postData["password"])),
                        "first_name"   =>trim($postData["fullname"]),                 
                        "Username"   => trim($postData["username"]),  
                        "phone"        =>trim($postData["phone"]),
                        "email"        =>trim($postData["email"]),
                        "picture"   =>trim("images/default.jpg"),
                       "status"=>"inactive",
                       "token"=>$reset_token,
                       "created_at"=>date("Y-m-d H:i:s"),
                       "device_type"=>trim($postData["device_type"]),
                       "done"   =>"0",
                       "user_type"=>$usertype    
                                  );


     $emailexist = $this->common_model->select("email",TB_USERS,array("email"=>trim($this->post('email'))));

            if(count($emailexist)>0) {
            $this->response(array("status"=>"error", "message"=> "User already exist with this email id."), 200);
            }
            else{         
                       $user = $this->common_model->insert(TB_USERS,$insertArr);
                      $userinfo= $this->common_model->select("username,user_type,email,id",TB_USERS,array("email"=>trim($this->post('email')),"email"=>trim($this->post('email')),"Username" =>trim($postData["username"])    ));
                                
           if($user) {          
               $this->response(array("status"=>"success","user"=>$userinfo,"statusCode"=>"true"), 200);  
                 }
            }

    }
    else
    {
    $this->response(array("status"=>"error","statusCode"=>"false", "message"=> "Confirm password not match."), 200);

    }

}





/*   Register drivers */
 public function registerDriver2_post()
{


    $postData = $_POST;
    $reset_token = random_string('unique');
    $usertype = trim($this->post('usertype'));
    $userid = trim($this->post('userid'));
    $operate_comy_vehicle = trim($this->post('operate_comy_vehicle'));
    $operate_own_vehicle_ownbusiness = trim($this->post('operate_own_vehicle_ownbusiness'));
    $operate_own_vehicle_comy = trim($this->post('operate_own_vehicle_comy'));
    $state = trim($this->post('state'));
    $paratrasmit = trim($this->post('paratrasmit'));
    $config['upload_path'] = "./uploads/Driver/doc";
    $config['allowed_types'] = "pdf|doc|docx|PDF|DOC|DOCX";
    $config['max_size'] = '307210';
    $config['max_width'] = '300000';
    $config['max_height'] = '300000';

 
  if($userid =='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>"userid"), 200);
  }

  else if($operate_comy_vehicle=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Do you operate own vehicle for own business?"), 200);
  }

  else if(isset($_FILES["comy_vehicle_doc"]["name"])=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload company vehicle document"), 200);
  }

  else if($operate_own_vehicle_ownbusiness=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Do you operate own vehicle for own business?"), 200);
  }

  else if(isset($_FILES["operate_own_vehicle_ownbusiness_doc"]["name"])=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload  own vehicle document for own business"), 200);
  }

  else if($operate_own_vehicle_comy=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Do you operate own vehicle for company?"), 200);
  }

  else if(isset($_FILES["own_vehicle_comy_doc"]["name"])=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload  own vehicle document for company"), 200);
  }


  else if($state=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>" select state"), 200);
  }

  else if(isset($_FILES["personal_doc"]["name"])=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload  personal document"), 200);
  }


  else if($paratrasmit=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>" select paratrasmit"), 200);
  }
  else
  {
        $comy_vehicle_doc = $_FILES["comy_vehicle_doc"]["name"];       
        $operate_own_vehicle_ownbusiness_doc = $_FILES["operate_own_vehicle_ownbusiness_doc"]["name"];   
        $own_vehicle_comy_doc = $_FILES["own_vehicle_comy_doc"]["name"];   
        $personal_doc = $_FILES["personal_doc"]["name"]; 

        $this->load->library('upload', $config);
        $i=0;
        foreach ($_FILES as $fieldname => $fileObject)  //fieldname is the form field name
        {
        $i=$i+1;

            $original_file_name = $fileObject['name'];
            $file_name_rename =$userid."_".$i.$fileObject['name'];
            $config['file_name'] = $file_name_rename;

 // if(file_exists($file_name_rename))
 // {
 //                $path = './uploads/Driver/doc/'.$file_name_rename;
 //                unlink($path);

 // }
            

            if (!empty($fileObject['name']))
            {

                $this->upload->initialize($config);
                if (!$this->upload->do_upload($fieldname))
                {
                    $errors = $this->upload->display_errors();           
                    $this->response(array("status"=>200,"statusCode"=>false, "message"=>$errors), 200);

                }
                else
                {          

                   $datareset = array($fieldname =>"uploads/Driver/doc/".$file_name_rename,
                    "operate_comy_vehicle"=>$operate_comy_vehicle , "operate_own_vehicle_ownbusiness"=>$operate_own_vehicle_ownbusiness ,"operate_own_vehicle_comy"=>$operate_own_vehicle_comy,"paratrasmit"=>$paratrasmit  ,"state"=>$state  ); 
                      $cond1 = array('id' =>  $userid,'user_type'=>'Driver'); 
                        $updae= $this->common_model->update(TB_USERS,$cond1,$datareset);
                    $userinfo= $this->common_model->select("Username,user_type,email,id",TB_USERS,$cond1);
                }

            }

        }


                                if($updae)
                                {
                                $to_email = $this->post('email');
                                $hostname = $this->config->item('hostname');  
                                $config['mailtype'] ='html';
                                $config['charset'] ='iso-8859-1';
                                $this->email->initialize($config);
                                $from  = EMAIL_FROM; 
                                $this->messageBody  = email_header();
                                $this->messageBody  .= '<tr>
                <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;height:200px;vertical-align:top;">
                    <p>Dear Customer,</p>
                        <p>
                        You are registered successfully on Transcura as a driver.<br>
                        Please wait while administrator activates your account. </p>
                       
                    </td>
                </tr>               
               ';                      
                                $this->messageBody  .= email_footer();        //print_r( $this->messageBody);die;
                                $this->email->from($from, $from);
                                $this->email->to($to_email);
                                $this->email->subject('Welcome to Transcura');
                                $this->email->message($this->messageBody);
                                $this->email->send();
                                $this->response(array("status"=>200,"message"=> "User has been successfully register.","statusCode"=>true), 200);  
                                }
  }


}




/* edit profile driver */



public function editProfileDriver_post()
{

    $postData = $this->input->post();
    $dateTime = date("Y-m-d H:i:s");                          
    $userid = $this->post('userid');
    //$insurance_id = trim($this->post('insurance_id'));
    $signature = trim($this->post('signature')) ;
    if($userid=='')
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please user id required"), 200);
    }
    
   
    // else if(!isset($_FILES["personal_doc"]["name"]))
    // {
    // $this->response(array("status"=>200,"statusCode"=>false, "message"=>"upload personal doc"), 200);
    // }
  
   else
       {

     if(isset($_FILES["personal_doc"]["name"]))
     {   
        $file_name = $_FILES["personal_doc"]["name"];
        $original_file_name = $file_name;
        $random = rand(1, 10000000000000000);
        $makeRandom = $random;
        $file_name_rename = $makeRandom;
        $explode = explode('.', $file_name);
        if(count($explode) >= 2) {
            $new_file = $file_name_rename.'.'.$explode[1];
            //print_r($new_file);die;
            $config['upload_path'] = "./uploads/Driver/doc";
            $config['allowed_types'] = "pdf|doc|docx|PDF|DOC";
            $config['file_name'] = $new_file;
            $config['max_size'] = '307210';
            $config['max_width'] = '300000';
            $config['max_height'] = '300000';
             $this->load->library('upload',$config);
              $this->upload->initialize($config);
            if(!$this->upload->do_upload("personal_doc")) {
                //print_r($this->upload->display_errors());
                $this->response(array("status"=>200,"statusCode"=>false, "message"=>$this->upload->display_errors()), 200);
            } else {
                 $doc = 'uploads/Driver/doc/'.$new_file;   

                }
        } 

        else {
           $this->response(array("status"=>200,"statusCode"=>false, "message"=>"upload valid file"), 200);
        }
    }

       else
      {

       $doc = trim($this->post('personal_docold')) ;;   
      }
 $base_url = $this->config->item('base_url');

                   $insertArr = array(
                            "date_of_birth"=>trim($this->post('date_of_birth')),
                            "email"=>trim($this->post('email')),                                                    
                            "address"=>trim($this->post('address')),
                            "zipcode"=>trim($this->post('zipcode')),
                            "phone"=>trim($this->post('phone')),                            
                            "company_name"=>trim($this->post('company_name')),
                            "company_address"=>trim($this->post('company_address')),
                            "company_contactno"=>trim($this->post('company_contactno')),
                            "first_name"=>trim($this->post('fullname')),
                            "personal_doc" =>$doc,
                            "done"=>1
                          );

                    $insertId = $this->common->update(TB_USERS,array('id'=>$userid),$insertArr);
                   // echo $this->db->last_query();die;
                    if($insertId){
                      $userlist = $this->common_model->select("*",TB_USERS,array("id"=>$userid));

                     $key=0;
                           $insurance = array(
                         "fullname" => $userlist[$key]["first_name"],
                         "email" => $userlist[$key]["email"],
                         "phone" => $userlist[$key]["phone"],
                         "address" => $userlist[$key]["address"],
                          "address" => $userlist[$key]["address"],
                           "date_of_birth" => $userlist[$key]["date_of_birth"],
                           "zipcode" => $userlist[$key]["zipcode"],
                           "picture" =>$base_url.$userlist[$key]["picture"],
                            "zipcode" => $userlist[$key]["zipcode"],
                           "Username" => $userlist[$key]["Username"],
                            "mobile" => $userlist[$key]["mobile"],                          
                            "personal_doc" =>$base_url.$userlist[$key]["personal_doc"],
                            "user_type" =>$userlist[$key]["user_type"],                                                    
                            "id" => $userlist[$key]["id"],
                            "company_name" => $userlist[$key]["company_name"],
                            "company_address" =>$userlist[$key]["company_address"],                                                  
                            "company_contactno" => $userlist[$key]["company_contactno"],
                            "password" => $userlist[$key]["password"],
                            "vehicle_doc" =>$base_url.$userlist[$key]["vehicle_doc"],
                            "operate_comy_vehicle" =>$userlist[$key]["operate_comy_vehicle"],                                 
                            "comy_vehicle_doc" =>$base_url.$userlist[$key]["comy_vehicle_doc"],
                            "operate_own_vehicle_ownbusiness" => $userlist[$key]["operate_own_vehicle_ownbusiness"],
                            "operate_own_vehicle_ownbusiness_doc" =>$base_url.$userlist[$key]["operate_own_vehicle_ownbusiness_doc"],
                            "operate_own_vehicle_comy" => $userlist[$key]["operate_own_vehicle_comy"],   
                            "own_vehicle_comy_doc" =>$base_url.$userlist[$key]["own_vehicle_comy_doc"],
                         
                              "state"=>$userlist[$key]["state"],
                             "paratrasmit"=>$userlist[$key]["paratrasmit"],
                            

                         );


                      $this->response(array("status"=>200, "statusCode"=>true,  "message"=> "Your profile has been updated successfully.","data"=> $insurance), 200); 


                    }  
                   else
                   {

                     $this->response(array("status"=>200, "statusCode"=>false, "message"=> "Some issue in update profile."), 200); 

                   }


        

       }

}





public function ServiceTypeList_get()
{
        $postData = $this->input->post();                       
        $servicelist = $this->common_model->select("type_title,type_id,image",TB_SERVICE_TYPE);
        if( count($servicelist) >0)
        {          

            foreach($servicelist as $key => $value)
            {
            $insurance[] = array("service" => $servicelist[$key]["type_title"],
                "service_id" => $servicelist[$key]["type_id"],
                 "image" => $servicelist[$key]["image"],

                );            
            $this->response(array("status"=>200,"statusCode"=>true,"service_info"=>$insurance), 200); 

            }
               
        }
      else
        {
        $this->response(array("status"=>200,"statusCode"=>false, "message"=> "Data not found"), 200); 
        } 

}






// /*   edit drivers */
//  public function EditProfileDriver2_post()
// {
//     $postData = $_POST;
//     $reset_token = random_string('unique');
//     $usertype = trim($this->post('usertype'));
//     $userid = trim($this->post('userid'));
//     $operate_comy_vehicle = trim($this->post('operate_comy_vehicle'));
//     $operate_own_vehicle_ownbusiness = trim($this->post('operate_own_vehicle_ownbusiness'));
//     $operate_own_vehicle_comy = trim($this->post('operate_own_vehicle_comy'));
//     $state = trim($this->post('state'));
//     $paratrasmit = trim($this->post('paratrasmit'));
//     $config['upload_path'] = "./uploads/Driver/doc";
//     $config['allowed_types'] = "pdf|doc|docx|PDF|DOC|DOCX";
//     $config['max_size'] = '307210';
//     $config['max_width'] = '300000';
//     $config['max_height'] = '300000';

 
//   if($userid =='')
//   {
//   $this->response(array("status"=>200,"statusCode"=>false, "message"=>"userid"), 200);
//   }

//   else if($operate_comy_vehicle=='')
//   {
//   $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Do you operate own vehicle for own business?"), 200);
//   }

//   else if(isset($_FILES["comy_vehicle_doc"]["name"])=='')
//   {
//   $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload company vehicle document"), 200);
//   }

//   else if($operate_own_vehicle_ownbusiness=='')
//   {
//   $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Do you operate own vehicle for own business?"), 200);
//   }

//   else if(isset($_FILES["operate_own_vehicle_ownbusiness_doc"]["name"])=='')
//   {
//   $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload  own vehicle document for own business"), 200);
//   }

//   else if($operate_own_vehicle_comy=='')
//   {
//   $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Do you operate own vehicle for company?"), 200);
//   }

//   else if(isset($_FILES["own_vehicle_comy_doc"]["name"])=='')
//   {
//   $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload  own vehicle document for company"), 200);
//   }


//   else if($state=='')
//   {
//   $this->response(array("status"=>200,"statusCode"=>false, "message"=>" select state"), 200);
//   }

//   else if(isset($_FILES["personal_doc"]["name"])=='')
//   {
//   $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload  personal document"), 200);
//   }


//   else if($paratrasmit=='')
//   {
//   $this->response(array("status"=>200,"statusCode"=>false, "message"=>" select paratrasmit"), 200);
//   }
//   else
//   {
//         $comy_vehicle_doc = $_FILES["comy_vehicle_doc"]["name"];       
//         $operate_own_vehicle_ownbusiness_doc = $_FILES["operate_own_vehicle_ownbusiness_doc"]["name"];   
//         $own_vehicle_comy_doc = $_FILES["own_vehicle_comy_doc"]["name"];   
//         $personal_doc = $_FILES["personal_doc"]["name"]; 

//         $this->load->library('upload', $config);
//         $i=0;
//         foreach ($_FILES as $fieldname => $fileObject)  //fieldname is the form field name
//         {
//         $i=$i+1;

//             $original_file_name = $fileObject['name'];
//             $file_name_rename =$userid."_".$i.$fileObject['name'];
//             $config['file_name'] = $file_name_rename;

//               //  $path = './uploads/Driver/doc/'.$file_name_rename;
//              //   unlink($path);
         

//             if (!empty($fileObject['name']))
//             {

//                 $this->upload->initialize($config);
//                 if (!$this->upload->do_upload($fieldname))
//                 {
//                     $errors = $this->upload->display_errors();           
//                     $this->response(array("status"=>200,"statusCode"=>false, "message"=>$errors), 200);

//                 }
//                 else
//                 {          

//                     $datareset = array($fieldname =>  $file_name_rename,"operate_comy_vehicle"=>$operate_comy_vehicle , "operate_own_vehicle_ownbusiness"=>$operate_own_vehicle_ownbusiness ,"operate_own_vehicle_comy"=>$operate_own_vehicle_comy,"paratrasmit"=>$paratrasmit  ,"state"=>$state  ); 
//                       $cond1 = array('id' =>  $userid,'user_type'=>'Driver'); 
//                         $updae= $this->common_model->update(TB_USERS,$cond1,$datareset);
//                      // Code After Files Upload Success GOES HERE
//                 }

//             }

//         }


//                                 if($updae)
//                                 {
//                                $this->response(array("status"=>200,"message"=> "User has been successfully updated.","statusCode"=>true), 200);  
//                                 }
//   }


// }





/*   edit drivers */
 public function EditProfileDriver2_post()
{
    $postData = $_POST;
    $reset_token = random_string('unique');
    $usertype = trim($this->post('usertype'));
    $userid = trim($this->post('userid'));
    $operate_comy_vehicle = trim($this->post('operate_comy_vehicle'));
    $operate_own_vehicle_ownbusiness = trim($this->post('operate_own_vehicle_ownbusiness'));
    $operate_own_vehicle_comy = trim($this->post('operate_own_vehicle_comy'));
    $state = trim($this->post('state'));
    $paratrasmit = trim($this->post('paratrasmit'));
    
 $base_url = $this->config->item('base_url');
 
  if($userid =='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>"userid"), 200);
  }

  else if($operate_comy_vehicle=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Do you operate own vehicle for own business?"), 200);
  }

  // else if(isset($_FILES["comy_vehicle_doc"]["name"])=='')
  // {
  // $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload company vehicle document"), 200);
  // }

  else if($operate_own_vehicle_ownbusiness=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Do you operate own vehicle for own business?"), 200);
  }

  // else if(isset($_FILES["operate_own_vehicle_ownbusiness_doc"]["name"])=='')
  // {
  // $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload  own vehicle document for own business"), 200);
  // }

  else if($operate_own_vehicle_comy=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Do you operate own vehicle for company?"), 200);
  }

  // else if(isset($_FILES["own_vehicle_comy_doc"]["name"])=='')
  // {
  // $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload  own vehicle document for company"), 200);
  // }


  else if($state=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>" select state"), 200);
  }

  // else if(isset($_FILES["personal_doc"]["name"])=='')
  // {
  // $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload  personal document"), 200);
  // }


  else if($paratrasmit=='')
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=>" select paratrasmit"), 200);
  }
  else
  {



    if(isset($_FILES["comy_vehicle_doc"]["name"]))
    {
    $comy_vehicle_doc = $_FILES["comy_vehicle_doc"]["name"];          
    }
    else
    {
    $comy_vehicle_doc = trim($this->post('comy_vehicle_docold'));   
    }

    if(isset($_FILES["operate_own_vehicle_ownbusiness_doc"]["name"]))
    {
    $operate_own_vehicle_ownbusiness_doc = $_FILES["operate_own_vehicle_ownbusiness_doc"]["name"];       
    }
    else
    {
    $operate_own_vehicle_ownbusiness_doc = trim($this->post('operate_own_vehicle_ownbusiness_docold'));   
    }

        if(isset($_FILES["own_vehicle_comy_doc"]["name"]))
    {
    $own_vehicle_comy_doc = $_FILES["own_vehicle_comy_doc"]["name"];       
    }
    else
    {
    $own_vehicle_comy_doc = trim($this->post('own_vehicle_comy_docold'));   
    }
             
           
    //    $own_vehicle_comy_doc = $_FILES["own_vehicle_comy_doc"]["name"];   
      //  $personal_doc = $_FILES["personal_doc"]["name"]; 

       
        $i=0;

  if($_FILES){

        foreach ($_FILES as $fieldname => $fileObject)  //fieldname is the form field name
        {
        $i=$i+1;

            $original_file_name = $fileObject['name'];

        $random = rand(1, 10000000000000000);
        $makeRandom = $random;

        $file_name_rename ='uploads/Driver/doc/'.$userid."_".$makeRandom.$fileObject['name'];
        $config['file_name'] = $file_name_rename;       

        $path = './uploads/Driver/doc/'.$file_name_rename;
        $config['upload_path'] = "./uploads/Driver/doc";
        $config['allowed_types'] = "pdf|doc|docx|PDF|DOC|DOCX";
        $config['max_size'] = '307210';
        $config['max_width'] = '300000';
        $config['max_height'] = '300000';

        $this->load->library('upload', $config);


            if (!empty($fileObject['name']))
            {

                $this->upload->initialize($config);
                if (!$this->upload->do_upload($fieldname))
                {
                    $errors = $this->upload->display_errors();           
                    $this->response(array("status"=>200,"statusCode"=>false, "message"=>$errors), 200);

                }
                else
                {          

                    $datareset = array($fieldname =>  $file_name_rename,"operate_comy_vehicle"=>$operate_comy_vehicle , "operate_own_vehicle_ownbusiness"=>$operate_own_vehicle_ownbusiness ,"operate_own_vehicle_comy"=>$operate_own_vehicle_comy,"paratrasmit"=>$paratrasmit  ,"state"=>$state  ); 
                      $cond1 = array('id' =>  $userid); 
                        $updae= $this->common_model->update(TB_USERS,$cond1,$datareset);

                       // echo $this->db->last_query();die;
                     // Code After Files Upload Success GOES HERE
                }

            }

        }

}

else
{

$datareset = array("comy_vehicle_doc" =>  $comy_vehicle_doc,
"operate_own_vehicle_ownbusiness_doc" =>  $operate_own_vehicle_ownbusiness_doc,
"own_vehicle_comy_doc" =>  $own_vehicle_comy_doc,
 "operate_comy_vehicle"=>$operate_comy_vehicle ,
 "operate_own_vehicle_ownbusiness"=>$operate_own_vehicle_ownbusiness ,
 "operate_own_vehicle_comy"=>$operate_own_vehicle_comy,
 "paratrasmit"=>$paratrasmit,
 "state"=>$state 
  ); 

$cond1 = array('id' =>$userid); 
$updae= $this->common_model->update(TB_USERS,$cond1,$datareset);
//echo $this->db->last_query();die;

}




  if($updae)
  {


 $userlist = $this->common_model->select("*",TB_USERS,array("id"=>$userid));

                     $key=0;
                           $insurance = array(
                         "fullname" => $userlist[$key]["first_name"],
                         "email" => $userlist[$key]["email"],
                         "phone" => $userlist[$key]["phone"],
                         "address" => $userlist[$key]["address"],
                          "address" => $userlist[$key]["address"],
                           "date_of_birth" => $userlist[$key]["date_of_birth"],
                           "zipcode" => $userlist[$key]["zipcode"],
                           "picture" =>$base_url.$base_url.$userlist[$key]["picture"],
                            "zipcode" => $userlist[$key]["zipcode"],
                           "Username" => $userlist[$key]["Username"],
                            "mobile" => $userlist[$key]["mobile"],                          
                            "personal_doc" =>$base_url.$userlist[$key]["personal_doc"],
                            "user_type" =>$userlist[$key]["user_type"],                                                    
                            "id" => $userlist[$key]["id"],
                            "company_name" => $userlist[$key]["company_name"],
                            "company_address" =>$userlist[$key]["company_address"],                                                  
                            "company_contactno" => $userlist[$key]["company_contactno"],
                            "password" => $userlist[$key]["password"],
                            "vehicle_doc" =>$base_url.$userlist[$key]["vehicle_doc"],
                            "operate_comy_vehicle" =>$userlist[$key]["operate_comy_vehicle"],                                 
                            "comy_vehicle_doc" =>$base_url.$userlist[$key]["comy_vehicle_doc"],
                            "operate_own_vehicle_ownbusiness" => $userlist[$key]["operate_own_vehicle_ownbusiness"],
                            "operate_own_vehicle_ownbusiness_doc" =>$base_url.$userlist[$key]["operate_own_vehicle_ownbusiness_doc"],
                            "operate_own_vehicle_comy" => $userlist[$key]["operate_own_vehicle_comy"],   
                            "own_vehicle_comy_doc" =>$base_url.$userlist[$key]["own_vehicle_comy_doc"]
                            ,                                                  
                            "state" => $userlist[$key]["state"],
                            "paratrasmit" => $userlist[$key]["paratrasmit"],
                         

                            

                         );


  $this->response(array("status"=>200, "userinfo"=> $insurance ,"message"=> "User has been successfully updated.","statusCode"=>true), 200);  
  }





}


}



public function resetPassword_post()
{

    $postData = $this->input->post();
    $dateTime = date("Y-m-d H:i:s");                          
    $userid = $this->post('userid');
    $oldpassword = trim($this->post('oldpassword'));
     $newpassword = trim($this->post('newpassword'));
     $cpassword = trim($this->post('cpassword'));
   

 $result = $this->common_model->select("*",TB_USERS,array("id"=>trim($this->post('userid'))));
//print_r($result[0]['password']);die; 

   



    if($userid=='')
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please user id required"), 200);
    }
    
    else if($oldpassword =='')
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please type old password"), 200);
    }
    
    else if(md5($oldpassword)!= trim($result[0]['password']))
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"wrong old password "), 200);
    }
    

     else if($newpassword =='')
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please type new password"), 200);
    }

    else if($cpassword =='')
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please type Confirm password"), 200);
    }

    else if(trim($cpassword) !== trim($newpassword))
    {

    $this->response(array("status"=>200,"statusCode"=>false, "message"=>"confirm password not match with new password"), 200);
    }

    else
    {


                    $insertArr = array(
                                                      
                                "password" =>md5($newpassword),
                                "updated_at"=>$dateTime
                              );

                   $insertId = $this->common->update(TB_USERS,array('id'=>$userid),$insertArr);
                    if($insertId){
                      
                        $this->response(array("status"=>200, "statusCode"=>true, "message"=> "Your password has been changed successfully."), 200); 

                        }    

                    else{
                     $this->response(array("status"=>200, "statusCode"=>false, "message"=> "Some issue in change password."), 200); 
                    }           

    }

}

public function logout_get() {
// $this->session->sess_destroy();       
$this->session->unset_userdata('auth_user'); 
$message = ['message' => 'User has been logout successfully'];
$this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
}








}
