<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url','html'));
		$this->load->database();
		$this->load->library("email");
		$this->load->model("common_model"); 
		$this->load->library("User_AuthSocial");	
		$this->load->library("email");
        $this->load->library("User_AuthSocial");
        $this->load->helper("email_template");
        $this->load->helper('string');
        $this->load->library("image_lib");
		//$this->load->model('user_model');
	}

	public function index()
	{
        $data['title']  = 'Dashboard';
		$this->load->view('frontend/incls/header',$data); 
		$this->load->view('frontend/home');
		$this->load->view('frontend/incls/footer');
	}

	public function home()
	{
		$this->load->view('frontend/agency/agency_Profile');
	}

	public function agency()
	{
		$this->load->view('frontend/agency/includes/agency_header');
		$this->load->view('frontend/agency/main');
		$this->load->view('frontend/agency/includes/agency_footer');
	}

public function properties_details()
	{
		$this->load->view('frontend/incls/header'); 
		$this->load->view('frontend/property_details');
		$this->load->view('frontend/incls/footer');
	}


	
	public function staff()
	{
		
		$this->load->view('frontend/agency/staff_add');
		
	}
	public function news()
	{
		$this->load->view('frontend/agency/news_add');
	}
	
	public function change_user_password()
	{
		
		$this->load->view('frontend/user/change_password');
		
	}


	public function privacypolicy()
	{
    $data['title']  = 'Privacy Policy';
    $this->load->view('frontend/agency/includes/agency_header',$data);
	$this->load->view('frontend/privacypolicy');
	$this->load->view('frontend/agency/includes/agency_footer');
	}

	public function termsandconditions()
	{
		$data['title']  = 'Terms and Conditions';
        
	$this->load->view('frontend/agency/includes/agency_header',$data);
		$this->load->view('frontend/termsandconditions');
		$this->load->view('frontend/agency/includes/agency_footer');

	}

	public function staff_profile()
	{
		$this->load->view('frontend/agency/staff_Profile');
	}

	public function property_list()
	{
		$this->load->view('frontend/property_list');
	}

	public function checkLogin(){
		if(is_ajax_request())
		{
		   // print_r(is_ajax_request());die;
			$postData = $this->input->post();
            //print_r($postData);die;
			if (strpos(trim($postData["username"]), '@') !== false) {
			$result = $this->common->select("*",TB_USERS,array("email"=>trim($postData["username"]),"password"=>md5(trim($postData["password"]))));
		    }
			else
			{
			$result = $this->common->select("*",TB_USERS,array("Username"=>trim($postData["username"]),"password"=>md5(trim($postData["password"]))));

			}
			if($result)
			{
			    if($result[0]['status']=='active'){
			      $this->session->set_userdata("auth_user", $result[0]);
				  echo $data =json_encode(array("status" => "success")); exit;	
			    }
				else
			    {
			     echo $data =json_encode(array("status" => "error","message"=>"User Not activate.")); exit;
			  	}
			}
			else
			{
				echo $data = json_encode(array("status" => "error","message"=>"Please provide valid login credential.")); exit;
			}
		}
	}

  
    public function signup(){

            $data['title']  = 'Sign Up';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$data['main_content'] = 'frontend/sign-up';
			$this->load->view("frontend/incls/layout",$data);		
		    //$this->load->view('frontend/sign-up');
	}

    public function register_user(){
    	//print_r($_REQUEST);die;
         $postData = $this->input->post();
        
         if($postData['username']=="" ||  $postData['email']=="" || $postData['password']==""  ||  $postData['confirm_password']=="")
          {
           echo $data = json_encode(array("status" => "error","message"=>"All fields are required."));
          }
          if($this->validate_email($_REQUEST['email']) == false) {
              
          echo $data = json_encode(array("status" => "error","message"=>"Email already exits."));exit;
        
        }
        else {
        	
            $combinationString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789*#&$^";
			$this->load->helper('string');
            $token =  random_string('alnum',20);
            $this->load->model('signup_model');				
            $login_data = array();
            $login_data["username"] = $_REQUEST['username'];
            $login_data["email"] = $_REQUEST['email'];
            $login_data["password"] = $_REQUEST['password'];
            $login_data["user_token"] = $token;
            $login_data["status"] = "inactive"; 
            $login_data["token"] = $token;                     

            if($login_data["password"]!=$_REQUEST["confirm_password"])
			{
				echo $data = json_encode(array("status" => "error","message"=>"Not match password.")); 
			}
			 $login_data["password"]=md5($login_data["password"]);
            $login_data["created_at"] = date("Y-m-d h:i:s", time());
            $insertId = $this->common->insert(TB_USERS,$login_data);         
            if($insertId){
          
				$mail = $login_data["email"]; 


				 $message = file_get_contents("media/emails/email_confirmation.html");


				 $message = preg_replace('/{user}/', $login_data["username"], $message);
				 $message = preg_replace('/{user_id}/', $insertId, $message);
				 $message = preg_replace('/{salt}/', $login_data["user_token"], $message);
				

	$from_email = "tanuja.b@example.com"; 
	$to_email = $mail;

	//Load email library 
	$this->load->library('email');    
    $config['protocol'] = 'sendmail';
    $config['mailpath'] = '/usr/sbin/sendmail';
	$config['charset'] = 'iso-8859-1';
	$config['wordwrap'] = TRUE;
	$config['mailtype'] = 'html';

    $this->email->initialize($config);
         $this->email->from($from_email, 'tanuja'); 
         $this->email->to($to_email);
         $this->email->subject('User registration mail'); 
         $this->email->message($message); 

        // $this->email->Body($message,"html");
         $this->email->send();
           //Send mail 
         /*if($this->email->send()) 
         $this->session->set_flashdata("email_sent","Email sent successfully."); 
         else 
         $this->session->set_flashdata("email_sent","Error in sending Email."); 
         $this->load->view('email_form'); 
*/
		     echo  $data =  json_encode(array("status"=>"sucess","action"=>"save","message"=>"Successfully Added."));exit;

			}else{
			echo  $data =  json_encode(array("status"=>"error","message"=>"Please try again."));exit;
			//redirect("signup"); exit;
			}
           // $this->session->set_userdata("auth_user", $udata);            
        }             
      
    
     
		
   }
   

// Check email is exist
  public function validate_email($email){

		$this->load->model('signup_model');
		$email=(array("email"=>$email));
		if($this->signup_model->check_email_exist(TB_USERS,"email",$email)){	
		//echo $this->db->last_query();die;	
		   return false;
		}else{
		   return true;
		}
   }

    public function forgot_password(){
            $data['title']  = 'forgot password';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$data['main_content'] = 'frontend/forgot_password';
			$this->load->view("frontend/incls/layout",$data);		
		    //$this->load->view('frontend/forgot_password');
	}

public function accountverify($abc)
{
	   
		$userData = $this->common->select("*",TB_USERS,array("token"=>trim($abc)));  

		if ($userData) {

		$cond1 = array('id' =>  $userData[0]['id']); 
		$datareset = array('status' =>  "active",'token'=> ""); 
		$sucess= $this->common->update(TB_USERS,$cond1,$datareset);		
		$this->session->set_flashdata('Your account successfully activate now', 'msg');
	    $data= $this->session->set_flashdata('msg', 'Your account successfully activate now');		
		$this->emailverify($data); 


            $hostname = $this->config->item('hostname');
            $config['mailtype'] ='html';
            $config['charset'] ='iso-8859-1';
            $this->email->initialize($config);
            $from="MHIQ@gmail.com";
            $to  = "tanuja.b@exceptionaire.co";
            $this->messageBody  = email_header();
            $this->messageBody  .= '<tr>
                <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                    <p>Dear Admin,</p>
                        <p>
                        <p>'.$userData[0]['first_name'].' has actvated now.</p>
                        
                        </p>
                        <p>
                        If the link is not working properly, then copy and paste the link in your 
                        browser. <br>If you did not send this request, please ignore this email.</p>
                    </td>
                </tr>
                <tr>';                      
            $this->messageBody  .= email_footer();
            $this->email->from($from);
            $this->email->to( $to);
            $this->email->subject('Reset Password Link');
            $this->email->message($this->messageBody);
            $this->email->send();






		}

		else{
		
		$data= $this->session->set_flashdata('error', 'expire activation links');	
		// $data = json_encode(array("status" => "sucess","message"=>"You have successfully reset "));
		$this->emailverify($data);  
		
	      }
	


}




public function accountverify($abc)
{
	   
		$userData = $this->common->select("*",TB_USERS,array("token"=>trim($abc)));  
	
		if ($userData) {

		$cond1 = array('id' =>  $userData[0]['id']); 
		$datareset = array('status' =>  "active",'token'=> ""); 
		$sucess= $this->common->update(TB_USERS,$cond1,$datareset);		
		$this->session->set_flashdata('Your account successfully activate now', 'msg');
	    $data= $this->session->set_flashdata('forgot_success_msg', 'Your account successfully activate now');		redirect("frontend/login",$data);    

	      }

		else{
		
		$data = $this->session->set_flashdata('error', 'expire activation links');	
		redirect("frontend/login",$data); 		
		
	       }
	


}









public function emailverify($data)
	{
		

		$this->load->view('frontend/agency/includes/agency_header');
		$this->load->view('frontend/agency/verify',$data);
		$this->load->view('frontend/agency/includes/agency_footer');	
	}






    public function forgotpassword(){        

            $postData = $this->input->post();
        
            
			if ($postData["uemail"]) {
			   $userData = $this->common->select("*",TB_USERS,array("email"=>trim($postData["uemail"]),"status"=>"active"));
		       // print_r($userData[0]['email']);die;          
		    if($userData)
			{
                       
			$useremail =trim($postData["uemail"]) ;
			$reset_key  = md5(uniqid(mt_rand(), true));
            $datareset = array('token' =>  $reset_key); 
            $cond1 = array('email' =>  $useremail); 
            $this->common->update(TB_USERS,$cond1,$datareset);
                        
						
						if($userData['Username']){
						$name = $userData['Username'];
						}else{
						$name = $userData['first_name']." ". $userData['last_name'];
						}
                                                $this->load->library('email');    
						$config['protocol'] = 'sendmail';
						$config['mailpath'] = '/usr/sbin/sendmail';
						$config['charset'] = 'iso-8859-1';
						$config['wordwrap'] = TRUE;
						$config['mailtype'] = 'html';
					        $this->email->initialize($config);
						$message = file_get_contents("media/emails/forgot_password.html");
						$message = preg_replace('/{user}/', $name, $message);
						$message = preg_replace('/{user_id}/', $userData['id'], $message);
						$message = preg_replace('/{key}/', $reset_key, $message);
						$page = base_url()."home/reset_password";

						$message = preg_replace('/{reset_page}/', $page, $message);
//print_r($message);die;
						//Send email actions;						
						 $from_email = "tanuja.b@example.com"; 
                                               $this->email->initialize($config);
        
						 $to_email =$userData['email'];						
						 $this->email->from($from_email, 'tanuja'); 
						 $this->email->to($useremail);
						 $this->email->subject('MHIQ - Reset password mail'); 
						 $this->email->message($message); 
						// $this->email->Body($message,"html");
						 $this->email->send();

			 echo json_encode(array("status" => "sucess","message"=>"Password reset code has been sent on your email")); 
			 redirect("home/reset_password/?key=".$reset_key);  exit;  
			}
			else{
			echo json_encode(array("status" => "error","message"=>"user not activate")); 
			exit;
			}
		}
		else
		{
			echo json_encode(array("status" => "error","message"=>"Please provide valid email.")); exit;
		}
}





 public function  reset_password($key){
//print_r($key);die;

 	                $data['title']  = 'Reset password';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
                    $data['key']=$key;
			$data['main_content'] = 'frontend/reset_password';
			$this->load->view("frontend/incls/layout",$data);		
		    //$this->load->view('frontend/forgot_password');


 }
    
function resetUserPassword($id){

	
		       if(!isset($_REQUEST['key'])){
			     redirect('home');
		       }
                        $postData = $this->input->post();
//print_r( $postData );die;

		    $cond1 = array('email' =>  trim($postData['uemail'])); 
		    $userData = $this->common->select("*",TB_USERS,$cond1 );  
		//print_r( count($userData));die;
                        if($postData['uemail']=="" )
			{

			echo json_encode(array("status" => "error","message"=>"please Enter valid email"));  exit;

			}
                        else if(count($userData)==0)
			{
			echo json_encode(array("status" => "error","message"=>"email id not found"));  exit;
			                       
			}
                        else if($postData['password']=="" )
			{

			echo json_encode(array("status" => "error","message"=>"Please Enter valid password"));  exit;

			}
                       else if($postData['confpwd']=="" )
			{

			echo json_encode(array("status" => "error","message"=>"Please Enter valid confirm password"));  exit;

			}
			else if($postData['password']!==  $postData['confpwd'])
			{

			echo json_encode(array("status" => "error","message"=>"Password and confirm password not match"));  exit;

			}
          
                        if($postData['key']=="" )
			{

			echo json_encode(array("status" => "error","message"=>"Expire token for reset password"));  exit;

			}

                     //  $data['messagereset'] = $this->session->flashdata('messagereset'); 
			$data['title']  = 'Reset Password';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$cond1 = array('email' =>  trim($postData['uemail']) ,'token'=> trim($postData['key']) ,"status"=>"active"); 
                        $userData = $this->common->select("*",TB_USERS,$cond1 );
			// echo $this->db->last_query();die;    
			//   print_r($userData) ;die; 
			if($userData){
		
				$key  =$postData['key']; 
				$useremail    = trim($this->input->post('uemail'));
				$password = md5($this->input->post('password'));
				$datareset = array('password' => $password);

				$cond = array('email' => $useremail, 'token' => trim($postData['key']));				
				$userData = $this->common->select("*",TB_USERS,$cond);
		     
		       		  
				if(count($userData) > 0)
				{	  
					$this->common->update(TB_USERS,$cond,$datareset);	
				
					$cond = array('email' => $useremail,'password' => $password);			       
					 $res = $this->common->select("email,password",TB_USERS,$cond);  
 
				if ( $res !== false ) {
						
						 $cond1 = array('email' => $useremail);
						 $datareset = array('token' => '');
	                               $this->common->update(TB_USERS,$cond,$datareset); 		
				//echo $this->db->last_query();die; 	
				
                   echo json_encode(array("status" => "sucess","message"=>"You have successfully reset the password."));  $this->load->view('frontend/reset_password', $data);exit;
								
				} 

				else{ 
	
				$data['validation_message'] = "The key is invalid or You have already reset the password.";
echo json_encode(array("status" => "error","message"=>"The key is invalid or You have already reset the password.","path"=>"home"));  
				
				} 

				
            }
            else{
              //  $data['validation_message'] = (validation_errors() ? validation_errors() : $this->session->set_flashdata('message'));
                $this->load->view('frontend/reset_password', $data);
            }
        }
        
    }
    
   public function facebook_login()
	{
	   $data["user_data"] =	$this->user_authsocial->facebook();
	  //$this->session->set_userdata("auth_user", $data["user_data"]);
	   $cond_key = array("email"=>trim($data["user_data"]['email']));
           $userData = $this->common->select("*",TB_USERS, $cond_key);
           $this->session->set_userdata("auth_user", $userData[0]);	 
	   redirect("home");
	}
	public function google_login()
	{

		$data["user_data"] =	$this->user_authsocial->google();		
		//$this->session->set_userdata("auth_user", $userData[0]);
		$cond_key = array("email"=>trim($data["user_data"]['email']));
		$userData = $this->common->select("*",TB_USERS, $cond_key);
		$this->session->set_userdata("auth_user", $userData[0]);	 
		//print_r($_SESSION);die;
		redirect("home");
	}
    
    

    public function logout(){
       $this->session->sess_destroy();
        redirect("home"); exit;
    }





}
