<?php
class Common_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
    
    function insert($table,$data)
    {
		$this->db->protect_identifiers=true;
        $this->db->insert($table,$data);
      //  echo $this->db->last_query();die;
        return $this->db->insert_id();
        
    }
    
    function insert_batch($table,$data)
    {	
		$this->db->protect_identifiers=true;
        $this->db->insert_batch($table,$data);
		//return $this->db->insert_id();
    }
    
    function update($table,$where=array(),$data)
    {
        $this->db->update($table,$data,$where);
        //echo $this->db->last_query();
        return $this->db->affected_rows();
    }
    
    function updateIncrement($table,$where=array(),$data)
    {
        foreach ($data AS $k => $v)
		{
			$this->db->set($k, $v, FALSE);
		}
        foreach ($where AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		$this->db->update($table);
		return $this->db->affected_rows();
    }
    
    function update_batch($table,$where,$data)
    {
		$this->db->protect_identifiers=true;
        $this->db->update_batch($table,$data,$where);
       // echo $this->db->last_query();
        return $this->db->affected_rows();
    }
    function delete($table,$where=array())
    {
        $this->db->delete($table,$where);
        return $this->db->affected_rows();
    }
    
    function delete_batch($table,$key,$ids=array())
    {	
		$this->db->where_in($key,$ids);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }
    
    function select($sel,$table,$cond = array(),$cond_or=array(), $join = array())
	{
		$this->db->select($sel, FALSE);
		$this->db->from($table);
                //$this->db->join('tbl_privilege', ''.$table.'.privilage_id=tbl_privilege.privilege_id', 'left');
                
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach ($cond_or AS $k => $v)
		{
			$this->db->or_where($k,$v);
		}
        foreach ($join AS $k => $v)
		{
			$this->db->join($k,$v,'left');
		}
		$query = $this->db->get();
                      
		return $query->result_array();
	}
	
	function selectQuery($sel,$table,$cond = array(),$orderBy=array())
	{
		$this->db->select($sel, FALSE);
		$this->db->from($table);
		foreach ($cond AS $k => $v)
		{
			$this->db->where($k,$v);
		}
		foreach($orderBy as $key => $val)
		{
			$this->db->order_by($key, $val);
		}
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function isUserExist($table,$where = array())
    {
		$this->db->select('user_id', FALSE);
		$this->db->from($table);
		foreach($where as $key => $val)
		{
			$this->db->where($key, $val);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
    public function getMaster($tablename, $where = FALSE, $join = FALSE, $order = false, $field = false, $select = false,$limit=false,$start=false, $search=false)
    {

		if($limit){
			$this->db->limit($limit, $start);
		}
		if ($search) {
                $where = $this->searchString($search);
                $query = $this->db->get_where($tablename, $where);
        } 
        if ($where) {
            $this->db->where($where, NULL, FALSE);
        }
        if ($order && $field) {
            $this->db->order_by($field, $order);
        }
        if ($join) {
            if (count($join) > 0) {
                foreach ($join as $key => $value) {
                    $explode = explode('|', $value);
                    $this->db->join($key, $explode[0], $explode[1]);
                }
            }
        }
        if ($select) {
            $this->db->select($select, FALSE);
        } else {
            $this->db->select('*');
        }
		$query = $this->db->get($tablename);
        //echo $this->db->last_query();die;
        return $query->result_array();
    }
    
}
?>
