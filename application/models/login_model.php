<?php
class Login_Model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
		$this->load->database();        
    }
	
	public function login_user($email,$pass,$usertype)
    { 
        $this->db->select('*');
        $this->db->from('tbl_users');
        $this->db->where('email',$email);
        $this->db->where('password',$pass);
        $this->db->where('user_type',$usertype);

        if($query=$this->db->get())
        {
          return $query->row_array();
        }
        else{
        return false;
        }
    }
    
    function validUser($table,$fields,$where=array(),$join=array())
    {
		$this->db->select($fields, FALSE);
		$this->db->from($table);
		foreach($where as $key => $val)
		{
			$this->db->where($key, $val);
		}
		foreach($join as $key => $val)
		{
			$this->db->join($key, $val);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		return $query->result_array();
    }
     
    function checkUserStatus($table,$fields,$where=array(),$join=array())
    {
		$this->db->select($fields, FALSE);
		$this->db->from($table);
		foreach($where as $key => $val)
		{
			$this->db->where($key, $val);
		}
		foreach($join as $key => $val)
		{
			$this->db->join($key, $val);
		}
		$query = $this->db->get();
		//echo $this->db->last_query();die;
		if($query->num_rows()>0){
			return 1;
		}
		else{
			return 0;
		}
    } 
	
	 public function getModulePermission($userId,$module){
            $this->db->select("access_id AS id,user_id,user_management,news_and_events_management,service_cost_management,pdf_manuals_management,car_catalogue_management,accessories_catalogue_management,community_gallery_management,reward_management,survey_management,test_drive_request_management,upgrade_or_trade_in_management,feedback_management,banner_management", FALSE);
            $this->db->from(TB_USER_ACCESS); 
            $this->db->where("user_id",$userId); 
            $this->db->where($module,'1'); 
            $query = $this->db->get();

            //echo $this->db->last_query();die;
            if($query->num_rows()>0){
                    return 1;
            }
            else{
                    return 0;
            }
    }

    function validOTP($where = array())
    {
                $this->db->select('user_id', FALSE);
                $this->db->from(TB_USERS);
                foreach($where as $key => $val)
                {
                        $this->db->where($key, $val);
                }

                $query = $this->db->get();
                //echo $this->db->last_query();die;
                return $query->result_array();
    }
    
    
    function getUserLoginAttempt($cond = array())
    {
            $this->db->select('id,ip_address,login_attempted,time', FALSE);
            $this->db->from(TB_LOGIN_ATTEMPT);
            foreach ($cond AS $k => $v)
            {
                    $this->db->where($k,$v);
            }
           
            $query = $this->db->get();
            //echo $this->db->last_query();die;
            return $query->result_array();
    }

    function getUserLogin($username,$password)
    {
            $this->db->select('user_id,privilage_id,user_role_id,user_currentCountry,user_email,user_phoneNumber,user_loginTime,user_isActive,user_isDeleted,user_lastUpdateBy,user_isVerify');
            $this->db->where('user_email', $username);
            $this->db->where('user_password', $password);

            $query = $this->db->get(TB_USERS);
            //echo $this->db->last_query();die;
            //print_r($c);die;
            if ($query->num_rows() == 1) {

                return $query->row();
            }
            else
                return false;
    }
    function get_user_login_role($user_id)
    {

            $this->db->select('*')->from(TB_USERS)
                 ->join(TB_PRIVILEGE,'TB_USERS.privilege_id = TB_PRIVILEGE.privilege_id');
            $this->db->where('username=', $username);
            $this->db->where('password=', $password);

            $query = $this->db->get();
            if ($query->num_rows() == 1) return $query->row();
            return NULL;
    }
    function get_user_role($user_id)
    {
            $this->db->select('*')->from($this->TB_PRIVILEGE)
                 ->join('tbl_privilege','tbl_user_privilege.privilege_id = tbl_privilege.privilege_id')
                 ->where('tbl_user_privilege.user_id', $user_id);

            $query = $this->db->get();
            if ($query->num_rows() == 1) return $query->row();
            return NULL;
    }
	
 	function isEmailExist($where = array())
    {
    	$this->db->select('id', FALSE);
    	$this->db->from(TB_USERS);
    	foreach($where as $key => $val)
    	{
    		$this->db->where($key, $val);
    	}
    
    	$query = $this->db->get();
    	//echo $this->db->last_query();die;
            if($query->num_rows()>0){
	                    return 1;
	            }
	            else{
	                    return 0;
	            }
    }
    
}
?>
