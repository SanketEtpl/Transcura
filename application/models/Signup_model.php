<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Signup_model extends CI_Model{   
                          
     // Encrypt Password 
    public function encrypt_password($password){
        //$salt = sha1(rand());
        $salt = sha1( $password."abcdefghijk" );
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "password" => $encrypted);
        return $hash; 
    } 
    
    public function check_email_exist($table,$fields,$where=array()) {

      $this->db->select($fields, FALSE);

    $this->db->from($table);
    foreach($where as $key => $val)
    {
      $this->db->where($key, $val);
    }
    $query = $this->db->get();
    
    if($query->num_rows()>0) {
      return $query->result_array();
    }
    else {
      return false;
    }

    //return $query->result_array();
    }

    
    // Store Register data - YK
     public function store_register_data($name){
       $user_id = $this->get_max_id("magsood_register", "user_id");
       $flname = $this->get_fl_name($name['name']);
       $register_data = array();
       $register_data["user_id"] = $user_id;
       $register_data["first_name"] = $flname['first_name'];
       $register_data["last_name"] = $flname['last_name'];
       $register_data["mobile_num"] = $name['mobile_num'];
       $register_data["dob"] = $name['dob'];
       $register_data["register_date"] = date("Y-m-d h:i:s", time());
       $this->db->insert('magsood_register', $register_data);                                                   
       return $user_id;
    }
    
    // FB - Store Register DATA
    public function fb_store_register_data($data){
       $user_id = $this->get_max_id("magsood_register", "user_id");
       $register_data = array();   
       $register_data["user_id"] = $user_id;
       $register_data["first_name"] = $data['first_name'];
       $register_data["last_name"] = $data['last_name'];
       $register_data["register_date"] = date("Y-m-d h:i:s", time());
       $this->db->insert('magsood_register', $register_data);                                                   
       return $user_id;
    }
    // Store Login Data - YK
    public function store_login_data($data){
        $lgn_id = $this->get_max_id("magsood_lgn_details", "lgn_id");
        $user_id = $this->store_register_data($data);
        $pdata = $this->encrypt_password($data["password"]);
        $login_data = array();
        $login_data["lgn_id"] = $lgn_id;
        $login_data["user_id"] = $user_id;
        $login_data["email"] = $data["email"];
        $login_data["password"] = $pdata["password"];
        $login_data["salt"] = $pdata["salt"];
        $this->db->insert('magsood_lgn_details', $login_data);                                                   
        // Send email
        $this->send_email_confirmation($data["email"], $user_id, $pdata["salt"], $data["name"]);
        $udata = array();
        $udata = $this->get_user_details($user_id);
        return $udata;
    }
    
    // FB - Store login data
    public function fb_store_login_data($data){
        $lgn_id = $this->get_max_id("magsood_lgn_details", "lgn_id");  
        $user_id = $this->fb_store_register_data($data);  
        $pwd = $this->generate_password();
        $pdata = $this->encrypt_password($pwd); 
        $login_data = array();
        $login_data["lgn_id"] = $lgn_id;
        $login_data["user_id"] = $user_id;
        $login_data["email"] = $data["email"];
        $login_data["username"] = $data["username"];
        $login_data["facebook_id"] = $data["id"];
        $login_data["password"] = $pdata["password"];
        $login_data["salt"] = $pdata["salt"]; 
        $this->db->insert('magsood_lgn_details', $login_data); 
        // Send email
        $this->fb_welcome_email($data["email"], $data["name"], $data["username"], $pwd);
        $udata = array();
        $udata = $this->get_user_details($user_id);
        return $udata;
    }
    
    // Twitter Store Login data
    public function twitter_store_login_data($data){
       $lgn_id = $this->get_max_id("magsood_lgn_details", "lgn_id");
       $user_id = $this->fb_store_register_data($data);
       $pwd = $this->generate_password(); 
       $pdata = $this->encrypt_password($pwd);
       $login_data = array();
        $login_data["lgn_id"] = $lgn_id;
        $login_data["user_id"] = $user_id;
        $login_data["email"] = $data["email"];
        $login_data["username"] = $data["username"];
        $login_data["twitter_id"] = $data["id"];
        $login_data["password"] = $pdata["password"];
        $login_data["salt"] = $pdata["salt"]; 
        $this->db->insert('magsood_lgn_details', $login_data); 
         // Send Welcome email
        $this->fb_welcome_email($data["email"], $data["name"], $data["username"], $pwd);
        // Send Confirmation email
        $this->send_email_confirmation($data["email"], $user_id, $pdata["salt"], $data["name"]);  
        $udata = array();
        $udata = $this->get_user_details($user_id);
        return $udata;
    }
    
    // Send Confimration email - YK
     public function send_email_confirmation($email, $user_id, $salt, $name){
        $enc_user_id = $this->enrypt_user_id($user_id, $salt);
        $enc_user_id = md5($enc_user_id);
        $salt = urldecode($salt);
        require_once("script/libmail.php");
        //Init. user email
        $mail = $email;
        $message = file_get_contents("media/emails/email_confirmation.html");
        $message = preg_replace('/{user}/', $name, $message);
        $message = preg_replace('/{user_id}/', $enc_user_id, $message);
        $message = preg_replace('/{salt}/', $salt, $message);
        //Send email actions;
        $m = new Mail ();
        //Sender email
        $m->From ("anything@admixtion.com");
        //Registration user mail
        $m->To ($mail);
        //Theme
        $m->Subject ("One Minute Receipt Account Confirmation");
        //Mail content
        $m->Body ($message,"html");
        $m->Send ();
    }
    
    // FB - Send Welcome Email  - YK
    public function fb_welcome_email($email, $name, $username, $password){
        require_once("script/libmail.php");
        //Init. user email
        $mail = $email;
        $message = file_get_contents("media/emails/fb_welcome.html");
        $message = preg_replace('/{user}/', $name, $message);
        $message = preg_replace('/{username}/', $username, $message);
        $message = preg_replace('/{password}/', $password, $message);
        //Send email actions;
        $m = new Mail ();
        //Sender email
        $m->From ("anything@admixtion.com");
        //Registration user mail
        $m->To ($mail);
        //Theme
        $m->Subject ("Welcome to One Minute Receipt");
        //Mail content
        $m->Body ($message,"html");
        $m->Send ();
    }
    // Encrypt user id to send for email verification 
    public function enrypt_user_id($user_id, $salt){
        return base64_encode(sha1($user_id . $salt, true) . $salt);                                                                    
    }
    
    // facebook Signup
    public function db_signup(){
          require_once("script/src/facebook.php");                                        
          $config = array(
              'appId' => '1551627335053085',
              'secret' => '73e30a131054ba246fccda020d9e511b',      
              'fileUpload' => false, // optional
              'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
          );
          $facebook = new Facebook($config);                                    
          $user = $facebook->getUser();                                  
          if ($user) { 
          try {
            // Proceed knowing you have a logged in user who's authenticated.
                $user_profile = $facebook->api('/me');  
                // print_r($user_profile);
                // exit();
                $data = array();
                $data["id"] = $user_profile["id"];
                $data["name"] = $user_profile["name"];
                $data["first_name"] = $user_profile["first_name"];
                $data["last_name"] = $user_profile["last_name"];
                $data["email"] = $user_profile["email"];
                $data["username"] = $user_profile["username"];
                // Check if user already registered with this FB ID
                if($this->check_social_id('facebook_id', $user_profile["id"])){
                    // return user data
                    return $this->get_user_details($this->get_user_id_using_social_id('facebook_id', $user_profile["id"]));
                }else{
                    return $this->fb_store_login_data($data);    
                }                
            }
             catch(Exception $e){
                 // Redirect to Signup
                // redirect('signup');
             }
          }
    }
    
    // Check Social ID is exist - YK
    public function check_social_id($column, $id){
       $this->db->select($column)->from('magsood_lgn_details')->where($column, $id);
        $query = $this->db->get();
        $row = $query->row_array();
        if($query->num_rows() > 0){
           return true;
        }else{
          return false;
        } 
    }
    
    // Google Login - Using User ID and Social ID 
    public function check_google_id($google_id, $user_id){
        $this->db->select('user_id')->from('magsood_lgn_details');
        $this->db->where('google_id', $google_id);
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();
        $row = $query->row_array();
        if($query->num_rows() > 0){
           return true;
        }else{
          return false;
        } 
    }
    
    // Get user ID using Social ID - YK 
    public function get_user_id_using_social_id($column, $id){
        $data = "";
        $this->db->select("user_id")->from("magsood_lgn_details")->where($column, $id);
        $query = $this->db->get();
        $row = $query->row_array();
        if($query->num_rows() > 0){     
            $data = $row["user_id"];
        }
        return $data;
    }
    
    // GET SALT using Google ID 
    public function get_salt_by_google_id($google_id){
        $data = "";
        $this->db->select("salt")->from("magsood_lgn_details")->where('google_id', $google_id); 
        $query = $this->db->get();
        $row = $query->row_array();
        if($query->num_rows() > 0){     
            $data = $row["salt"];
        }
        return $data;  
    }
    
    // FB - Generate Password to send 
    public function generate_password($length = 8){
        $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ123456789';
        $numChars = strlen($chars);
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= substr($chars, rand(1, $numChars) - 1, 1);
        }
        return $string;
    }
    
    // Do Twitter Operations 
    public function twitter_ope(){
         require("script/twitter/twitteroauth.php");
         require 'script/twitter/twconfig.php';
         
         $twitteroauth = new TwitterOAuth(YOUR_CONSUMER_KEY, YOUR_CONSUMER_SECRET);
         // Requesting authentication tokens, the parameter is the URL we will be redirected to
         $request_token = $twitteroauth->getRequestToken('http://localhost/magsood/index.php/signup/twittersu');
         
         // Saving them into the session
         $sess_data = array();
         $sess_data["oauth_token"] = $request_token['oauth_token']; 
         $sess_data["oauth_token_secret"] = $request_token['oauth_token_secret'];                                                                          
         $this->session->set_userdata($sess_data);   
         
         // If everything goes well..
        if ($twitteroauth->http_code == 200) {
            // Let's generate the URL and redirect
            $url = $twitteroauth->getAuthorizeURL($request_token['oauth_token']);
            header('Location: ' . $url);
        } else {
            // It's a bad idea to kill the script, but we've got to know when there's an error.
            die('Something wrong happened.');
        } 
        
    }
    
    public function twitter_register(){
       require("script/twitter/twitteroauth.php");
       require 'script/twitter/twconfig.php'; 
       if(!empty($_GET['oauth_verifier']) && $this->session->userdata('oauth_token') != "" && $this->session->userdata('oauth_token_secret') != ""){
             // We've got everything we need
             $twitteroauth = new TwitterOAuth(YOUR_CONSUMER_KEY, YOUR_CONSUMER_SECRET, $this->session->userdata('oauth_token'), $this->session->userdata('oauth_token_secret'));
             // Let's request the access token
             $access_token = $twitteroauth->getAccessToken($_GET['oauth_verifier']);
             // Save it in a session var
              $sess_data = array();
              $sess_data["access_token"] = $access_token;        
              $this->session->set_userdata($sess_data);
              // Let's get the user's info
              $user_info = $twitteroauth->get('account/verify_credentials');
              // Print user's info
              $user_data = array();
              $user_data["twitter_id"] = $user_info->id;
              $user_data["name"] = $user_info->name;                  
              $pos = strrpos($user_info->name, " "); 
              if($pos == true){
                  $fl_name = array();
                  $fl_name = $this->get_fl_name($user_info->name); 
                  $user_data["first_name"] = $fl_name["first_name"];
                  $user_data["last_name"] = $fl_name["last_name"];  
              }else{
                   $user_data["first_name"] = $user_info->name;                  
                   $user_data["last_name"] = '';                
              }    
              $user_data["username"] = $user_info->screen_name;
              $this->session->set_userdata($user_data);   
              redirect('signup/twitter_email');
             if (isset($user_info->error)) {
                  redirect('signup/twitter'); 
             }else{
                 
             }
       }else {
            // Something's missing, go back to square 1
            redirect('signup/twitter');
       }
    }
}
