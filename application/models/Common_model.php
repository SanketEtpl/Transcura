<?php
class Common_model extends CI_Model{   
    public $table;
    function __construct()
    {
        parent::__construct();
        $this->load->database();        
    }

    function record_count($sel,$table,$cond = array())
    {
        $this->db->select($sel)->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }        
        $query = $this->db->get();
        return $query->num_rows();
    } 

    function fetch_schedule_ride($sel,$table,$cond = array(),$limit,$start)
    {
        $this->db->limit($limit,$start);
        $this->db->select($sel)->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $query = $this->db->get();
        if($query->num_rows()>0){
            foreach ($query->result_array() as $row) {
                $data[] = $row; 
            }
            return $data;
        }
           return false; 
    }
    
    function insert($table,$data)
    {
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }

    function insertData($table,$data)
    {
        if($this->db->insert($table,$data))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    function insert_batch($table,$data)
    {   
        try {
            $this->db->insert_batch($table,$data);
            return true;
        }
        catch(Exception $e) {
          return false;
        }
    }

    /* function insert_csv($table, $data) {
    $this->db->insert($table, $data);
    }*/
    
    function update($table,$where=array(),$data)
    {
        try {
            $this->db->update($table,$data,$where);
            if($this->db->affected_rows() == 0){
                return true;
            }
            return $this->db->affected_rows();
        }
        catch(Exception $e) {
          return false;
        }
    }

    function updateIncrement($table,$where=array(),$data)
    {
        foreach ($data AS $k => $v)
        {
            $this->db->set($k, $v, FALSE);
        }
        foreach ($where AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $this->db->update($table);
        return $this->db->affected_rows();
    }
    
    function update_batch($table,$where,$data)
    {
        try {
            $this->db->_protect_identifiers=true;
            $this->db->update_batch($table,$data,$where);
            if($this->db->affected_rows() == 0){
                return true;
            }
            return $this->db->affected_rows();
        }
        catch(Exception $e) {
          return false;
        }
    }
    
    function delete($table,$where=array())
    {
        $this->db->delete($table,$where);
        return $this->db->affected_rows();
    }
    
    function delete_batch($table,$key,$ids=array())
    {   
        $this->db->where_in($key,$ids);
        $this->db->delete($table);
        return $this->db->affected_rows();
    }
    
    function select($sel,$table,$cond = array())
    {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $query = $this->db->get();
      //echo $this->db->last_query();die;
        return $query->result_array();
    }
	
    function selectWhereIn($sel,$table,$where=NULL,$in=array())
    {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        $this->db->where_in($where,$in);
        /*foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }*/
        $query = $this->db->get();
        return $query->result_array();
    }
	
    function group_by($sel,$table,$cond=array(),$where=array())
    {
        $this->db->select($sel,FALSE);
        $this->db->from($table);
        foreach ($where as $key => $value) {
           $this->db->where($key,$value);
        }        
        foreach ($cond as $key => $value) {
            $this->db->group_by($value);    
        }        
        $query = $this->db->get();        
        return $query->result_array();

    }
	
    function selectQuery($sel,$table,$cond = array(),$orderBy=array(),$join=array(),$joinType=array())
    {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        foreach($orderBy as $key => $val)
        {
            $this->db->order_by($key, $val);
        }
        foreach($join as $key => $val)
        {
            if(!empty($joinType) && $joinType[$key]!=""){
                $this->db->join($key, $val,$joinType[$key]);
            }else{
                $this->db->join($key, $val);    
            }
        }
        $query = $this->db->get();
        /*echo $this->db->last_query();
        exit;*/
        
        return $query->result_array();
    }


    function getRowsPerPage($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(), $page = 0,$join=array())
    {
        $this->db->select($select, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $q = "";
        foreach($like AS $k => $v)
        {
            $q .= $k." LIKE '%".$v."%' OR ";
        }
        if($q != ""){ $q = substr($q,0,-3);
            $this->db->where("(".$q.")");
        }
        foreach($join as $key => $val)
        {
            $this->db->join($key, $val);
        }
        foreach($orderBy as $key => $val)
        {
            $this->db->order_by($key, $val);
        }
        $this->db->limit(PER_PAGE,($page*PER_PAGE));
        $query = $this->db->get();
        return $query->result_array();
    }


    function getCount($select = "*", $table, $cond = array(), $like = array(), $orderBy = array(),$join=array())
    {
        $this->db->select($select, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $q = "";
        foreach($like AS $k => $v)
        {
            $q .= $k." LIKE '%".$v."%' OR ";
        }
        if($q != ""){ $q = substr($q,0,-3);
            $this->db->where("(".$q.")");
        }
        foreach($join as $key => $val)
        {
            $this->db->join($key, $val);
        }
        foreach($orderBy as $key => $val)
        {
            $this->db->order_by($key, $val);
        }
        $query = $this->db->get();
        //echo $this->db->last_query();
        return $query->result_array();
    }

    function getDriverLocation($sel,$table,$cond = array())
    {
        $this->db->select($sel, FALSE);
        $this->db->from($table);

        $this->db->where('user_id !=',$cond[0]);
        $this->db->where('updated_at >=',date('Y-m-d'));

        $query = $this->db->get();
      //echo $this->db->last_query();die;
        return $query->result_array();
    }

    /*function select($sel,$table,$cond = array())
    {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $query = $this->db->get();
      //echo $this->db->last_query();die;
        return $query->result_array();
    }*/

    /* @author : Smita
     * @function Name : FetchDriverUserDataById this function user for fetch register 
        driver data for profile
     * date : 12-1-2018
     */
    public function FetchDriverUserDataById($user_id){

        $this->db->select('u.*,coty.country_name, state.state_name, city.city_name,st.  type_title');
        $this->db->from(TB_USERS.' as u');
        $this->db->join(TB_COUNTRY.' as coty', 'coty.id = u.country', 'left'); 
        $this->db->join(TB_STATE.' as state', 'state.id = u.state', 'left');
        $this->db->join(TB_CITY.' as city', 'city.id = u.city', 'left');
        $this->db->join(TB_SERVICE_TYPE. ' as st', 'st.type_id = u.own_bus_services', 'left');
        $this->db->where('u.id',$user_id);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function FetchDriverUserDataById_WithoutServiceType($user_id){

        $this->db->select('u.*,coty.country_name, state.state_name, city.city_name');
        $this->db->from(TB_USERS.' as u');
        $this->db->join(TB_COUNTRY.' as coty', 'coty.id = u.country', 'left'); 
        $this->db->join(TB_STATE.' as state', 'state.id = u.state', 'left');
        $this->db->join(TB_CITY.' as city', 'city.id = u.city', 'left');        
        $this->db->where('u.id',$user_id);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;
        return $query->result_array();
    }

    public function updateDriverUserProfile($datas)
    {  
         $data=array(
            'full_name'=>$datas['full_name'],
            'date_of_birth'=>$datas['date_of_birth'],
            'phone'=>$datas['phone_no'],
            //'email'=>$datas['email'],
            'country'=>$datas['country'],
            'state'=>$datas['state'],
            'city'=>$datas['city'],
            'zipcode'=>$datas['zipcode'],
            'county'=>$datas['county'],
            'street'=>$datas['street'],
            'company_name'=>$datas['company_name'],
            'company_address'=>$datas['company_address'],
            'comp_veh_transp_comp'=>$datas['transport_comp'],
            'company_vehicle_status'=>$datas['company_vehicle_dropdown'],
            'comp_veh_vehicle_details' =>$datas['vehicle_details'],
            'comp_veh_services'=>$datas['com_veh_type_of_sevices'],
            'own_bus_vehicle_detiail'=>$datas['own_buss_vehicle_details'],
            'own_bus_services'=>$datas['own_bus_services'],
            'own_business_status'=>$datas['own_business_status'],
            'own_bus_vehicle_reg_doc'=>$datas['own_bus_vehicle_reg_doc'],
            'insurance_id'=>$datas['insurance_id'],
            'emergency_contactname'=>$datas['emergency_contact_name'],
            'emergency_contactno'=>$datas['emergency_contact_number'],
            'signature'=>$datas['signature'],
            'picture'=>$datas['picture'],
            
            'personal_doc_status' => $datas['personal_doc_status'],            
            'social_security_card_doc'=>$datas['social_security_card_doc'],
            'driver_license_doc'=>$datas['driver_license_doc'],
            'driver_license_expire_dt'=>$datas['driver_license_expire_dt'],

            // 'vehicle_doc'=>$datas['vehicle_doc'],
            // 'personal_doc'=>$datas['personal_doc'],
            'signature_file_upload'=>$datas['signature_file_upload'],
            'drug_test_results_doc'=>$datas['drug_test_results_doc'],
            'drug_test_results_expire_dt'=>$datas['drug_test_results_expire_dt'],
            'criminal_back_ground_doc'=>$datas['criminal_back_ground_doc'],
            'criminal_back_ground_expire_dt'=>$datas['criminal_back_ground_expire_dt'],
            'motor_vehicle_record_doc'=>$datas['motor_vehicle_record_doc'],
            'motor_vehicle_record_expire_dt'=>$datas['motor_vehicle_record_expire_dt'],
            'act_33_and_34_clearance_doc'=>$datas['act_33_and_34_clearance_doc'],
            'act_33_and_34_clearance_expire_dt'=>$datas['act_33_and_34_clearance_expire_dt'],
            'vehicle_doc_status'=>$datas['vehicle_doc_status'],
            'vehicle_inspections_doc'=>$datas['vehicle_inspections_doc'],
            'vehicle_inspections_expire_dt'=>$datas['vehicle_inspections_expire_dt'],

            'vehicle_maintnc_record_doc'=>$datas['vehicle_maintnc_record_doc'],
            'vehicle_insurance_doc'=> $datas['vehicle_insurance_doc'],
            'vehicle_insurance_expire_dt'=>$datas['vehicle_insurance_expire_dt'],
            'vehicle_regi_doc'=>$datas['vehicle_regi_doc'],
            'vehicle_regi_expire_dt'=>$datas['vehicle_regi_expire_dt'],
           // 'operate_own_vehicle_own_business_doc'=>$datas['operate_own_vehicle_own_business_doc'],
            'own_business_status'=>$datas['own_business_status'],            
            'own_bus_insurance_pol_doc'=>$datas['own_bus_insurance_pol_doc'],
            'own_bus_inspection_doc'=> $datas['own_bus_inspection_doc'],
            'own_bus_taxid_einno_doc'=>$datas['own_bus_taxid_einno_doc'],
            );  
/*echo "<pre>";
                     print_r($datas);echo "</pre>";exit;*/
        $this->db->where('id',$datas['user_id']);
        return $this->db->update(TB_USERS,$data);
    }


    public function fetchDriverDetails(){
        $this->db->select('u.full_name, u.first_name, u.phone,u.email,u.date_of_birth,u.picture,u.address,u.zipcode,u.user_type,u.status,dcl.*, DATE_FORMAT(NOW(), "%Y") - DATE_FORMAT(u.date_of_birth, "%Y") - (DATE_FORMAT(NOW(), "00-%m-%d") < DATE_FORMAT(u.date_of_birth, "00-%m-%d")) AS age');
        $this->db->from(TB_USERS.' as u');
        $this->db->join(TB_DRIVERS_CURRENT_LOCATION.' as dcl', 'dcl.user_id = u.id', 'inner');
        $this->db->where('u.status','active');
        $query = $this->db->get();
        return $query->result();
    }

    public function searchHealthcare($sel,$table,$column,$cond = array(),$join=array(),$joinType=array())
    {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach($column AS $k => $v)
        {
            $where = "FIND_IN_SET('".$v."', $k)";  
            $this->db->where($where);   
        }

        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }

        foreach($join as $key => $val)
        {
            if(!empty($joinType) && $joinType[$key]!=""){
                $this->db->join($key, $val,$joinType[$key]);
            }else{
                $this->db->join($key, $val);    
            }
        }

        $query = $this->db->get();
      //echo $this->db->last_query();die;

        return $query->result_array();

    }

    /*function select($sel,$table,$cond = array())
    {
        $this->db->select($sel, FALSE);
        $this->db->from($table);
        foreach ($cond AS $k => $v)
        {
            $this->db->where($k,$v);
        }
        $query = $this->db->get();
      //echo $this->db->last_query();die;
        return $query->result_array();
    }*/
   
    public function getMaster($tablename, $where = FALSE, $join = FALSE, $order = false, $field = false, $select = false,$limit=false,$start=false, $search=false)
    {
        if($limit){
            $this->db->limit($limit, $start);
        }
        if ($search) {
                $where = $this->searchString($search);
                $query = $this->db->get_where($tablename, $where);
        } 
        if ($where) {
            $this->db->where($where, NULL, FALSE);
        }
        if ($order && $field) {
            $this->db->order_by($field, $order);
        }
        if ($join) {
            if (count($join) > 0) {
                foreach ($join as $key => $value) {
                    $explode = explode('|', $value);
                    $this->db->join($key, $explode[0], $explode[1]);
                }
            }
        }
        if ($select) {
            $this->db->select($select, FALSE);
        } else {
            $this->db->select('*');
        }
        $query = $this->db->get($tablename);
        //echo $this->db->last_query();die;
        return $query->result_array();
    }
}
?>
