<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

  public function index()
  {
    if(is_user_logged_in())
    {
      redirect("users");
      exit;
    }else{
      $this->load->view('login');
    }
  }

  public function checkLogin(){
    if(is_ajax_request())
    {
      $postData = $this->input->post();
      $result = $this->common->select("*",TB_ADMIN,array("username"=>trim($postData["username"]),"password"=>md5(trim($postData["password"]))));
      if($result)
      {
        $this->session->set_userdata("auth_user", $result[0]);
        echo json_encode(array("status" => "success" ,"message"=>"User login sucessfully.")); exit;
      }
      else
      {
        echo json_encode(array("status" => "error","message"=>"Please provide valid login credential. ")); exit;
      }
    }
  }

 public function signup(){

      $data['title']  = 'Sign Up';
      $data['meta_description'] = '';
      $data['meta_keyword'] = '';
      $data['main_content'] = 'frontend/sign-up';
      $this->load->view("frontend/incls/layout",$data);   
      //$this->load->view('frontend/sign-up');
  }

public function register_user(){

        if ($this->validate_email($_REQUEST['email']) == false) {
            $errors['error'] = 101; 
            echo json_encode($errors);        
        }
        else {
          
            $combinationString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789*#&$^";
            $this->load->helper('string');
            $token =  random_string('alnum',20);
            $login_data = array();
            $login_data["username"] = $_REQUEST['username'];
            $login_data["email"] = $_REQUEST['email'];
            $login_data["password"] = $_REQUEST['password'];
            $login_data["user_token"] = $token;
            if($login_data["password"]!=$_REQUEST["confirm_password"])
            {
              $errors['error'] = "Not match password"; 
              echo json_encode($errors);             
            }
            $login_data["created_at"] = date("Y-m-d h:i:s", time());
            $insertId = $this->common->insert(TB_USERS,$login_data); 
            //echo $this->db->last_query();die;          
            $udata = array();
            if($insertId){
              echo json_encode(array("status"=>"success","action"=>"add","msg"=>"User has been added successfully."));
              redirect("home"); exit;
            }else{
              echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again."));
              redirect("home/signup"); exit;
            }

           // $this->session->set_userdata("auth_user", $udata);            
                     
        }
   }
   

// Check email is exist
public function validate_email($email){

  $this->load->model('signup_model');
  $email=(array("email"=>$email));
  if($this->signup_model->check_email_exist(TB_USERS,"email",$email)){    
  //$this->form_validation->set_message("validate_email", "Email already exist.");
     return false;
  }else{
     return true;
  }
}











  public function logout()
  {
    $this->session->sess_destroy();
    redirect("login"); exit;
  }
}
