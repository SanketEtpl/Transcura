<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Api extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->database();
        $this->load->model("common_model");
        $this->load->library("email");
        $this->load->library("User_AuthSocial");
        $this->load->helper("email_template");
        $this->load->helper('string');
        $this->load->library('upload');
        $this->load->library("image_lib"); 
        $this->load->helper('security');     
        $this->pro_profile_origin =  realpath('uploads/Patients/original');
        $this->pro_profile_thumb =  realpath('uploads/Patients/thumb');
        $this->pro_profile_origin1 =  realpath('uploads/Driver/original');
        $base_url = $this->config->item('base_url');

    }

/*   Register NEMT/member*/
public function registerUser_post()
{
  $postData = $_POST;
  $postData = $this->input->post(); 
  $reset_token = random_string('unique');
  $usertype = trim($this->post('usertype'));
  $base_url = $this->config->item('base_url');
  $reset_url = $base_url."accountverify/".$reset_token;

 if ($postData["email"] != ''  &&  $postData["usertype"] != '' &&  $postData["password"] != '')
 {
        $emailexist = $this->common_model->select("email",TB_USERS_NEW,array("email"=>trim($postData["email"]) , "user_type"=>trim($postData["usertype"])));  
        if(count($emailexist)>0) {
        $this->response(array("status"=>200, "statusCode"=>false, "message"=> "User already exist with this email id."), 200);
        }  
        else
        {
                if(isset($_FILES["personal_doc"]["name"]))
                {
                   $isValid = $this->upload_profile_bio($postData, $_FILES);
                    if ($isValid["status"] == 'success')
                    {         
                        $file_name = $isValid['data']['file_name']; 
                        if($postData['usertype']==2) 
                        {         
                        $doc = 'uploads/Patients/doc/'.$file_name; 
                        }
                        if($postData['usertype']==1) 
                        {         
                        $doc = 'uploads/Member/doc/'.$file_name; 
                        }
                                  
                    }
                    else
                    {
                    echo $isValid;
                    }
                }
                else
                {
                      $this->response(array("status"=>200,"statusCode"=>false, "message"=> "personal document missing"), 200);
                }

                if(isset($postData["usertype"]) &&  $postData['usertype']=='2')
                { 
                    $insurance_id = trim($postData["insurance_id"]);
                    // if(isset($postData["insurance_id"]) && $postData["insurance_id"]!="")
                    // {
                    //   $insurance_id = trim($postData["insurance_id"]);
                    // }               
                    // else
                    // {
                    //  $this->response(array("status"=>200,"statusCode"=>false, "message"=> "NEMT user insurance id  missing"), 200); 
                    // }
                
                } 
                else if(isset($postData["usertype"]) && $postData['usertype']=='1')
                {                
                  $insurance_id = 0;                   
                } 

                  $insertArr = array("user_type"=>$usertype,
                            "user_token" => mt_rand(100000, 999999),
                            "password"     =>md5(trim($postData["password"])),
                            "full_name"   =>trim($postData["fullname"]), 
                            "date_of_birth" =>trim($postData["date_of_birth"]),            
                            "phone"        =>trim($postData["phone"]),
                            "email"        =>trim($postData["email"]),                          
                            "status"=>"inactive",
                            "token"=>$reset_token,
                            "device_type"=>trim($postData["device_type"]),                                           
                            "country" => trim($postData["country_id"]),  
                            "county" => trim($postData["county"]), 
                            "city"   =>trim($postData["city_id"]),
                            "street" =>trim($postData["street"]),
                            "state"  =>trim($postData["state_id"]),  
                            "zipcode" => trim($postData["zipcode"]),
                            "personal_doc" => $doc,  
                            "insurance_id"    =>$insurance_id,                    
                            "signature" => trim($postData["signature"]),
                            "emergency_contactno"=>trim($postData["emergency_contactno"]),
                            "emergency_contactname"=>trim($postData["emergency_contactname"]),
                          );
                      

                  $user = $this->common_model->insert(TB_USERS_NEW,$insertArr);
                  //echo $this->db->last_query();die;                        
                  if($user) {
                                          $to_email =trim($postData["email"]);
                                          $hostname = $this->config->item('hostname');  
                                          $config['mailtype'] ='html';
                                          $config['charset'] ='iso-8859-1';
                                          $this->email->initialize($config);
                                          $from  = EMAIL_FROM; 
                                          $this->messageBody  = email_header();
                                   $this->messageBody  .= '<tr>
                                  <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                                      <p>Dear Customer,</p>
                                          <p>
                                          You are registered successfully on Transcura.<br>
                                          Please click the link below to verify your email address.</p>
                                          <p>
                                          <p>Activation Link : </p>
                                        <a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href='.$reset_url.'>Click Here</a>
                                        </p>
                                    </td>
                                </tr>';
                                        $this->messageBody  .= email_footer();                        
                                        $this->email->from($from, $from);
                                        $this->email->to($to_email);
                                        $this->email->subject('Welcome to Transcura');
                                        $this->email->message($this->messageBody);
                                        $this->email->send();
                                        $this->response(array("status"=>200,"statusCode"=>true,"message"=> "Thank You! Please check your email to activate your account.","statusCode"=>"true"), 200);  
                          }
                      else
                      {
                     $this->response(array("status"=>200,"statusCode"=>false, "message"=> "some issue in registration"), 200);
                      }
      }

  }
  else
  {
  $this->response(array("status"=>200,"statusCode"=>false, "message"=> "all fields are mandatory"), 200);
  }

}

/* upload file */

  public function upload_profile_bio1($postData, $FILES)
  {
    $_FILES = $FILES;
  //print_r($_FILES);die;
		$i=0;
	             // if($postData['usertype']==2) 
              //     {         
              //     $config['upload_path'] = "./uploads/Patients/doc";
              //     }
              //     if($postData['usertype']==1) 
              //     {         
              //     $config['upload_path'] = "./uploads/Member/doc";
              //     }
                  if($postData['usertype']==3) 
                  {         
                  $config['upload_path'] = "./uploads/Driver/doc";
                  }
                  $config['allowed_types'] = "pdf|PDF";      
                  $config['max_size'] = '102400000';
                  $config['max_width'] = '200000000';
                  $config['max_height'] = '1000000000000';
                  $random = rand(1, 10000);
                  $this->load->library('upload', $config);
                  $this->upload->initialize($config); 
                 
          if($postData['usertype']==3)
          {
			           
            if(isset($_FILES['operate_own_vehicle_ownbusiness_doc']))
			{	
				$filename = $_FILES['operate_own_vehicle_ownbusiness_doc']['name']; 
				$config['file_name'] =$random."_".$filename;       
				$check_upload = $this->upload->do_upload("operate_own_vehicle_ownbusiness_doc");				
				if ($check_upload) {
					$uploaded1 = $this->upload->data();								
					return array("status" => "success","data1"=>$uploaded1, "msg" => "Successfully added"); 
					} else {
					$error = $this->upload->display_errors();
					return $this->response(array("status" => "error", "msg" => $error), 200);
					}
            }

            else if(isset($_FILES['comy_vehicle_doc']))
			{	
				$filename = $_FILES['comy_vehicle_doc']['name']; 
				$config['file_name'] =$random."_".$filename;            
				$check_upload = $this->upload->do_upload("comy_vehicle_doc");              
               		if ($check_upload) {
						$uploaded1 = $this->upload->data();
					 return array("status" => "success","data1"=>$uploaded1,"msg" =>"Successfully added"); 
					} else {
					  $error = $this->upload->display_errors();
					  return $this->response(array("status" => "error", "msg" => $error), 200);
					}
            }

             else if(isset($_FILES['own_vehicle_comy_doc']))
            {
				$filename = $_FILES['own_vehicle_comy_doc']['name']; 
				$config['file_name'] =$random."_".$filename;            
				$check_upload = $this->upload->do_upload("own_vehicle_comy_doc");	
				if ($check_upload) {
					$uploaded1 = $this->upload->data();
	              return array("status" => "success","data1"=>$uploaded1,"msg" =>"Successfully added"); 
	               } else {
	              $error = $this->upload->display_errors();
	              return $this->response(array("status" => "error", "msg" => $error), 200);
	               }
            }

         }

   }


  public function upload_profile_bio($postData, $FILES)
  {
    $_FILES = $FILES;
  //print_r($_FILES);die;
		$i=0;
	          if($postData['usertype']==2) 
                  {         
                  $config['upload_path'] = "./uploads/Patients/doc";
                  }
                  if($postData['usertype']==1) 
                  {         
                  $config['upload_path'] = "./uploads/Member/doc";
                  }
                  if($postData['usertype']==3) 
                  {         
                  $config['upload_path'] = "./uploads/Driver/doc";
                  }
                  $config['allowed_types'] = "pdf|PDF";      
                  $config['max_size'] = '102400000';
                  $config['max_width'] = '200000000';
                  $config['max_height'] = '1000000000000';
                  $random = rand(1, 10000);
                  $this->load->library('upload', $config);
                  $this->upload->initialize($config); 
            if(isset($_FILES['personal_doc']))
            {
				      $filename = $_FILES['personal_doc']['name']; 
			      	$config['file_name'] =$random."_".$filename;            
				      $check_upload = $this->upload->do_upload("personal_doc");			
				      //print_r($uploaded1);die;
				if ($check_upload) {
					$uploaded1 = $this->upload->data();
	              return array("status" => "success","data"=>$uploaded1, "msg" => "Successfully added"); 
	               } else {
	              $error = $this->upload->display_errors();
	              return $this->response(array("status" => "error", "msg" => $error), 200);
	               }
            }
         
  
   }


 public function upload_profile_image($type, $FILES)
  {
    $usertype = $type ;
    $_FILES = $FILES;  
    if (isset($_FILES['picture']['name']) && $_FILES['picture']['name'] != "") {
                   $count = count($_FILES['picture']['name']);                     
                  $filename = $_FILES['picture']['name'];
                 if($usertype==2) 
                  {         
                  $config['upload_path'] = "./uploads/Patients/original";
                  }
                  if($usertype==1) 
                  {         
                  $config['upload_path'] = "./uploads/Member/original";
                  }
                   if($usertype==3) 
                  {         
                  $config['upload_path'] = "./uploads/Driver/original";
                  }

                  $base_url = $this->config->item('base_url');
                  $config['allowed_types'] =   "png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG";  
                  $config['min_width']     = "125"; // new
                  $config['min_height']    = "70"; // new    
                  $random = rand(1, 10000);
                  $new_file = $random.$filename;  
                  $config['file_name'] = $new_file;     
                   //  print_r($config);die;          
                  $this->load->library('upload', $config);
                  $this->upload->initialize($config);
                  $check_upload = $this->upload->do_upload('picture');  
                  if ($check_upload) {
                        $uploaded = $this->upload->data(); 
                        $data = $this->resize($usertype,$uploaded);
                      return array("status" => "success", "data" => $data, "count" => $count, "msg" => "Successfully added"); 
                       } else {
                      $error = $this->upload->display_errors();
                      return $this->response(array("status" => "error", "msg" => $error), 200);
                  }
      }
      else {
          return $this->response(array("status"=>200,"statusCode"=>false, "msg" => "document not found"), 200);
      }
  }




public function resize($type,$image_data) {

        $userlist = $this->common_model->select("user_type",TB_USERS,array("id"=>$_REQUEST['userid']));
        if( $type=='1')
          {
          $config['new_image'] ='uploads/Member/thumb/thumb_'.$image_data['raw_name'].$image_data['file_ext'];
          }
        if( $type=='2')
          {
          $config['new_image'] ='uploads/Patients/thumb/thumb_'.$image_data['raw_name'].$image_data['file_ext'];
          }   
        if( $type=='3')
          {
          $config['new_image'] ='uploads/Driver/thumb/thumb_'.$image_data['raw_name'].$image_data['file_ext'];
          }       
        $config['image_library'] = 'gd2';
        $config['source_image'] = $image_data['full_path'];
        $config['allowed_types'] = 'png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG';
        $config['width'] = 350;
        $config['height'] = 175;
        $this->image_lib->initialize($config);
        $src = $config['new_image'];
        $data['new_image'] = substr($src, 0);
        $data['img_src'] =$data['new_image'];
        if (!$this->image_lib->resize())
                    {
                    $errror= $this->image_lib->display_errors(); 
                    return  $error;
                    }
        else
        {
        return  $data;    
         
        }
                
  }



 /*   login NEMT/DRIVER/MEMBER using usertype */
  public function loginUser_post() {
     $postData = $_POST;
    if ($postData["email"] != ''  ||  $postData["password"] != '' ||  $postData["usertype"] != '')
    {   
          $result = $this->common_model->select("*",TB_USERS_NEW,array("email"=>trim($this->post('email')),"password"=>md5(trim($this->post('password')))));
          //echo $this->db->last_query();die;
          if(count($result)>0) {  
              if($result[0]["status"]=='active')
              { 
              $userinfo = array(
                            "full_name" => $result[0]["full_name"],
                            "email" => $result[0]["email"],
                            "phone" => $result[0]["phone"],                            
                            "date_of_birth" => $result[0]["date_of_birth"],
                            "zipcode" => $result[0]["zipcode"],
                            "picture" =>  $base_url.$result[0]["picture"],
                            "zipcode" => $result[0]["zipcode"],
                            "city" => $result[0]["city"],                                                
                            "user_type" =>$result[0]["usertype"],
                            "done" =>$result[0]["done"],
                            "id" => $result[0]["id"],
                            "state" =>  $result[0]["street"],   
                            "street" =>  $result[0]["state"],
                            "country" => $result[0]["country"],
                            "county" => $result[0]["county"]            
                                        
                         );
              $this->response(array("status"=>200,"statusCode"=>true,"userinfo"=>$userinfo,"message"=> ""), 200);
            }else 
            {
                $this->response(array("status"=>200,"statusCode"=>false,"message"=> "User not activate."), 200);
            }
        }
        else
        {
        $this->response(array("status"=>200,"statusCode"=>false, "message"=> "Invalid username or password, Please try again."), 200);
        }
    }
    else
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=> "all fields are mandatory"), 200);
    }

  }


  /*   forgotpassword */
  public function forgotPassword_post() {      
        $postData = $_POST;
          $email = $this->post('email');
      if (isset($_POST['email']) && $_POST['email'] != "") {
              $userdata = $this->common_model->select("`id`,`email`,`password`,`phone`",TB_USERS_NEW,array("email"=>trim($email),"status"=>"active"));
                  if(count($userdata)==0) {                      
                   $this->response(array("status"=>200,"statusCode"=>false, "message"=> "The email address that you have provided is not associated with any of the active accounts."), 200);
                  }                  
                  else {
                     $userId = $userdata[0]['id'];
                     $reset_token = random_string('unique');
                     $userId = base64_encode($userId);
                     $reset_url = base_url()."frontend/Homecontroller/resetPassword/".$userId."/".$reset_token;                  
                      $useremail =trim($userdata[0]["email"]) ;               
                      $datareset = array('token' =>  $reset_token); 
                      $cond1 = array('email' =>  $useremail); 
                      $this->common_model->update(TB_USERS_NEW,$cond1,$datareset);       
                      //Send Reset password Link
                      $hostname = $this->config->item('hostname');
                      $config['mailtype'] ='html';
                      $config['charset'] ='iso-8859-1';
                      $this->email->initialize($config);
                      $from  = EMAIL_FROM; 
                      $this->messageBody  = email_header();
                      $this->messageBody  .= '<tr> 
                          <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                              <p>Hello,</p>
                                  <p>
                                  A request has been made to reset your Transcura account password.<br>
                                  Please click the link below to reset your password.</p>
                                  <p>
                                  <p>Activation Link : </p>
                                  <a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href="'.$reset_url.'">Click Here</a>
                                  </p>
                              </td>
                          </tr>
                          <tr>';                      
                      $this->messageBody  .= email_footer();
                      $this->email->from($from, $from);
                      $this->email->to($email);
                      $this->email->subject('Reset Password Link');
                      $this->email->message($this->messageBody);
                      $this->email->send();         
                     $this->response(array("status"=>200,"statusCode"=>true, "message"=> "A link to reset your password
    has been sent to this email address."), 200);
                  }
      }
      else
      {
         return $this->response(array("status"=>200,"statusCode"=>false, "msg" => "Please enter valid email id"), 200);
      }
  }


  /* Edit profile for Nemt/Member*/
  public function editProfile_post()
  { 
    $postData = $_POST; 
    $base_url = $this->config->item('base_url');
    if ($postData["userid"] != '' &&  $postData['usertype']!='' &&  $postData["phone"] != '' &&  $postData["country_id"] != '' &&  $postData["date_of_birth"] != ''  && $postData['fullname']!='')
    {
      $userid = $this->post('userid');
      $userdata = $this->common_model->select("`id`,`email`,`phone`",TB_USERS_NEW,array("id"=>trim($userid),"user_type"=>$postData['usertype']));   
      if(count($userdata)==0) {                      
      $this->response(array("status"=>200,"statusCode"=>false, "message"=> "user not found."), 200);
      }
      else
      {
          if(isset($_FILES["personal_doc"]["name"]))
          {       
              $isValid = $this->upload_profile_bio($postData, $_FILES);
              if ($isValid["status"] == 'success')
              {         
                  $file_name = $isValid['data']['file_name']; 
                  if($postData['usertype']==2) 
                  {         
                  $doc = 'uploads/Patients/doc/'.$file_name; 
                  // $insurance_id=trim($this->post('personal_docold')) ; 
                  }
                  if($postData['usertype']==1) 
                  {         
                  $doc = 'uploads/Member/doc/'.$file_name; 
                  }                                  
              }
              else
              {
                  echo $isValid;
              }
          }

          else
          {            
              $doc = trim($this->post('personal_docold')) ; 
          }
          if( $postData['usertype']=="2")
          {              
              if(isset($postData['insurance_id']) &&  $postData['insurance_id']!="" && isset($postData['insurance_provider']) &&  $postData['insurance_provider']!="" )
              {
                $insurance_id=trim($this->post('insurance_id')) ;
                $insurance_provider= trim($postData["insurance_provider"]) ;
                $insertArr = array(                        
                          "full_name"   =>trim($postData["fullname"]), 
                          "date_of_birth" =>trim($postData["date_of_birth"]),            
                          "phone"        =>trim($postData["phone"]),
                          "email"        =>trim($postData["email"]), 
                          "country" => trim($postData["country_id"]),  
                          "county" => trim($postData["county"]), 
                          "city"   =>trim($postData["city_id"]),
                          "street" =>trim($postData["street"]),
                          "state"  =>trim($postData["state_id"]),  
                          "zipcode" => trim($postData["zipcode"]),
                          "personal_doc" => $doc,  
                          "insurance_id"    =>$insurance_id,                    
                          "signature" => trim($postData["signature"]),
                          "emergency_contactno"=>trim($postData["emergency_contactno"]),
                          "emergency_contactname"=>trim($postData["emergency_contactname"]),
                          "insurance_provider"=>$insurance_provider,
                           "done" =>1                              
                          );
              }
              else
              {
                 return $this->response(array("status"=>200,"statusCode"=>false, "msg" => "NEMT user required insurance id and insurnce insurance provider"), 200);
              }        
          }

          if( $postData['usertype']=="1")
          {           
                    $insertArr = array(                        
                          "full_name"   =>trim($postData["fullname"]), 
                          "date_of_birth" =>trim($postData["date_of_birth"]),            
                          "phone"        =>trim($postData["phone"]),
                          "email"        =>trim($postData["email"]), 
                          "country" => trim($postData["country_id"]),  
                          "county" => trim($postData["county"]), 
                          "city"   =>trim($postData["city_id"]),
                          "street" =>trim($postData["street"]),
                          "state"  =>trim($postData["state_id"]),  
                          "zipcode" => trim($postData["zipcode"]),
                          "personal_doc" => $doc,                         
                          "signature" => trim($postData["signature"]),
                          "emergency_contactno"=>trim($postData["emergency_contactno"]),
                          "emergency_contactname"=>trim($postData["emergency_contactname"]),
                          "done"=>1                                                
                          );                  
          }

          $insertId = $this->common->update(TB_USERS_NEW,array('id'=>$userid),$insertArr);
          if($insertId){
          $users = $this->common_model->select("*",TB_USERS_NEW,array("id"=>$userid));
          $country = $this->common_model->select("country_name,id",TB_COUNTRY,array('id' =>$users[0]["country"]));
          $state = $this->common_model->select("state_name,id",TB_STATE,array('id' =>$users[0]["state"]));
          $city = $this->common_model->select("city_name,id",TB_CITY,array('id' =>$users[0]["city"]));
          $insurancelist = $this->common_model->select("insurance_provider ,insurance_id",TB_INSURANCELIST,array('insurance_id'=>$insurance_provider));

                    if($users[0]['user_type']=="1")
                    {           
                      $Member = array(                        
                      "full_name"   =>$users[0]["full_name"], 
                      "date_of_birth" =>$users[0]["date_of_birth"],            
                      "phone"        =>$users[0]["phone"],
                      "email"        =>$users[0]["email"], 
                      "country" =>$country[0],  
                      "county" =>$users[0]["county"], 
                      "city"   =>$city[0],                     
                      "state"  =>$state[0],  
                       "street" =>$users[0]["street"],
                      "zipcode" =>$users[0]["zipcode"],
                      "personal_doc" =>$users[0]["personal_doc"],                         
                      "signature" => $users[0]["signature"],
                      "emergency_contactno"=>$users[0]["emergency_contactno"],
                      "emergency_contactname"=>$users[0]["emergency_contactname"],

                      );                  
                    }
                    if($users[0]['user_type']=="2")
                    {           
                      $Member = array(                        
                      "full_name"   =>$users[0]["full_name"], 
                      "date_of_birth" =>$users[0]["date_of_birth"],            
                      "phone"        =>$users[0]["phone"],
                      "email"        =>$users[0]["email"], 
                      "country" =>$country[0],  
                      "county" =>$users[0]["county"], 
                      "city"   =>$city[0],
                      "state"  =>$state[0],  
                      "zipcode" =>$users[0]["zipcode"],
                       "street" =>$users[0]["street"],
                      "personal_doc" =>$users[0]["personal_doc"],                         
                      "signature" => $users[0]["signature"],
                      "emergency_contactno"=>$users[0]["emergency_contactno"],
                      "emergency_contactname"=>$users[0]["emergency_contactname"],
                      "insurance_id"=>$users[0]["insurance_id"],
                      "insurance_provider"=>$insurancelist[0]                                         
                      );                  
                    }
            $this->response(array("status"=>200,"statusCode"=>true,  "message"=> "Your profile has been updated successfully.","data"=> $Member), 200); 
            }
      }
    }
    else
    {
     return $this->response(array("status"=>200,"statusCode"=>false, "msg" => "vaild filed"), 200);
    }   
  }


  /*  user details  */
  public function userinfo_post()
  {  
    $postData = $_POST;
    $postData = $this->input->post(); 
    if (isset($_POST['userid']) && $_POST['userid'] != "")
    {
      $userid = $this->post('userid');
      $userdata = $this->common_model->select("`id`,`email`,`password`,`phone`",TB_USERS_NEW,array("id"=>trim($userid)));
      if(count($userdata)==0) {                      
      $this->response(array("status"=>200,"statusCode"=>false, "message"=> "user not found."), 200);
      }
      else
       {       

         $country_id =$this->post('country_id');
         $users = $this->common_model->select("*",TB_USERS_NEW,array('id'=>$userid));            
         $base_url = $this->config->item('base_url'); 
         $key =0;
         foreach($users as $key => $value)
          {       
          $country = $this->common_model->select("id,country_name",TB_COUNTRY,array('id'=>$users[$key]["country"]));
          $country=array("id"=>$country[0]["id"] ,"country_name"=>$country[$key]["country_name"]);       
          $state = $this->common_model->select("id,state_name",TB_STATE,array('id'=>$users[$key]["state"]));
          $state=array("id"=>$state[0]["id"] ,"state_name"=>$state[0]["state_name"]);
          $city = $this->common_model->select("city_name,id",TB_CITY,array('id' =>$users[0]["city"]));
            if($users[$key]["user_type"]==2)
            {
              $insurancelist = $this->common_model->select("insurance_provider ,insurance_id",TB_INSURANCELIST,array('insurance_id'=>$users[$key]['insurance_provider'])); 
             $userinfo = array(
                              "fullname" => $users[$key]["full_name"],
                              "email" => $users[$key]["email"],
                              "phone" => $users[$key]["phone"],                        
                              "date_of_birth" => $users[$key]["date_of_birth"],
                              "zipcode" => $users[$key]["zipcode"],
                              "picture" =>$base_url.$users[$key]["picture"],
                              "zipcode" => $users[$key]["zipcode"],             
                              "emergency_contactno" => $users[$key]["emergency_contactno"],
                              "emergency_contactname" => $users[$key]["emergency_contactname"],
                              "signature" =>$users[$key]["signature"],
                              "insurance_provider" =>$insurance_provider,
                              "insurance_provider_id" => $insuranceid,
                              "personal_doc" =>$base_url.$users[$key]["personal_doc"],
                              "user_type" =>$users[$key]["user_type"],
                              "insurance_id" =>$users[$key]["insurance_id"],                            
                              "id" => $users[$key]["id"],
                              "country" =>$country,  
                              "county" => $users[$key]["county"], 
                              "city"   =>$city,
                              "street" =>$users[$key]["street"],
                              "state"  =>$state ,
                              "insurancelist" =>$insurancelist              
                               );
            }
            if($users[$key]["user_type"]==1)   
            {
             $userinfo = array(
                              "fullname" => $users[$key]["full_name"],
                              "email" => $users[$key]["email"],
                              "phone" => $users[$key]["phone"],                        
                              "date_of_birth" => $users[$key]["date_of_birth"],
                              "zipcode" => $users[$key]["zipcode"],
                              "picture" =>$base_url.$users[$key]["picture"],
                              "zipcode" => $users[$key]["zipcode"],             
                              "emergency_contactno" => $users[$key]["emergency_contactno"],
                              "emergency_contactname" => $users[$key]["emergency_contactname"],
                              "signature" =>$users[$key]["signature"],
                              "insurance_provider" =>$insurance_provider,
                              "insurance_provider_id" => $insuranceid,
                              "personal_doc" =>$base_url.$users[$key]["personal_doc"],
                              "user_type" =>$users[$key]["user_type"],                                            
                              "id" => $users[$key]["id"],
                              "country" =>$country,  
                              "county" => $users[$key]["county"], 
                              "city"   =>$city ,
                              "street" =>$users[$key]["street"],
                              "state"  =>$state               
                               );
            }
             if($users[$key]["user_type"]==3)   
            {
             $userinfo = array(
                              "fullname" => $users[$key]["full_name"],
                              "email" => $users[$key]["email"],
                              "phone" => $users[$key]["phone"],                        
                              "date_of_birth" => $users[$key]["date_of_birth"],
                              "zipcode" => $users[$key]["zipcode"],
                              "picture" =>$base_url.$users[$key]["picture"],                            
                              "personal_doc" =>$base_url.$users[$key]["personal_doc"],
                              "user_type" =>$users[$key]["user_type"],                                            
                              "id" => $users[$key]["id"],
                              "country" =>$country,  
                              "county" => $users[$key]["county"], 
                              "city"   =>$city ,
                              "street" =>$users[$key]["street"],
                              "state"  =>$state,
                              "company_name" => $users[$key]["company_name"],             
                              "company_address" => $users[$key]["company_address"],
                              "company_contactno" => $users[$key]["company_contactno"],
                              "vehicle_doc" =>$base_url.$users[$key]["vehicle_doc"],
                              "paratrasmit" =>$users[$key]["paratrasmit"],                            
                               );
            }


            return $this->response(array("status"=>200,"statusCode"=>true, "userinfo" =>$userinfo), 200);
        }
      }

    }else
      {
      return $this->response(array("status"=>200,"statusCode"=>false, "msg" => "missing user id"), 200);
      }       
  }


/*   edit profile pic*/
  public function uploadUserPic_post()
  {
    $postData = $_POST;
    $base_url = $this->config->item('base_url');         
    if ($postData["userid"] != '')
    {
      $postData = $this->input->post(); 
      $userid = $this->post('userid');
      $userdata = $this->common_model->select("`id`,`email`,`password`,`phone`,`user_type`",TB_USERS_NEW,array("id"=>trim($userid)));
      if(count($userdata)==0) {                      
      $this->response(array("status"=>200,"statusCode"=>false, "message"=> "user not found."), 200);
      }
      else
      {        if(isset($_FILES["picture"]["name"]))
                {
                
                   $usertype = $userdata[0]["user_type"];  
                   $isValid = $this->upload_profile_image($usertype, $_FILES);
                   //print_r($isValid['data']['new_image']);die;
                    if($isValid["status"] == 'success')
                    {           
                        $insertArr = array(                                
                        "picture"=>$isValid['data']['new_image']                             
                        );
                        $insertId = $this->common->update(TB_USERS_NEW,array('id'=>$userid),$insertArr);
                       // echo $this->db->last_query();die;
                        if($insertId){
                        $this->response(array("status"=>200,"statusCode"=>true, "message"=> "Your profile picture has been updated successfully.","picture"=> $base_url.$isValid['data']['new_image']), 200); 
                        }else{
                        $this->response(array("status"=>200,"statusCode"=>false, "message"=>"Please upload valid profile picture"), 200);
                        }                                                     
                    }
                    else
                    {
                    echo $isValid;
                    }
                }
                else
                {
                      $this->response(array("status"=>200,"statusCode"=>false, "message"=> "Missing profile picture"), 200);
                }
      }
    }
    else
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=> "missing user id"), 200); 
    }   

  }


 /* country list */
  public function country_get() 
  {
        $country = $this->common_model->select("country_name,id",TB_COUNTRY);   
         if( count($country) >0)
        {        
          foreach($country as $key => $value)
          {
          $country[] = array("id" => $country[$key]["id"],"country_name" => $country[$key]["country_name"]); 
          $this->response(array("status"=>200,"statusCode"=>true,"country"=>$country), 200); 
          }
               
        }
       else
        {
        $this->response(array("status"=>200,"statusCode"=>false, "message"=> "Data not found"), 200); 
        }      
  }


  /* state list */
  public function state_post() 
  {
       $postData = $_POST;
        if (isset($_POST['country_id']) && $_POST['country_id'] != "")
        {
            $country_id = $this->post('country_id');
            $statelist = $this->common_model->select("state_name,id",TB_STATE,array("country_id"=>trim($country_id)));   
            if( count($statelist) >0)
            {        
              foreach($statelist as $key => $value)
              {
              $state[] = array("id" => $statelist[$key]["id"],"state_name" => $statelist[$key]["state_name"]); 
               
              }
              $this->response(array("status"=>200,"statusCode"=>true,"state"=>$state), 200);           
            }
            else
            {
            $this->response(array("status"=>200,"statusCode"=>false, "message"=> "Data not found"), 200); 
            }   
        }
        else
        {
        $this->response(array("status"=>200,"statusCode"=>false, "message"=> "country id not found"), 200); 
        }
  }


/* city name */
  public function city_post() 
  {
       $postData = $_POST;
        if (isset($_POST['state_id']) && $_POST['state_id'] != "")
        {
            $state_id = $this->post('state_id');
            $citylist = $this->common_model->select("city_name,id",TB_CITY,array("id"=>trim($state_id)));   
            if( count($citylist) >0)
            {        
              foreach($citylist as $key => $value)
              {
              $citylist = array("id" => $citylist[$key]["id"],"city_name" => $citylist[$key]["city_name"]); 
              $this->response(array("status"=>200,"statusCode"=>true,"city"=>$citylist), 200); 
              }           
            }
            else
            {
            $this->response(array("status"=>200,"statusCode"=>false, "message"=> "Data not found"), 200); 
            }   
        }
        else
        {
        $this->response(array("status"=>200,"statusCode"=>false, "message"=> "country id not found"), 200); 
        }
  }


  /*  reset password*/
  public function resetPassword_post()
  {
     $postData = $_POST;
        if ($postData['userid'] == ""  || $postData['oldpassword'] == "" || $postData['newpassword'] == "" || $postData['cpassword'] == "")
        {
          $this->response(array("status"=>200,"statusCode"=>false, "message"=> "all fild are required"), 200);   
        }
        else
        {
            $emailexist = $this->common_model->select("id",TB_USERS_NEW,array("email"=>md5($postData['oldpassword']),"id"=>trim($postData["userid"]))); 
            if(count($emailexist)==0) {
                $this->response(array("status"=>200, "statusCode"=>false, "message"=> "wrong old password or user not found."), 200);
            }  
            else
            {
                $insertArr = array(
                "password" =>md5($postData['newpassword']),       
                );
                $insertId = $this->common->update(TB_USERS,array('id'=>$userid),$insertArr);
                if($insertId){
                $this->response(array("status"=>200, "statusCode"=>true, "message"=> "Your password has been changed successfully."), 200); 
                }
                else{
                $this->response(array("status"=>200, "statusCode"=>false, "message"=> "Some issue in change password."), 200); 
                }          
            }
        }
  }


/*   Register NEMT/member*/
public function registerDriver_post()
{
  $postData = $_POST;
  $postData = $this->input->post(); 
  $reset_token = random_string('unique');
  $usertype = trim($this->post('usertype'));
  $base_url = $this->config->item('base_url');
  $reset_url = $base_url."accountverify/".$reset_token;
 // echo "hgjkhk" ;die;

		if ($postData["email"] != '' && $_FILES["personal_doc"]["name"]!=''  &&  $postData["usertype"] != '' &&  $postData["password"] != '' && $postData["phone"] != '')
		{
		    $emailexist = $this->common_model->select("email",TB_USERS,array("email"=>trim($postData["email"]) , "user_type"=>trim($postData["usertype"])));  
		   //echo $this->db->last_query();die;
		    if(count($emailexist)>1) {
		    $this->response(array("status"=>200, "statusCode"=>false, "message"=> "User already exist with this email id."), 200);
		    }  
		    else
		    {
				if(isset($_FILES["personal_doc"]["name"]) && $_FILES["personal_doc"]["name"]!="")
				{
				   $isValid = $this->upload_profile_bio($postData, $_FILES);
				
				    if ($isValid["status"] == 'success')
				    {         
				        $file_name = $isValid['data']['file_name'];				                 
				        $doc = 'uploads/Driver/doc/'.$file_name; 
				    }
				    else
				    {
				    echo $isValid;
				    }
				}
				else
				{
				    $this->response(array("status"=>200,"statusCode"=>false, "message"=> "personal document missing"), 200);
				}

	            if(isset($postData['operate_own_vehicle_ownbusiness']) && $postData['operate_own_vehicle_ownbusiness']=="Yes")
	            {
	               if(isset($_FILES["operate_own_vehicle_ownbusiness_doc"]["name"]) && $_FILES["operate_own_vehicle_ownbusiness_doc"]["name"]!="" )
					{			
						$operate_own_vehicle_comy="No";
						$operate_comy_vehicle="No";
						$operate_own_vehicle_ownbusiness=$postData['operate_own_vehicle_ownbusiness'];
					    $isValid = $this->upload_profile_bio1($postData, $_FILES);					
					    if ($isValid["status"] == 'success')
					    {      
					        $file_name = $isValid['data1']['file_name'];			                 
					        $vehicle_doc = 'uploads/Driver/doc/'.$file_name;					   			                 
					    }
					    else
					    {
					    echo $isValid;
					    }
					}					
				}
	            else if(isset($postData['operate_own_vehicle_comy']) && $postData['operate_own_vehicle_comy']=="Yes")
	            {
	                 if(isset($_FILES["own_vehicle_comy_doc"]["name"])  && $_FILES["own_vehicle_comy_doc"]["name"]!="" )
					{
					   $operate_own_vehicle_comy=$postData['operate_own_vehicle_comy'];
						$operate_comy_vehicle="No";
						$operate_own_vehicle_ownbusiness="No";
					    $isValid = $this->upload_profile_bio1($postData, $_FILES);					  
					    if ($isValid["status"] == 'success')
					    {         
					        $file_name = $isValid['data1']['file_name'];				                 
					        $vehicle_doc = 'uploads/Driver/doc/'.$file_name; 					   		                 
					    }
					    else
					    {
					    echo $isValid;
					    }
					}			

				}
	           else if(isset($postData['operate_comy_vehicle']) && $postData['operate_comy_vehicle']=="Yes")
	            {
					 if(isset($_FILES["comy_vehicle_doc"]["name"]) && $_FILES["comy_vehicle_doc"]["name"]!="" )				
					{
						$operate_own_vehicle_comy="No";
						$operate_comy_vehicle=$postData['operate_comy_vehicle'];
						$operate_own_vehicle_ownbusiness="No";
					   $isValid = $this->upload_profile_bio1($postData, $_FILES);
					 
					    if ($isValid["status"] == 'success')
					    {         
					        $file_name = $isValid['data1']['file_name'];				                 
					        $vehicle_doc = 'uploads/Driver/doc/'.$file_name; 					  			                 
					    }
					    else
					    {
					    echo $isValid;
					    }
					}					
				}
				else
				{
               $this->response(array("status"=>200,"statusCode"=>false, "message"=> "vehicle document missing"), 200);
				}

		    	$insertArr = array("user_type"=>$usertype,
                            "user_token" => mt_rand(100000, 999999),
                            "password"     =>md5(trim($postData["password"])),
                            "full_name"   =>trim($postData["fullname"]), 
                            "date_of_birth" =>trim($postData["date_of_birth"]),            
                            "phone"        =>trim($postData["phone"]),
                            "email"        =>trim($postData["email"]),                          
                            "status"=>"inactive",
                            "token"=>$reset_token,
                            "device_type"=>trim($postData["device_type"]),                                           
                            "country" => trim($postData["country_id"]),  
                            "county" => trim($postData["county"]), 
                            "city"   =>trim($postData["city_id"]),
                            "street" =>trim($postData["street"]),
                            "state"  =>trim($postData["state_id"]),  
                            "zipcode" => trim($postData["zipcode"]),
                            "company_name" =>trim($postData["company_name"]),
                            "company_address"  =>trim($postData["company_address"]),  
                            "company_contactno"  =>trim($postData["company_contactno"]),  
                            "personal_doc" => $doc, 
                            "vehicle_doc"=>$vehicle_doc,
							"operate_own_vehicle_ownbusiness" =>$operate_own_vehicle_ownbusiness,							
							"paratrasmit"=>trim($postData["paratrasmit"]),  
							"operate_comy_vehicle" =>$operate_comy_vehicle,
							"operate_own_vehicle_comy" =>$operate_own_vehicle_comy						
                            );

			  $user = $this->common_model->insert(TB_USERS,$insertArr);
               //print_r($user);die;

			       if($user)
                                {
                                $to_email = $postData["email"];
                                $hostname = $this->config->item('hostname');  
                                $config['mailtype'] ='html';
                                $config['charset'] ='iso-8859-1';
                                $this->email->initialize($config);
                                $from  = EMAIL_FROM; 
                                $this->messageBody  = email_header();
                                $this->messageBody  .= '<tr>
                <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;height:200px;vertical-align:top;">
                    <p>Dear Customer,</p>
                        <p>
                        You are registered successfully on Transcura as a driver.<br>
                        Please wait while administrator activates your account. </p>
                       
                    </td>
                </tr>               
               ';                      
                                $this->messageBody  .= email_footer();        //print_r( $this->messageBody);die;
                                $this->email->from($from, $from);
                                $this->email->to($to_email);
                                $this->email->subject('Welcome to Transcura');
                                $this->email->message($this->messageBody);
                                $this->email->send();
                                $this->response(array("status"=>200,"message"=> "Registration Successful. Your account will be activated once the Admin approves it.","statusCode"=>true), 200);  
                                }                       
        }            

		}
		else
		{
		$this->response(array("status"=>200,"statusCode"=>false, "message"=> "all fields are mandatory"), 200);
		}
}



/*   Register NEMT/member*/
public function EditDriver_post()
{
  $postData = $_POST;
  $postData = $this->input->post(); 
  $reset_token = random_string('unique');
  $usertype = trim($this->post('usertype'));
  $base_url = $this->config->item('base_url');
 $userid= $postData["userid"];

	if ($postData["userid"] != '' &&  $postData["email"] != '' &&  $postData["usertype"] != '' && $postData["phone"] != '')
		{		    
				if(isset($_FILES["personal_doc"]["name"]) && $_FILES["personal_doc"]["name"]!="")
				{
				   $isValid = $this->upload_profile_bio($postData, $_FILES);				
				    if ($isValid["status"] == 'success')
				    {         
				        $file_name = $isValid['data']['file_name'];				                 
				        $doc = 'uploads/Driver/doc/'.$file_name; 
				    }
				    else
				    {
				    echo $isValid;
				    }
				}
				else
				{
				    $doc = $postData["personal_doc_old"];
				}

                if(isset($_FILES["operate_own_vehicle_ownbusiness_doc"]["name"]) && $_FILES["operate_own_vehicle_ownbusiness_doc"]["name"]!="" )
					{			
						$operate_own_vehicle_comy="No";
						$operate_comy_vehicle="No";
						$operate_own_vehicle_ownbusiness=$postData['operate_own_vehicle_ownbusiness'];
					    $isValid = $this->upload_profile_bio1($postData, $_FILES);					
					    if ($isValid["status"] == 'success')
					    {      
					        $file_name = $isValid['data1']['file_name'];			                 
					        $vehicle_doc = 'uploads/Driver/doc/'.$file_name;					   			                 
					    }
					    else
					    {
					    echo $isValid;
					    }
					}	
					else
					{
						$vehicle_doc = $postData["vehicle_doc_old"];	
					}

				            
	            if(isset($_FILES["own_vehicle_comy_doc"]["name"]) && $_FILES["own_vehicle_comy_doc"]["name"]!="" )
					{
					    $operate_own_vehicle_comy=$postData['operate_own_vehicle_comy'];
						$operate_comy_vehicle="No";
						$operate_own_vehicle_ownbusiness="No";
					    $isValid = $this->upload_profile_bio1($postData, $_FILES);					  
					    if ($isValid["status"] == 'success')
					    {         
					        $file_name = $isValid['data1']['file_name'];				                 
					        $vehicle_doc = 'uploads/Driver/doc/'.$file_name; 					   		                 
					    }
					    else
					    {
					    echo $isValid;
					    }
					}
					else
					{
						$vehicle_doc = $postData["vehicle_doc_old"];	
					}
				
           
					 if(isset($_FILES["comy_vehicle_doc"]["name"])  && $_FILES["comy_vehicle_doc"]["name"]!="" )				
					{
						$operate_own_vehicle_comy="No";
						$operate_comy_vehicle=$postData['operate_comy_vehicle'];
						$operate_own_vehicle_ownbusiness="No";
					    $isValid = $this->upload_profile_bio1($postData, $_FILES);					 
					    if ($isValid["status"] == 'success')
					    {         
					        $file_name = $isValid['data1']['file_name'];				                 
					        $vehicle_doc = 'uploads/Driver/doc/'.$file_name; 					  			                 
					    }
					    else
					    {
					    echo $isValid;
					    }
					}
					else
					{
						$vehicle_doc = $postData["vehicle_doc_old"];	
					}

				
	                    	$insertArr = array("user_type"=>$usertype,                                                   
                            "full_name"   =>trim($postData["fullname"]), 
                            "date_of_birth" =>trim($postData["date_of_birth"]),            
                            "phone"      =>trim($postData["phone"]),
                            "email"      =>trim($postData["email"]),                                                      
                            "country" => trim($postData["country_id"]),  
                            "county" => trim($postData["county"]), 
                            "city"   =>trim($postData["city_id"]),
                            "street" =>trim($postData["street"]),
                            "state"  =>trim($postData["state_id"]),  
                            "zipcode" => trim($postData["zipcode"]),
                            "company_name" =>trim($postData["company_name"]),
                            "company_address"  =>trim($postData["company_address"]),  
                            "company_contactno"  =>trim($postData["company_contactno"]),  
                            "personal_doc" => $doc, 
                            "vehicle_doc"=>$vehicle_doc,
							"operate_own_vehicle_ownbusiness" =>trim($postData["operate_own_vehicle_ownbusiness"]),				
							"paratrasmit"=>trim($postData["paratrasmit"]),  
							"operate_comy_vehicle" =>trim($postData["operate_comy_vehicle"]),
							"operate_own_vehicle_comy" =>trim($postData["operate_own_vehicle_comy"]),
							"done"=>1					
                            );

					$insertId = $this->common->update(TB_USERS,array('id'=>$userid),$insertArr);
					//	echo $this->db->last_query();die;
					if($insertId){
					$users = $this->common_model->select("*",TB_USERS,array("id"=>$userid));
					$country = $this->common_model->select("country_name,id",TB_COUNTRY,array('id' =>$users[0]["country"]));
					$state = $this->common_model->select("state_name,id",TB_STATE,array('id' =>$users[0]["state"]));
					$city = $this->common_model->select("city_name,id",TB_CITY,array('id' =>$users[0]["city"]));     

                      $driver = array(                        
                      "full_name"   =>$users[0]["full_name"], 
                      "date_of_birth" =>$users[0]["date_of_birth"],            
                      "phone"        =>$users[0]["phone"],
                      "email"        =>$users[0]["email"], 
                      "country" =>$country[0],  
                      "county" =>$users[0]["county"], 
                      "city"   =>$city[0],
                      "state"  =>$state[0],  
                      "zipcode" =>$users[0]["zipcode"],
                      "street" =>$users[0]["street"],
                      "personal_doc" =>$users[0]["personal_doc"],
                      "vehicle_doc" =>$base_url.$users[0]["vehicle_doc"],                                          
                      "company_name"=>$users[0]["company_name"],
                      "company_address"=>$users[0]["company_address"],
                      "company_contactno"=>$users[0]["company_contactno"],
                      "operate_comy_vehicle"=>$users[0]["operate_comy_vehicle"],
                      "operate_own_vehicle_ownbusiness"=>$users[0]["operate_own_vehicle_ownbusiness"],
                      "operate_own_vehicle_comy"=>$users[0]["operate_own_vehicle_comy"]
                       );               
                   
                $this->response(array("status"=>200,"statusCode"=>true,  "message"=> "Your profile has been updated successfully.","data"=> $driver), 200); 
            }

		}
		else
		{
		$this->response(array("status"=>200,"statusCode"=>false, "message"=> "all fields are mandatory"), 200);
		}
}

/* Shedule api's  */


 public function scheduleUser_post()
{
     $postData = $_POST;
     $postData = $this->input->post();     
     $base_url = $this->config->item('base_url');
     $userid = $postData["userid"];
    if($postData["userid"] == '' || $postData["sr_date"]==''  || $postData["sr_pick_up_time"]=='' || $postData["sr_appointment_time"] == '' || $postData["sr_street"]==''|| $postData["sr_state"] == ''|| $postData["sr_country"] == '' || $postData["sr_zipcode"]=='' || $postData["sr_destination"]=='' || $postData["sr_special_request"]==''|| $postData["sr_desire_transportaion"]==''  )
    {
    $this->response(array("status"=>200, "statusCode"=>false, "message"=> "All Filds required."), 200);
    }
    else
    {
        $userexit = $this->common_model->select("*",TB_USERS,array("id"=>trim($postData["userid"])));
     //   echo $this->db->last_query();die;
          if(count($userexit)>0){
                           $insertArr = array("user_id"=>$postData["userid"],
                            "sr_date" =>$postData["sr_date"],
                            "sr_appointment_time" =>trim($postData["sr_appointment_time"]),
                            "sr_street"   =>trim($postData["sr_street"]), 
                            "sr_country" =>trim($postData["sr_country"]),            
                            "sr_zipcode" =>trim($postData["sr_zipcode"]),
                            "sr_state"  =>trim($postData["sr_state"]),                          
                            "sr_destination"=>trim($postData["sr_destination"]),
                            "sr_special_request"=>trim($postData["sr_special_request"]),                          
                            "sr_desire_transportaion"=>trim($postData["sr_desire_transportaion"]),   
                            "sr_special_instruction"=>trim($postData["sr_special_instruction"]),
                            "sr_status"=>0,                                       
                       );
          $schedule = $this->common_model->insert(TB_SCHEDULE_RIDE,$insertArr); 
          //  echo $this->db->last_query();die;
          $this->response(array("status"=>200, "statusCode"=>true, "message"=> "Your schedule has been sent successfully"), 200);
          } 
          else
          {
          $this->response(array("status"=>200, "statusCode"=>false, "message"=> "User not found"), 200);
          }
    }

}

 public function scheduleUserdetail_post()
{
     $postData = $_POST;
     $postData = $this->input->post();     
     $base_url = $this->config->item('base_url');
     $userid = $postData["userid"];
    if($postData["userid"] == '' || $postData["schedule_ride_id"]=='')
    {
    $this->response(array("status"=>200, "statusCode"=>false, "message"=> "All Filds required."), 200);
    }
    else
    {
     $userexit = $this->common_model->select("*",TB_SCHEDULE_RIDE,array("id"=>trim($postData["schedule_ride_id"]) ,"user_id"=>trim($postData["userid"])));

        $country = $this->common_model->select("country_name,id",TB_COUNTRY,array('id' =>$userexit[0]["sr_country"]));
        $state = $this->common_model->select("state_name,id",TB_STATE,array('id' =>$userexit[0]["sr_state"]));
        $sr_destination = $this->common_model->select("*",TB_DESTINATION,array('id' =>$userexit[0]["sr_destination"]));
        $sr_special_request = $this->common_model->select("sr_name as request,id",TB_SPECIAL_REQUEST,array('id' =>$userexit[0]["sr_special_request"]));
        $sr_desire_transportaion = $this->common_model->select("dt_name as trasportation,id",TB_DESIRE_TRANSPORTATION,array('id' =>$userexit[0]["sr_desire_transportaion"]));

     
         if(count($userexit)>0){        

                 $schedule = array(                        
                      "sr_date"   =>$userexit[0]["sr_date"], 
                      "sr_pick_up_time" =>$userexit[0]["sr_pick_up_time"], 
                      "sr_appointment_time" =>$userexit[0]["sr_appointment_time"], 
                      "sr_special_instruction" =>$userexit[0]["sr_special_instruction"],
                      "sr_destination" =>$sr_destination[0],
                      "sr_special_request" =>$sr_special_request[0],                      
                      "sr_desire_transportaion" =>$sr_desire_transportaion[0],
                      "state" =>$state[0],  
                      "country" =>$country[0],  
                      "sr_zipcode" =>$userexit[0]["sr_zipcode"], 
                  
                       );      


           $this->response(array("status"=>200, "statusCode"=>true, "schedule"=> $schedule), 200);
       }
          else
          {
          $this->response(array("status"=>200, "statusCode"=>false, "message"=> "User not found"), 200);
          }
    }




} 
  





}