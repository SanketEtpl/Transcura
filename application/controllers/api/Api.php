  <?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  // This can be removed if you use __autoload() in config.php OR use Modular Extensions
  /** @noinspection PhpIncludeInspection */
  require APPPATH . '/libraries/REST_Controller.php';

  // use namespace
  use Restserver\Libraries\REST_Controller;
  /**
   * This is an example of a few basic user interaction methods you could use
   * all done with a hardcoded array
   *
   * @package         CodeIgniter
   * @subpackage      Rest Server
   * @category        Controller
   * @author          Phil Sturgeon, Chris Kacerguis
   * @license         MIT
   * @link            https://github.com/chriskacerguis/codeigniter-restserver
   */
  class Api extends REST_Controller {

    function __construct()
    {
          // Construct the parent class
      parent::__construct();
      $this->load->database();
      $this->load->model("common_model");
      $this->load->library("email");
      $this->load->library("User_AuthSocial");
      //$this->load->library("userauth");
      $this->load->helper("email_template");
      $this->load->helper('string');
      $this->load->library('upload');
      $this->load->library("image_lib"); 
      $this->load->helper('security');     
      $this->pro_profile_origin =  realpath('uploads/Patients/original');
      $this->pro_profile_thumb =  realpath('uploads/Patients/thumb');
      $this->pro_profile_origin1 =  realpath('uploads/Driver/original');
      $this->personal_doc_path = realpath('./uploads/personal_doc');
      $base_url = $this->config->item('base_url');

    }

    /*   Register NEMT/member*/
  /*public function registerUser_post()
  {
    $postData = $_POST;
    $postData = $this->input->post(); 
    $reset_token = random_string('unique');
    $usertype = trim($this->post('usertype'));
    $base_url = $this->config->item('base_url');
    $reset_url = $base_url."accountverify/".$reset_token;

   if ($postData["email"] != ''  &&  $postData["usertype"] != '' &&  $postData["password"] != '')
   {
          $emailexist = $this->common_model->select("email",TB_USERS_NEW,array("email"=>trim($postData["email"]) , "user_type"=>trim($postData["usertype"])));  
          if(count($emailexist)>0) {
          $this->response(array("status"=>200, "statusCode"=>false, "message"=> "User already exist with this email id."), 200);
          }  
          else
          {
                  if(isset($_FILES["personal_doc"]["name"]))
                  {
                     $isValid = $this->upload_profile_bio($postData, $_FILES);
                      if ($isValid["status"] == 'success')
                      {         
                          $file_name = $isValid['data']['file_name']; 
                          if($postData['usertype']==2) 
                          {         
                          $doc = 'uploads/Patients/doc/'.$file_name; 
                          }
                          if($postData['usertype']==1) 
                          {         
                          $doc = 'uploads/Member/doc/'.$file_name; 
                          }
                                    
                      }
                      else
                      {
                      echo $isValid;
                      }
                  }
                  else
                  {
                        $this->response(array("status"=>200,"statusCode"=>false, "message"=> "personal document missing"), 200);
                  }

                  if(isset($postData["usertype"]) &&  $postData['usertype']=='2')
                  { 
                      $insurance_id = trim($postData["insurance_id"]);
                      // if(isset($postData["insurance_id"]) && $postData["insurance_id"]!="")
                      // {
                      //   $insurance_id = trim($postData["insurance_id"]);
                      // }               
                      // else
                      // {
                      //  $this->response(array("status"=>200,"statusCode"=>false, "message"=> "NEMT user insurance id  missing"), 200); 
                      // }
                  
                  } 
                  else if(isset($postData["usertype"]) && $postData['usertype']=='1')
                  {                
                    $insurance_id = 0;                   
                  } 

                    $insertArr = array("user_type"=>$usertype,
                              "user_token" => mt_rand(100000, 999999),
                              "password"     =>md5(trim($postData["password"])),
                              "full_name"   =>trim($postData["fullname"]), 
                              "date_of_birth" =>trim($postData["date_of_birth"]),            
                              "phone"        =>trim($postData["phone"]),
                              "email"        =>trim($postData["email"]),                          
                              "status"=>"inactive",
                              "token"=>$reset_token,
                              "device_type"=>trim($postData["device_type"]),                                           
                              "country" => trim($postData["country_id"]),  
                              "county" => trim($postData["county"]), 
                              "city"   =>trim($postData["city_id"]),
                              "street" =>trim($postData["street"]),
                              "state"  =>trim($postData["state_id"]),  
                              "zipcode" => trim($postData["zipcode"]),
                              "personal_doc" => $doc,  
                              "insurance_id"    =>$insurance_id,                    
                              "signature" => trim($postData["signature"]),
                              "emergency_contactno"=>trim($postData["emergency_contactno"]),
                              "emergency_contactname"=>trim($postData["emergency_contactname"]),
                            );
                        

                    $user = $this->common_model->insert(TB_USERS_NEW,$insertArr);
                    //echo $this->db->last_query();die;                        
                    if($user) {
                                            $to_email =trim($postData["email"]);
                                            $hostname = $this->config->item('hostname');  
                                            $config['mailtype'] ='html';
                                            $config['charset'] ='iso-8859-1';
                                            $this->email->initialize($config);
                                            $from  = EMAIL_FROM; 
                                            $this->messageBody  = email_header();
                                     $this->messageBody  .= '<tr>
                                    <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                                        <p>Dear Customer,</p>
                                            <p>
                                            You are registered successfully on Transcura.<br>
                                            Please click the link below to verify your email address.</p>
                                            <p>
                                            <p>Activation Link : </p>
                                          <a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href='.$reset_url.'>Click Here</a>
                                          </p>
                                      </td>
                                  </tr>';
                                          $this->messageBody  .= email_footer();                        
                                          $this->email->from($from, $from);
                                          $this->email->to($to_email);
                                          $this->email->subject('Welcome to Transcura');
                                          $this->email->message($this->messageBody);
                                          $this->email->send();
                                          $this->response(array("status"=>200,"statusCode"=>true,"message"=> "Thank You! Please check your email to activate your account.","statusCode"=>"true"), 200);  
                            }
                        else
                        {
                       $this->response(array("status"=>200,"statusCode"=>false, "message"=> "some issue in registration"), 200);
                        }
        }

    }
    else
    {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=> "all fields are mandatory"), 200);
    }

  } */

  /* upload file */

  public function upload_personal_doc($postData, $FILES){
    $_FILES = $FILES; 
    $i=0;
    if (isset($_FILES['personal_doc']['name']) && $_FILES['personal_doc']['name'] != "") { 
      $config = array(
       'upload_path' => './uploads/personal_doc',
       'allowed_types' => 'gif|jpg|jpeg|png|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|exe',
       'max_width' => '6000',
       'max_height' => '5000',
       'encrypt_name' => TRUE
       );
      $this->load->library('upload', $config);
      $this->upload->initialize($config); 
      $check_upload = $this->upload->do_upload('personal_doc'); 
      if ($check_upload) {
       $uploaded = $this->upload->data(); 
  			return array("status" => "success", "data1" => $uploaded, "msg" => "Successfully added."); //200
  		} else {
  			$error = $this->upload->display_errors(); 
  			return $this->response(array("status" => "error", "msg" => $error), 200);
  		}
  	} else {
  		return $this->response(array("status" => "error", "msg" => "Data not found."), 200);
  	}
  }

  public function upload_profile_bio1($postData, $FILES)
  {
    $_FILES = $FILES;
    //print_r($_FILES);die;
    $i=0;
  	             // if($postData['usertype']==2) 
                //     {         
                //     $config['upload_path'] = "./uploads/Patients/doc";
                //     }
                //     if($postData['usertype']==1) 
                //     {         
                //     $config['upload_path'] = "./uploads/Member/doc";
                //     }
    if($postData['usertype']==3) 
    {         
      $config['upload_path'] = "./uploads/Driver/doc";
    }
    $config['allowed_types'] = "pdf|PDF";      
    $config['max_size'] = '102400000';
    $config['max_width'] = '200000000';
    $config['max_height'] = '1000000000000';
    $random = rand(1, 10000);
    $this->load->library('upload', $config);
    $this->upload->initialize($config); 

    if($postData['usertype']==3)
    {

      if(isset($_FILES['operate_own_vehicle_ownbusiness_doc']))
      {	
        $filename = $_FILES['operate_own_vehicle_ownbusiness_doc']['name']; 
        $config['file_name'] =$random."_".$filename;       
        $check_upload = $this->upload->do_upload("operate_own_vehicle_ownbusiness_doc");				
        if ($check_upload) {
         $uploaded1 = $this->upload->data();								
         return array("status" => "success","data1"=>$uploaded1, "msg" => "Successfully added"); 
       } else {
         $error = $this->upload->display_errors();
         return $this->response(array("status" => "error", "msg" => $error), 200);
       }
     }

     else if(isset($_FILES['comy_vehicle_doc']))
     {	
      $filename = $_FILES['comy_vehicle_doc']['name']; 
      $config['file_name'] =$random."_".$filename;            
      $check_upload = $this->upload->do_upload("comy_vehicle_doc");              
      if ($check_upload) {
        $uploaded1 = $this->upload->data();
        return array("status" => "success","data1"=>$uploaded1,"msg" =>"Successfully added"); 
      } else {
       $error = $this->upload->display_errors();
       return $this->response(array("status" => "error", "msg" => $error), 200);
     }
   }

   else if(isset($_FILES['own_vehicle_comy_doc']))
   {
    $filename = $_FILES['own_vehicle_comy_doc']['name']; 
    $config['file_name'] =$random."_".$filename;            
    $check_upload = $this->upload->do_upload("own_vehicle_comy_doc");	
    if ($check_upload) {
     $uploaded1 = $this->upload->data();
     return array("status" => "success","data1"=>$uploaded1,"msg" =>"Successfully added"); 
   } else {
     $error = $this->upload->display_errors();
     return $this->response(array("status" => "error", "msg" => $error), 200);
   }
  }

  }

  }


  public function upload_profile_bio($postData, $FILES)
  {
    $_FILES = $FILES;
    //print_r($_FILES);die;
    $i=0;
    if($postData['usertype']==2) 
    {         
      $config['upload_path'] = "./uploads/Patients/doc";
    }
    if($postData['usertype']==1) 
    {         
      $config['upload_path'] = "./uploads/Member/doc";
    }
    if($postData['usertype']==3) 
    {         
      $config['upload_path'] = "./uploads/Driver/doc";
    }
    $config['allowed_types'] = "pdf|PDF";      
    $config['max_size'] = '102400000';
    $config['max_width'] = '200000000';
    $config['max_height'] = '1000000000000';
    $random = rand(1, 10000);
    $this->load->library('upload', $config);
    $this->upload->initialize($config); 
    if(isset($_FILES['personal_doc']))
    {
      $filename = $_FILES['personal_doc']['name']; 
      $config['file_name'] =$random."_".$filename;            
      $check_upload = $this->upload->do_upload("personal_doc");			
  				      //print_r($uploaded1);die;
      if ($check_upload) {
       $uploaded1 = $this->upload->data();
       return array("status" => "success","data"=>$uploaded1, "msg" => "Successfully added"); 
     } else {
       $error = $this->upload->display_errors();
       return $this->response(array("status" => "error", "msg" => $error), 200);
     }
   }


  }


  public function upload_profile_image($type, $FILES)
  {
    $usertype = $type ;
    $_FILES = $FILES;  
    if (isset($_FILES['picture']['name']) && $_FILES['picture']['name'] != "") {
     $count = count($_FILES['picture']['name']);                     
     $filename = $_FILES['picture']['name'];
     if($usertype==2) 
     {         
      $config['upload_path'] = "./uploads/Patients/original";
    }
    if($usertype==1) 
    {         
      $config['upload_path'] = "./uploads/Member/original";
    }
    if($usertype==3) 
    {         
      $config['upload_path'] = "./uploads/Driver/original";
    }

    $base_url = $this->config->item('base_url');
    $config['allowed_types'] =   "png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG";  
                    $config['min_width']     = "125"; // new
                    $config['min_height']    = "70"; // new    
                    $random = rand(1, 10000);
                    $new_file = $random.$filename;  
                    $config['file_name'] = $new_file;     
                     //  print_r($config);die;          
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);
                    $check_upload = $this->upload->do_upload('picture');  
                    if ($check_upload) {
                      $uploaded = $this->upload->data(); 
                      $data = $this->resize($usertype,$uploaded);
                      return array("status" => "success", "data" => $data, "count" => $count, "msg" => "Successfully added"); 
                    } else {
                      $error = $this->upload->display_errors();
                      return $this->response(array("status" => "error", "msg" => $error), 200);
                    }
                  }
                  else {
                    return $this->response(array("status"=>200,"statusCode"=>false, "msg" => "document not found"), 200);
                  }
                }




                public function resize($type,$image_data) {

                  $userlist = $this->common_model->select("user_type",TB_USERS,array("id"=>$_REQUEST['userid']));
                  if( $type=='1')
                  {
                    $config['new_image'] ='uploads/Member/thumb/thumb_'.$image_data['raw_name'].$image_data['file_ext'];
                  }
                  if( $type=='2')
                  {
                    $config['new_image'] ='uploads/Patients/thumb/thumb_'.$image_data['raw_name'].$image_data['file_ext'];
                  }   
                  if( $type=='3')
                  {
                    $config['new_image'] ='uploads/Driver/thumb/thumb_'.$image_data['raw_name'].$image_data['file_ext'];
                  }       
                  $config['image_library'] = 'gd2';
                  $config['source_image'] = $image_data['full_path'];
                  $config['allowed_types'] = 'png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG';
                  $config['width'] = 350;
                  $config['height'] = 175;
                  $this->image_lib->initialize($config);
                  $src = $config['new_image'];
                  $data['new_image'] = substr($src, 0);
                  $data['img_src'] =$data['new_image'];
                  if (!$this->image_lib->resize())
                  {
                    $errror= $this->image_lib->display_errors(); 
                    return  $error;
                  }
                  else
                  {
                    return  $data;    

                  }
                  
                }



                /*   login NEMT/DRIVER/MEMBER using usertype */
                public function loginUser_post() {
                 $postData = $_POST;
                 $base_url = $this->config->item('base_url');
                 if ($postData["email"] != ''  &&  $postData["password"] != '' &&  $postData["usertype"] != '' &&  $postData["device_token"] != '' &&  $postData["device_type"] != '')
                 {

                  $this->db->select("*");
                  $this->db->from(TB_USERS);
                  $this->db->where("((email='".trim($this->post('email'))."' AND password='".md5(trim($this->post('password')))."') OR (username='".trim($this->post('email'))."' AND password='".md5(trim($this->post('password')))."')) AND user_type='".trim($this->post('usertype'))."'");
                  $res = $this->db->get();
        // $cond = array("email"=>trim($this->post('email')),"password"=>md5(trim($this->post('password'))));
        //     $result = $this->common_model->select("*",TB_USERS,$cond);
            // echo $this->db->last_query();die;
                  if($res->num_rows() >0) {
                    $result = $res->result_array();
                    $up_data = array("device_token"=>$postData['device_token'],"device_type"=>$postData['device_type']);
                    $where_up = array("id"=>$result[0]['id']);
                    $updateq = $this->common_model->update(TB_USERS,$where_up,$up_data); 

                    if($result[0]["status"]=='active')
                    { 
                      $userinfo = array(
                        "full_name" => $result[0]["full_name"],
                        "username" => $result[0]["username"],
                        "email" => $result[0]["email"],
                        "phone" => $result[0]["phone"],                            
                        "date_of_birth" => $result[0]["date_of_birth"],
                        "zipcode" => $result[0]["zipcode"],
                        "picture" =>  $base_url.$result[0]["picture"],
                        "zipcode" => $result[0]["zipcode"],
                        "city" => $result[0]["city"],                                                
                        "user_type" =>$result[0]["user_type"],
                        "done" =>$result[0]["done"],
                        "id" => $result[0]["id"],
                        "state" =>  $result[0]["state"],   
                        "street" =>  $result[0]["street"],
                        "country" => $result[0]["country"],
                        "county" => $result[0]["county"] 
                        );

                      $cardinfo=array("card_number"=>$result[0]['card_number'],"card_cvv"=>$result[0]['card_cvv'],"exp_month"=>$result[0]['exp_month'],"exp_year"=>$result[0]['exp_year']);
                      
                      $this->response(array("status"=>"success","userinfo"=>$userinfo,"cardinfo"=>$cardinfo,"message"=> ""), 200);
                    }else 
                    {
                      $this->response(array("status"=>"error","message"=> "User not activate."), 200);
                    }
                  }
                  else
                  {
                    $this->response(array("status"=>"error", "message"=> "Invalid username, password or usertype , Please try again."), 200);
                  }
                }
                else
                {
                  $this->response(array("status"=>"error", "message"=> "all fields are mandatory"), 200);
                }

              }


              /*   forgotpassword */
              public function forgotPassword_post() {      
                $postData = $_POST;
                $email = $this->post('email');
                $type = $this->post('type');

                if (!isset($_POST['email']) || $_POST['email'] == "" || !isset($_POST['type']) || $_POST['type'] == "") 
                {
                  $this->response(array("status"=>"error", "message"=> "Please enter valid email id and type."), 200);
                }else
                {        
                  $userdata = $this->common_model->select("`id`,`email`,`password`,`phone`,username",TB_USERS,array("email"=>trim($email),"status"=>"active"));             
                  if(count($userdata)==0)
                  {                      
                   $this->response(array("status"=>"error", "message"=> "The email address that you have provided is not associated with any of the active accounts."), 200);
                 }                  
                 else {
                  if($type == "password")
                  {
                   $userId = $userdata[0]['id'];
                   $reset_token = random_string('unique');
                   $userId = base64_encode($userId);
                   $reset_url = base_url()."frontend/Homecontroller/resetPassword/".$userId."/".$reset_token;                  
                   $useremail =trim($userdata[0]["email"]) ;               
                   $datareset = array('token' =>  $reset_token); 
                   $cond1 = array('email' =>  $useremail); 
                   $this->common_model->update(TB_USERS,$cond1,$datareset);       
                        //Send Reset password Link
                   $hostname = $this->config->item('hostname');
                   $config['mailtype'] ='html';
                   $config['charset'] ='iso-8859-1';
                   $this->email->initialize($config);
                   $from  = EMAIL_FROM; 
                   $this->messageBody  = email_header();
                   $this->messageBody  .= '<tr> 
                   <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                    <p>Hello,</p>
                    <p>
                      A request has been made to reset your Transcura account password.<br>
                      Please click the link below to reset your password.</p>
                      <p>
                        <p>Activation Link : </p>
                        <a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href="'.$reset_url.'">Click Here</a>
                      </p>
                    </td>
                  </tr>
                  <tr>';                      
                    $this->messageBody  .= email_footer();
                    $this->email->from($from, $from);
                    $this->email->to($email);
                    $this->email->subject('Reset Password Link');
                    $this->email->message($this->messageBody);
                    $this->email->send();         
                    $this->response(array("status"=>"success", "message"=> "A link to reset your password has been sent to this email address."), 200);
                  }else if($type == "username")
                  {
                    $usernamehere = $userdata[0]['username'];
                    $hostname = $this->config->item('hostname');
                    $config['mailtype'] ='html';
                    $config['charset'] ='iso-8859-1';
                    $this->email->initialize($config);
                    $from  = EMAIL_FROM; 
                    $this->messageBody  = email_header();
                    $this->messageBody  .= '<tr> 
                    <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                      <p>Hello,</p>
                      <p>
                        We have received your request for username.</p>
                        <p>Your username is "'.$usernamehere.'".</p>                   
                      </td>
                    </tr>
                    <tr>';                      
                      $this->messageBody  .= email_footer();
                      $this->email->from($from, $from);
                      $this->email->to($email);
                      $this->email->subject('Forgot Username');
                      $this->email->message($this->messageBody);
                      $this->email->send();         
                      $this->response(array("status"=>"success", "message"=> "Please check your email address for username."), 200);
                    }
                  }
                }

              }




             //  public function forgotPassword_post() {      
             //    $postData = $_POST;
             //    $email = $this->post('email');
             //    if (isset($_POST['email']) && $_POST['email'] != "") {
             //      $userdata = $this->common_model->select("`id`,`email`,`password`,`phone`",TB_USERS,array("email"=>trim($email),"status"=>"active"));             
             //      if(count($userdata)==0) {                      
             //       $this->response(array("status"=>"error", "message"=> "The email address that you have provided is not associated with any of the active accounts."), 200);
             //     }                  
             //     else {
             //       $userId = $userdata[0]['id'];
             //       $reset_token = random_string('unique');
             //       $userId = base64_encode($userId);
             //       $reset_url = base_url()."frontend/Homecontroller/resetPassword/".$userId."/".$reset_token;                  
             //       $useremail =trim($userdata[0]["email"]) ;               
             //       $datareset = array('token' =>  $reset_token); 
             //       $cond1 = array('email' =>  $useremail); 
             //       $this->common_model->update(TB_USERS,$cond1,$datareset);       
             //            //Send Reset password Link
             //       $hostname = $this->config->item('hostname');
             //       $config['mailtype'] ='html';
             //       $config['charset'] ='iso-8859-1';
             //       $this->email->initialize($config);
             //       $from  = EMAIL_FROM; 
             //       $this->messageBody  = email_header();
             //       $this->messageBody  .= '<tr> 
             //       <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
             //        <p>Hello,</p>
             //        <p>
             //          A request has been made to reset your Transcura account password.<br>
             //          Please click the link below to reset your password.</p>
             //          <p>
             //            <p>Activation Link : </p>
             //            <a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href="'.$reset_url.'">Click Here</a>
             //          </p>
             //        </td>
             //      </tr>
             //      <tr>';                      
             //        $this->messageBody  .= email_footer();
             //        $this->email->from($from, $from);
             //        $this->email->to($email);
             //        $this->email->subject('Reset Password Link');
             //        $this->email->message($this->messageBody);
             //        $this->email->send();         
             //        $this->response(array("status"=>"success", "message"=> "A link to reset your password has been sent to this email address."), 200);
             //      }
             //    }
             //    else
             //    {
             //     return $this->response(array("status"=>"error", "msg" => "Please enter valid email id"), 200);
             //   }
             // }


              /* Edit profile for Nemt/Member*/
              public function editProfile_post()
              { 
                $postData = $_POST; 
                $base_url = $this->config->item('base_url');
                if ($postData["userid"] != '' &&  $postData['usertype']!='' &&  $postData["phone"] != '' &&
                  $postData["country_id"] != '' &&  $postData["date_of_birth"] != ''  &&
                  $postData['fullname']!='' &&  $postData["county"] != '' &&  $postData["city_id"] != '' 
                  &&  $postData["state_id"] != '' &&  $postData["zipcode"] != '' &&  $postData["signature"] != '' 
                  &&  $postData["emergency_contactno"] != '' &&  $postData["emergency_contactname"] != '' &&  $postData["medical_insurance_id"] != '')
                {
                  $userid = $this->post('userid');
                  $userdata = $this->common_model->select("`id`,`email`,`phone`",TB_USERS,array("id"=>trim($userid),"user_type"=>$postData['usertype']));     
                  if(count($userdata)==0) {                      
                    $this->response(array("status"=>"error", "message"=> "User not found."), 200);
                  }
                  else
                  {
            // if(isset($_FILES["personal_doc"]["name"]))
            // {       
            //     $isValid = $this->upload_profile_bio($postData, $_FILES);
            //     if ($isValid["status"] == 'success')
            //     {         
            //         $file_name = $isValid['data']['file_name']; 
            //         if($postData['usertype']==2) 
            //         {         
            //         $doc = 'uploads/Patients/doc/'.$file_name; 
            //         // $insurance_id=trim($this->post('personal_docold')) ; 
            //         }
            //         if($postData['usertype']==1) 
            //         {         
            //         $doc = 'uploads/Member/doc/'.$file_name; 
            //         }                                  
            //     }
            //     else
            //     {
            //         echo $isValid;
            //     }
            // }

            // else
            // {            
            //     $doc = trim($this->post('personal_docold')) ; 

            // }
            // echo ".... ".$doc;die;
                    if( $postData['usertype']=="2")
                    {              
                      if(isset($postData['insurance_provider_id']) &&  $postData['insurance_provider_id']!="")
                      {
                        $insurance_provider_id=trim($this->post('insurance_provider_id')) ;
                  // $insurance_provider= trim($postData["insurance_provider"]) ;
                        $insertArr = array(                        
                          "full_name"   =>trim($postData["fullname"]), 
                          "date_of_birth" =>trim($postData["date_of_birth"]),            
                          "phone"        =>trim($postData["phone"]),
                            // "email"        =>trim($postData["email"]), 
                          "country" => trim($postData["country_id"]),  
                          "county" => trim($postData["county"]), 
                          "city"   =>trim($postData["city_id"]),
                          "street" =>trim($postData["street"]),
                          "state"  =>trim($postData["state_id"]),  
                          "zipcode" => trim($postData["zipcode"]),
                            // "personal_doc" => $doc,  
                          "insurance_id"    =>trim($postData['medical_insurance_id']),
                          "insurance_provider" =>$insurance_provider_id,                   
                          "signature" => trim($postData["signature"]),
                          "emergency_contactno"=>trim($postData["emergency_contactno"]),
                          "emergency_contactname"=>trim($postData["emergency_contactname"]),
                            // "insurance_provider"=>$insurance_provider,
                          "done" =>1                              
                          );
                      }
                      else
                      {
                       return $this->response(array("status"=>"error", "msg" => "NEMT user requires insurance id"), 200);
                     }        
                   }

                   if( $postData['usertype']=="1")
                   {           
                    $insertArr = array(                        
                      "full_name"   =>trim($postData["fullname"]), 
                      "date_of_birth" =>trim($postData["date_of_birth"]),            
                      "phone"        =>trim($postData["phone"]),
                            // "email"        =>trim($postData["email"]), 
                      "country" => trim($postData["country_id"]),  
                      "county" => trim($postData["county"]), 
                      "city"   =>trim($postData["city_id"]),
                      "street" =>trim($postData["street"]),
                      "state"  =>trim($postData["state_id"]),  
                      "zipcode" => trim($postData["zipcode"]),
                            // "personal_doc" => $doc,                         
                      "signature" => trim($postData["signature"]),
                      "insurance_id"    =>trim($postData['medical_insurance_id']),
                      "emergency_contactno"=>trim($postData["emergency_contactno"]),
                      "emergency_contactname"=>trim($postData["emergency_contactname"]),
                      "done"=>1                                                
                      );                  
                  }

                  $insertId = $this->common->update(TB_USERS,array('id'=>$userid),$insertArr);
                  if($insertId){
                    $users = $this->common_model->select("*",TB_USERS,array("id"=>$userid));
                    $country = $this->common_model->select("country_name,id",TB_COUNTRY,array('id' =>$users[0]["country"]));
                    $state = $this->common_model->select("state_name,id",TB_STATE,array('id' =>$users[0]["state"]));
                    $city = $this->common_model->select("city_name,id",TB_CITY,array('id' =>$users[0]["city"]));
                    $insurancelist = $this->common_model->select("insurance_provider ,insurance_id",TB_INSURANCELIST,array('insurance_id'=>$insurance_provider_id));

                    if($users[0]['user_type']=="1")
                    {           
                      $Member = array(                        
                        "full_name"   =>$users[0]["full_name"], 
                        "date_of_birth" =>$users[0]["date_of_birth"],            
                        "phone"        =>$users[0]["phone"],
                        "email"        =>$users[0]["email"], 
                        "country" =>$country[0]['id'],  
                        "county" =>$users[0]["county"], 
                        "city"   =>$city[0]['id'],                     
                        "state"  =>$state[0]['id'],  
                        "street" =>$users[0]["street"],
                        "zipcode" =>$users[0]["zipcode"],
                        // "personal_doc" =>$users[0]["personal_doc"],                         
                        "signature" => $users[0]["signature"],
                        "emergency_contactno"=>$users[0]["emergency_contactno"],
                        "emergency_contactname"=>$users[0]["emergency_contactname"],
                        "insurance_id"=>$users[0]["insurance_id"],
                        "user_type"=>$users[0]["user_type"],
                        "id"=>$users[0]["id"]
                        );                  
                    }
                    if($users[0]['user_type']=="2")
                    {           
                      $Member = array(                        
                        "full_name"   =>$users[0]["full_name"], 
                        "date_of_birth" =>$users[0]["date_of_birth"],            
                        "phone"        =>$users[0]["phone"],
                        "email"        =>$users[0]["email"], 
                        "country" =>$country[0]['id'],  
                        "county" =>$users[0]["county"], 
                        "city"   =>$city[0]['id'],
                        "state"  =>$state[0]['id'],  
                        "zipcode" =>$users[0]["zipcode"],
                        "street" =>$users[0]["street"],
                        // "personal_doc" =>$users[0]["personal_doc"],                         
                        "signature" => $users[0]["signature"],
                        "emergency_contactno"=>$users[0]["emergency_contactno"],
                        "emergency_contactname"=>$users[0]["emergency_contactname"],
                        "insurance_provider"=>$insurancelist[0]["insurance_provider"],
                        "insurance_id"=>$users[0]["insurance_id"],
                        "user_type"=>$users[0]["user_type"],                                       
                        "id"=>$users[0]["id"]
                        );                  
                    }
                    $this->response(array("status"=>"success",  "message"=> "Your profile has been updated successfully.","data"=> $Member), 200); 
                  }
                }
              }
              else
              {
               return $this->response(array("status"=>"error", "msg" => "Please give all requierd fields."), 200);
             }   
           }


           /*  user details  */
           public function userinfo_post()
           {  
            $postData = $_POST;
            $postData = $this->input->post(); 
            if (isset($_POST['userid']) && $_POST['userid'] != "")
            {
              $userid = $this->post('userid');
              $userdata = $this->common_model->select("`id`,`email`,`password`,`phone`",TB_USERS,array("id"=>trim($userid)));
              if(count($userdata)==0) {                      
                $this->response(array("status"=>"error", "message"=> "user not found."), 200);
              }
              else
              {       

               $country_id =$this->post('country_id');
               $users = $this->common_model->select("*",TB_USERS,array('id'=>$userid));            
               $base_url = $this->config->item('base_url'); 
               $key =0;
               foreach($users as $key => $value)
               {       
                $country = $this->common_model->select("id,country_name",TB_COUNTRY,array('id'=>$users[$key]["country"]));
                $country=array("id"=>$country[0]["id"] ,"country_name"=>$country[$key]["country_name"]);       
                $state = $this->common_model->select("id,state_name",TB_STATE,array('id'=>$users[$key]["state"]));
                $state=array("id"=>$state[0]["id"] ,"state_name"=>$state[0]["state_name"]);
                $city = $this->common_model->select("city_name,id",TB_CITY,array('id' =>$users[0]["city"]));
                if($users[$key]["user_type"]==2)
                {
                  $insurancelist = $this->common_model->select("insurance_provider ,insurance_id",TB_INSURANCELIST,array('insurance_id'=>$users[$key]['insurance_provider'])); 
                  $userinfo = array(
                    "fullname" => $users[$key]["full_name"],
                    "email" => $users[$key]["email"],
                    "phone" => $users[$key]["phone"],                        
                    "date_of_birth" => $users[$key]["date_of_birth"],
                    "zipcode" => $users[$key]["zipcode"],
                    "picture" =>$base_url.$users[$key]["picture"],
                    "zipcode" => $users[$key]["zipcode"],             
                    "emergency_contactno" => $users[$key]["emergency_contactno"],
                    "emergency_contactname" => $users[$key]["emergency_contactname"],
                    "signature" =>$users[$key]["signature"],
                    "insurance_provider" =>$insurancelist[0]['insurance_provider'],
                    "insurance_provider_id" => $insurancelist[0]['insurance_id'],
                    "personal_doc" =>$base_url.$users[$key]["personal_doc"],
                    "user_type" =>$users[$key]["user_type"],
                    "insurance_id" =>$users[$key]["insurance_id"],                            
                    "id" => $users[$key]["id"],
                    "country" =>$country,  
                    "county" => $users[$key]["county"], 
                    "city"   =>$city,
                    "street" =>$users[$key]["street"],
                    "state"  =>$state ,
                    "insurancelist" =>$insurancelist              
                    );
                }
                if($users[$key]["user_type"]==1)   
                {
                 $userinfo = array(
                  "fullname" => $users[$key]["full_name"],
                  "email" => $users[$key]["email"],
                  "phone" => $users[$key]["phone"],                        
                  "date_of_birth" => $users[$key]["date_of_birth"],
                  "zipcode" => $users[$key]["zipcode"],
                  "picture" =>$base_url.$users[$key]["picture"],
                  "zipcode" => $users[$key]["zipcode"],             
                  "emergency_contactno" => $users[$key]["emergency_contactno"],
                  "emergency_contactname" => $users[$key]["emergency_contactname"],
                  "signature" =>$users[$key]["signature"],
                  "insurance_id" =>$users[$key]["insurance_id"],  
                  "personal_doc" =>$base_url.$users[$key]["personal_doc"],
                  "user_type" =>$users[$key]["user_type"],                                            
                  "id" => $users[$key]["id"],
                  "country" =>$country,  
                  "county" => $users[$key]["county"], 
                  "city"   =>$city ,
                  "street" =>$users[$key]["street"],
                  "state"  =>$state               
                  );
               }
              // ----------- HEALTHCARE DETAILS -----------------
               if($users[$key]["user_type"]==3)   
               {
                 $userinfo = array(
                  "fullname" => $users[$key]["full_name"],
                  "email" => $users[$key]["email"],
                  "phone" => $users[$key]["phone"],                        
                  "date_of_birth" => $users[$key]["date_of_birth"],
                  "zipcode" => $users[$key]["zipcode"],
                  "picture" =>$base_url.$users[$key]["picture"],                            
                  "personal_doc" =>$base_url.$users[$key]["personal_doc"],
                  "user_type" =>$users[$key]["user_type"], 
                  "username" =>$users[$key]["username"],              
                  "id" => $users[$key]["id"],
                  "country" =>$country,  
                  "county" => $users[$key]["county"], 
                  "city"   =>$city ,
                  "street" =>$users[$key]["street"],
                  "state"  =>$state,
                  "company_name" => $users[$key]["company_name"],             
                  "company_address" => $users[$key]["company_address"],
                  "company_contactno" => $users[$key]["company_contactno"],
                  "vehicle_doc" =>$base_url.$users[$key]["vehicle_doc"],
                  "paratrasmit" =>$users[$key]["paratrasmit"],  
                  "insurance_id" =>$users[$key]["insurance_id"],  
                  "emergency_contactno" =>$users[$key]["emergency_contactno"],  
                  "emergency_contactname" =>$users[$key]["emergency_contactname"],   
                  "signature" =>$users[$key]["signature"]                          
                  );
               }
              //------------------ DRIVER DETAILS ----------------------
               if($users[$key]['user_type']==5)
               {
                $userinfo= array(
                  "fullname" =>$users[$key]['full_name'],
                  "email" =>$users[$key]['email'],
                  "phone" =>$users[$key]['phone'],
                  "date_of_birth" =>$users[$key]['date_of_birth'],
                  "zipcode" => $users[$key]["zipcode"],
                  "picture" =>$base_url.$users[$key]["picture"],                            
                  "personal_doc" =>$base_url.$users[$key]["personal_doc"],
                  "user_type" =>$users[$key]["user_type"],
                  "username" =>$users[$key]["username"],                                     
                  "id" => $users[$key]["id"],
                  "country" =>$country,  
                  "county" => $users[$key]["county"], 
                  "city"   =>$city,
                  "street" =>$users[$key]["street"],
                  "state"  =>$state,
                  "company_name" => $users[$key]["company_name"],             
                  "company_address" => $users[$key]["company_address"],
                  "company_contactno" => $users[$key]["company_contactno"],
                  "vehicle_doc" =>$base_url.$users[$key]["vehicle_doc"],
                  "paratrasmit" =>$users[$key]["paratrasmit"],  
                  "insurance_id" =>$users[$key]["insurance_id"], 
                  "emergency_contactno" =>$users[$key]["emergency_contactno"],  
                  "emergency_contactname" =>$users[$key]["emergency_contactname"],   
                  "signature" =>$users[$key]["signature"],  
                  "social_security_doc" =>$base_url.$users[$key]["social_security_card_doc"],
                  "driver_license_doc" =>$base_url.$users[$key]["driver_license_doc"], 
                  "driver_license_expire_dt"=>$users[$key]['driver_license_expire_dt'],
                  "drug_test_results_doc" =>$base_url.$users[$key]["drug_test_results_doc"], 
                  "drug_test_results_expire_dt"=>$users[$key]['drug_test_results_expire_dt'],
                  "criminal_back_ground_doc" =>$base_url.$users[$key]["criminal_back_ground_doc"], 
                  "criminal_back_ground_expire_dt"=>$users[$key]['criminal_back_ground_expire_dt'],
                  "motor_vehicle_record_doc" =>$base_url.$users[$key]["motor_vehicle_record_doc"], 
                  "motor_vehicle_record_expire_dt"=>$users[$key]['motor_vehicle_record_expire_dt'],
                  "act_33_and_34_clearance_doc" =>$base_url.$users[$key]["act_33_&_34_clearance_doc"], 
                  "act_33_and_34_clearance_expire_dt"=>$users[$key]['act_33_&_34_clearance_expire_dt'],
                  "vehicle_inspections_doc" =>$base_url.$users[$key]["vehicle_inspections_doc"], 
                  "vehicle_inspections_expire_dt"=>$users[$key]['vehicle_inspections_expire_dt'],
                  "vehicle_maintnc_record_doc" =>$base_url.$users[$key]["vehicle_maintnc_record_doc"], 
                  "vehicle_maintnc_record_expire_dt"=>$users[$key]['vehicle_maintnc_record_expire_dt'],
                  "vehicle_insurance_doc" =>$base_url.$users[$key]["vehicle_insurance_doc"], 
                  "vehicle_insurance_expire_dt"=>$users[$key]['vehicle_insurance_expire_dt'],
                  "vehicle_regi_doc" =>$base_url.$users[$key]["vehicle_regi_doc"], 
                  "vehicle_regi_expire_dt"=>$users[$key]['vehicle_regi_expire_dt'],
                  );
              }


              return $this->response(array("status"=>"success", "userinfo" =>$userinfo), 200);
            }
          }

        }else
        {
          return $this->response(array("status"=>"error", "msg" => "missing user id"), 200);
        }       
      }


      /*   edit profile pic*/
      public function uploadUserPic_post()
      {
        $postData = $_POST;
        $base_url = $this->config->item('base_url');         
        if ($postData["userid"] != '')
        {
          $postData = $this->input->post(); 
          $userid = $this->post('userid');
          $userdata = $this->common_model->select("`id`,`email`,`password`,`phone`,`user_type`",TB_USERS,array("id"=>trim($userid)));
          if(count($userdata)==0) {                      
            $this->response(array("status"=>"error", "message"=> "User not found."), 200);
          }
          else
            {        if(isset($_FILES["picture"]["name"]))
          {

           $usertype = $userdata[0]["user_type"];  
           $isValid = $this->upload_profile_image($usertype, $_FILES);
                     //print_r($isValid['data']['new_image']);die;
           if($isValid["status"] == 'success')
           {           
            $insertArr = array(                                
              "picture"=>$isValid['data']['new_image']                             
              );
            $insertId = $this->common->update(TB_USERS,array('id'=>$userid),$insertArr);
                         // echo $this->db->last_query();die;
            if($insertId){
              $this->response(array("status"=>"success", "message"=> "Your profile picture has been updated successfully.","picture"=> $base_url.$isValid['data']['new_image']), 200); 
            }else{
              $this->response(array("status"=>"error", "message"=>"Please upload valid profile picture"), 200);
            }                                                     
          }
          else
          {
            echo $isValid;
          }
        }
        else
        {
          $this->response(array("status"=>"error", "message"=> "Missing profile picture"), 200);
        }
      }
    }
    else
    {
      $this->response(array("status"=>"error", "message"=> "Missing user id"), 200); 
    }   

  }


  /* country list */
  public function country_get() 
  {
    $country = $this->common_model->select("country_name,id",TB_COUNTRY);   
    if( count($country) >0)
    {        
      foreach($country as $key => $value)
      {
        $country[] = array("id" => $country[$key]["id"],"country_name" => $country[$key]["country_name"]); 
        $this->response(array("status"=>200,"statusCode"=>true,"country"=>$country), 200); 
      }

    }
    else
    {
      $this->response(array("status"=>200,"statusCode"=>false, "message"=> "Data not found"), 200); 
    }      
  }


  /* state list */
  public function state_post() 
  {
   $postData = $_POST;
   if (isset($_POST['country_id']) && $_POST['country_id'] != "")
   {
    $country_id = $this->post('country_id');
    $statelist = $this->common_model->select("state_name,id",TB_STATE,array("country_id"=>trim($country_id)));   
    if( count($statelist) >0)
    {        
      foreach($statelist as $key => $value)
      {
        $state[] = array("id" => $statelist[$key]["id"],"state_name" => $statelist[$key]["state_name"]); 

      }
      $this->response(array("status"=>200,"statusCode"=>true,"state"=>$state), 200);           
    }
    else
    {
      $this->response(array("status"=>200,"statusCode"=>false, "message"=> "Data not found"), 200); 
    }   
  }
  else
  {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=> "country id not found"), 200); 
  }
  }


  /* city name */
  public function city_post() 
  {
   $postData = $_POST;
   if (isset($_POST['state_id']) && $_POST['state_id'] != "")
   {
    $state_id = $this->post('state_id');
    $citylist = $this->common_model->select("city_name,id",TB_CITY,array("state_id"=>trim($state_id)));
    if(count($citylist) >0)
    {        
                // foreach($citylist as $key => $value)
                // {
                // $citylist = array("id" => $citylist[$key]["id"],"city_name" => $citylist[$key]["city_name"]); 
      $this->response(array("status"=>"success","city"=>$citylist), 200); 
                // }           
    }
    else
    {
      $this->response(array("status"=>"error", "message"=> "Data not found"), 200); 
    }   
  }
  else
  {
    $this->response(array("status"=>"error", "message"=> "State id not found"), 200); 
  }
  }


  /*  reset password*/
  public function resetPassword_post()
  {
   $postData = $_POST;     
   if ($postData['userid'] == ""  || $postData['oldpassword'] == "" || $postData['newpassword'] == "" || $postData['cpassword'] == "")
   {
    $this->response(array("status"=>'error', "message"=> "all fild are required"), 200);   
  } else if($postData["newpassword"] != $postData["cpassword"]){
    $this->response(array("status"=>'error', "message"=> "New password and confirmed password does not match."), 200);
  } else
  {
    $emailexist = $this->common_model->select("id",TB_USERS,array("password"=>md5($postData['oldpassword']),"id"=>trim($postData["userid"]))); 
    if(count($emailexist)==0) {
      $this->response(array("status"=>'error', "message"=> "Wrong old password or user not found."), 200);
    }  
    else
    {
      $insertArr = array("password" =>md5($postData['newpassword']));
      $cond = array("id"=>trim($postData["userid"]));
      $insertId = $this->common->update(TB_USERS,$cond,$insertArr);
      if($insertId){
        $this->response(array("status"=>200, "message"=> "Your password has been changed successfully."), 200); 
      }
      else{
        $this->response(array("status"=>200,  "message"=> "Some issue in change password."), 200); 
      }          
    }
  }
  }


  /*   Register NEMT/member*/
  /*public function registerDriver_post()
  {
    $postData = $_POST;
    $postData = $this->input->post(); 
    $reset_token = random_string('unique');
    $usertype = trim($this->post('usertype'));
    $base_url = $this->config->item('base_url');
    $reset_url = $base_url."accountverify/".$reset_token;
   // echo "hgjkhk" ;die;

  		if ($postData["email"] != '' && $_FILES["personal_doc"]["name"]!=''  &&  $postData["usertype"] != '' &&  $postData["password"] != '' && $postData["phone"] != '')
  		{
  		    $emailexist = $this->common_model->select("email",TB_USERS,array("email"=>trim($postData["email"]) , "user_type"=>trim($postData["usertype"])));  
  		   //echo $this->db->last_query();die;
  		    if(count($emailexist)>1) {
  		    $this->response(array("status"=>200, "statusCode"=>false, "message"=> "User already exist with this email id."), 200);
  		    }  
  		    else
  		    {
  				if(isset($_FILES["personal_doc"]["name"]) && $_FILES["personal_doc"]["name"]!="")
  				{
  				   $isValid = $this->upload_profile_bio($postData, $_FILES);
  				
  				    if ($isValid["status"] == 'success')
  				    {         
  				        $file_name = $isValid['data']['file_name'];				                 
  				        $doc = 'uploads/Driver/doc/'.$file_name; 
  				    }
  				    else
  				    {
  				    echo $isValid;
  				    }
  				}
  				else
  				{
  				    $this->response(array("status"=>200,"statusCode"=>false, "message"=> "personal document missing"), 200);
  				}

  	            if(isset($postData['operate_own_vehicle_ownbusiness']) && $postData['operate_own_vehicle_ownbusiness']=="Yes")
  	            {
  	               if(isset($_FILES["operate_own_vehicle_ownbusiness_doc"]["name"]) && $_FILES["operate_own_vehicle_ownbusiness_doc"]["name"]!="" )
  					{			
  						$operate_own_vehicle_comy="No";
  						$operate_comy_vehicle="No";
  						$operate_own_vehicle_ownbusiness=$postData['operate_own_vehicle_ownbusiness'];
  					    $isValid = $this->upload_profile_bio1($postData, $_FILES);					
  					    if ($isValid["status"] == 'success')
  					    {      
  					        $file_name = $isValid['data1']['file_name'];			                 
  					        $vehicle_doc = 'uploads/Driver/doc/'.$file_name;					   			                 
  					    }
  					    else
  					    {
  					    echo $isValid;
  					    }
  					}					
  				}
  	            else if(isset($postData['operate_own_vehicle_comy']) && $postData['operate_own_vehicle_comy']=="Yes")
  	            {
  	                 if(isset($_FILES["own_vehicle_comy_doc"]["name"])  && $_FILES["own_vehicle_comy_doc"]["name"]!="" )
  					{
  					   $operate_own_vehicle_comy=$postData['operate_own_vehicle_comy'];
  						$operate_comy_vehicle="No";
  						$operate_own_vehicle_ownbusiness="No";
  					    $isValid = $this->upload_profile_bio1($postData, $_FILES);					  
  					    if ($isValid["status"] == 'success')
  					    {         
  					        $file_name = $isValid['data1']['file_name'];				                 
  					        $vehicle_doc = 'uploads/Driver/doc/'.$file_name; 					   		                 
  					    }
  					    else
  					    {
  					    echo $isValid;
  					    }
  					}			

  				}
  	           else if(isset($postData['operate_comy_vehicle']) && $postData['operate_comy_vehicle']=="Yes")
  	            {
  					 if(isset($_FILES["comy_vehicle_doc"]["name"]) && $_FILES["comy_vehicle_doc"]["name"]!="" )				
  					{
  						$operate_own_vehicle_comy="No";
  						$operate_comy_vehicle=$postData['operate_comy_vehicle'];
  						$operate_own_vehicle_ownbusiness="No";
  					   $isValid = $this->upload_profile_bio1($postData, $_FILES);
  					 
  					    if ($isValid["status"] == 'success')
  					    {         
  					        $file_name = $isValid['data1']['file_name'];				                 
  					        $vehicle_doc = 'uploads/Driver/doc/'.$file_name; 					  			                 
  					    }
  					    else
  					    {
  					    echo $isValid;
  					    }
  					}					
  				}
  				else
  				{
                 $this->response(array("status"=>200,"statusCode"=>false, "message"=> "vehicle document missing"), 200);
  				}

  		    	$insertArr = array("user_type"=>$usertype,
                              "user_token" => mt_rand(100000, 999999),
                              "password"     =>md5(trim($postData["password"])),
                              "full_name"   =>trim($postData["fullname"]), 
                              "date_of_birth" =>trim($postData["date_of_birth"]),            
                              "phone"        =>trim($postData["phone"]),
                              "email"        =>trim($postData["email"]),                          
                              "status"=>"inactive",
                              "token"=>$reset_token,
                              "device_type"=>trim($postData["device_type"]),                                           
                              "country" => trim($postData["country_id"]),  
                              "county" => trim($postData["county"]), 
                              "city"   =>trim($postData["city_id"]),
                              "street" =>trim($postData["street"]),
                              "state"  =>trim($postData["state_id"]),  
                              "zipcode" => trim($postData["zipcode"]),
                              "company_name" =>trim($postData["company_name"]),
                              "company_address"  =>trim($postData["company_address"]),  
                              "company_contactno"  =>trim($postData["company_contactno"]),  
                              "personal_doc" => $doc, 
                              "vehicle_doc"=>$vehicle_doc,
  							"operate_own_vehicle_ownbusiness" =>$operate_own_vehicle_ownbusiness,							
  							"paratrasmit"=>trim($postData["paratrasmit"]),  
  							"operate_comy_vehicle" =>$operate_comy_vehicle,
  							"operate_own_vehicle_comy" =>$operate_own_vehicle_comy						
                              );

  			  $user = $this->common_model->insert(TB_USERS,$insertArr);
                 //print_r($user);die;

  			       if($user)
                                  {
                                  $to_email = $postData["email"];
                                  $hostname = $this->config->item('hostname');  
                                  $config['mailtype'] ='html';
                                  $config['charset'] ='iso-8859-1';
                                  $this->email->initialize($config);
                                  $from  = EMAIL_FROM; 
                                  $this->messageBody  = email_header();
                                  $this->messageBody  .= '<tr>
                  <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;height:200px;vertical-align:top;">
                      <p>Dear Customer,</p>
                          <p>
                          You are registered successfully on Transcura as a driver.<br>
                          Please wait while administrator activates your account. </p>
                         
                      </td>
                  </tr>               
                 ';                      
                                  $this->messageBody  .= email_footer();        //print_r( $this->messageBody);die;
                                  $this->email->from($from, $from);
                                  $this->email->to($to_email);
                                  $this->email->subject('Welcome to Transcura');
                                  $this->email->message($this->messageBody);
                                  $this->email->send();
                                  $this->response(array("status"=>200,"message"=> "Registration Successful. Your account will be activated once the Admin approves it.","statusCode"=>true), 200);  
                                  }                       
          }            

  		}
  		else
  		{
  		$this->response(array("status"=>200,"statusCode"=>false, "message"=> "all fields are mandatory"), 200);
  		}
    } */


    function registerUserTest_post() {  
      $postData = $_POST; 
      if ($_FILES["personal_doc"]["name"] != '') {

        $imagePath = './uploads/personal_doc';
        $file = $_FILES["personal_doc"]["name"];
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $filename = time().'.'.$ext;	
        if(move_uploaded_file($_FILES["personal_doc"]["tmp_name"], $imagePath.'/'.$filename)){
         $this->response(array("status"=>"sucess", "message"=> "file uploaded....","file_name"=>$imagePath.'/'.$filename), 200);	
       } else {
         $error = $this->upload->display_errors();
         $this->response(array("status" => 'error', "message" => strip_tags($error)), 200);
       }

     }else{
      $this->response(array("status"=>"error", "message"=> "personal document missing."), 200);
    }
  }

  function registerUser_post() {
    $postData = $_POST;
    $reset_token = random_string('unique');
    $usertype = trim($this->post('user_type'));
    $base_url = $this->config->item('base_url');
    $reset_url = $base_url."accountverify/".$reset_token;
    $flag =0;
      // echo "hgjkhk" ;die;
    if ($postData["email"] != '' &&  $postData["user_type"] != '' &&  $postData["password"] != '')
    {
      $emailexist = $this->common_model->select("email",TB_USERS,array("email"=>trim($postData["email"]),"user_type"=>trim($postData["user_type"])));  
        //echo $this->db->last_query(); die;
      if(count($emailexist)>0) {
        $this->response(array("status"=>"error", "message"=> "User already exist with this email id."), 200);
      }else{
        if($usertype == "2")
        {
          if (isset($_FILES["personal_doc"]) && $_FILES["personal_doc"]["name"] != '')
          {   

            $imagePath = './uploads/personal_doc';
            $file = $_FILES["personal_doc"]["name"];
            $ext = pathinfo($file, PATHINFO_EXTENSION);
            $filename = time().'.'.$ext;  
            if(move_uploaded_file($_FILES["personal_doc"]["tmp_name"], $imagePath.'/'.$filename)){
              $data = array('personal_doc' => $filename);
              $path = $this->config->base_url() . 'uploads/personal_doc/';
            } else {
              $error = $this->upload->display_errors();
              $this->response(array("status" => 'error', "message" => strip_tags($error)), 200);
            }
            $file_name = $filename;                        
            $doc = 'uploads/personal_doc/'.$file_name; 
            $flag = 1;
          }else{
            $doc = "";
            $this->response(array("status"=>"error", "message"=> "personal document missing."), 200);
          }           
        }
        else if($usertype == "1")
        {
           //----- INSERT CARD DETAILS FOR MEMBER
          if($postData['card_number']!= '' && $postData['card_cvv'] != '' && $postData['exp_month']!='' && $postData['exp_year']!='')
          {
            $card_number = trim($postData['card_number']);
            $card_cvv = trim($postData['card_cvv']);
            $exp_month = trim($postData['exp_month']);
            $exp_year = trim($postData['exp_year']);          
            $flag = 1;
            $doc = "";
          }else
          {
            $this->response(array("status"=>"error", "message"=> "Please enter card details."), 200);
          }
        }
        else
        {
          $flag = 1;
          $doc = "";
          $card_number = "";
          $card_cvv = "";
          $exp_month = "";
          $exp_year = "";
        }

        

        if($flag == 1)
        {
          $insertArr= array("user_type"=>$usertype,
            "user_token" => mt_rand(100000, 999999),
            "password"     =>md5(trim($postData["password"])),
            "full_name"   =>trim($postData["full_name"]), 
            "date_of_birth" =>trim($postData["date_of_birth"]),            
            "phone"        =>trim($postData["phone"]),
            "email"        =>trim($postData["email"]),                          
            "status"=>"inactive",
            "token"=>$reset_token,
            "country" => trim($postData["country_id"]),  
            "county" => trim($postData["county"]), 
            "city"   =>trim($postData["city_id"]),
            "state"  =>trim($postData["state_id"]),  
            "zipcode" => trim($postData["zipcode"]),
            "personal_doc" =>  $doc,                               
            "insurance_id" => trim($postData["medical_insurance_id"]),
            "emergency_contactno" => trim($postData["emg_phone"]),
            "emergency_contactname" => trim($postData["emg_name"]),
            "signature" => trim($postData["signature_type"]),
            "street" => trim($postData["street"]),
            "username" => trim($postData["usernamehere"]),
            "card_number" => $card_number,
            "card_cvv" => $card_cvv,
            "exp_month" => $exp_month,
            "exp_year" => $exp_year
            );
                // echo "<pre>";print_r($insertArr);
          $user = $this->common_model->insert(TB_USERS,$insertArr);
          if($user)
          {
            $user_id = $this->db->insert_id();
            $to_email = $postData["email"];
            $hostname = $this->config->item('hostname');  
            $config['mailtype'] ='html';
            $config['charset'] ='iso-8859-1';
            $this->email->initialize($config);
            $from  = EMAIL_FROM; 
            $name = trim($postData['full_name']);
            $email = trim($postData['email']);
            $this->messageBody  = email_header();
            $this->messageBody.= '<tr>
            <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
              <p>Dear Customer,</p>
              <p>
                You have registered successfully on Transcura.<br> Please click the link given below to verify your email address</p>
                <p>
                  <p>Activation Link : </p>
                  <a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href='.$reset_url.'>Click Here</a>
                </p><br /><br />
                <p>If the above link does not work, please copy and paste the following link into your browser.</p>
                <p>Copy the Link : </p>
                <a href="'.$reset_url.'">'.$reset_url.'</a>
              </p>          
            </td>
          </tr>';                      
                  $this->messageBody.= email_footer();        //print_r( $this->messageBody);die;
                  $this->email->from($from, $from);
                  $this->email->to($to_email);
                  $this->email->subject('Welcome to Transcura');
                  $this->email->message($this->messageBody);
                  $this->email->send();
                  $this->response(array("status"=>"success","message"=> "Registration Successful. Please check your email for activation.","user_id"=>$user_id,"user_type"=>$usertype), 200);  
                }
                else
                {
                  $this->response(array("status" => "error", "msg" => "Please try again."), 200);
                  die;
                }   
              }

            }           
          }else{
            $this->response(array("status" => "error", "msg" => "Please enter all required field(s)."), 200);
          } 
        }



        /*   Register NEMT/member*/
        public function EditDriver_post()
        {
          $postData = $_POST;
          $postData = $this->input->post(); 
          $reset_token = random_string('unique');
          $usertype = trim($this->post('usertype'));
          $base_url = $this->config->item('base_url');
          $userid= $postData["userid"];

          if ($postData["userid"] != '' &&  $postData["email"] != '' &&  $postData["usertype"] != '' && $postData["phone"] != '')
          {		    
            if(isset($_FILES["personal_doc"]["name"]) && $_FILES["personal_doc"]["name"]!="")
            {
             $isValid = $this->upload_profile_bio($postData, $_FILES);				
             if ($isValid["status"] == 'success')
             {         
              $file_name = $isValid['data']['file_name'];				                 
              $doc = 'uploads/Driver/doc/'.$file_name; 
            }
            else
            {
              echo $isValid;
            }
          }
          else
          {
            $doc = $postData["personal_doc_old"];
          }

          if(isset($_FILES["operate_own_vehicle_ownbusiness_doc"]["name"]) && $_FILES["operate_own_vehicle_ownbusiness_doc"]["name"]!="" )
          {			
            $operate_own_vehicle_comy="No";
            $operate_comy_vehicle="No";
            $operate_own_vehicle_ownbusiness=$postData['operate_own_vehicle_ownbusiness'];
            $isValid = $this->upload_profile_bio1($postData, $_FILES);					
            if ($isValid["status"] == 'success')
            {      
             $file_name = $isValid['data1']['file_name'];			                 
             $vehicle_doc = 'uploads/Driver/doc/'.$file_name;					   			                 
           }
           else
           {
             echo $isValid;
           }
         }	
         else
         {
          $vehicle_doc = $postData["vehicle_doc_old"];	
        }


        if(isset($_FILES["own_vehicle_comy_doc"]["name"]) && $_FILES["own_vehicle_comy_doc"]["name"]!="" )
        {
         $operate_own_vehicle_comy=$postData['operate_own_vehicle_comy'];
         $operate_comy_vehicle="No";
         $operate_own_vehicle_ownbusiness="No";
         $isValid = $this->upload_profile_bio1($postData, $_FILES);					  
         if ($isValid["status"] == 'success')
         {         
           $file_name = $isValid['data1']['file_name'];				                 
           $vehicle_doc = 'uploads/Driver/doc/'.$file_name; 					   		                 
         }
         else
         {
           echo $isValid;
         }
       }
       else
       {
        $vehicle_doc = $postData["vehicle_doc_old"];	
      }


      if(isset($_FILES["comy_vehicle_doc"]["name"])  && $_FILES["comy_vehicle_doc"]["name"]!="" )				
      {
        $operate_own_vehicle_comy="No";
        $operate_comy_vehicle=$postData['operate_comy_vehicle'];
        $operate_own_vehicle_ownbusiness="No";
        $isValid = $this->upload_profile_bio1($postData, $_FILES);					 
        if ($isValid["status"] == 'success')
        {         
         $file_name = $isValid['data1']['file_name'];				                 
         $vehicle_doc = 'uploads/Driver/doc/'.$file_name; 					  			                 
       }
       else
       {
         echo $isValid;
       }
     }
     else
     {
      $vehicle_doc = $postData["vehicle_doc_old"];	
    }


    $insertArr = array("user_type"=>$usertype,                                                   
      "full_name"   =>trim($postData["fullname"]), 
      "date_of_birth" =>trim($postData["date_of_birth"]),            
      "phone"      =>trim($postData["phone"]),
      "email"      =>trim($postData["email"]),                                                      
      "country" => trim($postData["country_id"]),  
      "county" => trim($postData["county"]), 
      "city"   =>trim($postData["city_id"]),
      "street" =>trim($postData["street"]),
      "state"  =>trim($postData["state_id"]),  
      "zipcode" => trim($postData["zipcode"]),
      "company_name" =>trim($postData["company_name"]),
      "company_address"  =>trim($postData["company_address"]),  
      "company_contactno"  =>trim($postData["company_contactno"]),  
      "personal_doc" => $doc, 
      "vehicle_doc"=>$vehicle_doc,
      "operate_own_vehicle_ownbusiness" =>trim($postData["operate_own_vehicle_ownbusiness"]),				
      "paratrasmit"=>trim($postData["paratrasmit"]),  
      "operate_comy_vehicle" =>trim($postData["operate_comy_vehicle"]),
      "operate_own_vehicle_comy" =>trim($postData["operate_own_vehicle_comy"]),
      "done"=>1					
      );

    $insertId = $this->common->update(TB_USERS,array('id'=>$userid),$insertArr);
  					//	echo $this->db->last_query();die;
    if($insertId){
     $users = $this->common_model->select("*",TB_USERS,array("id"=>$userid));
     $country = $this->common_model->select("country_name,id",TB_COUNTRY,array('id' =>$users[0]["country"]));
     $state = $this->common_model->select("state_name,id",TB_STATE,array('id' =>$users[0]["state"]));
     $city = $this->common_model->select("city_name,id",TB_CITY,array('id' =>$users[0]["city"]));     

     $driver = array(                        
      "full_name"   =>$users[0]["full_name"], 
      "date_of_birth" =>$users[0]["date_of_birth"],            
      "phone"        =>$users[0]["phone"],
      "email"        =>$users[0]["email"], 
      "country" =>$country[0],  
      "county" =>$users[0]["county"], 
      "city"   =>$city[0],
      "state"  =>$state[0],  
      "zipcode" =>$users[0]["zipcode"],
      "street" =>$users[0]["street"],
      "personal_doc" =>$users[0]["personal_doc"],
      "vehicle_doc" =>$base_url.$users[0]["vehicle_doc"],                                          
      "company_name"=>$users[0]["company_name"],
      "company_address"=>$users[0]["company_address"],
      "company_contactno"=>$users[0]["company_contactno"],
      "operate_comy_vehicle"=>$users[0]["operate_comy_vehicle"],
      "operate_own_vehicle_ownbusiness"=>$users[0]["operate_own_vehicle_ownbusiness"],
      "operate_own_vehicle_comy"=>$users[0]["operate_own_vehicle_comy"]
      );               

     $this->response(array("status"=>200,"statusCode"=>true,  "message"=> "Your profile has been updated successfully.","data"=> $driver), 200); 
   }

  }
  else
  {
    $this->response(array("status"=>200,"statusCode"=>false, "message"=> "all fields are mandatory"), 200);
  }
  }

  /* Shedule api's  */


  public function scheduleUser_post()
  {
   $postData = $_POST;
   $postData = $this->input->post();     
   $base_url = $this->config->item('base_url');
   $userid = $postData["userid"];
   if($postData["userid"] == '' || $postData["sr_date"]==''  || $postData["sr_pick_up_time"]=='' || $postData["sr_appointment_time"] == '' || $postData["sr_street"]==''|| $postData["sr_state"] == ''|| $postData["sr_country"] == '' || $postData["sr_zipcode"]=='' || $postData["sr_destination"]=='' || $postData["sr_special_request"]==''|| $postData["sr_desire_transportaion"]==''  )
   {
    $this->response(array("status"=>200, "statusCode"=>false, "message"=> "All Filds required."), 200);
  }
  else
  {
    $userexit = $this->common_model->select("*",TB_USERS,array("id"=>trim($postData["userid"])));
       //   echo $this->db->last_query();die;
    if(count($userexit)>0){
     $insertArr = array("user_id"=>$postData["userid"],
      "sr_date" =>$postData["sr_date"],
      "sr_appointment_time" =>trim($postData["sr_appointment_time"]),
      "sr_street"   =>trim($postData["sr_street"]), 
      "sr_country" =>trim($postData["sr_country"]),            
      "sr_zipcode" =>trim($postData["sr_zipcode"]),
      "sr_state"  =>trim($postData["sr_state"]),                          
      "sr_destination"=>trim($postData["sr_destination"]),
      "sr_special_request"=>trim($postData["sr_special_request"]),                          
      "sr_desire_transportaion"=>trim($postData["sr_desire_transportaion"]),   
      "sr_special_instruction"=>trim($postData["sr_special_instruction"]),
      "sr_status"=>1,                                       
      );
     $schedule = $this->common_model->insert(TB_SCHEDULE_RIDE,$insertArr); 
            //  echo $this->db->last_query();die;
     $this->response(array("status"=>200, "statusCode"=>true, "message"=> "Your schedule has been sent successfully"), 200);
   } 
   else
   {
    $this->response(array("status"=>200, "statusCode"=>false, "message"=> "User not found"), 200);
  }
  }

  }

  public function scheduleUserdetail_post()
  {
   $postData = $_POST;
   $postData = $this->input->post();     
   $base_url = $this->config->item('base_url');
   $userid = $postData["userid"];
   if($postData["userid"] == '' || $postData["schedule_ride_id"]=='')
   {
    $this->response(array("status"=>200, "statusCode"=>false, "message"=> "All Filds required."), 200);
  }
  else
  {
   $userexit = $this->common_model->select("*",TB_SCHEDULE_RIDE,array("id"=>trim($postData["schedule_ride_id"]) ,"user_id"=>trim($postData["userid"])));

   $country = $this->common_model->select("country_name,id",TB_COUNTRY,array('id' =>$userexit[0]["sr_country"]));
   $state = $this->common_model->select("state_name,id",TB_STATE,array('id' =>$userexit[0]["sr_state"]));
   $sr_destination = $this->common_model->select("*",TB_DESTINATION,array('id' =>$userexit[0]["sr_destination"]));
   $sr_special_request = $this->common_model->select("sr_name as request,id",TB_SPECIAL_REQUEST,array('id' =>$userexit[0]["sr_special_request"]));
   $sr_desire_transportaion = $this->common_model->select("dt_name as trasportation,id",TB_DESIRE_TRANSPORTATION,array('id' =>$userexit[0]["sr_desire_transportaion"]));


   if(count($userexit)>0){        

     $schedule = array(                        
      "sr_date"   =>$userexit[0]["sr_date"], 
      "sr_pick_up_time" =>$userexit[0]["sr_pick_up_time"], 
      "sr_appointment_time" =>$userexit[0]["sr_appointment_time"], 
      "sr_special_instruction" =>$userexit[0]["sr_special_instruction"],
      "sr_destination" =>$sr_destination[0],
      "sr_special_request" =>$sr_special_request[0],                      
      "sr_desire_transportaion" =>$sr_desire_transportaion[0],
      "state" =>$state[0],  
      "country" =>$country[0],  
      "sr_zipcode" =>$userexit[0]["sr_zipcode"], 

      );      


     $this->response(array("status"=>200, "statusCode"=>true, "schedule"=> $schedule), 200);
   }
   else
   {
    $this->response(array("status"=>200, "statusCode"=>false, "message"=> "User not found"), 200);
  }
  }
  } 

  public function service_type_get(){
    $result = $this->common_model->select("*",TB_SERVICE_TYPE);
    if(count($result)>0){
      $this->response(array("status"=>"success",  "data"=> $result), 200);
    }else{
      $this->response(array("status"=>"error",  "data"=> "Data not found"), 200);
    }
    
  }

  public function special_request_get(){
    $result = $this->common_model->select("*",TB_SPECIAL_REQUEST);
    if(count($result)>0){
      $this->response(array("status"=>"success",  "data"=> $result), 200);
    }else{
      $this->response(array("status"=>"error",  "data"=> "Data not found"), 200);
    }
    
  }

  public function desired_transportation_get(){
    $result = $this->common_model->select("*",TB_DESIRE_TRANSPORTATION);
    if(count($result)>0){
      $this->response(array("status"=>"success",  "data"=> $result), 200);
    }else{
      $this->response(array("status"=>"error",  "data"=> "Data not found"), 200);
    }
    
  }

  public function insurance_provider_get(){
    $result = $this->common_model->select("*",TB_INSURANCELIST);
    if(count($result)>0){
      $this->response(array("status"=>"success",  "data"=> $result), 200);
    }else{
      $this->response(array("status"=>"error",  "data"=> "Data not found"), 200);
    }
    
  }

  public function category_list_get(){
    $cond= array('isDeleted' =>"0");
    $result = $this->common_model->select("*",TB_DOCTORS_CAT,$cond);
    if(count($result)>0){
      $this->response(array("status"=>"success",  "data"=> $result), 200);
    }else{
      $this->response(array("status"=>"error",  "data"=> "Data not found"), 200);
    }
    
  }

  public function healthcare_list_get(){
    $cond= array("user_type" =>"3","status"=>"active");
    $result = $this->common_model->select("*",TB_USERS,$cond);
    if(count($result)>0){
      $this->response(array("status"=>"success",  "data"=> $result), 200);
    }else{
      $this->response(array("status"=>"error",  "data"=> "Data not found"), 200);
    }
    
  }

  // --------- Upload all driver document  -------------------
  public function save_driverDocument_post()
  {
    $postData = $_POST;
    $postData = $this->input->post(); 
    $userid = trim($postData['user_id']);
    $usertype = trim($postData['user_type']);
    $doc_flag = trim($postData['doc_flag']);
    $cond = array("id" => $userid, "user_type"=>$usertype);
    $check_driverq = $this->common_model->select("id,email,user_type",TB_USERS,$cond);
    $flag=1;
    if($usertype != 5)
    {
     $this->response(array("status"=>"error",  "msg"=> "Please enter usertype for driver"), 200); 
   }
   else
   {
    if(count($check_driverq) > 0)
    {
      if(empty($doc_flag))
      {
       $this->response(array("status"=>"error",  "msg"=> "Please enter document flag."), 200);  
     }else
     {
          // ------- SOCIAL SECURITY CARD -----------
      if($doc_flag == "social_security_doc")
      {
        if(isset($_FILES["social_security_doc"]) && $_FILES["social_security_doc"]["name"] != '') 
        {
          $file_name = $_FILES['social_security_doc']["name"];
          $db_doc_col ='social_security_card_doc';
          $exp_date = date('Y-m-d H:i:s');
          $db_date_col ='updated_at';
          $flag=1;
        }
        else
        {
          $flag = 0;
          $this->response(array("status"=>"error",  "msg"=> "Please select social security card."), 200);
        }          
      } 
          // ------- DRIVER LICENCE -----------
      else if($doc_flag == "driver_lic_doc")
      {
        if(isset($_FILES["driver_lic_doc"]) && $_FILES["driver_lic_doc"]["name"] != '' && $postData['driver_lic_exp_dt']) 
        {
          $file_name = $_FILES['driver_lic_doc']["name"];
          $exp_date = $postData['driver_lic_exp_dt'];
          $db_doc_col ='driver_license_doc';
          $db_date_col ='driver_license_expire_dt';
          $flag=1;
        }
        else
        {
          $flag=0;
          $this->response(array("status"=>"error",  "msg"=> "Please select Driver licence and enter expiry date."), 200);
        }
      }
          // ------- 5-PANEL DRUG TEST RESULT -----------
      else if($doc_flag == "drug_test_doc")
      {
        if(isset($_FILES["drug_test_doc"]) && $_FILES["drug_test_doc"]["name"] != '' && $postData['drug_test_exp_dt']) 
        {
          $file_name = $_FILES['drug_test_doc']["name"];
          $exp_date = $postData['drug_test_exp_dt'];
          $db_doc_col ='drug_test_results_doc';
          $db_date_col ='drug_test_results_expire_dt';
          $flag=1;
        }
        else
        {
          $flag=0;
          $this->response(array("status"=>"error",  "msg"=> "Please select Driver licence and enter expiry date."), 200);
        }
      }
          // ------- CRIMINAL BACK GROUND -----------
      else if($doc_flag == "criminal_back_doc")
      {
        if(isset($_FILES["criminal_back_doc"]) && $_FILES["criminal_back_doc"]["name"] != '' && $postData['criminal_back_exp_dt']) 
        {
          $file_name = $_FILES['criminal_back_doc']["name"];
          $exp_date = $postData['criminal_back_exp_dt'];
          $db_doc_col ='criminal_back_ground_doc';
          $db_date_col ='criminal_back_ground_expire_dt';
          $flag=1;
        }
        else
        {
          $flag=0;
          $this->response(array("status"=>"error",  "msg"=> "Please select Criminal backgound doc and enter expiry date."), 200);
        }
      }
           // ------- MOTOR VEHICLE RECORD -----------
      else if($doc_flag == "motor_vehicle_doc")
      {
        if(isset($_FILES["motor_vehicle_doc"]) && $_FILES["motor_vehicle_doc"]["name"] != '' && $postData['motor_vehicle_exp_dt']) 
        {
          $file_name = $_FILES['motor_vehicle_doc']["name"];
          $exp_date = $postData['motor_vehicle_exp_dt'];
          $db_doc_col ='motor_vehicle_record_doc';
          $db_date_col ='motor_vehicle_record_expire_dt';
          $flag=1;
        }
        else
        {
          $flag=0;
          $this->response(array("status"=>"error",  "msg"=> "Please select Motor vehicle doc and enter expiry date."), 200);
        }
      }
           // ------- ACT 33 & 34 CLEARANCE -----------
      else if($doc_flag == "act33_clearance_doc")
      {
        if(isset($_FILES["act33_clearance_doc"]) && $_FILES["act33_clearance_doc"]["name"] != '' && $postData['act33_clearance_exp_dt']) 
        {
          $file_name = $_FILES['act33_clearance_doc']["name"];
          $exp_date = $postData['act33_clearance_exp_dt'];
          $db_doc_col ='act_33_and_34_clearance_doc';
          $db_date_col ='act_33_and_34_clearance_expire_dt';
          $flag=1;
        }
        else
        {
          $flag=0;
          $this->response(array("status"=>"error",  "msg"=> "Please select Act 33 & 34 clearance doc and enter expiry date."), 200);
        }
      }
             // ------- VEHICLE INSPECTATION -----------
      else if($doc_flag == "veh_inspectation_doc")
      {
        if(isset($_FILES["veh_inspectation_doc"]) && $_FILES["veh_inspectation_doc"]["name"] != '' && $postData['veh_inspectation_exp_dt']) 
        {
          $file_name = $_FILES['veh_inspectation_doc']["name"];
          $exp_date = $postData['veh_inspectation_exp_dt'];
          $db_doc_col ='vehicle_inspections_doc';
          $db_date_col ='vehicle_inspections_expire_dt';
          $flag=1;
        }
        else
        {
          $flag=0;
          $this->response(array("status"=>"error",  "msg"=> "Please select Vehicle inspecation doc and enter expiry date."), 200);
        }
      }
            // ------- VEHICLE MAINTEANANCE -----------
      else if($doc_flag == "veh_maintenance_doc")
      {
        if(isset($_FILES["veh_maintenance_doc"]) && $_FILES["veh_maintenance_doc"]["name"] != '' && $postData['veh_maintenance_exp_dt']) 
        {
          $file_name = $_FILES['veh_maintenance_doc']["name"];
          $exp_date = $postData['veh_maintenance_exp_dt'];
          $db_doc_col ='vehicle_maintnc_record_doc';
          $db_date_col ='vehicle_maintnc_record_expire_dt';
          $flag=1;
        }
        else
        {
          $flag=0;
          $this->response(array("status"=>"error",  "msg"=> "Please select Vehicle maintenance doc and enter expiry date."), 200);
        }
      }
            // ------- VEHICLE INSURANCE -----------
      else if($doc_flag == "veh_insurance_doc")
      {
        if(isset($_FILES["veh_insurance_doc"]) && $_FILES["veh_insurance_doc"]["name"] != '' && $postData['veh_insurance_exp_dt']) 
        {
          $file_name = $_FILES['veh_insurance_doc']["name"];
          $exp_date = $postData['veh_insurance_exp_dt'];
          $db_doc_col ='vehicle_insurance_doc';
          $db_date_col ='vehicle_insurance_expire_dt';
          $flag=1;
        }
        else
        {
          $flag=0;
          $this->response(array("status"=>"error",  "msg"=> "Please select Vehicle insurance doc and enter expiry date."), 200);
        }
      }
           // ------- VEHICLE REGISTRATION -----------
      else if($doc_flag == "veh_registration_doc")
      {
        if(isset($_FILES["veh_registration_doc"]) && $_FILES["veh_registration_doc"]["name"] != '' && $postData['veh_registration_exp_dt']) 
        {
          $file_name = $_FILES['veh_registration_doc']["name"];
          $exp_date = $postData['veh_registration_exp_dt'];
          $db_doc_col ='vehicle_regi_doc';
          $db_date_col ='vehicle_regi_expire_dt';
          $flag=1;
        }
        else
        {
          $flag=0;
          $this->response(array("status"=>"error",  "msg"=> "Please select Vehicle registration doc and enter expiry date."), 200);
        }
      }


      if($flag == 1)
      {
        $date =date("Y_m_d_H_s_i");
        $random = rand(1, 10000000000000000);
        $makeRandom = $random;
        $file_name_rename = $makeRandom;
        $explode = explode('.', $file_name);
        if(count($explode) >= 2) 
        {
          $new_file = $file_name_rename.$date.'.'.$explode[1];
          $config['upload_path'] = './uploads/Driver/doc';
          $config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
          $config['file_name'] = $new_file;
          $this->load->library('upload',$config);
          $this->upload->initialize($config);
          if(!$this->upload->do_upload($doc_flag)) 
          {
            $error = $this->upload->display_errors();
            $this->response(array("status"=>"error",  "msg"=> strip_tags($error)),200);
          }else{
            $socialDoc = 'uploads/Driver/doc/'.$new_file; 
            $data_update = array($db_doc_col => $socialDoc,$db_date_col=>$exp_date);
            $updateq = $this->common_model->update(TB_USERS,$cond,$data_update);                
            if($updateq)
            {
              $this->response(array("status"=>"success",  "msg"=> "Document uploaded successfully."), 200);
            }
            else
            {
              $this->response(array("status"=>"error",  "msg"=> "Oops!!! Something went wrong."), 200);
            }

          }
        }
      }


    }


        // if(isset($_FILES["social_security_doc"]) && $_FILES["social_security_doc"]["name"] != '') 
        // {
        //   $file_name = $_FILES['social_security_doc']["name"];
        //   // $original_file_name = $file_name;
        //   $date =date("Y_m_d_H_s_i");
        //   $random = rand(1, 10000000000000000);
        //   $makeRandom = $random;
        //   $file_name_rename = $makeRandom;
        //   $explode = explode('.', $file_name);
        //   if(count($explode) >= 2) 
        //   {
        //       $new_file = $file_name_rename.$date.'.'.$explode[1];
        //       $config['upload_path'] = './uploads/Driver/doc';
        //       $config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
        //       $config['file_name'] = $new_file;
        //       $this->load->library('upload',$config);
        //       $this->upload->initialize($config);
        //       if(!$this->upload->do_upload('social_security_doc')) 
        //       {
        //         $error = $this->upload->display_errors();
        //         $this->response(array("status"=>"error",  "msg"=> strip_tags($error)),200);
        //       }else{
        //         $socialDoc = 'uploads/Driver/doc/'.$new_file; 
        //           $data_update = array('social_security_card_doc' => $socialDoc);
        //           $updateq = $this->common_model->update(TB_USERS,$cond,$data_update);
        //           if($updateq)
        //           {
        //             $this->response(array("status"=>"success",  "msg"=> "Social security card uploaded successfully."), 200);
        //           }
        //           else
        //           {
        //             $this->response(array("status"=>"error",  "msg"=> "Oops!!! Something went wrong."), 200);
        //           }

        //       }
        //   }         
        // }
        // else
        // {
        //   $this->response(array("status"=>"error",  "msg"=> "Please select social security card."), 200);
        // }
  }
  else
  {
    $this->response(array("status"=>"error",  "msg"=> "Invalid user id or user type."), 200);
  }
  }
  }



  public function driver_ride_list_post()
  {
      $postData = $_POST;
      $user_id = $postData['driver_id'];
      $ride_status = $postData['status'];
     // $user_type = $postData['user_type'];

      switch ($ride_status) {
        case 'completed':
        $cond = array('driver_id' =>$user_id,"sr_status"=>"2","sr_date"." >= " =>date('Y-m-d'));
        break;
        case 'scheduled':
        $cond = array('driver_id' =>$user_id,"sr_status"=>"1","sr_date"." >= " =>date('Y-m-d'));
        break;
        case 'cancelled':
        $cond = array('driver_id' =>$user_id,"sr_status"=>"3","sr_date"." >= " =>date('Y-m-d'));
        break;
        case 'pending':
        $cond = array('driver_id' =>$user_id,"sr_status"=>"1","admin_status"=>"1","sr_date"." >= " =>date('Y-m-d'));
        break;
        case 'running':
        $cond = array('driver_id' =>$user_id,"sr_status"=>"4","sr_date"." >= " =>date('Y-m-d'));
        break;
        default:
        $cond = array('driver_id' =>$user_id,"sr_status"=>"1","sr_date"." >= " =>date('Y-m-d'));
        break;
      }

      
      $result = $this->common_model->selectQuery("*",TB_SCHEDULE_RIDE,$cond,array('sr_date' => 'asc'),array(TB_CAR_CATEGORY => TB_CAR_CATEGORY.".carcat_id = ".TB_SCHEDULE_RIDE.".sr_desire_transportaion"),array());

      /*echo $this->db->last_query();

      exit();*/



      /*print_r($result);
      print_r($this->db->last_query());

      exit();
*/
      //echo $this->db->last_query();

      $resultData  = array();

      if(count($result) > 0)
      {
        $i = 0;
        foreach($result as $key => $res)
        {
            
            $memberData = $this->common_model->select("id,full_name,phone,address,user_type,signature",TB_USERS,array('id' => $res['user_id']));
            $driverData = $this->common_model->select("id,full_name,phone,address,user_type",TB_USERS,array('id' => $postData['driver_id']));

           // echo 'count'.count($memberDriverData);
            if(count($memberData) > 0 && count($driverData) > 0)
            { 
              $resultData[$i] =  $res;

              if(0 != $res['driver_id'])
              {

                if(1 == $memberData[0]['user_type'])
                {
                    $resultData[$i]['member']['id'] =  $memberData[0]['id'];
                    $resultData[$i]['member']['full_name'] =  $memberData[0]['full_name'];
                    $resultData[$i]['member']['phone'] =  $memberData[0]['phone'];
                    $resultData[$i]['member']['address'] =  $memberData[0]['address'];

                    if(1 == $memberData[0]['signature'])
                    {
                        $resultData[$i]['member']['signature'] = 'yes';
                    }
                    elseif(0 == $memberData[0]['signature'])
                    {
                        $resultData[$i]['member']['signature'] = 'no';
                    }

                    

                    $resultData[$i]['driver']['id'] =  $driverData[0]['id'];
                    $resultData[$i]['driver']['full_name'] =  $driverData[0]['full_name'];
                    $resultData[$i]['driver']['phone'] =  $driverData[0]['phone'];
                    $resultData[$i]['driver']['address'] =  $driverData[0]['address'];
                }
                else if(5 == $driverData[0]['user_type'])
                {

                  $resultData[$i]['member']['id'] =  $memberData[0]['id'];
                    $resultData[$i]['member']['full_name'] =  $memberData[0]['full_name'];
                    $resultData[$i]['member']['phone'] =  $memberData[0]['phone'];
                    $resultData[$i]['member']['address'] =  $memberData[0]['address'];

                    if(1 == $memberData[0]['signature'])
                    {
                        $resultData[$i]['member']['signature'] = 'yes';
                    }
                    elseif(0 == $memberData[0]['signature'])
                    {
                        $resultData[$i]['member']['signature'] = 'no';
                    }

                  $resultData[$i]['driver']['id'] =  $driverData[0]['id'];
                    $resultData[$i]['driver']['full_name'] =  $driverData[0]['full_name'];
                    $resultData[$i]['driver']['phone'] =  $driverData[0]['phone'];
                    $resultData[$i]['driver']['address'] =  $driverData[0]['address'];
                }

                $i++;
              }
            }


        }

        $this->response(array("status"=>"success",  "data"=> $resultData), 200);
      }
      else
      {
          $this->response(array("status"=>"error",  "data"=> "no data found"), 200);
      }

  }

  public function user_ride_list_post()
  {

      $postData = $_POST;
    $user_id = $postData['user_id'];
    $ride_status = $postData['status'];
    //$user_type = $postData['user_type'];
    if(empty($user_id) || empty($ride_status))
    {
      $this->response(array('status' =>"error" , "msg"=>"Please enter user id, user type and ride status."),200);
    }
    else
    {
      
      switch ($ride_status) {
        case 'completed':
        $cond = array('user_id' =>$user_id,"sr_status"=>"2","sr_date"." >= " =>date('Y-m-d'));
        break;
        case 'scheduled':
        $cond = array('user_id' =>$user_id,"sr_status"=>"1","sr_date"." >= " =>date('Y-m-d'));
        break;
        case 'cancelled':
        $cond = array('user_id' =>$user_id,"sr_status"=>"3","sr_date"." >= " =>date('Y-m-d'));
        break;
        case 'pending':
        $cond = array('user_id' =>$user_id,"sr_status"=>"1","admin_status"=>"1");
        break;
        case 'running':
        $cond = array('user_id' =>$user_id,"sr_status"=>"4","sr_date"." >= " =>date('Y-m-d'));
        break;
        default:
        $cond = array('user_id' =>$user_id,"sr_status"=>"1","sr_date"." >= " =>date('Y-m-d'));
        break;
      }

      

      $result = $this->common_model->selectQuery("*",TB_SCHEDULE_RIDE,$cond,array('sr_date' => 'asc'),array(),array());

      

      //selectQuery($sel,$table,$cond = array(),$orderBy=array(),$join=array(),$joinType=array())

     

      $resultData  = array();

      if(count($result) > 0)
      {
        $i = 0;
        foreach($result as $key => $res)
        {
            
            $memberData = $this->common_model->select("id,full_name,phone,address,user_type,signature",TB_USERS,array('id' => $user_id));
            $driverData = $this->common_model->select("id,full_name,phone,address,user_type",TB_USERS,array('id' => $res['driver_id']));

           // echo 'count'.count($memberDriverData);
            /*if(count($memberData) > 0 && count($driverData) > 0)
            { */
              $resultData[$i] =  $res;

              /*if(0 != $res['driver_id'])
              {*/

                if(1 == $memberData[0]['user_type'])
                {
                    $resultData[$i]['member']['id'] =  $memberData[0]['id'];
                    $resultData[$i]['member']['full_name'] =  $memberData[0]['full_name'];
                    $resultData[$i]['member']['phone'] =  $memberData[0]['phone'];
                    $resultData[$i]['member']['address'] =  $memberData[0]['address'];

                    if(1 == $memberData[0]['signature'])
                    {
                        $resultData[$i]['member']['signature'] = 'yes';
                    }
                    elseif(0 == $memberData[0]['signature'])
                    {
                        $resultData[$i]['member']['signature'] = 'no';
                    }

                    

                    $resultData[$i]['driver']['id'] =  $driverData[0]['id'];
                    $resultData[$i]['driver']['full_name'] =  $driverData[0]['full_name'];
                    $resultData[$i]['driver']['phone'] =  $driverData[0]['phone'];
                    $resultData[$i]['driver']['address'] =  $driverData[0]['address'];
                }
                else if(5 == $driverData[0]['user_type'])
                {

                  $resultData[$i]['member']['id'] =  $memberData[0]['id'];
                    $resultData[$i]['member']['full_name'] =  $memberData[0]['full_name'];
                    $resultData[$i]['member']['phone'] =  $memberData[0]['phone'];
                    $resultData[$i]['member']['address'] =  $memberData[0]['address'];

                    if(1 == $memberData[0]['signature'])
                    {
                        $resultData[$i]['member']['signature'] = 'yes';
                    }
                    elseif(0 == $memberData[0]['signature'])
                    {
                        $resultData[$i]['member']['signature'] = 'no';
                    }

                  $resultData[$i]['driver']['id'] =  $driverData[0]['id'];
                    $resultData[$i]['driver']['full_name'] =  $driverData[0]['full_name'];
                    $resultData[$i]['driver']['phone'] =  $driverData[0]['phone'];
                    $resultData[$i]['driver']['address'] =  $driverData[0]['address'];
                }

                $i++;
              //}
            //}


        }

        $this->response(array("status"=>"success",  "data"=> $resultData), 200);
      }
      else
      {
          $this->response(array("status"=>"error",  "data"=> "no data found"), 200);
      }

      
    } 



  }







  // ------- GET RIDE LIST BY STATUS ----------------
  public function ride_list_post()
  {
    $postData = $_POST;
    $user_id = $postData['user_id'];
    $ride_status = $postData['status'];
    $user_type = $postData['user_type'];
    if(empty($user_id) || empty($ride_status) || empty($user_type))
    {
      $this->response(array('status' =>"error" , "msg"=>"Please enter user id, user type and ride status."),200);
    }
    else
    {
      if($user_type == "user")
      {
        $id_type = "user_id";
      }else if($user_type == "driver")
      {
        $id_type = "driver_id";
      }
      switch ($ride_status) {
        case 'completed':
        $cond = array($id_type =>$user_id,"sr_status"=>"2","sr_date"." >= " =>date('Y-m-d'));
        break;
        case 'scheduled':
        $cond = array($id_type =>$user_id,"sr_status"=>"1","sr_date"." >= " =>date('Y-m-d'));
        break;
        case 'cancelled':
        $cond = array($id_type =>$user_id,"sr_status"=>"3","sr_date"." >= " =>date('Y-m-d'));
        break;
        case 'pending':
        $cond = array($id_type =>$user_id,"sr_status"=>"1","admin_status"=>"1","sr_date"." >= " =>date('Y-m-d'));
        break;
        case 'running':
        $cond = array($id_type =>$user_id,"sr_status"=>"4","sr_date"." >= " =>date('Y-m-d'));
        break;
        default:
        $cond = array($id_type =>$user_id,"sr_status"=>"1","sr_date"." >= " =>date('Y-m-d'));
        break;
      }

      

      $result = $this->common_model->selectQuery("*",TB_SCHEDULE_RIDE,$cond,array('sr_date' => 'asc'),array(),array());



      //selectQuery($sel,$table,$cond = array(),$orderBy=array(),$join=array(),$joinType=array())

     

      $resultData  = array();

      if(count($result) > 0)
      {
        $i = 0;
        foreach($result as $key => $res)
        {
            
            $memberData = $this->common_model->select("id,full_name,phone,address,user_type,signature",TB_USERS,array('id' => $res['user_id']));
            $driverData = $this->common_model->select("id,full_name,phone,address,user_type",TB_USERS,array('id' => $res['driver_id']));

           // echo 'count'.count($memberDriverData);
            if(count($memberData) > 0 && count($driverData) > 0)
            { 
              $resultData[$i] =  $res;

              if(0 != $res['driver_id'])
              {

                if(1 == $memberData[0]['user_type'])
                {
                    $resultData[$i]['member']['id'] =  $memberData[0]['id'];
                    $resultData[$i]['member']['full_name'] =  $memberData[0]['full_name'];
                    $resultData[$i]['member']['phone'] =  $memberData[0]['phone'];
                    $resultData[$i]['member']['address'] =  $memberData[0]['address'];

                    if(1 == $memberData[0]['signature'])
                    {
                        $resultData[$i]['member']['signature'] = 'yes';
                    }
                    elseif(0 == $memberData[0]['signature'])
                    {
                        $resultData[$i]['member']['signature'] = 'no';
                    }

                    

                    $resultData[$i]['driver']['id'] =  $driverData[0]['id'];
                    $resultData[$i]['driver']['full_name'] =  $driverData[0]['full_name'];
                    $resultData[$i]['driver']['phone'] =  $driverData[0]['phone'];
                    $resultData[$i]['driver']['address'] =  $driverData[0]['address'];
                }
                else if(5 == $driverData[0]['user_type'])
                {

                  $resultData[$i]['member']['id'] =  $memberData[0]['id'];
                    $resultData[$i]['member']['full_name'] =  $memberData[0]['full_name'];
                    $resultData[$i]['member']['phone'] =  $memberData[0]['phone'];
                    $resultData[$i]['member']['address'] =  $memberData[0]['address'];

                    if(1 == $memberData[0]['signature'])
                    {
                        $resultData[$i]['member']['signature'] = 'yes';
                    }
                    elseif(0 == $memberData[0]['signature'])
                    {
                        $resultData[$i]['member']['signature'] = 'no';
                    }

                  $resultData[$i]['driver']['id'] =  $driverData[0]['id'];
                    $resultData[$i]['driver']['full_name'] =  $driverData[0]['full_name'];
                    $resultData[$i]['driver']['phone'] =  $driverData[0]['phone'];
                    $resultData[$i]['driver']['address'] =  $driverData[0]['address'];
                }

                $i++;
              }
            }


        }

        $this->response(array("status"=>"success",  "data"=> $resultData), 200);
      }
      else
      {
          $this->response(array("status"=>"error",  "data"=> "no data found"), 200);
      }

      
    }  
  }


  // ------- GET RIDE DETAILS BY STATUS ----------------
  public function ride_details_post()
  {
    $postData = $_POST;
    $user_id = $postData['user_id'];
    $ride_status = $postData['status'];
    $ride_id = $postData['ride_id'];
    if(empty($user_id) || empty($ride_id) || empty($ride_status))
    {
      $this->response(array('status' =>"error" , "msg"=>"Please enter valid user id, ride id and ride status."),200);
    }
    else
    {
      switch ($ride_status) {
        case 'completed':
        $cond = array("user_id" =>$user_id,"id" =>$ride_id,"sr_status"=>"2");
        break;
        case 'scheduled':
        $cond = array("user_id" =>$user_id,"id" =>$ride_id,"sr_status"=>"1");
        break;
        case 'cancelled':
        $cond = array("user_id" =>$user_id,"id" =>$ride_id,"sr_status"=>"3");
        break;
        default:
        $cond = array("user_id" =>$user_id,"id" =>$ride_id,"sr_status"=>"1");
        break;
      }
      $result = $this->common_model->select("*",TB_SCHEDULE_RIDE,$cond);
      if(count($result) > 0)
      {
        $this->response(array("status"=>"success",  "data"=> $result), 200);
      }
      else
      {
        $this->response(array("status"=>"error",  "data"=> "Data not found"), 200);
      }
    }
  }


  // ----------- COMPLAINT TYPE LIST -----------
  public function complaint_type_list_get()
  {
    $postData = $_POST;
    $result = $this->common_model->select("*",TB_COMPLAINTS_TYPE);
    if(count($result) > 0)
    {
      $this->response(array("status"=>"success",  "data"=> $result), 200);
    }
    else
    {
      $this->response(array("status"=>"error",  "message"=> "Data not found"), 200);
    }
  }

  // --------- SAVE COMPLAINT -------------
  public function save_complaint_post()
  {
    $postData = $_POST;
    $user_id=$postData['user_id'];
    $ride_id=$postData['ride_id'];
    $username = $postData['user_name'];
    $drivername = $postData['driver_name'];
    $complaint_type = $postData['complaint_type'];
    $date_of_service = $postData['date_of_service'];
    $transport_provider = $postData['transport_provider'];
    $description  = $postData['description'];
    if(empty($user_id) || empty($ride_id))
    {
      $this->response(array("status"=>"error",  "message"=> "User id or Ride id not found."), 200);
    }
    else if(empty($username) || empty($drivername) || empty($complaint_type) || empty($date_of_service) || empty($transport_provider) || empty($description))
    {
      $this->response(array("status"=>"error",  "message"=> "Please enter values for all fields."), 200);
    } 
    else
    {
      $insertData=array("user_id"=>$user_id,
        "ride_id"=>$ride_id,
        "src_username"=>$username,
        "src_driver_name"=>$drivername,
        "src_complaint_type"=>$complaint_type,
        "src_date_of_service"=>$date_of_service,
        "src_name_of_transportation_provider"=>$transport_provider,
        "src_description"=>$description,
        "src_created_on"=>date("Y-m-d H:i:s"));
      $result = $this->common_model->insert(TB_SCHEDULE_RIDE_COMPLAINTS,$insertData);
      if($result)
      {
        $this->response(array("status"=>"success",  "message"=> "Complaint saved successfully."), 200);
      }else
      {
        $this->response(array("status"=>"error",  "message"=> "Oops!!! Something goes wrong."), 200);
      }
    }

  }




  public function hotelList_post()
  {
   $flag = 0; 
   $entity_type = "";
   $entity_id = "";

   if("" == $this->input->post('location'))
   {
     $this->response(array('status' => 'success','message'=>'please select location'));
   }
   else
   {
     $location = $this->input->post('location');
     $curl = curl_init();
     $curl1 = curl_init();

     curl_setopt_array($curl, array(
      CURLOPT_URL => "https://developers.zomato.com/api/v2.1/locations?query=".$location,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "user-key:".Zomato_key
        ),
      ));

     $response = curl_exec($curl);

     $err = curl_error($curl);

     curl_close($curl);

     if ($err) 
     {
      $this->response(array('status'=>'success','message' => 'no data found'));
    }
    else
    {
     $flag = 1;
     $this->response(array('status'=>'success','data' => $response));
   }


   if(1 == $flag)
   {
    $data = json_decode($response);


    $entity_id = $data->location_suggestions[0]->entity_id;
    $parameters = "";

    $parameters .= "&entity_type=".$data->location_suggestions[0]->entity_type;

    if("" != $this->input->post('query'))
    {

     $parameters .= "&q=".$this->input->post('query');
   }

   curl_setopt_array($curl1, array(
     CURLOPT_URL => "https://developers.zomato.com/api/v2.1/search?entity_id=".$entity_id.$parameters,
     CURLOPT_RETURNTRANSFER => true,
     CURLOPT_ENCODING => "",
     CURLOPT_MAXREDIRS => 10,
     CURLOPT_TIMEOUT => 30,
     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
     CURLOPT_CUSTOMREQUEST => "GET",
     CURLOPT_HTTPHEADER => array(
      "accept: application/json",
      "cache-control: no-cache",
      "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
      "user-key: ".Zomato_key
      ),
     ));

   $response1 = curl_exec($curl1);

   $data = json_decode($response1);
   $err = curl_error($curl1);

   curl_close($curl);

   if ($err) 
   {
     $this->response(array('status'=>'error','message' => $err));

   }
   else
   {
  	         //$this->response(array('status'=>'success','data' => $response1));
    $this->response(array('status'=>'success','data' => $data));
  }



  }


  }
  }


  // ---------- search all types of service for member --------------
  public function searchMemberService_post()
  {
    $postData = $_POST;
    $member_service_id=trim($postData['member_service_id']);
    $location=trim($postData['location']);
    $query=trim($postData['query']);
    $getservice = $this->Common_model->select("ms_type",TB_MEMBER_SERVICE,array('ms_id' =>$member_service_id));
    $ms_service = $getservice[0]['ms_type'];
    $locationq = urlencode($query.' '.$ms_service.' in '.$location);
    $result = file_get_contents('https://maps.googleapis.com/maps/api/place/textsearch/json?query='.$locationq.'&key='.GOOGLE_API_KEY);
    $output= json_decode($result,true);
    if(empty($result))
    {
      $this->response(array('status' =>'error','msg'=>"No data found."));
    }
    else
    {
      $this->response(array('status' =>'success','data'=>$output));  
    }
  }



    //------------- save driver updated locations ----------------
  public function saveDriverLocation_post()
  {
    $postData = $_POST;
    $user_id=trim($postData['user_id']);
    $lat=trim($postData['lat']);
    $long=trim($postData['long']);



    if($user_id=="" || $lat == "" || $long == "")
    {
      $this->response(array("status" => "error", "message"=>"Please enter all required fields."),200);
    }else
    {

      $checkDriverType =  $this->common_model->select('id,user_type',TB_USERS,array('id' => $user_id));

      if(5 == $checkDriverType[0]['user_type'])
      {

        $cond = array("user_id"=>$user_id);
        $check_existsq = $this->common_model->select("*",TB_DRIVER_LOCATION,$cond);
        if(count($check_existsq) > 0 )
        {
              // ------ UPDATE LOCATION -----------
          $update_data=array("loc_lat"=>$lat,
            "loc_long"=>$long);
          $result = $this->common_model->update(TB_DRIVER_LOCATION,$cond,$update_data);
        }else
        {
              // ------- INSERT LOCATION ------------
          $insert_data=array("user_id"=>$user_id,
            "loc_lat"=>$lat,
            "loc_long"=>$long,
            "created_at"=>date("Y-m-d H:i:s"));
          $result = $this->common_model->insert(TB_DRIVER_LOCATION,$insert_data);
        }

        if($result)
        {
          $this->response(array("status" => "success", "message"=>"Location updated successfully."),200);
        }else
        {
          $this->response(array("status" => "error", "message"=>"Oops!!! Something went wrong."),200);
        }
      }
      else
      {
          $this->response(array("status" => "error", "message" => "Invalid user type"));
      }
    }
  }

  public function book_ride_post()
  {

    if("" == $this->input->post('user_id'))
    {
     $this->response(array('status' => 'error','msg' =>'please enter user id'));
   }

   if("" == $this->input->post('user_type'))
   {
     $this->response(array('status' => 'error','msg' =>'please select user type'));
   } 

   $query = $this->db->query("SELECT id, sr_trip_id FROM tbl_schedule_ride ORDER BY id DESC LIMIT 1");
   $result = $query->result_array();
   $tripID=explode("T", $result[0]['sr_trip_id']);
   $tripIDGenerated = "T".str_pad($tripID[1]+1, 14, '0', STR_PAD_LEFT);

   if("2" == $this->input->post('user_type'))
   {


     if("" == $this->input->post('date_of_service'))
     {
      $this->response(array('status' => 'error','msg' =>'please enter date_of_service'));
     }
     if("" == $this->input->post('pick_up_time'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter pick up time'));
     }
     if("" == $this->input->post('street'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter street'));
     }

     if("" == $this->input->post('distance'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter total distance'));
     }
     if("" == $this->input->post('appointment_time'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter appointment  time'));
     }

     if("" == $this->input->post('type_of_service'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter type_of_service'));
     } 
     if("" == $this->input->post('special_request'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter special_request'));
     }
     if("" == $this->input->post('special_instrutions'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter special instrution'));
     }

     if("" == $this->input->post('desired_transportation'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter desired transportation'));
     }

     if("" == $this->input->post('ride_type'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter ride type'));
     }

     if("" == $this->input->post('source_address'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter source  address'));
     }

     if("" == $this->input->post('destination_address'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter destination addres'));
     }

     if("" == $this->input->post('source_lat'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter source lat'));
     }

     if("" == $this->input->post('source_long'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter source long'));
     }

     if("" == $this->input->post('destination_lat'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter destination lat'));
     }

     if("" == $this->input->post('destination_long'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter destination long'));
     }

     if("" == $this->input->post('urgency_type'))
     {
       $this->response(array('status' => 'error','msg' =>'please enter urgency_type'));
     }

   $data = array(
    'sr_date' => date("Y-m-d",strtotime($this->input->post('date_of_service'))),
    'sr_pick_up_time' => $this->input->post('pick_up_time'),
    'sr_appointment_time' => $this->input->post('appointment_time'), 
    'sr_street' => trim($this->input->post('street')),
    'sr_country' => $this->input->post('country'),
    'sr_state' => $this->input->post('state'),
    'sr_zipcode' => trim($this->input->post('zipcode')),
    'sr_trip_id' => $tripIDGenerated,
    'sr_health_service'=>$this->input->post('type_of_service'),
    'sr_total_distance'=>$this->input->post('distance'),
    'duration'=>$this->input->post('duration'),
    'sr_special_request' => $this->input->post('special_request'),                           
    'sr_special_instruction' => trim($this->input->post('special_instrutions')),
    'sr_desire_transportaion' => $this->input->post('desired_transportation'),                                
    'user_id'=>$this->input->post('user_id'),
    'sr_urgency_type'=>$this->input->post('urgency_type'),
    'sr_trip_type'=>$this->input->post('ride_type'),
    //'created_on'=>date("Y-m-d H:i:a"),
    'sr_status'=>1,
    'admin_status'=>1,
    'source_address'=>trim($this->input->post('source_address')),
    'destination_address'=>trim($this->input->post('destination_address')),
    'sr_lat_lng_source'=>trim($this->input->post('source_lat').",".$this->input->post('source_long')),
    'sr_lat_lng_destination'=>trim($this->input->post('destination_lat').",".$this->input->post('destination_long')),
    'urjency_explainatin'=>$this->input->post('urgency_explanation')
    );

   $query  =$this->common_model->insert(TB_SCHEDULE_RIDE,$data);
   if($query)
   {
     $this->response(array('status'=>'success','trip_id'=>$tripIDGenerated,'msg' => 'Your Ride Schedule Successfully !!'));

   }
   else
   {
    $this->response(array('status'=>'error','msg' => 'error in NMT user insertion'));
  }

  } 
  else
  {


    $insertArr = array(
      'sr_trip_id'=> $tripIDGenerated,
      'sr_date'=> date("Y-m-d"),
      'sr_pick_up_time'=> date("H:i:A"),          
      'user_id'=>$this->input->post('user_id'),
      'source_address'=> trim($this->input->post('source_address')),
      'destination_address'=> trim($this->input->post('destination_address')),
      'sr_lat_lng_source'=> trim($this->input->post('source_lat').",".$this->input->post('source_long')),
      'sr_lat_lng_destination'=>trim($this->input->post('destination_lat').",".$this->input->post('destination_long')),
      'sr_total_distance'=> trim($this->input->post('distance')),
      'duration'=> trim($this->input->post('duration')),
      'created_on'=>date("Y-m-d H:i:s"),
      'driver_status'=>"1",
      'admin_status'=>"2"
      );



    if($this->common_model->insert(TB_SCHEDULE_RIDE,$insertArr))
    {
      $sched_id=$this->db->insert_id();
      $query = $this->db->query("SELECT * FROM  tbl_driver_location WHERE `updated_at` >= CURDATE( )");

      //$query = $this->db->query("SELECT * FROM  tbl_driver_location WHERE `updated_at` >= CURDATE( ) AND user_id != $driver_id");
      $getDriverLocation = $query->result_array();

      if(count($query) > 0)
      {
        foreach ($getDriverLocation as $key => $value) {

          $getDistance = $this->distance($this->input->post('source_lat'), $this->input->post('source_long'), $value['loc_lat'], $value['loc_long'], "M");

          //print_r(round($getDistance));
          if(round($getDistance)<=5)
          {
            $this->Common_model->insert(TB_DRIVER_REQUEST,
              array("sched_id"=>$sched_id,"driver_id"=>$value['user_id'],"created_date"=>date("Y-m-d H:i:s"))); 

            $driverArr = $this->common_model->select('device_token',TB_USERS,array('id'=>$value['user_id']));

            
            //print_r($driverArr);
            //$driverDeviceToken[] = $driverArr[0]['device_token'];

            $userData  = $this->common_model->select('user_type,full_name',TB_USERS,array('id' => $this->input->post('user_id')));

            $message = $userData[0]['full_name']." has booked a ride"; 

            $schedRideData = $this->common_model->select('id,sr_trip_id,user_id',TB_SCHEDULE_RIDE,array('id' => $sched_id));

            $data = array("schedule_id " => $schedRideData[0]['id'] ,"user_type" => $userData[0]['user_type'], "trip_id" => $schedRideData[0]['sr_trip_id'], "user_id" => $schedRideData[0]['user_id'],'notification_type' => 'Book a ride');

            $this->sendNotification($driverArr[0]['device_token'],$message,$data);

          }
        } 
        $this->response(array('status'=>'success','trip_id'=>$tripIDGenerated,'msg' => 'Your Ride Schedule Successfully !!'));
      }

      

    }
    else
    {
      $this->response(array('status'=>'error','msg' => 'error in member insertion'));
    }

  }



  }

  public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
    $theta = $lon1 - $lon2;
    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
    $dist = acos($dist);
    $dist = rad2deg($dist);
    $miles = $dist * 60 * 1.1515;
    $unit = strtoupper($unit);
    if ($unit == "K") {
      return ($miles * 1.609344);
    } else if ($unit == "N") {
      return ($miles * 0.8684);
    } else {
      return $miles;
    }
  }


  // ------ healthcare service ------------
  public function healthcare_services_get()
  {
    $result = $this->common_model->select("*",TB_HEALTHCARE_SERVICES);
    if(count($result) > 0)
    {
      $this->response(array("status"=>"success",  "data"=> $result), 200);
    }
    else
    {
      $this->response(array("status"=>"error",  "message"=> "Data not found"), 200);
    }
  }

  // ------ member service ------------
  public function member_services_get()
  {
    $result = $this->common_model->select("*",TB_MEMBER_SERVICE,array("is_active"=>"1"));
    if(count($result) > 0)
    {
      $this->response(array("status"=>"success",  "data"=> $result), 200);
    }
    else
    {
      $this->response(array("status"=>"error",  "message"=> "Data not found"), 200);
    }
  }

  // ------ member desired transportation ------------
  public function member_desired_transport_get()
  {
    $result = $this->common_model->select("*",TB_CAR_CATEGORY);
    if(count($result) > 0)
    {
      $this->response(array("status"=>"success",  "data"=> $result), 200);
    }
    else
    {
      $this->response(array("status"=>"error",  "message"=> "Data not found"), 200);
    }
  }



  /*public function driver_tracking_post()
  {
  	if("" == trim($this->input->post('source_address')))
  	{
  		$this->response();
  	}
  }*/

  // Driver accept the request
  public function driverAcceptReuest_post()
  {    
   if("" == $this->input->post('schedule_id'))
   {
    $this->response(array("status" => "error" , "message" => "please enter schedule id"));
   }

  if("" == $this->input->post('driver_id'))
  {
    $this->response(array("status" => "error" , "message" => "please enter driver id"));
  }

  

    $tripExist  = $this->common_model->select('id',TB_SCHEDULE_RIDE,array("id" => $this->input->post('schedule_id'),'driver_status'=>2));

    if(count($tripExist) > 0)
    {
     $this->response(array("status" => "error", "message" => "Trip is already exist"));
    }
    else
    {
       $data = array(

        'driver_id' => $this->input->post('driver_id'),
        'driver_status'=>2,
        'updated_on'=>date("Y-m-d H:i:s")
        );

       $updateSchedule = $this->common_model->update(TB_SCHEDULE_RIDE,array('id' => $this->input->post('schedule_id')),$data);

        

        if($updateSchedule)
        {
          $result = $this->common_model->delete(TB_DRIVER_REQUEST,array('sched_id' => $this->input->post('schedule_id')));



          /*if(1 ==$this->input->post('user_type'))
          {
              $deviceToken = $this->common_model->select('device_token',TB_USERS,array('id' => $this->input->post('member_id')));
          }
          elseif(2 ==$this->input->post('user_type'))
          {
              $deviceToken = $this->common_model->select('device_token',TB_USERS,array('id' => $this->input->post('nemt_id')));
          }*/

         $deviceToken = $this->common_model->select('device_token',TB_USERS,array('id' => $this->input->post('user_id')));

          
          $driverArr = $this->common_model->select('user_type,full_name',TB_USERS,array('id' => $this->input->post('driver_id')));

          $tripID = $this->common_model->select('sr_trip_id',TB_SCHEDULE_RIDE,array('id' => $this->input->post('schedule_id')));

          

          if(count($deviceToken) > 0 && count($driverArr) > 0 && count($tripID))
          {

            $message = $driverArr[0]['full_name']." has accepted your ride request";

            $data = array("schedule_id " => $this->input->post('schedule_id') ,"user_type" => $driverArr[0]['user_type'], "trip_id" => $tripID[0]['sr_trip_id'], "user_id" => $this->input->post('member_id'),'notification_type' => 'Accept request');

            $this->sendNotification($deviceToken[0]['device_token'],$message,$data);

            if($result)
            {
              $this->response(array("status" => "success", "message" => "Ride request has been accepeted successfully"));  
            }
            else
            {
              $this->response(array("status" => "error", "message" => "Oops!!! Somthing went wrong.")); 
            }

          }
          else
          {
            $this->response(array("status" => "error", "message" => "no data found.")); 
          }

        }
    }

  }

  public function driverRejectReuest_post()
  {

    if("" == $this->input->post('schedule_id'))
    {
      $this->response(array('status' => 'error','message' => 'please enter schedule id'));
    }

    if("" == $this->input->post('driver_id'))
    {
      $this->response(array('status' => 'error','message' => 'please enter driver id')); 
    }

    $result = $this->common_model->delete(TB_DRIVER_REQUEST,array('sched_id' =>$this->input->post('schedule_id'),'driver_id'=>$this->input->post('driver_id')));

    if($result)
    {
      $this->response(array("status"=>"success","message"=>"Ride request has been rejected successfully."));
    }
    else
    {
      $this->response(array("status"=>"error","message"=>"Oops!!! Somthing went wrong."));
    }
  }

  public function confirmDriverRequest_post()
  {


      /*print_r($this->input->post());

      exit();*/

      if("" == $this->input->post('schedule_id'))
      {
        //echo "test";
        $this->response(array('status' => 'error', 'message' => 'please enter schedule id'),200);
      }

      /*if("" == $this->input->post('member_id'))
      {
        $this->response(array('status' => 'error' ,'message' => 'please enter member id'),200);
      }*/

      if("" == $this->input->post('driver_id'))
      {
        $this->response(array('status' => 'error' ,'message' => 'please enter driver id'),200);  

      }

      $alreadyConfirm = $this->common_model->select('id',TB_SCHEDULE_RIDE,array('id' => $this->input->post('schedule_id'),'user_id'=>$this->input->post('user_id'),'driver_status'=>'3'));


      /*echo count($alreadyConfirm[0]);
      print_r($alreadyConfirm);

      exit();*/

      if(count($alreadyConfirm[0]) > 0)
      {
       $this->response(array('status' => "error", "message"=>"you already confirm this request."),200);

         //exit();
      }
      $id = 0;

     $data = array('driver_status' => 3);

     /*if(1 == $this->input->post('user_type'))
     {
        $id = $this->input->post('member_id');
     }
     elseif(2 == $this->input->post('user_type'))
     {
        $id = $this->input->post('nemt_id');
     }*/

     $id = $this->input->post('user_id');

     $result = $this->common_model->update(TB_SCHEDULE_RIDE,array('id' => $this->input->post('schedule_id'),'user_id' => $id),$data);




     if($result)
     {

      $driverData  = $this->common_model->select('user_type , device_token',TB_USERS,array('id' => $this->input->post('driver_id')));

     /* print_r($driverData);

      exit();*/

      $memberData = $this->common_model->select('full_name',TB_USERS,array('id' => $id));

      $tripId = $this->common_model->select('sr_trip_id',TB_SCHEDULE_RIDE,array('id' => $this->input->post('schedule_id')));

      if(count($driverData) > 0 && count($memberData) > 0 && count($tripId) > 0)
      {
        $message = $memberData[0]['full_name']." has confirm your ride request"; 

        $data = array("schedule_id " => $this->input->post('schedule_id') ,"user_type" => $driverData[0]['user_type'], "trip_id" => $tripId[0]['sr_trip_id'], "user_id" => $this->input->post('driver_id'),'notification_type' => 'Confirmation request');


        $this->sendNotification($driverData[0]['device_token'],$message,$data);

        $this->response(array('status' => "success", "message"=>"Driver is confirmed successfully."),200);
      }
      else
      {
        $this->response(array('status' => "error", "message"=>"no data found."),200);
      }
    }
    else
    {
      $this->response(array('status' =>'error','message'=>"Oops!!! Something went wrong."));
    }
  }

  public function rejectDriver_post()
  {
    $tmp = 0;
    if("" == $this->input->post('schedule_id'))
    {
      $this->response(array('status' => 'error' ,'message' => 'enter schedule id'));
    }

    if("" == $this->input->post('driver_id'))
    {
      $this->response(array('status' => 'error' ,'message' => 'enter driver id')); 
    }

    $gettripdetails = $this->common_model->select("*",TB_SCHEDULE_RIDE,array("id"=>$this->input->post('schedule_id')));

    if(count($gettripdetails) > 0)
    {
      $driver_id = $gettripdetails[0]['driver_id'];
      $latlong = $gettripdetails[0]['sr_lat_lng_source']; 

      $newlatlong = explode(",",$latlong);
      $src_lat = $newlatlong[0];
      $src_long = $newlatlong[1];  

      $up_cond = array('id' => $this->input->post('schedule_id'),'user_id'=>$this->input->post('member_id'));
      $update_data = array('driver_status'=>'1','driver_id'=>'0');
      $result = $this->common_model->update(TB_SCHEDULE_RIDE,$up_cond,$update_data);

          //$query = $this->db->query("SELECT * FROM  tbl_driver_location WHERE `updated_at` >= CURDATE( ) AND user_id != $driver_id");

      $getDriverLocation  = $this->common_model->getDriverLocation('*',TB_DRIVER_LOCATION,array($driver_id));


      if(count($getDriverLocation) > 0)
      {

        foreach ($getDriverLocation as $key => $value) 
        {
          $getDistance = $this->distance($src_lat, $src_long, $value['loc_lat'], $value['loc_long'], "M");
          if(round($getDistance)<=5)
          {
            $insertDriver = $this->common_model->insert(TB_DRIVER_REQUEST,
              array("sched_id"=>$this->input->post('schedule_id'),"driver_id"=>$value['user_id'],"created_date"=>date("Y-m-d H:i:s"))); 

            if($insertDriver)
            {

              $driverArr = $this->common_model->select('user_type,device_token',TB_USERS,array('id' => $value['user_id']));

              $tripId = $this->common_model->select('sr_trip_id',TB_SCHEDULE_RIDE,array('id' => $this->input->post('schedule_id')));

              $memberData = $this->common_model->select('full_name',TB_USERS,array('id' => $this->input->post('member_id')));

              if(count($driverArr[0]) > 0 && count($tripId[0]) > 0 && count($memberData) > 0)
              {

                $message  = $memberData[0]['full_name']." has reject a ride request";

                $data = array("schedule_id " => $this->input->post('schedule_id') ,"user_type" => $$driverArr[0]['user_type'], "trip_id" => $tripId[0]['sr_trip_id'], "user_id" => $value['user_id'],'notification_type' => 'Book a ride');

                $this->sendNotification($driverArr[0]['device_token'],$message,$data);

                $tmp = 1;
              }
            }
            else
            {
              $tmp = 0;
            }

          }

        }


        if(1 == $tmp)
        {
          $this->response(array('status' => 'success', 'message'=>'Your schedule request has been sent successfully .'));
        }
        else
        {
          $this->response(array('status' => 'error', 'message'=>'Your schedule request has been failed .'));

        }

      }

      else
      {
        $this->response(array('status' => 'error' , 'message' => 'no driver found'));
      }
    }
    else
    {
      $this->response(array('status' => 'error','message' => 'no data found'));
    }

  }


  /*public function nemtConfirmDriverRequest_post()
  {

      if("" == $this->input->post('schedule_id'))
      {
        //echo "test";
        $this->response(array('status' => 'error', 'message' => 'please enter schedule id'),200);
      }

      if("" == $this->input->post('nemt_id'))
      {
        $this->response(array('status' => 'error' ,'message' => 'please enter nmt id'),200);
      }

      if("" == $this->input->post('driver_id'))
      {
        $this->response(array('status' => 'error' ,'message' => 'please enter driver id'),200);
      }

      $alreadyConfirm = $this->common_model->select('id',TB_SCHEDULE_RIDE,array('id' => $this->input->post('schedule_id'),'user_id'=>$this->input->post('nemt_id'),'driver_status'=>'3'));

      

      if(count($alreadyConfirm[0]) > 0)
      {
       $this->response(array('status' => "error", "message"=>"you already confirm this request."),200);

         //exit();
      }

     $data = array('driver_status' => 3);

     $result = $this->common_model->update(TB_SCHEDULE_RIDE,array('id' => $this->input->post('schedule_id'),'user_id' => $this->input->post('nmt_id')),$data);

    if($result)
    {

      $driverData  = $this->common_model->select('user_type , device_token',TB_USERS,array('id' => $this->input->post('driver_id')));

      $nmtData = $this->common_model->select('full_name',TB_USERS,array('id' => $this->input->post('nmt_id')));

      $tripId = $this->common_model->select('sr_trip_id',TB_SCHEDULE_RIDE,array('id' => $this->input->post('schedule_id')));

      if(count($driverData) > 0 && count($nmtData) > 0 && count($tripId) > 0)
      {
        $message = $nmtData[0]['full_name']." has confirm your ride request"; 

        $data = array("schedule_id " => $this->input->post('schedule_id') ,"user_type" => $driverData[0]['user_type'], "trip_id" => $tripId[0]['sr_trip_id'], "user_id" => $this->input->post('driver_id'),'notification_type' => 'nmt Confirmation request');


        $this->sendNotification($driverData[0]['device_token'],$message,$data);

        $this->response(array('status' => "success", "message"=>"Driver is confirmed successfully."),200);
      }
      else
      {
        $this->response(array('status' => "error", "message"=>"no data found."),200);
      }
    }
    else
    {
       $this->response(array('status' => "error", "message"=>"something went wrong !!."),200);
     }
  }

  public function nemtRejectDriverRequest_post()
  {
    $tmp = 0;
    if("" == $this->input->post('schedule_id'))
    {
      $this->response(array('status' => 'error' ,'message' => 'enter schedule id'));
    }

    if("" == $this->input->post('driver_id'))
    {
      $this->response(array('status' => 'error' ,'message' => 'enter driver id')); 
    }

    $gettripdetails = $this->common_model->select("*",TB_SCHEDULE_RIDE,array("id"=>$this->input->post('schedule_id')));

    if(count($gettripdetails) > 0)
    {
      $driver_id = $gettripdetails[0]['driver_id'];
      $latlong = $gettripdetails[0]['sr_lat_lng_source']; 

      $newlatlong = explode(",",$latlong);
      $src_lat = $newlatlong[0];
      $src_long = $newlatlong[1];  

      $up_cond = array('id' => $this->input->post('schedule_id'),'user_id'=>$this->input->post('nemt_id'));
      $update_data = array('driver_status'=>'1','driver_id'=>'0');
      $result = $this->common_model->update(TB_SCHEDULE_RIDE,$up_cond,$update_data);

      if($result)
      {
         $this->response(array('status' => 'success', 'message'=>'Your schedule request has been sent successfully .'));
      }
      else
      {
         $this->response(array('status' => 'error', 'message'=>'Your schedule request has been failed .'));
      }

    }
    else
    {
      $this->response(array('status' => 'error','message' => 'no data found'));
    }

  }*/




  public function sendNotification($device_id,$message,$data)
  {
    $content = array(
      "en" => $message
      );
    
    $fields = array(
      'app_id' => onesignalKey,
      'include_player_ids' =>(array)$device_id,
      'data' => $data,
      'contents' => $content
      );
    
    $fields = json_encode($fields);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
     'Authorization: Basic NGEwMGZmMjItY2NkNy0xMWUzLTk5ZDUtMDAwYzI5NDBlNjJj'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);
  }

  //-------- get driver open request ----------
  public function getDriverRideRequest_post()
  {
    $postData = $_POST;
    $driver_id  = $postData['driver_id'];
    if(empty($driver_id))
    {
      $this->response(array('status' =>"error" , "msg"=>"Please enter driver id."),200);
    }else
    {
      $cond = array(TB_DRIVER_REQUEST.".driver_id" => $driver_id,TB_SCHEDULE_RIDE.".driver_status"=>"1");
     $jointype=array(TB_SCHEDULE_RIDE=>"LEFT",TB_USERS=>"LEFT");
     $join = array(TB_SCHEDULE_RIDE=>TB_SCHEDULE_RIDE.".id = ".TB_DRIVER_REQUEST.".sched_id",
      TB_USERS=>TB_USERS.".id = ".TB_SCHEDULE_RIDE.".user_id");
     $ride_request = $this->Common_model->selectQuery(TB_SCHEDULE_RIDE.'.id as sched_id,'.TB_SCHEDULE_RIDE.'.sr_trip_id,driver_req_id,'.TB_SCHEDULE_RIDE.'.user_id,full_name,sr_date,sr_pick_up_time,source_address,destination_address,sr_total_distance,sr_total_cost,duration,sr_lat_lng_source,sr_lat_lng_destination,phone',TB_DRIVER_REQUEST,$cond,array("sr_date"=>"DESC"),$join,$jointype); 

     if(count($ride_request) > 0)
     {
      $this->response(array('status' =>"success" , "data"=>$ride_request),200);
    }else
    {
      $this->response(array('status' =>"error" , "msg"=>"Data not found."),200);
    }
  }
  }


  // Save the USer Current location
  public function saveUserLocation_post()
  {
      if("" == $this->input->post('user_id'))
      {
         $this->response(array('status' => 'error' , 'message' => 'please enter user id'));
      }

      if("" == $this->input->post('lat'))
      {
        $this->response(array('status' => 'error' , 'message' => 'please enter latitude')); 
      }

      if("" == $this->input->post('long'))
      {
        $this->response(array('status' => 'error' , 'message' => 'please enter longitude'));  
      }



     // $cond = array("user_id"=>$user_id);
      $UserExist = $this->common_model->select("*",TB_DRIVER_LOCATION,array("user_id" => $this->input->post('user_id')));
      if(count($UserExist) > 0 )
      {
            // ------ UPDATE LOCATION -----------
        $update_data=array("loc_lat"=>$this->input->post('lat'),
          "loc_long"=>$this->input->post('long'));
        $result = $this->common_model->update(TB_DRIVER_LOCATION,array("user_id"=>$this->input->post('user_id')),$update_data);
      }else
      {
            // ------- INSERT LOCATION ------------
        $insert_data=array("user_id"=>$this->input->post('user_id'),
          "loc_lat"=>$this->input->post('lat'),
          "loc_long"=>$this->input->post('long'),
          "created_at"=>date("Y-m-d H:i:s"));
        $result = $this->common_model->insert(TB_DRIVER_LOCATION,$insert_data);
      }

      if($result)
      {
        $this->response(array("status" => "success", "message"=>"Location updated successfully."),200);
      }else
      {
        $this->response(array("status" => "error", "message"=>"Oops!!! Something went wrong."),200);
      }
  }



  // Get the driver current location who is scheduled to user
  public function getDriverLocation_post()
  {
     if("" == $this->input->post('user_id'))
     {
        $this->response(array('status' => 'error','message' => 'please enter user id'));
     }

     // get Driver data using user_id

     $driverInfo = $this->common_model->select('driver_id',TB_SCHEDULE_RIDE,array('sr_trip_id' => $this->input->post('trip_id'),'user_id' => $this->input->post('user_id'),'driver_status' => 3));

     if(count($driverInfo) > 0 &&  0 != $driverInfo[0]['driver_id'])
     {
        $driverLocation = $this->common_model->select('*',TB_DRIVER_LOCATION,array('user_id' => $driverInfo[0]['driver_id']));
        
        if(count($driverLocation) > 0)
        {
            $this->response(array('status' => 'success','data' => $driverLocation));
        } 
        else
        {
            $this->response(array('status' => 'error','message' => 'driver location not found'));
        }
     }
     else
     {
        $this->response(array('status' => 'error','message' => 'no data found'));
     }

     
  }


  public function getUserLocation_post()
  {
     if("" == $this->input->post('driver_id'))
     {
        $this->response(array('status' => 'error','message' => 'please enter user id'));
     }

     // get Driver data using user_id

     $userInfo = $this->common_model->select('user_id',TB_SCHEDULE_RIDE,array('sr_trip_id' => $this->input->post('trip_id'),'driver_id' => $this->input->post('driver_id'),'driver_status' => 3));

     if(count($userInfo) > 0 &&  0 != $userInfo[0]['user_id'])
     {
        $userLocation = $this->common_model->select('*',TB_DRIVER_LOCATION,array('user_id' => $userInfo[0]['user_id']));
        
        if(count($userLocation) > 0)
        {
            $this->response(array('status' => 'success','data' => $userLocation));
        } 
        else
        {
            $this->response(array('status' => 'error','message' => 'driver location not found'));
        }
     }
     else
     {
        $this->response(array('status' => 'error','message' => 'no data found'));
     }
  }

    public function logout_post()
    {
       if("" == $this->input->post('user_id'))
       {
          $this->response(array('status' => 'error', 'message' => 'please enter user id' ));
       }

       $userPresent = $this->common_model->select('id',TB_USERS,array('id' => $this->input->post('user_id')));

       if(count($userPresent) > 0)
       {
          $deleteToken = $this->common_model->update(TB_USERS,array('id' => $this->input->post('user_id')),array('device_token' => ""));

          if($deleteToken)
          {
              $this->response(array('status' => 'success' , 'message' => 'user logout sucessfully'));
          }
       }
       else
       {
          $this->response(array('status' => 'error' ,'message' => 'no user found'));
       }
    } 


    public function distanceCalculation_post()
    {

      //error_reporting(E_ALL);

       if("" == $this->input->post('trip_id'))
       {
          $this->response(array('status' => 'error', 'message' => 'please enter trip id'));
       }

       if("" == $this->input->post('lat'))
       {
          $this->response(array('status' => 'error', 'message' => 'please enter latitude'));
       }

       if("" == $this->input->post('long'))
       {
          $this->response(array('status' => 'error', 'message' => 'please enter longitude'));
       }

       if("" == $this->input->post('status'))
       {
          $this->response(array('status' => 'error', 'message' => 'please enter status'));
       }

      if('start' == $this->input->post('status'))
      {

         $data = array(

              'trip_id' => $this->input->post('trip_id'),
              'lat' => $this->input->post('lat'),
              'long' => $this->input->post('long'),
              'status' => $this->input->post('status'),
              'created_date' => date('Y-m-d h:i:s')

         );

         $insertTrip = $this->common_model->insert(TB_DISTANCE_CALCULATE,$data);

         /*echo $this->db->last_query();

         exit();*/

         if($insertTrip)
         {
            $this->response(array('status' => 'success','message' =>'Trip inserted sucessfully' ));
         }
         else
         {
            $this->response(array('status' => 'success','message' =>'problem in trip insertion' ));
         }

      }
      elseif("stop" == $this->input->post('status'))
      {

        $data = array(

              'trip_id' => $this->input->post('trip_id'),
              'lat' => $this->input->post('lat'),
              'long' => $this->input->post('long'),
              'status' => $this->input->post('status'),
              'created_date' => date('Y-m-d h:i:s')

         );

        $insertTrip = $this->common_model->insert(TB_DISTANCE_CALCULATE,$data);

        if($insertTrip)
        {

           //$insertTrip = $this->common_model->insert(TB_DISTANCE_CALCULATE,$data);
            $tripData = $this->common_model->select('*',TB_DISTANCE_CALCULATE,array('trip_id' => $this->input->post('trip_id')));



            if(count($tripData) > 0)
            {
                
               $distance = 0;

               $destination = '';

               $origin = $tripData[0]['lat'].','.$tripData[0]['long'];
               for($i=1;$i<=count($tripData);$i++)
               {
                  /*echo "Lat Long => ".$tripData[$i]['lat'],' ',$tripData[$i]['long'],' ',$tripData[$i+1]['lat'],' ',$tripData[$i+1]['long'];
                  echo "<br>";*/
                  /*if(isset($tripData[$i+1]['lat']))
                  {
                      $distance = $distance + $this->distance($tripData[$i]['lat'],$tripData[$i]['long'],$tripData[$i+1]['lat'],$tripData[$i+1]['long'],"M");
                  }*/

                  if("" != $tripData[$i]['lat'] && "" != $tripData[$i]['lat'])
                  {
                    $destination = $destination.$tripData[$i]['lat'].','.$tripData[$i]['long'].'|';  
                  }

                  


               }

               $destination = rtrim($destination,"|");

              $url = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$origin."&destinations=".$destination."&key=AIzaSyDqzRPdIfxpZWTDmM3VBaQ4_An7-e6Xg64";
               
               $result = file_get_contents($url);

               $res =json_decode($result);
               $cnt  = count($res->rows[0]->elements);

               /*print_r($cnt);

               exit();*/

                $dis = 0;
                $val = 0;
                for($i = ($cnt-1);$i>=0;$i--)
                {

                  if($dis == 0)
                  {
                    $dis = $res->rows[0]->elements[$i]->distance->value;  
                    
                  }
                  else
                  {
                    $val = $val + ($dis - $res->rows[0]->elements[$i]->distance->value);
                    $dis = $res->rows[0]->elements[$i]->distance->value;
                    if($i == 0)
                    {
                      $val = $val + $dis;
                    }
                  }
                  
                }

                // convert meter to miles
                $distance = ($val * 0.000621371);

               $this->response(array('status' =>'success' ,'distance' => round($distance,3)));
            }
            else
            {
               $this->response(array('status' => 'error' , 'message' => 'no data found'));
            }
        }
        else
        {
            $this->response(array('status' => 'error','message' =>'something went wrong !!' ));
        }
      }
    }

    public function changeDriverAvaibility_post()
    {
       if("" == $this->input->post('driver_id'))
       {
          $this->response(array('status' => 'error' , 'message' => 'please enter driver id'),200);
       }

       if("" == $this->input->post('status'))
       {
          $this->response(array('status' => 'error','message' => 'please enter status'),200);
       }

       if(0 != $this->input->post('status') && 1 != $this->input->post('status'))
       {
          $this->response(array('status' => 'error','message' => 'please enter valid status'),200);  
       }

       // check if the driver is registered or not 
       // 5 - driver user
       $driverExist = $this->common_model->select('id',TB_USERS,array('id' => $this->input->post('driver_id'),'user_type' => 5));

       if(count($driverExist) <= 0)
       {
          $this->response(array('status' => 'error','message' => 'driver is not registered'),200); 

       }
       else
       {

          if(0 == $this->input->post('status'))
          {

             $data = array(

                        'driver_id' => $this->input->post('driver_id'),
                        'status' => $this->input->post('status')
             );


             if(count($data) > 0)
             {
                $insertData = $this->common_model->insert(TB_DRIVER_UNAVAILABLE,$data);

                if($insertData)
                {
                    $this->response(array('status' => 'success','message' => 'Driver status changed successfully'),200);
                }
                else
                {
                    $this->response(array('status' => 'error','message' => 'Problem in driver status updation'),200);
                }
             }
             else
             {
                $this->response(array('status' => 'error','message' => 'Something went wrong !!'),200);
             }
          }

          elseif(1 == $this->input->post('status'))
          {
            // check driver data is pesent or not
              $driverPresent = $this->common_model->select('*',TB_DRIVER_UNAVAILABLE,array('driver_id' => $this->input->post('driver_id')));

              if(count($driverPresent) > 0)
              {
             
                $deleteData = $this->common_model->delete(TB_DRIVER_UNAVAILABLE,array('driver_id' => $this->input->post('driver_id')));

                if($deleteData)
                {
                    $this->response(array('status' => 'success' ,'message' =>'Driver status changed successfully'),200);
                }
                else
                {
                    $this->response(array('status' => 'error','message' => 'Problem in driver status updation'),200);
                }
              }
              else
              {
                  $this->response(array('status' => 'error','message' => 'driver is not present'),200);
              }
          }
        }
    }

    public function healthcareCategoryLocSearch_post()
    {
        if("" == $this->input->post("user_id"))
        {
            $this->response(array('status' => 'error' , 'message' => 'please enter user id'));
        }
        // check user type
        $checkUserType = $this->common_model->select('id',TB_USERS,array('id' => $this->input->post('user_id'),'user_type' => 2));

        if(count($checkUserType) > 0)
        {

            if("" != $this->input->post('search_type'))
            {
                if("byCategory" == $this->input->post('search_type'))
                {

                    if("" == $this->input->post("category_id"))
                    {
                        $this->response(array('status' => 'error' , 'message' => 'please enter category id')); 
                        exit();
                    }       
                    $data = $this->common_model->searchHealthcare(TB_USERS.'.id,full_name,phone,email,zipcode,user_type,insurance_id,category_id,latitude,longitude',TB_USERS,array('category_id'=>$this->input->post('category_id')),array('status' =>'active'),array(TB_DOCTOR_SEARCH_BY_CAT_LOC =>TB_DOCTOR_SEARCH_BY_CAT_LOC.'.user_id ='.TB_USERS.'.id'));
                }
                elseif("byName" == $this->input->post('search_type'))
                {

                    if("" == $this->input->post("name"))
                    {
                        $this->response(array('status' => 'error' , 'message' => 'please enter name')); 
                        exit();
                    }  

                    $data = $this->common_model->selectQuery(TB_USERS.'.id,full_name,phone,email,zipcode,user_type,insurance_id,category_id,latitude,longitude',TB_USERS,array(TB_USERS.'.full_name'=>$this->input->post('name')),array('status' =>'active'),array(TB_DOCTOR_SEARCH_BY_CAT_LOC =>TB_DOCTOR_SEARCH_BY_CAT_LOC.'.user_id ='.TB_USERS.'.id'));
                }


                if(count($data) > 0)
                {
                   $this->response(array('status' => 'success', 'data' => $data));
                }
                else
                {
                   $this->response(array('status' => 'error', 'message' => 'no data found'));
                }
            }

            else
            {
                $this->response(array('status' => 'error','message' => 'Please enter search type'));
            }
        }
        else
        {
            $this->response(array('status' => 'error' , 'message' => 'User not registered'));  
        }

    }


    public function healthProvederList_post()
    {
        if("" == $this->input->post('user_id'))
        {
           $this->response(array('status' => 'error' ,'message' => 'please enter '));
        }

        // check user exist and user type

        $checkUser = $this->common_model->select('user_type',TB_USERS,array('id' => $this->input->post('user_id')));

        if(count($checkUser) > 0)
        {
            if(2 == $checkUser[0]['user_type'])
            {
                $healtCareData = $this->common_model->selectQuery(TB_USERS.'.id,full_name,email,picture,phone,user_type,latitude,longitude,category_id',TB_USERS,array(TB_USERS.'.user_type' => 3),array(),array(TB_DOCTOR_SEARCH_BY_CAT_LOC => TB_DOCTOR_SEARCH_BY_CAT_LOC.'.user_id ='.TB_USERS.'.id'),array());

                if(count($healtCareData) > 0)
                {
                   $this->response(array('status' => 'success' ,'data' => $healtCareData));
                }
                else
                {
                  $this->response(array('status' => 'error' ,'message' => 'No data found')); 
                }


            }
            else
            {
                $this->response(array('status' =>'error','message' => 'Invalid user type'));
            }
        }
        else
        {
           $this->response(array('status' => 'error','message' =>'user does not exist'));
        }


    }


    /* Author : Ram K 
     Method : OnewayTripComplete
     Date: 08 Feb 2018 
  */
      public function OnewayTripComplete_post()
      {
        $postData = $_POST;
        $user_id = $postData['user_id'];
        $schedule_id = $postData['schedule_id'];
        $trip_id = $postData['trip_id'];

        if(empty($user_id) && $trip_id)
        {
          $this->response(array("status"=>"error",  "message"=> "Please enter user id."), 200);
        }else{
          $cond = array('user_id'=>$user_id,'sr_trip_id'=>$trip_id);
          $data_update = array('sr_first_ride_status' => '2');
          $updateq = $this->common_model->update(TB_SCHEDULE_RIDE,$cond,$data_update);
          if($updateq)
          {
            $cond = array('user_id'=>$user_id,'sr_trip_id'=>$trip_id);
            $result = $this->common_model->select("id,sr_trip_id,user_id,source_address,destination_address",TB_SCHEDULE_RIDE,$cond);

            $tripID=explode("T", $result[0]['sr_trip_id']);
            $RTidGenerated = "RT".str_pad($tripID[1], 14, '0', STR_PAD_LEFT);

            $cond = array('trip_id'=>$trip_id);
            $isExist = $this->common_model->select("trip_id",TB_ROUND_TRIP,$cond);
            if(count($isExist) == 0)
            {
              $cond = array('id'=>$result[0]['user_id']);
              $selectUser = $this->common_model->select("id,free_ride",TB_USERS,$cond);
              $free_ride = $selectUser[0]['free_ride'] - 1;
              $cond = array('id'=>$result[0]['user_id']);
              $data_update = array('free_ride' => $free_ride);
              $updateRide = $this->common_model->update(TB_USERS,$cond,$data_update);

                $insertArr = array(
                  'trip_id'=> $trip_id,
                  'round_trip_id'=> $RTidGenerated,
                  'round_trip_source'=> $result[0]['destination_address'],    
                  'round_trip_destination'=> $result[0]['source_address'],      
                  'created_date'=>date("Y-m-d H:i:s")
                );
              
                $Insertresult = $this->Common_model->insert(TB_ROUND_TRIP,$insertArr);
                if($Insertresult)
                {
                  $data = array(
                    'trip_id'=> $trip_id,
                    'round_trip_id'=> $RTidGenerated,
                    'schedule_id'=> $result[0]['id']
                  );

                  $this->response(array("status"=>"success", "result"=>$data, "message"=> "Your one way trip successfully completed."), 200);
                }else{
                  $this->response(array("status"=>"error", "message"=> "Something went wrong!!!."), 200);
                }
              }else{
                $this->response(array("status"=>"error", "message"=> "Trip id already exists."), 200);
              }
            
            
          }else{
              $this->response(array("status"=>"error", "message"=> "Something went wrong!!!."), 200);
            }    
         
        }
      }

  /* Author : Ram K 
     Method : RoundTripComplete
     Date: 08 Feb 2018 
  */
      public function RoundTripComplete_post()
      {
        $postData = $_POST;
        //$user_id = $postData['user_id'];
        $schedule_id = $postData['schedule_id'];
        $trip_id = $postData['trip_id'];
        
        if(empty($schedule_id) && $trip_id)
        {
          $this->response(array("status"=>"error",  "message"=> "Please enter user id."), 200);
        }else{
          $cond = array('sr_trip_id'=>$trip_id);
          $data_update = array('sr_second_ride_status' => '2','sr_status' => '2');
          $updateq = $this->common_model->update(TB_SCHEDULE_RIDE,$cond,$data_update);
          if($updateq)
          {

            $cond = array('sr_trip_id'=>$trip_id);
            $result = $this->common_model->select("id,sr_trip_id,user_id",TB_SCHEDULE_RIDE,$cond);

            
            $cond = array('id'=>$result[0]['user_id']);
            $selectUser = $this->common_model->select("id,free_ride",TB_USERS,$cond);
            $free_ride = $selectUser[0]['free_ride'] - 1;
            $cond = array('id'=>$result[0]['user_id']);
            $data_update = array('free_ride' => $free_ride);
            $updateRide = $this->common_model->update(TB_USERS,$cond,$data_update);

            $cond = array('trip_id'=>$trip_id);
            $isExist = $this->common_model->select("trip_id",TB_ROUND_TRIP,$cond);
            // echo "<pre>";print_r($isExist);die;
            if(count($isExist) != 0)
            {              
              $this->response(array("status"=>"success", "trip_id"=>$trip_id, "message"=> "Round trip successfully completed. I hope you enjoyed"), 200);
            }
          }else{
              $this->response(array("status"=>"error", "message"=> "Something went wrong!!!."), 200);
            }
        }
      }

      public function changeRideStatus_post()
      {
        $postData = $_POST;

          if("" == $postData['schedule_id'])
          {
              $this->response(array('status' => 'error' , 'message' => 'please enter schedule id'));
          }

          /*if("" == $postData['driver_status'])
          {
             $this->response(array('status' => 'error' , 'message' => 'please enter driver status'));
          }*/

          /*if("" == $postData['sr_status'])
          {
             $this->response(array('status' => 'error' , 'message' => 'please enter ride status'));
          }*/

          /*if("3" != $postData['driver_status'] || "1" != $postData['sr_status'])
          {
             $this->response(array('status' => 'error', 'message' => 'ride is not scheduled'));
          }*/

          if("" == $postData['ride_status'])
          {
            $this->response(array('status' => 'error', 'message' => 'Please enter status'));
          }


          //check Schedule exist 

          $scheduleExist = $this->common_model->select('id',TB_SCHEDULE_RIDE,array('id' => $postData['schedule_id']));

          if(count($scheduleExist) > 0)
          {
              $data = array(

                'sr_status' => $postData['ride_status']
              );

              $updateData = $this->common_model->update(TB_SCHEDULE_RIDE,array('id' => $postData['schedule_id']),$data);

              if($updateData)
              {
                  $this->response(array('status' => 'success' , 'message' => 'Ride Updated Sucessfully'));
              }
              else
              {
                  $this->response(array('status' => 'error' , 'message' => 'Something went wrong !!'));
              }
          }
          else
          {
              $this->response(array('status' => 'error' , 'message' => 'No schedule exist'));
          }

      }

      public function makePayment_post()
      {
          $postData = $_POST;

          if("" == $postData['user_id'])
          {
              $this->response(array('status' => 'error','message' => 'please enter user id'));
          }

          if("" == $postData['amount'])
          {
              $this->response(array('status' => 'error','message' => 'please enter user id'));
          }

          if("" == $postData['schedule_id'])
          {
              $this->response(array('status' => 'error','message' => 'please enter schedule id'));
          }



          $getUserData = $this->common_model->select('*',TB_USERS,array('id' => $postData['user_id']));

          if(count($getUserData) > 0)
          {
              if('1' != $getUserData[0]['user_type'])
              {
                  $this->response(array('status' => 'error','message' => 'Invalid User Type'));
                  die();
              }
          }
          else
          {
              $this->response(array('status' => 'error','message' => 'No User Available'));
              die();
          }

          try 
          {
                require_once APPPATH.'libraries/stripe/init.php';
                \Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);


                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < 14; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }

                /*$itemName = "Stripe Donation dmo";
                $itemNumber = "PS123456";
                $itemPrice = $postData['amount'];*/
                //$currency = "usd";
                $orderID = $randomString;

                $charge = \Stripe\Charge::create(array(
                 'customer' =>  $getUserData[0]['card_token_id'],
                 'amount'   => ($postData['amount'] * 100), // cents to dollar conversion
                 'currency' => "usd",
                 'description' => "Ride booking",
                 'metadata' => array(
                   'item_id' => $postData['schedule_id']
                 )
                ));

                //echo "<pre>"; print_r($charge);

                if(null!= $charge)
                {
                    $data = array(

                        'order_id' => $orderID,
                        'schedule_id' => $postData['schedule_id'],
                        'paymentStatus' => 'completed',
                        'amount' => $postData['amount'],
                        'created_date' => date('Y-m-d h:i:s')
                      );

                    $insertData  = $this->common_model->insertData(TB_MEMBER_PAYMENT,$data);

                    if($insertData)
                    {
                        $this->response(array('status' => 'success','message' => 'Payment completed'));        
                    }
                    else
                    {
                       $this->response(array('status' => 'error','message' => 'Something went wrong !!'));        
                    }
                }
          } 
          catch (\Stripe\Error\Card $e)
          {
                // Card was declined.
                
                $this->response(array('status' => 'error','message' => $e->getJsonBody()));

          } 
          catch (\Stripe\Error\ApiConnection $e) 
          {
                // Network problem, perhaps try again.
                
                $this->response(array('status' => 'error','message' => $e->getJsonBody()));

          }
          catch (\Stripe\Error\InvalidRequest $e) 
          {
                // You screwed up in your programming. Shouldn't happen!
                
                $this->response(array('status' => 'error','message' => $e->getJsonBody()));

          }
          catch (\Stripe\Error\Api $e) 
          {
                // Stripe's servers are down!
                
                $this->response(array('status' => 'error','message' => $e->getJsonBody()));

          } 
          catch (\Stripe\Error\Base $e) 
          {
                // Something else that's not the customer's fault.
                
                 $this->response(array('status' => 'error','message' => $e->getJsonBody()));
          }
      }

      /*public function changeRideStatus_post()*/

  }