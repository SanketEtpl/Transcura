<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FBlogin extends CI_Controller {

	public function index()
	{
		if(is_user_logged_in())
		{
			redirect("users");
			exit;
		}else{
			$this->load->view('login');
		}
	}

	public function checkLogin(){
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			$result = $this->common->select("*",TB_ADMIN,array("username"=>trim($postData["username"]),"password"=>md5(trim($postData["password"]))));
			if($result)
			{
				$this->session->set_userdata("auth_user", $result[0]);
				echo json_encode(array("status" => "success" ,"message"=>"User login sucessfully.")); exit;
			}
			else
			{
				echo json_encode(array("status" => "error","message"=>"Please provide valid login credential.	")); exit;
			}
		}
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect("login"); exit;
	}
}
