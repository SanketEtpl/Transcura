<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
 
	 function __construct() {
		parent::__construct(); 
		$this->load->model("login_model");
        $this->load->model("common_model"); 
		$this->load->helper("userauth");
		$this->load->library("User_AuthSocial");		
	}
	 
	public function index()
	{
		$data['title']  = 'Welcome To login';
		$data['meta_description'] = '';
		$data['meta_keyword'] = '';

		
		do_auth_api();
		if(check_user_logged_in()==true)
                {
			$data['user_session'] = '1';
			$data['main_content'] = 'front/home';
		}else{
			$data['user_session'] = '0';
			$data['main_content'] = 'front/register';
		}
		
		$this->load->view('front/incls/layout',$data);
	}
	
	public function facebook_login()
	{
	  $data["user_data"] =	$this->user_authsocial->facebook();
	  redirect("jobseeker/Home");
	}
	public function google_login()
	{
		$data["user_data"] =	$this->user_authsocial->google();
		redirect("Home");
	}
	public function linkedin_login()
	{
		$data["user_data"] =	$this->user_authsocial->linkedin();
		redirect("Home");
	}

	public function reset_password()
	{
		redirect('forgot_password');		
	}
	
    public function do_login()
    {
            
        $data['title']  = 'Welcome To login';
	$data['meta_description'] = '';
	$data['meta_keyword'] = '';
	
	$cond = false;
	if(check_user_logged_in())
        {
		$data['main_content'] = 'front/home';
		do_auth_api();		
	}
	else{
		$fb_url = $this->user_authsocial->fb_redirect_url();
		$data["FB_authUrl"] =$fb_url;
		$gl_url = $this->user_authsocial->gl_redirect_url();
		$data["GL_authUrl"] =$gl_url;
		$data["LD_authUrl"] =base_url().'Login/linkedin_login';
		$data['main_content'] = 'front/login';
	}
	$this->load->view('front/incls/layout',$data);	
    } 
    
    public function oauth($providername)
    {
		
        $key=$this->config->item('key',$providername);
        $secret=$this->config->item('secret',$providername);

        $this->load->helper('url');

        $this->load->spark('oauth/0.3.1');

        // Create an consumer from the config
        $consumer = $this->oauth->consumer(array(
            'key' => $key,
            'secret' => $secret,
        ));

        // Load the provider
        $provider = $this->oauth->provider($providername);

        // Create the URL to return the user to
        
        $callback = site_url('signin/oauth/'.$provider->name);

        if ( ! $this->input->get_post('oauth_token'))
        {
            // Add the callback URL to the consumer
            $consumer->callback($callback);

            // Get a request token for the consumer
            $token = $provider->request_token($consumer);

            // Store the token
            $this->session->set_userdata('oauth_token', base64_encode(serialize($token)));

            // Get the URL to the twitter login page
            $url = $provider->authorize($token, array(
                'oauth_callback' => $callback,
            ));

            // Send the user off to login
            redirect($url);
        }
        else
        {
            if ($this->session->userdata('oauth_token'))
            {
                // Get the token from storage
                $token = unserialize(base64_decode($this->session->userdata('oauth_token')));
            }

            if ( ! empty($token) AND $token->access_token !== $this->input->get_post('oauth_token'))
            {
                // Delete the token, it is not valid
                $this->session->unset_userdata('oauth_token');

                // Send the user back to the beginning
                exit('invalid token after coming back to site');
            }

            // Get the verifier
            $verifier = $this->input->get_post('oauth_verifier');

            // Store the verifier in the token
            $token->verifier($verifier);

            // Exchange the request token for an access token
            $token = $provider->access_token($consumer, $token);

            // We got the token, let's get some user data
            $user = $provider->get_user_info($consumer, $token);


            $this->saveUserData($providername,$token,$user);
        }
    }
    
    public function oauth2($providername)
    {
        $key = $this->config->item('key',$providername);
        $secret = $this->config->item('secret',$providername);

        $this->load->helper('url_helper');

        $this->load->spark('oauth2/0.3.1');

        $provider = $this->oauth2->provider($providername, array(
            'id' => $key,
            'secret' => $secret,
        ));
		//var_dump($provider);
        if ( ! $this->input->get('code'))
        {  
			/*if($providername =='facebook'){
				$url = 'http://exceptionaire.co'.$provider->redirect_uri;
				redirect($url);
			}*/
            // By sending no options it'll come back here
           $provider->authorize();
        }
        else
        {
            // Howzit?
          //  echo "in else";die;
            try
            {
                $token = $provider->access($_GET['code']);
                
                $user = $provider->get_user_info($token);
              //  print_r($user);die;
                $this->saveUserData($providername,$token,$user);

            }

            catch (OAuth2_Exception $e)
            {
                show_error('That didnt work: '.$e);
            }

        }
    }
    
    private function saveUserData($providername,$token,$user)
    {
        
        //return;
        $usertoken = $token->access_token;
        $usersecret = $token->secret;

        $uid = $user['uid'];
		
        $nickname= array_key_exists('nickname',$user)?$user['nickname']:$uid;
        $first_name =array_key_exists('first_name',$user)? $user['first_name']:null;
        $last_name =array_key_exists('last_name',$user)? $user['last_name']:null;
        $name =array_key_exists('name',$user)? $user['name']:null;
        $location= array_key_exists('location',$user)?$user['location']:null;
        //$description= array_key_exists('description',$user)?$user['description']:null;
        $profileimage=array_key_exists('image',$user)? $user['image']:null;
        $email=array_key_exists('email',$user)? $user['email']:null;
		$cond_email = array();
		
		if($providername == 'facebook' || $providername == 'google'){
			if($email!=""){	
				$cond_email = array("email_address" => $email,"provider !=" => $providername);
				$checkEmail = $this->login_model->validUser(TB_USERS,"user_id,slogin_id,first_name,last_name,email_address,provider",$cond_email);
				
				if(count($checkEmail)>0){
					
					$this->session->set_flashdata("dup_email_address","The email address is already register with system");
					
					redirect('index');
					exit();
				}
			}
		}
		
		$cond = array("slogin_id" => $uid, "provider" => $providername); 
		//$cond = array_merge($cond_email,$cond);
		
		$userData = $this->login_model->validUser(TB_USERS,"user_id,slogin_id,first_name,last_name,email_address,provider",$cond);
		//print_r($userData);
		
		if(count($userData) > 0)
		{	
			/*
			 * Authenticate and redirect user to profile
			 * */
			$userData[0]["social_token"] = $usertoken;
			$userData[0]["social_image"] = $profileimage;
			$this->session->set_userdata("auth_huntinguser", $userData[0]);
			//$this->session->set_userdata('referred_from', current_url_string());
			if($this->session->userdata('referred_from')){
				 $url_redirect = $this->session->userdata('referred_from');
			}			
			else{
				 $url_redirect = base_url().'dashboard';
				 
			}
			
			redirect($url_redirect); 
				
		}else{
			
			/*
			 * Authenticate if user is not available insert and redirect user to profile
			 * */
			
			if($providername == 'twitter' || $providername == 'instagram'){
				$explode = explode(' ',$name);
				$first_name = $explode[0];
				$last_name = $explode[1];
			}
			
			$insertId = $this->common_model->insert(
						TB_USERS,
						array(
						"slogin_id" => $uid,
						"first_name" => ($first_name?$first_name:""), 
						"last_name" => ($last_name?$last_name:""),
						"email_address" =>($email?$email:""),
						"provider" => $providername,
						"date_modified" => date('Y-m-d H:i:s'))
					);
					
				if($insertId){
					
					$cond = array("slogin_id" => $uid, "provider" => $providername); 
		
					$userData = $this->login_model->validUser(TB_USERS,"user_id,slogin_id,first_name,last_name,email_address,provider",$cond);
					
					$userData[0]["social_token"] = $usertoken;
					$userData[0]["social_image"] = $profileimage;
					$this->session->set_userdata("auth_huntinguser", $userData[0]);
					//$this->session->set_userdata('referred_from', current_url_string());
					
					if($this->session->userdata('referred_from')){
							 $url_redirect = $this->session->userdata('referred_from');
						 }			
						 else{
							 $url_redirect = base_url().'dashboard';
							 
						 }
					redirect($url_redirect); 
				}
		}

        //redirect('index');

    } 
    
    public function do_logout(){
        if($this->session->userdata('auth_jobseeker')){
            $this->session->unset_userdata('auth_jobseeker'); 
            redirect('login');
        }
        elseif($this->session->userdata('auth_employer')){
            $this->session->unset_userdata('auth_employer'); 
            redirect('login');
        }elseif($this->session->userdata('auth_institute')){
            $this->session->unset_userdata('auth_institute'); 
            redirect('login');
        }elseif($this->session->userdata('auth_hr')){
            $this->session->unset_userdata('auth_hr'); 
            redirect('login');
        }
    }

    public function set_password()
    {
		if(!isset($_REQUEST['token']) && !isset($_REQUEST['uid'])) {
			redirect("index");
		}
		else{
			$data['title']  = 'Set password';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$data['main_content'] = 'front/set_password';		
			do_auth_api();
			$this->load->view('front/incls/layout',$data);
		}
    }
    
    public function forgot_password()
    {       
        $data['title']  = 'Welcome To Forgot password';
        $data['meta_description'] = '';
        $data['meta_keyword'] = '';
        $data['main_content'] = 'front/forgot_password';	
        do_auth_api();
        $this->load->view('front/incls/layout',$data);
    } 
    
    function getcountry()
    {
      $cond = false;
      $data['country'] = $this->common_model->select("id,ISO2,CountryName,PhoneCode",TB_COUNTRYCODE,$cond);
      $rowCount = count($data['country']);
            //Display states list
      if($rowCount > 0){
                echo '<option value="">Select Country</option>';
                foreach($data['country'] as $row){
                        echo '<option  value="'.$row['ISO2'].'">'.$row['CountryName'].'</option>';

                }
      }
      else{
                echo '<option value="">State not available</option>';
      }
    }
}
