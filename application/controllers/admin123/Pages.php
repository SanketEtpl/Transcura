<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$this->load->view('pages/listPages');
		}else{
			redirect("login");
			exit;
		}
	}

	public function listPages(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$arrayColumn = array("page_id"=>"page_id","page_title"=>"page_title");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$where=array("isDeleted"=>'0');
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'page_title','page_id',TB_PAGES,'*','listPages',$where);
              // echo $this->db->last_query();die;
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $pages) {
						$page_id = $this->encrypt->encode($pages['page_id']);
						$rows .= '<tr id="'.$page_id.'">
	                            <td class="text-left">'.$pages['page_title'].'</td>
	                            <td class="text-left">'.$pages['page_detail'].'</td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$page_id.'" class="" onclick="getPage(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
									</a>
	                                <a data-id="'.$i.'" data-row-id="'.$page_id.'" class="" onclick="deletePage(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getPage(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){

				$postData = $this->input->post();
				//print_r($this->encrypt->decode($postData['key']));
				//print_r($this->db->last_query());die;);die;
				$pageData = $this->common->selectQuery("page_title,page_detail,page_name",TB_PAGES,array('page_id'=>$this->encrypt->decode($postData['key'])));
				//echo $this->db->last_query();die;
				if($pageData){
					echo json_encode(array("status"=>"success","pageData"=>$pageData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong, Please try again...!!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function savePage(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				//print_r($postData );die;
				$dateTime = date("Y-m-d H:i:s");
				$insertArr = array(
								'page_title'=>$postData["title"],
								"page_detail"=>$postData["page_detail"],															
								"updated_at" => $dateTime,
								);
				if($postData["page_key"]){
					$insertId = $this->common->update(TB_PAGES,array('page_id'=>$this->encrypt->decode($postData['page_key'])),$insertArr);
					
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Content has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					$insertArr["created_at"] = $dateTime;
					$insertId = $this->common->insert(TB_PAGES,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Content has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}

    }


   public function deletePage(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				//print_r($postData );die;
				$dateTime = date("Y-m-d H:i:s");	
				$insertArr = array('isDeleted'=>'1');			
				if($postData["key"]){
					$insertId = $this->common->update(TB_PAGES,array('page_id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					//echo $this->db->last_query();die;
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Content has been deleted successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}

    }


}
