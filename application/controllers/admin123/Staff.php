<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staffs extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		
		if(is_user_logged_in())
		{
			$this->load->view('Staffs/listStaffs');

			$this->load->model('common_model');
		}else{
			redirect("login");
			exit;
		}

	}

	public function listStaffs(){
		
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();
				//print_r($postData) ;die;
				$arrayColumn = array("staff_id"=>"staff_id","fname"=>"first_name","lname"=>"last_name","email"=>"email","phone"=>"phone"  );
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");

				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'fname','staff_id',TB_STAFFS,'*','listStaffs');
              // print_r($result) ;die;
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['staff_id']);
						//$agenciesdata = $this->common->selectQuery("first_name",TB_STAFFS,array('staff_id'=>$user['agencies_id'] ) );
						
						//echo $this->$db->last_query();die;
						//print_r($agenciesdata[0]['agencies_name']);die;
						$rows .= '<tr id="'.$userId.'">
	                            <td class="text-left">'.$user['first_name'].'</td>
	                            <td class="text-left">'.$user['last_name'].'</td>
	                            <td class="text-left">'.$user['email'].'</td>
								<td class="text-left">'.$user['phone'].'</td>	                           
								
								
	                           <td class="text-left">
								<div class="btn-group btn-toggle" id="accept"> 
									<a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="changeStatus(this)" href="javascript:void(0)">
									'.$user['status'].'	
									</a>
								  </div>
	                           </td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getStaff(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
									</a>
	                              
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getStaff(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("*",TB_STAFFS,array('staff_id'=>$this->encrypt->decode($postData['key'])));
				//$agentData = $this->common->selectQuery("agencies_id,agencies_name",TB_AGENCIES,array('status'=>'active'));
			//$sc=json_encode($agentData);

				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0]), true); exit;
			


				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function saveStaff(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
                //  print_r($_REQUEST);die;
				  //"phone"=>"phone" ,"gender"=>"gender","address"=>"address" ,"city"=>"city" 
					$postData = $this->input->post();
					$insertArr = array('first_name'=>$postData["first_name"],
					'last_name' => $postData['last_name'],
					'email'=>$postData["email"],
					'address' => $postData['address'],
					'zipcode'=>$postData["zipcode"],
					'phone'=>$postData["phone"],							
				
					);
				if($postData["user_key"]){
					$insertId = $this->common->update(TB_STAFFS,array('staff_id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
			     	//echo $this->db->last_query();die;
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"staff has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					$insertId = $this->common->insert(TB_STAFFS,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"staff has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

	
  public function change_user_status() {

  	if(is_ajax_request())
		{
			if(is_user_logged_in()){
					//print_r($_REQUEST);die;
					$postData = $this->input->post();
					$userData = $this->common->selectQuery("status",TB_STAFFS,array('staff_id'=>$this->encrypt->decode($postData['key'])));
					//print_r($userData);die;
				if($userData[0]["status"] == "active"){
						$status_update = "inactive";
					}
					else if($userData[0]["status"] == "inactive"){
						$status_update = "active";
					}
				$insertArr = array('status'=>$status_update);

				if($postData["key"]){
					$insertId = $this->common->update(TB_STAFFS,array('staff_id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					//echo $this->db->last_query();die;
					$userData = $this->common->selectQuery("*",TB_STAFFS,array('staff_id'=>$this->encrypt->decode($postData['key'])));
					if($insertId){
						echo json_encode(array("status"=>"success","userData"=>$userData[0],"msg"=>"staff has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

}
