<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model("Dashboard_model");
        	$this->load->model('common_model');
   // echo "GVJGHJGHJGh" ;die;
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			
			if($this->input->post('userinfo') == 'userinfo'){
			$updateres = $this->user->update_info();
			if($updateres){					
				$msg_type = 'success';
				$msgArr = array(
									'msg'=>'Profile information updated successfully.',
									'msg_type'=>$msg_type
								);
				$this->session->set_flashdata($msgArr);
			}else{
				$msg ='Oops! error please try agin.';
				$msg_type = 'error';
				$msgArr = array(
									'msg'=>$msg,
									'msg_type'=>$msg_type
								);
				$this->session->set_flashdata($msgArr);
			}
			redirect("/userprofile");
			exit();				
		}
		else
		{


			$dashoard_count['patient_count']= $this->Dashboard_model->get_count(TB_USERS,"Patient");
			$dashoard_count['driver_count']= $this->Dashboard_model->get_count(TB_USERS,"Driver");
			$dashoard_count['agent_count'] =$this->Dashboard_model->get_count1(TB_DOCTORS);
			$dashoard_count['serviceprovider_count']= 0;//$this->Dashboard_model->get_count(TB_SERVICE_PROVIDERS);
			$this->load->view('dashboard/listDashboard',$dashoard_count);
		}

		}else{
			redirect("admin/login");
			exit;
		}


	}

	public function get_chart_status()
	{
		//print_r("hjhbjghj");die;

		echo json_encode($this->Dashboard_model->get_chart_status(), true);
	}



     public function get_adminprofile()
	{
			if(is_user_logged_in())
			{
			// $postData = $this->input->post();
			$userinfo = $this->common->select("*",TB_ADMIN,$arrayName = array());	
			//print_r($userinfo);die;		
			$this->load->view('dashboard/profileinfo',$userinfo);
			}

			else{
			redirect("admin/login");
			exit;
			}

			
	}



}