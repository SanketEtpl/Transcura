<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Doctors_cat extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
 		if(is_user_logged_in())
		{
			
//echo "hgjghj" ;die
			$this->load->view('doctors/listdoctors_cat');
			$this->load->model('common_model');
			
		}else{
			redirect("login");
			exit;
		}

	}

	public function listdoctors_cat(){
	//print_r("ghjfgh");die;
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();
				//print_r($postData) ;die;
				$arrayColumn = array("cat_id"=>"cat_id","cat_title"=>"cat_title");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where=array("isDeleted"=>'0');
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'cat_title','cat_id',TB_DOCTORS_CAT,'*','listdoctors_cat',$where);
               //print_r($result) ;die;
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['cat_id']);
							
						//print_r($result['rows']);
						$rows .= '<tr id="'.$userId.'">
	                            <td class="text-left">'.$user['cat_title'].'</td>
	                            
	                           
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getDoctors_cat(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
										  <a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="deleteDoctors_cat(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>	                              
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getDoctors_cat(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();


				$userData = $this->common->selectQuery("*",TB_DOCTORS_CAT,array('cat_id'=>$this->encrypt->decode($postData['key'])));
				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function saveDoctors_cat(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				
                   //print_r($_REQUEST);die;
					$postData = $this->input->post();
					$insertArr = array('cat_title'=>$postData["category_title"],
					
					'updated_at'=>date("Y-m-d h:i:s", time()),	
					);
				if($postData["user_key"]){
					$insertId = $this->common->update(TB_DOCTORS_CAT,array('cat_id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
				//echo $this->db->last_query();die;
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Category has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{


					$insertArr = array('cat_title'=>$postData["category_title"],
					'created_at'=>date("Y-m-d h:i:s", time()),
					'updated_at'=>date("Y-m-d h:i:s", time()),	
					);

					$insertId = $this->common->insert(TB_DOCTORS_CAT,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Category has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

	
  

	public function deleteDoctors_cat() {

  	if(is_ajax_request())
		{
			if(is_user_logged_in()){
				//print_r($_REQUEST);die;
					$postData = $this->input->post();
					$userData = $this->common->selectQuery("isDeleted",TB_DOCTORS_CAT,array('cat_id'=>$this->encrypt->decode($postData['key'])));
					//print_r($userData);die;
				if($userData[0]["isDeleted"] == "0"){
						$status_update = "1";
					}
					else if($userData[0]["isDeleted"] == "1"){
						$status_update = "0";
					}
				$insertArr = array('isDeleted'=>$status_update,'updated_at'=>date("Y-m-d h:i:s", time())	);

				if($postData["key"]){
					$insertId = $this->common->update(TB_DOCTORS_CAT,array('cat_id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					//echo $this->db->last_query();die;
					$userData = $this->common->selectQuery("*",TB_DOCTORS_CAT,array('cat_id'=>$this->encrypt->decode($postData['key'])));
					if($insertId){
						echo json_encode(array("status"=>"success","userData"=>$userData[0],"msg"=>"Category has been deleted successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

}
