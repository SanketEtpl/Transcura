<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service_types extends CI_Controller {

	function __construct()
	{
		parent::__construct();
			$this->load->library('upload');
		$this->load->helper(array('form', 'url'));
		$this->load->library('image_lib');
	}

	public function index()
	{
 		if(is_user_logged_in())
		{
			
//echo "hgjghj" ;die;
			$this->load->view('drivers/listService_type');
			$this->load->model('common_model');

			
		}else{
			redirect("login");
			exit;
		}

	}

	public function listService_type(){
	
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();
				//print_r($postData) ;die;
				$arrayColumn = array("type_id"=>"type_id","type_title"=>"type_title");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where=array();
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'type_title','type_id',TB_SERVICE_TYPE,'*','listService_type',$where);
               //print_r($result) ;die;
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$type_id = $this->encrypt->encode($user['type_id']);

							
						//print_r($result['rows']);
						$rows .= '<tr id="'.$type_id.'">
	                            <td class="text-left">'.$user['type_title'].'</td>
	                            
	                         <td class="text-left"><img src='.base_url().$user['image'].' width="75" height="75" ></td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$type_id.'" class="" onclick="getService_type(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
										  <a data-id="'.$i.'" data-row-id="'.$type_id.'" onclick="deleteService_type(this,'.$user['type_id'].')" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>	                              
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getService_type(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("*",TB_SERVICE_TYPE,array('type_id'=>$this->encrypt->decode($postData['key'])));
				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"Service type has been logout.")); exit;
			}
		}
		
	}

	public function saveService_type(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				
                   //print_r($_FILES);die;
					$postData = $this->input->post();
               $dateTime = date("Y-m-d H:i:s");
			   $config['upload_path']   =    "./uploads/service_type/";			
		       $config['allowed_types'] =   'gif|jpg|jpeg|png|pdf|bmp'; 
		       $config['max_size']      =   "5000"; 
		       $config['max_width']     =   "800"; 
		       $config['max_height']    =   "500";	
		      // $config['min_width']     = "400"; // new
              // $config['min_height']    = "150"; // new

               $this->load->library('upload', $config);
		       // $config['max_size'] = '100';
		       $this->load->library('upload');	   
		       $this->upload->initialize($config);
		       //move_uploaded_file($_FILES['userfile']['tmp_name'],'./uploads/test.jpg');die;
		

    

     	    if(isset($_FILES["userfile"]["name"]))   
		    {   
               if(!$this->upload->do_upload('userfile'))
		       {		 
		       $error = $this->upload->display_errors();
		       echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
		       } 
		       else
		        {		 
		          $data=$this->upload->data();
				  $data = $this->resize($data);			            
		          $data['thumbnail_name'] = $data['new_image'];	
		        }				
    		}

    		else if(isset($postData['oldimg']))   
		    {   
              	 
		         		            
		          $data['thumbnail_name'] =$postData['oldimg'];	
		        				
    		}

    	  else{
             $data['thumbnail_name'] = "images/no-image.png";	
            echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please upload valid file.")); exit;	
				

    	    }	


          $insertArr = array('type_title'=>$postData["type_title"],'image'=>$data['thumbnail_name'],
	                    'updated_at'=>date("Y-m-d h:i:s", time()));


				if($postData["user_key"]){



					$insertId = $this->common->update(TB_SERVICE_TYPE,array('type_id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
				
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Service type has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{


					$insertArr = array('type_title'=>$postData["type_title"],'image'=>$data['thumbnail_name'],
					'created_at'=>date("Y-m-d h:i:s", time()),
					'updated_at'=>date("Y-m-d h:i:s", time()),	
					);

					$insertId = $this->common->insert(TB_SERVICE_TYPE,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Service type has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}


public function resize($image_data) {

	//print_r($image_data['raw_name'].$image_data['file_ext']);die;
        $img = substr($image_data['full_path'], 51);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $image_data['full_path'];
        $config['new_image'] = './uploads/service_type/thumb_'.$image_data['raw_name'].$image_data['file_ext'];
        $config['allowed_types'] = 'png|mpg|mpeg|wmv|jpg';       
        $config['width'] = 80;
        $config['height'] = 80;
        $this->image_lib->initialize($config);
        $src = $config['new_image'];
        $data['new_image'] = substr($src, 2);
        $data['img_src'] = base_url().$data['new_image'];
        if(!$this->image_lib->resize())
		{
		 $error = $this->image_lib->display_errors();
		// echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
	    return $error;	
		}
		else
		{
        return $data;
		      
		}
    }

	public function deleteService_type() {

  	if(is_ajax_request())
		{
			if(is_user_logged_in()){
		//	print_r($_REQUEST);die;
					$postData = $this->input->post();		

				if($postData["key"]){
					$insertId = $this->common->delete(TB_SERVICE_TYPE,array('type_id'=>$this->encrypt->decode($postData['key'])));
			
					if($insertId){
						echo json_encode(array("status"=>"success","msg"=>"Service Type has been deleted successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

}
