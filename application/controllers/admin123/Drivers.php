<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Drivers extends CI_Controller {

	function __construct()
	{
		parent::__construct();		
			//$this->load->library('upload');
		  $this->load->library('image_lib');
		       $config['allowed_types'] =   "png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG";  
               $config['max_size']      =   "5000"; 
               $config['max_width']     =   "1000"; 
               $config['max_height']    =   "900"; 
               $config['min_width']     = "70"; // new
               $config['min_height']    = "70"; // new    
               $config['upload_path']   =  "./uploads/Driver/original/" ;   
               	$this->load->library('upload', $config);
	}

	public function index()
	{
		if(is_user_logged_in())
		{//	$this->load->library('upload');
			$this->load->view('drivers/listDrivers');
			$this->load->model('common_model');
			
		     
		}else{
			redirect("login");
			exit;
		}

	}

	public function listDrivers(){
		//print_r("ghjfgh");die;
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();
				//print_r($_FILES) ;die;
				$arrayColumn = array("id"=>"id","fname"=>"first_name","email"=>"email","phone"=>"phone" ,"gender"=>"gender" );
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where =array("user_type"=>"Driver");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'fname','id',TB_USERS,'*','listDrivers',$where );
               //print_r($result) ;die;
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['id']);
							
						//print_r($result['rows']);
						$rows .= '<tr id="'.$userId.'">
	                            <td class="text-left">'.$user['first_name'].'</td>
	                           
	                            <td class="text-left">'.$user['email'].'</td>
								<td class="text-left">'.$user['phone'].'</td>	                           
								
								
	                           <td class="text-left">
								<div class="btn-group btn-toggle" id="accept"> 
									<a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="changeStatus(this)" href="javascript:void(0)" id="status">
									'.$user['status'].'	
									</a>
								  </div>
	                           </td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getDriver(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
									</a>
	                              
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getDriver(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));			


				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function saveDriver(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){


			$postData = $this->input->post();
			$token = "";
			//Combination of character, number and special character...
			$combinationString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789*#&$^";
			$this->load->helper('string');
            $token=  random_string('alnum',20);
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
            $password = substr( str_shuffle( $chars ), 0, 5 );
     	    if(isset($_FILES["pimage"]["name"]))   
		    {   
               if(!$this->upload->do_upload('pimage'))
		       {		 
		       $error = $this->upload->display_errors();
		       echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
		       } 
		       else
		        {		 
		          $data=$this->upload->data();
				  $data = $this->resize($data);					        
		          $data['picture'] = $data['new_image'];	
		        }				
    		}

    		else if(isset($postData['oldimg']))   
		    {   
              	 
		         		            
		          $data['picture'] =$postData['oldimg'];	
		        				
    		}

    	  else{
             $data['picture'] = "images/default.jpg";	
         	   }	




           if(isset($_FILES["personal_doc"]["name"]))   
		    {   
        $file_name = $_FILES["personal_doc"]["name"];
        $original_file_name = $file_name;
        $random = rand(1, 10000000000000000);
        $makeRandom = $random;
        $file_name_rename = $makeRandom;
        $explode = explode('.', $file_name);
        if(count($explode) >= 2) {
            $new_file = $file_name_rename.'.'.$explode[1];
            //print_r($new_file);die;
            $config['upload_path'] = "./uploads/Driver/doc";
            $config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
            $config['file_name'] = $new_file;
            $config['max_size'] = '307210';
            $config['max_width'] = '300000';
            $config['max_height'] = '300000';
            $config['overwrite'] = TRUE;
             $this->load->library('upload',$config);
              $this->upload->initialize($config);
				if(!$this->upload->do_upload("personal_doc")) {
				$error = $this->upload->display_errors();
				echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
				} else {
				$doc = 'uploads/Driver/doc/'.$new_file;   

				}
           }
		}

		else if(isset($postData['personal_docold']))   
	    {          	 
	    $doc =$postData['personal_docold'];		        				
		}
       else
        {
	   $doc ="";	
        }	


  if(isset($_FILES["vehicle_doc"]["name"]))   
		    {   
        $file_name = $_FILES["vehicle_doc"]["name"];
        $original_file_name = $file_name;
        $random = rand(1, 10000000000000000);
        $makeRandom = $random;
        $file_name_rename = $makeRandom;
        $explode = explode('.', $file_name);
        if(count($explode) >= 2) {
            $new_file = $file_name_rename.'.'.$explode[1];
            //print_r($new_file);die;
            $config['upload_path'] = "./uploads/Driver/doc";
            $config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
            $config['file_name'] = $new_file;
            $config['max_size'] = '307210';
            $config['max_width'] = '300000';
            $config['max_height'] = '300000';
            $config['overwrite'] = TRUE;
             $this->load->library('upload',$config);
              $this->upload->initialize($config);
				if(!$this->upload->do_upload("vehicle_doc")) {
				$error = $this->upload->display_errors();
				echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
				} else {
				$doc1 = 'uploads/Driver/doc/'.$new_file;   

				}
           }
		}

		else if(isset($postData['vehicle_docold']))   
	    {          	 
	    $doc1 =$postData['vehicle_docold'];		        				
		}
       else
        {
	   $doc1 ="";	
        }	












     				$postData = $this->input->post();
					$birth = date("Y-m-d", strtotime($postData["birth"])); 
					$insertArr = array('first_name'=>$postData["first_name"],					
					'email'=>$postData["email_address"],
					'address' =>$postData['address'],
					'zipcode'=>$postData["zipcode"],
					'phone'=>$postData["phone"],					
					'updated_at'=>date("Y-m-d h:i:s", time()),	
					'date_of_birth'=>$birth	,
					'user_token'=>$token,
					'company_name' =>$postData['company_name'],
					 'company_contactno' =>$postData['company_contactno'],
					 'company_address'=>$postData["caddress"],
					'user_type'=>"Driver",					
					'picture'=> $data['picture'],					
					'personal_doc'=>$doc,
					'vehicle_doc'=>	$doc1	

					);
				if($postData["user_key"]){
					$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
			//echo $this->db->last_query();die;
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Patient has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{

                $insertArr = array('first_name'=>$postData["first_name"],					
					'email'=>$postData["email_address"],
					'address' =>$postData['address'],
					'zipcode'=>$postData["zipcode"],
					'phone'=>$postData["phone"],					
					'updated_at'=>date("Y-m-d h:i:s", time()),	
					'date_of_birth'=>$birth	,
					'user_token'=>$token,
					'company_name' =>$postData['company_name'],
					 'company_contactno' =>$postData['company_contactno'],
					 'company_address'=>$postData["caddress"],
					'user_type'=>"Driver",					
					'picture'=> $data['picture'],					
					'personal_doc'=>$doc,
					'vehicle_doc'=>	$doc1			

					);

					$insertId = $this->common->insert(TB_USERS,$insertArr);					
	//echo $this->db->last_query();die;
					if($insertId){

                 //    $userinfo = $this->common->selectQuery("*",TB_USERS,array("email"=>$postData['email_address']));
                 // // print_r($userinfo );die;    
                        
                 //        $to_email = $userinfo[0]['email'] ;
                 //        $hostname = $this->config->item('hostname');  
                 //        $config['mailtype'] ='html';
                 //        $config['charset'] ='iso-8859-1';
                 //        $this->email->initialize($config);
                 //        $from  = EMAIL_FROM; 
                 //        $this->messageBody  = email_header();
                 //        $this->messageBody  .= '<tr>
                 //            <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                 //                <p>Dear '.$userinfo[0]['Username'].',</p>
                 //                    <p>
                 //                    You are registered successfully from admin on Transcura.<br>
                 //                    </p>
                                   
                 //                    </p>

                 //                </td>
                 //            </tr>
                 //            <tr>';                      
                 //        $this->messageBody  .= email_footer();        //print_r( $this->messageBody);die;
                 //        $this->email->from($from, $from);
                 //        $this->email->to($to_email);
                 //        $this->email->subject('Welcome to Transcura');
                 //        $this->email->message($this->messageBody);
                 //        $this->email->send();
                   //      $this->response(array("status"=>"success","message"=> "User has been successfully register.","statusCode"=>"true"), 200);  
                        

						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Patient has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}



public function resize($image_data) {

	//print_r($image_data['raw_name'].$image_data['file_ext']);die;
        $img = substr($image_data['full_path'], 51);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $image_data['full_path'];
        //$config['new_image'] = './uploads/Patients/thumb_'.$image_data['raw_name'].$image_data['file_ext'];
        $config['new_image'] ='./uploads/Driver/thumb/thumb_'.$image_data['raw_name'].$image_data['file_ext'];
        $config['allowed_types'] = 'png|mpg|mpeg|wmv|jpg|JPEG|jpeg|PNG';       
        $config['width'] = 250;
        $config['height'] = 250;
        $this->image_lib->initialize($config);
        $src = $config['new_image'];
        $data['new_image'] = substr($src, 2);
        $data['img_src'] = base_url().$data['new_image'];
        if(!$this->image_lib->resize())
		{
		 $error = $this->image_lib->display_errors();
		 echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
	    //return $error;	
		}
		else
		{
        return $data;
		      
		}
    }



  public function change_status() {

  	if(is_ajax_request())
		{
			if(is_user_logged_in()){
					//print_r($_REQUEST);die;
					$postData = $this->input->post();
					$userData = $this->common->selectQuery("status",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
					//print_r($userData);die;
				if($userData[0]["status"] == "active"){
						$status_update = "inactive";
					}
					else if($userData[0]["status"] == "inactive"){
						$status_update = "active";
					}
				$insertArr = array('status'=>$status_update,'updated_at'=>date("Y-m-d h:i:s", time()));

				if($postData["key"]){
					$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					//echo $this->db->last_query();die;
					$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));

//                            if($userData[0]["status"] == "active"){
// 					$message = file_get_contents("media/emails/accountactivation.html");
// 					$message = preg_replace('/{user}/', $userData[0]['first_name'], $message);
// 					$message = preg_replace('/{user_id}/', $userData[0]['id'], $message);
// 					$message = preg_replace('/{url}/', $config['base_url'], $message);
// 					$this->load->library('email');  
// 					$config['charset'] = 'iso-8859-1';
// 					$config['wordwrap'] = TRUE;
// 					$config['mailtype'] = 'html';
// 					$this->email->initialize($config);  
// 					$this->email->from(EMAIL_FROM, 'tanuja'); 
// 					$this->email->to($userData[0]['email']);
// 					$this->email->subject('Patient Activation mail'); 
// 					$this->email->message($message); 
// 					$this->email->send();
// }
					if($insertId){
						echo json_encode(array("status"=>"success","userData"=>$userData[0],"msg"=>"Driver has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

}
