<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Transportation extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		$this->load->library('image_lib');
		$config['allowed_types'] =   "png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG";  
		$config['max_size']      =   "5000"; 
		$config['max_width']     =   "1000"; 
		$config['max_height']    =   "900"; 
        $config['min_width']     = "70"; // new
        $config['min_height']    = "70"; // new    
        $config['upload_path']   =  "./uploads/Member/original/" ;   
        $this->load->library('upload', $config);
        $this->load->helper("email_template");
    }


    public function index()
    {
    	if(is_user_logged_in())
    	{
    		$this->load->view('transportation/listTransProv');
    		$this->load->model('common_model');			
    	}else{
    		redirect("login");
    		exit;
    	}
    }

	// --------- listing of transportation provider --------
    public function listTranportation()
    {
    	if(is_ajax_request())
    	{
    		if(is_user_logged_in())
    		{			
    			$postData = $this->input->post();				
    			$arrayColumn = array("id"=>"id","fname"=>"full_name","email"=>"email","phone"=>"phone");
    			$arrayStatus["is_active"] = array();
    			$arrayColumnOrder = array("ASC","asc","DESC","desc");
    			$where =array("user_type"=>"4" ,"done"=>"1");
    			$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'id','id',TB_USERS,'*','listTranportation',$where);
    			$rows = '';
    			if(!empty($result['rows']))
    			{
    				$i=1;
    				foreach ($result['rows'] as $user)
    				{
    					$userId = $this->encrypt->encode($user['id']);
    					$rows .= '<tr id="'.$userId.'">
    					<td class="text-left">'.$i.'</td>	                           
    					<td class="text-left">'.$user['full_name'].'</td>	                           
    					<td class="text-left">'.$user['email'].'</td>
    					<td class="text-left">'.$user['phone'].'</td>
    					<td class="text-left">
    						<div class="btn-group btn-toggle" id="accept"> 
    							<a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="changeStatus(this)" href="javascript:void(0)">
    								'.ucfirst($user['status']).'	
    							</a>
    						</div>
    					</td>
    					<td class="text-left">
    						<a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="getTransportation(this)" href="javascript:void(0)">
    							<i class="fa fa-fw fa-check-square-o"></i>
    						</a>
    						<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="deleteTransportation(this)" href="javascript:void(0)">
    							<i class="fa fa-fw fa-close"></i>
    						</a>
    					</td>
    				</tr>';
    				$i++;
    			}
    		}
    		else
    		{
    			$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
    		}
    		$data["rows"] = $rows;
    		$data["pagelinks"] = $result["pagelinks"];
    		$data["entries"] = $result['entries'];
    		$data["status"] = "success";
    		echo json_encode($data);

    	}else
    	{
    		redirect("login");
    		exit;
    	}
    }
}

//------------ get transportation for edit ----------
public function getTransportProv(){
	if(is_ajax_request())
	{
		if(is_user_logged_in()){
			$postData = $this->input->post();
			$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));	
			$dataState = $this->Common_model->selectQuery("*",TB_STATE,array('country_id'=>$userData[0]['country']));
			$dataCity = $this->Common_model->selectQuery("*",TB_CITY,array('state_id'=>$userData[0]['state']));

			$stateJSON='';
			if($dataState > 0)
			{
				$stateJSON .= '<option value="">Select state</option>';
				foreach ($dataState as $key => $state_name) {
					$stateJSON.= '<option value="'.$state_name['id'].'">'.$state_name['state_name'].'</option>';
				}       		
			}
			else
			{
				$stateJSON.= '<option value="">State not available</option>';
			}


			$cityJSON='';
			if($dataCity > 0)
			{
				$cityJSON .= '<option value="">Select City</option>';
				foreach ($dataCity as $key => $city_name) {
					$cityJSON.= '<option value="'.$city_name['id'].'">'.$city_name['city_name'].'</option>';
				}       		
			}
			else
			{
				$cityJSON.= '<option value="">City not available</option>';
			}
			if($userData){
				echo json_encode(array("status"=>"success","userData"=>$userData[0],"stateJSON"=>$stateJSON,"cityJSON"=>$cityJSON)); exit;
			}else{
				echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
			}
		}else{
			echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
		}
	}
}

// -------- save transportation provider -------
public function saveTransProv(){

	if(is_user_logged_in()){
		$postData = $this->input->post();	
		if(isset($_FILES["pimage"]["name"]))   
		{   
			if(!$this->upload->do_upload('pimage'))
			{		 
				$error = $this->upload->display_errors();
				echo json_encode(array("status"=>"error","msg"=>"For profile picture ". $error)); exit;
			} 
			else
			{		 
				$data=$this->upload->data();
				$data = $this->resize($data);					        
				$data['picture'] = $data['new_image'];	
			}				
		}
		else if(isset($postData['oldimg']))   
		{    		            
			$data['picture'] =$postData['oldimg'];	
		}
		else
		{
			$data['picture'] = "images/default.jpg";	
		}

		$birth = date("Y-m-d", strtotime($postData["birth"])); 
		$insertArr = array('full_name'=>$postData["full_name"],	
			'zipcode'=>$postData["zipcode"],
			'phone'=>$postData["phone"],			
			'date_of_birth'=>$birth	,
			'emergency_contactname' =>$postData['econtact_name'],
			'emergency_contactno'=>$postData["econtact_no"],
			'picture'=> $data['picture'],
			'signature'=> $postData['signature'],
			'done'	=>1,
			'state'=> $postData['state'],
			'city'=> $postData['city'],
			'street'=>$postData['street'],
			'county' =>$postData['county']
			);
		// echo "<pre>";print_r($insertArr);die;
		if($postData["user_key"]){
			$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);			
			if($insertId){
				echo json_encode(array("status"=>"success","msg"=>"User has been updated successfully.")); exit;	
			}else{
				echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
			}
		}else{
			echo json_encode(array("status"=>"error","msg"=>"Oops!!! Something went wrong.")); exit;	
			
		}
	}
}

//--------- chage status ----------
public function change_user_status()
{
	if(is_ajax_request())
	{
		if(is_user_logged_in()){			
			$postData = $this->input->post();
			$userData = $this->common->selectQuery("status",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
			if($userData[0]["status"] == "active")
			{
				$status_update = "inactive";
			}
			else if($userData[0]["status"] == "inactive")
			{
				$status_update = "active";
			}
			$insertArr = array('status'=>$status_update,'updated_at'=>date("Y-m-d h:i:s", time()));
			if($postData["key"])
			{
				$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])),$insertArr);
				
				if($insertId)
				{
					// ------- change the status of driver registered under TP and send mail -------
					$updatetpDriver = $this->common->update(TB_USERS,array('transport_parent_id'=>$this->encrypt->decode($postData['key'])),$insertArr);

					if($status_update == "active")
					{
						$cond = "id='".$this->encrypt->decode($postData['key'])."' OR transport_parent_id='".$this->encrypt->decode($postData['key'])."'";
						$userData1 = $this->Common_model->getMaster(TB_USERS,$cond,FALSE,FALSE,FALSE,"*");
						
						foreach ($userData1 as $valued) {
						$message = file_get_contents("media/emails/accountactivation.html");
						$message = preg_replace('/{user}/', $valued['full_name'], $message);
						$message = preg_replace('/{user_id}/', $valued['id'], $message);
						$this->load->library('email');  
						$config['charset'] = 'iso-8859-1';
						$config['wordwrap'] = TRUE;
						$config['mailtype'] = 'html';
						$this->email->initialize($config);  
						$this->email->from(EMAIL_FROM, 'Admin'); 
						$this->email->to($userData[0]['email']);
						$this->email->subject('User Activation mail'); 
						$this->email->message($message); 
						$this->email->send();
						}						
					}
					echo json_encode(array("status"=>"success","msg"=>"User status has been change successfully.")); exit;	

				}else
				{
					echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
				}





				
				
			}
		}
	}
}

public function resize($image_data) 
{
	$date = date("Y-m-d-H-i-s");
	$img = substr($image_data['full_path'], 51);
	$config['image_library'] = 'gd2';
	$config['source_image'] = $image_data['full_path'];
	$config['new_image'] ='./uploads/Transport/thumb/thumb_'.$image_data['raw_name'].$date.$image_data['file_ext'];
	$config['allowed_types'] = 'png|mpg|mpeg|wmv|jpg|JPEG|jpeg|PNG';       
	$config['width'] = 250;
	$config['height'] = 250;
	$this->image_lib->initialize($config);
	$src = $config['new_image'];
	$data['new_image'] = substr($src, 2);
	$data['img_src'] = base_url().$data['new_image'];
	if(!$this->image_lib->resize())
	{
		$error = $this->image_lib->display_errors();
		echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;		    
	}
	else
	{
		return $data;		      
	}
}

public function listAssignRides()
{
	if(is_ajax_request())
	{
		if(is_user_logged_in()){
			$postData = $this->input->post();
			$postData = $this->input->post();				
			$arrayColumn = array("id"=>"id","name"=>"user_id","trip_id"=>"sr_trip_id","trip_date"=>"sr_date","sr_pick_up_time" => "sr_pick_up_time","source_address" => "source_address");
			$arrayStatus["is_active"] = array();
			$arrayColumnOrder = array("ASC","asc","DESC","desc");
			$where =array("admin_status"=>"2",'DATE(sr_date) >=' => date('Y-m-d'));
			$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,TB_SCHEDULE_RIDE.'.id',TB_SCHEDULE_RIDE.'.id',TB_SCHEDULE_RIDE,'*,'.TB_SCHEDULE_RIDE.'.id as sch_id','listAssignRides',$where,array(TB_USERS=>TB_USERS.'.id ='.TB_SCHEDULE_RIDE.'.user_id'));

			/*print_r($result);
			exit();*/

			//select distinct(u2.transport_parent_id) as parent from tbl_users u1 ,tbl_users u2 where u1.id = u2.transport_parent_id

			$res  = $this->common->select('distinct(transport_parent_id)',TB_USERS,array('transport_parent_id !='=>0));
			//
			/*$res = $this->common->selectQuery('distinct(u2.transport_parent_id)',TB_USERS.' u1',array("u1.user_type"=>"4" ,"u1.done"=>"1"),array(),array(TB_USERS.' u2'=>'u1.id = u2.transport_parent_id'));*/
			foreach($res as $val)
			{
				$str[] =$val['transport_parent_id'];
			}
			
			$listTp = $this->common->selectWhereIn('*',TB_USERS,'id',$str);

			
			$rows = '';
    			if(!empty($result['rows']))
    			{
    				$i=1;
    				foreach ($result['rows'] as $user)
    				{
    					$userId = $this->encrypt->encode($user['id']);
    					$rows .= '<tr id="'.$userId.'">
    					<td class="text-left">'.$user['full_name'].'</td>	                           
    					<td class="text-left">'.$user['sr_trip_id'].'</td>
    					<td class="text-left">'.$user['sr_date'].'</td>
    					<td class="text-left">'.$user['sr_pick_up_time'].'</td>
    					<td class="text-left">'.$user['source_address'].'</td>';
    					
    					if(2 == $user['admin_status'])
    					{
    						if(count($listTp) > 0)
    						{
    							$rows .= '<td class="text-left"><select class="form-group" onchange="assignRide('.$user['sch_id'].',this.value)"><option value="">select</option>';
	    						foreach($listTp as $data)
	    						{
	    							$rows .= '<option value="'.$data["id"].'">'.$data["full_name"].'</option>';	
	    						}
	    						$rows .= '</select></td>';
	    					}
	    					else
	    					{
	    						$rows .= '<td class="text-left"><select class="form-group" ><option value="">No Transpotation Provider available</option></select></td>';
	    					}
    						
    					}
    					$rows .='<td class="text-left">
    						<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="deleteTransportation(this)" href="javascript:void(0)">
    							<i class="fa fa-fw fa-close"></i>
    						</a>
    					</td>
	    				</tr>';
	    				$i++;
    				}
    			}
		}
		else
		{
			$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
		}
		$data["rows"] = $rows;
		$data["pagelinks"] = $result["pagelinks"];
		$data["entries"] = $result['entries'];
		$data["status"] = "success";
		echo json_encode($data);
	}
}

public function assignRideToTp()
{
	if(is_ajax_request())
	{
		if(is_user_logged_in())
		{
			$checkRide = $this->common->select('*',TB_SCHEDULE_RIDE,array('id' => $this->input->post('id')));

			if(count($checkRide) > 0)
			{
				$data = array('admin_status' => 3 , 'assign_to_tp' => $this->input->post('val'));

				$update = $this->common->update(TB_SCHEDULE_RIDE,array('id' => $this->input->post('id')),$data);

				if($update)
				{
					echo json_encode(array("status"=>"success","msg"=>"Trip assigned to Transpotation Provider.")); exit;			
				}
				else
				{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong !!.")); exit;				
				}
			}
		}
		else
		{
			echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
		}
	}
}
}
?>