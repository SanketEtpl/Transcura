<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct()
	{
		parent::__construct();
        $this->load->model("Dashboard_model");
   // echo "GVJGHJGHJGh" ;die;
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$dashoard_count['member_count'] =$this->Dashboard_model->get_count(TB_USERS,"1","1");
			$dashoard_count['patient_count']= $this->Dashboard_model->get_count(TB_USERS,"2","1");
			$dashoard_count['healthcare_count'] =$this->Dashboard_model->get_count(TB_USERS,"3","1");
			$dashoard_count['transport_count'] =$this->Dashboard_model->get_count(TB_USERS,"4","1");
			$dashoard_count['driver_count']= $this->Dashboard_model->get_count(TB_USERS,"5","1");
			$dashoard_count['serviceprovider_count']=2;//$this->Dashboard_model->get_count(TB_SERVICE_PROVIDERS);
			$this->load->view('dashboard/listDashboard',$dashoard_count);
		}else{
			redirect("admin");
			exit;
		}
	}

	public function get_chart_status()
	{
			echo json_encode($this->Dashboard_model->get_chart_status(), true);
	}
}