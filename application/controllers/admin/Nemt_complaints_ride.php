<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nemt_complaints_ride extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');		
	}

	public function index()
	{
 		if(is_user_logged_in())
		{
			$this->load->view('dynamic_records/listOfNemtComplaintsRide');			
		}else{
			redirect("login");
			exit;
		}
	}

	public function listOfNemtComplaintsRide(){	
		if(is_ajax_request())
		{	
			if(is_user_logged_in()){			
				$postData = $this->input->post();				
				$arrayColumn = array("full_name"=>"full_name","src_driver_name"=>"src_driver_name","src_date_of_service"=>"src_date_of_service");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where=array(TB_USERS.'.user_type'=>2);
                $join=array();
				$join = array(TB_USERS=>TB_SCHEDULE_RIDE_COMPLAINTS.'.user_id='. TB_USERS.'.id',TB_COMPLAINTS_TYPE=>TB_SCHEDULE_RIDE_COMPLAINTS.'.src_complaint_type='.TB_COMPLAINTS_TYPE.'.id');
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'full_name',"'id'",TB_SCHEDULE_RIDE_COMPLAINTS,'DISTINCT ('.TB_SCHEDULE_RIDE_COMPLAINTS.'.id),'.TB_SCHEDULE_RIDE_COMPLAINTS.'.src_driver_name,'.TB_SCHEDULE_RIDE_COMPLAINTS.'.src_date_of_service,'.TB_SCHEDULE_RIDE_COMPLAINTS.'.src_name_of_transportation_provider,'.TB_SCHEDULE_RIDE_COMPLAINTS .'.src_description,'.TB_COMPLAINTS_TYPE.'.ct_name,'.TB_USERS.'.full_name','listOfNemtComplaintsRide', $where,$join,"","user_id");
            	$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['id']);
						$rows .= '<tr id="'.$userId.'">
							<td class="text-left">'.$user['full_name'].'</td>
							<td class="text-left">'.$user['src_driver_name'].'</td>						   
							<td class="text-left">'.$user['src_date_of_service'].'</td>
							 <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getDetails(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-eye"></i>
										  <a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="delete_complaints_ride(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>	                              
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getDetails(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$query = $this->db->query("SELECT DISTINCT (src.id), src.src_driver_name, src.src_date_of_service, src.src_name_of_transportation_provider, src.src_description, ct.ct_name, u.full_name
				FROM `tbl_schedule_ride_complaints` as `src`
				JOIN `tbl_users` as `u` ON `src`.`user_id`=`u`.`id`
				JOIN `tbl_complaints_type` as `ct` ON `src`.`src_complaint_type`=`ct`.`id`
				WHERE `u`.`user_type`=2 AND `src`.`id`=".$this->encrypt->decode($postData['key'])."");
				$getDetails=$query->result_array();	
				if($getDetails){
					echo json_encode(array("status"=>"success","userData"=>$getDetails[0]), true); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	public function delete_complaints_ride() 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();				
				$deleteId = $this->common->delete(TB_SCHEDULE_RIDE_COMPLAINTS,array('id'=>$this->encrypt->decode($postData['key'])));
				if($deleteId){
					echo json_encode(array("status"=>"success","msg"=>"Complaint has been deleted successfully.")); exit;	
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
				}
			}
		}
	}
}
