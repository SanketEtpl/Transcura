<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Schedule extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		//echo "fghfghfg" ;die;
	}

	public function index()
	{
 		if(is_user_logged_in())
		{
			$this->load->view('schedule/listSchedule');
			$this->load->model('common_model');
		}else{
			redirect("login");
			exit;
		}
	}

	public function listSchedule(){		
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();				
				$arrayColumn = array("id"=>"id"  );
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                  $where=array();
                  $join=array();
                 // $arrayColumngroup =array("user_id" =>"user_id");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'id','id',TB_SCHEDULE_RIDE,'DISTINCT (user_id),sr_status,sr_state,sr_country,sr_date,sr_appointment_time','listSchedule', $where,$join,'','user_id');			
           // echo $this->db->last_query();die;
    			$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;

					foreach ($result['rows'] as $user) {
						$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$user['user_id']));
						$userId = $this->encrypt->encode($user['user_id']);
                        $state = $this->common->selectQuery("state_name",TB_STATE,array('id' =>$user['sr_state']));
                       $country = $this->common->selectQuery("country_name",TB_COUNTRY,array('id' =>$user["sr_country"]));
						if($user["sr_status"] == "1"){
					$status_update = "pending";
					}
					else if($user["sr_status"] == "2"){
					$status_update = "Accepted";
					}
					else if($user["sr_status"] == "3"){
					$status_update = "Cancelled";
					}

						$rows .= '<tr id="'.$userId.'">
							<td class="text-left">'.$userData[0]['full_name'].'</td>
							<td class="text-left">'.$user['sr_date'].'</td>						   
							<td class="text-left">'.$user['sr_appointment_time'].'</td>
								<td class="text-left">'.$country[0]['country_name'].'</td>
							<td class="text-left">'.$state[0]['state_name'].'</td>
						

	                            
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getDetails(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$getDetails = $this->common->selectQuery("*",TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])));
								
				if($userData){
					echo json_encode(array("status"=>"success","getDetails"=>$getDetails[0]), true); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	

	
  public function change_status() {

  	if(is_ajax_request())
		{
			if(is_user_logged_in()){
					//print_r($_REQUEST);die;
					$postData = $this->input->post();
					$userData = $this->common->selectQuery("sr_status",TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])));
					//print_r($userData);die;
				if($userData[0]["sr_status"] == "1"){
					$status_update = "pending";
					}
					else if($userData[0]["sr_status"] == "2"){
					$status_update = "Accepted";
					}
					else if($userData[0]["sr_status"] == "3"){
					$status_update = "Cancelled";
					}

				$insertArr = array('status'=>$status_update,'updated_at'=>date("Y-m-d h:i:s", time()));
				if($postData["key"]){
					$insertId = $this->common->update(TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					$userData = $this->common->selectQuery("*",TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])));
					if($insertId){
						echo json_encode(array("status"=>"success","userData"=>$userData[0],"msg"=>"User has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

}
