<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Properties extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$this->load->view('properties/listProperties');
			$this->load->model('common_model');
		}else{
			redirect("login");
			exit;
		}

	}

	public function listProperties(){
	//print_r("ghjfgh");die;
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();
				//print_r($postData) ;die;
				$arrayColumn = array("id"=>"id","name"=>"name","property_type"=>"property_type");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");

				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'name','id',TB_PROPERTIES,'*','listProperties');
              // print_r($result) ;die;
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['id']);
						
						//print_r($userId);
						$rows .= '<tr id="'.$userId.'">
	                            <td class="text-left">'.$user['name'].'</td>
	                            <td class="text-left">'.$user['property_type'].'</td>
	                            <td class="text-left">'.$user['address'].'</td>
								
								
	                           
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	
  

}
