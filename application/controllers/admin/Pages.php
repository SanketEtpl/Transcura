<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$this->load->view('pages/listPages');
		}else{
			redirect("login");
			exit;
		}
	}

	public function listPages(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$arrayColumn = array("page_id"=>"page_id","page_title"=>"page_title" ,"sub_title"=>"sub_title");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$where=array("");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'page_id','page_name',TB_PAGES,'*','listPages',$where);
              // echo $this->db->last_query();die;
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $pages) {
						$page_id = $this->encrypt->encode($pages['page_id']);

$string = strip_tags($pages['page_detail']);

if (strlen($string) > 250) {

    // truncate string
    $stringCut = substr($string, 0, 250);

    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
}




						$rows .= '<tr id="'.$page_id.'">
	                            <td class="text-left">'.$pages['page_title'].'</td>
	                            <td class="text-left">'.$pages['sub_title'].'</td>
	                            <td class="text-left">'.



	                            $string.'</td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$page_id.'" class="" onclick="getPage(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
									</a>
	                                <a data-id="'.$i.'" data-row-id="'.$page_id.'" class="" onclick="deletePage(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getPage(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){

				$postData = $this->input->post();
				//print_r($this->encrypt->decode($postData['key']));
				//print_r($this->db->last_query());die;);die;
				$pageData = $this->common->selectQuery("page_title,page_detail,page_name,sub_title",TB_PAGES,array('page_id'=>$this->encrypt->decode($postData['key'])));
				//echo $this->db->last_query();die;
				if($pageData){
					echo json_encode(array("status"=>"success","pageData"=>$pageData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong, Please try again...!!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function savePage(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
$testImage ='';
				$pageData = $this->common->selectQuery('image_icon_upload',TB_PAGES,array('page_id'=>$this->encrypt->decode($postData['page_key'])));
				
				if(isset($_FILES["image_icon"]["name"]))   
		  		{   
			        $file_name = $_FILES["image_icon"]["name"];
			        $original_file_name = $file_name;
			        $random = rand(1, 10000000000000000);			        
			        $file_name_rename = $random;
			        $date = date("Y_m_d_H_i_s");
			        $explode = explode('.', $file_name);
			        if(count($explode) >= 2) 
			        {
			            $new_file = $file_name_rename.$date.'.'.$explode[1];			            
			            $config['upload_path'] = "./assets/img";
			            $config['allowed_types'] ="png|jpeg|jpg";
			            $config['file_name'] = $new_file;
			            $config['max_size'] = '307210';
			            $config['max_width'] = '300000';
			            $config['max_height'] = '300000';
			            $config['overwrite'] = TRUE;
			            $this->load->library('upload',$config);
			            $this->upload->initialize($config);
						if(!$this->upload->do_upload("image_icon"))
						{
							$error = $this->upload->display_errors();
							echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
						}
						else
						{
							$testImage = 'assets/img/'.$new_file;   
						}
		           		 }
				}
				else if(isset($pageData[0]['image_icon_upload']))   
			    	{          	 
			    	$testImage =$pageData[0]['image_icon_upload'];		        				
				}
				else
				{
				   	$testImage ="";	
				}	
				$dateTime = date("Y-m-d H:i:s");
				$insertArr = array(
						'page_title'=>$postData["title"],
						"page_detail"=>$postData["page_detail"],
						"page_name"=>$postData['page_name'],										
						"image_icon_upload"=>$testImage,		
						"updated_at" => $dateTime,
						"sub_title"=>$postData['sub_title']
						);
				if($postData["page_key"]){
					$insertId = $this->common->update(TB_PAGES,array('page_id'=>$this->encrypt->decode($postData['page_key'])),$insertArr);
					
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Content has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					$insertArr["created_at"] = $dateTime;
					$insertId = $this->common->insert(TB_PAGES,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Content has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}

    }


   public function deletePage(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				//print_r($postData );die;
				$dateTime = date("Y-m-d H:i:s");	
					
				if($postData["key"]){
					$deleteImage = $this->common->selectQuery("image_icon_upload",TB_PAGES,array("page_id"=>$this->encrypt->decode($postData['key'])));
					if(!empty($deleteImage[0]))
					unlink($deleteImage[0]['image_icon_upload']);					
					$insertId = $this->common->delete(TB_PAGES,array('page_id'=>$this->encrypt->decode($postData['key'])));
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Content has been deleted successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
				else{
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
				}
			}
		}

    }


}
