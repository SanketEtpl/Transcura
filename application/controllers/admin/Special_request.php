<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Special_request extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{		
		if(is_user_logged_in())
		{
			$this->load->view('dynamic_records/listOfSpecialRequestData');
		}
		else
		{
			redirect("login");
			exit;
		}
	}

	public function list_of_special_request_data(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();				
				$arrayColumn = array("id"=>"id","sr_name"=>"sr_name");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				//$where=array("");
				// $result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'id','id'
				// 	,TB_SPECIAL_REQUEST,'id,sr_name','listAllSpecialRequestData');    
				$where=array("");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'id','id',TB_SPECIAL_REQUEST,'*','listOfSpecialRequestData',$where);	          
				$rows = '';				
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $data) {
						$service_id = $this->encrypt->encode($data['id']);
						$rows .= '<tr id="'.$service_id.'">								
	                            <td class="text-left">'.$i.'</td>
	                            <td class="text-left">'.$data['sr_name'].'</td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$service_id.'" class="" onclick="getSpecialRequest(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
									</a>
	                                <a data-id="'.$i.'" data-row-id="'.$service_id.'" class="" onclick="deleteSpecialRequest(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>
	                            </td>
	                        </tr>';
	                        $i++;
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}
			else
			{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}


	public function getSpecialRequest(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){				
				$postData = $this->input->post();
				$pageData = $this->common->selectQuery("*",TB_SPECIAL_REQUEST,array('id'=>$this->encrypt->decode($postData['key'])));
				
				if($pageData){
					echo json_encode(array("status"=>"success","pageData"=>$pageData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong, Please try again...!!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function save_special_request(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$insertArr = array(
								'sr_name'=>$postData["sr_name"],								
								);
				if($postData["page_key"]){
					$insertId = $this->common->update(TB_SPECIAL_REQUEST,array('id'=>$this->encrypt->decode($postData['page_key'])),$insertArr);
					
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Special request has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					//$insertArr["created_at"] = $dateTime;
					$insertId = $this->common->insert(TB_SPECIAL_REQUEST,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Special request has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"add","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
    }


   public function deleteSpecialRequest(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();					
				if($postData["key"]){					
					$deleteId = $this->common->delete(TB_SPECIAL_REQUEST,array('id'=>$this->encrypt->decode($postData['key'])));
					if($deleteId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Special request has been deleted successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
    }
}