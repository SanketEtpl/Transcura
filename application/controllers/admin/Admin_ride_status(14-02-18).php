<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_ride_status extends CI_Controller {

	function __construct()
	{
		parent::__construct();		
	}

	public function index()
	{		
		if(is_user_logged_in())
		{
			$this->load->view('dynamic_records/adminRideStatus');
		}else{
			redirect("login");
			exit;
		}
	}

	public function listOfScheduleRide(){		
		if(is_ajax_request())
		{

			if(is_user_logged_in()){		
				$curDate = date("Y-m-d");	
				$postData = $this->input->post();				
				$arrayColumn = array("full_name"=>"full_name","sr_trip_id"=>"sr_trip_id","admin_status"=>"admin_status","sr_pick_up_time"=>"sr_pick_up_time","sr_date"=>"sr_date","urjency_explainatin"=>"urjency_explainatin","source_address"=>"source_address");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where=array(TB_USERS.".user_type"=>"2",TB_SCHEDULE_RIDE.".sr_date >="=>$curDate);
                $join=array();               	
				$join = array(TB_USERS=>TB_SCHEDULE_RIDE.'.user_id='. TB_USERS.'.id');
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'urjency_explainatin',"'id'",TB_SCHEDULE_RIDE,'DISTINCT ('.TB_SCHEDULE_RIDE.'.id),'.TB_SCHEDULE_RIDE.'.driver_id,'.TB_SCHEDULE_RIDE.'.source_address,'.TB_SCHEDULE_RIDE.'.sr_trip_id,'.TB_SCHEDULE_RIDE.'.urjency_explainatin,'.TB_SCHEDULE_RIDE.'.sr_date,'.TB_SCHEDULE_RIDE .'.sr_pick_up_time,'.TB_USERS.'.full_name,'.TB_SCHEDULE_RIDE.'.sr_pick_up_time,'.TB_SCHEDULE_RIDE.'.user_id,'.TB_SCHEDULE_RIDE.'.admin_status','listOfScheduleRide', $where,$join,"","user_id");
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;

					foreach ($result['rows'] as $value) {
						if($value['urjency_explainatin']!="")
							$urgency=$value['urjency_explainatin'];
						else
							$urgency='None';
						$status ="";
						$adminApprv = "";
						$ride_id = $this->encrypt->encode($value['id']);
						$driver_id = $this->encrypt->encode($value['driver_id']);

						if($value['admin_status'] == 1)
							 $status = "Open ride";
						else if($value['admin_status'] == 2)
							 $status = '<a data-id="'.$i.'" data-row-id="'.$driver_id.'" class="" onclick="driverDetails(this)" href="javascript:void(0)">Accepted</a>';
						else if($value['admin_status'] == 3)
							 $status = "Closed ride";
						else if($value['admin_status'] == 5)
							 $status = "Request sent";	
						else
							 $status = "Rejected ride";
						$rows .= 
								'<tr id="'.$ride_id.'">
									<td class="text-left">'.$value['full_name'].'</td>
									<td class="text-left">'.$urgency.'</td>
									<td class="text-left">'.$value['sr_trip_id'].'</td>
		                            <td class="text-left">'.$value['sr_date'].'</td>
		                            <td class="text-left">'.$value['sr_pick_up_time'].'</td>
		                            <td class="text-left">'.$value['source_address'].'</td> 
		                            <td class="text-left">'.$status.'</td> 
		                            <td class="text-left">
		                            	<a data-id="'.$i.'" data-row-id="'.$ride_id.'" class="" onclick="getScheduleRide(this)" href="javascript:void(0)">
											<i class="fa fa-fw fa-eye"></i>
										</a>
										<a data-id="'.$i.'" data-row-status="1" data-row-id="'.$ride_id.'" onclick="changeStatus(this)" href="javascript:void(0)">
										Accept											
										</a> / 
										<a data-id="'.$i.'" data-row-status="2" data-row-id="'.$ride_id.'" onclick="changeStatus(this)" href="javascript:void(0)">
										Reject
										</a>
	                            	</td>
	                        	</tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function driverDetails()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();				
				$pageData = $this->common->selectQuery("full_name,email,date_of_birth,phone,picture",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
				if($pageData)
				{
					echo json_encode(array("status"=>"success","driverDetails"=>$pageData[0])); exit;
				}
				else
				{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong, Please try again...!!!")); exit;
				}
			}
			else
			{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}		
	}

	public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
	    $theta = $lon1 - $lon2;
	    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	    $dist = acos($dist);
	    $dist = rad2deg($dist);
	    $miles = $dist * 60 * 1.1515;
	    $unit = strtoupper($unit);
		if ($unit == "K") {
		    return ($miles * 1.609344);
		} else if ($unit == "N") {
	      return ($miles * 0.8684);
	    } else {
	        return $miles;
	    }
	}

	public function change_user_status()
	{
	  	if(is_ajax_request())
		{
			$postData = $this->input->post();
			$statusData="";
			if($postData['status']==1)
			{ 
				$checkStatus = $this->Common_model->select('id,admin_status',TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])));
				$statusData = $checkStatus[0]['admin_status'];
				if($statusData == 5)
				$st = " request sent";
				else if($statusData == 2)
				$st =" accepted";
				else
				$st ="Open ride";	
			}
			else	
			{ $statusData = 4; $st ="rejected"; }
			$query = $this->db->query("SELECT * FROM  tbl_driver_location WHERE `updated_at` >= CURDATE( )");
			$getDriverLocation = $query->result_array();
			if(empty($getDriverLocation)){
				echo json_encode(array('status' => "error", "msg"=>"Today no drivers available."));
				exit();
			}
			else{
				//echo "exit()";exit;
				if($statusData != 1 ){
					$checkStatus = $this->Common_model->select('id,admin_status',TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key']),'admin_status'=>$statusData));
				}
				else{
					$checkStatus =array();
				}
				if(!empty($checkStatus)){	
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Schedule ride already".$st)); exit;	
				}else{
					if($postData['status'] == 1){
						$rideInfo = $this->Common_model->select('sr_trip_id,id,source_address,destination_address,sr_total_distance,sr_date,sr_pick_up_time,sr_lat_lng_source,user_id',TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])));
						if(count($rideInfo)>0){						
						 	list($lat,$lng) =explode(',', $rideInfo[0]['sr_lat_lng_source']);
						} else {
							$lat = $lng = 0;
						}
						$driverLoc = $this->Common_model->select('*',TB_DRIVER_LOCATION);
						$result = $updateStatus = $notifResult ="";
						foreach ($driverLoc as $key => $value) {
							$getDriverLoc = $this->distance($value['loc_lat'], $value['loc_long'], $lat, $lng, "M");
							if( round($getDriverLoc) <= 5 ){
								$insertArr =array(
									"sched_id" =>$rideInfo[0]['id'],
									"driver_id"=>$value['user_id'],
									"created_date"=>date("Y-m-d H:i:s"),
									"ride_status" =>"Request Sent"
								); 
								$notificationArr = array(
									"n_title" =>"Schedule a ride request",
									"n_full_description"=>"Please check your trip request.<br />Schedule ride date :<b>".$rideInfo[0]['sr_date']."</b> <br />Pick up time :<b>".$rideInfo[0]['sr_pick_up_time']."</b><br />Total distance :<b>".$rideInfo[0]['sr_total_distance']."</b> <br />Source location :<b>".$rideInfo[0]['source_address']."</b><br /> Destination location: <b>".$rideInfo[0]['destination_address']."</b>",
									"notification_flag"=>2,
									"read_or_unread" =>1,
									"n_status"=>'ME',
									"user_id" =>$value['user_id'],
									"created_on" => date("Y-m-d H:i:s"),
									);
								$notifResult = $this->Common_model->insert(TB_NOTIFICATION,$notificationArr);
								$result = $this->Common_model->insert(TB_DRIVER_REQUEST,$insertArr);	
								$updateArr = array('admin_status'=>5);
								$updateStatus = $this->common->update(TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])),$updateArr);
										
							} else {								
								echo json_encode(array("status"=>"error","action"=>"update","msg"=>"No driver available within 5 miles.")); exit;	
							}
						}
						if($result && $updateStatus && $notifResult){
							echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Ride request has been ".$st)); exit;	
						} else {
							echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Request ride has been failed.")); exit;	
						}	
					}
					else
					{
						$deleteRecode =  $this->Common_model->delete(TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])));
						if($deleteRecode)
						{
							echo json_encode(array("status"=>"success","msg"=>"Scheduel ride request has been deleted")); exit;
						}
						else
						{
							echo json_encode(array("status"=>"error","msg"=>"Something went wrong, Please try again...!!!")); exit;
						}
					}
				}
			}			
		}
	}

	public function getScheduleRide()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();				
				$pageData = $this->common->selectQuery("*",TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])));
				
				if($pageData)
				{
					echo json_encode(array("status"=>"success","pageData"=>$pageData[0])); exit;
				}
				else
				{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong, Please try again...!!!")); exit;
				}
			}
			else
			{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}		
	}

	public function saveRideStatus(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$message ="";
				$postData = $this->input->post();
				if ($postData['admin_status']==2)
				{
					$checkStatus = $this->Common_model->select('id,admin_status',TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['user_key'])));
					$status = $checkStatus[0]['admin_status'];
					if($status == 5)
						$message = " request sent";
					else if($status == 2)
						$message =" accepted";	
					else
						$message =" open ride";									
				} else { 
						$message ="Rejected"; $status = 4; 
				}
				$query = $this->db->query("SELECT * FROM  tbl_driver_location WHERE `updated_at` >= CURDATE( )");
				$getDriverLocation = $query->result_array();
				if(empty($getDriverLocation)){
					echo json_encode(array('status' => "error", "msg"=>"Today no drivers available."));
					exit();
				}
				else{
					//$message ="Invalid";
					if($status != 1)
						$checkStatus = $this->Common_model->select('id,admin_status',TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['user_key']),'admin_status'=>$status));
				 	else
				 		$checkStatus =array();	
					if(!empty($checkStatus)){
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Schedule ride already".$message)); exit;	
					}
					else{
						if ($postData['admin_status'] ==2 ){
							$rideInfo = $this->Common_model->select('sr_trip_id,id,source_address,destination_address,sr_total_distance,sr_date,sr_pick_up_time,sr_lat_lng_source,user_id',TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['user_key'])));
							if(count($rideInfo)>0){						
							 	list($lat,$lng) =explode(',', $rideInfo[0]['sr_lat_lng_source']);
							} else {
								$lat = $lng = 0;
							}
							$driverLoc = $this->Common_model->select('*',TB_DRIVER_LOCATION);
							foreach ($driverLoc as $key => $value) {
								$getDriverLoc = $this->distance($value['loc_lat'], $value['loc_long'], $lat, $lng, "M");
								if( round($getDriverLoc) <= 5 ){
									$insertArr =array(
										"sched_id" =>$rideInfo[0]['id'],
										"driver_id"=>$value['user_id'],
										"created_date"=>date("Y-m-d H:i:s"),
										"ride_status" =>"Request Sent"
									); 
									$notificationArr = array(
										"n_title" =>"Schedule a ride request",
										"n_full_description"=>"Please check your trip request.<br />Schedule ride date :<b>".$rideInfo[0]['sr_date']."</b> <br />Pick up time :<b>".$rideInfo[0]['sr_pick_up_time']."</b><br />Total distance :<b>".$rideInfo[0]['sr_total_distance']."</b> <br />Source location :<b>".$rideInfo[0]['source_address']."</b><br /> Destination location: <b>".$rideInfo[0]['destination_address']."</b>",
										"notification_flag"=>2,
										"read_or_unread" =>1,
										"n_status"=>'ME',
										"user_id" =>$value['user_id'],
										"created_on" => date("Y-m-d H:i:s"),
									);
									$notifResult = $this->Common_model->insert(TB_NOTIFICATION,$notificationArr);
									$result = $this->Common_model->insert(TB_DRIVER_REQUEST,$insertArr);	
									$updateArr = array('admin_status'=>5);
									$updateStatus = $this->common->update(TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['user_key'])),$updateArr);
									}else{								
									echo json_encode(array("status"=>"error","action"=>"update","msg"=>"No driver available within 5 miles.")); exit;	
								}
							}
							if($result && $updateStatus && $notifResult){
								echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Your ride accepted successfully.")); exit;	
							} else {
								echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Request ride has been failed.")); exit;	
							}
						}else if ($postData['admin_status'] ==4 ){
							$deleteRecode =  $this->Common_model->delete(TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['user_key'])));
							if($deleteRecode){
								echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Schedule ride has been rejected successfully.")); exit;	
							}else{
								echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
							}
						}
						 else {
							$updateArr = array('admin_status'=>$postData["admin_status"]);
							$updateStatus = $this->common->update(TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['user_key'])),$updateArr);
						    if($updateStatus){
								echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Schedule ride has been ".$message)); exit;	
							}else{
								echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
							}
						}
					}
				}
			}
		}
	}	
}
