<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Drivers extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('image_lib');
		$config['allowed_types'] =   "png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG|DOCX|doc|docx|gif";  
		$config['max_size']      =   "5000"; 
		$config['max_width']     =   "1000"; 
		$config['max_height']    =   "900"; 
		$config['min_width']     = "70"; // new
		$config['min_height']    = "70"; // new    
		$config['upload_path']   =  "./uploads/Driver/original/" ;   
		$this->load->helper(array('form','url'));
		$this->load->model('Common_model');
		$this->load->library('upload', $config);
		$this->load->helper("email_template");
		$this->load->helper('email_template_helper');		
		$this->load->library('upload');
		$this->load->helper('security');
		$this->load->helper('string');
		$this->load->helper('common');
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);
			$data['typeOfServices'] = $this->Common_model->select("type_title,type_id",TB_SERVICE_TYPE);
			$this->load->view('drivers/listDrivers',$data);
			$this->load->model('common_model');
		}else{
			redirect("login");
			exit;
		}
	}

	//get city name
	public function get_city()
	{
		$postData = $this->input->post();
		$stateID = array('state_id'=>$postData['state_id']);
		$dataCity = $this->Common_model->select("*",TB_CITY,$stateID);
		$cityJSON = '';
		if($dataCity > 0)
		{
			$cityJSON .= '<option value="">Select city</option>';
			foreach ($dataCity as $key => $city) {
				$cityJSON.= '<option value="'.$city['id'].'">'.$city['city_name'].'</option>';
			}
		}
		else
		{
			$cityJSON .= '<option value="">City not available</option>';
		}
		echo json_encode(array('city'=>$cityJSON));
	}

	//get state name
	public function get_state()
	{		
		$postData = $this->input->post();
		$countryID=array('country_id'=>$postData['countryID']);
		$dataState = $this->Common_model->select("*",TB_STATE,$countryID);
		$stateJSON='';
		if($dataState > 0)
		{
			$stateJSON .= '<option value="">Select state</option>';
			foreach ($dataState as $key => $state_name) {
				$stateJSON.= '<option value="'.$state_name['id'].'">'.$state_name['state_name'].'</option>';
			}       		
		}
		else
		{
			$stateJSON.= '<option value="">State not available</option>';
		}
		echo json_encode(array('state'=>$stateJSON));		
	}

	public function listDrivers()
	{	
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();				
				$arrayColumn = array("id"=>"id","fname"=>"full_name","email"=>"email","phone"=>"phone");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$where =array("user_type"=>"5","done"=>1);
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'id','id',TB_USERS,'*','listDrivers',$where );

				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['id']);		
						$get_parent = getDriverParent($user['transport_parent_id']);		
						$parent = ($get_parent == "") ? "Normal Driver" : $get_parent;
						$approve_label=($user['is_approve'] == "1") ? "Approved" : "Not Approved";
						$rows .= '<tr id="'.$userId.'">
						<td class="text-left">'.$i.'</td>	                           
						<td class="text-left">'.$user['full_name'].'</td>	                           
						<td class="text-left">'.$user['email'].'</td>						
						<td class="text-left">'.$user['phone'].'</td>
						<td>'.$parent.'</td>								
						<td class="text-left">
							<div class="btn-group btn-toggle" id="accept"> 
								<a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="changeStatus(this)" href="javascript:void(0)" id="status">
									'.ucfirst($user['status']).'	
								</a> | 
								<a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="changeApproval(this)" href="javascript:void(0)">
									'.$approve_label.'	
								</a>
							</div>
						</td>
						<td class="text-left">
							<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getDriver(this)" href="javascript:void(0)" title="Edit">
								<i class="fa fa-fw fa-check-square-o"></i>
							</a>
							<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getDriverDoc(this)" href="javascript:void(0)" title="View Documents">
								<i class="fa fa-fw fa-eye"></i>
							</a>
							<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="deleteDriver(this)" href="javascript:void(0)" title="Delete">
								<i class="fa fa-fw fa-close"></i>
							</a>

						</td>
					</tr>';
					$i++;
				}
			}
			else
			{
				$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
			}
			$data["rows"] = $rows;
			$data["pagelinks"] = $result["pagelinks"];
			$data["entries"] = $result['entries'];
			$data["status"] = "success";
			echo json_encode($data);

		}else{
			echo json_encode(array("status"=>"logout"));
		}
	}
}

public function deleteDriver()
{
	if(is_ajax_request())
	{
		if(is_user_logged_in())
		{
			$postData = $this->input->post();
			if($postData["key"])
			{	

				$deleteFiles = $this->common->selectQuery("social_security_card_doc ,driver_license_doc,drug_test_results_doc,criminal_back_ground_doc,motor_vehicle_record_doc,`act_33_and_34_clearance_doc`,vehicle_inspections_doc,vehicle_maintnc_record_doc, vehicle_insurance_doc, vehicle_regi_doc,own_bus_vehicle_reg_doc, own_bus_insurance_pol_doc, own_bus_inspection_doc, own_bus_taxid_einno_doc",TB_USERS,array("id"=>$this->encrypt->decode($postData['key'])));
					//print_r($deleteFiles);exit;
				foreach ($deleteFiles as $key => $value) {		
					foreach ($value as $key1 => $unlinkFileName) {
						if(!empty($unlinkFileName))
							unlink($unlinkFileName);	
						else
							continue; 
					}
				}

				$insertId = $this->common->delete(TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
				if($insertId){											
					echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Driver user has been deleted successfully.")); exit;	
				}else{
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
				}
			}
			else
			{
				echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
			}
		}
	}
}

public function getDriver()
{
	if(is_ajax_request())
	{
		if(is_user_logged_in()){
			$postData = $this->input->post();
			$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
			$dataState = $this->Common_model->selectQuery("*",TB_STATE,array('country_id'=>231));
			$dataCity = $this->Common_model->selectQuery("*",TB_CITY,array('state_id'=>$userData[0]['state']));

			$stateJSON='';
			if($dataState > 0)
			{
				$stateJSON .= '<option value="">Select state</option>';
				foreach ($dataState as $key => $state_name) {
					$stateJSON.= '<option value="'.$state_name['id'].'">'.$state_name['state_name'].'</option>';
				}       		
			}
			else
			{
				$stateJSON.= '<option value="">State not available</option>';
			}
			$cityJSON='';
			if($dataCity>0)
			{
				$cityJSON .='<option value="">Select city</option>';
				foreach($dataCity as $key => $city_name)
				{
					$cityJSON.="<option value='".$city_name['id']."'>".$city_name['city_name']."</option>";
				}
			}
			else
			{
				$cityJSON.="<option value=''>Select not available</option>";
			}

			if($userData){
				echo json_encode(array("status"=>"success","userData"=>$userData[0],"stateJSON"=>$stateJSON,"cityJSON"=>$cityJSON)); exit;
			}else{
				echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
			}
		}else{
			echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
		}
	}		
}



public function saveDriver()
{
	if(is_ajax_request())
	{
		if(is_user_logged_in())
		{
			$socialDoc='';
			$driverDoc='';
			$drugTestDoc='';
			$criminalBkDoc='';
			$motorVehicleDoc='';
			$clearanceDoc ='';
			$vehicleInspectionDoc='';
			$vehiclMaintDoc='';
			$vehicleInsuranceDoc='';
			$vehicleRegisDoc='';
			$dynamicPath='';
			$ownBusVehicleRegisDoc = '';
			$insurancePolicyDoc ='';
			$inspectionDoc ='';
			$taxidEinoDoc='';
			$ACED=$MVRED=$CBKED=$DTED=$DLED=$VRED=$VIED=$VehiclInsDt='';
			$postData = $this->input->post();
			if(!empty($postData['clearanceED'])){
				$ACED=date("Y-m-d",strtotime($postData['clearanceED']));
			}else{ $ACED = '0000-00-00'; }
			if(!empty($postData['motrVehRecED'])){
				$MVRED=date("Y-m-d",strtotime($postData['motrVehRecED']));
			}else{ $MVRED = '0000-00-00'; }
			if(!empty($postData['crimnlBGED'])){
				$CBKED = date("Y-m-d",strtotime($postData['crimnlBGED']));
			}else{ $CBKED='0000-00-00'; }

			if(!empty($postData['panelDrugTsED'])){
				$DTED = date("Y-m-d",strtotime($postData['panelDrugTsED']));
			}else{ $DTED = '0000-00-00'; }

			if(!empty($postData['drivrLicED'])){
				$DLED=date("Y-m-d",strtotime($postData['drivrLicED']));
			}else{ $DLED ='0000-00-00'; }

			if(!empty($postData['vehInspeED'])){
				$VehiclInsDt=date("Y-m-d",strtotime($postData['vehInspeED']));
			}else{ $VehiclInsDt='0000-00-00'; }

			if(!empty($postData['vehInsrnED'])){
				$VIED=date("Y-m-d",strtotime($postData['vehInsrnED']));
			}else{ $VIED='0000-00-00'; }

			if(!empty($postData['vehRegED']))
				{ $VRED = date("Y-m-d",strtotime($postData['vehRegED'])); }else{ $VRED='0000-00-00'; }

			$uploadFiles =array();
			$uploadFiles =$_FILES;
				/*echo $this->db->last_query();
				echo "<pre>";
				print_r($uploadFiles);
				echo "</pre>";
				print_r($postData);
				exit;*/
				if(!empty($uploadFiles))
				{
					foreach ($uploadFiles as $key => $value) {		
						if(empty($value['name'])){
							continue;
						}else{
							if(isset($_FILES[$key]["name"]))   
							{   
								$file_name = $_FILES[$key]["name"];
								$original_file_name = $file_name;
								$date =date("Y_m_d_H_s_i");
								$random = rand(1, 10000000000000000);
								$makeRandom = $random;
								$file_name_rename = $makeRandom;
								$explode = explode('.', $file_name);
								if(count($explode) >= 2) 
								{
									$new_file = $file_name_rename.$date.'.'.$explode[1];
									$config['upload_path'] = './uploads/Driver/doc';
									$config['allowed_types'] ="gif|jpg|jpeg|png|rar|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|exe";
									$config['file_name'] = $new_file;
									$this->load->library('upload',$config);
									$this->upload->initialize($config);
									if(!$this->upload->do_upload($key)) 
									{
										$error = $this->upload->display_errors();
										echo json_encode(array("status"=>"error","msg"=>"File format and extension do not match.")); exit;	
									}
									else
									{
										$userData1 = $this->Common_model->select("social_security_card_doc ,driver_license_doc,drug_test_results_doc,criminal_back_ground_doc,motor_vehicle_record_doc,`act_33_and_34_clearance_doc`,vehicle_inspections_doc,vehicle_maintnc_record_doc, vehicle_insurance_doc, vehicle_regi_doc,own_bus_vehicle_reg_doc, own_bus_insurance_pol_doc, own_bus_inspection_doc, own_bus_taxid_einno_doc",TB_USERS,array('id'=>$this->encrypt->decode($postData['user_key']),'user_type'=>5));
										if(!empty($postData['user_key']))
										{
											if(!empty($userData1[0])){
												if(strcmp($key,'socSecrtCd')==0)
													$socialDoc = 'uploads/Driver/doc/'.$new_file;	
												else
													$socialDoc = $userData1[0]['social_security_card_doc'];
												if(strcmp($key,'drivrLicDoc')==0)
													$driverDoc = 'uploads/Driver/doc/'.$new_file;
												else
													$driverDoc = $userData1[0]['driver_license_doc'];
												if(strcmp($key,'panelDrgTstResltDoc')==0)
													$drugTestDoc = 'uploads/Driver/doc/'.$new_file;
												else
													$drugTestDoc = $userData1[0]['drug_test_results_doc'];
												if(strcmp($key,'crimnlBGDoc')==0)
													$criminalBkDoc = 'uploads/Driver/doc/'.$new_file;
												else
													$criminalBkDoc = $userData1[0]['criminal_back_ground_doc'];
												if(strcmp($key,'MotrVehRecDoc')==0)
													$motorVehicleDoc = 'uploads/Driver/doc/'.$new_file;
												else
													$motorVehicleDoc = $userData1[0]['motor_vehicle_record_doc'];
												if(strcmp($key,'clearanceDoc')==0)
													$clearanceDoc = 'uploads/Driver/doc/'.$new_file;
												else
													$clearanceDoc =$userData1[0]['act_33_and_34_clearance_doc'];
												if(strcmp($key,'vehInspDoc')==0)
													$vehicleInspectionDoc = 'uploads/Driver/doc/'.$new_file;
												else
													$vehicleInspectionDoc = $userData1[0]['vehicle_inspections_doc'];
												if(strcmp($key,'vehMaintRecDoc')==0)
													$vehiclMaintDoc = 'uploads/Driver/doc/'.$new_file;
												else
													$vehiclMaintDoc = $userData1[0]['vehicle_maintnc_record_doc'];
												if(strcmp($key,'vehInsrnDoc')==0)
													$vehicleInsuranceDoc = 'uploads/Driver/doc/'.$new_file;
												else
													$vehicleInsuranceDoc = $userData1[0]['vehicle_insurance_doc'];
												if(strcmp($key,'vehRegDoc')==0)
													$vehicleRegisDoc = 'uploads/Driver/doc/'.$new_file;
												else
													$vehicleRegisDoc = $userData1[0]['vehicle_regi_doc'];
												if(strcmp($key,'own_vehicle_reg_doc')==0)
													$ownBusVehicleRegisDoc = 'uploads/Driver/doc/'.$new_file;
												else
													$ownBusVehicleRegisDoc = $userData1[0]['own_bus_vehicle_reg_doc'];
												if(strcmp($key,'insurance_policy_doc')==0)
													$insurancePolicyDoc = 'uploads/Driver/doc/'.$new_file;
												else
													$insurancePolicyDoc = $userData1[0]['own_bus_insurance_pol_doc'];
												if(strcmp($key,'inspection_doc')==0)
													$inspectionDoc = 'uploads/Driver/doc/'.$new_file;
												else
													$inspectionDoc = $userData1[0]['own_bus_inspection_doc'];
												if(strcmp($key,'tax_id_or_ein_no')==0)
													$taxidEinoDoc = 'uploads/Driver/doc/'.$new_file;
												else
													$taxidEinoDoc = $userData1[0]['own_bus_taxid_einno_doc'];
											}																	
										}
										else
										{
											if(strcmp($key,'socSecrtCd')==0)
												$socialDoc = 'uploads/Driver/doc/'.$new_file;	
											elseif(strcmp($key,'drivrLicDoc')==0)
												$driverDoc = 'uploads/Driver/doc/'.$new_file;
											elseif(strcmp($key,'panelDrgTstResltDoc')==0)
												$drugTestDoc = 'uploads/Driver/doc/'.$new_file;
											elseif(strcmp($key,'crimnlBGDoc')==0)
												$criminalBkDoc = 'uploads/Driver/doc/'.$new_file;
											elseif(strcmp($key,'MotrVehRecDoc')==0)
												$motorVehicleDoc = 'uploads/Driver/doc/'.$new_file;
											elseif(strcmp($key,'clearanceDoc')==0)
												$clearanceDoc = 'uploads/Driver/doc/'.$new_file;
											elseif(strcmp($key,'vehInspDoc')==0)
												$vehicleInspectionDoc = 'uploads/Driver/doc/'.$new_file;
											elseif(strcmp($key,'vehMaintRecDoc')==0)
												$vehiclMaintDoc = 'uploads/Driver/doc/'.$new_file;
											elseif(strcmp($key,'vehInsrnDoc')==0)
												$vehicleInsuranceDoc = 'uploads/Driver/doc/'.$new_file;
											elseif(strcmp($key,'vehRegDoc')==0)
												$vehicleRegisDoc = 'uploads/Driver/doc/'.$new_file;
											elseif(strcmp($key,'own_vehicle_reg_doc')==0)
												$ownBusVehicleRegisDoc = 'uploads/Driver/doc/'.$new_file;
											elseif(strcmp($key,'insurance_policy_doc')==0)
												$insurancePolicyDoc = 'uploads/Driver/doc/'.$new_file;
											elseif(strcmp($key,'inspection_doc')==0)
												$inspectionDoc = 'uploads/Driver/doc/'.$new_file;
											elseif(strcmp($key,'tax_id_or_ein_no')==0)
												$taxidEinoDoc = 'uploads/Driver/doc/'.$new_file;											
										}							
									}
								}
							}
						}
					}
				}
				else
				{
					if(!empty($postData['user_key'])){
						$userData1 = $this->Common_model->select("social_security_card_doc ,driver_license_doc,drug_test_results_doc,criminal_back_ground_doc,motor_vehicle_record_doc,`act_33_and_34_clearance_doc`,vehicle_inspections_doc,vehicle_maintnc_record_doc, vehicle_insurance_doc, vehicle_regi_doc,own_bus_vehicle_reg_doc, own_bus_insurance_pol_doc, own_bus_inspection_doc, own_bus_taxid_einno_doc",TB_USERS,array('id'=>$this->encrypt->decode($postData['user_key']),'user_type'=>5));
						/*echo "<pre>";
						print_r($userData1);
						echo "</pre>";
						exit;*/
						foreach($userData1 as $key => $value)
						{
							foreach ($value as $key1 => $docData) {	
								if(strcmp($key1, 'social_security_card_doc') == 0)
									$socialDoc=$docData;
								if(strcmp($key1, 'driver_license_doc') == 0)
									$driverDoc=$docData;
								if(strcmp($key1,'drug_test_results_doc')==0)							
									$drugTestDoc=$docData;
								if(strcmp($key1,'criminal_back_ground_doc')==0)							
									$criminalBkDoc=$docData;
								if(strcmp($key1,'motor_vehicle_record_doc')==0)							
									$motorVehicleDoc=$docData;
								if(strcmp($key1,'act_33_and_34_clearance_doc')==0)							
									$clearanceDoc=$docData;
								if(strcmp($key1,'vehicle_inspections_doc')==0)							
									$vehicleInspectionDoc=$docData;	
								if(strcmp($key1,'vehicle_maintnc_record_doc')==0)							
									$vehiclMaintDoc=$docData;	
								if(strcmp($key1,'vehicle_insurance_doc')==0)							
									$vehicleInsuranceDoc=$docData;
								if(strcmp($key1,'vehicle_regi_doc')==0)							
									$vehicleRegisDoc=$docData;	
								if(strcmp($key1,'own_bus_vehicle_reg_doc')==0)							
									$ownBusVehicleRegisDoc=$docData;	
								if(strcmp($key1,'own_bus_insurance_pol_doc')==0)							
									$insurancePolicyDoc=$docData;
								if(strcmp($key1,'own_bus_inspection_doc')==0)							
									$inspectionDoc=$docData;	
								if(strcmp($key1,'own_bus_taxid_einno_doc')==0)							
									$taxidEinoDoc=$docData;	
							}	
						}					
					}
					else{
						$socialDoc='';
						$driverDoc='';
						$drugTestDoc='';
						$criminalBkDoc='';
						$motorVehicleDoc='';
						$clearanceDoc ='';
						$vehicleInspectionDoc='';
						$vehiclMaintDoc='';
						$vehicleInsuranceDoc='';
						$vehicleRegisDoc='';
						$dynamicPath='';
						$ownBusVehicleRegisDoc = '';
						$insurancePolicyDoc ='';
						$inspectionDoc ='';
						$taxidEinoDoc='';
					}				
				}
				$data['userData'] = $this->Common_model->select("*",TB_ROLE);	
				$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);	
				$data['typeOfServices'] = $this->Common_model->select("type_title,type_id",TB_SERVICE_TYPE);
				$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
				$token = "";				
				//$this->load->helper("string");		
				$token=  random_string('alnum',20);	
				$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
				$password = substr( str_shuffle( $chars ), 0, 7 );
				if(isset($_FILES["pimage"]["name"]))   
				{   
					if(!$this->upload->do_upload('pimage'))
					{		 
						$error = $this->upload->display_errors();
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"For profile picture ".$error)); exit;	
					} 
					else
					{		 
						$data=$this->upload->data();
						$data = $this->resize($data);					        
						$data['picture'] = $data['new_image'];	
					}				
				}
				else if(isset($postData['oldimg']))   
				{    		            
					$data['picture'] =$postData['oldimg'];	
				}
				else
				{
					$data['picture'] = "images/default.jpg";	
				}					
				$insertArr = array(
					'user_token' => trim($token),
					'user_type' => 5,								
					'full_name'  => trim($postData['full_name']),
					'date_of_birth' => date("Y-m-d",strtotime($postData['birth'])),
					'email'      => trim($postData['email_address']),
					'password'   => trim(md5($password)),								
					'token'      => $token,
					'username'	 =>	trim($postData['username']),					 
					'created_at' => date('Y-m-d H:i:s'),
					'status'     => "active",
					'phone'      => trim($postData['phone']),
					'done'		 => 1,
					'country'    => $postData['country'],
					'state'		 => $postData['state'],
					'city'		 => $postData['city'],
					'county'	 => $postData['county'],
					'street'	 => trim($postData['address']),
					'zipcode'	 => trim($postData['zipcode']),						
					'insurance_id' => trim($postData['insurance_id']),
					'personal_doc_status' => trim($postData['pers_doc']),
					'vehicle_doc_status' => trim($postData['vehRegDropdown']),
					'emergency_contactname' => trim($postData['emer_contc_name']),
					'emergency_contactno' => $postData['emer_contc_number'],
					'signature'	=> $postData['signature'],
					'social_security_card_doc' => $socialDoc,
					'driver_license_doc'=>$driverDoc, 
					'driver_license_expire_dt'=> $DLED, 
					'drug_test_results_doc'=>$drugTestDoc, 
					'drug_test_results_expire_dt'=>$DTED, 
					'criminal_back_ground_doc'=>$criminalBkDoc,
					'criminal_back_ground_expire_dt'=>$CBKED,
					'motor_vehicle_record_doc'=>$motorVehicleDoc,
					'motor_vehicle_record_expire_dt'=>$MVRED,
					'act_33_and_34_clearance_doc'=>$clearanceDoc,
					'act_33_and_34_clearance_expire_dt'=>$ACED,
					'vehicle_inspections_doc'=>$vehicleInspectionDoc,
					'vehicle_inspections_expire_dt'=>$VehiclInsDt,
					'vehicle_maintnc_record_doc'=>$vehiclMaintDoc,
						//'vehicle_maintnc_record_expire_dt'=>$postData['eme_number'],
					'vehicle_insurance_doc'=>$vehicleInsuranceDoc,
					'vehicle_insurance_expire_dt'=>$VIED,
					'vehicle_regi_doc'=>$vehicleRegisDoc,
					'vehicle_regi_expire_dt'=>$VRED,						
					'own_business_status' =>trim($postData['own_vehicle_dd']),
					'own_bus_vehicle_reg_doc' =>$ownBusVehicleRegisDoc,
					'own_bus_insurance_pol_doc' =>$insurancePolicyDoc,
					'own_bus_inspection_doc' =>$inspectionDoc,
					'own_bus_taxid_einno_doc'=>$taxidEinoDoc,
					'own_bus_vehicle_detiail'=>trim($postData['own_buss_vehicle_details']),
					'own_bus_services'=>trim($postData['type_of_sevices']),
					'company_vehicle_status'=>trim($postData['company_vehicle_dropdown']),
					'comp_veh_transp_comp'=> trim($postData['transport_comp']),
					'comp_veh_vehicle_details'=>trim($postData['vehicle_details']),
					'comp_veh_services'=>trim($postData['com_veh_type_of_sevices']),
					'picture'=> $data['picture'],
					);
						/*echo "<pre>";
						print_r($insertArr);
						echo "</pre>";
						exit;*/
						if($postData["user_key"]){					
							$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
							if($insertId){
								echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Driver user has been updated successfully.")); exit;	
							}else{
								echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
							}
						}else{		    		
					// Record insert in user table			
							$insertId = $this->common->insert(TB_USERS,$insertArr);	
							if($insertId){
								$to_email = $postData["email_address"];
								$hostname = $this->config->item('hostname');  
								$config['mailtype'] ='html';
								$config['charset'] ='iso-8859-1';
								$this->email->initialize($config);
								$from  = EMAIL_FROM; 
								$this->messageBody  = email_header();
								$this->messageBody  .= '<tr>
								<td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
									<p>Dear '.$postData["full_name"].',</p>
									<p>
										You are registered successfully from admin on Transcura.Your login credencial are:<br>
										<p>Your email is- <b>'.trim($postData["email_address"]).'</b></p> Or <br/>
										<p>Your username is- <b>'.trim($postData["username"]).'</b></p>
										<p>Your password is- <b>'.trim($password).'</b></p>
										<p>Your usertype is- <b>Driver</b></p>
									</p>                                   
								</p>
							</td>
						</tr>
						<tr>';                      
                        $this->messageBody  .= email_footer();        //print_r( $this->messageBody);die;
                        $this->email->from($from, $from);
                        $this->email->to($to_email);
                        $this->email->subject('Welcome to Transcura');
                        $this->email->message($this->messageBody);
                        $this->email->send();                   
                        echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Driver user has been added successfully.")); exit;	
                    }else{
                    	echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
                    }
                }
            }
            else
            {
            	echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
            }
        }	
    }


public function resize($image_data) {
	$date = date("Y-m-d-H-i-s");
        $img = substr($image_data['full_path'], 51);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $image_data['full_path'];
        $config['new_image'] ='./uploads/Driver/thumb/thumb_'.$image_data['raw_name'].$date.$image_data['file_ext'];
        $config['allowed_types'] = 'png|mpg|mpeg|wmv|jpg|JPEG|jpeg|PNG';       
        $config['width'] = 250;
        $config['height'] = 250;
        $this->image_lib->initialize($config);
        $src = $config['new_image'];
        $data['new_image'] = substr($src, 2);
        $data['img_src'] = base_url().$data['new_image'];
        if(!$this->image_lib->resize())
		{
			$error = $this->image_lib->display_errors();
			echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;		    
		}
		else
		{
        	return $data;		      
		}
    }

    // public function resize($image_data) {
    // 	$img = substr($image_data['full_path'], 51);
    // 	$config['image_library'] = 'gd2';
    // 	$config['source_image'] = $image_data['full_path'];        
    // 	$config['new_image'] ='./uploads/Driver/thumb/thumb_'.$image_data['raw_name'].$image_data['file_ext'];
    // 	$config['allowed_types'] = 'png|mpg|mpeg|wmv|jpg|JPEG|jpeg|PNG|doc|docx|DOCX|gif';       
    // 	$config['width'] = 250;
    // 	$config['height'] = 250;
    // 	$this->image_lib->initialize($config);
    // 	$src = $config['new_image'];
    // 	$data['new_image'] = substr($src, 2);
    // 	$data['img_src'] = base_url().$data['new_image'];
    // 	if(!$this->image_lib->resize())
    // 	{
    // 		$error = $this->image_lib->display_errors();
    // 		echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;		    
    // 	}
    // 	else
    // 	{
    // 		return $data;		      
    // 	}
    // }

    public function change_status() {
    	if(is_ajax_request())
    	{
    		if(is_user_logged_in()){					
    			$postData = $this->input->post();
    			$userData = $this->common->selectQuery("status",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
    			if($userData[0]["status"] == "active"){
    				$status_update = "inactive";
    			}
    			else if($userData[0]["status"] == "inactive"){
    				$status_update = "active";
    			}
    			$insertArr = array('status'=>$status_update,'updated_at'=>date("Y-m-d h:i:s", time()));
    			if($postData["key"]){
    				$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])),$insertArr);
    				$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
    				if($userData[0]["status"] == "active"){
    					$message = file_get_contents("media/emails/accountactivation.html");
    					$message = preg_replace('/{user}/', $userData[0]['full_name'], $message);
    					$message = preg_replace('/{user_id}/', $userData[0]['id'], $message);
    					$this->load->library('email');  
    					$config['charset'] = 'iso-8859-1';
    					$config['wordwrap'] = TRUE;
    					$config['mailtype'] = 'html';
    					$this->email->initialize($config);  
    					$this->email->from(EMAIL_FROM, 'Admin'); 
    					$this->email->to($userData[0]['email']);
    					$this->email->subject('Driver Activation mail'); 
    					$this->email->message($message); 
    					$this->email->send();
    				}
    				if($insertId){
    					echo json_encode(array("status"=>"success","userData"=>$userData[0],"msg"=>"Driver user has been updated successfully.")); exit;	
    				}else{
    					echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
    				}
    			}
    		}
    	}
    }

    // ----------change approval status-----------
    public function change_appoval()
    {
    	if(is_ajax_request())
    	{
    		if(is_user_logged_in())
    		{					
    			$postData = $this->input->post();
    			$userData = $this->common->selectQuery("is_approve",TB_USERS,array('id'=>$this->encrypt->decode($postData['driver_id'])));
    			if($userData[0]["is_approve"] == "1"){
    				$status_update = "0";
    			}
    			else if($userData[0]["is_approve"] == "0"){
    				$status_update = "1";
    			}
    			$updateArr = array('is_approve'=>$status_update,'updated_at'=>date("Y-m-d h:i:s", time()));
    			
    			$updateData = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['driver_id'])),$updateArr);
    			if($updateData)
    			{
    				echo json_encode(array("status"=>"success","msg"=>"Driver user has been updated successfully.")); exit;	
    			}else
    			{
    				echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
    			}
    		}
    	}
    }

    // --------- get driver documents -----------
    public function getDriverDoc()
    {
    	if(is_ajax_request())
    	{
    		if(is_user_logged_in())
    		{
    			$postData = $this->input->post();
    			$getData = getDriverDochtml($this->encrypt->decode($postData['driver_id']));
    			echo json_encode(array("status"=>"success","response"=>$getData)); exit;
    		}
    		else
    		{
    			echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
    		}
    	}
    }
}


