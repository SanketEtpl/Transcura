<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	
		$this->load->helper(array('form', 'url'));
		$this->load->library('image_lib');
		       $config['allowed_types'] =   "png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG";  
               $config['max_size']      =   "5000"; 
               $config['max_width']     =   "1000"; 
               $config['max_height']    =   "900"; 
              
              $config['upload_path']   =  "./images/" ;   
               
               	// $this->load->helper("email_template");
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$this->load->view('setting/listSetting');
			$this->load->model('common_model');
		}else{
			redirect("login");
			exit;
		}

	}

	public function listSetting(){
	//print_r("ghjfgh");die;
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();
			


				$arrayColumn = array("office_email"=>"office_email","company_address"=>"company_address","mobile"=>"mobile","company_phone"=>"company_phone","facebook"=>"facebook" ,"twitter"=>"twitter" ,"google"=>"google" ,"twitter"=>"twitter" ,"pintrest"=>"pintrest"  ,"logo"=>"logo" );
			$result = $this->common->selectQuery("*",TB_SETTINGS);   
              //echo $this->db->last_query();die;
				$rows = '';
				if(!empty($result[0]))
				{
					$i=1;
					
						$userId = $this->encrypt->encode($result[0]['id']);
							
		


             $rows .= '<div class="datapro" id="idpro">
                      <div class="col-md-6">
                        <div class="form-group showgroup">
                          <label>Address<i class="fa fa-map-marker" aria-hidden="true"></i></label>
                          <div class="showdiv">'.$result[0]['company_address'].'</div>
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group showgroup">
                          <label>Email <i class="fa fa-envelope-o" aria-hidden="true"></i></label>
                          <div class="showdiv">'.$result[0]['office_email'].'</div>
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group showgroup">
                          <label>Mobile <i class="fa fa-mobile" aria-hidden="true"></i></label>
                          <div class="showdiv">'.$result[0]['mobile'].'</div>
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group showgroup">
                          <label>Phone <i class="fa fa-phone" aria-hidden="true"></i></label>
                           <div class="showdiv">'.$result[0]['company_phone'].'</div>
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group showgroup">
                          <label>Facebook<i class="fa fa-facebook" aria-hidden="true"></i></label>
                          <div class="showdiv">'.$result[0]['facebook'].'</div>
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group showgroup">
                          <label>Twitter <i class="fa fa-twitter" aria-hidden="true"></i></label>
                           <div class="showdiv">'.$result[0]['twitter'].'</div>
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group showgroup">
                          <label>Google <i class="fa fa-google-plus" aria-hidden="true"></i></label>
                        <div class="showdiv">'.$result[0]['google'].'</div>
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group showgroup">
                          <label>Pinterest <i class="fa fa-pinterest-p" aria-hidden="true"></i></label>
                          <div class="showdiv">'.$result[0]['pintrest'].'</div>
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group showgroup">
                          <label>Logo <i class="fa fa-logo-p" aria-hidden="true"></i></label>
                          <div class="showdiv"><img src="'.base_url().$result[0]['logo'].'"></div>
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group showgroup">
                          <label>Multiple Locations <i class="fa fa-logo-p" aria-hidden="true"></i></label>
                          <div class="showdiv">'.$result[0]['multiple_location'].'</div>
                      </div>
                    </div>
                    <div class="col-md-12">
                     <a data-id="'.$i.'" data-row-id="'.$userId.'" title="Edit" value="Edit" class="btn btn-info tiny" onclick="getSetting(this)" href="javascript:void(0)">
							<i class="fa fa-fw fa-check-square-o"></i>
								</a>
                    </div>
                    </div>
                  </div>  
                </div>';








					
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getSetting(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("*",TB_SETTINGS);         

				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0] )); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function saveSetting(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){


			$postData = $this->input->post();


               $config['allowed_types'] =   "png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG";  
               $config['max_size']      =   "5000"; 
               $config['max_width']     =   "1000"; 
               $config['max_height']    =   "900"; 
               $config['upload_path']   =  "./images/" ;   

			$this->load->library('upload', $config);
     	    if(isset($_FILES["pimage"]["name"]))   
		    {   
				if(!$this->upload->do_upload('pimage'))
				{		 
				$error = $this->upload->display_errors();
				echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
				} 
				else
				{		 
				$data=$this->upload->data();							        
				$data['picture'] = "images/".$data['file_name'];	
				}				
    		}

    		else if(isset($postData['oldimg']))   
		    {   
		           
		          $data['picture'] =$postData['oldimg'];	
		        				
    		}
   	 	
         

     				$postData = $this->input->post();
					
					$insertArr = array('company_address'=>$postData["company_address"],					
					'office_email'=>$postData["office_email"],
					'company_phone' =>$postData['company_phone'],
					'facebook'=>$postData["facebook"],
					'twitter'=>$postData["twitter"],
					'google' =>$postData['google'],
					'pintrest' =>$postData['pintrest'],
					 'mobile'=>$postData["mobile"],									
					'logo'=> $data['picture'],
					'multiple_location' => $postData['multipleLocation']		
					);
				if($postData["user_key"]){
					$insertId = $this->common->update(TB_SETTINGS,array('id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
		
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Setting has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
}




}
