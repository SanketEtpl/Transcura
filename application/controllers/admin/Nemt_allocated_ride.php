<?php 
/* Author : Ram K 
   Page : Nemt_allocated_ride.php
   Description:  NEMT allocated ride
   Date: 09 Feb 2018
*/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nemt_allocated_ride extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');		
	}

	public function index()
	{
 		if(is_user_logged_in())
		{
			$this->load->view('dynamic_records/listOfAllocatedRideDetails');			
		}else{
			redirect("login");
			exit;
		}
	}

	public function listOfAllocatedRideDetails(){	
		if(is_ajax_request())
		{	
			if(is_user_logged_in()){			
				$postData = $this->input->post();				
				$arrayColumn = array("full_name"=>"full_name","free_ride"=>"free_ride");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where=array(TB_USERS.'.user_type'=>2);
                $join=array();
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'full_name',"'id'",TB_USERS,'free_ride,id,full_name','listOfAllocatedRideDetails', $where,$join,"","user_id");
            	$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$user['id']));
						$userId = $this->encrypt->encode($user['id']);
						
						$rows .= '<tr id="'.$userId.'">
							<td class="text-left">'.$user['full_name'].'</td>
							<td class="text-left">'.$user['free_ride'].'</td>						   
							
							 <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="getDetails(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa fa-edit"></i>
									</a>							                               
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}


	public function getAllocatedDetails(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("full_name,free_ride",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));				

				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}


	public function saveAllocatedRide(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
			$searcharray = $this->input->post();
			parse_str($searcharray['formData'], $postData);

				if($postData["user_key"]){

					$insertArr = array(
						'full_name'=>trim($postData["full_name"]),
						'free_ride'=>trim($postData["free_ride"])
					);
					$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);

					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"NEMT user ride has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
	    		}
			}
		}
	}


}