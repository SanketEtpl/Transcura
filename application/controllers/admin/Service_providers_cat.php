<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service_providers_cat extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
 		if(is_user_logged_in())
		{
			
//echo "hgjghj" ;die
			$this->load->view('service_providers/listServiceproviders_cat');
			$this->load->model('common_model');
			
		}else{
			redirect("login");
			exit;
		}

	}

	public function listServiceproviders_cat(){
	//print_r("ghjfgh");die;
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();
				//print_r($postData) ;die;
				$arrayColumn = array("cat_id"=>"cat_id","cat_title"=>"cat_title");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where=array("isDeleted"=>'0');
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'cat_title','cat_id',TB_SERVICE_PROVIDERS_CAT,'*','listServiceproviders_cat',$where);
               //print_r($result) ;die;
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['cat_id']);
							
						//print_r($result['rows']);
						$rows .= '<tr id="'.$userId.'">
	                            <td class="text-left">'.$user['cat_title'].'</td>
	                            
	                           <td class="text-left">
								<div class="btn-group btn-toggle" id="accept"> 
									<a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="changeStatus(this)" href="javascript:void(0)">
									'.$user['status'].'	
									</a>
								  </div>
	                           </td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getServiceprovider_cat(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
										  <a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="deleteServiceprovider_cat(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>	                              
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getServiceprovider_cat(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("*",TB_SERVICE_PROVIDERS_CAT,array('cat_id'=>$this->encrypt->decode($postData['key'])));
				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function saveServiceprovider_cat(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				
                   //print_r($_REQUEST);die;
					$postData = $this->input->post();
					$insertArr = array('cat_title'=>$postData["category_title"],
					
					'updated_at'=>date("Y-m-d h:i:s", time()),	
					);
				if($postData["user_key"]){
					$insertId = $this->common->update(TB_SERVICE_PROVIDERS_CAT,array('cat_id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
				//echo $this->db->last_query();die;
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"User has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{


					$insertArr = array('cat_title'=>$postData["category_title"],
					'created_at'=>date("Y-m-d h:i:s", time()),
					'updated_at'=>date("Y-m-d h:i:s", time()),	
					);

					$insertId = $this->common->insert(TB_SERVICE_PROVIDERS_CAT,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"User has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

	public function saveRegisterUser(){

			$postData = $this->input->post();


			$token = "";
			//Combination of character, number and special character...
			$combinationString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789*#&$^";

			$this->load->helper('string');
            $token=  random_string('alnum',20);

			$insertArr = array('first_name'=>$postData["first_name"],'username' => $postData['username'],'user_type' => $postData['user_type'],'password' => $postData['Password'],'phone' => $postData['contact_number'],'email' => $postData['email_address'],'address'=>$postData['address'],'state'=>$postData['state'],'country'=>$postData['country'] ,'zipcode'=>$postData['zipcode'],'user_token'=>$token);
			$insertId = $this->common->insert(TB_SERVICE_PROVIDERS_CAT,$insertArr); 
			//echo $this->db->last_query();die;
			if($insertId){
			echo json_encode(array("status"=>"success","action"=>"add","msg"=>"User has been added successfully.")); exit;	
			}else{
			echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
			}
	
	}
  public function change_status() {

  	if(is_ajax_request())
		{
			if(is_user_logged_in()){
					//print_r($_REQUEST);die;
					$postData = $this->input->post();
					$userData = $this->common->selectQuery("status",TB_SERVICE_PROVIDERS_CAT,array('cat_id'=>$this->encrypt->decode($postData['key'])));
					//print_r($userData);die;
				if($userData[0]["status"] == "published"){
						$status_update = "unpublished";
					}
					else if($userData[0]["status"] == "unpublished"){
						$status_update = "published";
					}
				$insertArr = array('status'=>$status_update,'updated_at'=>date("Y-m-d h:i:s", time()));

				if($postData["key"]){
					$insertId = $this->common->update(TB_SERVICE_PROVIDERS_CAT,array('cat_id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					//echo $this->db->last_query();die;
					$userData = $this->common->selectQuery("*",TB_SERVICE_PROVIDERS_CAT,array('cat_id'=>$this->encrypt->decode($postData['key'])));
					if($insertId){
						echo json_encode(array("status"=>"success","userData"=>$userData[0],"msg"=>"Category has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}


	public function deleteServiceprovider_cat() {

  	if(is_ajax_request())
		{
			if(is_user_logged_in()){
				//print_r($_REQUEST);die;
					$postData = $this->input->post();
					$userData = $this->common->selectQuery("isDeleted",TB_SERVICE_PROVIDERS_CAT,array('cat_id'=>$this->encrypt->decode($postData['key'])));
					//print_r($userData);die;
				if($userData[0]["isDeleted"] == "0"){
						$status_update = "1";
					}
					else if($userData[0]["isDeleted"] == "1"){
						$status_update = "0";
					}
				$insertArr = array('isDeleted'=>$status_update,'updated_at'=>date("Y-m-d h:i:s", time())	);

				if($postData["key"]){
					$insertId = $this->common->update(TB_SERVICE_PROVIDERS_CAT,array('cat_id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					//echo $this->db->last_query();die;
					$userData = $this->common->selectQuery("*",TB_SERVICE_PROVIDERS_CAT,array('cat_id'=>$this->encrypt->decode($postData['key'])));
					if($insertId){
						echo json_encode(array("status"=>"success","userData"=>$userData[0],"msg"=>"Category has been deleted successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

}
