<?php if(!defined("BASEPATH")) exit('No direct script access allowed');

class Complaint_type extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
 		if(is_user_logged_in())
		{			
			$this->load->view('dynamic_records/listOfComplaintType');
			$this->load->model('common_model');			
		}else{
			redirect("login");
			exit;
		}
	}

	public function listOfComplaintType(){	
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();				
				$arrayColumn = array("id"=>"id","ct_name"=>"ct_name");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where=array();
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'id','ct_name',TB_COMPLAINTS_TYPE,'*','listOfComplaintType',$where);               
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['id']);
						$rows .= '<tr id="'.$userId.'">
								<td class="text-left">'.$user['id'].'</td>
								<td class="text-left">'.$user['ct_name'].'</td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="get_complaint_type(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
										  <a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="delete_complaint_type(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>	                              
	                            </td>
	                        </tr>';	                        
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function get_complaint_type(){

		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("*",TB_COMPLAINTS_TYPE,array('id'=>$this->encrypt->decode($postData['key'])));
				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}		
	}

	public function save_complaint_type(){

		if(is_ajax_request())
		{
			if(is_user_logged_in()){				
				$postData = $this->input->post();
				 if($postData["user_key"]) {
					$updateArr = array('ct_name'=>$postData["complaint_title"]);
					$updateId = $this->common->update(TB_COMPLAINTS_TYPE,array('id'=>$this->encrypt->decode($postData['user_key'])),$updateArr);
					if($updateId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Complaint title has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					$insertArr = array('ct_name'=>$postData["complaint_title"]);
					$insertId = $this->common->insert(TB_COMPLAINTS_TYPE,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Complaint title has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}  

	public function delete_complaint_type() {
	if(is_ajax_request())
	{
		if(is_user_logged_in()){
				$postData = $this->input->post();				
				$deleteId = $this->common->delete(TB_COMPLAINTS_TYPE,array('id'=>$this->encrypt->decode($postData['key'])));
				if($deleteId){
					echo json_encode(array("status"=>"success","msg"=>"Complaint title has been deleted successfully.")); exit;	
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
				}
			}
		}
	}
}
