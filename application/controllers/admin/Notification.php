<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
 		if(is_user_logged_in())
		{
			$data['driverData'] = $this->Common_model->select("id,full_name",TB_USERS,array('user_type'=>5));
			$this->load->view('notification/adminNotification',$data);
			$this->load->model('common_model');			
		}else{
			redirect("login");
			exit;
		}
	}

	public function list_of_notification(){	
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();				
				$arrayColumn = array("id"=>"id","n_title"=>"n_title","n_full_description"=>"n_full_description");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where=array();
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'id','n_title',TB_NOTIFICATION,'*','list_of_notification',$where);               
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['id']);
						$rows .= '<tr id="'.$userId.'">
								<td class="text-left">'.$user['id'].'</td>
								<td class="text-left">'.$user['n_title'].'</td>
	                            <td class="text-left">'.$user['n_full_description'].'</td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="get_notification(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
										  <a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="delete_notification(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>	                              
	                            </td>
	                        </tr>';	                        
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function get_notification(){

		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData['driverData'] = $this->Common_model->select("id,full_name",TB_USERS,array('user_type'=>5));
				$userData = $this->common->selectQuery("*",TB_NOTIFICATION,array('id'=>$this->encrypt->decode($postData['key'])));
				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}		
	}

	public function save_notification(){

		if(is_ajax_request())
		{
			if(is_user_logged_in()){				
				$postData = $this->input->post();
				if($postData['user'] == "allDrivers") {
					$userData = $this->Common_model->select("id,full_name",TB_USERS,array('user_type'=>5));
					if(!empty($userData)){
					foreach ($userData as $key => $value) {
						$insertAllDriversArr = array('n_title'=>$postData["notification_title"],
							'n_full_description'=>$postData["notification_description"],
							'notification_flag'=>2,
							'user_id'=>$value['id'],
							'read_or_unread'=>1,
							'created_on'=>date("Y-m-d h:i:s", time()),						
						);
						$insertId = $this->common->insert(TB_NOTIFICATION,$insertAllDriversArr);
						$status = $insertId; 
						}
						if($status){
								echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Notification has been sent all drivers successfully.")); exit;	
							}else{
							echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
						}
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Drivers does not exists.")); exit;	
					}						
					} else if($postData["user_key"]){
					$updateArr = array('n_title'=>$postData["notification_title"],					
						'n_full_description'=>$postData["notification_description"],
						'user_id'=>$postData['user'],
						'updated_on'=>date("Y-m-d h:i:s", time()),	
					);
					$updateId = $this->common->update(TB_NOTIFICATION,array('id'=>$this->encrypt->decode($postData['user_key'])),$updateArr);
					if($updateId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Notification has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					$insertArr = array('n_title'=>$postData["notification_title"],
						'n_full_description'=>$postData["notification_description"],
						'notification_flag'=>2,
						'user_id'=>$postData['user'],
						'read_or_unread'=>1,
						'created_on'=>date("Y-m-d h:i:s", time()),						
					);
					$insertId = $this->common->insert(TB_NOTIFICATION,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Notification has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}  

	public function delete_notification() {
	if(is_ajax_request())
	{
		if(is_user_logged_in()){
				$postData = $this->input->post();				
				$deleteId = $this->common->delete(TB_NOTIFICATION,array('id'=>$this->encrypt->decode($postData['key'])));
				if($deleteId){
					echo json_encode(array("status"=>"success","msg"=>"Notification has been deleted successfully.")); exit;	
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
				}
			}
		}
	}
}
