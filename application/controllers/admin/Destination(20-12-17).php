<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class Destination extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{		
		if(is_user_logged_in())
		{
			$this->load->view('dynamic_records/listOfDestination');
		}
		else
		{
			redirect("login");
			exit;
		}
	}

	public function listOfDestination(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();				
				$arrayColumn = array("id"=>"id","destination"=>"destination");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");				
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'id','destination'
					,TB_DESTINATION,'*','listDestination');
				$rows = '';				
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $data) {
						$service_id = $this->encrypt->encode($data['id']);
						$rows .= '<tr id="'.$service_id.'">
							
	                            <td class="text-left">'.$data['destination'].'</td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$service_id.'" class="" onclick="getDestination(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
									</a>
	                                <a data-id="'.$i.'" data-row-id="'.$service_id.'" class="" onclick="deleteDestination(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);				
			}
			else
			{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getDestination(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){				
				$postData = $this->input->post();
				$pageData = $this->common->selectQuery("*",TB_DESTINATION,array('id'=>$this->encrypt->decode($postData['key'])));
				if($pageData){
					echo json_encode(array("status"=>"success","pageData"=>$pageData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong, Please try again...!!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function saveDestination(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$insertArr = array(
								'destination'=>$postData["destination"],								
								);
				if($postData["page_key"]){
					$insertId = $this->common->update(TB_DESTINATION,array('id'=>$this->encrypt->decode($postData['page_key'])),$insertArr);
					
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Destination has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					//$insertArr["created_at"] = $dateTime;
					$insertId = $this->common->insert(TB_DESTINATION,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Destination has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"add","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
    }


   public function deleteDestination(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();					
				if($postData["key"]){					
					$deleteId = $this->common->delete(TB_DESTINATION,array('id'=>$this->encrypt->decode($postData['key'])));
					if($deleteId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Destination has been deleted successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
    }
}