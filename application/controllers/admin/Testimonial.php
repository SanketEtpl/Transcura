<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Testimonial extends CI_Controller {

	function __construct()
	{
		parent::__construct();		
	}

	public function index()
	{		
		if(is_user_logged_in())
		{
			$this->load->view('dynamic_records/testimonial');
		}else{
			redirect("login");
			exit;
		}
	}

	public function listOfTestimonial()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{								
				$postData = $this->input->post();
				$arrayColumn = array("tm_id"=>"tm_id","tm_name"=>"tm_name" ,"tm_description"=>"tm_description","tm_image"=>"tm_image","tm_stars"=>"tm_stars");
				$arrayStatus["is_active"] = array();				
				$arrayColumnOrder = array("ASC","asc","DESC","desc");				
				$where=array("");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'tm_id','tm_name',TB_TESTIMONIAL,'*','listOfTestimonialData',$where);	          
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $value) 
					{
						$testimonial_id = $this->encrypt->encode($value['tm_id']);
						$string = $value['tm_description'];
											if (strlen($string) > 100) {													
											$stringCut = substr($string, 0, 100);
											$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
											}	
$imgData ='';
											if(empty($value['tm_image']))
											{
												$imgData ='uploads/NEMT/thumb/img-dummy.jpg';
											}	
											else{
												$imgData=$value['tm_image'];
											}$imgData ='';
											if(empty($value['tm_image']))
											{
												$imgData ='uploads/no-img.jpg';
											}	
											else{
												$imgData=$value['tm_image'];
											}					
						$rows .= 
								'<tr id="'.$testimonial_id.'">
								<td class="text-left">'.$value['tm_id'].'</td>
	                            <td class="text-left">'.$value['tm_name'].'</td>
	                            <td class="text-left">'.$string.'</td>
	                            <td class="text-left"><img src='.base_url().$imgData.' width="75" height="75"></td>	                            
	                                <td class="text-left">
		                            	<a data-id="'.$i.'" data-row-id="'.$testimonial_id.'" class="" onclick="getTestimonial(this)" href="javascript:void(0)">
											<i class="fa fa-fw fa-check-square-o"></i>
										</a>
		                                <a data-id="'.$i.'" data-row-id="'.$testimonial_id.'" class="" onclick="deleteTestimonial(this)" href="javascript:void(0)">
											<i class="fa fa-fw fa-close"></i>
										</a>
	                            	</td>
	                        	</tr>';
	                }
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getTestimonial()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();				
				$pageData = $this->common->selectQuery("*",TB_TESTIMONIAL,array('tm_id'=>$this->encrypt->decode($postData['key'])));
				
				if($pageData)
				{
					echo json_encode(array("status"=>"success","pageData"=>$pageData[0])); exit;
				}
				else
				{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong, Please try again...!!!")); exit;
				}
			}
			else
			{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}		
	}

	public function save_testimonial()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();	
				$pageData = $this->common->selectQuery('*',TB_TESTIMONIAL,array('tm_id'=>$this->encrypt->decode($postData['page_key'])));
				
				if(isset($_FILES["tm_image"]["name"]))   
		  		{   
			        $file_name = $_FILES["tm_image"]["name"];
			        $original_file_name = $file_name;
			        $random = rand(1, 10000000000000000);			        
			        $file_name_rename = $random;
				$date = date("Y_m_d_H_i_s");
			        $explode = explode('.', $file_name);
			        if(count($explode) >= 2) 
			        {
			            $new_file = $file_name_rename.$date.'.'.$explode[1];			            
			            $config['upload_path'] = "./assets/img";
			            $config['allowed_types'] ="png|jpeg|jpg";
			            $config['file_name'] = $new_file;
			            $config['max_size'] = '307210';
			            $config['max_width'] = '300000';
			            $config['max_height'] = '300000';
			            $config['overwrite'] = TRUE;
			            $this->load->library('upload',$config);
			            $this->upload->initialize($config);
						if(!$this->upload->do_upload("tm_image"))
						{
							$error = $this->upload->display_errors();
							echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
						}
						else
						{
							$testImage = 'assets/img/'.$new_file;   
						}
		            }
				}
				else if(isset($pageData[0]['tm_image']))   
			    {          	 
			    	$testImage =$pageData[0]['tm_image'];		        				
				}
				else
		        {
			   		$testImage ="";	
		        }	

		        /*if(isset($_FILES["tm_stars"]["name"]))   
		  		{   
			        $file_name = $_FILES["tm_stars"]["name"];
			        $original_file_name = $file_name;
			        $random = rand(1, 10000000000000000);			        
			        $file_name_rename = $random;
			        $explode = explode('.', $file_name);
			        if(count($explode) >= 2) 
			        {
			            $new_file = $file_name_rename.'.'.$explode[1];			            
			            $config['upload_path'] = "./assets/img";
			            $config['allowed_types'] ="png|jpeg|jpg";
			            $config['file_name'] = $new_file;
			            $config['max_size'] = '307210';
			            $config['max_width'] = '300000';
			            $config['max_height'] = '300000';
			            $config['overwrite'] = TRUE;
			            $this->load->library('upload',$config);
			            $this->upload->initialize($config);
						if(!$this->upload->do_upload("tm_stars"))
						{
							$error = $this->upload->display_errors();
							echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
						}
						else
						{
							$testStars = 'assets/img/'.$new_file;   
						}
		            }
				}
				else if(isset($pageData[0]['tm_stars']))   
			    {          	 
			    	$testStars =$pageData[0]['tm_stars'];		        				
				}
				else
		        {
			   		$testStars ="";	
		        }	*/
				
		       
				$insertArr = array(
								'tm_name' => $postData["tm_name"],
								"tm_description" => $postData["tm_description"],
								"tm_image" => $testImage
								//"tm_stars" => $testStars								
								);				
				if($postData["page_key"])
				{
					$insertId = $this->common->update(TB_TESTIMONIAL,array('tm_id'=>$this->encrypt->decode($postData['page_key'])),$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Testimonial has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{					
					$insertId = $this->common->insert(TB_TESTIMONIAL,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Testimonial has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"add","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
    }

   public function deleteTestimonial()
   {
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				if($postData["key"])
				{	
					$deleteImage = $this->common->selectQuery("tm_id,tm_image",TB_TESTIMONIAL,array("tm_id"=>$this->encrypt->decode($postData['key'])));
					if(!empty($deleteImage[0]))
					unlink($deleteImage[0]['tm_image']);
					$insertId = $this->common->delete(TB_TESTIMONIAL,array('tm_id'=>$this->encrypt->decode($postData['key'])));
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Testimonial has been deleted successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
				else
				{
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
				}
			}
		}
    }
}
