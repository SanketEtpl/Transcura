<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$this->load->view('users/listUsers');
			$this->load->model('common_model');
		}else{
			redirect("login");
			exit;
		}

	}

	public function listUsers(){
		//print_r("ghjfgh");die;
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();
				//print_r($postData) ;die;
				$arrayColumn = array("id"=>"id","fname"=>"first_name","lname"=>"last_name","email"=>"email","phone"=>"phone" ,"gender"=>"gender" );
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");

				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'fname','id',TB_USERS,'*','listUsers');
               //print_r($result) ;die;
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['id']);
							
						//print_r($result['rows']);
						$rows .= '<tr id="'.$userId.'">
	                            <td class="text-left">'.$user['first_name'].'</td>
	                            <td class="text-left">'.$user['last_name'].'</td>
	                            <td class="text-left">'.$user['email'].'</td>
								<td class="text-left">'.$user['phone'].'</td>	                           
								<td class="text-left">'.$user['gender'].'</td>
								
	                           <td class="text-left">
								<div class="btn-group btn-toggle" id="accept"> 
									<a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="changeStatus(this)" href="javascript:void(0)">
									'.$user['status'].'	
									</a>
								  </div>
	                           </td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getUser(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
									</a>
	                              
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getUser(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function saveUser(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				
               //  print_r($_REQUEST);die;
					$postData = $this->input->post();
					 $birth = date("Y-m-d", strtotime($postData["birth"])); 
					$insertArr = array('first_name'=>$postData["first_name"],
					'last_name' => $postData['last_name'],
					'email'=>$postData["email_address"],
					'address' => $postData['address'],
					'zipcode'=>$postData["zipcode"],
					'phone'=>$postData["phone"],
					'gender'=>$postData["gender"],
					'updated_at'=>date("Y-m-d h:i:s", time()),	
					'date_of_birth'=>$birth					

					);
				if($postData["user_key"]){
					$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
				//echo $this->db->last_query();die;
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"User has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					$insertId = $this->common->insert(TB_USERS,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"User has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

	public function saveRegisterUser(){

			$postData = $this->input->post();


			$token = "";
			//Combination of character, number and special character...
			$combinationString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789*#&$^";

			$this->load->helper('string');
            $token=  random_string('alnum',20);

			$insertArr = array('first_name'=>$postData["first_name"],'username' => $postData['username'],'user_type' => $postData['user_type'],'password' => $postData['Password'],'phone' => $postData['contact_number'],'email' => $postData['email_address'],'address'=>$postData['address'],'state'=>$postData['state'],'country'=>$postData['country'] ,'zipcode'=>$postData['zipcode'],'user_token'=>$token);
			$insertId = $this->common->insert(TB_USERS,$insertArr); 
			//echo $this->db->last_query();die;
			if($insertId){
			echo json_encode(array("status"=>"success","action"=>"add","msg"=>"User has been added successfully.")); exit;	
			}else{
			echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
			}
	
	}
  public function change_user_status() {

  	if(is_ajax_request())
		{
			if(is_user_logged_in()){
					//print_r($_REQUEST);die;
					$postData = $this->input->post();
					$userData = $this->common->selectQuery("status",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
					//print_r($userData);die;
				if($userData[0]["status"] == "active"){
						$status_update = "inactive";
					}
					else if($userData[0]["status"] == "inactive"){
						$status_update = "active";
					}
				$insertArr = array('status'=>$status_update,'updated_at'=>date("Y-m-d h:i:s", time()));

				if($postData["key"]){
					$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					//echo $this->db->last_query();die;
					$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));

                           if($userData[0]["status"] == "active"){
					$message = file_get_contents("media/emails/accountactivation.html");
					$message = preg_replace('/{user}/', $userData[0]['first_name'], $message);
					$message = preg_replace('/{user_id}/', $userData[0]['id'], $message);
					$message = preg_replace('/{url}/', $config['base_url'], $message);
					$this->load->library('email');  
					$config['charset'] = 'iso-8859-1';
					$config['wordwrap'] = TRUE;
					$config['mailtype'] = 'html';
					$this->email->initialize($config);  
					$this->email->from(EMAIL_FROM, 'tanuja'); 
					$this->email->to($userData[0]['email']);
					$this->email->subject('User Activation mail'); 
					$this->email->message($message); 
					$this->email->send();
}
					if($insertId){
						echo json_encode(array("status"=>"success","userData"=>$userData[0],"msg"=>"User has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

}
