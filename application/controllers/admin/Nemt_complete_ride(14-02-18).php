<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Nemt_complete_ride extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');		
	}

	public function index()
	{
 		if(is_user_logged_in())
		{
			$this->load->view('dynamic_records/listOfCompleteRideDetails');			
		}else{
			redirect("login");
			exit;
		}
	}

	public function listOfCompleteRideDetails(){	
		if(is_ajax_request())
		{	
			if(is_user_logged_in()){			
				$postData = $this->input->post();				
				$arrayColumn = array("sr_trip_id"=>"sr_trip_id","full_name"=>"full_name","destination_address"=>"destination_address","source_address"=>"source_address","sr_appointment_time"=>"sr_appointment_time","sr_date"=>"sr_date");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where=array(TB_USERS.'.user_type'=>2,TB_SCHEDULE_RIDE.'.sr_status'=>2);
                $join=array();
				$join = array(TB_USERS=>TB_SCHEDULE_RIDE.'.user_id='. TB_USERS.'.id',TB_COUNTRY=>TB_SCHEDULE_RIDE.'.sr_country='.TB_COUNTRY.'.id',TB_STATE=> TB_SCHEDULE_RIDE.'.sr_state='.TB_STATE.'.id');
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'full_name',"'id'",TB_SCHEDULE_RIDE,'DISTINCT ('.TB_SCHEDULE_RIDE.'.id),'.TB_SCHEDULE_RIDE.'.sr_status,'.TB_STATE.'.state_name,'.TB_COUNTRY.'.country_name,'.TB_USERS .'.full_name,'.TB_SCHEDULE_RIDE.'.sr_date,'.TB_SCHEDULE_RIDE.'.sr_appointment_time,'.TB_SCHEDULE_RIDE.'.user_id,'.TB_SCHEDULE_RIDE.'.sr_state,'.TB_SCHEDULE_RIDE.'.sr_country,'.TB_SCHEDULE_RIDE.'.sr_trip_id,'.TB_SCHEDULE_RIDE.'.sr_pick_up_time,'.TB_SCHEDULE_RIDE.'.source_address,'.TB_SCHEDULE_RIDE.'.destination_address','listOfCompleteRideDetails', $where,$join,"","user_id");
            	$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$user['user_id']));
						$userId = $this->encrypt->encode($user['user_id']);
						$scheduleId = $this->encrypt->encode($user['id']);
                        $state = $this->common->selectQuery("state_name",TB_STATE,array('id' =>$user['sr_state']));
                    	$country = $this->common->selectQuery("country_name",TB_COUNTRY,array('id' =>$user["sr_country"]));
						$rows .= '<tr id="'.$userId.'">
							<td class="text-left">'.$userData[0]['full_name'].'</td>
							<td class="text-left">'.$user['sr_date'].'</td>						   
							<td class="text-left">'.$user['sr_appointment_time'].'</td>
							<td class="text-left">'.$user['source_address'].'</td>
							<td class="text-left">'.$user['destination_address'].'</td>
							 <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$scheduleId.'" class="" onclick="getDetails(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-eye"></i>
										  <a data-id="'.$i.'" data-row-id="'.$scheduleId.'" class="" onclick="delete_complete_ride(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>	                              
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getDetails(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$query = $this->db->query("SELECT DISTINCT (tbl_schedule_ride.id), tbl_schedule_ride.sr_status, tbl_states.state_name, tbl_country.country_name, tbl_users.full_name, tbl_schedule_ride.sr_date, tbl_schedule_ride.sr_appointment_time, tbl_schedule_ride.user_id, tbl_schedule_ride.sr_state, tbl_schedule_ride.sr_country, tbl_schedule_ride.sr_trip_id, tbl_schedule_ride.sr_pick_up_time, tbl_schedule_ride.source_address, tbl_schedule_ride.destination_address
				FROM `tbl_schedule_ride`
				JOIN `tbl_users` ON `tbl_schedule_ride`.`user_id`=`tbl_users`.`id`
				JOIN `tbl_country` ON `tbl_schedule_ride`.`sr_country`=`tbl_country`.`id`
				JOIN `tbl_states` ON `tbl_schedule_ride`.`sr_state`=`tbl_states`.`id`
				WHERE tbl_users.user_type = 2 AND tbl_schedule_ride.sr_status =2 and tbl_schedule_ride.id=".$this->encrypt->decode($postData['key'])."");
				$getDetails=$query->result_array();	
				if($getDetails){
					echo json_encode(array("status"=>"success","userData"=>$getDetails[0]), true); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
	}
	public function delete_complete_ride() 
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();				
				$deleteId = $this->common->delete(TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])));
				if($deleteId){
					echo json_encode(array("status"=>"success","msg"=>"Complete ride has been deleted successfully.")); exit;	
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
				}
			}
		}
	}
}
