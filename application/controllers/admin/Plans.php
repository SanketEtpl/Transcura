<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Plans extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$this->load->view('plans/listPlans');
			$this->load->model('common_model');
		}else{
			redirect("login");
			exit;
		}

	}

	public function listPlans(){
		//print_r("ghjfgh");die;
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();
				//print_r($postData) ;die;
				$arrayColumn = array("plan_id"=>"plan_id","plan_title"=>"plan_title");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where=array("del_status"=>'no');
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'plan_title','plan_id',TB_PLANS,'*','listPlans' ,$where);
              //print_r($result) ;die;
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['plan_id']);							
						//print_r($result['rows']);
						$rows .= '<tr id="'.$userId.'">
	                            <td class="text-left">'.$user['plan_title'].'</td>
	                             <td class="text-left">'.$user['plan_price'].'</td>
	                             <td class="text-left">'.$user['days'].'</td>
								
	                           <td class="text-left">
								<div class="btn-group btn-toggle" id="accept"> 
									<a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="change_status_Plan(this)" href="javascript:void(0)">
									'.$user['status'].'	
									</a>
								  </div>
	                           </td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getPlan(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
									</a>
	                               <a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="deletePlan(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getPlan(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("*",TB_PLANS,array('plan_id'=>$this->encrypt->decode($postData['key'])));
				
				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function savePlan(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
                  
					$postData = $this->input->post();

					$insertArr = array('plan_title'=>$postData["plan_title"],
					'plan_price' => $postData['plan_price'],
					'days' => $postData['days'],

					'updated_at'=>date("Y-m-d h:i:s", time())				
					);


				if($postData["user_key"]){
					$insertId = $this->common->update(TB_PLANS,array('plan_id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
					
					if($insertId){
                    	echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Plan has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}

				}else{
                    $insertArr = array('plan_title'=>$postData["plan_title"],
					'plan_price' => $postData['plan_price'],										
					'updated_at'=>date("Y-m-d h:i:s", time()),
					'days' => $postData['days'],	
					'created_at'=>date("Y-m-d h:i:s", time()),
					'status'=>"Published",
					'del_status'=>"no"

					);

					$insertId = $this->common->insert(TB_PLANS,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Plan has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

	
  public function deletePlan() {

  	if(is_ajax_request())
		{
			if(is_user_logged_in()){
				//print_r($_REQUEST);die;
					$postData = $this->input->post();
					$userData = $this->common->selectQuery("del_status",TB_PLANS,array('plan_id'=>$this->encrypt->decode($postData['key'])));
					//print_r($userData);die;
				if($userData[0]["del_status"] == "yes"){
						$status_update = "no";
					}
					else if($userData[0]["del_status"] == "no"){
						$status_update = "yes";
					}
				$insertArr = array('del_status'=>$status_update,'updated_at'=>date("Y-m-d h:i:s", time())	);

				if($postData["key"]){
					$insertId = $this->common->update(TB_PLANS,array('plan_id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					//echo $this->db->last_query();die;
					$userData = $this->common->selectQuery("*",TB_PLANS,array('plan_id'=>$this->encrypt->decode($postData['key'])));
					if($insertId){
						echo json_encode(array("status"=>"success","userData"=>$userData[0],"msg"=>"Plan has been deleted successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

	public function change_status_Plan() {

  	if(is_ajax_request())
		{
			if(is_user_logged_in()){
					//print_r($_REQUEST);die;
					$postData = $this->input->post();
					$userData = $this->common->selectQuery("status",TB_PLANS,array('plan_id'=>$this->encrypt->decode($postData['key'])));
				//	print_r($userData);die;
				if($userData[0]["status"] == "Published"){
						$status_update = "Unpublished";
					}
					else if($userData[0]["status"] == "Unpublished"){
						$status_update = "Published";
					}
				$insertArr = array('status'=>$status_update,'updated_at'=>date("Y-m-d h:i:s", time())	);
				if($postData["key"]){
					$insertId = $this->common->update(TB_PLANS,array('plan_id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					//echo $this->db->last_query();die;
					$userData = $this->common->selectQuery("*",TB_PLANS,array('plan_id'=>$this->encrypt->decode($postData['key'])));
					
					if($insertId){
						echo json_encode(array("status"=>"success","userData"=>$userData[0],"msg"=>"Plan has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}


}
