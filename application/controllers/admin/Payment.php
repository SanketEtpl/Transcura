<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Payment extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$this->load->view('payment');
		}
		else
		{
			redirect("admin");
			exit;
		}
	}
}