<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
			$this->load->library('encrypt');
		if(is_user_logged_in())
		{
			redirect("admindashboard");
			exit;
		}else{
			$this->session->unset_userdata("auth_admin");
			$this->load->view('login');
			//exit;
		}
	}

	public function checkLogin(){
		if(is_ajax_request())
		{
			$postData = $this->input->post();
			$result = $this->common->select("*",TB_ADMIN,array("username"=>trim($postData["username"]),"password"=>md5($postData["password"])));
			if($result)
			{
				$this->session->set_userdata("auth_admin", $result[0]);
				//print_r($_SESSION);die;
				echo json_encode(array("status" => "success" ,"message"=>"User login sucessfully.")); exit;
			}
			else
			{
				echo json_encode(array("status" => "error","message"=>"Please provide valid login credential.	")); exit;
			}
		}
	}

public function admin_reset_password()
{
	//redirect("forgot_password");
	$this->load->view('forgot_password');
}

/*public function forgetPassword()
{
	$this->load->library('form_validation');
	$postData = $this->input->post();
$this->form_validation->set_rules('username', 'Email', 'trim|required|max_length[50]|valid_email|xss_clean',array('valid_email'=>'Please enter a valid email address'));
$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
		if( $this->form_validation->run() == FALSE )
        {  			        	
			$this->load->view('forgot_password');				
		}
		else
		{ 
	$result = $this->common->select("*",TB_ADMIN,array("username"=>trim($postData["username"])));
	$this->load->library('encrypt');
	
	if(count($result)>0)
	{
		$decrypted_string = $this->encrypt->decode($result[0]['password']);
		$hostname = $this->config->item('hostname');
		$config['mailtype'] ='html';
		$config['charset'] ='iso-8859-1';
		$this->email->initialize($config);
		$from  = EMAIL_FROM; 
		$email = $postData['username'];
		
		$this->messageBody  = email_header();
		$base_url = $this->config->item('base_url');           
            $this->messageBody  .= '<tr>
                <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                    <p>Dear Admin,</p>
                        <p>
                        You password has '.$decrypted_string.'</p>
                        <p>
                        </p><p>If the above link does not work, please copy and paste the following link into your browser.</p>
                    </td>
                </tr>
                <tr>';                      
            $this->messageBody  .= email_footer();
            $this->email->from($from, $from);
            $this->email->to($email);
            $this->email->subject('Admin Password');
            $this->email->message($this->messageBody);
            $this->email->send();
	   if($this->email->send()) 
	    {
	    	$this->session->set_flashdata('success', 'Your password has been sent to your email. please check your email.');
	    	$this->load->view('forgot_password');
		}       
	else
	{
	    $this->session->set_flashdata('fail', 'Error occured,Please try again.');
	    $this->load->view('forgot_password');
	}	
// echo json_encode(array("status" => "success" ,"message"=>"Your password has been sent to your email. please check your email.")); exit;
	}
	else
	{
		//echo json_encode(array("status" => "error","message"=>"Please provide valid login credential.	")); exit;
		$this->session->set_flashdata('fail', 'Please enter valid credential.');
		$this->load->view('forgot_password');
	}
}*/
public function forgetPassword()
	{
		$this->load->library('form_validation');
		$postData = $this->input->post();
		$this->form_validation->set_rules('username', 'Email', 'trim|required|max_length[50]|valid_email|xss_clean',array('valid_email'=>'Please enter a valid email address'));
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
		if( $this->form_validation->run() == FALSE )
	    {  			        	
			$this->load->view('forgot_password');				
		}
		else
		{ 
		$result = $this->common->select("*",TB_ADMIN,array("username"=>trim($postData["username"])));
		//print_r($result);exit;
		$this->load->library('encrypt');
		
		if(count($result)>0)
		{
			$decrypted_string = $this->encrypt->decode($result[0]['password']);
			$hostname = $this->config->item('hostname');
			$config['mailtype'] ='html';
			$config['charset'] ='iso-8859-1';
			$this->email->initialize($config);
			$from  = EMAIL_FROM; 
			$email = $postData['username'];
			$this->messageBody  = email_header();
			$base_url = $this->config->item('base_url');           
	            $this->messageBody  .= '<tr>
	                <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
	                    <p>Dear Admin,</p>
	                        <p>
	                        You password has '.$decrypted_string.'</p>
	                        <p>
	                        </p><p>If the above link does not work, please copy and paste the following link into your browser.</p>
	                    </td>
	                </tr>
	                <tr>';                      
	            $this->messageBody  .= email_footer();
	            $this->email->from($from, $from);
	            $this->email->to($email);
	            $this->email->subject('Admin Password');
	            $this->email->message($this->messageBody);
	            $this->email->send();
	            if($this->email->send()) 
			    {
			    	$this->session->set_flashdata('success', 'Your password has been sent to your email. please check your email.');
			    	$this->load->view('forgot_password');
				}       
	        	else
	        	{
	        	    $this->session->set_flashdata('fail', 'Error occured,Please try again.');
	        	    $this->load->view('forgot_password');
				}	
		  // echo json_encode(array("status" => "success" ,"message"=>"Your password has been sent to your email. please check your email.")); exit;
		}
		else
		{
			//echo json_encode(array("status" => "error","message"=>"Please provide valid login credential.	")); exit;
			$this->session->set_flashdata('fail', 'Please enter valid credential.');
			$this->load->view('forgot_password');
		}
	}
	public function logout()
	{
		$this->session->sess_destroy();
		redirect("admin/"); exit;
	}
}
