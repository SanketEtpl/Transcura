<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Car_category extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
 		if(is_user_logged_in())
		{
			$this->load->view('dynamic_records/listOfCarCategory');
			$this->load->model('common_model');			
		}else{
			redirect("login");
			exit;
		}
	}

	public function listcar_category(){	
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();				
				$arrayColumn = array("carcat_id"=>"carcat_id","car_category"=>"car_category");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where=array();
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'carcat_id','car_category',TB_CAR_CATEGORY,'*','listcar_category',$where);               
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['carcat_id']);
						$rows .= '<tr id="'.$userId.'">
								<td class="text-left">'.$i.'</td>
	                            <td class="text-left">'.$user['car_category'].'</td>
	                            <td class="text-left">'.$user['car_rate_per_mile'].'</td>
	                            <td class="text-left">'.$user['car_capacity'].'</td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getCar_cat(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
										  <a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="deleteCar_cat(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>	                              
	                            </td>
	                        </tr>';
	                        $i++;
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getCar_cat(){

		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("*",TB_CAR_CATEGORY,array('carcat_id'=>$this->encrypt->decode($postData['key'])));
				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function saveCar_cat(){

		if(is_ajax_request())
		{
			if(is_user_logged_in()){				
					$postData = $this->input->post();
					
				if($postData["user_key"]){
					$updateArr = array('car_category'=>$postData["category_title"],					
						'car_rate_per_mile'=>$postData["car_rate_per_mile"],
						'car_capacity'=>$postData["car_capacity"],
					'updated_at'=>date("Y-m-d h:i:s", time()),	
					);
					$updateId = $this->common->update(TB_CAR_CATEGORY,array('carcat_id'=>$this->encrypt->decode($postData['user_key'])),$updateArr);
					if($updateId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Car category has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					$insertArr = array('car_category'=>$postData["category_title"],
						'car_rate_per_mile'=>$postData["car_rate_per_mile"],
						'car_capacity'=>$postData["car_capacity"],
					'created_at'=>date("Y-m-d h:i:s", time()),						
					);
					$insertId = $this->common->insert(TB_CAR_CATEGORY,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Car category has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

	
  

	public function deleteCar_cat() {
	if(is_ajax_request())
	{
		if(is_user_logged_in()){
				$postData = $this->input->post();				
				$deleteId = $this->common->delete(TB_CAR_CATEGORY,array('carcat_id'=>$this->encrypt->decode($postData['key'])));
				if($deleteId){
					echo json_encode(array("status"=>"success","msg"=>"Car category has been deleted successfully.")); exit;	
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
				}
			}
		}
	}
}
