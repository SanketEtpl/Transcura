<?php 
/* Author : Ram K 
   Page : Customer_care.php
   Description:  Customer Care Details
   Date: 12 Feb 2018
*/

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_care extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('common_model');		
	}

	public function index()
	{
 		if(is_user_logged_in())
		{
			$this->load->view('dynamic_records/listOfCustomerCareDetails');			
		}else{
			redirect("login");
			exit;
		}
	}
	/* Display listing */
	public function listOfCustomerCareDetails(){
		if(is_ajax_request())
		{	
			if(is_user_logged_in()){			
				$postData = $this->input->post();				
				$arrayColumn = array("username"=>"username","password"=>"password","first_name"=>"first_name","last_name"=>"last_name","email"=>"email","phone"=>"phone","is_active"=>"is_active");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where=array(TB_ADMIN.'.role'=>2,'is_deleted'=>'0');
                $join=array();
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'first_name',"'id'",TB_ADMIN,'first_name,id,last_name,email,phone,is_active','listOfCustomerCareDetails', $where,$join,"","id");
				// echo $this->db->last_query();die;
            	$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $admin) {
						$adminData = $this->common->selectQuery("*",TB_ADMIN,array('id'=>$admin['id']));
						$adminId = $this->encrypt->encode($admin['id']);
						$aid = strtr($adminId, '+/', '-_');

						if($admin['is_active'] == 1)
						{
							$status = 'Active';
						}else{
							$status = 'Inactive';
						}
						$rows .= '<tr id="'.$aid.'">
							<td class="text-left">'.$admin['first_name'].' '.$admin['last_name'].'</td>
							<td class="text-left">'.$admin['email'].'</td>
							<td class="text-left">'.$admin['phone'].'</td>
							<td class="text-left">
								<div class="btn-group btn-toggle" id="accept"> 
									<a data-id="'.$i.'" data-row-id="'.$aid.'" onclick="changeStatus(this)" href="javascript:void(0)">
									'.ucfirst($status).'	
									</a>
								  </div>
	                           </td>						   
							
							 <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$aid.'" onclick="getDetails(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa fa-edit"></i>
									</a>
									<a data-id="'.$i.'" data-row-id="'.$aid.'" class="" onclick="deleteUser(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>							                               
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	/* Get details while edit */
	public function getDetails(){

		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$adminID = strtr($postData["key"], '-_', '+/');
				$user_key = $this->encrypt->decode($adminID);

				$userData = $this->common->selectQuery("id,first_name,last_name,email,phone,username,actual_pwd",TB_ADMIN,array('id'=>$user_key));				

				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	/* Save and Edit  */
	public function save(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
			$searcharray = $this->input->post();
			parse_str($searcharray['formData'], $postData);
			$adminID = strtr($postData["user_key"], '-_', '+/');
			$user_key = $this->encrypt->decode($adminID);

			if($user_key !='')
			{
				$data = $this->Common_model->select('id,email,username',TB_ADMIN,array('id'=>$user_key));

				$updateArr = array(
						'username'=>$data[0]['username'],
						'password'=>trim(md5($postData["password"])),
						'actual_pwd'=>trim($postData["password"]),
						'first_name'=>trim($postData["first_name"]),
						'last_name'=>trim($postData["last_name"]),
						'email'=>$data[0]['email'],
						'phone'=>trim($postData["phone"]),
						'role'=>2,
						'modified_date' => date('Y-m-d H:i:s')
					);

				$updateId = $this->common->update(TB_ADMIN,array('id'=>$user_key),$updateArr);
				if($updateId)
				{
					echo json_encode(array("status"=>"success","msg"=>"Customer care user has been updated successfully.")); exit;	
				}
				else
				{
					echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
				}

			}else{
				$data = $this->Common_model->select('id,email,username',TB_ADMIN,array('email'=>$postData['email_address'],'username'=>$postData['username']));

				if($data)
				{
					$this->output
				    ->set_content_type("application/json")
				    ->set_output(json_encode(array('status'=>true,'msg' => 'Email ID Or Username already exists.')));		    
				}
				else
				{
					$insertArr = array(
						'username'=>trim($postData["username"]),
						'password'=>trim(md5($postData["password"])),
						'actual_pwd'=>trim($postData["password"]),
						'first_name'=>trim($postData["first_name"]),
						'last_name'=>trim($postData["last_name"]),
						'email'=>trim($postData["email_address"]),
						'phone'=>trim($postData["phone"]),
						'role'=>2,
						'created_date' => date('Y-m-d H:i:s')
					);
					$result = $this->Common_model->insert(TB_ADMIN,$insertArr);
					

					if($result){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Customer care user has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"add","msg"=>"Please try again.")); exit;	
					}
	    		}
			}
			
			}
		}
	}


	// check email availability 
	public function check_email_availability()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$email = $this->input->post('email');
				// check email & user type are already exist or not
				$data = $this->Common_model->select('id,email',TB_ADMIN,array('email'=>$email));
				if($data)
				{
					$this->output
				    ->set_content_type("application/json")
				    ->set_output(json_encode(array('status'=>true,'message' => 'Email ID already exists.')));		    
				}
				else
				{
					$this->output
				    ->set_content_type("application/json")
				    ->set_output(json_encode(array('status'=>false)));
				}
			}
		}
	}

	// check username availability 
	public function check_username_availability()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$username = $this->input->post('username');
				// check username are already exist or not
				$data = $this->Common_model->select('id,username',TB_ADMIN,array('username'=>$username));
				if($data)
				{
					$this->output
				    ->set_content_type("application/json")
				    ->set_output(json_encode(array('status'=>true,'message' => 'Username already exists.')));		    
				}
				else
				{
					$this->output
				    ->set_content_type("application/json")
				    ->set_output(json_encode(array('status'=>false)));
				}
			}
		}
	}

	public function change_user_status()
	{
	  	if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("is_active",TB_ADMIN,array('id'=>$this->encrypt->decode($postData['key'])));
				if($userData[0]["is_active"] == "1")
				{
					$status_update = "0";
				}
				else if($userData[0]["is_active"] == "0")
				{
					$status_update = "1";
				}
				$insertArr = array('is_active'=>$status_update,'modified_date'=>date("Y-m-d h:i:s", time()));
				if($postData["key"])
				{
					$insertId = $this->common->update(TB_ADMIN,array('id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					$userData = $this->common->selectQuery("*",TB_ADMIN,array('id'=>$this->encrypt->decode($postData['key'])));
		   
					if($insertId)
					{
						echo json_encode(array("status"=>"success","userData"=>$userData[0],"msg"=>"Customer care user status has been change successfully.")); exit;	
					}
					else
					{
						echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

	public function deleteUser()
    {
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();
				$adminID = strtr($postData["key"], '-_', '+/');
				$user_key = $this->encrypt->decode($adminID);
				if($user_key)
				{						
					$updateArr = array('is_deleted' => '1');
					$updateId = $this->common->update(TB_ADMIN,array('id'=>$user_key),$updateArr);
					if($updateId){											
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Customer care user has been deleted successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
				else
				{
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
				}
			}
		}
    }

}