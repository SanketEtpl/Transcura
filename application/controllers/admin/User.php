<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('email_template_helper');
		$this->load->helper('security');
		$this->load->model('Common_model');
		$this->load->helper('string');
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$this->load->view('User/listUser');
			$this->load->model('common_model');
		}else{
			redirect("login");
			exit;
		}

	}

	public function listUser(){
		//print_r("ghjfgh");die;
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();
				//print_r($postData) ;die;
				$arrayColumn = array("id"=>"id","fname"=>"first_name","lname"=>"last_name","email"=>"email","phone"=>"phone" ,"gender"=>"gender" );
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");

				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'fname','id',TB_USERS,'*','listUser');
               //print_r($result) ;die;
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['id']);

						//print_r($result['rows']);
						$rows .= '<tr id="'.$userId.'">
						<td class="text-left">'.$user['first_name'].'</td>
						<td class="text-left">'.$user['last_name'].'</td>
						<td class="text-left">'.$user['email'].'</td>
						<td class="text-left">'.$user['phone'].'</td>	                           
						<td class="text-left">'.$user['gender'].'</td>
						<td class="text-left">'.$user['user_type'].'</td>
						<td class="text-left">
							<div class="btn-group btn-toggle" id="accept"> 
								<a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="changeStatus(this)" href="javascript:void(0)">
									'.$user['status'].'	
								</a>
							</div>
						</td>
						<td class="text-left">
							<a data-id="'.$i.'" data-row-id="'.$userId.'" class="" onclick="getUser(this)" href="javascript:void(0)">
								<i class="fa fa-fw fa-check-square-o"></i>
							</a>

						</td>
					</tr>';
				}
			}
			else
			{
				$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
			}
			$data["rows"] = $rows;

			$data["pagelinks"] = $result["pagelinks"];
			$data["entries"] = $result['entries'];
			$data["status"] = "success";

			echo json_encode($data);

		}else{
			echo json_encode(array("status"=>"logout"));
		}
	}
}

public function getUser(){
	if(is_ajax_request())
	{
		if(is_user_logged_in()){
			$postData = $this->input->post();
			$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
			if($userData){
				echo json_encode(array("status"=>"success","userData"=>$userData[0])); exit;
			}else{
				echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
			}
		}else{
			echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
		}
	}

}

public function saveUser(){
	if(is_ajax_request())
	{
		if(is_user_logged_in()){

                   //print_r($_REQUEST);die;
			$postData = $this->input->post();
			$insertArr = array('first_name'=>$postData["first_name"],
				'last_name' => $postData['last_name'],
				'email'=>$postData["email_address"],
				'address' => $postData['address'],
				'zipcode'=>$postData["zipcode"],
				'phone'=>$postData["phone"],
				'gender'=>$postData["gender"],
				'updated_at'=>date("Y-m-d h:i:s", time()),				


				);
			if($postData["user_key"]){
				$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
				//echo $this->db->last_query();die;
				if($insertId){
					echo json_encode(array("status"=>"success","action"=>"update","msg"=>"User has been updated successfully.")); exit;	
				}else{
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
				}
			}else{
				$insertId = $this->common->insert(TB_USERS,$insertArr);
				if($insertId){
					echo json_encode(array("status"=>"success","action"=>"add","msg"=>"User has been added successfully.")); exit;	
				}else{
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
				}
			}
		}
	}
}

public function saveRegisterUser(){

	$postData = $this->input->post();


	$token = "";
			//Combination of character, number and special character...
	$combinationString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789*#&$^";

	$this->load->helper('string');
	$token=  random_string('alnum',20);

	$insertArr = array('first_name'=>$postData["first_name"],'username' => $postData['username'],'user_type' => $postData['user_type'],'password' => $postData['Password'],'phone' => $postData['contact_number'],'email' => $postData['email_address'],'address'=>$postData['address'],'state'=>$postData['state'],'country'=>$postData['country'] ,'zipcode'=>$postData['zipcode'],'user_token'=>$token);
	$insertId = $this->common->insert(TB_USERS,$insertArr); 
			//echo $this->db->last_query();die;
	if($insertId){
		echo json_encode(array("status"=>"success","action"=>"add","msg"=>"User has been added successfully.")); exit;	
	}else{
		echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
	}
	
}
public function change_user_status() {

	if(is_ajax_request())
	{
		if(is_user_logged_in()){
					//print_r($_REQUEST);die;
			$postData = $this->input->post();
			$userData = $this->common->selectQuery("status",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
					//print_r($userData);die;
			if($userData[0]["status"] == "active"){
				$status_update = "inactive";
			}
			else if($userData[0]["status"] == "inactive"){
				$status_update = "active";
			}
			$insertArr = array('status'=>$status_update,'updated_at'=>date("Y-m-d h:i:s", time()));

			if($postData["key"]){
				$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					//echo $this->db->last_query();die;
				$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
				if($insertId){
					echo json_encode(array("status"=>"success","userData"=>$userData[0],"msg"=>"User has been updated successfully.")); exit;	
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
				}
			}
		}
	}
}

	//--------- Ashwini Code Here ----------
	//---- view file for create user ------
public function create_user()
{
	if(is_user_logged_in())
	{
		$data['userRole'] = $this->Common_model->select("*",TB_ROLE);
		$this->load->view('dynamic_records/create_user',$data);
	}else
	{
		redirect("login");
		exit;
	}
}

	//----- upload data to save user --------
public function saveUsers()
{
	if(is_user_logged_in())
	{
		$postData = $this->input->post();
		$usertype = $postData['selectType'];
		$filename=$_FILES["userfile"]["tmp_name"];
		$mimes = array('application/vnd.ms-excel','text/xlsx','text/csv');
		if(in_array($_FILES['userfile']['type'],$mimes))
		{
			if($_FILES["userfile"]["size"] > 0)
			{
				$row = 0;
				$result_cnt=0;
				$handle = fopen($filename, "r");
				while (($importdata = fgetcsv($handle, 0, ",")) !== FALSE)
				{				
					if($row != 0)
					{
						if($usertype == "5")
						{
							$appoval = "0";
						}else
						{
							$appoval = "1";
						}

						if(empty(trim($importdata[3])))
						{

						}else
						{
							$token = "";						
							$token=  random_string('alnum',20);
							$data = array(
								'user_token'=>$token,
								'user_type'=>$usertype,
								'full_name' => trim($importdata[0]),
								'date_of_birth' =>date('Y-m-d',strtotime($importdata[1])),
								'username' => trim($importdata[2]),
								'email' => trim($importdata[3]),
								'phone' => trim($importdata[4]),
								'password' => trim(md5($importdata[5])),
								'country' => trim($importdata[6]),
								'state' => trim($importdata[7]),
								'city' => trim($importdata[8]),
								'street' => trim($importdata[9]),
								'zipcode' => trim($importdata[10]),
								'county' => trim($importdata[11]),
								'emergency_contactname' => trim($importdata[12]),
								'emergency_contactno' => trim($importdata[13]),
								'signature' => trim($importdata[14]),
								'token'      => $token,
								'status'     => "active",
								'done'		 => 1,
								'is_approve'=>$appoval
								);
							$insert_query = $this->db->insert_string(TB_USERS, $data);
							$insert_query = str_replace('INSERT INTO', 'INSERT IGNORE INTO', $insert_query);
							$result = $this->db->query($insert_query);
							$result_cnt += $this->db->affected_rows();

							$last_id = $this->db->insert_id();
							$getemail = $this->Common_model->select('token,email',TB_USERS,array('id'=>$last_id));

							// Email to registration
							$hostname = $this->config->item('hostname');
							$config['mailtype'] ='html';
							$config['charset'] ='iso-8859-1';
							$this->email->initialize($config);
							$from  = EMAIL_FROM; 
							$email = $getemail[0]['email'];
							$this->messageBody  = email_header();
							$base_url = $this->config->item('base_url');
							$reset_url = $base_url."accountverify/".$getemail[0]['token'];			
							$this->messageBody  .= '<tr>
							<td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
								<p>Dear Customer,</p>
								<p>
									You have registered successfully on Transcura.<br> Please click the link given below to verify your email address</p>
									<p>
										<p>Activation Link : </p>
										<a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href='.$reset_url.'>Click Here</a>
									</p><br /><br />
									<p>If the above link does not work, please copy and paste the following link into your browser.</p>
									<p>Copy the Link : </p>
									<a href="'.$reset_url.'">'.$reset_url.'</a>
								</p>					
							</td>
						</tr>'; 			
						$this->messageBody  .= email_footer();
						$this->email->from($from, $from);
						$this->email->to($email);
						$this->email->subject('Welcome to Transcura');
						$this->email->message($this->messageBody);
						$this->email->send();
					}
				}					
				$row++;					
			}
			fclose($handle);
			if($result_cnt > 0)
			{
				echo json_encode(array("status"=>"success","message"=>$result_cnt." records added successfully.")); exit;
			}else
			{
				echo json_encode(array("status"=>"error","message"=>"Oops!!! Something went wrong. The users are alredy uploaded.")); exit;
			}
		}
	} else {
		echo json_encode(array("status"=>"error","message"=>"File type not allowed. Please upload csv file.")); exit;
	}
}else
{
	redirect("login");
	exit;
}
}

}
