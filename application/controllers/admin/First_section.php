<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
class First_section extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');	
	}

	public function index()
	{		
		if(is_user_logged_in())
		{
			$this->load->view('dynamic_records/listOfFirstSection');
		}
		else
		{
			redirect("login");
			exit;
		}
	}

	public function listAllFirstSectionData(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();				
				$arrayColumn = array("fs_id"=>"fs_id","fs_title"=>"fs_title","fs_description"=>"fs_description","fs_icon_image"=>"fs_icon_image");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
				$where=array("");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'fs_id','fs_id',TB_FIRST_SECTION,'*','listAllFirstSectionData',$where);	          
				//print_r($result);exit;
				$rows = '';				
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $data) {
						$service_id = $this->encrypt->encode($data['fs_id']);
						$string = $data['fs_description'];
						if (strlen($string) > 250)
						{													
							$stringCut = substr($string, 0, 250);
							$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
						}						
						if(empty($data['fs_icon_image']))
						$image ='<td class="text-left"><img src='.base_url().'uploads/service_type/img-dummy.jpg'.' width="75" height="75" ></td>';	
						else
							$image ='<td class="text-left"><img src='.base_url().$data['fs_icon_image'].' width="75" height="75" ></td>';
						$rows .= '<tr id="'.$service_id.'">								
	                            <td class="text-left">'.$data['fs_title'].'</td>
	                            <td class="text-left">'.$string.'</td>										                           
	                            '.$image.'
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$service_id.'" class="" onclick="getFirstSection(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
									</a>
	                                <a data-id="'.$i.'" data-row-id="'.$service_id.'" class="" onclick="deleteFirstSection(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);				
			}
			else
			{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}


	public function getFirstSection(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){				
				$postData = $this->input->post();
				$pageData = $this->common->selectQuery("*",TB_FIRST_SECTION,array('fs_id'=>$this->encrypt->decode($postData['key'])));
				
				if($pageData){
					echo json_encode(array("status"=>"success","pageData"=>$pageData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong, Please try again...!!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function save_first_section(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$iconImage ='';
				$postData = $this->input->post();
				if(isset($_FILES["icon_image"]["name"]))   
			    {   
			        $file_name = $_FILES["icon_image"]["name"];
			        $original_file_name = $file_name;
			        $date = date("Y-m-d_h_i_s");
			        $random = rand(1, 10000000000000000);
			        $makeRandom = $random;
			        $file_name_rename = $makeRandom;
			        $explode = explode('.', $file_name);
			        if(count($explode) >= 2)
			        {
			            $new_file = $file_name_rename.$date.'.'.$explode[1];		         
			            $config['upload_path'] = "./assets/img/";
			            $config['allowed_types'] ="pdf|doc|docx|PDF|DOC|DOCX|jpeg|png|jpg";
			            $config['file_name'] = $new_file;
			            $config['max_size'] = '5000000';
			            $config['overwrite'] = TRUE;
			            $this->load->library('upload',$config);
	              		$this->upload->initialize($config);
						if(!$this->upload->do_upload("icon_image")) {
						$error = $this->upload->display_errors();
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
						} else {
						$iconImage = 'assets/img/'.$new_file;
						}
	           		}
	           	}
				else if(isset($postData['oldimg']))   
			    {          	 
			    	$iconImage =$postData['oldimg'];		        				
				}
		       	else
		        {
			   		$iconImage ="";	
		        }
		        //echo $iconImage;exit;
				if($postData["page_key"]){
					$updatedArr = array(
								'fs_title'=>$postData["title_name"],
								'fs_description'=>$postData["description"],
								'fs_icon_image'=>$iconImage,	
								'updated_on'=>date('Y-m-d H:i:s')							
								);
					$insertId = $this->common->update(TB_FIRST_SECTION,array('fs_id'=>$this->encrypt->decode($postData['page_key'])),$updatedArr);
					
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Content has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					$insertArr = array(
								'fs_title'=>$postData["title_name"],
								'fs_description'=>$postData["description"],
								'fs_icon_image'=>$iconImage,	
								'created_on'=>date('Y-m-d H:i:s')							
								);
					//$insertArr["created_at"] = $dateTime;
					$insertId = $this->common->insert(TB_FIRST_SECTION,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Content has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"add","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
    }


   public function deleteFirstSection(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();					
				if($postData["key"]){	
					$deleteImage = $this->common->selectQuery("fs_icon_image",TB_FIRST_SECTION,array("fs_id"=>$this->encrypt->decode($postData['key'])));
					if(!empty($deleteImage[0]))
					unlink($deleteImage[0]['fs_icon_image']);					
					$deleteId = $this->common->delete(TB_FIRST_SECTION,array('fs_id'=>$this->encrypt->decode($postData['key'])));
					if($deleteId){					
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Content has been deleted successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
				else
				{
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
				}
			}
		}
    }
}
