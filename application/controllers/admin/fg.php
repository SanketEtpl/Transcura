<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	
		$this->load->helper(array('form', 'url'));
		$this->load->library('image_lib');
		       $config['allowed_types'] =   "png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG";  
               $config['max_size']      =   "5000"; 
               $config['max_width']     =   "1000"; 
               $config['max_height']    =   "900"; 
              
              $config['upload_path']   =  "./images/" ;   
               
               	// $this->load->helper("email_template");
	}

	public function index()
	{
		if(is_user_logged_in())
		{
			$this->load->view('profile/listProfile');
			$this->load->model('common_model');
		}else{
			redirect("login");
			exit;
		}

	}

	public function listProfile(){
	//print_r("ghjfgh");die;
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();
				$arrayColumn = array("email"=>"email","first_name"=>"first_name","phone"=>"phone");
			    $result = $this->common->selectQuery("*",TB_ADMIN);   
              //echo $this->db->last_query();die;
				$rows = '';
				if(!empty($result[0]))
				{
					$i=1;
					
						$userId = $this->encrypt->encode($result[0]['id']);							
					
             $rows .= '<div class="datapro" id="idpro">
                      <div class="col-md-6">
                        <div class="">
                          <label>Name</label>
                          <div class="showdiv">'.$result[0]['first_name'].'</div>                          
                      </div>
                    </div>

                      <div class="col-md-6">
                        <div class="">
                          <label>last name</label>
                          <div class="showdiv">'.$result[0]['last_name'].'</div>                          
                      </div>
                    </div>
                    <div class="col-md-6">
                        <div class="">
                          <label>email</label>
                          <div class="showdiv">'.$result[0]['email'].'</div>                          
                      </div>
                    </div>
                      <div class="col-md-6">
                        <div class="">
                          <label>username</label>
                          <div class="showdiv">'.$result[0]['username'].'</div>                          
                      </div>
                    </div>

                  <div class="col-md-6">
                        <div class="">
                          <label>Phone</label>
                          <div class="showdiv">'.$result[0]['phone'].'</div>                          
                      </div>
                    </div>
                    <div class="col-md-12">
                     <a data-id="'.$i.'" data-row-id="'.$userId.'" title="Edit" value="Edit" class="btn btn-info tiny" onclick="getProfile(this)" href="javascript:void(0)">
							<i class="">Edit profile</i>
								</a>
                    </div>
                    </div>
                  </div>  
                </div>';					
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;

				
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getProfile(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("*",TB_ADMIN);         

				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0] )); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function ChangePassword(){
	if(is_ajax_request())
		{
			if(is_user_logged_in()){
			   $postData = $this->input->post();
                
			             $result = $this->common->selectQuery("*",TB_ADMIN); 
              			if($result[0]['password']!=md5($postData["oldpassword"])){
						echo json_encode(array("status"=>"error","msg"=>"old password goes wrong..!!")); exit;
						}
				       	else if($postData['confirmpassword']!=$postData["newpassword"]){
						echo json_encode(array("status"=>"error","msg"=>"new password and confirm password doesnot match..!")); exit;
						} 
                        else
                        {
		         			$postData = $this->input->post();
							$insertArr = array('password'=>md5($postData["newpassword"]	)					
							);


					        if($postData["oldpassword"]){
							  $insertId = $this->common->update(TB_ADMIN,array('id'=>$result[0]['id'] ),$insertArr);	
							 // echo $this->db->last_query();die;
								if($insertId){
									echo json_encode(array("status"=>"success","action"=>"update","msg"=>"profile has been updated successfully.")); 	
									$this->session->sess_destroy();
		                            redirect("admin");
								
								}else{
									echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
								}
						    }
				        }
			}
		}
   }





    public function saveProfile(){

		
			if(is_user_logged_in()){


			$postData = $this->input->post();


               $config['allowed_types'] =   "png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG";  
               $config['max_size']      =   "5000"; 
               $config['max_width']     =   "1000"; 
               $config['max_height']    =   "900"; 
               $config['upload_path']   =  "./images/" ;   

			$this->load->library('upload', $config);
    //  	    if(isset($_FILES["pimage"]["name"]))   
		  //   {   
				// if(!$this->upload->do_upload('pimage'))
				// {		 
				// $error = $this->upload->display_errors();
				// echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
				// } 
				// else
				// {		 
				// $data=$this->upload->data();							        
				// $data['picture'] = "images/".$data['file_name'];	
				// }				
    // 		}

    // 		else if(isset($postData['oldimg']))   
		  //   {   
		           
		  //         $data['picture'] =$postData['oldimg'];	
		        				
    // 		}
   	 	
         

     				$postData = $this->input->post();
					
					$insertArr = array('username'=>$postData["username"],					
					'email'=>$postData["email"],
					'phone' =>$postData['phone'],
					'first_name'=>$postData["first_name"],
					'last_name'=>$postData["last_name"],
					
						
					);
				if($postData["user_key"]){
					$insertId = $this->common->update(TB_ADMIN,array('id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);
		
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"profile has been updated successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		
    }


}
