<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->helper(array('form', 'url'));
		$this->load->library('image_lib');
        $config['allowed_types'] =   "png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG";  
        $config['max_size']      =   "5000"; 
        $config['max_width']     =   "1000"; 
        $config['max_height']    =   "900"; 
        $config['min_width']     = "70"; // new
        $config['min_height']    = "70"; // new    
        $config['upload_path']   =  "./uploads/Member/original/" ;   
       	$this->load->library('upload', $config);
       	$this->load->helper("email_template");
	}
	// default patient list call
	public function index()
	{
		if(is_user_logged_in())
		{
			$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);
			$this->load->view('member/listMember' ,$data);
			$this->load->model('common_model');
		}else{
			redirect("login");
			exit;
		}

	}
	// get list of patient data
	public function listMember(){		
		if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();				
				$arrayColumn = array("id"=>"id","fname"=>"full_name","email"=>"email","phone"=>"phone");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");
                $where =array("user_type"=>"1" ,"done"=>"1");
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'id','id',TB_USERS,'*','listMember',$where);
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $user) {
						$userId = $this->encrypt->encode($user['id']);
						$rows .= '<tr id="'.$userId.'">
	                            <td class="text-left">'.$user['full_name'].'</td>	                           
	                            <td class="text-left">'.$user['email'].'</td>
								<td class="text-left">'.$user['phone'].'</td>
	                            <td class="text-left">
								<div class="btn-group btn-toggle" id="accept"> 
									<a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="changeStatus(this)" href="javascript:void(0)">
									'.ucfirst($user['status']).'	
									</a>
								  </div>
	                           </td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$userId.'" onclick="getMember(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
									</a>
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}
//get city name
	public function get_city()
	{
		$postData = $this->input->post();
		$stateID = array('state_id'=>$postData['state_id']);
		$dataCity = $this->Common_model->select("*",TB_CITY,$stateID);
		$cityJSON = '';
		if($dataCity > 0)
		{
			$cityJSON .= '<option value="">Select city</option>';
			foreach ($dataCity as $key => $city) {
				$cityJSON.= '<option value="'.$city['id'].'">'.$city['city_name'].'</option>';
			}
		}
		else
		{
			$cityJSON .= '<option value="">City not available</option>';
		}
		echo json_encode(array('city'=>$cityJSON));
	}
	
	//get state name
	public function get_state()
	{	
		$postData = $this->input->post();
		//print_r($postData);die;
		$countryID=array('country_id'=>$postData['countryID']);
		$dataState = $this->Common_model->select("*",TB_STATE,$countryID);
		$stateJSON='';
		if($dataState > 0)
		{
			$stateJSON .= '<option value="">Select state</option>';
       		foreach ($dataState as $key => $state_name) {
       			$stateJSON.= '<option value="'.$state_name['id'].'">'.$state_name['state_name'].'</option>';
       		}       		
		}
		else
		{
			$stateJSON.= '<option value="">State not available</option>';
		}
		echo json_encode(array('state'=>$stateJSON));		
	}



	public function getMember(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));	
				$dataState = $this->Common_model->selectQuery("*",TB_STATE,array('country_id'=>$userData[0]['country']));
				$dataCity = $this->Common_model->selectQuery("*",TB_CITY,array('state_id'=>$userData[0]['state']));

				$stateJSON='';
				if($dataState > 0)
				{
					$stateJSON .= '<option value="">Select state</option>';
		       		foreach ($dataState as $key => $state_name) {
		       			$stateJSON.= '<option value="'.$state_name['id'].'">'.$state_name['state_name'].'</option>';
		       		}       		
				}
				else
				{
					$stateJSON.= '<option value="">State not available</option>';
				}


				$cityJSON='';
				if($dataCity > 0)
				{
					$cityJSON .= '<option value="">Select City</option>';
		       		foreach ($dataCity as $key => $city_name) {
		       			$cityJSON.= '<option value="'.$city_name['id'].'">'.$city_name['city_name'].'</option>';
		       		}       		
				}
				else
				{
					$cityJSON.= '<option value="">City not available</option>';
				}
				if($userData){
					echo json_encode(array("status"=>"success","userData"=>$userData[0],"stateJSON"=>$stateJSON,"cityJSON"=>$cityJSON)); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something goes wrong..!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"Member has been logout.")); exit;
			}
		}
		
	}

	public function saveMember(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
			$postData = $this->input->post();	
			//print_r($postData);die;		
			$token = "";				
			$combinationString = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789*#&$^";
			$this->load->helper('string');
            $token=  random_string('alnum',20);
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
            $password = substr( str_shuffle( $chars ), 0, 7 );
     	    if(isset($_FILES["pimage"]["name"]))   
		    {   
                if(!$this->upload->do_upload('pimage'))
		        {		 
			       $error = $this->upload->display_errors();
			       echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
		        } 
		        else
		        {		 
		            $data=$this->upload->data();
					$data = $this->resize($data);					        
		            $data['picture'] = $data['new_image'];	
		        }				
    		}
    		else if(isset($postData['oldimg']))   
		    {    		            
		        $data['picture'] =$postData['oldimg'];	
		    }
    	  	else
    	  	{
            	$data['picture'] = "images/default.jpg";	
         	}
         	if(isset($_FILES["personal_doc"]["name"]))   
		    {   
		        $file_name = $_FILES["personal_doc"]["name"];
		        $original_file_name = $file_name;
		        $random = rand(1, 10000000000000000);
		        $makeRandom = $random;
		        $file_name_rename = $makeRandom;
		        $explode = explode('.', $file_name);
		        if(count($explode) >= 2)
		        {
		            $new_file = $file_name_rename.'.'.$explode[1];		         
		            $config['upload_path'] = "./uploads/Member/doc";
		            $config['allowed_types'] ="pdf|doc|docx|PDF|DOC|DOCX";
		            $config['file_name'] = $new_file;
		            $config['max_size'] = '5000000';
		            $config['max_width'] = '300000';
		            $config['max_height'] = '300000';
		            $config['overwrite'] = TRUE;
		            $this->load->library('upload',$config);
              		$this->upload->initialize($config);
					if(!$this->upload->do_upload("personal_doc")) {
					$error = $this->upload->display_errors();
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
					} else {
					$doc = 'uploads/Member/doc/'.$new_file;
					}
           		}
           	}
			else if(isset($postData['oldpdf']))   
		    {          	 
		    	$doc =$postData['oldpdf'];		        				
			}
	       	else
	        {
		   		$doc ="";	
	        }	
			$postData = $this->input->post();
			$birth = date("Y-m-d", strtotime($postData["birth"])); 
			$insertArr = array('full_name'=>$postData["full_name"],					
			'email'=>$postData["email_address"],			
			'zipcode'=>$postData["zipcode"],
			'phone'=>$postData["phone"],			
			'date_of_birth'=>$birth	,
			'user_token'=>$token,
			'emergency_contactname' =>$postData['econtact_name'],
			'emergency_contactno'=>$postData["econtact_no"],
			'user_type'=>"1",			
			'picture'=> $data['picture'],
			'signature'=> $postData['signature'],
			'personal_doc'=>$doc,
			'done'	=>1,
			'country'=> $postData['country'],
			'state'=> $postData['state'],
            'city'=> $postData['city'],
            'street'=>$postData['street'],
            'county' =>$postData['county'],
            'status'=>'active'
			);
			if($postData["user_key"]){

			$userData = $this->common->selectQuery("*",TB_USERS,array('email'=>$postData["email_address"],'user_type'=>1));	
			
			$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['user_key'])),$insertArr);			
			if($insertId){
				echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Member user has been updated successfully.")); exit;	
			}else{
				echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
			}
		}else{

                $userData = $this->common->selectQuery("*",TB_USERS,array('email'=>$postData["email_address"],'user_type'=>1));	
 				if(count($userData)>0){
				echo json_encode(array("status"=>"error","msg"=>"Mail address you have entered is already exits")); exit;
				}

                 $insertArr = array('full_name'=>$postData["full_name"],					
					'email'=>$postData["email_address"],					
					'zipcode'=>$postData["zipcode"],
					'phone'=>$postData["phone"],					
					'updated_at'=>date("Y-m-d h:i:s", time()),	
					'date_of_birth'=>$birth	,
					'user_token'=>$token,
					'emergency_contactname' =>$postData['econtact_name'],
					'emergency_contactno'=>$postData["econtact_no"],
					'user_type'=>"1",								
					'password' =>md5($password),
					'picture'=> $data['picture'],
					'signature'=> $postData['signature'],
                    'personal_doc'=>$doc,
                    'done'	=>1,
                    'country'=> $postData['country'],
			       'state'=> $postData['state'],
                   'city'=> $postData['city'],
                   'street'=>$postData['street'],
                   'county' =>$postData['county'],
                   'status'=>'active'
					);
					$insertId = $this->common->insert(TB_USERS,$insertArr);	
				//	echo $this->db->last_query();die;				
					if($insertId){
                        $to_email = $postData["email_address"];
                        $hostname = $this->config->item('hostname');  
                        $config['mailtype'] ='html';
                        $config['charset'] ='iso-8859-1';
                        $this->email->initialize($config);
                        $from  = EMAIL_FROM; 
                        $this->messageBody  = email_header();
                        $this->messageBody  .= '<tr>
                            <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
                                <p>Dear '.$postData["full_name"].',</p>
                                    <p>
                                     You are registered successfully from admin on Transcura.Your login credencial are:<br>
                                      <p>Your email-'.trim($postData["email_address"]).'</p>
                                     <p>Your Password-'.trim($password).'</p>
                                    </p>                                   
                                    </p>
                                </td>
                            </tr>
                            <tr>';                      
                        $this->messageBody  .= email_footer();        //print_r( $this->messageBody);die;
                        $this->email->from($from, $from);
                        $this->email->to($to_email);
                        $this->email->subject('Welcome to Transcura');
                        $this->email->message($this->messageBody);
                        $this->email->send();                   
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"Member user has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}

	public function resize($image_data) {
        $img = substr($image_data['full_path'], 51);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $image_data['full_path'];
        $config['new_image'] ='./uploads/Member/thumb/thumb_'.$image_data['raw_name'].$image_data['file_ext'];
        $config['allowed_types'] = 'png|mpg|mpeg|wmv|jpg|JPEG|jpeg|PNG';       
        $config['width'] = 250;
        $config['height'] = 250;
        $this->image_lib->initialize($config);
        $src = $config['new_image'];
        $data['new_image'] = substr($src, 2);
        $data['img_src'] = base_url().$data['new_image'];
        if(!$this->image_lib->resize())
		{
			$error = $this->image_lib->display_errors();
			echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;		    
		}
		else
		{
        	return $data;		      
		}
    }

	public function change_user_status()
	{
	  	if(is_ajax_request())
		{
			if(is_user_logged_in()){			
				$postData = $this->input->post();
				$userData = $this->common->selectQuery("status",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
				if($userData[0]["status"] == "active")
				{
					$status_update = "inactive";
				}
				else if($userData[0]["status"] == "inactive")
				{
					$status_update = "active";
				}
				$insertArr = array('status'=>$status_update,'updated_at'=>date("Y-m-d h:i:s", time()));
				if($postData["key"])
				{
					$insertId = $this->common->update(TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])),$insertArr);
					$userData = $this->common->selectQuery("*",TB_USERS,array('id'=>$this->encrypt->decode($postData['key'])));
		            if($userData[0]["status"] == "active")
		            {
						$message = file_get_contents("media/emails/accountactivation.html");
						$message = preg_replace('/{user}/', $userData[0]['full_name'], $message);
						$message = preg_replace('/{user_id}/', $userData[0]['id'], $message);
						$this->load->library('email');  
						$config['charset'] = 'iso-8859-1';
						$config['wordwrap'] = TRUE;
						$config['mailtype'] = 'html';
						$this->email->initialize($config);  
						$this->email->from(EMAIL_FROM, 'Admin'); 
						$this->email->to($userData[0]['email']);
						$this->email->subject('Member Activation mail'); 
						$this->email->message($message); 
						$this->email->send();
					}
					if($insertId)
					{
						echo json_encode(array("status"=>"success","userData"=>$userData[0],"msg"=>"Member status has been change successfully.")); exit;	
					}
					else
					{
						echo json_encode(array("status"=>"error","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}
	}
}
