<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_ride_status extends CI_Controller {

	function __construct()
	{
		parent::__construct();		
	}

	public function index()
	{		
		if(is_user_logged_in())
		{
			$this->load->view('dynamic_records/adminRideStatus');
		}else{
			redirect("login");
			exit;
		}
	}

	public function listOfScheduleRide()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{								
				$postData = $this->input->post();
				$arrayColumn = array("user_id"=>"user_id","urjency_explainatin"=>"urjency_explainatin","id"=>"id","sr_trip_id"=>"sr_trip_id","sr_date"=>"sr_date","source_address"=>"source_address","destination_address"=>"destination_address" ,"sr_pick_up_time"=>"sr_pick_up_time");
				$arrayStatus["is_active"] = array();				
				$arrayColumnOrder = array("ASC","asc","DESC","desc");				
				$where=array();
				$result = pagination_data1($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'urjency_explainatin','sr_trip_id',TB_SCHEDULE_RIDE,'*','listOfScheduleRide',$where);	          
				$rows = '';
				//echo $this->db->last_query();exit;
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $value) 
					{

						$query = $this->db->query("SELECT id,full_name FROM tbl_users where id =".$value['user_id']);
						$result1 = $query->result_array();
						//print_r($result);exit();
						$name=$urgency="";
						if(!empty($result1[0]['full_name']))
							$name =$result1[0]['full_name'];
						else
							$name='';
						if($value['urjency_explainatin']!="")
							$urgency=$value['urjency_explainatin'];
						else
							$urgency='No';
						//print_r($result);exit;
						$status ="";
						$adminApprv = "";
						$ride_id = $this->encrypt->encode($value['id']);
						if($value['admin_status'] == 1)
							 $status = "Open ride";
						else if($value['admin_status'] == 2)
							 $status = "Accepted ride";
						else if($value['admin_status'] == 3)
							 $status = "Closed ride";
						else
							 $status = "Rejected ride";

						$rows .= 
								'<tr id="'.$ride_id.'">
									<td class="text-left">'.$name.'</td>
									<td class="text-left">'.$urgency.'</td>
									<td class="text-left">'.$value['sr_trip_id'].'</td>
		                            <td class="text-left">'.$value['sr_date'].'</td>
		                            <td class="text-left">'.$value['sr_pick_up_time'].'</td>
		                            <td class="text-left">'.$value['source_address'].'</td> 
		                            <td class="text-left">'.$status.'</td> 
		                            <td class="text-left">
		                            	<a data-id="'.$i.'" data-row-id="'.$ride_id.'" class="" onclick="getScheduleRide(this)" href="javascript:void(0)">
											<i class="fa fa-fw fa-eye"></i>
										</a>
										<a data-id="'.$i.'" data-row-status="1" data-row-id="'.$ride_id.'" onclick="changeStatus(this)" href="javascript:void(0)">
										Accept											
										</a> / 
										<a data-id="'.$i.'" data-row-status="2" data-row-id="'.$ride_id.'" onclick="changeStatus(this)" href="javascript:void(0)">
										Reject
										</a>
	                            	</td>
	                        	</tr>';
	                }
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				//print_r($ows);exit;
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";
				echo json_encode($data);				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}
	public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
	    $theta = $lon1 - $lon2;
	    $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	    $dist = acos($dist);
	    $dist = rad2deg($dist);
	    $miles = $dist * 60 * 1.1515;
	    $unit = strtoupper($unit);
		if ($unit == "K") {
		    return ($miles * 1.609344);
		} else if ($unit == "N") {
	      return ($miles * 0.8684);
	    } else {
	        return $miles;
	    }
	}

	public function change_user_status()
	{
	  	if(is_ajax_request())
		{
			$postData = $this->input->post();
			
			$statusData="";
			if($postData['status']==1)
			{ $statusData = 2; $st ="accepted"; }
			else	
			{ $statusData = 4; $st ="rejected"; }	
			$checkStatus = $this->Common_model->select('id,admin_status',TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key']),'admin_status'=>$statusData));
			//print_r($checkStatus);exit;
			if(!empty($checkStatus)){				
				echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Schedule ride already accepted.")); exit;	
			}else{
				//echo "success";exit;
				if($postData['status'] == 1){
					$rideInfo = $this->Common_model->select('id,sr_lat_lng_source,user_id',TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])));
					//print_r($rideInfo);exit;
					if(count($rideInfo)>0){						
					 	list($lat,$lng) =explode(',', $rideInfo[0]['sr_lat_lng_source']);
					} else {
						$lat = $lng = 0;
					}
					//echo $lat .''.$lng;
					$driverLoc = $this->Common_model->select('*',TB_DRIVER_LOCATION);
					//print_r($driverLoc);exit;
					foreach ($driverLoc as $key => $value) {
						$getDriverLoc = $this->distance($value['loc_lat'], $value['loc_long'], $lat, $lng, "M");
						//echo '1 '.$value['loc_lat'].' 2 '.$value['loc_long'].' 3 '.$lat.' 4 '.$lng."<br/>";
						//echo $getDriverLoc."<br/>";
						if( round($getDriverLoc) <= 5 ){
							$insertArr =array(
								"sched_id" =>$rideInfo[0]['id'],
								"driver_id"=>$value['user_id'],
								"created_date"=>date("Y-m-d H:i:s"),
								"ride_status" =>"Request Sent"
							); 
							$result = $this->Common_model->insert(TB_DRIVER_REQUEST,$insertArr);	
							$updateArr = array('admin_status'=>2,"driver_id"=>$value['user_id']);
							$updateStatus = $this->common->update(TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])),$updateArr);
							if($result && $updateStatus){
								echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Ride request has been ".$st)); exit;	
							} else {
								echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Request ride has been failed.")); exit;	
							}			

						}else{								
							echo json_encode(array("status"=>"error","action"=>"update","msg"=>"No driver available within 5 miles.")); exit;	
						}
					}
				}
				else
				{
					//echo "udpdate";exit;
					$updateArr = array('admin_status'=>$statusData);
					$insertId = $this->common->update(TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])),$updateArr);
					if($insertId)
					{
						echo json_encode(array("status"=>"success","msg"=>"Ride request has been ".$st)); exit;
					}
					else
					{
						echo json_encode(array("status"=>"error","msg"=>"Something went wrong, Please try again...!!!")); exit;
					}
				}
			}
			
		}
	}

	public function getScheduleRide()
	{
		if(is_ajax_request())
		{
			if(is_user_logged_in())
			{
				$postData = $this->input->post();				
				$pageData = $this->common->selectQuery("*",TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['key'])));
				
				if($pageData)
				{
					echo json_encode(array("status"=>"success","pageData"=>$pageData[0])); exit;
				}
				else
				{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong, Please try again...!!!")); exit;
				}
			}
			else
			{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}		
	}

	

	public function saveRideStatus(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$message ="";
				$postData = $this->input->post();
				if($postData['admin_status']==1)
					$message ="Open";
				elseif ($postData['admin_status']==2)
					$message ="Accepted";
				elseif ($postData['admin_status']==3)
					$message ="Closed";
				elseif ($postData['admin_status']==4)
					$message ="Rejected";
				else
					$message ="Invalid";
				$checkStatus = $this->Common_model->select('id,admin_status',TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['user_key']),'admin_status'=>$postData['admin_status']));
				//print_r($checkStatus);exit;
				if(!empty($checkStatus)){
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Schedule ride already accepted.")); exit;	
				}
				else{
					if ($postData['admin_status'] ==2 ){
						$rideInfo = $this->Common_model->select('id,sr_lat_lng_source,user_id',TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['user_key'])));
						if(count($rideInfo)>0){						
						 	list($lat,$lng) =explode(',', $rideInfo[0]['sr_lat_lng_source']);
						} else {
							$lat = $lng = 0;
						}
						$driverLoc = $this->Common_model->select('*',TB_DRIVER_LOCATION);
						foreach ($driverLoc as $key => $value) {
							$getDriverLoc = $this->distance($value['loc_lat'], $value['loc_long'], $lat, $lng, "M");
							if( round($getDriverLoc) <= 5 ){
								$insertArr =array(
									"sched_id" =>$rideInfo[0]['id'],
									"driver_id"=>$value['user_id'],
									"created_date"=>date("Y-m-d H:i:s"),
									"ride_status" =>"Request Sent"
								); 
								$result = $this->Common_model->insert(TB_DRIVER_REQUEST,$insertArr);	
								$updateArr = array('admin_status'=>$postData["admin_status"]);
								$updateStatus = $this->common->update(TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['user_key'])),$updateArr);
								if($result && $updateStatus){
									echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Your ride accepted successfully.")); exit;	
								} else {
									echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Request ride has been failed.")); exit;	
								}			

							}else{								
								echo json_encode(array("status"=>"error","action"=>"update","msg"=>"No driver available within 5 miles.")); exit;	
							}
						}
					} else {
						$updateArr = array('admin_status'=>$postData["admin_status"]);
						$updateStatus = $this->common->update(TB_SCHEDULE_RIDE,array('id'=>$this->encrypt->decode($postData['user_key'])),$updateArr);
					    if($updateStatus){
							echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Schedule ride has been ".$message)); exit;	
						}else{
							echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
						}
					}
				}
			}
		}
	}	
}
