<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FAQ extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	// default method call
	public function index()
	{		
		if(is_user_logged_in())
		{
			// view page of FAQ
			$this->load->view('faq/listFaq');
		}else{
			redirect("login");
			exit;
		}
	}

	public function listFaq(){
		if(is_ajax_request())
		{
			if(is_user_logged_in()){// user is logged in check
				// check get data from user  
				$postData = $this->input->post();
				$arrayColumn = array("id"=>"id","question"=>"question" ,"answer"=>"answer");
				$arrayStatus["is_active"] = array();
				$arrayColumnOrder = array("ASC","asc","DESC","desc");				
				$result = pagination_data($arrayColumn,$arrayStatus,$postData,$arrayColumnOrder,'id','question'
					,TB_FAQ,'*','listFaq');               
				$rows = '';
				if(!empty($result['rows']))
				{
					$i=1;
					foreach ($result['rows'] as $faqs) {
						$faq_id = $this->encrypt->encode($faqs['id']);
						$string = strip_tags($faqs['answer']);
						// charater should be 250 characters
						if (strlen($string) > 250) {						
						$stringCut = substr($string, 0, 250);
						$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
						}
						// fetch all recors from table
						$rows .= '<tr id="'.$faq_id.'">
	                            <td class="text-left">'.$faqs['question'].'</td>
	                           
	                            <td class="text-left">'.$string.'</td>
	                            <td class="text-left">
	                            	<a data-id="'.$i.'" data-row-id="'.$faq_id.'" class="" onclick="getFaq(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-check-square-o"></i>
									</a>
	                                <a data-id="'.$i.'" data-row-id="'.$faq_id.'" class="" onclick="deleteFaq(this)" href="javascript:void(0)">
										<i class="fa fa-fw fa-close"></i>
									</a>
	                            </td>
	                        </tr>';
					}
				}
				else
				{
					$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
				}
				// count rows
				$data["rows"] = $rows;
				$data["pagelinks"] = $result["pagelinks"];
				$data["entries"] = $result['entries'];
				$data["status"] = "success";

				echo json_encode($data);
				
			}else{
				echo json_encode(array("status"=>"logout"));
			}
		}
	}

	public function getFaq(){
		if(is_ajax_request()) // check FAQ is ajax request
		{
			if(is_user_logged_in()){ // check user is logged in
				$postData = $this->input->post();
				$pageData = $this->common->selectQuery("*",TB_FAQ,array('id'=>$this->encrypt->decode($postData['key'])));
				// show all records of FAQ
				if($pageData){
					echo json_encode(array("status"=>"success","pageData"=>$pageData[0])); exit;
				}else{
					echo json_encode(array("status"=>"error","msg"=>"Something went wrong, Please try again...!!!")); exit;
				}
			}else{
				echo json_encode(array("status"=>"logout","msg"=>"User has been logout.")); exit;
			}
		}
		
	}

	public function saveFaq(){ // save record
		if(is_ajax_request()) 
		{
			if(is_user_logged_in()){ // check user is logged in
				// user get records
				$postData = $this->input->post();			
				$dateTime = date("Y-m-d H:i:s");

    if(trim($postData["Question"])=="" || trim($postData["answer"])=="" )
    {


echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please Add Question and answer properly")); exit;	

    }



				$insertArr = array(
								'question'=>trim($postData["Question"]),
								"answer"=>trim($postData["answer"]),
								"updated_at" => $dateTime,								
								);
				if($postData["page_key"]){


					// 1 record update
					$insertId = $this->common->update(TB_FAQ,array('id'=>$this->encrypt->decode($postData['page_key'])),$insertArr);
					
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"FAQ's has been updated successfully")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}else{
					// 1 record insert in FAQ
					$insertId = $this->common->insert(TB_FAQ,$insertArr);
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"add","msg"=>"FAQ's has been added successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}

    }


   public function deletefaq(){// delete FAQ
		if(is_ajax_request())
		{
			if(is_user_logged_in()){
				$postData = $this->input->post();				
				$dateTime = date("Y-m-d H:i:s");					
				if($postData["key"]){			
					// record delete by id 		
					$insertId = $this->common->delete(TB_FAQ,array('id'=>$this->encrypt->decode($postData['key'])));
					if($insertId){
						echo json_encode(array("status"=>"success","action"=>"update","msg"=>"Faq's question and answer has been deleted successfully.")); exit;	
					}else{
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>"Please try again.")); exit;	
					}
				}
			}
		}

    }


}
