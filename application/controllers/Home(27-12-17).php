<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url','html'));
		$this->load->database();
		$this->load->library("email");
		$this->load->model("common_model"); 
		$this->load->library("User_AuthSocial");	
		$this->load->library("email");
        $this->load->library("User_AuthSocial");
        $this->load->helper("email_template");
        $this->load->helper('string');
        $this->load->library("image_lib");
        $this->load->helper('security');
        $this->load->helper('string');
	}

	public function change_user_password()
	{		
		$this->load->view('frontend/user/change_password');		
	}



public function accountverify($abc)
{	   
	$userData = $this->common->select("*",TB_USERS,array("token"=>trim($abc)));  
	if ($userData)
	{
		$token=  random_string('alnum',20);	
		$cond1 = array('id' =>  $userData[0]['id']); 
		$datareset = array('status' =>  'active','token'=>$token); 
		$sucess= $this->common->update(TB_USERS,$cond1,$datareset);
		$this->session->set_flashdata('Your account has been activated successfully. You can now login', 'msg');
	    $data= $this->session->set_flashdata('forgot_success_msg', 'Your account has been activated successfully. You can now login');		redirect("frontend-login",$data);    
	}
	else
	{	
		$data = $this->session->set_flashdata('error', 'This link has been expired');	
		redirect("frontend-login",$data); 		
	}
}

function resetUserPassword($id){

	
		    $postData = $this->input->post();


		    $cond1 = array('email' =>  trim($postData['uemail'])); 
		    $userData = $this->common->select("*",TB_USERS,$cond1 );  
		//print_r( count($userData));die;
                        if($postData['email']=="" )
			{

			echo json_encode(array("status" => "error","message"=>"please Enter valid email"));  exit;

			}
                 else if(count($userData)==0)
			{
			echo json_encode(array("status" => "error","message"=>"email id not found"));  exit;
			                       
			}
                        else if($postData['password']=="" )
			{

			echo json_encode(array("status" => "error","message"=>"Please Enter valid password"));  exit;

			}
                       else if($postData['confpwd']=="" )
			{

			echo json_encode(array("status" => "error","message"=>"Please Enter valid confirm password"));  exit;

			}
			else if($postData['password']!==  $postData['confirmPassword'])
			{

			echo json_encode(array("status" => "error","message"=>"Password and confirm password not match"));  exit;

			}
          
              if($postData['key']=="" )
			{

			echo json_encode(array("status" => "error","message"=>"Expire token for reset password"));  exit;

			}

                     //  $data['messagereset'] = $this->session->flashdata('messagereset'); 
			$data['title']  = 'Reset Password';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$cond1 = array('email' =>  trim($postData['email']) ,'token'=> trim($postData['key']) ,"status"=>"active"); 
                        $userData = $this->common->select("*",TB_USERS,$cond1 );
			// echo $this->db->last_query();die;    
			//   print_r($userData) ;die; 
			if($userData){
		
				$key  =$postData['key']; 
				$useremail    = trim($this->input->post('uemail'));
				$password = md5($this->input->post('password'));
				$datareset = array('password' => $password);

				$cond = array('email' => $useremail, 'token' => trim($postData['key']));				
				$userData = $this->common->select("*",TB_USERS,$cond);
		     
		       		  
				if(count($userData) > 0)
				{	  
					$this->common->update(TB_USERS,$cond,$datareset);	
				
					$cond = array('email' => $useremail,'password' => $password);			       
					 $res = $this->common->select("email,password",TB_USERS,$cond);  
 
				if ( $res !== false ) {
						
						 $cond1 = array('email' => $useremail);
						 $datareset = array('token' => '');
	                               $this->common->update(TB_USERS,$cond,$datareset); 		
				//echo $this->db->last_query();die; 	
				
                   echo json_encode(array("status" => "sucess","message"=>"You have successfully reset the password."));
                     $this->load->view('frontend/reset_password', $data);exit;
				} 

				else{ 
	
				$data['validation_message'] = "The key is invalid or You have already reset the password.";
				echo json_encode(array("status" => "error","message"=>"The key is invalid or You have already reset the password.","path"=>"home"));  
				
				} 

				
            }
            else{
              //  $data['validation_message'] = (validation_errors() ? validation_errors() : $this->session->set_flashdata('message'));
                $this->load->view('frontend/change_password', $data);
            }
        }
        
    }
    
 

// public function emailverify($data)
// 	{
		

		
		
		
// 	}



    

    public function logout(){
       $this->session->sess_destroy();
        redirect("home"); exit;
    }





}

