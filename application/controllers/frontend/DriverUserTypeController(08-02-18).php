<?php if(!defined("BASEPATH")) exit("No direct script access allowed");
class DriverUserTypeController extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->load->library('upload');	
		$this->load->helper('file');
		$this->load->helper(array('form', 'url'));
	}

	// default driver dashboard
	public function index()
	{		
		if($this->session->userdata('user_id'))
		{	
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			//$data['myRideCount'] = $this->Common_model->group_by("sr_status,count(*) total",TB_SCHEDULE_RIDE,array('sr_status'),array('user_id'=>$this->session->userdata('user_id'))); 
			//$data['userDetails']=$this->Common_model->select('picture,full_name,email',TB_USERS,array('id'=>$this->session->userdata('user_id')));
			$this->load->view("driverUser/driverDashBoard",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	// driver my profile
	public function driver_my_profile()
	{		
		if($this->session->userdata('user_id'))
		{	
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			$this->load->view("driverUser/driverProfile",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	public function driver_reset_pwd()
	{
		if($this->session->userdata('user_id'))
		{	
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			$postData = $this->input->post();
			$this->form_validation->set_rules('change_pwd', 'Password', 'required');
			$this->form_validation->set_rules('confirm_pwd', 'Confirm Password', 'required|matches[change_pwd]');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
			if ($this->form_validation->run() == FALSE)
			{ 	
				$this->load->view("driverUser/change_pwd",$data);		
			}
			else
			{	
				$updateData = array('password' =>md5($postData['change_pwd']));
				$where = array('id' =>$this->session->userdata('user_id')); 
				$result = $this->Common_model->update(TB_USERS,$where,$updateData); 
				if($result)
				{
					$this->session->set_flashdata('success', 'Your passowrd has been changed successfully.');
					$this->load->view("driverUser/change_pwd",$data);		
				}
				else
				{
					$this->session->set_flashdata('fail', 'Your passowrd has not been changed successfully.');
					$this->load->view("driverUser/change_pwd",$data);		    	
				}    			
			}					
		} 
		else 
		{
			redirect("frontend-login"); exit;
		}
	}

	// driver my trip assignments
	public function driver_my_trip_assignment()
	{		
		if($this->session->userdata('user_id'))
		{	
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			$this->load->view("driverUser/driverMyTripAssignment",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	// driver my trip request
	public function driver_my_trip_request()
	{		
		if($this->session->userdata('user_id'))
		{				
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section

			//---------- ride request ---------
			$cond = array("tbl_driver_request.driver_id" => $this->session->userdata('user_id'),"tbl_schedule_ride.driver_status"=>"1");
			$jointype=array("tbl_schedule_ride"=>"LEFT","tbl_users"=>"LEFT");
			$join = array("tbl_schedule_ride"=>"tbl_schedule_ride.id = tbl_driver_request.sched_id",
				"tbl_users"=>"tbl_users.id = tbl_schedule_ride.user_id");
			$data['ride_request'] = $this->Common_model->selectQuery('*,tbl_schedule_ride.id as sched_id,tbl_schedule_ride.sr_trip_id',TB_DRIVER_REQUEST,$cond,array("sr_date"=>"DESC"),$join,$jointype); 
			//----- ashwini code to accept request --------
			$acceptquery=$this->db->query('select tbl_schedule_ride.id,tbl_schedule_ride.sr_trip_id,CONCAT(DATE_FORMAT(tbl_schedule_ride.sr_date, "%d %b %Y")," ",tbl_schedule_ride.sr_pick_up_time ) as datefrmt,tbl_users.full_name,tbl_schedule_ride.driver_status FROM tbl_schedule_ride LEFT JOIN tbl_users ON tbl_users.id = tbl_schedule_ride.user_id WHERE tbl_schedule_ride.driver_id="'.$this->session->userdata('user_id').'" AND tbl_schedule_ride.driver_status IN (2,3)');
			$data['ride_request_accepted'] = $acceptquery->result_array();	
			$closedCond = array("tbl_schedule_ride.driver_id" => $this->session->userdata('user_id'),"tbl_schedule_ride.driver_status"=>"4");
			$closedJointype=array("tbl_users"=>"LEFT");
			$closedJoin = array("tbl_users"=>"tbl_users.id = tbl_schedule_ride.driver_id");
			$data['ride_request_closed'] = $this->Common_model->selectQuery('tbl_schedule_ride.id,tbl_schedule_ride.sr_trip_id,CONCAT(DATE_FORMAT(tbl_schedule_ride.sr_date, "%d %b %Y")," ",tbl_schedule_ride.sr_pick_up_time ) as datefrmt,tbl_users.full_name',TB_SCHEDULE_RIDE,$closedCond,array(),$closedJoin,$closedJointype); 
			$this->load->view("driverUser/driverMytripRequest",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

		// driver accepeted ride request
	public function driver_accepted_request()
	{
		$postData = $this->input->post();	
			// ------ check if already accepted ------------
		$cond= array("id" => $postData['tripID'],'driver_status'=>2);
		$checkexistsq = $this->Common_model->select("*",TB_SCHEDULE_RIDE,$cond);
			/*print_r($postData);
			print_r($checkexistsq);exit;*/
			if(count($checkexistsq) > 0)
			{
				$this->output
				->set_content_type("application/json")
				->set_output(json_encode(array("status"=>false,"message"=>"Ride is booked by another driver.")));
			}else
			{
				//--------- accept the request ----------
				$updateData = array('driver_id' => $postData['driverID'],'driver_status'=>2,'admin_status'=>2,
					'updated_on'=>date("Y-m-d H:i:s"));
				$where = array('id' =>$postData['tripID']); 
				$result= $this->Common_model->update(TB_SCHEDULE_RIDE,$where,$updateData);	 
				//----------- trip details----------------------------------
				$tripDetailsCond= array("id" => $postData['tripID']);
				$tripDetails = $this->Common_model->select("user_id,sr_trip_id,sr_date,sr_pick_up_time,sr_total_distance,source_address,destination_address",TB_SCHEDULE_RIDE,$tripDetailsCond);
				$notificationArr = array(
									"n_title" =>"Trip request accepted",
									"n_full_description"=>"Please check your trip request.<br/> Trip ID :<b>".$tripDetails[0]['sr_trip_id']."</b><br />Schedule ride date :<b>".$tripDetails[0]['sr_date']."</b> <br />Pick up time :<b>".$tripDetails[0]['sr_pick_up_time']."</b><br />Total distance :<b>".$tripDetails[0]['sr_total_distance']."</b> <br />Source location :<b>".$tripDetails[0]['source_address']."</b><br /> Destination location: <b>".$tripDetails[0]['destination_address']."</b>",
									"notification_flag"=>2,
									"read_or_unread" =>1,
									"n_status"=>'ME',
									"user_id" =>$tripDetails[0]['user_id'],
									"created_on" => date("Y-m-d H:i:s"),
									);
				$notifResult = $this->Common_model->insert(TB_NOTIFICATION,$notificationArr);
				if($result && $notifResult)
				{
					$where = array('sched_id' =>$postData['tripID']); 
					$result = $this->Common_model->delete(TB_DRIVER_REQUEST,$where);
					$this->output
					->set_content_type("application/json")
					->set_output(json_encode(array("status"=>true,"message"=>"Ride request has been accepeted successfully.")));
				}
				else
				{
					$this->output
					->set_content_type("application/json")
					->set_output(json_encode(array("status"=>false,"message"=>"Ride request has been failed.")));
				}
			}
		}	


		//------- Reject request -------------
		public function driver_reject_request()
		{
			$postData = $this->input->post();
			$where = array('sched_id' =>$postData['tripID'],'driver_id'=>$postData['driverID']); 
			$result = $this->Common_model->delete(TB_DRIVER_REQUEST,$where);
			if($result)
			{
				$this->output
				->set_content_type("application/json")
				->set_output(json_encode(array("status"=>true,"message"=>"Ride request has been rejected successfully.")));
			}else
			{
				$this->output
				->set_content_type("application/json")
				->set_output(json_encode(array("status"=>false,"message"=>"Oops!!! Somthing went wrong.")));
			}
		}

		public function get_accepted_ride_details()
		{
			$id = $this->input->post("id");	
			$data['rideAccepetedDetails'] = $this->Common_model->select('*',TB_SCHEDULE_RIDE,array('id'=>$id));	
			$getAcceptedRideDetailsQuery = $this->db->query("SELECT sr.sr_date,sr.sr_pick_up_time,sr.sr_total_distance,sr.source_address,sr.destination_address,sr.duration,u.full_name FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.id = $id");
			$data = $getAcceptedRideDetailsQuery->result_array();
		//print_r($data);exit;
			if($data)
			{
				$this->output
				->set_content_type("application/json")
				->set_output(json_encode(array("status"=>true,'data'=>$data)));
			}
			else{
				$this->output
				->set_content_type("application/json")
				->set_output(json_encode(array("status"=>false,'message'=>"Accepted request ride view has been failed to show.")));
			}
		}

	// driver my bills
		public function driver_my_bills()
		{		
			if($this->session->userdata('user_id'))
			{	
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			$this->load->view("driverUser/driverMyBill",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	// driver notifications
	public function driver_notifications()
	{		
		if($this->session->userdata('user_id'))
		{	
			$user_id =$this->session->userdata('user_id');
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['notification'] = $this->Common_model->selectQuery("*",TB_NOTIFICATION,array('user_id'=>$user_id),array('id desc'=>'order by'));
			$totalNotif = $this->Common_model->select("count(*) AS notification",TB_NOTIFICATION,array('user_id'=>$user_id));
			if($totalNotif[0]['notification']>=1)
			{				
				$flagReset = array('notification_flag' => 1);
				$where = array('id' =>$user_id); 
				$this->Common_model->update(TB_NOTIFICATION,$where,$flagReset);	    		
			}			
			$this->load->view("driverUser/driverNotification",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	public function notificationRead() { 
		$notificationId = $this->input->post("id");	
		$user_id =$this->session->userdata('user_id');
		
		$flagReset = array('notification_flag' => 1);
		$where = array('id' =>$notificationId); 
		$result = $this->Common_model->update(TB_NOTIFICATION,$where,$flagReset);
		
		if($result){
			$totalNotif = $this->Common_model->select("count(*) AS notification",TB_NOTIFICATION,array('user_id'=>$user_id,'notification_flag'=>'2'));
			echo json_encode(array('count'=>$totalNotif[0]['notification'],'status'=>'true')); die;
			 //echo 'true';	
		}else{
			echo 'false'; die;
		}
	}

	//delete notification 
	public function driver_delete_notification()
	{
		$deleteNotifID = $this->input->post("id");				
		$where = array('id' =>$deleteNotifID); 
		$result = $this->Common_model->delete(TB_NOTIFICATION,$where);
		if($result)
		{
			$this->output
			->set_content_type("application/json")
			->set_output(json_encode(array("status"=>true,"message"=>"Notification has been deleted successfully.")));
		}	
		else
		{
			$this->output
			->set_content_type("application/json")
			->set_output(json_encode(array("status"=>false,"message"=>"Notification has not been deleted successfully.")));
		}
	}

	// ------ show driver requests as notifications ---------
	public function getUserNotifications()
	{
		if($this->session->userdata('user_id'))
		{
			$cond = array("is_read"=>"0","driver_id"=>$this->session->userdata('user_id'));
			$getreq = $this->Common_model->select("*",TB_DRIVER_REQUEST,$cond);			
			$this->output
			->set_content_type("application/json")
			->set_output(json_encode(array("status"=>"success","data"=>count($getreq))));
		}
	}
}