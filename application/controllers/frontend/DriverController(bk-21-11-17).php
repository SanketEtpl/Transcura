<?php if(!defined("BASEPATH")) exit("No direct script access allowed");
class DriverController extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}

	// default driver dashboard
	public function index()
	{		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
		$data['myRideCount'] = $this->Common_model->group_by("sr_status,count(*) total",TB_SCHEDULE_RIDE,array('sr_status'),array('user_id'=>1)); 
		$data['userDetails']=$this->Common_model->select('picture,full_name,email',TB_USERS,array('id'=>1));
		$data['totalCount'] =$this->db->count_all_results(TB_SCHEDULE_RIDE, FALSE);	
		$this->load->view("frontDrivers/driverDashBoard",$data);		
	}

	// driver profile view
	public function driver_profile()
	{
		$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
		$data['getDriverDetails'] = $this->Common_model->select("*",TB_USERS,array('id'=>1)); // get driver records
		$query = $this->db->query("SELECT ct.city_name,s.state_name,c.country_name FROM tbl_city as ct 
								   left join tbl_states as s on ct.state_id = s.id
								   left join tbl_country as c on c.id = s.country_id
								   where ct.id=".$data['getDriverDetails'][0]['city']."");
		$data['countryDetails']=$query->result_array();		
		$this->load->view("frontDrivers/driverProfile",$data);
	}

	// driver edit profile
	public function driver_edit_profile()
	{
		$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
		$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);
		$data['getDriverDetails'] = $this->Common_model->select("*",TB_USERS,array('id'=>1));
		$this->load->view("frontDrivers/driverEditProfile",$data);
	}

	// changes in driver edit profile
	public function update_driver_edit_profile()
	{
		$postData = $this->input->post();
		$this->form_validation->set_rules('full_name', 'full name', 'trim|required|alpha|min_length[3]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('date_of_birth','date of birth','trim|required|xss_clean');
		/*$this->form_validation->set_rules('last_name', 'last name', 'trim|required|alpha|min_length[3]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('email', 'email', 'trim|required|max_length[50]|valid_email|xss_clean');
		$this->form_validation->set_rules('phone', 'phone', 'trim|required|max_length[20]|min_length[10]|integer|xss_clean');
		$this->form_validation->set_rules('message', 'message', 'trim|required|xss_clean');		*/
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
		if( $this->form_validation->run() == FALSE )
        {  			        	
			$this->load->view('frontDrivers/driverEditProfile');				
		}
		else
		{ 
			echo "success";exit;
		}

	}

	public function driver_schedule_an_ride()
	{
		$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
		$this->load->view("frontDrivers/driverScheduleAnRide",$data);
	}
	public function driver_schedule_an_trip_details()
	{
		$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
		$this->load->view("frontDrivers/driverScheduleAnTripDetails",$data);
	}
	public function driver_my_schedule_rides()
	{
		$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
		$this->load->view("frontDrivers/driverMyScheduleRides",$data);
	}

	//get state name
	public function get_state()
	{		
		$postData = $this->input->post();
		$countryID=array('country_id'=>$postData['countryID']);
		$dataState = $this->Common_model->select("*",TB_STATE,$countryID);
		$stateJSON='';
		if($dataState > 0)
		{
			$stateJSON .= '<option value="">Select state</option>';
       		foreach ($dataState as $key => $state_name) {
       			$stateJSON.= '<option value="'.$state_name['id'].'">'.$state_name['state_name'].'</option>';
       		}       		
		}
		else
		{
			$stateJSON.= '<option value="">State not available</option>';
		}
		echo json_encode(array('state'=>$stateJSON));		
	}

	//get city name
	public function get_city()
	{
		$postData = $this->input->post();
		$stateID = array('state_id'=>$postData['state_id']);
		$dataCity = $this->Common_model->select("*",TB_CITY,$stateID);
		$cityJSON = '';
		if($dataCity > 0)
		{
			$cityJSON .= '<option value="">Select city</option>';
			foreach ($dataCity as $key => $city) {
				$cityJSON.= '<option value="'.$city['id'].'">'.$city['city_name'].'</option>';
			}
		}
		else
		{
			$cityJSON .= '<option value="">City not available</option>';
		}
		echo json_encode(array('city'=>$cityJSON));
	}

	public function driver_notification()
	{
		$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
		$this->load->view("frontDrivers/driverNotification",$data);
	}
	public function driver_my_ride()
	{
		$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
		$this->load->view("frontDrivers/driverMyRides",$data);	
	}
}