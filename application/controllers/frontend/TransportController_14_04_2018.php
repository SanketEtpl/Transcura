<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed'); 

class TransportController extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_model');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		$this->load->helper('security');
		$this->load->helper('email_template_helper');
		$this->load->library('upload');		
		$this->load->helper('string');
		$this->load->helper('common');
	}

	// ----- transport dashboard -----
	public function index()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
			$data['userDetails']=$this->Common_model->select('picture,full_name,email',TB_USERS,array('id'=>$this->session->userdata('user_id')));
			$this->load->view('frontTransport/transport_dashboard',$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}	
	}

	// ----- transport profile -------
	public function transport_profile()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
			$category='';
			$cond = array(TB_USERS.".id" => $this->session->userdata('user_id'));
			$jointype=array(TB_COUNTRY=>"LEFT",TB_CITY=>"LEFT",TB_STATE=>"LEFT",TB_DOCTOR_SEARCH_BY_CAT_LOC=>"LEFT");
			$join = array(
				TB_COUNTRY=>TB_COUNTRY.".id = ".TB_USERS.".country",
				TB_STATE=>TB_STATE.".id = ".TB_USERS.".state",
				TB_CITY=>TB_CITY.".id = ".TB_USERS.".city");
			$select = "full_name,phone,email,date_of_birth,picture,zipcode,username,mobile,emergency_contactno,emergency_contactname,signature,state_name,country_name,city_name,county,street";
			$data['transport_profile'] = $this->Common_model->selectQuery($select,TB_USERS,$cond,false,$join,$jointype); 
			$this->load->view("frontTransport/transport_profile",$data);

		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	// --------- registered-driver-listing --------
	public function driver_listing()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
			$data['driverlist'] = $this->Common_model->select('*',TB_USERS,array('transport_parent_id'=>$this->session->userdata('user_id')));

			
			$this->load->view("frontTransport/driver_listing",$data);

		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	//------ add driver ------------
	public function add_diver()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
			$data['country'] = $this->Common_model->select('*',TB_COUNTRY,array('id'=>231));
			$data['stateData'] = $this->Common_model->select('*',TB_STATE,array('country_id'=>231));
			$this->load->view("frontTransport/register_driver",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}	
	}

	//-------- save driver ---------
	public function save_driver()
	{
		if($this->session->userdata('user_id'))
		{
			$postData = $this->input->post();
			$token=  random_string('alnum',20);	
			$insertArr = array(
				'user_token' => trim($token),
				'user_type' => trim($postData['usertype']),								
				'transport_parent_id' => $this->session->userdata('user_id'),								
				'full_name'  => trim($postData['fullname']),
				'email'      => trim($postData['email']),
				'password'   => trim(md5($postData['password'])),								
				'actual_password'   => trim($postData['password']),								
				'token'      => $token,
				'username'	 =>	trim($postData['username1']),					 
				'created_at' => date('Y-m-d H:i:s'),
				'status'     => "active",
				'phone'      => trim($postData['phone_no']),
				'done'		 => 1
				);
			$result = $this->Common_model->insert(TB_USERS,$insertArr);	

			if($result)
			{

				// Email to registration
				$hostname = $this->config->item('hostname');
				$config['mailtype'] ='html';
				$config['charset'] ='iso-8859-1';
				$this->email->initialize($config);
				$from  = EMAIL_FROM; 
				$email = trim($postData['email']);
				$this->messageBody  = email_header();
				$base_url = $this->config->item('base_url');
				// $reset_url = $base_url."accountverify/".$token ;			
				$this->messageBody  .= '<tr>
				<td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
					<p>Dear Customer,</p>
					<p>
						You have registered successfully on Transcura.<br> Following are your details : </p>
						<p>Email ID : '.trim($postData['email']).'</p>
						<p>Password : '.trim($postData['password']).'</p>
						<p>
							<p>Link : </p>
							<a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href='.$base_url.'>Click Here</a>
						</p><br /><br />
						<p>If the above link does not work, please copy and paste the following link into your browser.</p>
						<p>Copy the Link : </p>
						<a href="'.$base_url.'">'.$base_url.'</a>
					</p>					
				</td>
			</tr>'; 			
			$this->messageBody  .= email_footer();
			$this->email->from($from, $from);
			$this->email->to($email);
			$this->email->subject('Welcome to Transcura');
			$this->email->message($this->messageBody);
			$this->email->send();

				// Email to registration
				// 	$hostname = $this->config->item('hostname');
				// 	$config['mailtype'] ='html';
				// 	$config['charset'] ='iso-8859-1';
				// 	$this->email->initialize($config);
				// 	$from  = EMAIL_FROM; 
				// 	$name = trim($postData['fullname']);
				// 	$email = trim($postData['email']);
				// 	$userType = $postData['usertype'];
				// 	$this->messageBody  = email_header();
				// 	$base_url = $this->config->item('base_url');
				// 	$reset_url = $base_url."accountverify/".$token ;			
				// 	$this->messageBody  .= '<tr>
				// 	<td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
				// 		<p>Dear Customer,</p>
				// 		<p>
				// 			You have registered successfully on Transcura.<br> Please click the link given below to verify your email address</p>
				// 			<p>
				// 				<p>Activation Link : </p>
				// 				<a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href='.$reset_url.'>Click Here</a>
				// 			</p><br /><br />
				// 			<p>If the above link does not work, please copy and paste the following link into your browser.</p>
				// 			<p>Copy the Link : </p>
				// 			<a href="'.$reset_url.'">'.$reset_url.'</a>
				// 		</p>					
				// 	</td>
				// </tr>'; 			
				// $this->messageBody  .= email_footer();
				// $this->email->from($from, $from);
				// $this->email->to($email);
				// $this->email->subject('Welcome to Transcura');
				// $this->email->message($this->messageBody);
				// $this->email->send();
			echo json_encode(array('status' => 'success','message'=>'Driver addedd successfully.'));
		}else
		{
			echo json_encode(array('status' => 'error','message'=>'Oops!!! Something went wrong.'));
		}
	}
	else
	{
		redirect("frontend-login"); exit;
	}
}

	//--------- My Trips Listing --------
public function transport_trips()
{
	if($this->session->userdata('user_id'))
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); 

			//---------- scheduled ride request ---------
		$cond = array(TB_DRIVER_REQUEST.".driver_id" => $this->session->userdata('user_id'),TB_SCHEDULE_RIDE.".driver_status"=>"1");
		$jointype=array(TB_SCHEDULE_RIDE=>"LEFT",TB_USERS=>"LEFT");
		$join = array(TB_SCHEDULE_RIDE=>TB_SCHEDULE_RIDE.".id = ".TB_DRIVER_REQUEST.".sched_id",
			TB_USERS=>TB_USERS.".id = ".TB_SCHEDULE_RIDE.".user_id");
		$data['ride_request'] = $this->Common_model->selectQuery('*,'.TB_SCHEDULE_RIDE.'.id as sched_id,sr_trip_id',TB_DRIVER_REQUEST,$cond,array("sr_date"=>"DESC"),$join,$jointype); 

			//--------- accepted ride trip ----------
		$child_driverq = $this->Common_model->select('id,full_name',TB_USERS,array('transport_parent_id'=>$this->session->userdata('user_id')));
		foreach ($child_driverq as $drvalue) {
			$alldriver[] = $drvalue['id'];
		}			
		$eachdriver = implode(",",$alldriver);
		$where = "driver_id IN (".$eachdriver.") AND driver_status IN (2,3)";
		$join = array(TB_USERS." as u" => TB_SCHEDULE_RIDE . '.user_id=u.id',
			TB_USERS." as u1" => TB_SCHEDULE_RIDE . '.driver_id=u1.id');
		$select = "u.full_name as customer_name,u1.full_name as driver_name,sr_trip_id,sr_date,".TB_SCHEDULE_RIDE.".id as sched_id,driver_status";
		$data['accepted_ride'] = $this->Common_model->getMaster(TB_SCHEDULE_RIDE, $where,$join,$order = false, $field = false, $select,$limit=false,$start=false, $search=false);
		// echo $this->db->last_query();die;
		$this->load->view("frontTransport/transport_trip_listing",$data);
	}
	else
	{
		redirect("frontend-login"); exit;
	}
}

		// ----- show driver and trip details ----------
public function getDriver()
{
	if($this->session->userdata('user_id'))
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); 
		$schedId = strtr($_GET['sched_id'], '-_', '+/');
		$sch_id = $this->encrypt->decode($schedId);
		$data['ride_detail'] = $this->Common_model->select('id,sr_trip_id,sr_date,source_address,destination_address,duration,sr_total_distance',TB_SCHEDULE_RIDE,array('id'=>$sch_id));
		$data['driver'] = $this->Common_model->select('id,full_name',TB_USERS,array('transport_parent_id'=>$this->session->userdata('user_id')));		

			//------- check if already assigned ------
		foreach ($data['driver'] as $drvalue) {
			$alldriver[] = $drvalue['id'];
		}

		$eachdriver = implode(",",$alldriver);
		$checkq = $this->db->query("select * from ".TB_DRIVER_REQUEST." where sched_id=".$sch_id." AND driver_id IN (".$eachdriver.")");
		$data['checked_dr'] = $checkq->result_array();

		$this->load->view("frontTransport/show_driver",$data);
	}
	else
	{
		redirect("frontend-login"); exit;
	}
}

	//------- assign Driver To Trip --------
public function assignDriverToTrip()
{
	if($this->session->userdata('user_id'))
	{
		$postData = $this->input->post();
			//------- check for ride is not accepted by any other driver ----------
		$cond_ride = array("id"=>trim($postData['sched_id']),"driver_id != "=>"0","driver_status != "=>"1");
		$getStatus = $this->Common_model->select('*',TB_SCHEDULE_RIDE,$cond_ride);

		if(count($getStatus) > 0)
		{
			echo json_encode(array('status' => 'error','message'=>'Oops!!! This trip is already assigned to another driver. Please try another.'));
		}else
		{
				//-------- Update --------
			if(trim($postData['driver_req_id']) > 0)
			{
				$where = array('driver_req_id' =>trim($postData['driver_req_id'])); 
				$up_data=array("driver_id"=>trim($postData['check_driver']),'created_date'=>date("Y-m-d H:i:s"));
				$result = $this->Common_model->update(TB_DRIVER_REQUEST,$where,$up_data);
			}else
			{
				$insertArr = array(
					'sched_id' =>trim($postData['sched_id']),
					'driver_id'=>trim($postData['check_driver']),
					'created_date'=>date("Y-m-d H:i:s"),
					'ride_status'=>'Request Sent',
					'is_read'=> '0'
					);
				$result = $this->Common_model->insert(TB_DRIVER_REQUEST, $insertArr); 
			}

			if($result > 0)
			{
				echo json_encode(array('status' => 'success','message'=>'Driver assigned successfully.'));
			}else
			{
				echo json_encode(array('status' => 'error','message'=>'Oops!!! Something went wrong.'));
			}

		}
			// $all_driver = explode(",",$postData['check_driver']);
			// foreach ($all_driver as $eachdr) {
			// 	$insertArr[] = array(
			// 		'sched_id' =>trim($postData['sched_id']),
			// 		'driver_id'=>$eachdr,
			// 		'created_date'=>date("Y-m-d H:i:s"),
			// 		'ride_status'=>'Request Sent',
			// 		'is_read'=> '0'
			// 		);
			// }
			// $result = $this->db->insert_batch(TB_DRIVER_REQUEST, $insertArr); 
			// $result_cnt = $this->db->affected_rows();

	}
	else
	{
		redirect("frontend-login"); exit;
	}
}

	//------ edit transport profile -----
public function editTransportProfile()
{
	if($this->session->userdata('user_id'))
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$cond = array(TB_USERS.".id" => $this->session->userdata('user_id'));
		$jointype=array(TB_COUNTRY=>"LEFT",TB_CITY=>"LEFT",TB_STATE=>"LEFT");
		$join = array(
			TB_COUNTRY=>TB_COUNTRY.".id = ".TB_USERS.".country",
			TB_STATE=>TB_STATE.".id = ".TB_USERS.".state",
			TB_CITY=>TB_CITY.".id = ".TB_USERS.".city");
		$select = "full_name,phone,email,date_of_birth,picture,zipcode,username,mobile,emergency_contactno,emergency_contactname,signature,state,state_name,country_name,city,city_name,county,street,signature_file_upload";
		$data['transport_profile'] = $this->Common_model->selectQuery($select,TB_USERS,$cond,false,$join,$jointype); 

		$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY,array('id'=>231));
		$data['stateData'] = $this->Common_model->select("*",TB_STATE,array('country_id'=>231));
		$data['cityData'] = $this->Common_model->select("*",TB_CITY,array('state_id'=>$data['transport_profile'][0]['state']));
		$this->load->view("frontTransport/editTransportProfile",$data);
	}
	else
	{
		redirect("frontend-login"); exit;
	}
}

	//---------- update transport profile --------------
public function updateTransportProfile()
{
	if($this->session->userdata('user_id'))
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$cond = array(TB_USERS.".id" => $this->session->userdata('user_id'));
		$jointype=array(TB_COUNTRY=>"LEFT",TB_CITY=>"LEFT",TB_STATE=>"LEFT");
		$join = array(
			TB_COUNTRY=>TB_COUNTRY.".id = ".TB_USERS.".country",
			TB_STATE=>TB_STATE.".id = ".TB_USERS.".state",
			TB_CITY=>TB_CITY.".id = ".TB_USERS.".city");
		$select = "full_name,phone,email,date_of_birth,picture,zipcode,username,mobile,emergency_contactno,emergency_contactname,signature,state,state_name,country_name,city,city_name,county,street,signature_file_upload";
		$data['transport_profile'] = $this->Common_model->selectQuery($select,TB_USERS,$cond,false,$join,$jointype); 

		$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY,array('id'=>231));
		$data['stateData'] = $this->Common_model->select("*",TB_STATE,array('country_id'=>231));
		$data['cityData'] = $this->Common_model->select("*",TB_CITY,array('state_id'=>$data['transport_profile'][0]['state']));

		$this->form_validation->set_rules('full_name', 'Full Name', 'required');
		$this->form_validation->set_rules('date_of_birth', 'Date of Birth', 'required');
		$this->form_validation->set_rules('phone_no', 'Mobile Number', 'required');
		$this->form_validation->set_rules('state', 'State', 'required');
		$this->form_validation->set_rules('city', 'City', 'required');
		$this->form_validation->set_rules('zipcode', 'Zipcode', 'required');
		$this->form_validation->set_rules('county', 'County', 'required');
		$this->form_validation->set_rules('street', 'Street', 'required');
		$this->form_validation->set_rules('emergency_contact_name', 'Emergency contact name', 'required');
		$this->form_validation->set_rules('emergency_contact_number', 'Emergency contact number', 'required');
		$this->form_validation->set_rules('signature', 'Signature', 'required');

		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view("frontTransport/editTransportProfile",$data);
		}
		else
		{
			$perDoc=$signDoc="";
			$postData = $this->input->post();
			if(!empty($_FILES["sign_file_upload"]["name"]))   
			{   
				$file_name = $_FILES["sign_file_upload"]["name"];
				$original_file_name = $file_name;
				$random = rand(1, 10000000000000000);
				$date = date("Y-m-d-h:i:sa");
				$makeRandom = $random;
				$file_name_rename = $makeRandom;
				$explode = explode('.', $file_name);
				if(count($explode) >= 2) 
				{
					$new_file = $file_name_rename.$date.'.'.$explode[1];		            
					$config['upload_path'] = "./uploads/Healthcare/doc";
					$config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
					$config['file_name'] = $new_file;
					$config['max_size'] = '307210';
					$config['max_width'] = '300000';
					$config['max_height'] = '300000';
					$config['overwrite'] = TRUE;
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload("sign_file_upload")) 
					{
						$error = $this->upload->display_errors();
						$this->session->set_flashdata("error",$error." for signature");				
						redirect(current_url());
					}
					else
					{
						$signDoc = 'uploads/Transport/doc/'.$new_file;
					}
				}
			}			
			else if(isset($postData["sign_file_upload_old"]))   
			{          	 
				$signDoc = $postData["sign_file_upload_old"];		        				
			}
			else
			{
				$signDoc ="";	
			}

				//----- profile image -----
			if(!empty($_FILES["profile_image"]["name"]))   
			{   		    	
				$file_name = $_FILES["profile_image"]["name"];
				$original_file_name = $file_name;
				$random = rand(1, 10000000000000000);
				$date = date("Y-m-d-h:i:sa");
				$makeRandom = $random;
				$file_name_rename = $makeRandom;
				$explode = explode('.', $file_name);
				if(count($explode) >= 2) 
				{
					$new_file = $file_name_rename.$date.'.'.$explode[1];		            
					$config['upload_path'] = "./uploads/Transport/thumb";
					$config['allowed_types'] ="png|jpeg|jpg";
					$config['file_name'] = $new_file;
					$config['max_size'] = '307210';
					$config['max_width'] = '300000';
					$config['max_height'] = '300000';
					$config['overwrite'] = TRUE;
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload("profile_image")) 
					{
						$error = $this->upload->display_errors();
						$this->session->set_flashdata("error",$error." for profile picture");				
						redirect(current_url());
					}
					else
					{
						$profImage = 'uploads/Transport/thumb/'.$new_file;
					}
				}
			}
			else if(isset($postData["profile_image_old"]))   
			{          	 

				$profImage =$postData["profile_image_old"];		        				
			}
			else
			{

				$profImage ="";	
			}

				// ---- update data -------
			$updateArr  = array(
				'full_name'=>$postData['full_name'],
				'date_of_birth' => date("Y-m-d",strtotime($postData['date_of_birth'])),
				'phone' => $postData['phone_no'],	
				'street' => trim($postData['street']),
					// 'country' => $postData['country'],
				'state' => $postData['state'],
				'city'=>$postData['city'],
				'zipcode' => trim($postData['zipcode']),
				'county' => $postData['county'],											
				'emergency_contactname' => trim($postData['emergency_contact_name']),
				'emergency_contactno' => $postData['emergency_contact_number'],	
				'signature_file_upload'=>$signDoc,	
				'picture'=>$profImage,							
				'signature'=>$postData['signature'],								
				'updated_at'=>date("Y-m-d H:i:a")
				);
			$where = array('id' =>$this->session->userdata('user_id'));
			$result = $this->Common_model->update(TB_USERS,$where,$updateArr);
			if($result)
			{

				$this->session->set_flashdata("success","Profile updated successfully");
				redirect("transport-profile");
			}
			else
			{
				$this->session->set_flashdata("fail","Oops!!! Something went wrong.");				
				redirect(current_url());
			}	
		}
			// die;


			// $perDoc=$signDoc="";
			// $postData = $this->input->post();		
			// if(!empty($_FILES["sign_file_upload"]["name"]))   
			// {   
			// 	$file_name = $_FILES["sign_file_upload"]["name"];
			// 	$original_file_name = $file_name;
			// 	$random = rand(1, 10000000000000000);
			// 	$date = date("Y-m-d-h:i:sa");
			// 	$makeRandom = $random;
			// 	$file_name_rename = $makeRandom;
			// 	$explode = explode('.', $file_name);
			// 	if(count($explode) >= 2) 
			// 	{
			// 		$new_file = $file_name_rename.$date.'.'.$explode[1];		            
			// 		$config['upload_path'] = "./uploads/Healthcare/doc";
			// 		$config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
			// 		$config['file_name'] = $new_file;
			// 		$config['max_size'] = '307210';
			// 		$config['max_width'] = '300000';
			// 		$config['max_height'] = '300000';
			// 		$config['overwrite'] = TRUE;
			// 		$this->load->library('upload',$config);
			// 		$this->upload->initialize($config);
			// 		if(!$this->upload->do_upload("sign_file_upload")) 
			// 		{
			// 			$error = $this->upload->display_errors();
			// 			$this->session->set_flashdata("error",$error);				
			// 			redirect(current_url());exit;
			// 			// print_r($error);exit;
			// 				//echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
			// 		}
			// 		else
			// 		{
			// 			$signDoc = 'uploads/Transport/doc/'.$new_file;
			// 		}
			// 	}
			// }			
			// else if(isset($postData["sign_file_upload_old"]))   
			// {          	 
			// 	$signDoc = $postData["sign_file_upload_old"];		        				
			// }
			// else
			// {
			// 	$signDoc ="";	
			// }

			// 	//----- profile image -----
			// if(!empty($_FILES["profile_image"]["name"]))   
			// {   		    	
			// 	$file_name = $_FILES["profile_image"]["name"];
			// 	$original_file_name = $file_name;
			// 	$random = rand(1, 10000000000000000);
			// 	$date = date("Y-m-d-h:i:sa");
			// 	$makeRandom = $random;
			// 	$file_name_rename = $makeRandom;
			// 	$explode = explode('.', $file_name);
			// 	if(count($explode) >= 2) 
			// 	{
			// 		$new_file = $file_name_rename.$date.'.'.$explode[1];		            
			// 		$config['upload_path'] = "./uploads/Transport/thumb";
			// 		$config['allowed_types'] ="png|jpeg|jpg";
			// 		$config['file_name'] = $new_file;
			// 		$config['max_size'] = '307210';
			// 		$config['max_width'] = '300000';
			// 		$config['max_height'] = '300000';
			// 		$config['overwrite'] = TRUE;
			// 		$this->load->library('upload',$config);
			// 		$this->upload->initialize($config);
			// 		if(!$this->upload->do_upload("profile_image")) 
			// 		{
			// 			$error = $this->upload->display_errors();
			// 			$this->session->set_flashdata("error",$error);				
			// 			redirect(current_url());
			// 			exit;
			// 			// print_r($error);exit;
			// 				//echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
			// 		}
			// 		else
			// 		{
			// 			$profImage = 'uploads/Transport/thumb/'.$new_file;
			// 		}
			// 	}
			// }
			// else if(isset($postData["profile_image_old"]))   
			// {          	 

			// 	$profImage =$postData["profile_image_old"];		        				
			// }
			// else
			// {

			// 	$profImage ="";	
			// }
			// $health_cat = implode(",", $postData['health_category']); 
			// //---- update data -------
			// $updateArr  = array(
			// 	'full_name'=>$postData['full_name'],
			// 	'date_of_birth' => date("Y-m-d",strtotime($postData['date_of_birth'])),
			// 	'phone' => $postData['phone_no'],	
			// 	'street' => trim($postData['street']),
			// 	'country' => $postData['country'],
			// 	'state' => $postData['state'],
			// 	'city'=>$postData['city'],
			// 	'zipcode' => trim($postData['zipcode']),
			// 	'county' => $postData['county'],											
			// 	'emergency_contactname' => trim($postData['emergency_contact_name']),
			// 	'emergency_contactno' => $postData['emergency_contact_number'],	
			// 	'signature_file_upload'=>$signDoc,	
			// 	'picture'=>$profImage,							
			// 	'signature'=>$postData['signature'],								
			// 	'updated_at'=>date("Y-m-d H:i:a")
			// 	);
			// $where = array('id' =>$this->session->userdata('user_id'));
			// $result = $this->Common_model->update(TB_USERS,$where,$updateArr);
			// if($result)
			// {

			// 	$this->session->set_flashdata("success","Profile updated successfully");
			// 	redirect("transport-profile");
			// }
			// else
			// {
			// 	$this->session->set_flashdata("fail","Oops!!! Something went wrong.");				
			// 	redirect(current_url());
			// }	

	}else
	{
		redirect("frontend-login"); exit;
	}
}

	//---------- update driver info -------------
public function updatetpDriver()
{
	if($this->session->userdata('user_id'))
	{
		$postData = $this->input->post();
		$updtArr = array('full_name' => $postData['fullname'],
			'phone'=>$postData['phoneno'],
			'status'=>$postData['drstatus']);
		$where = array('id' =>$postData['drid']);
		$result = $this->Common_model->update(TB_USERS,$where,$updtArr);
		if($result)
		{

			echo json_encode(array('status' => 'success','message'=>'Driver updated successfully.'));
		}else
		{
			echo json_encode(array('status' => 'error','message'=>'Oops!!! Something went wrong.'));
		}		
	}else
	{
		redirect("frontend-login"); exit;
	}
}

	//--------- change driver availabality --------
public function changeDrAvailability()
{
	if($this->session->userdata('user_id'))
	{
		$postData = $this->input->post();
		$status = $postData['status'];
		$driver_id = $this->encrypt->decode($postData['drid']);
		$user_type = $postData['user_type'];
		$result = changeDriverAvail($status,$driver_id,$user_type);
		echo $result;		
	}else
	{
		redirect("frontend-login"); exit;
	}
}

//-----------  change driver approval  ------------
public function changeDrApproval()
{
	if($this->session->userdata('user_id'))
	{
		$postData = $this->input->post();
		$status = $postData['status'];
		$driver_id = $this->encrypt->decode($postData['drid']);

		$updateData = $this->Common_model->update(TB_USERS,array('id'=>$driver_id),array('is_approve'=>$status));
		if($updateData)
		{
			echo json_encode(array("status"=>"success","message"=>"Driver user has been updated successfully."));
		}else
		{
			echo json_encode(array("status"=>"error","message"=>"Oops!!! Something went wrong.")); exit;	
		}
	}else
	{
		redirect("frontend-login"); exit;
	}
}


	// ---------- upload driver data by csv file -----------
public function upload_driver()
{
	if($this->session->userdata('user_id'))
	{
		$postData = $this->input->post();
		$filename=$_FILES["user_file"]["tmp_name"];
		$mimes = array('application/vnd.ms-excel','text/xlsx','text/csv');
		if(in_array($_FILES['user_file']['type'],$mimes))
		{
			if($_FILES["user_file"]["size"] > 0)
			{
				$row = 0;
				$handle = fopen($filename, "r");
				while (($importdata = fgetcsv($handle, 0, ",")) !== FALSE)
				{				
					if($row != 0)
					{

						if(empty(trim($importdata[2])))
						{

						}
						else
						{
							$token = "";						
							$token=  random_string('alnum',20);
							$data[] = array(
								'user_token'=>$token,
								'user_type'=>"5",
								'transport_parent_id' => $this->session->userdata('user_id'),	
								'full_name' => trim($importdata[0]),
								'username' => trim($importdata[1]),
								'email' => trim($importdata[2]),
								'phone' => trim($importdata[3]),
								'password' => trim(md5($importdata[4])),
								'actual_password'   => trim($importdata[4]),	
								'created_at' => date('Y-m-d H:i:s'),
								'token'      => $token,
								'status'     => "active",
								'done'		 => 1
								);
							$email_data[]=array("dr_email"=>trim($importdata[2]),"dr_password"=>trim($importdata[4]));
						}						
					}
					$row++;
				}

				$result = $this->db->insert_batch(TB_USERS, $data); 
				$result_cnt = $this->db->affected_rows();
				fclose($handle);
				if($result_cnt > 0)
				{
					foreach ($email_data as $drvalue) 
					{
							// Email to registration
						$hostname = $this->config->item('hostname');
						$config['mailtype'] ='html';
						$config['charset'] ='iso-8859-1';
						$this->email->initialize($config);
						$from  = EMAIL_FROM; 
						$email = $drvalue['dr_email'];
						$this->messageBody  = email_header();
						$base_url = $this->config->item('base_url');
							// $reset_url = $base_url."accountverify/".$token ;			
						$this->messageBody  .= '<tr>
						<td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
							<p>Dear Customer,</p>
							<p>
								You have registered successfully on Transcura.<br> Following are your details : </p>
								<p>Email ID : '.$drvalue['dr_email'].'</p>
								<p>Password : '.$drvalue['dr_password'].'</p>
								<p>
									<p>Link : </p>
									<a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href='.$base_url.'>Click Here</a>
								</p><br /><br />
								<p>If the above link does not work, please copy and paste the following link into your browser.</p>
								<p>Copy the Link : </p>
								<a href="'.$base_url.'">'.$base_url.'</a>
							</p>					
						</td>
					</tr>'; 			
					$this->messageBody  .= email_footer();
					$this->email->from($from, $from);
					$this->email->to($email);
					$this->email->subject('Welcome to Transcura');
					$this->email->message($this->messageBody);
					$this->email->send();
				}

				$this->session->set_flashdata('success', $result_cnt." records addedd successfully.");
				redirect("add-driver");	
			}
			else
			{
				$this->session->set_flashdata('error', 'Oops!!! Something went wrong.');
				redirect("add-driver");
			}
		}

	} 
	else 
	{
		$this->session->set_flashdata('error', 'File type not allowed. Please upload csv file.');
		redirect("add-driver");
	}
}
else
{
	redirect("frontend-login"); exit;
}
}

//-------- get driver document ---------
public function getDriverDoc()
{
	if($this->session->userdata('user_id'))
	{
		$postData = $this->input->post();
		$driver_id = $postData['driver_id'];
		$getData = getDriverDochtml($driver_id);
		echo json_encode(array("status"=>"success","response"=>$getData)); exit;
	}else
	{
		redirect("frontend-login"); exit;
	}
}

	// delete driver by transpotation provider
public function deleteTPDriver()
{
	if($this->session->userdata('user_id'))
	{
		$postData = $this->input->post();

		$checkDriver = $this->Common_model->select('driver_id',TB_SCHEDULE_RIDE,array('driver_id' => $postData['drid'],'driver_status' => 3, 'sr_date >=' => date('Y-m-d')));

		if(!empty($checkDriver))
		{
			echo json_encode(array('status' => 'assigned' , 'message' => ' Can\'t delete driver, Trip is already assigned '));
		}
		else
		{
			$updateDriverStatus =  $this->Common_model->update(TB_USERS,array('id' => $postData['drid']), array('is_delete' => '1'));

			if($updateDriverStatus)
			{
				echo json_encode(array('status' => 'success' , 'message' => ' Driver deleted Successfully '));
			}
			else
			{
				echo json_encode(array('status' => 'error' , 'message' => 'something went wrong !'));
			}
				//echo json_encode(array('status' => 'success' , 'message' => ' Success '));	
		}
	}
	else
	{

	}

}



}
?>