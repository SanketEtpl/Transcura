<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed'); 

class MemberController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_model');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		$this->load->helper('security');
		$this->load->helper('email_template_helper');
		$this->load->library('upload');		
		$this->load->helper('string');
		$this->load->helper('common');
	}

	// member dashboard
	public function index()
	{	

		if($this->session->userdata('user_id'))
		{
			$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY,array("id"=>"231"));
			$data['states'] = $this->Common_model->select('*',TB_STATE);	
			$data['desire_tran'] = $this->Common_model->select('*',TB_CAR_CATEGORY);
			$data['mem_service'] = $this->Common_model->select('*',TB_MEMBER_SERVICE,array("is_active"=>"1"));			
			// $data['sp_request'] = $this->Common_model->select('*',TB_SPECIAL_REQUEST);		
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
			$data['home_section1'] = $this->Common_model->selectWhereIn('page_id,sub_title,page_title,page_detail',TB_PAGES,'page_id',array(2,14,7));				
			// form validation

			$this->form_validation->set_rules('date','date','trim|required|xss_clean');		
			/*$this->form_validation->set_rules('appointment_time', 'appointment time', 'trim|required|xss_clean');*/
			$this->form_validation->set_rules('pick_up_time','pick up time','trim|required|xss_clean');
			$this->form_validation->set_rules('pick_up_street', 'pick up address', 'trim|required|min_length[3]|max_length[100]|xss_clean');
			$this->form_validation->set_rules('state', 'state', 'trim|required|xss_clean');
			/*$this->form_validation->set_rules('country', 'country', 'trim|required|xss_clean');*/
			$this->form_validation->set_rules('zipcode','zipcode','trim|required|min_length[5]|integer|xss_clean');

			$this->form_validation->set_rules('member_service', 'Type of service', 'trim|required|xss_clean');
			$this->form_validation->set_rules('special_instruction','Special instructions','trim|required|xss_clean');
			$this->form_validation->set_rules('desire_transp', 'Desired transportation', 'trim|required|xss_clean');
			

			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

			$data['states'] = $this->Common_model->select("*",TB_STATE,array('country_id' => 231));
			if ($this->form_validation->run() == FALSE)
			{

				//print_r($data['states']);exit();
				$this->load->view('frontMember/memberSchedule',$data);
			}
			else
			{
				//get input data from user
				$postData = $this->input->post();	
				// $query = $this->db->query("SELECT id, sr_trip_id FROM tbl_schedule_ride ORDER BY id DESC LIMIT 1");
				// $result = $query->result_array();
				// $tripID=explode("T", $result[0]['sr_trip_id']);
				// $tripIDGenerated = "T".str_pad($tripID[1]+1, 14, '0', STR_PAD_LEFT);
				$scheduleRideSession =array(); 
				$scheduleRideSession = array(
					'sr_date' => date("Y-m-d",strtotime($postData['date'])),
					'sr_pick_up_time' => $postData['pick_up_time'],
					// 'sr_appointment_time' => $postData['appointment_time'],	
					'sr_street' => trim($postData['pick_up_street']),
					'sr_country' => 231,//$postData['country'],
					'sr_state' => $postData['state'],
					'sr_zipcode' => trim($postData['zipcode']),
					'sr_health_service' => trim($postData['member_service']),
					'sr_special_instruction' => trim($postData['special_instruction']),
					'sr_desire_transportaion' => trim($postData['desire_transp']),
									//'created_on'=>date("Y-m-d H:i:a"),
					'sr_status'=>1
					);
				$this->session->set_userdata('memberRestaurant',$scheduleRideSession);
				//$sessionAddress = $this->session->userdata('scheduleRide')['sr_date'];
				//print_r($this->session->all_userdata());exit;
				//$this->load->view('user/address',$data);	

				//----- get service name ----
				$getservname = $this->Common_model->select("ms_type",TB_MEMBER_SERVICE,array("ms_id"=>$this->session->userdata('memberRestaurant')['sr_health_service']));
				//$data['service_name'] = $getservname[0]['ms_type']; 
				$this->session->set_userdata('service_here',$getservname[0]['ms_type']);

				$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); 	
				$this->load->view("frontMember/restaurants",$data);	
			}
			
		}
		else
		{
			redirect("frontend-login"); exit;
		}
		/*if($this->session->userdata('user_id'))
		{	
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); 	
			$this->load->view("frontMember/restaurants",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}*/
	}

	// restaurants view
	public function restaurants()
	{
		if($this->session->userdata('user_id'))
		{	
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); 	
			$this->load->view("frontMember/restaurants",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	// searchRestaurant
	public function searchRestaurant()
	{
		if($this->session->userdata('user_id'))
		{
			$location =  $this->input->post('location');
			$searchquery =  $this->input->post('query');

			$service = $this->session->userdata('memberRestaurant')['sr_health_service'];
			$getservice = $this->Common_model->select("ms_type",TB_MEMBER_SERVICE,array('ms_id' =>$service));
			$ms_service = $getservice[0]['ms_type'];
			$locationq = urlencode($searchquery.' '.$ms_service.' in '.$location);
			$result = file_get_contents('https://maps.googleapis.com/maps/api/place/textsearch/json?query='.$locationq.'&key='.GOOGLE_API_KEY);
			 //echo "<pre>";print_r($result);
			$output= json_decode($result,true);
			//echo "<pre>";print_r($output);
			if(empty($output['results']))
			{
				echo json_encode(array('status' =>'error','msg'=>"No data found."));
			}
			else
			{
				echo json_encode(array('status' =>'success','data'=>$output));	
			}


			
			// $flag = 0; 
			// $entity_type = "";
			// $entity_id = "";

			// $curl = curl_init();
			// $curl1 = curl_init();

			// curl_setopt_array($curl, array(
			// 	CURLOPT_URL => "https://developers.zomato.com/api/v2.1/locations?query=".$location,
			// 	CURLOPT_RETURNTRANSFER => true,
			// 	CURLOPT_ENCODING => "",
			// 	CURLOPT_MAXREDIRS => 10,
			// 	CURLOPT_TIMEOUT => 30,
			// 	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			// 	CURLOPT_CUSTOMREQUEST => "GET",
			// 	CURLOPT_HTTPHEADER => array(
			// 		"accept: application/json",
			// 		"cache-control: no-cache",
			// 		"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
			// 		"user-key:".Zomato_key
			// 		),
			// 	));

			// $response = curl_exec($curl);

			// $err = curl_error($curl);

			// curl_close($curl);

			// if (!$err) 
			// {
			// 	$flag = 1;

			// }


			// if(1 == $flag)
			// {
			// 	$data = json_decode($response);

			// 	$entity_id = $data->location_suggestions[0]->entity_id;
			// 	$parameters = "";

			// 	$parameters .= "&entity_type=".$data->location_suggestions[0]->entity_type;

			// 	if("" != $this->input->post('query'))
			// 	{

			// 		$parameters .= "&q=".$this->input->post('query');
			// 	}

			// 	curl_setopt_array($curl1, array(
			// 		CURLOPT_URL => "https://developers.zomato.com/api/v2.1/search?entity_id=".$entity_id.$parameters,
			// 		CURLOPT_RETURNTRANSFER => true,
			// 		CURLOPT_ENCODING => "",
			// 		CURLOPT_MAXREDIRS => 10,
			// 		CURLOPT_TIMEOUT => 30,
			// 		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			// 		CURLOPT_CUSTOMREQUEST => "GET",
			// 		CURLOPT_HTTPHEADER => array(
			// 			"accept: application/json",
			// 			"cache-control: no-cache",
			// 			"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
			// 			"user-key: ".Zomato_key
			// 			),
			// 		));

			// 	$response1 = curl_exec($curl1);

			// 	$data = json_decode($response1);
			// 	$err1 = curl_error($curl1);

			// 	curl_close($curl1);

			// 	if($err1)
			// 	{
			// 		echo json_encode(array('status' =>'error','msg'=>$err1));
			// 	}
			// 	else
			// 	{
			// 		echo json_encode(array('status' =>'success','data'=>$data));	
			// 	}

			// }
		}else
		{
			redirect("frontend-login"); exit;
		}
	}

	// -------- get Restaurant data by reasturant id ------------
	public function getRestaurant($placeid)
	{	
		if($this->session->userdata('user_id'))
		{		
			$result = file_get_contents('https://maps.googleapis.com/maps/api/place/details/json?placeid='.$placeid.'&key='.GOOGLE_API_KEY);
			$output= json_decode($result,true);

			// echo "<pre>";print_r($output);

			// $curl = curl_init();
			// curl_setopt_array($curl, array(
			// 	CURLOPT_URL => "https://developers.zomato.com/api/v2.1/restaurant?res_id=".$id,
			// 	CURLOPT_RETURNTRANSFER => true,
			// 	CURLOPT_ENCODING => "",
			// 	CURLOPT_MAXREDIRS => 10,
			// 	CURLOPT_TIMEOUT => 30,
			// 	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			// 	CURLOPT_CUSTOMREQUEST => "GET",
			// 	CURLOPT_HTTPHEADER => array(
			// 		"accept: application/json",
			// 		"cache-control: no-cache",
			// 		"content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
			// 		"user-key:".Zomato_key
			// 		),
			// 	));

			// $response = curl_exec($curl);
			// curl_close($curl);
			// $data['restaurant_details'] = json_decode($response);

			$data['restaurant_details'] = $output;
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
			$this->load->view('frontMember/view_restaurant_details',$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	//--------- get distance and km bewtween source and destination ---------
	public function get_Restaddress()
	{	

		if($this->session->userdata('user_id'))
		{
			$postData = $this->input->post();

			$this->session->set_userdata('memberSource',$this->input->post('source'));
			$this->session->set_userdata('memberDestination',$this->input->post('destination'));
			$this->session->set_userdata('end_lat',$this->input->post('end_lat'));
			$this->session->set_userdata('end_long',$this->input->post('end_long'));

			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => "https://maps.googleapis.com/maps/api/place/textsearch/json?query='".str_replace(" ", "+", $postData["destination"])."'&key=".GOOGLE_API_KEY,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					"cache-control: no-cache",		    
					),
				));		
			$data = curl_exec($curl);

			$err = curl_error($curl);
			$address = json_decode($data, true);
			// echo "<pre>";print_r($address);die;
			$rows ='';
			if(!empty($address['results']))
			{

				$cnt = count($address['results']);


				$curlDt = curl_init();
				curl_setopt_array($curlDt, array(
					CURLOPT_URL => "https://maps.googleapis.com/maps/api/place/details/json?placeid=".$address['results'][$cnt-1]['place_id']."&key=".GOOGLE_API_KEY,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 30,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "GET",
					CURLOPT_HTTPHEADER => array(
						"cache-control: no-cache",							   
						),
					));
				$dataDt = curl_exec($curlDt);
				//echo "<pre>";print_r($dataDt);
				$errDt = curl_error($curlDt);
				$totalDistance = json_decode($dataDt, true);

				if(!empty($totalDistance['result'])) {
					/*$distance = $totalDistance['rows'][0]['elements'][0]['distance']['text'];
					$duration = $totalDistance['rows'][0]['elements'][0]['duration']['text'];*/

					$this->output
					->set_content_type("application/json")
					->set_output(json_encode(array("status"=>true,"data"=>$totalDistance['result'])));


				}
				else
				{
					$this->output
					->set_content_type("application/json")
					->set_output(json_encode(array("status"=>false,"msg"=>"something went wrong !!")));
				}
				/*$rows .= '<tr id="'.$address['results'][0]['formatted_address'].'">
				<td class="text-left">'.$address['results'][0]['formatted_address'].'</td>	
				<td class="text-left">'.$postData["destination"].'</td>               	
				<td class="text-left">'.$distance.'</td>
				<td class="text-left">'.$duration.'</td>
			</tr>';	

			$loc_data=array("start_lat"=>$address['results'][0]['geometry']['location']['lat'],
				"start_long"=>$address['results'][0]['geometry']['location']['lng'],
				"distance"=>$distance,"duration"=>$duration);  */      	             	   
			}
			else
			{
				$rows = '<tr><td colspan="5" align="center">No Record Found0000.</td></tr>';	
			}


			curl_close($curl);
			curl_close($curlDt);
		/*if ($err) {
			$this->output
			->set_content_type("application/json")
			->set_output(json_encode(array("status"=>false,"message"=>$err,"loc_data"=>"")));
		} else {
			$this->output
			->set_content_type("application/json")
			->set_output(json_encode(array("status"=>true,"rows"=>$rows,"loc_data"=>$loc_data)));
		}	*/
	}else
	{
		redirect("frontend-login"); exit;
	}
}

	//------- save restaurants schedule -----------
public function save_rest_schedule()
{	
	if($this->session->userdata('user_id'))
	{	

		$postData = $this->input->post();
		// $curl = curl_init();
		// curl_setopt_array($curl, array(
		// 	CURLOPT_URL => "https://maps.googleapis.com/maps/api/distancematrix/json?origins='".str_replace(" ", "+", $postData["source"])."'&destinations='".str_replace(" ", "+", $postData["destination"])."'&key=".GOOGLE_API_KEY,
		// 	CURLOPT_RETURNTRANSFER => true,
		// 	CURLOPT_ENCODING => "",
		// 	CURLOPT_MAXREDIRS => 10,
		// 	CURLOPT_TIMEOUT => 30,
		// 	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		// 	CURLOPT_CUSTOMREQUEST => "GET",
		// 	CURLOPT_HTTPHEADER => array(
		// 		"cache-control: no-cache",		    
		// 		),
		// 	));		
		// $data = curl_exec($curl);		
		// $err = curl_error($curl);
		// curl_close($curl);
		// $res = json_decode($data);
		// $arr = explode(" ",$res->rows[0]->elements[0]->distance->text);
		// $distance = ($arr[0] * 0.621);

			// ----- get source lat long ----------
		$address = urlencode($postData['source']);
		$geocode = file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.trim($address).'&sensor=false&key='.GOOGLE_API_KEY);

		$output= json_decode($geocode,true);
		$src_lat = $output['results'][0]['geometry']['location']['lat'];
		$src_long = $output['results'][0]['geometry']['location']['lng'];
		
		// echo "<pre>";print_r($this->session->userdata('memberRestaurant'));die;

		$user_id=$this->session->userdata('user_id');


		/*if(empty($postData['start_lat']) || empty($postData['start_long']))
		{
			echo json_encode(array('status' => "0", "message"=>"Source point is not valid"));
			exit();
		}
		if(empty($postData['end_lat']) || empty($postData['end_long']))
		{
			echo json_encode(array('status' => "0", "message"=>"Destination point is not valid"));
			exit();
		}
		if(empty($postData['distance']) || empty($postData['duration']))
		{
			echo json_encode(array('status' => "0", "message"=>"Distance or Duration is not valid"));
			exit();
		}*/

		// $query = $this->db->query("SELECT id, sr_trip_id FROM tbl_schedule_ride ORDER BY id DESC LIMIT 1");
		// $result = $query->result_array();
		// $tripID=explode("T", $result[0]['sr_trip_id']);
		// $tripIDGenerated = "T".str_pad($tripID[1]+1, 14, '0', STR_PAD_LEFT);
		$tripIDGenerated = date("YmdHis");
		$pick_date = date("Y-m-d",strtotime($this->session->userdata('memberRestaurant')['sr_date']))." ".date("H:i:s",strtotime($this->session->userdata('memberRestaurant')['sr_pick_up_time']));
		$insertArr = array(
			'sr_trip_id'=> $tripIDGenerated,
			'sr_date'=> $pick_date,
			'sr_pick_up_time'=> trim($this->session->userdata('memberRestaurant')['sr_pick_up_time']),		
			'sr_street'=> trim($this->session->userdata('memberRestaurant')['sr_street']),			
			'sr_country'=> trim($this->session->userdata('memberRestaurant')['sr_country']),			
			'sr_state'=> trim($this->session->userdata('memberRestaurant')['sr_state']),			
			'sr_zipcode'=> trim($this->session->userdata('memberRestaurant')['sr_zipcode']),			
			'sr_health_service'=> trim($this->session->userdata('memberRestaurant')['sr_health_service']),	
			'sr_special_instruction'=> trim($this->session->userdata('memberRestaurant')['sr_special_instruction']),			
			'sr_desire_transportaion'=> trim($this->session->userdata('memberRestaurant')['sr_desire_transportaion']),			
			'user_id'=>$user_id,
			'source_address'=> trim($postData['source']),
			'destination_address'=> trim($postData['destination']),
			'sr_lat_lng_source'=> $src_lat.",".$src_long,
			'sr_lat_lng_destination'=>trim($postData['end_lat'].",".$postData['end_long']),
			'sr_total_distance'=> trim($postData['tot_distance']),
			'sr_total_cost'=> trim($postData['tot_amt']),
			'duration'=> trim($postData['trip_duration']),
			'created_on'=>date("Y-m-d H:i:s"),
			'driver_status'=>"1",
			'admin_status'=>"2"
			);
		// echo "<pre>";print_r($insertArr);die;
		$existSchedule = $this->Common_model->select("id",TB_SCHEDULE_RIDE,array('user_id' => $this->session->userdata('user_id'),'sr_date' => $this->session->userdata('memberRestaurant')['sr_date'],'sr_pick_up_time' => $this->session->userdata('memberRestaurant')['sr_pick_up_time']));
		// echo "<pre>".count($existSchedule);print_r($existSchedule);die;
		if(isset($existSchedule) &&  (count($existSchedule) > 0))
		{
			echo json_encode(array('status' => 'exist','message' =>'Ride is already scheduled'));

			exit();
		}
		
		$result = $this->Common_model->insert(TB_SCHEDULE_RIDE,$insertArr);	
		$sched_id=$this->db->insert_id();
		$query = $this->db->query("SELECT * FROM  tbl_driver_location WHERE `updated_at` >= CURDATE( )");
		$getDriverLocation = $query->result_array();
		foreach ($getDriverLocation as $key => $value) {
			$check_available = getDriverAvailability($value['user_id']);
			if($check_available == "availabale")
			{
				$getDistance = $this->distance($src_lat, $src_long, $value['loc_lat'], $value['loc_long'], "M");
				if(round($getDistance)<=5)
				{
					$this->Common_model->insert(TB_DRIVER_REQUEST,
						array("sched_id"=>$sched_id,"driver_id"=>$value['user_id'],"created_date"=>date("Y-m-d H:i:s")));	
				}
			}
		}	
		if($result)
		{
			echo json_encode(array('status' => "1", "message"=>"Your schedule request has been sent successfully .","trip_id" => $tripIDGenerated));
			exit();

		}
		else
		{
			echo json_encode(array('status' => "0", "message"=>"Your schedule request has been failed ."));
			exit();
		}
	}else
	{
		redirect("frontend-login"); exit;
	}	
}

public function member_ride_details()
{
	if($this->session->userdata('user_id'))
	{	
		// -------- get distance ----------------
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://maps.googleapis.com/maps/api/distancematrix/json?origins='".str_replace(" ", "+", $this->session->userdata('memberSource'))."'&destinations='".str_replace(" ", "+", $this->session->userdata('memberDestination'))."'&key=".GOOGLE_API_KEY,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",		    
				),
			));		
		$datadist = curl_exec($curl);		
		$err = curl_error($curl);
		curl_close($curl);
		$res = json_decode($datadist);
		//$res->origin_addresses[0];
		$arr = explode(" ",$res->rows[0]->elements[0]->distance->text);
		$distance = ($arr[0] * 0.621);
		$duration = $res->rows[0]->elements[0]->duration->text;
		$data['totdistance']=$distance;
		$data['duration']=$duration;

		$data['country'] = $this->Common_model->select('country_name',TB_COUNTRY,array('id' => $this->session->userdata('memberRestaurant')['sr_country']));
		$data['state'] = $this->Common_model->select('state_name',TB_STATE,array('id' => $this->session->userdata('memberRestaurant')['sr_state']));
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); 
		$data['transportation'] = $this->Common_model->select('car_category,car_rate_per_mile',TB_CAR_CATEGORY,array('carcat_id' => $this->session->userdata('memberRestaurant')['sr_desire_transportaion']));
		$this->load->view("frontMember/memberRideDetails",$data);
	}else
	{
		redirect("frontend-login"); exit;
	}

}

public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
	$theta = $lon1 - $lon2;
	$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	$dist = acos($dist);
	$dist = rad2deg($dist);
	$miles = $dist * 60 * 1.1515;
	$unit = strtoupper($unit);
	if ($unit == "K") {
		return ($miles * 1.609344);
	} else if ($unit == "N") {
		return ($miles * 0.8684);
	} else {
		return $miles;
	}
}

//-------- cancel paypal payment ------------
public function cancel_url()
{
	if($this->session->userdata('user_id'))
	{
		$this->session->set_flashdata('cancel_pay_msg', "Payment Canceled.");
		redirect('member-my-ride');
	}
}

// save schedule and payment
public function process_topayment()
{
	if($this->session->userdata('user_id'))
	{
		$sched_id = $this->session->userdata('sess_member_pay_sched_id');
		if (isset($_REQUEST)) 
		{
			$pay_status = $_REQUEST['payment_status'];
			$pay_trans_id = $_REQUEST['txn_id'];

			switch ($pay_status) {
				case 'Completed':
				$up_cond = array('id' => $sched_id,'user_id'=>$this->session->userdata('user_id'));
				$update_data = array('sr_transaction_id' => $pay_trans_id,'sr_payment_status'=>$pay_status,'driver_status'=>'3');
				$result = $this->Common_model->update(TB_SCHEDULE_RIDE,$up_cond,$update_data);
				break;

				case 'Canceled_Reversal':
					# code...
				break;	
				case 'Denied':
				break;
				case 'Expired':
				break;
				case 'Failed':	
				break;
				case 'Processed':
				break;
				case 'Refunded':
				break;
				case 'Reversed':
				break;
				case 'Voided':
				break;
				
				default:
					# code...
				break;
			}

			if($result)
			{
				$this->session->set_flashdata('pay_msg', "Payment done successfully.");
				redirect('member-my-ride');	
			}
		}else
		{
			$this->session->set_flashdata('cancel_pay_msg', "Something went wrong in payment.");
			redirect('member-my-ride');	
		}
	}
}

//----------- Confirm Driver -----------
public function confirmDriver()
{
	if($this->session->userdata('user_id'))
	{
		$postData = $this->input->post();
		$sched_id = $postData['sched_id'];

		$alreadyConfirm = $this->common_model->select('id',TB_SCHEDULE_RIDE,array('id' => $sched_id,'user_id'=>$this->session->userdata('user_id'),'driver_status'=>'3'));

		if(count($alreadyConfirm) > 0)
		{
			echo json_encode(array('status' => "success", "message"=>"You already confirm this request."));
		}

		$up_cond = array('id' => $sched_id,'user_id'=>$this->session->userdata('user_id'));
		$update_data = array('driver_status'=>'3');
		$result = $this->Common_model->update(TB_SCHEDULE_RIDE,$up_cond,$update_data);
		if($result)
		{
			echo json_encode(array('status' => "success", "message"=>"Driver is confirmed successfully."));
		}else
		{
			echo json_encode(array('status' =>'error','message'=>"Oops!!! Something went wrong."));
		}
	}
}

//------- REJECT DRIVER -------
public function rejectDriver()
{
	if($this->session->userdata('user_id'))
	{
		$postData = $this->input->post();
		$sched_id = $postData['sched_id'];
		$gettripdetails = $this->Common_model->select("*",TB_SCHEDULE_RIDE,array("id"=>$sched_id));
		$driver_id = $gettripdetails[0]['driver_id'];
		$latlong = $gettripdetails[0]['sr_lat_lng_source'];
		
		$newlatlong = explode(",",$latlong);
		$src_lat = $newlatlong[0];
		$src_long = $newlatlong[1];

		$up_cond = array('id' => $sched_id,'user_id'=>$this->session->userdata('user_id'));
		$update_data = array('driver_status'=>'1','driver_id'=>'0');
		$result = $this->Common_model->update(TB_SCHEDULE_RIDE,$up_cond,$update_data);

		$query = $this->db->query("SELECT * FROM  tbl_driver_location WHERE `updated_at` >= CURDATE( ) AND user_id != $driver_id");
		$getDriverLocation = $query->result_array();
		foreach ($getDriverLocation as $key => $value) {
			$check_available = getDriverAvailability($value['user_id']);
			if($check_available == "availabale")
			{
				$getDistance = $this->distance($src_lat, $src_long, $value['loc_lat'], $value['loc_long'], "M");
				if(round($getDistance)<=5)
				{
					$this->Common_model->insert(TB_DRIVER_REQUEST,
						array("sched_id"=>$sched_id,"driver_id"=>$value['user_id'],"created_date"=>date("Y-m-d H:i:s")));	
				}
			}
		}	

		if($result)
		{

			echo json_encode(array('status' => "success", "message"=>"Your schedule request has been sent successfully ."));
			exit();

		}
		else
		{
			echo json_encode(array('status' => "error", "message"=>"Your schedule request has been failed ."));
			exit();
		}
	}
}

// my ride view      Ram k code
public function member_my_ride()
{ 

	$this->load->library('pagination');
	$this->load->helper('url');

	if($this->session->userdata('user_id'))
	{  
		$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));

		/*Scheduled Ride*/
		$data['scheduleRide'] = $this->Common_model->select("id,sr_trip_id",TB_SCHEDULE_RIDE);

		$query = $this->db->query("SELECT sr.id,sr.sr_trip_id,sr.sr_date,sr.sr_pick_up_time,u.full_name,u.picture,sr.driver_status FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.user_id = '".$this->session->userdata('user_id')."' AND sr.sr_status =1 order by sr.sr_date DESC");
		$data['tripDetails'] = $query->result_array();			


		/*Complted Rides */
		$completedRideQuery = $this->db->query("SELECT sr.id,sr.sr_trip_id,sr.sr_date,sr.sr_pick_up_time,u.full_name,u.picture FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.user_id = '".$this->session->userdata('user_id')."' AND sr.sr_status = 2 ");
		$data['completedTripDetails'] = $completedRideQuery->result_array();


		/*Cancellted Rides */
		$cancelledRideQuery = $this->db->query("SELECT sr.id,sr.sr_trip_id,sr.sr_date,sr.sr_pick_up_time,u.full_name,u.picture FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.user_id = '".$this->session->userdata('user_id')."' AND sr.sr_status =3 ");
		$data['cancelledTripDetails'] = $cancelledRideQuery->result_array();

			/*$paging['total_rows'] = count($data['cancelledTripDetails']);
			$paging['per_page'] = 5;
			$paging['base_url'] = base_url().'driver_my_ride';
	        $paging['uri_segment']= 3;
	        $paging['num_links'] = 2;
	        $paging['first_link'] = 'First';
	        $paging['first_tag_open'] = '<li>>';
	        $paging['first_tag_close'] = '</li>';
	        $paging['num_tag_open'] = '<li>';
	        $paging['num_tag_close'] = '</li>';
	        $paging['prev_link'] = 'Prev';
	        $paging['prev_tag_open'] = '<li>';
	        $paging['prev_tag_close'] = '</li>';
	        $paging['next_link'] = 'Next';
	        $paging['next_tag_open'] = '<li>';
	        $paging['next_tag_close'] = '</li>';
	        $paging['last_link'] = 'Last';
	        $paging['last_tag_open'] = '<li>';
	        $paging['last_tag_close'] = '</li>';
	        $paging['cur_tag_open'] = '<li class="active"><a href="javascript:void(0);">';
	        $paging['cur_tag_close'] = '</a></li>';      
            $this->pagination->initialize($paging); 
            $data['limit'] = 5;
        	$data['number_page'] = $paging['per_page']; 
        	$data['offset'] = $this->uri->segment(3) ? $this->uri->segment(3) : 1;
        	
        	$start = $data['offset'] * $data['number_page'] - $data['number_page'];
        	$data['nav'] = $this->pagination->create_links();

        	$cancelledRideQuery_paginated = $this->db->query("SELECT sr.id,sr.sr_trip_id,sr.sr_date,sr.sr_pick_up_time,u.full_name,u.picture FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.user_id = '".$this->session->userdata('user_id')."' AND sr.sr_status =3 limit ".$start.",".$data['limit']."");
        	$data['cancelledTripDetails_paginated'] = $cancelledRideQuery_paginated->result_array();*/

        	$data['srstatus'] = $this->input->get("sr_status");

        	$this->load->view("frontMember/memberMyRides",$data);
        }
        else
        {
        	redirect("frontend-login"); exit;
        }	
    }

    public function getScheduleDetails()
    {
    	if($this->session->userdata('user_id'))
    	{
    		$postData = $this->input->post();
    		$sched_id = $postData['sched_id'];

    		$cond = array(TB_SCHEDULE_RIDE.".id" => $sched_id);
    		$jointype=array(TB_USERS=>"LEFT");
    		$join = array(TB_USERS=>TB_SCHEDULE_RIDE.".driver_id = ".TB_USERS.".id");
    		$tripDetails = $this->Common_model->selectQuery(TB_SCHEDULE_RIDE.'.id,sr_trip_id,source_address,destination_address,sr_total_cost,DATE_FORMAT(sr_date,"%d-%b-%Y") as trip_date,sr_pick_up_time,sr_total_distance,full_name',TB_SCHEDULE_RIDE,$cond,FALSE,$join,$jointype); 
    		$this->session->set_userdata('sess_member_pay_sched_id',$tripDetails[0]['id']);
    		if(empty($tripDetails))
    		{
    			echo json_encode(array('status' =>'error','data'=>"No data found."));
    		}
    		else
    		{
    			echo json_encode(array('status' =>'success','data'=>$tripDetails[0]));	
    		}
    	}
    }	

// ----------- member profile --------
    public function member_profile()
    {
    	if($this->session->userdata('user_id'))
    	{
    		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
    		$cond = array(TB_USERS.".id" => $this->session->userdata('user_id'));
    		$jointype=array(TB_COUNTRY=>"LEFT",TB_CITY=>"LEFT",TB_STATE=>"LEFT",TB_DOCTOR_SEARCH_BY_CAT_LOC=>"LEFT");
    		$join = array(
    			TB_COUNTRY=>TB_COUNTRY.".id = ".TB_USERS.".country",
    			TB_STATE=>TB_STATE.".id = ".TB_USERS.".state",
    			TB_CITY=>TB_CITY.".id = ".TB_USERS.".city");
    		$select = "full_name,phone,email,date_of_birth,picture,zipcode,username,mobile,emergency_contactno,emergency_contactname,signature,state_name,country_name,city_name,county,street,card_token_id";
    		$data['member_profile'] = $this->Common_model->selectQuery($select,TB_USERS,$cond,false,$join,$jointype); 
    		if(!empty($data['member_profile']['0']['card_token_id']))
    		{
    				//include Stripe PHP library
    			require_once APPPATH."third_party/stripe/init.php";		
    			\Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

			//-------- get customer stripe details ---------
    			$cards = \Stripe\Customer::retrieve($data['member_profile']['0']['card_token_id'])->sources->all(array("object" => "card"));
    			$data['card_data']=$cards->data;
    		}
    		$this->load->view("frontMember/memberProfile",$data);
    	}else
    	{
    		redirect("frontend-login"); exit;
    	}
    }

    // ------- show member data for edit profile ------
    public function editMemebrProfile()
    {
    	if($this->session->userdata('user_id'))
    	{
    		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
    		$cond = array(TB_USERS.".id" => $this->session->userdata('user_id'));
    		$jointype=array(TB_COUNTRY=>"LEFT",TB_CITY=>"LEFT",TB_STATE=>"LEFT",TB_DOCTOR_SEARCH_BY_CAT_LOC=>"LEFT");
    		$join = array(
    			TB_COUNTRY=>TB_COUNTRY.".id = ".TB_USERS.".country",
    			TB_STATE=>TB_STATE.".id = ".TB_USERS.".state",
    			TB_CITY=>TB_CITY.".id = ".TB_USERS.".city");
    		$select = "full_name,phone,email,date_of_birth,picture,zipcode,username,mobile,emergency_contactno,emergency_contactname,signature,state,state_name,country_name,city,city_name,county,street,signature_file_upload,card_token_id";
    		$data['member_profile'] = $this->Common_model->selectQuery($select,TB_USERS,$cond,false,$join,$jointype); 
    		$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY,array('id'=>231));
    		$data['stateData'] = $this->Common_model->select("*",TB_STATE,array('country_id'=>231));
    		$data['cityData'] = $this->Common_model->select("*",TB_CITY,array('state_id'=>$data['member_profile'][0]['state']));

    		if(!empty($data['member_profile']['0']['card_token_id']))
    		{
    			require_once APPPATH."third_party/stripe/init.php";		
    			\Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);

			//-------- get customer stripe details ---------
    			$cards = \Stripe\Customer::retrieve($data['member_profile']['0']['card_token_id'])->sources->all(array("object" => "card"));
    			$data['card_data']=$cards->data;
    			$data['customer_id'] = $data['member_profile']['0']['card_token_id'];
    		}

    		$this->load->view("frontMember/editmemberProfile",$data);
    	}else
    	{
    		redirect("frontend-login"); exit;
    	}
    }

    // -------- update member profile ------------
    public function updateMemebrProfile()
    {
    	if($this->session->userdata('user_id'))
    	{
    		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
    		$cond = array(TB_USERS.".id" => $this->session->userdata('user_id'));
    		$jointype=array(TB_COUNTRY=>"LEFT",TB_CITY=>"LEFT",TB_STATE=>"LEFT");
    		$join = array(
    			TB_COUNTRY=>TB_COUNTRY.".id = ".TB_USERS.".country",
    			TB_STATE=>TB_STATE.".id = ".TB_USERS.".state",
    			TB_CITY=>TB_CITY.".id = ".TB_USERS.".city");
    		$select = "full_name,phone,email,date_of_birth,picture,zipcode,username,mobile,emergency_contactno,emergency_contactname,signature,state,state_name,country_name,city,city_name,county,street,signature_file_upload";
    		$data['member_profile'] = $this->Common_model->selectQuery($select,TB_USERS,$cond,false,$join,$jointype); 

    		$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY,array('id'=>231));
    		$data['stateData'] = $this->Common_model->select("*",TB_STATE,array('country_id'=>231));
    		$data['cityData'] = $this->Common_model->select("*",TB_CITY,array('state_id'=>$data['member_profile'][0]['state']));

    		$this->form_validation->set_rules('full_name', 'Full Name', 'required');
    		$this->form_validation->set_rules('date_of_birth', 'Date of Birth', 'required');
    		$this->form_validation->set_rules('phone_no', 'Mobile Number', 'required');
    		$this->form_validation->set_rules('state', 'State', 'required');
    		$this->form_validation->set_rules('city', 'City', 'required');
    		$this->form_validation->set_rules('zipcode', 'Zipcode', 'required');
    		$this->form_validation->set_rules('county', 'County', 'required');
    		$this->form_validation->set_rules('street', 'Street', 'required');
    		$this->form_validation->set_rules('emergency_contact_name', 'Emergency contact name', 'required');
    		$this->form_validation->set_rules('emergency_contact_number', 'Emergency contact number', 'required');
    		$this->form_validation->set_rules('signature', 'Signature', 'required');

    		if ($this->form_validation->run() == FALSE)
    		{
    			$this->load->view("frontMember/editmemberProfile",$data);
    		}

    		else
    		{
    			$perDoc=$signDoc="";
    			$postData = $this->input->post();
    			if(!empty($_FILES["sign_file_upload"]["name"]))   
    			{   
    				$file_name = $_FILES["sign_file_upload"]["name"];
    				$original_file_name = $file_name;
    				$random = rand(1, 10000000000000000);
    				$date = date("Y-m-d-h:i:sa");
    				$makeRandom = $random;
    				$file_name_rename = $makeRandom;
    				$explode = explode('.', $file_name);
    				if(count($explode) >= 2) 
    				{
    					$new_file = $file_name_rename.$date.'.'.$explode[1];		            
    					$config['upload_path'] = "./uploads/Member/doc";
    					$config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
    					$config['file_name'] = $new_file;
    					$config['max_size'] = '307210';
    					$config['max_width'] = '300000';
    					$config['max_height'] = '300000';
    					$config['overwrite'] = TRUE;
    					$this->load->library('upload',$config);
    					$this->upload->initialize($config);
    					if(!$this->upload->do_upload("sign_file_upload")) 
    					{
    						$error = $this->upload->display_errors();
    						$this->session->set_flashdata("error",$error." for signature");				
    						redirect(current_url());
    					}
    					else
    					{
    						$signDoc = 'uploads/Member/doc/'.$new_file;
    					}
    				}
    			}			
    			else if(isset($postData["sign_file_upload_old"]))   
    			{          	 
    				$signDoc = $postData["sign_file_upload_old"];		        				
    			}
    			else
    			{
    				$signDoc ="";	
    			}

				//----- profile image -----
    			if(!empty($_FILES["profile_image"]["name"]))   
    			{   		    	
    				$file_name = $_FILES["profile_image"]["name"];
    				$original_file_name = $file_name;
    				$random = rand(1, 10000000000000000);
    				$date = date("Y-m-d-h:i:sa");
    				$makeRandom = $random;
    				$file_name_rename = $makeRandom;
    				$explode = explode('.', $file_name);
    				if(count($explode) >= 2) 
    				{
    					$new_file = $file_name_rename.$date.'.'.$explode[1];		            
    					$config['upload_path'] = "./uploads/Member/thumb";
    					$config['allowed_types'] ="png|jpeg|jpg";
    					$config['file_name'] = $new_file;
    					$config['max_size'] = '307210';
    					$config['max_width'] = '300000';
    					$config['max_height'] = '300000';
    					$config['overwrite'] = TRUE;
    					$this->load->library('upload',$config);
    					$this->upload->initialize($config);
    					if(!$this->upload->do_upload("profile_image")) 
    					{
    						$error = $this->upload->display_errors();
    						$this->session->set_flashdata("error",$error." for profile picture");				
    						redirect(current_url());
    					}
    					else
    					{
    						$profImage = 'uploads/Member/thumb/'.$new_file;
    					}
    				}
    			}
    			else if(isset($postData["profile_image_old"]))   
    			{          	 

    				$profImage =$postData["profile_image_old"];		        				
    			}
    			else
    			{

    				$profImage ="";	
    			}
    			

				// ---- update data -------
    			$updateArr  = array(
    				'full_name'=>$postData['full_name'],
    				'date_of_birth' => date("Y-m-d",strtotime($postData['date_of_birth'])),
    				'phone' => $postData['phone_no'],	
    				'street' => trim($postData['street']),
					// 'country' => $postData['country'],
    				'state' => $postData['state'],
    				'city'=>$postData['city'],
    				'zipcode' => trim($postData['zipcode']),
    				'county' => $postData['county'],											
    				'emergency_contactname' => trim($postData['emergency_contact_name']),
    				'emergency_contactno' => $postData['emergency_contact_number'],	
    				'signature_file_upload'=>$signDoc,	
    				'picture'=>$profImage,							
    				'signature'=>$postData['signature'],								
    				'updated_at'=>date("Y-m-d H:i:a")
    				);
    			$where = array('id' =>$this->session->userdata('user_id'));
    			$result = $this->Common_model->update(TB_USERS,$where,$updateArr);
    			if($result)
    			{
    				$this->session->set_flashdata("success","Profile updated successfully");
    				redirect("member-profile");
    			}
    			else
    			{
    				$this->session->set_flashdata("fail","Oops!!! Something went wrong.");				
    				redirect(current_url());
    			}	
    		}
    		
    	}else
    	{
    		redirect("frontend-login"); exit;
    	}
    }

    // ---------- update Stripe card Details ----------
    public function updateStripeDetails()
    {
    	if($this->session->userdata('user_id'))
    	{    		
    		require_once APPPATH."third_party/stripe/init.php";
    		\Stripe\Stripe::setApiKey(STRIPE_SECRET_KEY);
    		if (isset($_POST['stripeToken']))
    		{

    			try {
    				$cu = \Stripe\Customer::retrieve($_POST['customer_id']);
    				$cu->source = $_POST['stripeToken'];
    				$cu->save();
    				$this->session->set_flashdata('success', "Card details have been updated successfully.");
    			} catch (\Stripe\Error\Card $e) {
    				$body = $e->getJsonBody();
    				$err  = $body['error'];
    				$error = $err['message'];
    				$this->session->set_flashdata('error', $error);    				
    			}
    		}else
    		{
    			$this->session->set_flashdata('error', "Oops!!! Something went wrong with stripe.");
    		}
    		redirect('edit-member-profile');

    	}else
    	{
    		redirect("frontend-login"); exit;
    	}
    }


}
?>