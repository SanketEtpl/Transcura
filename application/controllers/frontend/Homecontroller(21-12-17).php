<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed'); 

class Homecontroller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_model');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		$this->load->helper('security');
		$this->load->helper('email_template_helper');
		$this->load->library('upload');		
	}

	public function index()
	{	
		// record fetch from db show for dropdownbox dynamically	
		$data['testimonial'] = $this->Common_model->select('*',TB_TESTIMONIAL);		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
		$data['home_section_about'] = $this->Common_model->selectWhereIn('page_id,sub_title,page_title,page_detail,image_icon_upload',TB_PAGES,'page_id',array(2,7));
		//$data['home_section1'] = $this->Common_model->selectWhereIn('page_id,sub_title,page_title,page_detail,image_icon_upload',TB_PAGES,'page_id',array(2,14,7,20,21,22,23,24,25,26,27,28,29,30));				
		$data['home_section1'] = $this->Common_model->select('fs_id,fs_title,fs_description,fs_icon_image',TB_FIRST_SECTION);
		$data['home_section2'] = $this->Common_model->select('ss_id,ss_title,ss_description,ss_icon_image',TB_SECOND_SECTION);		
		$this->load->view('user/index',$data);		
	}

	// Homepage read more section
	public function homepage_section()
	{
		$whereCond = array('page_id'=>'2');
		$data['aboutData'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title,image_icon_upload",TB_PAGES,$whereCond);		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
		$this->load->view('user/homepage_section',$data);
	}

	//get state name
	public function get_state()
	{		
		$postData = $this->input->post();
		$countryID=array('country_id'=>$postData['countryID']);
		$dataState = $this->Common_model->select("*",TB_STATE,$countryID);
		$stateJSON='';
		// if record are exist
		if($dataState > 0)
		{
			$stateJSON .= '<option value="">Select state</option>';
       		foreach ($dataState as $key => $state_name) {
       			$stateJSON.= '<option value="'.$state_name['id'].'">'.$state_name['state_name'].'</option>';
       		}       		
		}
		else
		{
			$stateJSON.= '<option value="">State not available</option>';
		}
		// convert in json format
		echo json_encode(array('state'=>$stateJSON));		
	}

	//get city name
	public function get_city()
	{
		$postData = $this->input->post();
		$stateID = array('state_id'=>$postData['state_id']);
		$dataCity = $this->Common_model->select("*",TB_CITY,$stateID);
		$cityJSON = '';
		if($dataCity > 0)
		{
			$cityJSON .= '<option value="">Select city</option>';
			foreach ($dataCity as $key => $city) {
				$cityJSON.= '<option value="'.$city['id'].'">'.$city['city_name'].'</option>';
			}
		}
		else
		{
			$cityJSON .= '<option value="">City not available</option>';
		}
		// convert in json format
		echo json_encode(array('city'=>$cityJSON));
	}

	// check email availability 
	public function check_email_availability()
	{
		$email = $this->input->post('email');
		$usertype = $this->input->post('usertype');
		// check email & user type are already exist or not
		$data = $this->Common_model->select('id,email',TB_USERS,array('email'=>$email));
		if($data)
		{
			$this->output
		    ->set_content_type("application/json")
		    ->set_output(json_encode(array('status'=>true,'message' => 'Email ID already exists.')));		    
		}
		else
		{
			$this->output
		    ->set_content_type("application/json")
		    ->set_output(json_encode(array('status'=>false)));		    			    
		}			
	}

	// Call registration form
	public function registration()
	{
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);	
		$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);	
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));				
		$this->load->view('user/registration',$data);		
	}

	// submit register data in table
	public function form_registration()
	{	
		$this->load->helper('email_template_helper');		
		$this->load->library('upload');
		$socialDoc='';
		$driverDoc='';
		$drugTestDoc='';
		$criminalBkDoc='';
		$motorVehicleDoc='';
		$clearanceDoc ='';
		$vehicleInspectionDoc='';
		$vehiclMaintDoc='';
		$vehicleInsuranceDoc='';
		$vehicleRegisDoc='';
		$dynamicPath='';
		$ACED=$MVRED=$CBKED=$DTED=$DLED=$VRED=$VIED=$VehiclInsDt='';
		$postData = $this->input->post();
		if(!empty($postData['act_33_34_clearance_expiry_date'])){
			$ACED=date("Y-m-d",strtotime($postData['act_33_34_clearance_expiry_date']));
		}else{ $ACED = '0000-00-00'; }
		if(!empty($postData['motor_vehile_record_expiry_date'])){
			$MVRED=date("Y-m-d",strtotime($postData['motor_vehile_record_expiry_date']));
		}else{ $MVRED = '0000-00-00'; }
		if(!empty($postData['criminal_bk_expiry_date'])){
			$CBKED = date("Y-m-d",strtotime($postData['criminal_bk_expiry_date']));
		}else{ $CBKED='0000-00-00'; }

		if(!empty($postData['drug_test_expiry_date'])){
			$DTED = date("Y-m-d",strtotime($postData['drug_test_expiry_date']));
		}else{ $DTED = '0000-00-00'; }

		if(!empty($postData['driving_license_expiry_date'])){
			$DLED=date("Y-m-d",strtotime($postData['driving_license_expiry_date']));
		}else{ $DLED ='0000-00-00'; }

		if(!empty($postData['vehicle_inspection_expiry_date'])){
			$VehiclInsDt=date("Y-m-d",strtotime($postData['vehicle_inspection_expiry_date']));
		}else{ $VehiclInsDt='0000-00-00'; }

		if(!empty($postData['vehicle_insurance_expiry_date'])){
			$VIED=date("Y-m-d",strtotime($postData['vehicle_insurance_expiry_date']));
		}else{ $VIED='0000-00-00'; }

		if(!empty($postData['vehicle_registration_expiry_date']))
			{ $VRED = date("Y-m-d",strtotime($postData['vehicle_registration_expiry_date'])); }else{ $VRED='0000-00-00'; }
		if($postData['user_type'] == 2)
		{
			$dynamicPath ='NEMT';
		}	
		else if($postData['user_type'] == 5)
		{
			$dynamicPath = 'Driver';
		}
		else{ }
		$uploadFiles =array();
		$uploadFiles =$_FILES;
		if(!empty($uploadFiles))
		{
			foreach ($uploadFiles as $key => $value) {		
				if(empty($value['name'])){
					continue;
				}else{
					if(isset($_FILES[$key]["name"]))   
					{   
				        $file_name = $_FILES[$key]["name"];
				        $original_file_name = $file_name;
				        $date =date("Y_m_d_H_s_i");
				        $random = rand(1, 10000000000000000);
				        $makeRandom = $random;
				        $file_name_rename = $makeRandom;
				        $explode = explode('.', $file_name);
				        if(count($explode) >= 2) 
				        {
							$new_file = $file_name_rename.$date.'.'.$explode[1];
							$config['upload_path'] = './uploads/'.$dynamicPath.'/doc';
							$config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
							$config['file_name'] = $new_file;
							$this->load->library('upload',$config);
							$this->upload->initialize($config);
							if(!$this->upload->do_upload($key)) 
							{
								$error = $this->upload->display_errors();
								echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); //exit;	
							}
							else
							{
								if(strcmp($key,'social_security_card_doc')==0)
								{
									$socialDoc = 'uploads/'.$dynamicPath.'/doc/'.$new_file;	
								}
								elseif(strcmp($key,'driving_license_doc')==0)
								{
									$driverDoc = 'uploads/'.$dynamicPath.'/doc/'.$new_file;
								}
								elseif(strcmp($key,'drug_test_doc')==0)
								{
									$drugTestDoc = 'uploads/'.$dynamicPath.'/doc/'.$new_file;
								}
								elseif(strcmp($key,'criminal_bk_doc')==0)
								{
									$criminalBkDoc = 'uploads/'.$dynamicPath.'/doc/'.$new_file;
								}
								elseif(strcmp($key,'motor_vehicle_details_doc')==0)
								{
									$motorVehicleDoc = 'uploads/'.$dynamicPath.'/doc/'.$new_file;
								}
								elseif(strcmp($key,'act_33_34_clearance_doc')==0)
								{
									$clearanceDoc = 'uploads/'.$dynamicPath.'/doc/'.$new_file;
								}
								elseif(strcmp($key,'vehicle_inspection_doc')==0)
								{
									$vehicleInspectionDoc = 'uploads/'.$dynamicPath.'/doc/'.$new_file;
								}
								elseif(strcmp($key,'vehicle_maintainance_record_doc')==0)
								{
									$vehiclMaintDoc = 'uploads/'.$dynamicPath.'/doc/'.$new_file;
								}
								elseif(strcmp($key,'vehicle_insurance_record_doc')==0)
								{
									$vehicleInsuranceDoc = 'uploads/'.$dynamicPath.'/doc/'.$new_file;
								}
								elseif(strcmp($key,'vehicle_registration_doc')==0)
								{
									$vehicleRegisDoc = 'uploads/'.$dynamicPath.'/doc/'.$new_file;
								}else{}								
							}
				        }
					}
				}
			}
		}
		else
		{
			$socialDoc='';
			$driverDoc='';
			$drugTestDoc='';
			$criminalBkDoc='';
			$motorVehicleDoc='';
			$clearanceDoc ='';
			$vehicleInspectionDoc='';
			$vehiclMaintDoc='';
			$vehicleInsuranceDoc='';
			$vehicleRegisDoc='';
		}
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);	
		$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);	
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$token = "";						
		$token=  random_string('alnum',20);						
		$insertArr = array(
						'user_token' => trim($token),
						'user_type' => trim($postData['user_type']),								
						'full_name'  => trim($postData['full_name']),
						'date_of_birth' => date("Y-m-d",strtotime($postData['date_of_birth'])),
						'email'      => trim($postData['email']),
						'password'   => trim(md5($postData['password'])),								
						'token'      => $token,
						'username'	 =>	trim($postData['username']),					 
						'created_at' => date('Y-m-d H:i:s'),
						'status'     => "inactive",
						'phone'      => trim($postData['phone_no']),
						'done'		 => 1,
						'country'    => $postData['country'],
						'state'		 => $postData['state'],
						'city'		 => $postData['city'],
						'county'	 => $postData['county'],
						'street'	 => trim($postData['street']),
						'zipcode'	 => ($postData['zipcode']),						
						'insurance_id' => trim($postData['insurance_id']),
						'emergency_contactname' => trim($postData['eme_name']),
						'emergency_contactno' => $postData['eme_number'],
						'signature'	=> $postData['signature'],
						'social_security_card_doc' => $socialDoc,
						'driver_license_doc'=>$driverDoc,
						'driver_license_expire_dt'=> $DLED,
						'drug_test_results_doc'=>$drugTestDoc,
						'drug_test_results_expire_dt'=>$DTED,
						'criminal_back_ground_doc'=>$criminalBkDoc,
						'criminal_back_ground_expire_dt'=>$CBKED,
						'motor_vehicle_record_doc'=>$motorVehicleDoc,
						'motor_vehicle_record_expire_dt'=>$MVRED,
						'act_33_&_34_clearance_doc'=>$clearanceDoc,
						'act_33_&_34_clearance_expire_dt'=>$ACED,
						'vehicle_inspections_doc'=>$vehicleInspectionDoc,
						'vehicle_inspections_expire_dt'=>$VehiclInsDt,
						'vehicle_maintnc_record_doc'=>$vehiclMaintDoc,
						//'vehicle_maintnc_record_expire_dt'=>$postData['eme_number'],
						'vehicle_insurance_doc'=>$vehicleInsuranceDoc,
						'vehicle_insurance_expire_dt'=>$VIED,
						'vehicle_regi_doc'=>$vehicleRegisDoc,
						'vehicle_regi_expire_dt'=>$VRED
					);
			// Record insert in user table
		$result = $this->Common_model->insert(TB_USERS,$insertArr);	
	
		if($result)
		{
			// Email to registration 
			$hostname = $this->config->item('hostname');
			$config['mailtype'] ='html';
			$config['charset'] ='iso-8859-1';
			$this->email->initialize($config);
			$from  = EMAIL_FROM; 
			$name = trim($postData['full_name']);
			$email = trim($postData['email']);
			$userType = $postData['user_type'];
			$this->messageBody  = email_header();
			$base_url = $this->config->item('base_url');
			$reset_url = $base_url."accountverify/".$token ;			
			$this->messageBody  .= '<tr>
					<td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
					    <p>Dear Customer,</p>
						<p>
							You have registered successfully on Transcura.<br> Please click the link given below to verify your email address</p>
						<p>
						<p>Activation Link : </p>
							<a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href='.$reset_url.'>Click Here</a>
						</p><br /><br />
						<p>If the above link does not work, please copy and paste the following link into your browser.</p>
						<p>Copy the Link : </p>
							<a href="'.$reset_url.'">'.$reset_url.'</a>
						</p>					
					</td>
				</tr>'; 			
		    $this->messageBody  .= email_footer();
		    $this->email->from($from, $from);
		    $this->email->to($email);
		    $this->email->subject('Welcome to Transcura');
		    $this->email->message($this->messageBody);
		    $this->email->send();
		    $this->output
		    ->set_content_type("application/json")
		    ->set_output(json_encode(array('status'=>true,'message' => 'Your registration has been successfully, Please check your email for activation.', 'redirect'=>base_url('frontend-login'))));
		    $this->session->set_flashdata("auth_reg_success", "Your registration has been successfully, Please check your email for activation.");	   
		}
		else
		{
			$this->output
            ->set_content_type("application/json")
            ->set_output(json_encode(array('status'=>false, 'message'=>'Fail to registration, please try again.','redirect'=>base_url('registration'))));			
            $this->session->set_flashdata("auth_reg_fail", "Fail to registration, please try again.");	   
		}				
	}

	// Call reset password form
	public function reset_password()
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->load->view('user/reset_password',$data);		
	}

	// Set forgot password
	public function set_reset_password()
	{	
		$this->load->helper('email_template_helper');
		$email = $this->input->post('email');
		$userdata = $this->Common_model->select("`id`,`first_name`,`email`,`token`,`password`,`phone`,`Username`,`status`",TB_USERS,array("email"=>trim($email)));
		
		//check user is active or deactive
		if(empty($userdata))
		{
			$this->output
		    ->set_content_type("application/json")
		    ->set_output(json_encode(array('status'=>false,'message' => 'Your account does not exist.')));	
		}
		else if($userdata[0]['status']=='inactive')
		{	
			$this->output
		    ->set_content_type("application/json")
		    ->set_output(json_encode(array('status'=>false,'message' => 'Your account is deactive, please cantact to admin.')));
		}        
		else
		{			
			// Email send to the user for verification			
			$userId = base64_encode($userdata[0]['id']);
			$token = $userdata[0]['token'];
			$reset_token = random_string('unique');
			$reset_url = base_url()."reset-user-password/".$userId."/".$reset_token;	

			$updateData = array('token' =>$reset_token);
			$where = array('id' =>$userdata[0]['id'] ); 
    		$result = $this->Common_model->update(TB_USERS,$where,$updateData); 
    		$hostname = $this->config->item('hostname');
			$config['mailtype'] ='html';
			$config['charset'] ='iso-8859-1';
			$this->email->initialize($config);
			$from  = EMAIL_FROM; 
			$this->messageBody  = email_header();
			$this->messageBody  .= '<tr> 
		        <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
		            <p>Hello,</p>
		                <p>
		                A request has been made to reset your password for Transcura account.<br>
		                Please click the link below to reset your password.</p>
		                <p>
		                <p>Reset Password Link : </p>
		                <a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href="'.$reset_url.'">Reset Password</a>
		                </p>
				<p>
		                If the above link does not work, please copy and paste the following link into your browser.</p>
		                <p>Copy link :</p>
		                <a href="'.$reset_url.'">'.$reset_url.'</a>
		                
		            </td>
		        </tr>
		        ';                      
		    $this->messageBody  .= email_footer();
		    $this->email->from($from, $from);
		    $this->email->to($email);
		    $this->email->subject('Reset Password Link');
		    $this->email->message($this->messageBody);
		    if($this->email->send()) 
		    {
		        $this->output
			    	 ->set_content_type("application/json")
			    	 ->set_output(json_encode(array("status"=>true)));
		    }       
        	else
        	{
        		$this->output
        			 ->set_content_type
        			 ->set_output(json_encode(array("status"=>false,"message"=>"Fail send message, please try again")));
        	}
       	}
	}

	// Password reset by getting id and token
    public function resetPassword($id,$token)
    {

		$id = base64_decode($id);			
		$userdata = $this->Common_model->select("id,token,status",TB_USERS,array("id"=>trim($id),"token"=>trim($token),"status"=>"active"));
		$data['mydata']=array('token'=>$token,'id'=>$id);		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		//print_r($data);exit;	
    	if(count($userdata) > 0 )
		{
			// Clear token for one time user change password 
			$datareset = array('token' =>'');
			$cond1 = array('id' =>$id); 
    		$result = $this->Common_model->update(TB_USERS,$cond1,$datareset);    		
    		$this->load->view("user/change_password",$data);
		}
		else
		{
		  	$this->session->set_flashdata('forgot__error_msg', 'You reset password link has been expired, already use!');
			redirect("reset_password");
		}
	}
	// Call reset password form
	public function change_password()
	{		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->load->view('user/change_password',$data);	
	}

	// Update token
	function update_token()
	{
		$ftoken=$this->input->post('token');
		$fid = $this->input->post('id');
		$datareset1 = array('token' =>$ftoken);
		$cond1 = array('id' =>$fid); 
    	$result = $this->Common_model->update(TB_USERS,$cond1,$datareset1);
	}
	// Change user password 
    public function changePassword()
    {
		$ftoken=$this->input->post('token');
		$fid = $this->input->post('id');
	    $postData = $this->input->post();
	    $data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$userdata = $this->Common_model->select("`id`,`first_name`,`email`,`token`,`password`,`phone`,`Username`",TB_USERS,array("id"=>trim($postData['id']),"status"=>"active"));
	    
		// Form validation	    
		$this->form_validation->set_rules('newpassword', 'Password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('confirm_password', 'Password confirmation', 'trim|required|matches[newpassword]',array('matches'=>'New password & Confirm password do not match.'));
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$data['mydata']=array('id'=>$postData['id'],'token'=>$postData['token']);
		if ($this->form_validation->run() == FALSE)
		{	
			// If user fail to change password then previous token update for verification		
			$this->update_token();
			$this->load->view('user/change_password',$data);			
		}
		else
		{      	 
			// Check user get data is empty or not 	
			if (empty($userdata)) {
 				$this->update_token(); 				
 				$this->load->view('user/change_password',$data);
 			}
			else
			{
				// Check user id correct or not
				if(trim($userdata[0]['id']) == trim($postData['id']))
				{
					// Update token
					$generateToken=  random_string('alnum',20);
					$datareset = array('password' =>  md5($postData['newpassword']),'token' => $generateToken);
					$cond1 = array('id' =>  $postData['id']); 
					// Chnage password successfully
			    	$result = $this->Common_model->update(TB_USERS,$cond1,$datareset);
						
					if($result)
					{
						// success then redirect login page
						$this->session->set_flashdata('change_success_msg', 'Your password has been changed successfully.');
						redirect('frontend-login');
					}
					else
					{	
						$this->update_token();
						$this->session->set_flashdata('fail_change_msg', 'Error occured,Please try again.');
						$this->load->view('user/change_password',$data);		 				
					}
				}
				else
				{			
					$this->update_token();
					$this->session->set_flashdata('fail_change_msg', 'Please enter valid credential,Please try again.');								
					redirect('reset-user-password/'.base64_encode($fid).'/'.$ftoken,$data);
				}
			}					
		}
    }

	// Call login form 
	public function login()
	{
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);	
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));		
		$this->load->view('user/login',$data);	
	}	

	// User login
	public function user_login()
	{		
		$this->load->model('Login_model');
		// Login form validation				
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);	
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->form_validation->set_rules('username', 'Email', 'required|valid_email',array('valid_email'=>'Please enter a valid email address'));
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('user_type', 'User-type', 'required',array('required'=>'Please select User type'));
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
		if ($this->form_validation->run() == FALSE)
	        { 	
			$this->load->view('user/login',$data);			
		}
		else
		{		
			$user_login = array(
				'user_email' => trim($this->input->post('username')),
				'user_password'	=> md5($this->input->post('password')),
				'user_type'	=> $this->input->post('user_type')									
			);

			// Check user login credential are exit or not		
			$data = $this->Login_model->login_user($user_login['user_email']);	
	//echo $this->db->last_query();					
	//print_r($data);exit;
	if($data){
		if($data['status'] != 'inactive')
		{
			if($data['email'] == $user_login['user_email'] && $data['password'] == $user_login['user_password'] && $data['user_type'] == $user_login['user_type'] ) {
				//login successfully

				$userType = $this->input->post('user_type');
				$sessionData = array('user_id' => $data['id'],'fullName' => $data['full_name'],'userType' => $data['user_type']);  
				$this->session->set_userdata($sessionData);
				if($userType == 2){
					redirect('my-dashboard');
				}elseif($userType == 5){
					redirect('driver-my-dashboard');
				}else{
					redirect('user');
				}
			} else if($data['email'] == $user_login['user_email'] && $data['password'] == $user_login['user_password'] && $data['user_type'] != $user_login['user_type']){

				//user type error
				$data['userData'] = $this->Common_model->select("*",TB_ROLE);
				$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
				$this->session->set_flashdata('log_error_msg', 'Please select corrrect user type.');
				$this->load->view('user/login',$data);	
			}else if($data['email'] == $user_login['user_email'] && $data['password'] != $user_login['user_password'] && $data['user_type'] == $user_login['user_type']){

				//PASSWORD error
				$data['userData'] = $this->Common_model->select("*",TB_ROLE);
				$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
				$this->session->set_flashdata('log_error_msg', 'Password is incorrect.');
				$this->load->view('user/login',$data);
			}		
		} else{

			$data['userData'] = $this->Common_model->select("*",TB_ROLE);
			$this->session->set_flashdata('log_error_msg', 'Your account is inactive. Contact your administrator to activate it.');
			$this->load->view('user/login',$data);
		}
	} else {

		$data['userData'] = $this->Common_model->select("*",TB_ROLE);
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->session->set_flashdata('log_error_msg', 'Email address or Username does not exist please register.');
		$this->load->view('user/login',$data);
			}
		}	
	}

	// user logout method
	public function user_logout()
	{
		//$session_data = array('user_id'=>'','fullName'=>'','userType'=>'');
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('fullName');
		$this->session->unset_userdata('userType');
		redirect("user");
	}

	// call about form
	public function about()
	{	
		$whereCond = array('page_id'=>'2');
		$data['aboutData'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title,image_icon_upload",TB_PAGES,$whereCond);		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));		
		$this->load->view('user/about',$data);		
	}

	// call about form
	public function services()
	{			
		$whereCond = array('page_id'=>'12');
		$data['servicesData'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title,image_icon_upload",TB_PAGES,$whereCond);
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->load->view('user/services',$data);		
	}

	//call contact form
	public function contact()
	{
		$whereCond = array('page_id'=>'13');
		$data['contactUs'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title,image_icon_upload",TB_PAGES,$whereCond);		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->load->view('user/contact_us',$data);		
	}

	//submit contact form data
	public function save_contact_us()
	{		
		//print_r($_FILES); //exit;
		$postData = $this->input->post();
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$whereCond = array('page_id'=>'13');
		$data['contactUs'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title,image_icon_upload",TB_PAGES,$whereCond);				
		$this->form_validation->set_rules('first_name', 'First name', 'trim|required|alpha|min_length[3]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last name', 'trim|required|alpha|min_length[3]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|max_length[50]|valid_email|xss_clean',array('valid_email'=>'Please enter a valid email address'));
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|max_length[20]|min_length[10]|integer|xss_clean');
		$this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean');		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
		if( $this->form_validation->run() == FALSE )
        {  			        	
			$this->load->view('user/contact_us',$data);				
		}
		else
		{ 
			$insertArr=array(								
				'cu_first_name' => trim($postData['first_name']),
				'cu_last_name' => trim($postData['last_name']),	
				'cu_email' => trim($postData['email']),
				'cu_phone' => trim($postData['phone']),
				'cu_message' => trim($postData['message'])								
			);			
			// Record insert in schedule ride table
			$result = $this->Common_model->insert(TB_CONTACT_US,$insertArr);
			if($result)
			{
				// Email send to the user for verification
				$email =trim($postData['email']) ;               
				$hostname = $this->config->item('hostname');
				$name = $postData['first_name'];
				$config['mailtype'] ='html';
				$config['charset'] ='iso-8859-1';
				$this->email->initialize($config);
				$from  = EMAIL_FROM; 
				$this->messageBody  = email_header();

				$this->messageBody  .= '<tr> 
			        <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
			            <p>Hello, '.$name.'</p>
			                <p>
			                	Thank you for contact us , as soon as possible we will contact you.
			                </p>
			            </td>
			        </tr>
			        <tr>';                      
			    $this->messageBody  .= email_footer();
			    $this->email->from($from, $from);
			    $this->email->to($email);
			    $this->email->subject('Thanks for contact us transcura');
			    $this->email->message($this->messageBody);
			    if($this->email->send()) 
			    {
			    	$this->session->set_flashdata('success', 'As soon as possible we will contact you');
				}       
	        	else
	        	{
	        	    $this->session->set_flashdata('fail', 'Error occured,Please try again.');
				}				
				redirect(current_url());
			}
			else
			{
				$this->session->set_flashdata("fail","Fail to inserted successfully");				
				redirect(current_url());
			}
		}
	}

	//call news form
	public function news()
	{		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->load->view('user/news',$data);			
	}

	//call blogs form
	public function blogs()
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$whereCond = array('page_id'=>'11');
		$data['blogs'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title,image_icon_upload",TB_PAGES,$whereCond);		
		$this->load->view('user/blogs',$data);			
	}

	//call FAQs form
	public function faqs()
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));		
		$data['faqs'] = $this->Common_model->select("id,question,answer",TB_FAQ);
		$this->load->view('user/faqs',$data);		
	}

	//call terms and confidition form
	public function terms_and_conditions()
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$whereCond = array('page_id'=>'3');
		$data['terms_and_conditions'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title,image_icon_upload",TB_PAGES,$whereCond);
		$this->load->view('user/terms_and_conditions',$data);		
	}

	//call privacy policy form
	public function privacy_policy()
	{	$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$whereCond = array('page_id'=>'9');
		$data['privacy_policy'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title,image_icon_upload",TB_PAGES,$whereCond);
		$this->load->view('user/privacy_policy',$data);						
	}	
}

