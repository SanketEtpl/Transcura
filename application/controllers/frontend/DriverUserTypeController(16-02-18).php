<?php if(!defined("BASEPATH")) exit("No direct script access allowed");
class DriverUserTypeController extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->load->library('upload');	
		$this->load->helper('file');
		$this->load->helper(array('form', 'url'));
	}

	// default driver dashboard
	public function index()
	{		
		if($this->session->userdata('user_id'))
		{	
			//echo "hello";exit;
			// echo "eppp";exit;
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			$data['myScheduleRideCount'] = $this->Common_model->select("sr_status,count(*) total",TB_SCHEDULE_RIDE,array('driver_id'=>$this->session->userdata('user_id'),'sr_status'=>'1')); 
			$data['myCompleteRideCount'] = $this->Common_model->select("sr_status,count(*) total",TB_SCHEDULE_RIDE,array('driver_id'=>$this->session->userdata('user_id'),'sr_status'=>'2')); 
			$data['myCancelledRideCount'] = $this->Common_model->select("sr_status,count(*) total",TB_SCHEDULE_RIDE,array('driver_id'=>$this->session->userdata('user_id'),'sr_status'=>'3')); 
			$data['userDetails']=$this->Common_model->select('picture,full_name,email,user_type',TB_USERS,array('id'=>$this->session->userdata('user_id')));
			//print_r($data['myCancelledRideCount']);exit;
			$this->load->view("driverUser/driverDashBoard",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}


// driver my profile
	public function driver_my_profile()
	{		
		// echo "eppp";exit;
		/*ini_set("display_errors","1");
		error_reporting(E_ALL);
		exit;*/
		$data =array();
		if($this->session->userdata('user_id'))
		{	
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section

			$id = $this->session->userdata('user_id');
			$checkData = $this->Common_model->FetchDriverUserDataById($id);	
			//echo "<pre>";print_r($checkData);die;		
			if(!empty($checkData) && $checkData != "")
			{
				$data['driveUserProfile'] = $this->Common_model->FetchDriverUserDataById($id);			
			}
			else
			{
				$data['driveUserProfile'] = $this->Common_model->FetchDriverUserDataById_WithoutServiceType($id);	
			}			
			/*echo $this->db->last_query();
			print_r($data['driveUserProfile']);exit;*/
			$this->load->view("driverUser/driverProfile",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	 /* @author : Smita
	* Function Use : driver_edit_profile function use for show edit profile form
	* Date : 12-1-2018
	*/
	public function driver_edit_user_profile(){
		//echo "heo";exit;
		//echo "<pre>";print_r($this->session->userdata('user_id'));die;
		if($this->session->userdata('user_id'))
		{
			$data = array();	
			$id	= $this->session->userdata('user_id');

			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section

			$driveUserProfile = $this->Common_model->FetchDriverUserDataById($id);
			/*echo "hello";
			print_r($driveUserProfile);exit;*/
			if(!empty($driveUserProfile) || $driveUserProfile =='')
			{

				$data['driveUserProfile'] = $this->Common_model->FetchDriverUserDataById($id);
				$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY,array('id'=>231));
				
				if(isset($data['driveUserProfile'][0]['country']))
				{
					$data['stateData'] = $this->Common_model->select("*",TB_STATE,array('country_id'=>231));
				}

				if(isset($data['driveUserProfile'][0]['state']))
				{
					$data['cityData'] = $this->Common_model->select("*",TB_CITY,array('state_id'=>$data['driveUserProfile'][0]['state']));
				}
			}else{
				$data['driveUserProfile'] = $this->Common_model->FetchDriverUserDataById_WithoutServiceType($id);
				$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY,array('id'=>231));
				
				if(isset($data['driveUserProfile'][0]['country']))
				{
					$data['stateData'] = $this->Common_model->select("*",TB_STATE,array('country_id'=>231));
				}

				if(isset($data['driveUserProfile'][0]['state']))
				{
					$data['cityData'] = $this->Common_model->select("*",TB_CITY,array('state_id'=>$data['driveUserProfile'][0]['state']));
				}
			}
			
			
	 		$data['typeOfServices'] = $this->Common_model->select("type_title,type_id",TB_SERVICE_TYPE);		
			
			$this->load->view("driverUser/driverEditProfile",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	/* @author : smita
	 * @function use : this function use for edit a driver profile
	 * @date : 14-1-2018
	 */
	public function update_driver_edit_user_profile()
	{		
		
		$data = array();
		$this->load->library('upload');
		if($this->session->userdata('user_id'))
		{	
			$userid = $this->session->userdata('user_id');
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			//sk add query : 12-1-2018
			if($this->input->post('edit_profile_save') == 'Submit' && $this->input->post('user_id')!= '') {

			$profileImage = '';
			$socialDoc='';
			$driverDoc='';
			$drugTestDoc='';
			$criminalBkDoc='';
			$motorVehicleDoc='';
			$clearanceDoc ='';
			$vehicleInspectionDoc='';
			$vehiclMaintDoc='';
			$vehicleInsuranceDoc='';
			$vehicleRegisDoc='';
			$dynamicPath='';
			$ownBusVehicleRegisDoc = '';
			$insurancePolicyDoc ='';
			$inspectionDoc ='';
			$taxidEinoDoc='';
			$signatureFile = '';
			$extention = '';
			$allowedTypeArr =  array();
			$filesError = '';
			$ACED=$MVRED=$CBKED=$DTED=$DLED=$VRED=$VIED=$VehiclInsDt='';

			if(!empty($this->input->post('act_33_34_clearance_expiry_date'))){
				$ACED=date("Y-m-d",strtotime($this->input->post('act_33_34_clearance_expiry_date')));
			}else{ $ACED = '0000-00-00'; }
			if(!empty($this->input->post('motor_vehile_record_expiry_date'))){
				$MVRED=date("Y-m-d",strtotime($this->input->post('motor_vehile_record_expiry_date')));
			}else{ $MVRED = '0000-00-00'; }
			if(!empty($this->input->post('criminal_bk_expiry_date'))){
				$CBKED = date("Y-m-d",strtotime($this->input->post('criminal_bk_expiry_date')));
			}else{ $CBKED='0000-00-00'; }

			if(!empty($this->input->post('drug_test_expiry_date'))){
				$DTED = date("Y-m-d",strtotime($this->input->post('drug_test_expiry_date')));
			}else{ $DTED = '0000-00-00'; }

			if(!empty($this->input->post('driving_license_expiry_date'))){
				$DLED=date("Y-m-d",strtotime($this->input->post('driving_license_expiry_date')));
			}else{ $DLED ='0000-00-00'; }

			if(!empty($this->input->post('vehicle_inspection_expiry_date'))){
				$VehiclInsDt=date("Y-m-d",strtotime($this->input->post('vehicle_inspection_expiry_date')));
			}else{ $VehiclInsDt='0000-00-00'; }

			if(!empty($this->input->post('vehicle_insurance_expiry_date'))){
				$VIED=date("Y-m-d",strtotime($this->input->post('vehicle_insurance_expiry_date')));
			}else{ $VIED='0000-00-00'; }

			if(!empty($this->input->post('vehicle_registration_expiry_date')))
			{ $VRED = date("Y-m-d",strtotime($this->input->post('vehicle_registration_expiry_date'))); }else{ $VRED='0000-00-00'; }

			$dynamicPath = 'Driver';
			$uploadFiles =array();
			$uploadFiles =$_FILES;

			if(!empty($uploadFiles))
			{

				foreach ($uploadFiles as $key => $value) {	
					if(empty($value['name'])){
					continue;
					}else{
						if(isset($_FILES[$key]["name"])){   
							$file_name = $_FILES[$key]["name"];
							$original_file_name = $file_name;
							$date =date("Y_m_d_H_s_i");
							$random = rand(1, 10000000000000000);
							$makeRandom = $random;
							$file_name_rename = $makeRandom;
							$explode = explode('.', $file_name);
							if(count($explode) >= 2){
								$new_file = $file_name_rename.$date.'.'.$explode[1];
								$config['upload_path'] = './uploads/'.$dynamicPath.'/doc';
								$config['allowed_types'] ="png|jpeg|jpg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
								//$config['max_size'] = '80000000'; 
								$config['file_name'] = $new_file;

								$this->load->library('upload',$config);
								$this->upload->initialize($config);
								if(!$this->upload->do_upload($key)){
									$error = $this->upload->display_errors();
									$filesError =  json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); 
								}else{
									if(strcmp($key,'profile_image')==0){
									//$profileImage = 'uploads/'.$dynamicPath.'/doc/'.$new_file;	
									$profileImage = $new_file;
									}
									if(strcmp($key,'social_security_card_doc')==0){
										$socialDoc = $new_file;	
									}elseif(strcmp($key,'driving_license_doc')==0){
										$driverDoc = $new_file;
									}elseif(strcmp($key,'drug_test_doc')==0){
										$drugTestDoc = $new_file;
									}elseif(strcmp($key,'criminal_bk_doc')==0){
										$criminalBkDoc = $new_file;
									}elseif(strcmp($key,'motor_vehicle_details_doc')==0){
										$motorVehicleDoc = $new_file;
									}elseif(strcmp($key,'act_33_34_clearance_doc')==0){
										$clearanceDoc = $new_file;
									}elseif(strcmp($key,'vehicle_inspection_doc')==0){
										$vehicleInspectionDoc = $new_file;
									}elseif(strcmp($key,'vehicle_maintainance_record_doc')==0){
										$vehiclMaintDoc = $new_file;
									}elseif(strcmp($key,'vehicle_insurance_record_doc')==0){
										$vehicleInsuranceDoc = $new_file;
									}elseif(strcmp($key,'vehicle_registration_doc')==0){
										$vehicleRegisDoc = $new_file;
									}elseif(strcmp($key, 'own_vehicle_reg_doc')==0){
										$ownBusVehicleRegisDoc = $new_file;
									}elseif(strcmp($key, 'insurance_policy_doc')==0) {
										$insurancePolicyDoc = $new_file;
									}elseif(strcmp($key, 'inspection_doc')==0){
										$inspectionDoc = $new_file;
									}elseif(strcmp($key, 'tax_id_or_ein_no')==0){
										$taxidEinoDoc = $new_file;
									}elseif(strcmp($key,'sign_file_upload') == 0){
										$signatureFile = $new_file;
									}else{}								
								}
							}
						}
					}
				}
			}	
			$data['picture']= $profileImage;
			$data['full_name'] = $this->input->post('full_name');
			$data['date_of_birth'] = $this->input->post('date_of_birth');
			$data['user_id'] =$this->input->post('user_id');
			$data['phone_no']= $this->input->post('phone_no');
			$data['country']=$this->input->post('country');
			$data['state'] = $this->input->post('state');
			$data['city']= $this->input->post('city');
			$data['zipcode'] = $this->input->post('zipcode');
			$data['county'] = $this->input->post('county');
			$data['street'] = $this->input->post('street');
			$data['company_name'] = $this->input->post('company_name');
			$data['company_address'] = $this->input->post('company_address');
			if($profileImage!= ''){					
				$data['picture'] = $profileImage;
			}else{
				$data['picture'] = $this->input->post('profile_image_old');
			}

			$data['personal_doc_status'] = trim($this->input->post('pers_doc_dd'));
			if($data['personal_doc_status'] == 2){
				$data['insurance_id'] = '';
				$data['social_security_card_doc'] = '';
				$data['driver_license_doc'] = '';
				$data['drug_test_results_doc'] = '';
				$data['criminal_back_ground_doc'] = '';
				$data['motor_vehicle_record_doc'] = '';
				$data['act_33_and_34_clearance_doc'] = '';
			}else{
				$data['insurance_id'] = $this->input->post('insurance_id');
				if($socialDoc != ''){
					$data['social_security_card_doc'] = $socialDoc; 
				}else{
					if($filesError != ''){							
						$fliesJsonArr = json_decode($filesError);
						$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
						redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
					}else{
						$data['social_security_card_doc'] = $this->input->post('social_security_card_doc_old');	
					}
				}

				if($driverDoc!= ''){					
				$data['driver_license_doc']= $driverDoc;					
				}else{
					if($filesError != ''){							
						$fliesJsonArr = json_decode($filesError);
						$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
						redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
					}else{
						$data['driver_license_doc'] = $this->input->post('driver_license_doc_old');
					}
				}

				if($drugTestDoc!= ''){
				$data['drug_test_results_doc']=$drugTestDoc;
				}else{
					if($filesError != ''){							
						$fliesJsonArr = json_decode($filesError);
						$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
						redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
					}else{
						$data['drug_test_results_doc'] = $this->input->post('drug_test_results_doc_old');
					}
				}

				if($criminalBkDoc!= ''){
				$data['criminal_back_ground_doc']=$criminalBkDoc;
				}else{
					if($filesError != ''){							
						$fliesJsonArr = json_decode($filesError);
						$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
						redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
					}else{
						$data['criminal_back_ground_doc'] = $this->input->post('criminal_back_ground_doc_old');
					}
				}

				if($motorVehicleDoc!= ''){
					$data['motor_vehicle_record_doc']=$motorVehicleDoc;
				}else{
					if($filesError != ''){							
						$fliesJsonArr = json_decode($filesError);
						$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
						redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
					}else{
						$data['motor_vehicle_record_doc'] = $this->input->post('motor_vehicle_record_doc_old');
					}	
				}

				if($clearanceDoc!= ''){
					$data['act_33_and_34_clearance_doc']=$clearanceDoc;
				}else{
					if($filesError != ''){							
						$fliesJsonArr = json_decode($filesError);
						$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
						redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
					}else{
						$data['act_33_and_34_clearance_doc'] = $this->input->post('act_33_and_34_clearance_doc_old');
					}
				}
			}

			$data['vehicle_doc_status'] =trim($this->input->post('vehicle_reg_documents_id'));
			
			if($data['vehicle_doc_status'] == 2){
				$data['vehicle_inspections_doc'] = '';
				$data['vehicle_maintnc_record_doc'] = '';
				$data['vehicle_insurance_doc'] = '';
				$data['vehicle_regi_doc'] = '';				
			}else{
			if($vehicleInspectionDoc!= ''){
				$data['vehicle_inspections_doc']= $vehicleInspectionDoc;
			}else{
				if($filesError != ''){							
					$fliesJsonArr = json_decode($filesError);
					$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
					redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
				}else{
					$data['vehicle_inspections_doc'] = $this->input->post('vehicle_inspections_doc_old');
				}
			}

			if($vehiclMaintDoc!= ''){
				$data['vehicle_maintnc_record_doc']=$vehiclMaintDoc;
			}else{
				if($filesError != ''){							
					$fliesJsonArr = json_decode($filesError);
					$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
					redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
				}else{
					$data['vehicle_maintnc_record_doc'] = $this->input->post('vehicle_maintnc_record_doc_old');
				}
			}

			if($vehicleInsuranceDoc!= ''){					
				$data['vehicle_insurance_doc']=$vehicleInsuranceDoc;
			}else{
				if($filesError != ''){							
					$fliesJsonArr = json_decode($filesError);
					$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
					redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
				}else{
					$data['vehicle_insurance_doc'] = $this->input->post('profile_image_old');
				}
			}

			if($vehicleRegisDoc != ''){
				$data['vehicle_regi_doc']=$vehicleRegisDoc;
			}else{
				if($filesError != ''){							
					$fliesJsonArr = json_decode($filesError);
					$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
					redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
				}else{
					$data['vehicle_regi_doc'] = $this->input->post('vehicle_regi_doc_old');
				}
			}

			}

			$data['own_business_status'] =trim($this->input->post('own_vehicle_dd'));
			if($data['own_business_status'] == 2){
				$data['own_bus_vehicle_reg_doc'] = '';
				$data['own_bus_services']= '';
				$data['own_bus_insurance_pol_doc'] = '';
				$data['own_bus_inspection_doc'] = '';
				$data['own_bus_taxid_einno_doc']= '';
				$data['own_buss_vehicle_details'] = '';
			}else{
				$data['own_bus_services']=trim($this->input->post('type_of_sevices'));
			if($ownBusVehicleRegisDoc != ''){
				$data['own_bus_vehicle_reg_doc'] = $ownBusVehicleRegisDoc;	
			}else{
				if($filesError != ''){							
					$fliesJsonArr = json_decode($filesError);
					$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
					redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
				}else{
					$data['own_bus_vehicle_reg_doc'] =$this->input->post('own_bus_vehicle_reg_doc_old');
				}
			}
			if($insurancePolicyDoc != ''){
				$data['own_bus_insurance_pol_doc'] =$insurancePolicyDoc;
			}else{
				if($filesError != ''){							
					$fliesJsonArr = json_decode($filesError);
					$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
					redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
				}else{
					$data['own_bus_insurance_pol_doc'] = $this->input->post('own_bus_insurance_pol_doc_old');
				}
			}
			if($inspectionDoc != ''){
				$data['own_bus_inspection_doc'] = $inspectionDoc;
			}else{
				if($filesError != ''){							
					$fliesJsonArr = json_decode($filesError);
					$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
					redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
				}else{
					$data['own_bus_inspection_doc'] = $this->input->post('own_bus_inspection_doc_old');
				}
			}

			if($taxidEinoDoc != ''){
				$data['own_bus_taxid_einno_doc']=$taxidEinoDoc;
			}else{
				if($filesError != ''){							
					$fliesJsonArr = json_decode($filesError);
					$this->session->set_flashdata('filetypeError',$fliesJsonArr->msg);
					redirect('driver-edit-user-profile/'.$this->session->userdata('user_id')); 
				}else{
					$data['own_bus_taxid_einno_doc'] =$this->input->post('own_bus_taxid_einno_doc_old');
				}
			}
			$data['own_buss_vehicle_details'] = trim($this->input->post('own_buss_vehicle_details'));
			}

			$data['company_vehicle_dropdown'] = trim($this->input->post('company_vehicle_dropdown'));
			if($data['company_vehicle_dropdown'] == 2){
				$data['transport_comp'] = '';
				$data['vehicle_details'] = '';
				$data['com_veh_type_of_sevices'] = '';
			}else{
				$data['transport_comp'] = trim($this->input->post('transport_comp'));
				$data['vehicle_details'] = trim($this->input->post('vehicle_details'));
				$data['com_veh_type_of_sevices'] = trim($this->input->post('com_veh_type_of_sevices'));
			}

			$data['signature'] = trim($this->input->post('signature'));
			if($data['signature'] == 0){
				$data['signature_file_upload'] = '';
			}else{
				if($signatureFile != ''){
					$data['signature_file_upload'] = $signatureFile;
				}else{
					$data['signature_file_upload'] = $this->input->post('sign_file_upload_old');				
				}
			}
		
			$data['driver_license_expire_dt']=$DLED;				
			$data['drug_test_results_expire_dt']=$DTED;				
			$data['criminal_back_ground_expire_dt']=$CBKED;				
			$data['motor_vehicle_record_expire_dt']=$MVRED;				
			$data['act_33_and_34_clearance_expire_dt']=$ACED;				
			$data['vehicle_inspections_expire_dt']=$VehiclInsDt;
			$data['vehicle_insurance_expire_dt']=$VIED;				
			$data['vehicle_regi_expire_dt']=$VRED;
			$data['emergency_contact_name'] = trim($this->input->post('emergency_contact_name'));
			$data['emergency_contact_number'] = trim($this->input->post('emergency_contact_number'));

			$result = $this->Common_model->updateDriverUserProfile($data);
			if($result){
				$this->session->set_flashdata("success","Driver edit profile has been updated successfully.");
				redirect('driver-my-profile', 'refresh');
			}
			else
			{
				$this->session->set_flashdata("fail","Driver edit profile has been failed.");
			}				
			}	
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	public function driver_reset_pwd()
	{
		if($this->session->userdata('user_id'))
		{	
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			$postData = $this->input->post();
			$this->form_validation->set_rules('change_pwd', 'Password', 'required');
			$this->form_validation->set_rules('confirm_pwd', 'Confirm Password', 'required|matches[change_pwd]');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
			if ($this->form_validation->run() == FALSE)
			{ 	
				$this->load->view("driverUser/change_pwd",$data);		
			}
			else
			{	
				$updateData = array('password' =>md5($postData['change_pwd']));
				$where = array('id' =>$this->session->userdata('user_id')); 
				$result = $this->Common_model->update(TB_USERS,$where,$updateData); 
				if($result)
				{
					$this->session->set_flashdata('success', 'Your passowrd has been changed successfully.');
					$this->load->view("driverUser/change_pwd",$data);		
				}
				else
				{
					$this->session->set_flashdata('fail', 'Your passowrd has not been changed successfully.');
					$this->load->view("driverUser/change_pwd",$data);		    	
				}    			
			}					
		} 
		else 
		{
			redirect("frontend-login"); exit;
		}
	}

	// driver my trip assignments
	public function driver_my_trip_assignment()
	{		
		if($this->session->userdata('user_id'))
		{	
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			$this->load->view("driverUser/driverMyTripAssignment",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	// driver my trip request
	public function driver_my_trip_request()
	{		
		if($this->session->userdata('user_id'))
		{				
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section

			//---------- ride request ---------
			$cond = array("tbl_driver_request.driver_id" => $this->session->userdata('user_id'),"tbl_schedule_ride.driver_status"=>"1");
			$jointype=array("tbl_schedule_ride"=>"LEFT","tbl_users"=>"LEFT");
			$join = array("tbl_schedule_ride"=>"tbl_schedule_ride.id = tbl_driver_request.sched_id",
				"tbl_users"=>"tbl_users.id = tbl_schedule_ride.user_id");
			$data['ride_request'] = $this->Common_model->selectQuery('*,tbl_schedule_ride.id as sched_id,tbl_schedule_ride.sr_trip_id',TB_DRIVER_REQUEST,$cond,array("sr_date"=>"DESC"),$join,$jointype); 
			//----- ashwini code to accept request --------
			$acceptquery=$this->db->query('select tbl_schedule_ride.id,tbl_schedule_ride.sr_trip_id,CONCAT(DATE_FORMAT(tbl_schedule_ride.sr_date, "%d %b %Y")," ",tbl_schedule_ride.sr_pick_up_time ) as datefrmt,tbl_users.full_name,tbl_schedule_ride.driver_status FROM tbl_schedule_ride LEFT JOIN tbl_users ON tbl_users.id = tbl_schedule_ride.user_id WHERE tbl_schedule_ride.driver_id="'.$this->session->userdata('user_id').'" AND tbl_schedule_ride.driver_status IN (2,3)');
			$data['ride_request_accepted'] = $acceptquery->result_array();	
			$closedCond = array("tbl_schedule_ride.driver_id" => $this->session->userdata('user_id'),"tbl_schedule_ride.driver_status"=>"4");
			$closedJointype=array("tbl_users"=>"LEFT");
			$closedJoin = array("tbl_users"=>"tbl_users.id = tbl_schedule_ride.driver_id");
			$data['ride_request_closed'] = $this->Common_model->selectQuery('tbl_schedule_ride.id,tbl_schedule_ride.sr_trip_id,CONCAT(DATE_FORMAT(tbl_schedule_ride.sr_date, "%d %b %Y")," ",tbl_schedule_ride.sr_pick_up_time ) as datefrmt,tbl_users.full_name',TB_SCHEDULE_RIDE,$closedCond,array(),$closedJoin,$closedJointype); 
			$this->load->view("driverUser/driverMytripRequest",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

		// driver accepeted ride request
	public function driver_accepted_request()
	{
		$postData = $this->input->post();	
			// ------ check if already accepted ------------
		$cond= array("id" => $postData['tripID'],'driver_status'=>2);
		$checkexistsq = $this->Common_model->select("*",TB_SCHEDULE_RIDE,$cond);
			/*print_r($postData);
			print_r($checkexistsq);exit;*/
			if(count($checkexistsq) > 0)
			{
				$this->output
				->set_content_type("application/json")
				->set_output(json_encode(array("status"=>false,"message"=>"Ride is booked by another driver.")));
			}else
			{
				//--------- accept the request ----------
				$updateData = array('driver_id' => $postData['driverID'],'driver_status'=>2,'admin_status'=>2,
					'updated_on'=>date("Y-m-d H:i:s"));
				$where = array('id' =>$postData['tripID']); 
				$result= $this->Common_model->update(TB_SCHEDULE_RIDE,$where,$updateData);	 
				//----------- trip details----------------------------------
				$tripDetailsCond= array("id" => $postData['tripID']);
				$tripDetails = $this->Common_model->select("user_id,sr_trip_id,sr_date,sr_pick_up_time,sr_total_distance,source_address,destination_address",TB_SCHEDULE_RIDE,$tripDetailsCond);
				$notificationArr = array(
									"n_title" =>"Trip request accepted",
									"n_full_description"=>"Please check your trip request.<br/> Trip ID :<b>".$tripDetails[0]['sr_trip_id']."</b><br />Schedule ride date :<b>".$tripDetails[0]['sr_date']."</b> <br />Pick up time :<b>".$tripDetails[0]['sr_pick_up_time']."</b><br />Total distance :<b>".$tripDetails[0]['sr_total_distance']."</b> <br />Source location :<b>".$tripDetails[0]['source_address']."</b><br /> Destination location: <b>".$tripDetails[0]['destination_address']."</b>",
									"notification_flag"=>2,
									"read_or_unread" =>1,
									"n_status"=>'ME',
									"user_id" =>$tripDetails[0]['user_id'],
									"created_on" => date("Y-m-d H:i:s"),
									);
				$notifResult = $this->Common_model->insert(TB_NOTIFICATION,$notificationArr);
				if($result && $notifResult)
				{
					$where = array('sched_id' =>$postData['tripID']); 
					$result = $this->Common_model->delete(TB_DRIVER_REQUEST,$where);
					$this->output
					->set_content_type("application/json")
					->set_output(json_encode(array("status"=>true,"message"=>"Ride request has been accepeted successfully.")));
				}
				else
				{
					$this->output
					->set_content_type("application/json")
					->set_output(json_encode(array("status"=>false,"message"=>"Ride request has been failed.")));
				}
			}
		}	


		//------- Reject request -------------
		public function driver_reject_request()
		{
			$postData = $this->input->post();
			$where = array('sched_id' =>$postData['tripID'],'driver_id'=>$postData['driverID']); 
			$result = $this->Common_model->delete(TB_DRIVER_REQUEST,$where);
			if($result)
			{
				$this->output
				->set_content_type("application/json")
				->set_output(json_encode(array("status"=>true,"message"=>"Ride request has been rejected successfully.")));
			}else
			{
				$this->output
				->set_content_type("application/json")
				->set_output(json_encode(array("status"=>false,"message"=>"Oops!!! Somthing went wrong.")));
			}
		}

		public function get_accepted_ride_details()
		{
			$id = $this->input->post("id");	
			$data['rideAccepetedDetails'] = $this->Common_model->select('*',TB_SCHEDULE_RIDE,array('id'=>$id));	
			$getAcceptedRideDetailsQuery = $this->db->query("SELECT sr.sr_date,sr.sr_pick_up_time,sr.sr_total_distance,sr.source_address,sr.destination_address,sr.duration,u.full_name FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.id = $id");
			$data = $getAcceptedRideDetailsQuery->result_array();
		//print_r($data);exit;
			if($data)
			{
				$this->output
				->set_content_type("application/json")
				->set_output(json_encode(array("status"=>true,'data'=>$data)));
			}
			else{
				$this->output
				->set_content_type("application/json")
				->set_output(json_encode(array("status"=>false,'message'=>"Accepted request ride view has been failed to show.")));
			}
		}

	// driver my bills
		public function driver_my_bills()
		{		
			if($this->session->userdata('user_id'))
			{	
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			$this->load->view("driverUser/driverMyBill",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	// driver notifications
	public function driver_notifications()
	{		
		if($this->session->userdata('user_id'))
		{	
			$user_id =$this->session->userdata('user_id');
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['notification'] = $this->Common_model->selectQuery("*",TB_NOTIFICATION,array('user_id'=>$user_id),array('id desc'=>'order by'));
			$totalNotif = $this->Common_model->select("count(*) AS notification",TB_NOTIFICATION,array('user_id'=>$user_id));
			if($totalNotif[0]['notification']>=1)
			{				
				$flagReset = array('notification_flag' => 1);
				$where = array('id' =>$user_id); 
				$this->Common_model->update(TB_NOTIFICATION,$where,$flagReset);	    		
			}			
			$this->load->view("driverUser/driverNotification",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	public function notificationRead() { 
		$notificationId = $this->input->post("id");	
		$user_id =$this->session->userdata('user_id');
		
		$flagReset = array('notification_flag' => 1);
		$where = array('id' =>$notificationId); 
		$result = $this->Common_model->update(TB_NOTIFICATION,$where,$flagReset);
		
		if($result){
			$totalNotif = $this->Common_model->select("count(*) AS notification",TB_NOTIFICATION,array('user_id'=>$user_id,'notification_flag'=>'2'));
			echo json_encode(array('count'=>$totalNotif[0]['notification'],'status'=>'true')); die;
			 //echo 'true';	
		}else{
			echo 'false'; die;
		}
	}

	//delete notification 
	public function driver_delete_notification()
	{
		$deleteNotifID = $this->input->post("id");				
		$where = array('id' =>$deleteNotifID); 
		$result = $this->Common_model->delete(TB_NOTIFICATION,$where);
		if($result)
		{
			$this->output
			->set_content_type("application/json")
			->set_output(json_encode(array("status"=>true,"message"=>"Notification has been deleted successfully.")));
		}	
		else
		{
			$this->output
			->set_content_type("application/json")
			->set_output(json_encode(array("status"=>false,"message"=>"Notification has not been deleted successfully.")));
		}
	}

	// ------ show driver requests as notifications ---------
	public function getUserNotifications()
	{
		if($this->session->userdata('user_id'))
		{
			$cond = array("is_read"=>"0","driver_id"=>$this->session->userdata('user_id'));
			$getreq = $this->Common_model->select("*",TB_DRIVER_REQUEST,$cond);			
			$this->output
			->set_content_type("application/json")
			->set_output(json_encode(array("status"=>"success","data"=>count($getreq))));
		}
	}
}