<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();		
		$this->load->model("Common_model");
		$this->load->model("Login_model");
		$this->load->library('encryption');
		$this->load->library('encrypt');		
		$this->load->helper('string');
  		$this->load->helper("email_template");
		$this->load->helper(array('form', 'url'));
		$this->load->library('image_lib');
		$config['allowed_types'] =   "png|mpg|mpeg|wmv|jpg|jpeg|JPG|JPEG|PNG";  
                $config['max_size']      =   "5000"; 
                $config['max_width']     =   "1000"; 
                $config['max_height']    =   "900"; 
                $config['min_width']     = "70"; // new
                $config['min_height']    = "70"; // new    
                $config['upload_path']   =  "./uploads/Patients/original/" ;   
               	$this->load->library('upload', $config);
		$this->load->library('form_validation');
		$this->load->helper('security');
	}
	
	public function index()
	{
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);	
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->load->view('frontend/login',$data);		
	}
	
	// User registration 
	public function registration()
	{
		$this->load->helper("email");
		$this->load->helper("email_template_helper");
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);
		//form validation
		$this->form_validation->set_rules('first_name', 'First name', 'trim|required|min_length[3]|max_length[20]|alpha|xss_clean');
		$this->form_validation->set_rules('last_name', 'Last name', 'trim|required|min_length[3]|max_length[20]|alpha|xss_clean');
		$this->form_validation->set_rules('phone_no', 'Mobile no', 'required|regex_match[/^[0-9]{10}$/]|integer');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[tbl_users.email]|max_length[50]');
		$this->form_validation->set_rules('password', 'Password One', 'trim|required|min_length[5]|min_length[50]|xss_clean');
		$this->form_validation->set_rules('confirmpwd', 'Password Confirmation Field', 'required|matches[password]');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]|max_length[50]|alpha|xss_clean');
		$this->form_validation->set_rules('user_type', 'User type', 'trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('frontend/login',$data);
		}
		else
		{
			$token = "";
			$postData = $this->input->post();
			$md5e = md5($postData['password']);
			$token=  random_string('alnum',20);
			if(strcmp($postData['confirmpwd'],$postData['password'])==0)
			{			
				$insertArr = array(
						'user_token' => $token,
						'first_name' => $postData['first_name'],
						'last_name'  => $postData['last_name'],	
						'phone'      => $postData['phone_no'],
						'email'      => $postData['email'],
						'password'   => md5($postData['password']),
						'Username'   => $postData['username'],
						'token'      => $token,							
						'created_at' => date('Y-m-d H:i:s'),
						'status'     => "inactive",
						'user_type'  => $postData['user_type'],
						'done'		=> 1
					);
				// Record insert in user table
				$result = $this->Common_model->insert(TB_USERS,$insertArr);
				if($result)
				{
					// Email to registration 
					$hostname = $this->config->item('hostname');
					$config['mailtype'] ='html';
					$config['charset'] ='iso-8859-1';
					$this->email->initialize($config);
					$from  = EMAIL_FROM; 
					$name = $postData['first_name'];
					$email = $postData['email'];
					$userType = $postData['user_type'];
					$this->messageBody  = email_header();
					$base_url = $this->config->item('base_url');
					$reset_url = $base_url."accountverify/".$token ;
					// Email for patient registration 
					if($userType=="Patient")
					{
						$this->messageBody  .= '<tr>
							<td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
							    <p>Dear Customer,</p>
								<p>
									You are registered successfully on Transcura as a patient.<br>
									Please click the link below to verify your email address.</p>
								<p>
								<p>Activation Link : </p>
									<a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href='.$reset_url.'>Click Here</a>
								</p>
								<p>
									If the link is not working properly, then copy and paste the link in your 
									browser. <br>If you did not send this request, please ignore this email.
								</p>
							</td>
						</tr>'; 
					}
					else
					{
					// Email for other registration 
					$this->messageBody  .= '<tr>
								<td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;height:200px;vertical-align:top;">
								    <p>Dear Customer,</p>
									<p>
									You are registered successfully on Transcura as a driver.<br>
									Please wait while administrator activates your account. </p>
							    </td>
							</tr>';
					}
				    $this->messageBody  .= email_footer();
				    $this->email->from($from, $from);
				    $this->email->to($email);
				    $this->email->subject('Welcome to Transcura');
				    $this->email->message($this->messageBody);
				    $this->email->send();
				    $this->session->set_flashdata("auth_reg", "Registration has been successfully");
				    redirect("frontend/login");
				}
				else
				{
					$this->session->set_flashdata("auth_error", "Registration has been Fail");
					redirect("frontend/login");
				}
			}
			else
			{
				$this->session->set_flashdata("confirm_msg", "Password & confirm both are not equal");
			}
			redirect('frontend/login');
		}
	}
	
	// Home page call
	public function fronthome()
	{		
		$this->load->view("frontend/fronthome");		
	}

	// About page call
	public function about()
	{		
		$this->load->view("frontend/about");		
	}
	
	// Doctor page call
	public function doctor()
	{		
		$this->load->view("frontend/doctor");		
	}

	// Patient page call
	public function patient()
	{		
		$this->load->view("frontend/patient");		
	}

	// Driver page call
	public function driver()
	{		
		$this->load->view("frontend/driver");		
	}

	// portfolio page call
	public function portfolio()
	{
		if(!$this->session->userdata('user_id'))
		redirect('frontend/login');	
		$this->load->view("frontend/portfolio");			
	}

	// Services page call
	public function service()
	{
		if(!$this->session->userdata('user_id'))
		redirect('frontend/login');	
		$this->load->view("frontend/services");		
	}

	// Contact page call
	public function contact()
	{		
		$this->load->view("frontend/contact");
	}

	// Pricing table page call
	public function pricingTable()
	{
		if(!$this->session->userdata('user_id'))
		redirect('frontend/login');	
		$this->load->view("frontend/pricingTable");		
	}
	
	// Show user profile page call		
	public function userProfile()
	{
		if(!$this->session->userdata('user_id'))
		redirect('frontend/login');	
		$userID = $this->session->userdata('user_id');		
		$selectArr = array('id' => $userID);
		$data['userData'] = $this->Common_model->select("*",TB_USERS,$selectArr);
		// User profile show as per user login
		if($data['userData'][0]['user_type'] == 'Driver')
		{
			$this->load->view("frontend/driverUserProfile",$data);	
		}	
		else
		{
			$this->load->view("frontend/userprofile",$data);	
		}
	}

	// Edit profile page call
	public function editProfile()
	{
		if(!$this->session->userdata('user_id'))
		redirect('frontend/login');	
		$userID = $this->session->userdata('user_id');
		$selectArr = array('id' => $userID);
		$data['userData'] = $this->Common_model->select("*",TB_USERS,$selectArr);
		// Edit profile show as per user login
		if($data['userData'][0]['user_type'] == 'Driver')
		{
			$this->load->view("frontend/driverEditProfile",$data);	
		}	
		else
		{
			$this->load->view("frontend/editprofile",$data);	
		}
	}

	// Change Edit profile of driver
	public function changeDriverEditProfile()
	{
		if(!$this->session->userdata('user_id'))
		redirect('frontend/login');	

		$postData = $this->input->post();	
		$userID = $this->session->userdata('user_id');
		$selectArr = array('id' => $userID);
		$data['userData'] = $this->Common_model->select("*",TB_USERS,$selectArr);		
		// Form validation
		$this->form_validation->set_rules('f_name', 'First name', 'required');
		$this->form_validation->set_rules('l_name', 'Last name', 'required');
		$this->form_validation->set_rules('phone', 'phone', 'required|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('date_of_birth', 'Date of birth', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('zipcode', 'Zipcode', 'required');
		$this->form_validation->set_rules('company_name', 'Company name', 'required');
		$this->form_validation->set_rules('comp_address', 'Company address', 'required');
		$this->form_validation->set_rules('contact_no', 'Contact no', 'required');
		$this->form_validation->set_rules('own_vehicle', 'Own vehicle', 'required');
		 
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE)
		{        	
		    $this->load->view('frontend/driverEditProfile',$data);
		}
		else
		{	
			// File uploads of driver
			$config['upload_path'] = './uploads/Driver/doc';
	       	$config['allowed_types'] = 'pdf|doc|docx';
	       	$this->load->library('upload', $config);
	       	$this->upload->initialize($config);
	       	// Personal Documents upload
			if($this->upload->do_upload('personal_doc'))
			{        	
			    $uploadData = $this->upload->data();
			    $personalDoc = 'uploads/Driver/doc/'.$uploadData['file_name'];
			}
			else
			{    
			    // Already file uploaded file name assign	    	
			    $personalDoc = $data['userData'][0]['personal_doc'];
			}
	       		// Vehicle Documents upload
			if($this->upload->do_upload('vehicle_doc'))
			{
			    $uploadDataVeh = $this->upload->data();
			    $vehicDoc = 'uploads/Driver/doc/'.$uploadDataVeh['file_name'];
			}
			else
			{
			    $vehicDoc = $data['userData'][0]['vehicle_doc'];
			}
			// Profile picture image file upload
			if(isset($_FILES["profile_picture"]["name"]))   
			{   
		        $file_name = $_FILES["profile_picture"]["name"];
		        $original_file_name = $file_name;
		        $random = rand(1, 10000000000000000);
		        $makeRandom = $random;
		        $file_name_rename = $makeRandom;
		        $explode = explode('.', $file_name);
		        if(count($explode) >= 2) 
		        {
					$new_file = $file_name_rename.'.'.$explode[1];
					$config['upload_path'] = "./uploads/Driver/thumb";
					$config['allowed_types'] ="png|jpeg|jpg";
					$config['file_name'] = $new_file;
					$config['max_size'] = '307210';
					$config['max_width'] = '300000';
					$config['max_height'] = '300000';
					$config['overwrite'] = TRUE;
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload("profile_picture")) 
					{
						$error = $this->upload->display_errors();
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); //exit;	
					}
					else
					{
						$pic = 'uploads/Driver/thumb/'.$new_file;   

					}
		        }
		}
		// If user edit some fields but not upload file then previous file name assign
		if($_FILES["profile_picture"]['name']=='')   
	   	{          	 
	   		$pic =$data['userData'][0]['picture'];		        				
		}
        	$updateArr = array(
					'first_name'       	=> $postData['f_name'],
					'last_name'        	=> $postData['l_name'],
					'phone'      		=> $postData['phone'],
					'email'      		=> $postData['email'],
					'date_of_birth' 	=> date("Y-m-d",strtotime($postData['date_of_birth'])),
					'address' 		    => $postData['address'],
					'zipcode' 		    => $postData['zipcode'],
					'company_name' 		=> $postData['company_name'],
					'company_address' 	=> $postData['comp_address'],
					'vehicle_doc' 		=> $vehicDoc,
					'personal_doc' 		=> $personalDoc,
					'picture' 		    => $pic,
					'company_contactno' => $postData['contact_no'],				
					'paratrasmit' 		=> $postData['own_vehicle'],
					'updated_at' 		=> date("Y-m-d H:s:i"),
				);

			$userID = $this->session->userdata('user_id');
			$where = array('id'=>$userID);
			// Update edit profile
			$update =$this->Common_model->update(TB_USERS,$where,$updateArr);

			if($update)
			{
				$this->session->set_flashdata("success","Edit driver profile update successfully");
				redirect("edit-profile",$data);		
			}
			else
			{
				$this->session->set_flashdata("success","Edit driver profile not update successfully");
				redirect("edit-profile",$data);
			}
		}
	}

	// Edit profile of user
	public function changeEditProfile()
	{
		if(!$this->session->userdata('user_id'))
		redirect('frontend/login');
		$doc ="";
       	$pic ="";
		$postData = $this->input->post();
		$userID = $this->session->userdata('user_id');
		$selectArr = array('id' => $userID);
		$data['userData'] = $this->Common_model->select("*",TB_USERS,$selectArr);			
		// form validation
		$this->form_validation->set_rules('date_of_birth', 'Date of birth', 'required');
		$this->form_validation->set_rules('address', 'Address', 'required');
		$this->form_validation->set_rules('zipcode', 'zipcode', 'required');
		$this->form_validation->set_rules('mobile', 'Mobile', 'required|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('medical_insurance_id', 'Medical insurance id', 'required');
		$this->form_validation->set_rules('emergency_contact_name', 'emergency contact name', 'required');
		$this->form_validation->set_rules('emergency_contact_number', 'emergency contact number', 'required|regex_match[/^[0-9]{10}$/]');
		$this->form_validation->set_rules('signature', 'Signature', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE)
		{        	
		    $this->load->view("frontend/editprofile",$data);
		}
		else
		{        
			// Personal documents upload	 		
        	if(isset($_FILES["personal_doc"]["name"]))   
			{
				$file_name = $_FILES["personal_doc"]["name"];
				$original_file_name = $file_name;
				$random = rand(1, 10000000000000000);
				$makeRandom = $random;
				$file_name_rename = $makeRandom;
				$explode = explode('.', $file_name);
				if(count($explode) >= 2)
				{
					$new_file = $file_name_rename.'.'.$explode[1];
					$config['upload_path'] = "./uploads/Patients/doc";
					$config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
					$config['file_name'] = $new_file;
					$config['max_size'] = '307210';
					$config['max_width'] = '300000';
					$config['max_height'] = '300000';
					$config['overwrite'] = TRUE;
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload("personal_doc"))
					{
						$error = $this->upload->display_errors();
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); 	
					} 
					else
					{

						$doc = 'uploads/Patients/doc/'.$new_file;   
					}
			    }
			}
			// User not uploaded file then previous file assign
			if($_FILES["personal_doc"]["name"]=='')
			{          	 				
			    $doc =$data['userData'][0]['personal_doc'];		        				
			}
			// Profile picture upload
			if(isset($_FILES["picture_profile"]["name"]))   
			{   
				
		        $file_name = $_FILES["picture_profile"]["name"];
		        $original_file_name = $file_name;
		        $random = rand(1, 10000000000000000);
		        $makeRandom = $random;
		        $file_name_rename = $makeRandom;
		        $explode = explode('.', $file_name);
		        if(count($explode) >= 2) 
		        {
				$new_file = $file_name_rename.'.'.$explode[1];
				$config['upload_path'] = "./uploads/Patients/thumb";
				$config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
				$config['file_name'] = $new_file;
				$config['max_size'] = '307210';
				$config['max_width'] = '300000';
				$config['max_height'] = '300000';
				$config['overwrite'] = TRUE;
				$this->load->library('upload',$config);
				$this->upload->initialize($config);
				if(!$this->upload->do_upload("picture_profile")) 
				{
					$error = $this->upload->display_errors();
					echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); //exit;	
				}
				else
				{
					$pic = 'uploads/Patients/thumb/'.$new_file;   

				}
		    }
		}
		// Previous profile picture assign
		if($_FILES["picture_profile"]["name"]=='')
		{   	
		 	$pic =$data['userData'][0]['picture'];			        				
		}    	
		$updateArr = array(
				'date_of_birth' => date("Y-m-d",strtotime($postData['date_of_birth'])),				
				'address' => $postData['address'],
				'zipcode' => $postData['zipcode'],
				'mobile' => $postData['mobile'],
				'insurance_id' => $postData['medical_insurance_id'],
				'personal_doc'=> $doc,
				'picture'=> $pic,
				'emergency_contactname'=> $postData['emergency_contact_name'],
				'emergency_contactno'=> $postData['emergency_contact_number'],
				'signature' =>$postData['signature'],
				'updated_at' => date("Y-m-d H:s:i")
			);
			$userID = $this->session->userdata('user_id');
			$where = array('id'=>$userID);
			// Update of user profile
			$update =$this->Common_model->update(TB_USERS,$where,$updateArr);			 
			if($update)
			{
				$this->session->set_flashdata("success","Edit patient profile update successfully");
				redirect("edit-profile");				
			}
			else
			{
				$this->session->set_flashdata("success","Edit patient profile not update successfully");
				redirect("edit-profile");
			}
		}	
	}
	// User login
	public function login()
	{
		$this->load->model('Login_Model');
		// Login form validation
		$this->form_validation->set_error_delimiters('<div class="error">','</div>');
		$this->form_validation->set_rules('lemail','Email','required|valid_email');
		$this->form_validation->set_rules('lpassword','Password','required');
		$this->form_validation->set_rules('luser_type','User type','required');
			
		if($this->form_validation->run() == FALSE)
		{
			
			//echo "Invalid credencial, please try again";
			$this->session->set_flashdata('log_error_msg', 'Invalid credencial, please try again');
			redirect("frontend/login");
		}
		else
		{		
			$user_login =array(
					'user_email'	=>$this->input->post('lemail'),
					'user_password'	=>md5($this->input->post('lpassword')),
					'user_type'	=>$this->input->post('luser_type'),
					'status'	=>'active'
				);		
			//print_r($user_login);exit;
			// Check user login credential are exit or not		
			$data = $this->Login_Model->login_user($user_login['user_email'],$user_login['user_password'],$user_login['user_type'],$user_login['status']);			
			if($data)
			{
				$sessionData = array( 
						'user_id'    => $data['id'], 
			            'firstName'  => $data['first_name'], 
				        'lastName'   => $data['last_name'],
				        'userType' 	 => $data['user_type']
					);  
				$this->session->set_userdata($sessionData);
				redirect('homepage');				
	   		}
			else
			{
				$this->session->set_flashdata('log_error_msg', 'Error occured,Invalid Data Try again.');
				redirect("frontend/login");
			}	
		}	
	}
	// User logout 
	public function logout()
	{
 		$this->session->unset_userdata(array('user_id','firstName','lastName','userType'));
		redirect("homepage");
	}
	// Forgot password page call
	public function forgotPassword()
	{
		$this->load->view("frontend/forgotPassword");
	}
	// Set forgot password
	public function setForgotPassword()
	{
		$this->load->helper('email_template_helper');
		$email = $this->input->post('email');
		$this->load->helper('email');
	    $userdata = $this->Common_model->select("`id`,`first_name`,`email`,`token`,`password`,`phone`,`Username`",TB_USERS,array("email"=>trim($email),"status"=>"active"));
	
		if(count($userdata)==0)
		{
			$this->session->set_flashdata('check_email_msg', 'This email does not exist or active');
			redirect("forgot-pwd");
		}        
		else
		{
			// Email send to the user for verification
			$password =$userdata[0]['password'];
			$userId = base64_encode($userdata[0]['id']);
			$token = $userdata[0]['token'];
			$reset_token = random_string('unique');
			$reset_url = base_url()."frontend/login/resetPassword/".$userId."/".$token;
			$useremail =trim($userdata[0]["email"]) ;               
			$hostname = $this->config->item('hostname');
			$config['mailtype'] ='html';
			$config['charset'] ='iso-8859-1';
			$this->email->initialize($config);
			$from  = EMAIL_FROM; 
			$this->messageBody  = email_header();
			$this->messageBody  .= '<tr> 
		        <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
		            <p>Hello,</p>
		                <p>
		                A request has been made to forgot your password Transcura account password.<br>
		                Please click the link below to reset your password.</p>
		                <p>
		                <p>Activation Link : </p>
		                <a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href="'.$reset_url.'">Click Here</a>
		                </p>
		                <p>
		                If the link is not working properly, then copy and paste the link in your 
		                browser. <br>If you did not send this request, please ignore this email.</p>
		            </td>
		        </tr>
		        <tr>';                      
		    $this->messageBody  .= email_footer();
		    $this->email->from($from, $from);
		    $this->email->to($email);
		    $this->email->subject('Reset Password Link');
		    $this->email->message($this->messageBody);
		    if($this->email->send()) 
		    {
		    	$this->session->set_flashdata('forgot_success_msg', 'Send reset password link your mail, please check it');
			    redirect("frontend/login");
		    }       
        	else
        	{
        	    $this->session->set_flashdata('forgot__error_msg', 'Error occured,Please try again.');
			    redirect("frontend/login");
        	}
       	}
	}
	// Password reset by getting id and token
    public function resetPassword($id,$token)
    {
		$id = base64_decode($id);
		$userdata = $this->Common_model->select("*",TB_USERS,array("id"=>trim($id),"token"=>trim($token),"status"=>"active"));
		$data['mydata']=array('token'=>$token,'id'=>$id);	
    	if($userdata)
		{
			// Clear token for one time user change password 
			$datareset = array('token' =>'');
			$cond1 = array('id' =>$id); 
    		$result = $this->Common_model->update(TB_USERS,$cond1,$datareset);		    		
    		$this->load->view("frontend/resetPassword",$data);		 
		 }
		else
		{
		  	$this->session->set_flashdata('forgot__error_msg', 'You reset password link has been expired, already use!');
			redirect("forgot-pwd",$data);
		}
	}
	// Update token
	function update_token()
	{
		$ftoken=$this->input->post('token');
		$fid = $this->input->post('id');
		$datareset1 = array('token' =>$ftoken);
		$cond1 = array('id' =>$fid); 
    	$result = $this->Common_model->update(TB_USERS,$cond1,$datareset1);
	}
	// Change user password 
    public function changePassword()
    {
		$ftoken=$this->input->post('token');
		$fid = $this->input->post('id');
	    $postData = $this->input->post();
	    $userdata = $this->Common_model->select("`id`,`first_name`,`email`,`token`,`password`,`phone`,`Username`",TB_USERS,array("id"=>trim($postData['id']),"status"=>"active"));
		// Form validation
	    $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
		$this->form_validation->set_rules('pwd', 'password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('confirmpwd', 'password confirmation field', 'trim|required|matches[pwd]');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$data['mydata']=array('id'=>$postData['id'],'token'=>$postData['token']);

		if ($this->form_validation->run() == FALSE)
		{	
			// If user fail to change password then previous token update for verification		
			$this->update_token();
			$this->session->set_flashdata('fail_change_msg', 'Please enter valid credential,Please try again.');				
			redirect('frontend/login/resetPassword/'.base64_encode($fid).'/'.$ftoken,$data);
		}
		else
		{      	 
			// Check user get data is empty or not 	
			if (empty($userdata)) {
 				$this->update_token();
				$this->session->set_flashdata('fail_change_msg', 'Error occur,Please try again.');								
				redirect('frontend/login/resetPassword/'.base64_encode($fid).'/'.$ftoken);
			}
			else
			{
				// Check user email correct or not
				if(trim($postData['email']) == trim($userdata[0]['email']) && trim($userdata[0]['id']) == trim($postData['id']))
				{
					// Update token
					$generateToken=  random_string('alnum',20);
					$datareset = array('password' =>  md5($postData['pwd']),'token' => $generateToken);
					$cond1 = array('email' =>  $postData['email']); 
					// Chnage password successfully
			    	$result = $this->Common_model->update(TB_USERS,$cond1,$datareset);
						
					if($result)
					{
						// success then redirect login page
						$this->session->set_flashdata('change_success_msg', 'Password change success,please login');
						redirect('frontend/login');
					}
					else
					{	
						$this->update_token();
						$this->session->set_flashdata('fail_change_msg', 'Error occured,Please try again.');
						$this->load->view('frontend/resetPassword',$data);
					}
				}
				else
				{			
					$this->update_token();
					$this->session->set_flashdata('fail_change_msg', 'Please enter valid credential,Please try again.');								
					redirect('frontend/login/resetPassword/'.base64_encode($fid).'/'.$ftoken,$data);
				}
			}					
		}
    }
}
