<?php if(!defined("BASEPATH")) exit("No direct script access allowed");
class DriverController extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->load->library('upload');	
		$this->load->helper('file');
		$this->load->helper(array('form', 'url'));
		$this->load->library("pagination");
	}

	// default driver dashboard
	public function index()
	{		
		if($this->session->userdata('user_id'))
		{	
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			$data['myRideCount'] = $this->Common_model->group_by("sr_status,count(*) total",TB_SCHEDULE_RIDE,array('sr_status'),array('user_id'=>$this->session->userdata('user_id'))); 
			$data['scheduledRide'] = $this->Common_model->select("sr_status,count(*) total",TB_SCHEDULE_RIDE,array('user_id'=>$this->session->userdata('user_id'),'sr_status' => '1')); 
			$data['completedRide'] = $this->Common_model->select("sr_status,count(*) total",TB_SCHEDULE_RIDE,array('user_id'=>$this->session->userdata('user_id'),'sr_status' => '2')); 
			$data['cancelledRide'] = $this->Common_model->select("sr_status,count(*) total",TB_SCHEDULE_RIDE,array('user_id'=>$this->session->userdata('user_id'),'sr_status' => '3')); 
			$data['userDetails']=$this->Common_model->select('picture,full_name,email',TB_USERS,array('id'=>$this->session->userdata('user_id')));
			$cond3 = array("tbl_schedule_ride.user_id" => $this->session->userdata('user_id'),"tbl_schedule_ride.sr_date" => date("Y-m-d"));
			$jointype=array("tbl_country"=>"LEFT","tbl_states"=>"LEFT","tbl_drivers"=>"LEFT");
			$join = array("tbl_country"=>"tbl_country.id = tbl_schedule_ride.sr_country","tbl_states"=>"tbl_states.id = tbl_schedule_ride.sr_state","tbl_drivers"=>"tbl_drivers.id = tbl_schedule_ride.driver_id");
			$data['getMapDetails']=$this->Common_model->selectQuery("tbl_schedule_ride.id,tbl_country.country_name,tbl_states.state_name,sr_street,sr_zipcode,tbl_drivers.latitude as driverLat,tbl_drivers.longitude as driverLong,tbl_schedule_ride.driver_id",TB_SCHEDULE_RIDE,$cond3,array(),$join,$jointype);	
			
			$this->load->view("frontDrivers/driverDashBoard",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	// driver profile view
	public function driver_profile()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['getDriverDetails'] = $this->Common_model->select("*",TB_USERS,array('id'=>$this->session->userdata('user_id'))); // get driver records
			$query = $this->db->query("SELECT ct.city_name,s.state_name,c.country_name FROM tbl_city as ct 
				left join tbl_states as s on ct.state_id = s.id
				left join tbl_country as c on c.id = s.country_id
				where ct.id=".$data['getDriverDetails'][0]['city']."");
			$data['countryDetails']=$query->result_array();		
			$this->load->view("frontDrivers/driverProfile",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}	
	}
	
	public function address()
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
		$this->load->view("user/address",$data);
	}

	// get address by api 
	public function get_address()
	{	
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
		$postData = $this->input->post();
		$mydata = array();
		$append_session = array();
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://maps.googleapis.com/maps/api/place/textsearch/json?query='".str_replace(" ", "+", $postData["toname"])."'&key=".GOOGLE_API_KEY,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",		    
				),
			));
		
		$data = curl_exec($curl);
		$err = curl_error($curl);
		$address = json_decode($data, true);
		$placeid = "";
		if(isset($address['results'][0]['place_id']))
			$placeid = $address['results'][0]['place_id'];
		else
			$placeid = '';
		//print_r($address);exit;
		$append_session1 = array();
		$curl1 = curl_init();
		curl_setopt_array($curl1, array(
			CURLOPT_URL => "https://maps.googleapis.com/maps/api/place/details/json?placeid=".$placeid."&key=".GOOGLE_API_KEY,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",		    
				),
			));
		//echo "<pre>";print_r(CURLOPT_URL);
		$data1 = curl_exec($curl1);
		$err1 = curl_error($curl1);
		$address1 = json_decode($data1, true);
		
		$append_session_current = array();
		$curl_current = curl_init();
		curl_setopt_array($curl_current, array(
			CURLOPT_URL => "https://maps.googleapis.com/maps/api/place/textsearch/json?query='".str_replace(" ", "+", $postData["name"])."'&key=".GOOGLE_API_KEY,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"cache-control: no-cache",		    
				),
			));
		//echo "<pre>";print_r(CURLOPT_URL);
		$data_current = curl_exec($curl_current);
		$err_current = curl_error($curl_current);
		$address_current = json_decode($data_current, true);
		$rows ='';
		if($address['results'] != '')
		{
			$curlDt = curl_init();
			curl_setopt_array($curlDt, array(
				CURLOPT_URL => "https://maps.googleapis.com/maps/api/distancematrix/json?origins='".str_replace(" ", "+", $postData["name"])."'&destinations='".str_replace(" ", "+", $postData["toname"])."'&key=".GOOGLE_API_KEY,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					"cache-control: no-cache",							   
					),
				));
			$dataDt = curl_exec($curlDt);
			$errDt = curl_error($curlDt);

			$totalDistance = json_decode($dataDt, true);
			if($totalDistance !='' && isset($totalDistance['rows'][0]['elements'][0]['distance'])) {
				$distance = $totalDistance['rows'][0]['elements'][0]['distance']['text'];
				$duration = $totalDistance['rows'][0]['elements'][0]['duration']['text'];
			}
			else
			{
				$distance = "";
				$duration = "";
			}
			if(isset($address['results'][0]) && isset($address_current['results'][0]))
			{
				$rows .= '<tr id="'.$address['results'][0]['formatted_address'].'">
				<td class="text-left">'.$address['results'][0]['formatted_address'].'</td>	 
				<td class="text-left">'.$postData["name"].'</td>
				<td class="text-left">'.$postData["toname"].'</td>
				<td class="text-left">'.$address['results'][0]['geometry']['location']['lat'].' , '.$address['results'][0]['geometry']['location']['lng'].'</td>	                           
				<td class="text-left">'.$address_current['results'][0]['geometry']['location']['lat'].' , '.$address_current['results'][0]['geometry']['location']['lng'].'</td>
				<td class="text-left">'.$distance.'</td>
				<td class="text-left">'.$duration.'</td>
			</tr>';	 
		}			         	             	   
	}
	else
	{
		$rows = '<tr><td colspan="5" align="center">No Record Found.</td></tr>';	
	}	
	$form_data = array();
	$form_data =json_decode($postData['form_data'], true);
	$workingHours="";
	if(isset($address1['result']['opening_hours']))
	{
		$workingHours.=($address1['result']['opening_hours']['weekday_text'][0]?$address1['result']['opening_hours']['weekday_text'][0]:'');
		$workingHours.= '<br />';
		$workingHours.=($address1['result']['opening_hours']['weekday_text'][1]?$address1['result']['opening_hours']['weekday_text'][1]:'');
		$workingHours.= '<br />';
		$workingHours.=($address1['result']['opening_hours']['weekday_text'][2]?$address1['result']['opening_hours']['weekday_text'][2]:'');
		$workingHours.= '<br />';
		$workingHours.=($address1['result']['opening_hours']['weekday_text'][3]?$address1['result']['opening_hours']['weekday_text'][3]:'');
		$workingHours.= '<br />';
		$workingHours.=($address1['result']['opening_hours']['weekday_text'][4]?$address1['result']['opening_hours']['weekday_text'][4]:'');
		$workingHours.='<br />';
		$workingHours.=($address1['result']['opening_hours']['weekday_text'][5]?$address1['result']['opening_hours']['weekday_text'][5]:'');
		$workingHours.='<br />';
		$workingHours.=($address1['result']['opening_hours']['weekday_text'][6]?$address1['result']['opening_hours']['weekday_text'][6]:'');		
		$workingHours.='<br />';
	}

	$merge_form_data = array();
	if(!empty($form_data)){			
		$merge_form_data = array(
			"name" => $postData['name'],
			"toname" => $postData['toname'],	
			"current_lat" => $postData['current_lat'],
			"current_lng" => $postData['current_lng'],
			"sr_date" => $form_data['sr_date'],
			"sr_pick_up_time" => $form_data['sr_pick_up_time'],
			"sr_appointment_time" =>$form_data['sr_appointment_time'],
			"sr_street" =>$form_data['sr_street'],
			"sr_country" =>$form_data['sr_country'],
			"sr_state" =>$form_data['sr_state'],
			"sr_zipcode" =>$form_data['sr_zipcode'],
			"sr_trip_id" => $form_data['sr_trip_id'],
			"sr_health_service" => $form_data['sr_health_service'],
			"sr_special_request" => $form_data['sr_special_request'],
			"sr_special_instruction" => $form_data['sr_special_instruction'],
			"sr_desire_transportaion" => $form_data['sr_desire_transportaion'],
			"user_id" => $form_data['user_id'],
			"sr_return_time"=>$form_data['sr_return_time'],
			"sr_urgency_type" => $form_data['sr_urgency_type'],
			"sr_trip_type" => $form_data['sr_trip_type'],
			"urjency_explainatin" => $form_data['urjency_explainatin'],
			"sr_status" => $form_data['sr_status'],	
			"formatted_address" => $address['results'][0]['formatted_address'],
			"from_address" => $postData["name"],
			"to_address" => $postData["toname"],
			"destin_lat_lng" => $address['results'][0]['geometry']['location']['lat'].','.$address['results'][0]['geometry']['location']['lng'],
			"current_lat_lng" => $address_current['results'][0]['geometry']['location']['lat'].','.$address_current['results'][0]['geometry']['location']['lng'],
			"distance" => $distance,
			"duration" => $duration,			
			);
	}		
	curl_close($curl);
	curl_close($curlDt);
	curl_close($curl_current);
	curl_close($curl1);
	$phoneNumber=$nameJSON=$addressJSON="";
	if(isset($address1['result']['formatted_phone_number']))
		$phoneNumber = $address1['result']['formatted_phone_number'];
	else	
		$phoneNumber="";
	if(isset($address1['result']['name']))
		$nameJSON =$address1['result']['name'];
	else
		$nameJSON="";
	if(isset($address1['result']['vicinity']))
		$addressJSON = $address1['result']['vicinity'];
	else
		$addressJSON='';

	if ($err && $err_current && $errDt && $err1) {
		$this->output
		->set_content_type("application/json")
		->set_output(json_encode(array("status"=>false,"message"=>$err)));
	} else {
		$this->output
		->set_content_type("application/json")
		->set_output(json_encode(array("status"=>true,"rows"=>$rows,"Addsname"=>$nameJSON,"Addphone"=>$phoneNumber,"AddreFull"=>$addressJSON,"workingHours"=>$workingHours,'merge_form_data'=>json_encode($merge_form_data))));
	}
}

public function display_schedule_ride_details()
{
	if($this->session->userdata('user_id'))
	{	
		$postData = $this->input->post();
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
		$data['scheduleRide'] = $this->Common_model->select('user_id,sr_trip_id,source_address,destination_address,sr_total_distance,duration,driver_status',TB_SCHEDULE_RIDE,array('user_id'=>$this->session->userdata('user_id')));
		$data['states'] = $this->Common_model->select('state_name',TB_STATE,array('id'=>$this->session->userdata('scheduleRide')['sr_state']));	
		$data['desire_tran'] = $this->Common_model->select('dt_name',TB_DESIRE_TRANSPORTATION,array('id'=>$this->session->userdata('scheduleRide')['sr_desire_transportaion']));
		$data['hc_service'] = $this->Common_model->select('hs_name',TB_HEALTHCARE_SERVICES,array('id'=>$this->session->userdata('scheduleRide')['sr_health_service']));
		$data['sp_request'] = $this->Common_model->select('sr_name',TB_SPECIAL_REQUEST,array('id'=>$this->session->userdata('scheduleRide')['sr_special_request']));
		$this->load->view('frontDrivers/show_ride_details',$data);
	} else {
		redirect("frontend-login"); exit;
	}
}

public function show_ride_address()
{
	$form_data = array();
	$form_data =json_decode($_POST['test_data'], true);		
	$data['form_data']=$form_data;
	$data['scheduleRide'] = $this->Common_model->select('user_id,sr_trip_id,source_address,destination_address,sr_total_distance,duration,driver_status',TB_SCHEDULE_RIDE,array('user_id'=>$this->session->userdata('user_id')));
	$data['states'] = $this->Common_model->select('state_name',TB_STATE,array('id'=>$form_data['sr_state']));	
	$data['desire_tran'] = $this->Common_model->select('dt_name',TB_DESIRE_TRANSPORTATION,array('id'=>$form_data['sr_desire_transportaion']));
	$data['hc_service'] = $this->Common_model->select('hs_name',TB_HEALTHCARE_SERVICES,array('id'=>$form_data['sr_health_service']));
	$data['sp_request'] = $this->Common_model->select('sr_name',TB_SPECIAL_REQUEST,array('id'=>$form_data['sr_special_request']));
	if($form_data['user_id'])
	{				
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));				
		$this->load->view("frontDrivers/show_ride_details",$data);
	} else {
		redirect("frontend-login"); exit;
	}		
}

public function request_to_schedule_ride()
{		
	$form_data = array();
	$form_data =json_decode($_POST['form_data'], true);
	$insertArr = array(
		'sr_trip_id'=> $form_data['sr_trip_id'],
		'sr_date'=> $form_data['sr_date'],
		'sr_pick_up_time'=> $form_data['sr_pick_up_time'],
		'sr_appointment_time'=> $form_data['sr_appointment_time'],
		'sr_street'=> $form_data['sr_street'],
		'sr_country'=> $form_data['sr_country'],
		'sr_state'=> $form_data['sr_state'],						
		'sr_zipcode'=> $form_data['sr_zipcode'],
		'sr_health_service'=> $form_data['sr_health_service'],
		'sr_special_request' => $form_data['sr_special_request'],		
		'sr_return_time' => $form_data['sr_return_time'],
		'sr_special_instruction' => $form_data['sr_special_instruction'],
		'sr_desire_transportaion' => $form_data['sr_desire_transportaion'],
		'sr_urgency_type'=>$form_data['sr_urgency_type'],
		'sr_trip_type'=>$form_data['sr_trip_type'],
		'user_id'=>$form_data['user_id'],
		'sr_destination'=> $form_data['to_address'],	
		'source_address'=> $form_data['from_address'],
		'destination_address'=> $form_data['formatted_address'],
		'sr_lat_lng_source'=> $form_data['current_lat_lng'],
		'sr_lat_lng_destination'=> $form_data['destin_lat_lng'],
		'sr_total_distance'=> $form_data['distance'],
		'duration'=> $form_data['duration'],
		'created_on'=>date("Y-m-d H:i:s"),						
		'driver_status'=> 1,
		'admin_status'=>1,
		'urjency_explainatin'=>$form_data['urjency_explainatin']												
		);
		// Record insert in user table
	$result = $this->Common_model->insert(TB_SCHEDULE_RIDE,$insertArr);	
	if($result)
	{
		$this->output
		->set_content_type("application/json")
		->set_output(json_encode(array("status"=>true,"message"=>"Your schedule ride request has been successfully .")));						
		 		// $this->session->unset_userdata('scheduleRide');				
	}
	else
	{
		$this->output
		->set_content_type("application/json")
		->set_output(json_encode(array("status"=>false,"message"=>"Your schedule ride request has been failed .")));
	}					
}

public function show_schedule_ride_details()
{
	if($this->session->userdata('user_id'))
	{	
		$where = array('user_id'=>$this->session->userdata('user_id'));
		$config = array(); 
		$config["base_url"] = base_url() . "pending-ride";
		$config["total_rows"] = $this->Common_model->record_count('user_id,sr_trip_id,source_address,destination_address,sr_total_distance,duration,driver_status',TB_SCHEDULE_RIDE,$where);
		$config["per_page"] = 10;
	        //$config["uri_segment"] = 2;
		$config['full_tag_open']        = "<ul class='pagination'>";
		$config['full_tag_close']       = "</ul>";
		$config['num_tag_open']         = '<li>';
		$config['num_tag_close']        = '</li>';
		$config['cur_tag_open']         = "<li class='disabled'><li class='active'><a href='#'>";
		$config['cur_tag_close']        = "<span class='sr-only'></span></a></li>";
		$config['next_tag_open']        = "<li>";
		$config['next_tagl_close']      = "</li>";
		$config['prev_tag_open']        = "<li>";
		$config['prev_tagl_close']      = "</li>";
		$config['first_tag_open']       = "<li>";
		$config['first_tagl_close']     = "</li>";
		$config['last_tag_open']        = "<li>";
			 //For PREVIOUS PAGE Setup
		$config['prev_link'] = 'prev';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
						 //For NEXT PAGE Setup
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';

			 //$config['page_query_string']    = true;
			 //$config['query_string_segment'] = 'page';
			// $config['reuse_query_string']   = true;
			 //$config['use_page_numbers']     = true;
		$config['last_tagl_close']      = "</li>"; 
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data["results"] = $this->Common_model->fetch_schedule_ride('id,user_id,sr_trip_id,source_address,destination_address,sr_total_distance,duration,driver_status',TB_SCHEDULE_RIDE,$where,$config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();	        
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
		$this->load->view('frontDrivers/driverScheduleRideDetails',$data);
	} else {
		redirect("frontend-login"); exit;
	}
}

public function nemt_reset_pwd()
{
	if($this->session->userdata('user_id'))
	{	
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			$postData = $this->input->post();
			$this->form_validation->set_rules('change_pwd', 'Password', 'required');
			$this->form_validation->set_rules('confirm_pwd', 'Confirm Password', 'required|matches[change_pwd]');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
			if ($this->form_validation->run() == FALSE)
			{ 	
				$this->load->view("frontDrivers/change_pwd",$data);		
			}
			else
			{	
				$updateData = array('password' =>md5($postData['change_pwd']));
				$where = array('id' =>$this->session->userdata('user_id')); 
				$result = $this->Common_model->update(TB_USERS,$where,$updateData); 
				if($result)
				{
					$this->session->set_flashdata('success', 'Your passowrd has been changed successfully.');
					$this->load->view("frontDrivers/change_pwd",$data);		
				}
				else
				{
					$this->session->set_flashdata('fail', 'Your passowrd has not been changed successfully.');
					$this->load->view("frontDrivers/change_pwd",$data);		    	
				}    			
			}					
		} 
		else 
		{
			redirect("frontend-login"); exit;
		}
	}

	
	public function view_map()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$cond3 = array("tbl_schedule_ride.user_id" => $this->session->userdata('user_id'),"tbl_schedule_ride.sr_date" => date("Y-m-d"));
			$jointype=array("tbl_country"=>"LEFT","tbl_states"=>"LEFT","tbl_drivers"=>"LEFT");
			$join = array("tbl_country"=>"tbl_country.id = tbl_schedule_ride.sr_country","tbl_states"=>"tbl_states.id = tbl_schedule_ride.sr_state","tbl_drivers"=>"tbl_drivers.id = tbl_schedule_ride.driver_id");
			$data['getPickupDetails']=$this->Common_model->selectQuery("tbl_schedule_ride.id,tbl_country.country_name,tbl_states.state_name,sr_street,sr_zipcode,tbl_drivers.latitude as driverLat,tbl_drivers.longitude as driverLong",TB_SCHEDULE_RIDE,$cond3,array(),$join,$jointype);	
			if(count($data['getPickupDetails'])>0){
				$locationAdd='';
				if($data['getPickupDetails'][0]['sr_street']){ $locationAdd .=$data['getPickupDetails'][0]['sr_street'].','; }
				if($data['getPickupDetails'][0]['sr_zipcode']){ $locationAdd .=$data['getPickupDetails'][0]['sr_zipcode'].','; }
				if($data['getPickupDetails'][0]['state_name']){ $locationAdd .=$data['getPickupDetails'][0]['state_name'].','; }
				if($data['getPickupDetails'][0]['country_name']){ $locationAdd .=$data['getPickupDetails'][0]['country_name'].','; }
				$address = str_replace(" ", "+", $locationAdd);
				$json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
				$json = json_decode($json);
				$lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
				$long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
				$data['getPickupDetails'][0]['default_lat'] = $lat;
				$data['getPickupDetails'][0]['default_lon'] = $long;				
			}			
			$this->load->view("frontDrivers/viewMap",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}	
	}

	// driver edit profile
	public function driver_edit_profile()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY,array('id'=>231));
			$data['getDriverDetails'] = $this->Common_model->select("*",TB_USERS,array('id'=>$this->session->userdata('user_id')));
			$data['stateData'] = $this->Common_model->select("*",TB_STATE,array('country_id'=>$data['getDriverDetails'][0]['country']));
			$data['cityData'] = $this->Common_model->select("*",TB_CITY,array('state_id'=>$data['getDriverDetails'][0]['state']));
			$this->load->view("frontDrivers/driverEditProfile",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}
	
	// changes in driver edit profile
	public function update_driver_edit_profile()
	{		
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY,array('id'=>231));
			$data['getDriverDetails'] = $this->Common_model->select("*",TB_USERS,array('id'=>$this->session->userdata('user_id')));
			$data['stateData'] = $this->Common_model->select("*",TB_STATE,array('country_id'=>$data['getDriverDetails'][0]['country']));
			$data['cityData'] = $this->Common_model->select("*",TB_CITY,array('state_id'=>$data['getDriverDetails'][0]['state']));
			$this->form_validation->set_rules('full_name', 'full name', 'trim|required|min_length[3]|max_length[50]|xss_clean');
			$this->form_validation->set_rules('date_of_birth','date of birth','trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'email', 'trim|required|max_length[50]|min_length[6]|valid_email|xss_clean');
			$this->form_validation->set_rules('phone_no', 'phone', 'trim|required|max_length[20]|min_length[10]|integer|xss_clean');
			$this->form_validation->set_rules('country', 'country', 'trim|required|xss_clean');
			$this->form_validation->set_rules('state', 'state', 'trim|required|xss_clean');		
			$this->form_validation->set_rules('city', 'city', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('zipcode', 'zipcode', 'trim|required|integer|max_length[15]|min_length[5]|xss_clean');
			$this->form_validation->set_rules('county', 'county', 'trim|required|max_length[50]|min_length[5]|integer|xss_clean');
			$this->form_validation->set_rules('street', 'street', 'trim|required|xss_clean');
			$this->form_validation->set_rules('insurance_id', 'insurance id', 'trim|required|xss_clean');		
			$this->form_validation->set_rules('emergency_contact_name', 'emergency contact name', 'trim|required|min_length[3]|max_length[50]|xss_clean');
			$this->form_validation->set_rules('emergency_contact_number', 'emergency contact number', 'trim|required|max_length[20]|min_length[8]|xss_clean');
			$this->form_validation->set_rules('signature', 'signature', 'trim|required|xss_clean');		
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
			if( $this->form_validation->run() == FALSE )
			{  			        	
				$this->load->view('frontDrivers/driverEditProfile',$data);				
			}
			else
			{ 
				$perDoc=$signDoc="";
				//get input data from user
				$postData = $this->input->post();		
				if(!empty($_FILES["sign_file_upload"]["name"]))   
				{   
					$file_name = $_FILES["sign_file_upload"]["name"];
					$original_file_name = $file_name;
					$random = rand(1, 10000000000000000);
					$date = date("Y-m-d-h:i:sa");
					$makeRandom = $random;
					$file_name_rename = $makeRandom;
					$explode = explode('.', $file_name);
					if(count($explode) >= 2) 
					{
						$new_file = $file_name_rename.$date.'.'.$explode[1];		            
						$config['upload_path'] = "./uploads/Driver/doc";
						$config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
						$config['file_name'] = $new_file;
						$config['max_size'] = '307210';
						$config['max_width'] = '300000';
						$config['max_height'] = '300000';
						$config['overwrite'] = TRUE;
						$this->load->library('upload',$config);
						$this->upload->initialize($config);
						if(!$this->upload->do_upload("sign_file_upload")) 
						{
							$error = $this->upload->display_errors();
							print_r($error);exit;
							//echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
						}
						else
						{
							$signDoc = 'uploads/Driver/doc/'.$new_file;
						}
					}
				}			
				else if(isset($postData["sign_file_upload_old"]))   
				{          	 
					$signDoc = $postData["sign_file_upload_old"];		        				
				}
				else
				{
					$signDoc ="";	
				}

				if(!empty($_FILES["personal_doc"]["name"]))   
				{   		    	
					$file_name = $_FILES["personal_doc"]["name"];
					$original_file_name = $file_name;
					$random = rand(1, 10000000000000000);
					$date = date("Y-m-d-h:i:sa");
					$makeRandom = $random;
					$file_name_rename = $makeRandom;
					$explode = explode('.', $file_name);
					if(count($explode) >= 2) 
					{
						$new_file = $file_name_rename.$date.'.'.$explode[1];		            
						$config['upload_path'] = "./uploads/Driver/doc";
						$config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
						$config['file_name'] = $new_file;
						$config['max_size'] = '307210';
						$config['max_width'] = '300000';
						$config['max_height'] = '300000';
						$config['overwrite'] = TRUE;
						$this->load->library('upload',$config);
						$this->upload->initialize($config);
						if(!$this->upload->do_upload("personal_doc")) 
						{
							$error = $this->upload->display_errors();
							print_r($error);exit;
							//echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
						}
						else
						{
							$perDoc = 'uploads/Driver/doc/'.$new_file;
						}
					}
				}
				else if(isset($postData["personal_doc_old"]))   
				{          	 

					$perDoc =$postData["personal_doc_old"];		        				
				}
				else
				{

					$perDoc ="";	
				}	
				if(!empty($_FILES["profile_image"]["name"]))   
				{   		    	
					$file_name = $_FILES["profile_image"]["name"];
					$original_file_name = $file_name;
					$random = rand(1, 10000000000000000);
					$date = date("Y-m-d-h:i:sa");
					$makeRandom = $random;
					$file_name_rename = $makeRandom;
					$explode = explode('.', $file_name);
					if(count($explode) >= 2) 
					{
						$new_file = $file_name_rename.$date.'.'.$explode[1];		            
						$config['upload_path'] = "./uploads/Driver/thumb";
						$config['allowed_types'] ="png|jpeg|jpg";
						$config['file_name'] = $new_file;
						$config['max_size'] = '307210';
						$config['max_width'] = '300000';
						$config['max_height'] = '300000';
						$config['overwrite'] = TRUE;
						$this->load->library('upload',$config);
						$this->upload->initialize($config);
						if(!$this->upload->do_upload("profile_image")) 
						{
							$error = $this->upload->display_errors();
							print_r($error);exit;
							//echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
						}
						else
						{
							$profImage = 'uploads/Driver/thumb/'.$new_file;
						}
					}
				}
				else if(isset($postData["profile_image_old"]))   
				{          	 

					$profImage =$postData["profile_image_old"];		        				
				}
				else
				{

					$profImage ="";	
				} 
				$updateArr  = array(
					'full_name'=>$postData['full_name'],
					'date_of_birth' => date("Y-m-d",strtotime($postData['date_of_birth'])),
					'email' => $postData['email'],
					'phone' => $postData['phone_no'],	
					'street' => trim($postData['street']),
					'country' => $postData['country'],
					'state' => $postData['state'],
					'city'=>$postData['city'],
					'zipcode' => trim($postData['zipcode']),
					'county' => $postData['county'],								
					'insurance_id' => $postData['insurance_id'],							
					'emergency_contactname' => trim($postData['emergency_contact_name']),
					'emergency_contactno' => $postData['emergency_contact_number'],								
					'personal_doc'=>$perDoc,
					'signature_file_upload'=>$signDoc,	
					'picture'=>$profImage,							
					'signature'=>$postData['signature'],								
					'updated_at'=>date("Y-m-d H:i:a")
					);
				$where = array('id' =>$this->session->userdata('user_id'));
				// Record insert in schedule ride table
				$result = $this->Common_model->update(TB_USERS,$where,$updateArr);
				if($result)
				{
					$this->session->set_flashdata("success","Driver edit profile has been updated successfully");
					redirect("my-profile");
				}
				else
				{
					$this->session->set_flashdata("fail","Driver edit profile has not been updated successfully");				
					redirect(current_url());
				}
			}
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	// trip details
	public function ride_trip_details_view()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$this->load->view("frontDrivers/rideTripDetailsView",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	//complaints from customer on driver
	public function customer_complaints()
	{
		if($this->session->userdata('user_id'))
		{	
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['complainData'] = $this->Common_model->select("*",TB_COMPLAINTS_TYPE);
			if($this->input->post())
			{
				$postData = $this->input->post();
				$insertArr = array(
					'src_username'=> $postData['userName'],
					'src_driver_name'=> $postData['driverName'],
					'src_complaint_type'=> $postData['complaintType'],
					'src_date_of_service'=> $postData['dateOfService'],
					'src_name_of_transportation_provider'=> $postData['nameOfTransportProv'],
					'src_description'=> $postData['description']
					);

				// Record insert in user table
				$result = $this->Common_model->insert(TB_SCHEDULE_RIDE_COMPLAINTS,$insertArr);	
				if($result)
				{
					$this->output
					->set_content_type("application/json")
					->set_output(json_encode(array("status"=>true)));
				}
				else
				{
					$this->output
					->set_content_type("application/json")
					->set_output(json_encode(array("status"=>false,"message"=>"your complaint has been fail to submitted")));
				}				
			}
			else
			{
				$this->load->view("frontDrivers/customerComplaints",$data);
			}
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	//check time validation
	function return_time_valid($pickup_time,$appointment_time)
	{		
		list($appointment_time, $pickup_time, $return_time) = explode('|', $appointment_time);
		//echo 'Appointment time '.$appointment_time.' Return time'.$return_time.' pick up time '.$pickup_time;exit;
		if($appointment_time >= $return_time && $pickup_time <= $return_time)
		{			
			$this->form_validation->set_message('return_time_valid', 'Return time should be greater than appointment time and Pick up time.');
			return false;  
		}
		else
		{
			return true;
		}
	}

	function check_urjency_type($urjencty,$urjency_type)
	{
		list($urjency_explanation, $urjency_type1) = explode('|', $urjency_type);
		if($urjency_type1 == 2)
			return true;
		if($urjency_explanation =="")
		{			
			$this->form_validation->set_message('check_urjency_type', 'Urgency explanation field is required.');
			return false;  
		}
		else
			return true;  			
	}

	public function driver_schedule_an_ride()
	{	
		if($this->session->userdata('user_id'))
		{
			$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY,array("id"=>231));
			$data['states'] = $this->Common_model->select('*',TB_STATE);	
			$data['desire_tran'] = $this->Common_model->select('*',TB_DESIRE_TRANSPORTATION);
			$data['hc_service'] = $this->Common_model->select('*',TB_HEALTHCARE_SERVICES);
			$data['sp_request'] = $this->Common_model->select('*',TB_SPECIAL_REQUEST);		
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
			$data['home_section1'] = $this->Common_model->selectWhereIn('page_id,sub_title,page_title,page_detail',TB_PAGES,'page_id',array(2,14,7));

			$appointment_time = $this->input->post('appointment_time');
			$pick_up_time = $this->input->post('pick_up_time');
			$return_time = $this->input->post('return_time');
			$urgency_exp = $this->input->post('urgency_exp');	
			$urgency_type = $this->input->post('urgency_type');
			$urgency_param = $urgency_exp.'|'.$urgency_type; 		
			$parameters = $appointment_time. '|' .$pick_up_time. '|' .$return_time;				
			// form validation
			$this->form_validation->set_rules('date','Date','trim|required|xss_clean');		
			$this->form_validation->set_rules('appointment_time', 'Appointment time', 'trim|required|xss_clean');
			$this->form_validation->set_rules('pick_up_time','Pick up time','trim|required|xss_clean|callback_check_time_valid['.$this->input->post('appointment_time').']');
			$this->form_validation->set_rules('return_time','Return time','trim|required|xss_clean|callback_return_time_valid['.$parameters.']');
			$this->form_validation->set_rules('pick_up_street', 'Pick up address', 'trim|required|min_length[3]|max_length[100]|xss_clean');
			$this->form_validation->set_rules('state', 'State', 'trim|required|xss_clean');
			$this->form_validation->set_rules('urgency_type', 'Urgency type', 'trim|required|xss_clean');
			$this->form_validation->set_rules('req_again_ride', 'Trip type', 'trim|required|xss_clean');
			$this->form_validation->set_rules('country', 'Country', 'trim|required|xss_clean');
			$this->form_validation->set_rules('zipcode','Zipcode','trim|required|min_length[5]|max_length[15]|integer|xss_clean');
			$this->form_validation->set_rules('health_service','Health service','trim|required|xss_clean');
			$this->form_validation->set_rules('special_request', 'Special request', 'trim|required|xss_clean');
			$this->form_validation->set_rules('special_instruction', 'Special instruction', 'trim|required|alpha_numeric_spaces|min_length[3]|max_length[100]|xss_clean');
			$this->form_validation->set_rules('desire_transp', 'Desired transportation','trim|required|xss_clean');		
			$this->form_validation->set_rules('urgency_exp', 'Urgency','trim|xss_clean|callback_check_urjency_type['.$urgency_param.']');
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('frontDrivers/driverMyScheduleRide',$data);
			}
			else
			{
				//get input data from user
				$postData = $this->input->post();	
				$query = $this->db->query("SELECT id, sr_trip_id FROM tbl_schedule_ride ORDER BY id DESC LIMIT 1");
				$result = $query->result_array();
				if(!empty($result[0]['sr_trip_id'])) {
					$tripID=explode("T", $result[0]['sr_trip_id']);	
					$tripIDGenerated = "T".str_pad($tripID[1]+1, 14, '0', STR_PAD_LEFT);
				} else {
					$tripIDGenerated = "T00000000000001";
				}		

				if($postData['req_again_ride'] == 1)
					$return_time =$postData['return_time'];
				else
					$return_time = "";

				if($postData['urgency_type'] == 1)
					$urgency_exp = $postData['urgency_exp'];
				else
					$urgency_exp ="";

				$scheduleRideSession =array(); 
				$scheduleRideSession = array(
					'sr_date' => $postData['date'],
					'sr_pick_up_time' => $postData['pick_up_time'],
					'sr_appointment_time' => $postData['appointment_time'],	
					'sr_street' => trim($postData['pick_up_street']),
					'sr_country' => $postData['country'],
					'sr_state' => $postData['state'],
					'sr_zipcode' => trim($postData['zipcode']),
					'sr_trip_id' => $tripIDGenerated,
					'sr_health_service'=>$postData['health_service'],
					'sr_special_request' => $postData['special_request'],							
					'sr_special_instruction' => trim($postData['special_instruction']),
					'sr_desire_transportaion' => $postData['desire_transp'],								
					'user_id'=>$this->session->userdata('user_id'),
					'sr_urgency_type'=>$postData['urgency_type'],
					'sr_trip_type'=>$postData['req_again_ride'],
					'urjency_explainatin'=>$urgency_exp,
					'sr_return_time'=>$return_time,
									//'created_on'=>date("Y-m-d H:i:a"),
					'sr_status'=>1
					);
				$data['form_data']=$scheduleRideSession;
				$this->load->view('user/address',$data);
			}
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

    //Return Ride
	public function driver_return_schedule_an_ride()
	{	
		if($this->session->userdata('user_id'))
		{
			$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY,array('id'=>231));
			$data['states'] = $this->Common_model->select('*',TB_STATE);	
			$data['desire_tran'] = $this->Common_model->select('*',TB_DESIRE_TRANSPORTATION);
			$data['hc_service'] = $this->Common_model->select('*',TB_HEALTHCARE_SERVICES);
			$data['sp_request'] = $this->Common_model->select('*',TB_SPECIAL_REQUEST);		
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
			$data['home_section1'] = $this->Common_model->selectWhereIn('page_id,sub_title,page_title,page_detail',TB_PAGES,'page_id',array(2,14,7));
			$this->load->view('frontDrivers/driverReturnScheduleAnRide',$data);
		} else {
			redirect("frontend-login"); exit;
		}
	}

	//check time validation
	function check_time_valid($pickup_time,$appointment_time)
	{		
		if($appointment_time <= $pickup_time)
		{			
			$this->form_validation->set_message('check_time_valid', 'Pick up time should be less than appointment time.');
			return false;  
		}
		else
		{
			return true;
		}
	}

	public function driverScheduleAnRide()
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
		$data['home_section1'] = $this->Common_model->selectWhereIn('page_id,sub_title,page_title,page_detail',TB_PAGES,'page_id',array(2,14,7));				
		$this->load->view('frontDrivers/driverScheduleAnRide',$data);
	}

	public function driver_schedule_an_trip_details()
	{
		if($this->session->userdata('user_id'))  
		{	
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$this->load->view("frontDrivers/driverScheduleAnTripDetails",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}
	public function driver_my_schedule_rides()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$this->load->view("frontDrivers/driverMyScheduleRides",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	public function driver_notification()
	{
		if($this->session->userdata('user_id'))
		{
			$user_id =$this->session->userdata('user_id');
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['notification'] = $this->Common_model->selectQuery("*",TB_NOTIFICATION,array('user_id'=>$user_id),array('id desc'=>'order by'));
			$totalNotif = $this->Common_model->select("count(*) AS notification",TB_NOTIFICATION,array('user_id'=>$user_id));
			if($totalNotif[0]['notification']>=1)
			{				
				$flagReset = array('notification_flag' => 1);
				$where = array('id' =>$user_id); 
				$this->Common_model->update(TB_NOTIFICATION,$where,$flagReset);	    		
			}		
			$this->load->view("frontDrivers/driverNotification",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}
	
	public function notificationRead() { 
		$notificationId = $this->input->post("id");	
		$user_id =$this->session->userdata('user_id');
		
		$flagReset = array('notification_flag' => 1);
		$where = array('id' =>$notificationId); 
		$result = $this->Common_model->update(TB_NOTIFICATION,$where,$flagReset);
		
		if($result){
			$totalNotif = $this->Common_model->select("count(*) AS notification",TB_NOTIFICATION,array('user_id'=>$user_id,'notification_flag'=>'2'));
			echo json_encode(array('count'=>$totalNotif[0]['notification'],'status'=>'true')); die;
			 //echo 'true';	
		}else{
			echo 'false'; die;
		}		
		
	}

	public function driver_my_ride()
	{
		if($this->session->userdata('user_id'))
		{  
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['scheduleRide'] = $this->Common_model->select("id,sr_trip_id",TB_SCHEDULE_RIDE);
			$query = $this->db->query("SELECT sr.id,sr.sr_trip_id,sr.sr_date,sr.sr_pick_up_time,u.full_name,u.picture FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.user_id = '".$this->session->userdata('user_id')."' AND sr.sr_status =1");
			$data['tripDetails'] = $query->result_array();
			$completedRideQuery = $this->db->query("SELECT sr.id,sr.sr_trip_id,sr.sr_date,sr.sr_pick_up_time,u.full_name,u.picture FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.user_id = '".$this->session->userdata('user_id')."' AND sr.sr_status =2");
			$data['completedTripDetails'] = $completedRideQuery->result_array();
			$cancelledRideQuery = $this->db->query("SELECT sr.id,sr.sr_trip_id,sr.sr_date,sr.sr_pick_up_time,u.full_name,u.picture FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.user_id = '".$this->session->userdata('user_id')."' AND sr.sr_status =3");
			$data['cancelledTripDetails'] = $cancelledRideQuery->result_array();
			$data['srstatus'] = $this->input->get("sr_status");

			$this->load->view("frontDrivers/driverMyRides",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}	
	}

	public function get_schedule_ride_details()
	{
		$id = $this->input->post("id");		
		$getScheduleRideDetailsQuery = $this->db->query("SELECT sr.id,sr.sr_trip_id,sr.sr_pick_up_time,DATE_FORMAT(sr.sr_date, '%W %Y %M %e') as sr_date,sr.sr_street,u.full_name FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.id = $id");
		$data = $getScheduleRideDetailsQuery->result_array();
		$this->output
		->set_content_type("application/json")
		->set_output(json_encode(array("status"=>true,'data'=>$data)));
	}

	public function schedule_ride_cancelled()
	{
		$scheduleRideID = $this->input->post("id");		
		$datareset = array('sr_status' => 3);
		$where = array('id' =>$scheduleRideID ); 
		$result = $this->Common_model->update(TB_SCHEDULE_RIDE,$where,$datareset);
		if($result)
		{
			$this->output
			->set_content_type("application/json")
			->set_output(json_encode(array("status"=>true,"message"=>"Schedule ride has been cancelled successfully.")));
		}	
		else
		{
			$this->output
			->set_content_type("application/json")
			->set_output(json_encode(array("status"=>false,"message"=>"Schedule ride has not been cancelled successfully.")));
		}
	}
	//delete notification 
	public function delete_notification()
	{
		$deleteNotifID = $this->input->post("id");				
		$where = array('id' =>$deleteNotifID); 
		$result = $this->Common_model->delete(TB_NOTIFICATION,$where);
		if($result)
		{
			$this->output
			->set_content_type("application/json")
			->set_output(json_encode(array("status"=>true,"message"=>"Notification has been deleted successfully.")));
		}	
		else
		{
			$this->output
			->set_content_type("application/json")
			->set_output(json_encode(array("status"=>false,"message"=>"Notification has not been deleted successfully.")));
		}
	}
	//----------- Confirm NEMT -----------
	public function confirmNEMT()
	{
		if($this->session->userdata('user_id'))
		{
			$postData = $this->input->post();
			$sched_id = $postData['id'];
			$alreadyConfirm = $this->Common_model->select('id',TB_SCHEDULE_RIDE,array('id' => $sched_id,'user_id'=>$this->session->userdata('user_id'),'driver_status'=>'3'));
			if(count($alreadyConfirm) > 0)
			{
				echo json_encode(array('status' => "success", "message"=>"You already confirm this request."));
			}
			$up_cond = array('id' => $sched_id,'user_id'=>$this->session->userdata('user_id'));
			$update_data = array('driver_status'=>'3');
			$result = $this->Common_model->update(TB_SCHEDULE_RIDE,$up_cond,$update_data);
			if($result)
			{
				echo json_encode(array('status' => "success", "message"=>"Your request has been confirmed successfully."));
			}else
			{
				echo json_encode(array('status' =>'error','message'=>"Oops!!! Something went wrong."));
			}
		}
	}

	//------- REJECT NEMT -------
	public function rejectNEMT()
	{
		if($this->session->userdata('user_id'))
		{
			$postData = $this->input->post();
			$sched_id = $postData['id'];
			$gettripdetails = $this->Common_model->select("id,driver_id,sr_lat_lng_source,sr_date,sr_pick_up_time,sr_total_distance,source_address,destination_address",TB_SCHEDULE_RIDE,array("id"=>$sched_id));
			$driver_id = $gettripdetails[0]['driver_id'];
			$latlong = $gettripdetails[0]['sr_lat_lng_source'];
			$newlatlong = explode(",",$latlong);
			$src_lat = $newlatlong[0];
			$src_long = $newlatlong[1];

			$query = $this->db->query("SELECT * FROM  tbl_driver_location WHERE `updated_at` >= CURDATE( ) AND user_id != $driver_id");
			$getDriverLocation = $query->result_array();
			if(empty($getDriverLocation)){
				echo json_encode(array('status' => "error", "message"=>"Today no drivers available."));
				exit();
			}
			else{
				$up_cond = array('id' => $sched_id,'user_id'=>$this->session->userdata('user_id'));
				$update_data = array('driver_status'=>'1','driver_id'=>'0','admin_status'=>1);
				$result = $this->Common_model->update(TB_SCHEDULE_RIDE,$up_cond,$update_data);

				foreach ($getDriverLocation as $key => $value) {
					$getDistance = $this->distance($src_lat, $src_long, $value['loc_lat'], $value['loc_long'], "M");
					if(round($getDistance)<=5)
					{
						$this->Common_model->insert(TB_DRIVER_REQUEST,array("sched_id"=>$sched_id,"driver_id"=>$value['user_id'],"created_date"=>date("Y-m-d H:i:s"),"ride_status"=>'Request Sent'));	
						$notificationArr = array(
							"n_title" =>"Schedule a ride request",
							"n_full_description"=>"Please check your trip request.<br />Schedule ride date :<b>".$gettripdetails[0]['sr_date']."</b> <br />Pick up time :<b>".$gettripdetails[0]['sr_pick_up_time']."</b><br />Total distance :<b>".$gettripdetails[0]['sr_total_distance']."</b> <br />Source location :<b>".$gettripdetails[0]['source_address']."</b><br /> Destination location: <b>".$gettripdetails[0]['destination_address']."</b>",
							"notification_flag"=>2,
							"read_or_unread" =>1,
							"n_status"=>'ME',
							"user_id" =>$value['user_id'],
							"created_on" => date("Y-m-d H:i:s"),
							);
						$notifResult = $this->Common_model->insert(TB_NOTIFICATION,$notificationArr);
					}
					else
					{
						echo json_encode(array('status' => "error", "message"=>"No drivers available within 5 miles."));
						exit();
					}
				}		
				if($result)
				{
					echo json_encode(array('status' => "success", "message"=>"Your schedule request has been sent successfully ."));
					exit();
				}
				else
				{
					echo json_encode(array('status' => "error", "message"=>"Your schedule request has been failed ."));
					exit();
				}	
			}		
		}
	}
	public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
		if ($unit == "K") {
			return ($miles * 1.609344);
		} else if ($unit == "N") {
			return ($miles * 0.8684);
		} else {
			return $miles;
		}
	}

	//-------- show helathcare list  -------------
	public function showHealthcareList()
	{
		if($this->session->userdata('user_id'))
		{
			$postData = $this->input->post();
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			$data['drcat'] = $this->Common_model->select("*",TB_DOCTORS_CAT,array("isDeleted"=>"0"));
			$data['healthcarelist'] = $this->Common_model->selectQuery("*",TB_USERS,array("user_type"=>3,"status"=>"active"),array("username"=>"ASC"));
			$this->load->view('frontDrivers/showHealthCare',$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}	
	}
}
