<?php if(!defined("BASEPATH")) exit("No direct script access allowed");
class DriverController extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('security');
		$this->load->library('form_validation');
		$this->load->library('upload');	
		$this->load->helper('file');
		$this->load->helper(array('form', 'url'));
	}

	// default driver dashboard
	public function index()
	{		
		if($this->session->userdata('user_id'))
		{	
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1)); // footer section
			$data['myRideCount'] = $this->Common_model->group_by("sr_status,count(*) total",TB_SCHEDULE_RIDE,array('sr_status'),array('user_id'=>$this->session->userdata('user_id'))); 
			$data['scheduledRide'] = $this->Common_model->select("sr_status,count(*) total",TB_SCHEDULE_RIDE,array('user_id'=>$this->session->userdata('user_id'),'sr_status' => '1')); 
			$data['completedRide'] = $this->Common_model->select("sr_status,count(*) total",TB_SCHEDULE_RIDE,array('user_id'=>$this->session->userdata('user_id'),'sr_status' => '2')); 
			$data['cancelledRide'] = $this->Common_model->select("sr_status,count(*) total",TB_SCHEDULE_RIDE,array('user_id'=>$this->session->userdata('user_id'),'sr_status' => '3')); 
			$data['userDetails']=$this->Common_model->select('picture,full_name,email',TB_USERS,array('id'=>$this->session->userdata('user_id')));
			$cond3 = array("tbl_schedule_ride.user_id" => $this->session->userdata('user_id'),"tbl_schedule_ride.sr_date" => date("Y-m-d"));
			$jointype=array("tbl_country"=>"LEFT","tbl_states"=>"LEFT","tbl_drivers"=>"LEFT");
			$join = array("tbl_country"=>"tbl_country.id = tbl_schedule_ride.sr_country","tbl_states"=>"tbl_states.id = tbl_schedule_ride.sr_state","tbl_drivers"=>"tbl_drivers.id = tbl_schedule_ride.driver_id");
			$data['getMapDetails']=$this->Common_model->selectQuery("tbl_schedule_ride.id,tbl_country.country_name,tbl_states.state_name,sr_street,sr_zipcode,tbl_drivers.latitude as driverLat,tbl_drivers.longitude as driverLong,tbl_schedule_ride.driver_id",TB_SCHEDULE_RIDE,$cond3,array(),$join,$jointype);	
			
			$this->load->view("frontDrivers/driverDashBoard",$data);		
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	// driver profile view
	public function driver_profile()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['getDriverDetails'] = $this->Common_model->select("*",TB_USERS,array('id'=>$this->session->userdata('user_id'))); // get driver records
			$query = $this->db->query("SELECT ct.city_name,s.state_name,c.country_name FROM tbl_city as ct 
									   left join tbl_states as s on ct.state_id = s.id
									   left join tbl_country as c on c.id = s.country_id
									   where ct.id=".$data['getDriverDetails'][0]['city']."");
			$data['countryDetails']=$query->result_array();		
			$this->load->view("frontDrivers/driverProfile",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}	
	}
	
	public function view_map()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$cond3 = array("tbl_schedule_ride.user_id" => $this->session->userdata('user_id'),"tbl_schedule_ride.sr_date" => date("Y-m-d"));
			$jointype=array("tbl_country"=>"LEFT","tbl_states"=>"LEFT","tbl_drivers"=>"LEFT");
			$join = array("tbl_country"=>"tbl_country.id = tbl_schedule_ride.sr_country","tbl_states"=>"tbl_states.id = tbl_schedule_ride.sr_state","tbl_drivers"=>"tbl_drivers.id = tbl_schedule_ride.driver_id");
			$data['getPickupDetails']=$this->Common_model->selectQuery("tbl_schedule_ride.id,tbl_country.country_name,tbl_states.state_name,sr_street,sr_zipcode,tbl_drivers.latitude as driverLat,tbl_drivers.longitude as driverLong",TB_SCHEDULE_RIDE,$cond3,array(),$join,$jointype);	
			if(count($data['getPickupDetails'])>0){
				$locationAdd='';
				if($data['getPickupDetails'][0]['sr_street']){ $locationAdd .=$data['getPickupDetails'][0]['sr_street'].','; }
				if($data['getPickupDetails'][0]['sr_zipcode']){ $locationAdd .=$data['getPickupDetails'][0]['sr_zipcode'].','; }
				if($data['getPickupDetails'][0]['state_name']){ $locationAdd .=$data['getPickupDetails'][0]['state_name'].','; }
				if($data['getPickupDetails'][0]['country_name']){ $locationAdd .=$data['getPickupDetails'][0]['country_name'].','; }
				$address = str_replace(" ", "+", $locationAdd);
				$json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
				$json = json_decode($json);
				$lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
				$long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
				$data['getPickupDetails'][0]['default_lat'] = $lat;
				$data['getPickupDetails'][0]['default_lon'] = $long;				
			}			
			$this->load->view("frontDrivers/viewMap",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}	
	}

	// driver edit profile
	public function driver_edit_profile()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);
			$data['getDriverDetails'] = $this->Common_model->select("*",TB_USERS,array('id'=>$this->session->userdata('user_id')));
			$data['stateData'] = $this->Common_model->select("*",TB_STATE,array('country_id'=>$data['getDriverDetails'][0]['country']));
			$data['cityData'] = $this->Common_model->select("*",TB_CITY,array('state_id'=>$data['getDriverDetails'][0]['state']));
			$this->load->view("frontDrivers/driverEditProfile",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}
	
	// changes in driver edit profile
	public function update_driver_edit_profile()
	{		
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);
			$data['getDriverDetails'] = $this->Common_model->select("*",TB_USERS,array('id'=>$this->session->userdata('user_id')));
			$data['stateData'] = $this->Common_model->select("*",TB_STATE,array('country_id'=>$data['getDriverDetails'][0]['country']));
			$data['cityData'] = $this->Common_model->select("*",TB_CITY,array('state_id'=>$data['getDriverDetails'][0]['state']));
			$this->form_validation->set_rules('full_name', 'full name', 'trim|required|min_length[3]|max_length[50]|xss_clean');
			$this->form_validation->set_rules('date_of_birth','date of birth','trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'email', 'trim|required|max_length[50]|min_length[6]|valid_email|xss_clean');
			$this->form_validation->set_rules('phone_no', 'phone', 'trim|required|max_length[20]|min_length[10]|integer|xss_clean');
			$this->form_validation->set_rules('country', 'country', 'trim|required|xss_clean');
			$this->form_validation->set_rules('state', 'state', 'trim|required|xss_clean');		
			$this->form_validation->set_rules('city', 'city', 'trim|required|xss_clean');	
			$this->form_validation->set_rules('zipcode', 'zipcode', 'trim|required|integer|exact_length[6]|xss_clean');
			$this->form_validation->set_rules('county', 'county', 'trim|required|exact_length[5]|integer|xss_clean');
			$this->form_validation->set_rules('street', 'street', 'trim|required|xss_clean');
			$this->form_validation->set_rules('insurance_id', 'insurance id', 'trim|required|xss_clean');		
			$this->form_validation->set_rules('emergency_contact_name', 'emergency contact name', 'trim|required|min_length[3]|max_length[50]|xss_clean');
			$this->form_validation->set_rules('emergency_contact_number', 'emergency contact number', 'trim|required|max_length[20]|min_length[8]|xss_clean');
			$this->form_validation->set_rules('signature', 'signature', 'trim|required|xss_clean');		
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
			if( $this->form_validation->run() == FALSE )
	        {  			        	
				$this->load->view('frontDrivers/driverEditProfile',$data);				
			}
			else
			{ 
				$perDoc=$signDoc="";
				//get input data from user
				$postData = $this->input->post();		
				if(!empty($_FILES["sign_file_upload"]["name"]))   
			    {   
			        $file_name = $_FILES["sign_file_upload"]["name"];
			        $original_file_name = $file_name;
			        $random = rand(1, 10000000000000000);
			        $date = date("Y-m-d-h:i:sa");
			        $makeRandom = $random;
			        $file_name_rename = $makeRandom;
			        $explode = explode('.', $file_name);
			        if(count($explode) >= 2) 
			        {
			            $new_file = $file_name_rename.$date.'.'.$explode[1];		            
			            $config['upload_path'] = "./uploads/Driver/doc";
			            $config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
			            $config['file_name'] = $new_file;
			            $config['max_size'] = '307210';
			            $config['max_width'] = '300000';
			            $config['max_height'] = '300000';
			            $config['overwrite'] = TRUE;
			            $this->load->library('upload',$config);
			            $this->upload->initialize($config);
						if(!$this->upload->do_upload("sign_file_upload")) 
						{
							$error = $this->upload->display_errors();
							print_r($error);exit;
							//echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
						}
						else
						{
							$signDoc = 'uploads/Driver/doc/'.$new_file;
						}
			        }
				}			
				else if(isset($postData["sign_file_upload_old"]))   
			    {          	 
			    	$signDoc = $postData["sign_file_upload_old"];		        				
				}
		        else
		        {
			    	$signDoc ="";	
		        }

		        if(!empty($_FILES["personal_doc"]["name"]))   
			    {   		    	
			        $file_name = $_FILES["personal_doc"]["name"];
			        $original_file_name = $file_name;
			        $random = rand(1, 10000000000000000);
			        $date = date("Y-m-d-h:i:sa");
			        $makeRandom = $random;
			        $file_name_rename = $makeRandom;
			        $explode = explode('.', $file_name);
			        if(count($explode) >= 2) 
			        {
			            $new_file = $file_name_rename.$date.'.'.$explode[1];		            
			            $config['upload_path'] = "./uploads/Driver/doc";
			            $config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
			            $config['file_name'] = $new_file;
			            $config['max_size'] = '307210';
			            $config['max_width'] = '300000';
			            $config['max_height'] = '300000';
			            $config['overwrite'] = TRUE;
			            $this->load->library('upload',$config);
			            $this->upload->initialize($config);
						if(!$this->upload->do_upload("personal_doc")) 
						{
							$error = $this->upload->display_errors();
							print_r($error);exit;
							//echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
						}
						else
						{
							$perDoc = 'uploads/Driver/doc/'.$new_file;
						}
			        }
				}
				else if(isset($postData["personal_doc_old"]))   
			    {          	 
			    	
			    	$perDoc =$postData["personal_doc_old"];		        				
				}
		        else
		        {
		        	
			    	$perDoc ="";	
		        }	
		        if(!empty($_FILES["profile_image"]["name"]))   
			    {   		    	
			        $file_name = $_FILES["profile_image"]["name"];
			        $original_file_name = $file_name;
			        $random = rand(1, 10000000000000000);
			        $date = date("Y-m-d-h:i:sa");
			        $makeRandom = $random;
			        $file_name_rename = $makeRandom;
			        $explode = explode('.', $file_name);
			        if(count($explode) >= 2) 
			        {
			            $new_file = $file_name_rename.$date.'.'.$explode[1];		            
			            $config['upload_path'] = "./uploads/Driver/thumb";
			            $config['allowed_types'] ="png|jpeg|jpg";
			            $config['file_name'] = $new_file;
			            $config['max_size'] = '307210';
			            $config['max_width'] = '300000';
			            $config['max_height'] = '300000';
			            $config['overwrite'] = TRUE;
			            $this->load->library('upload',$config);
			            $this->upload->initialize($config);
						if(!$this->upload->do_upload("profile_image")) 
						{
							$error = $this->upload->display_errors();
							print_r($error);exit;
							//echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
						}
						else
						{
							$profImage = 'uploads/Driver/thumb/'.$new_file;
						}
			        }
				}
				else if(isset($postData["profile_image_old"]))   
			    {          	 
			    	
			    	$profImage =$postData["profile_image_old"];		        				
				}
		        else
		        {
		        	
			    	$profImage ="";	
		        } 
				$updateArr  = array(
									'full_name'=>$postData['full_name'],
									'date_of_birth' => date("Y-m-d",strtotime($postData['date_of_birth'])),
									'email' => $postData['email'],
									'phone' => $postData['phone_no'],	
									'street' => trim($postData['street']),
									'country' => $postData['country'],
									'state' => $postData['state'],
									'city'=>$postData['city'],
									'zipcode' => trim($postData['zipcode']),
									'county' => $postData['county'],								
									'insurance_id' => $postData['insurance_id'],							
									'emergency_contactname' => trim($postData['emergency_contact_name']),
									'emergency_contactno' => $postData['emergency_contact_number'],								
									'personal_doc'=>$perDoc,
									'signature_file_upload'=>$signDoc,	
									'picture'=>$profImage,							
									'signature'=>$postData['signature'],								
									'updated_at'=>date("Y-m-d H:i:a")
								);
				$where = array('id' =>$this->session->userdata('user_id'));
				// Record insert in schedule ride table
				$result = $this->Common_model->update(TB_USERS,$where,$updateArr);
				if($result)
				{
					$this->session->set_flashdata("success","Driver edit profile has been updated successfully");
					redirect("my-profile");
				}
				else
				{
					$this->session->set_flashdata("fail","Driver edit profile has not been updated successfully");				
					redirect(current_url());
				}
			}
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	// trip details
	public function ride_trip_details_view()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$this->load->view("frontDrivers/rideTripDetailsView",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	//complaints from customer on driver
	public function customer_complaints()
	{
		if($this->session->userdata('user_id'))
		{	
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['complainData'] = $this->Common_model->select("*",TB_COMPLAINTS_TYPE);
			if($this->input->post())
			{
				$postData = $this->input->post();
				$insertArr = array(
						'src_username'=> $postData['userName'],
						'src_driver_name'=> $postData['driverName'],
						'src_complaint_type'=> $postData['complaintType'],
						'src_date_of_service'=> $postData['dateOfService'],
						'src_name_of_transportation_provider'=> $postData['nameOfTransportProv'],
						'src_description'=> $postData['description']
					);

				// Record insert in user table
				$result = $this->Common_model->insert(TB_SCHEDULE_RIDE_COMPLAINTS,$insertArr);	
				if($result)
				{
					$this->output
    			 		 ->set_content_type("application/json")
    			 		 ->set_output(json_encode(array("status"=>true)));
				}
				else
				{
					$this->output
    			 		 ->set_content_type("application/json")
    			 		 ->set_output(json_encode(array("status"=>false,"message"=>"your complaint has been fail to submitted")));
				}				
			}
			else
			{
				$this->load->view("frontDrivers/customerComplaints",$data);
			}
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	public function driver_schedule_an_ride()
	{	
		if($this->session->userdata('user_id'))
		{
			$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);
			$data['states'] = $this->Common_model->select('*',TB_STATE);	
			$data['desire_tran'] = $this->Common_model->select('*',TB_DESIRE_TRANSPORTATION);
			$data['hc_service'] = $this->Common_model->select('*',TB_HEALTHCARE_SERVICES);
			$data['sp_request'] = $this->Common_model->select('*',TB_SPECIAL_REQUEST);		
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
			$data['home_section1'] = $this->Common_model->selectWhereIn('page_id,sub_title,page_title,page_detail',TB_PAGES,'page_id',array(2,14,7));				
			// form validation
			$this->form_validation->set_rules('date','date','trim|required|xss_clean');		
			$this->form_validation->set_rules('appointment_time', 'appointment time', 'trim|required|xss_clean');
			$this->form_validation->set_rules('pick_up_time','pick up time','trim|required|xss_clean|callback_check_time_valid['.$this->input->post('appointment_time').']');
			$this->form_validation->set_rules('pick_up_street', 'pick up address', 'trim|required|min_length[3]|max_length[100]|xss_clean');
			$this->form_validation->set_rules('state', 'state', 'trim|required|xss_clean');
			$this->form_validation->set_rules('country', 'country', 'trim|required|xss_clean');
			$this->form_validation->set_rules('zipcode','zipcode','trim|required|min_length[5]|integer|xss_clean');
			$this->form_validation->set_rules('health_service','health service','trim|required|xss_clean');
			$this->form_validation->set_rules('special_request', 'special request', 'trim|required|xss_clean');
			$this->form_validation->set_rules('special_instruction', 'special instruction', 'trim|required|alpha_numeric_spaces|min_length[3]|max_length[100]|xss_clean');
			$this->form_validation->set_rules('desire_transp', 'desired transportation','trim|required|xss_clean');		
			$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			if ($this->form_validation->run() == FALSE)
			{
				$this->load->view('frontDrivers/driverMyScheduleRide',$data);
			}
			else
			{
				//get input data from user
				$postData = $this->input->post();	
				$query = $this->db->query("SELECT id, sr_trip_id FROM tbl_schedule_ride ORDER BY id DESC LIMIT 1");
				$result = $query->result_array();
				$tripID=explode("T", $result[0]['sr_trip_id']);
				$tripIDGenerated = "T".str_pad($tripID[1]+1, 14, '0', STR_PAD_LEFT);
				$insertArr  = array(
									'sr_date' => date("Y-m-d",strtotime($postData['date'])),
									'sr_pick_up_time' => $postData['pick_up_time'],
									'sr_appointment_time' => $postData['appointment_time'],	
									'sr_street' => trim($postData['pick_up_street']),
									'sr_country' => $postData['country'],
									'sr_state' => $postData['state'],
									'sr_zipcode' => trim($postData['zipcode']),
									'sr_trip_id' => $tripIDGenerated,
									'sr_special_request' => $postData['special_request'],							
									'sr_special_instruction' => trim($postData['special_instruction']),
									'sr_desire_transportaion' => $postData['desire_transp'],								
									'user_id'=>$this->session->userdata('user_id'),
									'created_on'=>date("Y-m-d H:i:a"),
									'sr_status'=>1
								);

				// Record insert in schedule ride table
				$result = $this->Common_model->insert(TB_SCHEDULE_RIDE,$insertArr);
				if($result)
				{
					$this->session->set_flashdata("success","Schedule a ride has been inserted successfully");
					$this->load->view('frontDrivers/driverScheduleAnRide',$data);
				}
				else
				{
					$this->session->set_flashdata("fail","Schedule a ride has not been inserted successfully");				
					redirect(current_url());
				}
			}
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

    //Return Ride
	public function driver_return_schedule_an_ride()
	{	
		if($this->session->userdata('user_id'))
		{
			$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);
			$data['states'] = $this->Common_model->select('*',TB_STATE);	
			$data['desire_tran'] = $this->Common_model->select('*',TB_DESIRE_TRANSPORTATION);
			$data['hc_service'] = $this->Common_model->select('*',TB_HEALTHCARE_SERVICES);
			$data['sp_request'] = $this->Common_model->select('*',TB_SPECIAL_REQUEST);		
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
			$data['home_section1'] = $this->Common_model->selectWhereIn('page_id,sub_title,page_title,page_detail',TB_PAGES,'page_id',array(2,14,7));
			$this->load->view('frontDrivers/driverReturnScheduleAnRide',$data);
		} else {
			redirect("frontend-login"); exit;
		}
    }

	//check time validation
	function check_time_valid($pickup_time,$appointment_time)
	{		
		if($appointment_time <= $pickup_time)
		{			
			$this->form_validation->set_message('check_time_valid', 'Pick up time should not be greater than or equal appointment time.');
        	return false;  
		}
		else
		{
			return true;
		}
	}

	public function driverScheduleAnRide()
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
		$data['home_section1'] = $this->Common_model->selectWhereIn('page_id,sub_title,page_title,page_detail',TB_PAGES,'page_id',array(2,14,7));				
		$this->load->view('frontDrivers/driverScheduleAnRide',$data);
	}

	public function driver_schedule_an_trip_details()
	{
		if($this->session->userdata('user_id'))  
		{	
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$this->load->view("frontDrivers/driverScheduleAnTripDetails",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}
	public function driver_my_schedule_rides()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$this->load->view("frontDrivers/driverMyScheduleRides",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	public function driver_notification()
	{
		if($this->session->userdata('user_id'))
		{
			$user_id =$this->session->userdata('user_id');
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['notification'] = $this->Common_model->selectQuery("*",TB_NOTIFICATION,array('user_id'=>$user_id),array('id desc'=>'order by'));
			$totalNotif = $this->Common_model->select("count(*) AS notification",TB_NOTIFICATION,array('user_id'=>$user_id));
			/*if($totalNotif[0]['notification']>=1)
			{
				$flagReset = array('notification_flag' => "2");
				$where = array('user_id' =>$this->session->userdata('user_id') ); 
	    		$this->Common_model->update(TB_NOTIFICATION,$where,$flagReset);
			}	*/
			//echo $data['notification'][0]['id'];exit;
			if($totalNotif[0]['notification']>=1)
			{				
				$flagReset = array('notification_flag' => 1);
				$where = array('id' =>$user_id); 
	    		$this->Common_model->update(TB_NOTIFICATION,$where,$flagReset);	    		
	    	}		
			$this->load->view("frontDrivers/driverNotification",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}
	
	public function notificationRead() { 
		$notificationId = $this->input->post("id");	
		$user_id =$this->session->userdata('user_id');
		
		$flagReset = array('notification_flag' => 1);
		$where = array('id' =>$notificationId); 
	    $result = $this->Common_model->update(TB_NOTIFICATION,$where,$flagReset);
		
		if($result){
			$totalNotif = $this->Common_model->select("count(*) AS notification",TB_NOTIFICATION,array('user_id'=>$user_id,'notification_flag'=>'2'));
			echo json_encode(array('count'=>$totalNotif[0]['notification'],'status'=>'true')); die;
			 //echo 'true';	
		}else{
			echo 'false'; die;
		}		
		
	}
		
	public function driver_my_ride()
	{
		if($this->session->userdata('user_id'))
		{  
			$data['footer'] = $this->Common_model->select("*",TB_SETTINGS,array('id'=>1));
			$data['scheduleRide'] = $this->Common_model->select("id,sr_trip_id",TB_SCHEDULE_RIDE);
			$query = $this->db->query("SELECT sr.id,sr.sr_trip_id,sr.sr_date,sr.sr_pick_up_time,u.full_name,u.picture FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.user_id = '".$this->session->userdata('user_id')."' AND sr.sr_status =1");
			$data['tripDetails'] = $query->result_array();
			$completedRideQuery = $this->db->query("SELECT sr.id,sr.sr_trip_id,sr.sr_date,sr.sr_pick_up_time,u.full_name,u.picture FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.user_id = '".$this->session->userdata('user_id')."' AND sr.sr_status =2");
			$data['completedTripDetails'] = $completedRideQuery->result_array();
			$cancelledRideQuery = $this->db->query("SELECT sr.id,sr.sr_trip_id,sr.sr_date,sr.sr_pick_up_time,u.full_name,u.picture FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.user_id = '".$this->session->userdata('user_id')."' AND sr.sr_status =3");
			$data['cancelledTripDetails'] = $cancelledRideQuery->result_array();
			$data['srstatus'] = $this->input->get("sr_status");
		
			$this->load->view("frontDrivers/driverMyRides",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}	
	}

	public function get_schedule_ride_details()
	{
		$id = $this->input->post("id");		
		$getScheduleRideDetailsQuery = $this->db->query("SELECT sr.id,sr.sr_trip_id,sr.sr_pick_up_time,DATE_FORMAT(sr.sr_date, '%W %Y %M %e') as sr_date,sr.sr_street,u.full_name FROM tbl_schedule_ride as sr join tbl_users as u on u.id =sr.user_id where sr.id = $id");
		$data = $getScheduleRideDetailsQuery->result_array();
		$this->output
			 ->set_content_type("application/json")
			 ->set_output(json_encode(array("status"=>true,'data'=>$data)));
	}

	public function schedule_ride_cancelled()
	{
		$scheduleRideID = $this->input->post("id");		
		$datareset = array('sr_status' => 3);
		$where = array('id' =>$scheduleRideID ); 
    	$result = $this->Common_model->update(TB_SCHEDULE_RIDE,$where,$datareset);
    	if($result)
    	{
    		$this->output
    			 ->set_content_type("application/json")
    			 ->set_output(json_encode(array("status"=>true,"message"=>"Schedule ride has been cancelled successfully.")));
    	}	
    	else
    	{
    		$this->output
    			 ->set_content_type("application/json")
    			 ->set_output(json_encode(array("status"=>false,"message"=>"Schedule ride has not been cancelled successfully.")));
    	}
	}
	//delete notification 
	public function delete_notification()
	{
		$deleteNotifID = $this->input->post("id");				
		$where = array('id' =>$deleteNotifID); 
    	$result = $this->Common_model->delete(TB_NOTIFICATION,$where);
		if($result)
    	{
    		$this->output
    			 ->set_content_type("application/json")
    			 ->set_output(json_encode(array("status"=>true,"message"=>"Notification has been deleted successfully.")));
    	}	
    	else
    	{
    		$this->output
    			 ->set_content_type("application/json")
    			 ->set_output(json_encode(array("status"=>false,"message"=>"Notification has not been deleted successfully.")));
    	}
	}
}
