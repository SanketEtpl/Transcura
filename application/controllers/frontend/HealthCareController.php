<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed'); 

class HealthCareController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_model');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		$this->load->helper('security');
		$this->load->helper('email_template_helper');
		$this->load->library('upload');		
		$this->load->helper('string');
	}

	// ----- healthcare dashboard -----
	public function index()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
			$data['userDetails']=$this->Common_model->select('picture,full_name,email',TB_USERS,array('id'=>$this->session->userdata('user_id')));
			$this->load->view('frontHealthCare/healthcare_dashboard',$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}	
	}

	// ----- healthcare profile -----------
	public function healthcare_profile()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
			$category='';
			$cond = array(TB_USERS.".id" => $this->session->userdata('user_id'));
			$jointype=array(TB_COUNTRY=>"LEFT",TB_CITY=>"LEFT",TB_STATE=>"LEFT",TB_DOCTOR_SEARCH_BY_CAT_LOC=>"LEFT");
			$join = array(
				TB_COUNTRY=>TB_COUNTRY.".id = ".TB_USERS.".country",
				TB_STATE=>TB_STATE.".id = ".TB_USERS.".state",
				TB_CITY=>TB_CITY.".id = ".TB_USERS.".city",
				TB_DOCTOR_SEARCH_BY_CAT_LOC=>TB_DOCTOR_SEARCH_BY_CAT_LOC.".user_id = ".TB_USERS.".id");
			$select = "full_name,phone,email,date_of_birth,picture,zipcode,username,mobile,emergency_contactno,emergency_contactname,signature,state_name,country_name,city_name,county,street,category_id";
			$data['health_profile'] = $this->Common_model->selectQuery($select,TB_USERS,$cond,false,$join,$jointype); 

			$gethcat = $data['health_profile'][0]['category_id'];
			$sel = "cat_title";
			$all_catid = explode(",",$gethcat);
			foreach ($all_catid as $valuecat) {
				$health_cat = $this->Common_model->select($sel,TB_DOCTORS_CAT,array('cat_id' => $valuecat));
				$category.=$health_cat[0]['cat_title'].", ";
			}
			$data['category']=$category;
			$this->load->view("frontHealthCare/health_profile",$data);

		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}

	//-------- edit healthcare profile ----------
	public function editHealthProfile()
	{
		if($this->session->userdata('user_id'))
		{
			$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
			$cond = array(TB_USERS.".id" => $this->session->userdata('user_id'));
			$jointype=array(TB_COUNTRY=>"LEFT",TB_CITY=>"LEFT",TB_STATE=>"LEFT",TB_DOCTOR_SEARCH_BY_CAT_LOC=>"LEFT");
			$join = array(
				TB_COUNTRY=>TB_COUNTRY.".id = ".TB_USERS.".country",
				TB_STATE=>TB_STATE.".id = ".TB_USERS.".state",
				TB_CITY=>TB_CITY.".id = ".TB_USERS.".city",
				TB_DOCTOR_SEARCH_BY_CAT_LOC=>TB_DOCTOR_SEARCH_BY_CAT_LOC.".user_id = ".TB_USERS.".id");
			$select = "full_name,phone,email,date_of_birth,picture,zipcode,username,mobile,emergency_contactno,emergency_contactname,signature,state,state_name,country_name,city,city_name,county,street,category_id,signature_file_upload";
			$data['health_profile'] = $this->Common_model->selectQuery($select,TB_USERS,$cond,false,$join,$jointype); 

			$data['gethcat'] = $data['health_profile'][0]['category_id'];
			$data['health_category'] = $this->Common_model->select($sel,TB_DOCTORS_CAT);
			$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY,array('id'=>231));
			$data['stateData'] = $this->Common_model->select("*",TB_STATE,array('country_id'=>231));
			$data['cityData'] = $this->Common_model->select("*",TB_CITY,array('state_id'=>$data['health_profile'][0]['state']));
			$this->load->view("frontHealthCare/editHealthProfile",$data);
		}
		else
		{
			redirect("frontend-login"); exit;
		}
	}


	//---------- update healthcare profile --------------
	public function updateHealthProfile()
	{
		if($this->session->userdata('user_id'))
		{
			$perDoc=$signDoc="";
			$postData = $this->input->post();		
			if(!empty($_FILES["sign_file_upload"]["name"]))   
			{   
				$file_name = $_FILES["sign_file_upload"]["name"];
				$original_file_name = $file_name;
				$random = rand(1, 10000000000000000);
				$date = date("Y-m-d-h:i:sa");
				$makeRandom = $random;
				$file_name_rename = $makeRandom;
				$explode = explode('.', $file_name);
				if(count($explode) >= 2) 
				{
					$new_file = $file_name_rename.$date.'.'.$explode[1];		            
					$config['upload_path'] = "./uploads/Healthcare/doc";
					$config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
					$config['file_name'] = $new_file;
					$config['max_size'] = '307210';
					$config['max_width'] = '300000';
					$config['max_height'] = '300000';
					$config['overwrite'] = TRUE;
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload("sign_file_upload")) 
					{
						$error = $this->upload->display_errors();
						print_r($error);exit;
							//echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
					}
					else
					{
						$signDoc = 'uploads/Healthcare/doc/'.$new_file;
					}
				}
			}			
			else if(isset($postData["sign_file_upload_old"]))   
			{          	 
				$signDoc = $postData["sign_file_upload_old"];		        				
			}
			else
			{
				$signDoc ="";	
			}

				//----- profile image -----
			if(!empty($_FILES["profile_image"]["name"]))   
			{   		    	
				$file_name = $_FILES["profile_image"]["name"];
				$original_file_name = $file_name;
				$random = rand(1, 10000000000000000);
				$date = date("Y-m-d-h:i:sa");
				$makeRandom = $random;
				$file_name_rename = $makeRandom;
				$explode = explode('.', $file_name);
				if(count($explode) >= 2) 
				{
					$new_file = $file_name_rename.$date.'.'.$explode[1];		            
					$config['upload_path'] = "./uploads/Healthcare/thumb";
					$config['allowed_types'] ="png|jpeg|jpg";
					$config['file_name'] = $new_file;
					$config['max_size'] = '307210';
					$config['max_width'] = '300000';
					$config['max_height'] = '300000';
					$config['overwrite'] = TRUE;
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload("profile_image")) 
					{
						$error = $this->upload->display_errors();
						print_r($error);exit;
							//echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); exit;	
					}
					else
					{
						$profImage = 'uploads/Healthcare/thumb/'.$new_file;
					}
				}
			}
			else if(isset($postData["profile_image_old"]))   
			{          	 

				$profImage =$postData["profile_image_old"];		        				
			}
			else
			{

				$profImage ="";	
			}
			$health_cat = implode(",", $postData['health_category']); 
			//---- update data -------
			$updateArr  = array(
				'full_name'=>$postData['full_name'],
				'date_of_birth' => date("Y-m-d",strtotime($postData['date_of_birth'])),
				'phone' => $postData['phone_no'],	
				'street' => trim($postData['street']),
				'country' => $postData['country'],
				'state' => $postData['state'],
				'city'=>$postData['city'],
				'zipcode' => trim($postData['zipcode']),
				'county' => $postData['county'],											
				'emergency_contactname' => trim($postData['emergency_contact_name']),
				'emergency_contactno' => $postData['emergency_contact_number'],	
				'signature_file_upload'=>$signDoc,	
				'picture'=>$profImage,							
				'signature'=>$postData['signature'],								
				'updated_at'=>date("Y-m-d H:i:a")
				);
			$where = array('id' =>$this->session->userdata('user_id'));
			$result = $this->Common_model->update(TB_USERS,$where,$updateArr);
			if($result)
			{
				$health_arr= array('category_id'=>$health_cat);
				$cond_health = array('user_id' =>$this->session->userdata('user_id'));
				$resulth = $this->Common_model->update(TB_DOCTOR_SEARCH_BY_CAT_LOC,$cond_health,$health_arr);

				$this->session->set_flashdata("success","Profile updated successfully");
				redirect("healthcare-profile");
			}
			else
			{
				$this->session->set_flashdata("fail","Oops!!! Something went wrong.");				
				redirect(current_url());
			}	

		}else
		{
			redirect("frontend-login"); exit;
		}
	}

	//---- search healthcare --------
	public function search_healthcare()
	{
		if($this->session->userdata('user_id'))
		{
			$htmlData='';
			$postData = $this->input->post();
			$search_name=$postData['search_name'];
			$search_cat=$postData['search_cat'];

			if($search_name != "")
			{
				$cond_name = array(TB_USERS.".username"=>$search_name);
			}
			else
			{
				$cond_name =array();
			}

			if($search_cat=="")
			{
				$cond_cat = array("user_type"=>3,"status"=>"active");
				$cond_all = array_merge($cond_name,$cond_cat);
				$getDoc = $this->Common_model->selectQuery("*",TB_USERS,$cond_all,array("username"=>"ASC"));

			}else if($search_cat !="")
			{
				$select = TB_USERS.'.id,full_name,username,phone,email,user_type,insurance_id,category_id,latitude,longitude';
				$find = array('category_id'=>$search_cat);
				$cond_cat = array('status' =>'active');
				$cond_all = array_merge($cond_name,$cond_cat);
				$join = array(TB_DOCTOR_SEARCH_BY_CAT_LOC =>TB_DOCTOR_SEARCH_BY_CAT_LOC.'.user_id ='.TB_USERS.'.id');
				$getDoc = $this->Common_model->searchHealthcare($select,TB_USERS,$find,$cond_all,$join);
			}
			if(count($getDoc) > 0)
			{
				foreach ($getDoc as $healthcare) {
					$userid = $this->encrypt->encode($healthcare['id']);
					$uid = strtr($userid, '+/', '-_');
					$htmlData.='<div class="col-md-4">
					<div class="heal_rides">
						<div class="heal_img">
							<img src="'.base_url().'assets/img/healt_bg.png">
							<div class="hela_logo_box">
								<span class="hela_logo" style="background-image:url('.base_url().'assets/img/hea_logo.png);"></span>
							</div>
						</div>
						<div class="heal_inn">
							<div class="hel_name">'.$healthcare['username'].'</div>
							<div class="hel_txt">'.$healthcare['email'].'</div>
							<div class="hel_txt">'.$healthcare['phone'].'</div>
							<input type="hidden" name="id" value="'. $uid.'">
							<div class="heal_btn">
								<span><button type="submit">Apply</button></span>
								<span><button type="button">View Details</button></span>
							</div>	
						</div>
					</div>
				</div>';
			}	
		}else
		{
			$htmlData='Data not found';
		}
		echo json_encode(array('data'=>$htmlData));	
	}
	else
	{
		redirect("frontend-login"); exit;
	}
}

public function healthDetails()
{
	//echo "<pre>";print_r($_POST);die;
	if($this->session->userdata('user_id'))
	{
		$uid = strtr($_POST['id'], '-_', '+/');
		$user_id = $this->encrypt->decode($uid);
		$cond = array(TB_USERS.".id" => $user_id);
		$jointype=array(TB_COUNTRY=>"LEFT",TB_CITY=>"LEFT",TB_STATE=>"LEFT");
		$join = array(
			TB_COUNTRY=>TB_COUNTRY.".id = ".TB_USERS.".country",
			TB_STATE=>TB_STATE.".id = ".TB_USERS.".state",
			TB_CITY=>TB_CITY.".id = ".TB_USERS.".city");
		$select = "state_name,country_name,city_name,county,street";
		$getuserdest = $this->Common_model->selectQuery($select,TB_USERS,$cond,false,$join,$jointype);
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		// $data['form_data']=$_POST['test_data'];
		$this->session->set_userdata('userdest',$getuserdest[0]['street'].",".$getuserdest[0]['city_name'].",".$getuserdest[0]['state_name'].",".$getuserdest[0]['country_name']);
		$this->load->view('user/address',$data);
	}
	else
	{
		redirect("frontend-login"); exit;
	}
}

}