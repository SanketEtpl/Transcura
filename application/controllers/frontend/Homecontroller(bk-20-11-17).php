<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed'); 

class Homecontroller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_model');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		$this->load->helper('security');
		$this->load->helper('email_template_helper');
		$this->load->library('upload');		
	}

	public function index()
	{	
		// record fetch from db show for dropdownbox dynamically	
		$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);
		$data['states'] = $this->Common_model->select('*',TB_STATE);
		$data['desire_tran'] = $this->Common_model->select('*',TB_DESIRE_TRANSPORTATION);
		$data['hc_service'] = $this->Common_model->select('*',TB_HEALTHCARE_SERVICES);
		$data['sp_request'] = $this->Common_model->select('*',TB_SPECIAL_REQUEST);
		$data['testimonial'] = $this->Common_model->select('*',TB_TESTIMONIAL);		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
		$data['home_section1'] = $this->Common_model->selectWhereIn('page_id,sub_title,page_title,page_detail',TB_PAGES,'page_id',array(2,14,7));				
		//print_r($data['home_section1']);exit;
		$this->load->view('user/index',$data);		
	}

	// Homepage read more section
	public function homepage_section()
	{
		$whereCond = array('page_id'=>'2');
		$data['aboutData'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title",TB_PAGES,$whereCond);		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
		$this->load->view('user/homepage_section',$data);
	}

	// request for schedule ride
	public function schedule_ride()
	{			
		$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);
		$data['states'] = $this->Common_model->select('*',TB_STATE);	
		$data['desire_tran'] = $this->Common_model->select('*',TB_DESIRE_TRANSPORTATION);
		$data['hc_service'] = $this->Common_model->select('*',TB_HEALTHCARE_SERVICES);
		$data['sp_request'] = $this->Common_model->select('*',TB_SPECIAL_REQUEST);		
		$data['testimonial'] = $this->Common_model->select('*',TB_TESTIMONIAL);
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));	
		$data['home_section1'] = $this->Common_model->selectWhereIn('page_id,sub_title,page_title,page_detail',TB_PAGES,'page_id',array(2,14,7));				
		
		// form validation
		$this->form_validation->set_rules('date','date','trim|required|xss_clean');		
		$this->form_validation->set_rules('appointment_time', 'appointment time', 'trim|required|xss_clean');
		$this->form_validation->set_rules('pick_up_time','pick up time','trim|required|xss_clean|callback_check_time_valid['.$this->input->post('appointment_time').']');
		$this->form_validation->set_rules('pick_up_street', 'pick up address', 'trim|required|min_length[3]|max_length[100]|xss_clean');
		$this->form_validation->set_rules('state', 'state', 'trim|required|xss_clean');
		$this->form_validation->set_rules('country', 'country', 'trim|required|xss_clean');
		$this->form_validation->set_rules('zipcode','zipcode','trim|required|min_length[5]|integer|xss_clean');
		$this->form_validation->set_rules('health_service','health service','trim|required|xss_clean');
		$this->form_validation->set_rules('special_request', 'special request', 'trim|required|xss_clean');
		$this->form_validation->set_rules('special_instruction', 'special instruction', 'trim|required|alpha_numeric_spaces|min_length[3]|max_length[100]|xss_clean');
		$this->form_validation->set_rules('desire_transp', 'desired transportation','trim|required|xss_clean');		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('user/index',$data);
		}
		else
		{
			//get input data from user
			$postData = $this->input->post();		
			$insertArr  = array(
								'sr_date' => date("Y-m-d",strtotime($postData['date'])),
								'sr_pick_up_time' => $postData['pick_up_time'],
								'sr_appointment_time' => $postData['appointment_time'],	
								'sr_street' => trim($postData['pick_up_street']),
								'sr_country' => $postData['country'],
								'sr_state' => $postData['state'],
								'sr_zipcode' => trim($postData['zipcode']),
								//'sr_types_of_services' => $postData['health_service'],
								'sr_special_request' => $postData['special_request'],							
								'sr_special_instruction' => trim($postData['special_instruction']),
								'sr_desire_transportaion' => $postData['desire_transp'],								
								'user_id'=>1,
								'created_on'=>date("Y-m-d H:i:a")
							);

			// Record insert in schedule ride table
			$result = $this->Common_model->insert(TB_SCHEDULE_RIDE,$insertArr);
			if($result)
			{
				$this->session->set_flashdata("success","Schedule a ride inserted successfully");
				redirect(current_url());
			}
			else
			{
				$this->session->set_flashdata("fail","Schedule a ride not inserted successfully");				
				redirect(current_url());
			}
		}		
	}

	//check time validation
	function check_time_valid($pickup_time,$appointment_time)
	{		
		if($appointment_time <= $pickup_time)
		{			
			$this->form_validation->set_message('check_time_valid', 'The pick up time should not be greater than or equal appointment time.');
        	return false;  
		}
		else
		{
			return true;
		}
	}
	//get state name
	public function get_state()
	{		
		$postData = $this->input->post();
		$countryID=array('country_id'=>$postData['countryID']);
		$dataState = $this->Common_model->select("*",TB_STATE,$countryID);
		$stateJSON='';
		if($dataState > 0)
		{
			$stateJSON .= '<option value="">Select state</option>';
       		foreach ($dataState as $key => $state_name) {
       			$stateJSON.= '<option value="'.$state_name['id'].'">'.$state_name['state_name'].'</option>';
       		}       		
		}
		else
		{
			$stateJSON.= '<option value="">State not available</option>';
		}
		echo json_encode(array('state'=>$stateJSON));		
	}

	//get city name
	public function get_city()
	{
		$postData = $this->input->post();
		$stateID = array('state_id'=>$postData['state_id']);
		$dataCity = $this->Common_model->select("*",TB_CITY,$stateID);
		$cityJSON = '';
		if($dataCity > 0)
		{
			$cityJSON .= '<option value="">Select city</option>';
			foreach ($dataCity as $key => $city) {
				$cityJSON.= '<option value="'.$city['id'].'">'.$city['city_name'].'</option>';
			}
		}
		else
		{
			$cityJSON .= '<option value="">City not available</option>';
		}
		echo json_encode(array('city'=>$cityJSON));
	}

	// check email availability 
	public function check_email_availability()
	{
		$email = $this->input->post('email');
		$usertype = $this->input->post('usertype');
		$data = $this->Common_model->select('id,email,user_type',TB_USERS,array('email'=>$email,'user_type'=>$usertype));
		if($data)
		{
			$this->output
		    ->set_content_type("application/json")
		    ->set_output(json_encode(array('status'=>true,'message' => 'Email & Usertype already exist.')));		    
		}
		else
		{
			$this->output
		    ->set_content_type("application/json")
		    ->set_output(json_encode(array('status'=>false)));		    			    
		}			
	}

	// Call registration form
	public function registration()
	{
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);	
		$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);	
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));				
		$this->load->view('user/registration',$data);		
	}

	// submit register data in table
	public function form_registration()
	{	
		$this->load->helper('email_template_helper');
		
			$this->load->library('upload');
			$perDoc='';		
			$postData = $this->input->post();
			if(isset($_FILES["per_doc"]["name"]))   
			{   
		        $file_name = $_FILES["per_doc"]["name"];
		        $original_file_name = $file_name;
		        $random = rand(1, 10000000000000000);
		        $makeRandom = $random;
		        $file_name_rename = $makeRandom;
		        $explode = explode('.', $file_name);
		        if(count($explode) >= 2) 
		        {
					$new_file = $file_name_rename.'.'.$explode[1];
					$config['upload_path'] = './uploads/personaldoc/doc';
					$config['allowed_types'] ="png|jpeg|pdf|doc|xml|docx|PDF|DOC|XML|DOCX";
					$config['file_name'] = $new_file;
					$config['max_size'] = '307210';
					$config['max_width'] = '300000';
					$config['max_height'] = '300000';
					$config['overwrite'] = TRUE;
					$this->load->library('upload',$config);
					$this->upload->initialize($config);
					if(!$this->upload->do_upload("per_doc")) 
					{
						$error = $this->upload->display_errors();
						echo json_encode(array("status"=>"error","action"=>"update","msg"=>$error)); //exit;	
					}
					else
					{
						$perDoc = $new_file;   

					}
		        }
			}
		
						
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);	
		$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);	
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$token = "";						
		$token=  random_string('alnum',20);						
		$insertArr = array(
						'user_token' => trim($token),
						'user_type' => trim($postData['user_type']),								
						'full_name'  => trim($postData['full_name']),
						'date_of_birth' => date("Y-m-d",strtotime($postData['date_of_birth'])),
						'email'      => trim($postData['email']),
						'password'   => trim(md5($postData['password'])),								
						'token'      => $token,
						'username'	 =>	trim($postData['username']),					 
						'created_at' => date('Y-m-d H:i:s'),
						'status'     => "inactive",
						'phone'      => trim($postData['phone_no']),
						'done'		 => 1,
						'country'    => $postData['country'],
						'state'		 => $postData['state'],
						'city'		 => $postData['city'],
						'county'	 => $postData['county'],
						'street'	 => trim($postData['street']),
						'zipcode'	 => ($postData['zipcode']),
						'personal_doc' => $perDoc,
						'insurance_id' => trim($postData['insurance_id']),
						'emergency_contactname' => trim($postData['eme_name']),
						'emergency_contactno' => $postData['eme_number'],
						'signature'	=> $postData['signature']
					);

		// Record insert in user table
		$result = $this->Common_model->insert(TB_USERS,$insertArr);	
	
		if($result)
		{
			// Email to registration 
			$hostname = $this->config->item('hostname');
			$config['mailtype'] ='html';
			$config['charset'] ='iso-8859-1';
			$this->email->initialize($config);
			$from  = EMAIL_FROM; 
			$name = trim($postData['full_name']);
			$email = trim($postData['email']);
			$userType = $postData['user_type'];
			$this->messageBody  = email_header();
			$base_url = $this->config->item('base_url');
			$reset_url = $base_url."accountverify/".$token ;			
			$this->messageBody  .= '<tr>
					<td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
					    <p>Dear Customer,</p>
						<p>
							You are registered successfully on Transcura<br>
							Please click the link below to verify your email address.</p>
						<p>
						<p>Activation Link : </p>
							<a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href='.$reset_url.'>Click Here</a>
						</p><br /><br />	
						<p>Copy the Link : </p>
							'.$reset_url.'
						</p>					
					</td>
				</tr>'; 			
		    $this->messageBody  .= email_footer();
		    $this->email->from($from, $from);
		    $this->email->to($email);
		    $this->email->subject('Welcome to Transcura');
		    $this->email->message($this->messageBody);
		    $this->email->send();
		    $this->output
		    ->set_content_type("application/json")
		    ->set_output(json_encode(array('status'=>true,'message' => 'Your registration has been successfully, Please check your email for activation.', 'redirect'=>base_url('frontend-login'))));
		    $this->session->set_flashdata("auth_reg_success", "Your registration has been successfully, Please check your email for activation.");	   
		}
		else
		{
			$this->output
            ->set_content_type("application/json")
            ->set_output(json_encode(array('status'=>false, 'message'=>'Fail to registration, please try again.','redirect'=>base_url('registration'))));			
            $this->session->set_flashdata("auth_reg_fail", "Fail to registration, please try again.");	   
		}				
	}

	// Call reset password form
	public function reset_password()
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->load->view('user/reset_password',$data);		
	}

	// Set forgot password
	public function set_reset_password()
	{	
		$this->load->helper('email_template_helper');
		$email = $this->input->post('email');
		$userdata = $this->Common_model->select("`id`,`first_name`,`email`,`token`,`password`,`phone`,`Username`,`status`",TB_USERS,array("email"=>trim($email)));
		
		//check user is active or deactive
		if(empty($userdata))
		{
			$this->output
		    ->set_content_type("application/json")
		    ->set_output(json_encode(array('status'=>false,'message' => 'Your account does not exist.')));	
		}
		else if($userdata[0]['status']=='inactive')
		{	
			$this->output
		    ->set_content_type("application/json")
		    ->set_output(json_encode(array('status'=>false,'message' => 'Your account is deactive, please cantact to admin.')));
		}        
		else
		{			
			// Email send to the user for verification			
			$userId = base64_encode($userdata[0]['id']);
			$token = $userdata[0]['token'];
			$reset_token = random_string('unique');
			$reset_url = base_url()."reset-user-password/".$userId."/".$reset_token;	

			$datareset = array('token' =>$reset_token);
			$cond1 = array('id' =>$userId ); 
    		$result = $this->Common_model->update(TB_USERS,$cond1,$datareset); 

			$hostname = $this->config->item('hostname');
			$config['mailtype'] ='html';
			$config['charset'] ='iso-8859-1';
			$this->email->initialize($config);
			$from  = EMAIL_FROM; 
			$this->messageBody  = email_header();
			$this->messageBody  .= '<tr> 
		        <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
		            <p>Hello,</p>
		                <p>
		                A request has been made to forgot your password Transcura account password.<br>
		                Please click the link below to reset your password.</p>
		                <p>
		                <p>Activation Link : </p>
		                <a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href="'.$reset_url.'">Click Here</a>
		                </p>
		                <p>Copy link :</p>
		                '.$reset_url.'
		                <p>
		                If the link is not working properly, then copy and paste the link in your 
		                browser. <br>If you did not send this request, please ignore this email.</p>
		            </td>
		        </tr>
		        <tr>';                      
		    $this->messageBody  .= email_footer();
		    $this->email->from($from, $from);
		    $this->email->to($email);
		    $this->email->subject('Reset Password Link');
		    $this->email->message($this->messageBody);
		    if($this->email->send()) 
		    {
		        $this->output
			    	 ->set_content_type("application/json")
			    	 ->set_output(json_encode(array("status"=>true)));
		    }       
        	else
        	{
        		$this->output
        			 ->set_content_type
        			 ->set_output(json_encode(array("status"=>false,"message"=>"Fail send message, please try again")));
        	}
       	}
	}

	// Password reset by getting id and token
    public function resetPassword($id,$token)
    {
		$id = base64_decode($id);			
		$userdata = $this->Common_model->select("id,token,status",TB_USERS,array("id"=>trim($id),"token"=>trim($token),"status"=>"active"));
		$data['mydata']=array('token'=>$token,'id'=>$id);		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		//print_r($data);exit;	
    	if(count($userdata) > 0 )
		{
			// Clear token for one time user change password 
			$datareset = array('token' =>'');
			$cond1 = array('id' =>$id); 
    		$result = $this->Common_model->update(TB_USERS,$cond1,$datareset);    		
    		$this->load->view("user/change_password",$data);
		}
		else
		{
		  	$this->session->set_flashdata('forgot__error_msg', 'You reset password link has been expired, already use!');
			redirect("reset_password");
		}
	}
	// Call reset password form
	public function change_password()
	{		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->load->view('user/change_password',$data);	
	}

	// Update token
	function update_token()
	{
		$ftoken=$this->input->post('token');
		$fid = $this->input->post('id');
		$datareset1 = array('token' =>$ftoken);
		$cond1 = array('id' =>$fid); 
    	$result = $this->Common_model->update(TB_USERS,$cond1,$datareset1);
	}
	// Change user password 
    public function changePassword()
    {
		$ftoken=$this->input->post('token');
		$fid = $this->input->post('id');
	    $postData = $this->input->post();
	    $data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$userdata = $this->Common_model->select("`id`,`first_name`,`email`,`token`,`password`,`phone`,`Username`",TB_USERS,array("id"=>trim($postData['id']),"status"=>"active"));
	    
		// Form validation	    
		$this->form_validation->set_rules('newpassword', 'password', 'trim|required|min_length[6]');
		$this->form_validation->set_rules('confirm_password', 'password confirmation field', 'trim|required|matches[newpassword]');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$data['mydata']=array('id'=>$postData['id'],'token'=>$postData['token']);
		if ($this->form_validation->run() == FALSE)
		{	
			// If user fail to change password then previous token update for verification		
			$this->update_token();
			$this->load->view('user/change_password',$data);			
		}
		else
		{      	 
			// Check user get data is empty or not 	
			if (empty($userdata)) {
 				$this->update_token(); 				
 				$this->load->view('user/change_password',$data);
 			}
			else
			{
				// Check user id correct or not
				if(trim($userdata[0]['id']) == trim($postData['id']))
				{
					// Update token
					$generateToken=  random_string('alnum',20);
					$datareset = array('password' =>  md5($postData['newpassword']),'token' => $generateToken);
					$cond1 = array('id' =>  $postData['id']); 
					// Chnage password successfully
			    	$result = $this->Common_model->update(TB_USERS,$cond1,$datareset);
						
					if($result)
					{
						// success then redirect login page
						$this->session->set_flashdata('change_success_msg', 'Password change success,please login');
						redirect('frontend-login');
					}
					else
					{	
						$this->update_token();
						$this->session->set_flashdata('fail_change_msg', 'Error occured,Please try again.');
						$this->load->view('user/change_password',$data);		 				
					}
				}
				else
				{			
					$this->update_token();
					$this->session->set_flashdata('fail_change_msg', 'Please enter valid credential,Please try again.');								
					redirect('reset-user-password/'.base64_encode($fid).'/'.$ftoken,$data);
				}
			}					
		}
    }

	// Call login form 
	public function login()
	{
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);	
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));		
		$this->load->view('user/login',$data);	
	}	

	// User login
	public function user_login()
	{		
		$this->load->model('Login_model');
		// Login form validation				
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);	
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->form_validation->set_rules('username', 'username', 'required|valid_email');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('user_type', 'user-type', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
		if ($this->form_validation->run() == FALSE)
        {  
			$this->load->view('user/login',$data);			
		}
		else
		{		
			$user_login = array(
								'user_email' => trim($this->input->post('username')),
								'user_password'	=> md5($this->input->post('password')),
								'user_type'	=> $this->input->post('user_type')									
							);

			// Check user login credential are exit or not		
			$data = $this->Login_model->login_user($user_login['user_email'],$user_login['user_password'],$user_login['user_type']);						
			//print_r($data);exit;
			if($data['status'] == 'inactive')
			{
				$data['userData'] = $this->Common_model->select("*",TB_ROLE);
				$this->session->set_flashdata('log_error_msg', 'Your account is deactivate, please contact admin.');
				$this->load->view('user/login',$data);				
			}
			else
			{							
				if($data)
				{
					$sessionData = array('user_id' => $data['id'],'firstName' => $data['full_name'],'userType' => $data['user_type']);  
					$this->session->set_userdata($sessionData);
					redirect('my-dashboard');				
		   		}
				else
				{
					$data['userData'] = $this->Common_model->select("*",TB_ROLE);
					$this->session->set_flashdata('log_error_msg', 'Error occured,Invalid Data Try again.');
					$this->load->view('user/login',$data);					
				}
			}
		}	
	}

	// call about form
	public function about()
	{	
		$whereCond = array('page_id'=>'2');
		$data['aboutData'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title",TB_PAGES,$whereCond);		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));		
		$this->load->view('user/about',$data);		
	}

	// call about form
	public function services()
	{			
		$whereCond = array('page_id'=>'12');
		$data['servicesData'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title",TB_PAGES,$whereCond);
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->load->view('user/services',$data);		
	}

	//call contact form
	public function contact()
	{
		$whereCond = array('page_id'=>'13');
		$data['contactUs'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title",TB_PAGES,$whereCond);		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->load->view('user/contact_us',$data);		
	}

	//submit contact form data
	public function save_contact_us()
	{		
		//print_r($_FILES); //exit;
		$postData = $this->input->post();
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$whereCond = array('page_id'=>'13');
		$data['contactUs'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title",TB_PAGES,$whereCond);				
		$this->form_validation->set_rules('first_name', 'first name', 'trim|required|alpha|min_length[3]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('last_name', 'last name', 'trim|required|alpha|min_length[3]|max_length[20]|xss_clean');
		$this->form_validation->set_rules('email', 'email', 'trim|required|max_length[50]|valid_email|xss_clean');
		$this->form_validation->set_rules('phone', 'phone', 'trim|required|max_length[20]|min_length[10]|integer|xss_clean');
		$this->form_validation->set_rules('message', 'message', 'trim|required|xss_clean');		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
		if( $this->form_validation->run() == FALSE )
        {  			        	
			$this->load->view('user/contact_us',$data);				
		}
		else
		{ 
			$insertArr=array(								
				'cu_first_name' => trim($postData['first_name']),
				'cu_last_name' => trim($postData['last_name']),	
				'cu_email' => trim($postData['email']),
				'cu_phone' => trim($postData['phone']),
				'cu_message' => trim($postData['message'])								
			);			
			// Record insert in schedule ride table
			$result = $this->Common_model->insert(TB_CONTACT_US,$insertArr);
			if($result)
			{
				// Email send to the user for verification
				$email =trim($postData['email']) ;               
				$hostname = $this->config->item('hostname');
				$name = $postData['first_name'];
				$config['mailtype'] ='html';
				$config['charset'] ='iso-8859-1';
				$this->email->initialize($config);
				$from  = EMAIL_FROM; 
				$this->messageBody  = email_header();

				$this->messageBody  .= '<tr> 
			        <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
			            <p>Hello, '.$name.'</p>
			                <p>
			                	Thank you for contact us , as soon as possible we will contact you.
			                </p>
			            </td>
			        </tr>
			        <tr>';                      
			    $this->messageBody  .= email_footer();
			    $this->email->from($from, $from);
			    $this->email->to($email);
			    $this->email->subject('Thanks for contact us transcura');
			    $this->email->message($this->messageBody);
			    if($this->email->send()) 
			    {
			    	$this->session->set_flashdata('success', 'As soon as possible we will contact you');
				}       
	        	else
	        	{
	        	    $this->session->set_flashdata('fail', 'Error occured,Please try again.');
				}				
				redirect(current_url());
			}
			else
			{
				$this->session->set_flashdata("fail","Fail to inserted successfully");				
				redirect(current_url());
			}
		}
	}

	//call news form
	public function news()
	{		
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$this->load->view('user/news',$data);			
	}

	//call blogs form
	public function blogs()
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$whereCond = array('page_id'=>'11');
		$data['blogs'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title",TB_PAGES,$whereCond);		
		$this->load->view('user/blogs',$data);			
	}

	//call FAQs form
	public function faqs()
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));		
		$data['faqs'] = $this->Common_model->select("id,question,answer",TB_FAQ);
		$this->load->view('user/faqs',$data);		
	}

	//call terms and confidition form
	public function terms_and_conditions()
	{
		$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$whereCond = array('page_id'=>'3');
		$data['terms_and_conditions'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title",TB_PAGES,$whereCond);
		$this->load->view('user/terms_and_conditions',$data);		
	}

	//call privacy policy form
	public function privacy_policy()
	{	$data['footer'] = $this->Common_model->select('*',TB_SETTINGS,array('id'=>1));
		$whereCond = array('page_id'=>'9');
		$data['privacy_policy'] = $this->Common_model->select("page_title,page_detail,page_name,sub_title",TB_PAGES,$whereCond);
		$this->load->view('user/privacy_policy',$data);						
	}	
}


