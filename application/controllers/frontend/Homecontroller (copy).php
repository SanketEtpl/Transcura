<?php if( ! defined('BASEPATH') ) exit('No direct script access allowed'); 

class Homecontroller extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Common_model');
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
		$this->load->helper('security');
	}

	public function index()
	{		
		$this->load->view('user/header');
		// record fetch from db show for dropdownbox dynamically	
		$data['states'] = $this->Common_model->select('*',TB_STATE);
		$data['desire_tran'] = $this->Common_model->select('*',TB_DESIRE_TRANSPORTATION);
		$data['hc_service'] = $this->Common_model->select('*',TB_HEALTHCARE_SERVICES);
		$data['sp_request'] = $this->Common_model->select('*',TB_SPECIAL_REQUEST);		
		$this->load->view('user/index',$data);
		$this->load->view('user/footer');
	}
	// request for schedule ride
	public function schedule_ride()
	{		
		$this->load->view('user/header');			
		$data['states'] = $this->Common_model->select('*',TB_STATE);	
		$data['desire_tran'] = $this->Common_model->select('*',TB_DESIRE_TRANSPORTATION);
		$data['hc_service'] = $this->Common_model->select('*',TB_HEALTHCARE_SERVICES);
		$data['sp_request'] = $this->Common_model->select('*',TB_SPECIAL_REQUEST);
		//get data from user
		$postData = $this->input->post();
		// form validation
		$this->form_validation->set_rules('state', 'state', 'trim|required|xss_clean');
		$this->form_validation->set_rules('first_name', 'first name', 'trim|required|min_length[3]|max_length[20]|alpha|xss_clean');
		$this->form_validation->set_rules('last_name', 'last name', 'trim|required|min_length[3]|max_length[20]|alpha|xss_clean');				
		$this->form_validation->set_rules('time','time','trim|required|xss_clean');
		$this->form_validation->set_rules('date','date','trim|required|xss_clean');
		$this->form_validation->set_rules('pick_up_address', 'pick up address', 'trim|required|min_length[3]|alpha_numeric_spaces|xss_clean');
		$this->form_validation->set_rules('health_service','health service','trim|required|xss_clean');
		$this->form_validation->set_rules('special_request','special request','trim|required|xss_clean');
		$this->form_validation->set_rules('special_instruction', 'special instruction', 'trim|required|xss_clean|alpha|xss_clean');
		$this->form_validation->set_rules('desire_transp', 'desired transportation','trim|required|xss_clean');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('user/index',$data);
		}
		else
		{
			$insertArr  = array(
									'state' => $postData['state'],
									'first_name' => $postData['first_name'],
									'last_name' => $postData['last_name'],	
									'date_of_service' => date("Y-m-d",strtotime($postData['date'])),
									'pick_up_time' => $postData['time'],
									'pick_up_address' => $postData['pick_up_address'],
									'type_of_healthcare_service' => $postData['health_service'],
									'special_request' => $postData['special_request'],							
									'special_instruction' => $postData['special_instruction'],
									'desire_transportaion' => $postData['desire_transp']
								);
			// Record insert in schedule ride table
			$result = $this->Common_model->insert(TB_SCHEDULE_RIDE,$insertArr);
			if($result)
			{
				$this->session->set_flashdata("success","Record inserted successfully");
				redirect(current_url());
			}
			else
			{
				$this->session->set_flashdata("fail","Record not inserted successfully");				
				redirect(current_url());
			}
		}
		$this->load->view('user/footer');
	}
	//get state name
	public function get_state()
	{		
		$postData = $this->input->post();
		$countryID=array('country_id'=>$postData['countryID']);
		$dataState = $this->Common_model->select("*",TB_STATE,$countryID);
		$stateJSON='';
		if($dataState > 0)
		{
			$stateJSON .= '<option value="">Select state</option>';
       		foreach ($dataState as $key => $state_name) {
       			$stateJSON.= '<option value="'.$state_name['id'].'">'.$state_name['state_name'].'</option>';
       		}       		
		}
		else
		{
			$stateJSON.= '<option value="">State not available</option>';
		}
		echo json_encode(array('state'=>$stateJSON));		
	}

	//get city name
	public function get_city()
	{
		$postData = $this->input->post();
		$stateID = array('state_id'=>$postData['state_id']);
		$dataCity = $this->Common_model->select("*",TB_CITY,$stateID);
		$cityJSON = '';
		if($dataCity > 0)
		{
			$cityJSON .= '<option value="">Select city</option>';
			foreach ($dataCity as $key => $city) {
				$cityJSON.= '<option value="'.$city['id'].'">'.$city['city_name'].'</option>';
			}
		}
		else
		{
			$cityJSON .= '<option value="">City not available</option>';
		}
		echo json_encode(array('city'=>$cityJSON));
	}

	// Call registration form
	public function registration()
	{
		$this->load->view('user/header');
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);				
		$this->load->view('user/registration',$data);
		$this->load->view('user/footer');
	}

	public function registration_next_step()
	{
		$this->load->view('user/header');
		$this->load->view('user/registration_next_step');
		$this->load->view('user/footer');	
	}

	// User registration 
	public function user_registration()
	{
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);						
		//form validation
		$this->form_validation->set_rules('full_name', 'full name', 'trim|required|min_length[3]|max_length[30]|alpha|xss_clean');
		$this->form_validation->set_rules('user_type', 'user type', 'trim|required|xss_clean');
		//$this->form_validation->set_rules('phone_no', 'phone no', 'required|regex_match[/^[0-9]{10}$/]|integer');
		$this->form_validation->set_rules('email', 'email', 'required|valid_email|max_length[50]');
		$this->form_validation->set_rules('password', 'password', 'trim|required|min_length[3]|max_length[50]|xss_clean');
		$this->form_validation->set_rules('date_of_birth', 'date of birth', 'trim|required|xss_clean');
		$this->form_validation->set_rules('confirm_pwd', 'confirm password', 'trim|required|xss_clean|matches[password]');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');

		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('user/header');
			$this->load->view('user/registration',$data);
			$this->load->view('user/footer');
		}
		else
		{
			$data['firstFormData'] = $this->input->post();
			//call second form of registration			
			$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);			
			$this->load->view('user/header');
			$this->load->view('user/registration_next_step',$data);
			$this->load->view('user/footer');
		}
	}

	public function form_registration()
	{		
		$this->load->helper("email_template_helper");
		//form validation
		$this->form_validation->set_rules('country', 'country', 'trim|required|xss_clean');
		$this->form_validation->set_rules('state', 'state', 'trim|required|xss_clean');		
		$this->form_validation->set_rules('city', 'city', 'trim|required|xss_clean');
		$this->form_validation->set_rules('street', 'street', 'trim|required|min_length[5]|max_length[100]|alpha_numeric_spaces|xss_clean');
		$this->form_validation->set_rules('zipcode', 'zipcode', 'trim|required|min_length[5]|integer|xss_clean');
		$this->form_validation->set_rules('county', 'county', 'trim|required|xss_clean');
		$this->form_validation->set_rules('eme_name', 'emergency contact name', 'trim|min_length[5]|alpha|required|xss_clean');
		$this->form_validation->set_rules('eme_number', 'emergency contact number', 'trim|required|xss_clean|integer');
		$this->form_validation->set_rules('county', 'county', 'trim|required|xss_clean');		
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		$data['firstFormData'] = $this->input->post();
		$data['countryData'] = $this->Common_model->select("*",TB_COUNTRY);
		if($this->form_validation->run() == FALSE)
		{
			$this->load->view('user/header');
			$this->load->view('user/registration_next_step',$data);
			$this->load->view('user/footer');
		}
		else
		{
			//print_r($data);exit;
			$token = "";						
			$token=  random_string('alnum',20);
					
			$insertArr = array(
								'user_token' => $token,
								'first_name' => $firstFormData['password'],								
								'phone'      => $firstFormData['phone_no'],
								'email'      => $firstFormData['email'],
								'password'   => md5($firstFormData['password']),								
								'token'      => $token,							
								'created_at' => date('Y-m-d H:i:s'),
								'status'     => "inactive",
								'user_type'  => $firstFormData['user_type'],
								'done'		 => 1,
								'country'    => $firstFormData['country'],
								'state'		 => $firstFormData['state'],
								'city'		 => $firstFormData['city'],
								'county'	 => $firstFormData['county'],
								'zipcode'	 => $firstFormData['zipcode'],
								'zipcode'	 => $firstFormData['zipcode'],								
							);
				// Record insert in user table
				$result = $this->Common_model->insert(TB_USERS,$insertArr);
				if($result)
				{
					// Email to registration 
					$hostname = $this->config->item('hostname');
					$config['mailtype'] ='html';
					$config['charset'] ='iso-8859-1';
					$this->email->initialize($config);
					$from  = EMAIL_FROM; 
					$name = $postData['first_name'];
					$email = $postData['email'];
					$userType = $postData['user_type'];
					$this->messageBody  = email_header();
					$base_url = $this->config->item('base_url');
					$reset_url = $base_url."accountverify/".$token ;
					// Email for patient registration 
					if($userType=="Patient")
					{
						$this->messageBody  .= '<tr>
							<td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
							    <p>Dear Customer,</p>
								<p>
									You are registered successfully on Transcura as a patient.<br>
									Please click the link below to verify your email address.</p>
								<p>
								<p>Activation Link : </p>
									<a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href='.$reset_url.'>Click Here</a>
								</p>
								<p>
									If the link is not working properly, then copy and paste the link in your 
									browser. <br>If you did not send this request, please ignore this email.
								</p>
							</td>
						</tr>'; 
					}
					else
					{
					// Email for other registration 
					$this->messageBody  .= '<tr>
								<td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;height:200px;vertical-align:top;">
								    <p>Dear Customer,</p>
									<p>
									You are registered successfully on Transcura as a driver.<br>
									Please wait while administrator activates your account. </p>
							    </td>
							</tr>';
					}
				    $this->messageBody  .= email_footer();
				    $this->email->from($from, $from);
				    $this->email->to($email);
				    $this->email->subject('Welcome to Transcura');
				    $this->email->message($this->messageBody);
				    $this->email->send();
				    $this->session->set_flashdata("auth_reg", "Registration has been successfully");
				    redirect("frontend/login");
				}
				else
				{
					//$this->session->set_flashdata("auth_error", "Registration has been Fail");
					redirect("frontend//login");
				}	
		}
	}

	// Call reset password form
	public function reset_password()
	{
		$this->load->view('user/header');
		$this->load->view('user/reset_password');
		$this->load->view('user/footer');
	}

	// Set forgot password
	public function set_reset_password()
	{
		$this->load->helper('email_template_helper');
		$email = $this->input->post('email');
		$this->form_validation->set_rules('email', 'email', 'required|valid_email|max_length[50]');
		$this->form_validation->set_error_delimiters('<div class="error">','</div>');		
	    $userdata = $this->Common_model->select("`id`,`first_name`,`email`,`token`,`password`,`phone`,`Username`",TB_USERS,array("email"=>trim($email),"status"=>"active"));
	
		if(count($userdata)==0)
		{
			$this->session->set_flashdata('check_email_msg', 'This email does not exist or active,Please contact admin');
			//$this->load->view('user/reset_password');
			redirect("reset_password");			
		}        
		else
		{
			// Email send to the user for verification
			$password =$userdata[0]['password'];
			$userId = base64_encode($userdata[0]['id']);
			$token = $userdata[0]['token'];
			$reset_token = random_string('unique');
			$reset_url = base_url()."frontend/login/resetPassword/".$userId."/".$token;
			$useremail =trim($userdata[0]["email"]) ;               
			$hostname = $this->config->item('hostname');
			$config['mailtype'] ='html';
			$config['charset'] ='iso-8859-1';
			$this->email->initialize($config);
			$from  = EMAIL_FROM; 
			$this->messageBody  = email_header();
			$this->messageBody  .= '<tr> 
		        <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
		            <p>Hello,</p>
		                <p>
		                A request has been made to forgot your password Transcura account password.<br>
		                Please click the link below to reset your password.</p>
		                <p>
		                <p>Activation Link : </p>
		                <a style="background: #0091CA;color: #ffffff; font-size: 14px; padding: 8px 20px;text-decoration: none;" href="'.$reset_url.'">Click Here</a>
		                </p>
		                <p>
		                If the link is not working properly, then copy and paste the link in your 
		                browser. <br>If you did not send this request, please ignore this email.</p>
		            </td>
		        </tr>
		        <tr>';                      
		    $this->messageBody  .= email_footer();
		    $this->email->from($from, $from);
		    $this->email->to($email);
		    $this->email->subject('Reset Password Link');
		    $this->email->message($this->messageBody);
		    if($this->email->send()) 
		    {
		    	$this->session->set_flashdata('forgot_success_msg', 'Send reset password link your mail, please check it');
			    redirect("frontend/login");
		    }       
        	else
        	{
        	    $this->session->set_flashdata('forgot__error_msg', 'Error occured,Please try again.');
			    redirect("frontend/login");
        	}
       	}
	}

	// Call login form 
	public function login()
	{
		$this->load->view('user/header');
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);			
		$this->load->view('user/login',$data);
		$this->load->view('user/footer');
	}	

	// User login
	public function user_login()
	{		
		$this->load->model('Login_model');
		// Login form validation				
		$data['userData'] = $this->Common_model->select("*",TB_ROLE);	
		$this->form_validation->set_rules('username', 'username', 'required|valid_email');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('user_type', 'user-type', 'required');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
		if ($this->form_validation->run() == FALSE)
        {        	
			redirect('frontend/Homecontroller/login');
		}
		else
		{		
			$user_login = array(
									'user_email' => $this->input->post('username'),
									'user_password'	=> md5($this->input->post('password')),
									'user_type'	=> $this->input->post('user_type'),
									'status' => 'active'
								);					
			// Check user login credential are exit or not		
			$data = $this->Login_model->login_user($user_login['user_email'],$user_login['user_password'],$user_login['user_type'],$user_login['status']);						
			if($data)
			{
				$sessionData = array( 
										'user_id' => $data['id'], 
							            'firstName' => $data['first_name'], 
								        'lastName' => $data['last_name'],
								        'userType' => $data['user_type']
									);  
				$this->session->set_userdata($sessionData);
				redirect('homepage');				
	   		}
			else
			{
				$this->session->set_flashdata('log_error_msg', 'Error occured,Invalid Data Try again.');
				$this->load->view('user/header');
				$this->load->view('user/login',$data);
				$this->load->view('user/footer');
			}	
		}	
	}

	// call about form
	public function about()
	{		
		$this->load->view('user/header');
		$whereCond = array('page_name'=>'about_us','isDeleted'=>'1');
		$data['aboutData'] = $this->Common_model->select("page_title,page_detail,page_name,isDeleted",TB_PAGES,$whereCond);		
		$this->load->view('user/about',$data);
		$this->load->view('user/footer');
	}

	// call about form
	public function services()
	{		
		$this->load->view('user/header');
		$whereCond = array('page_name'=>'services','isDeleted'=>'1');
		$data['servicesData'] = $this->Common_model->select("page_title,page_detail,page_name,isDeleted",TB_PAGES,$whereCond);
		$this->load->view('user/services',$data);
		$this->load->view('user/footer');
	}

	//call contact form
	public function contact()
	{
		$this->load->view('user/header');		
		$this->load->view('user/contact_us');
		$this->load->view('user/footer');
	}

	//call news form
	public function news()
	{
		$this->load->view('user/header');		
		$this->load->view('user/news');
		$this->load->view('user/footer');	
	}

	//call blogs form
	public function blogs()
	{
		$this->load->view('user/header');		
		$this->load->view('user/blogs');
		$this->load->view('user/footer');		
	}

	//call FAQs form
	public function faqs()
	{
		$this->load->view('user/header');		
		$this->load->view('user/faqs');
		$this->load->view('user/footer');			
	}

	//call terms and confidition form
	public function terms_and_conditions()
	{
		$this->load->view('user/header');		
		$this->load->view('user/terms_and_conditions');
		$this->load->view('user/footer');				
	}

	//call privacy policy form
	public function privacy_policy()
	{
		$this->load->view('user/header');		
		$this->load->view('user/privacy_policy');
		$this->load->view('user/footer');					
	}
}


