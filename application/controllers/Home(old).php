<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form','url','html'));
		$this->load->database();
		$this->load->library("email");
		$this->load->model("common_model"); 
		$this->load->library("User_AuthSocial");	
		$this->load->library("email");
        $this->load->library("User_AuthSocial");
        $this->load->helper("email_template");
        $this->load->helper('string');
        $this->load->library("image_lib");
        $this->load->helper('security');
        $this->load->helper('string');
	}

	public function change_user_password()
	{		
		$this->load->view('frontend/user/change_password');		
	}

	public function accountverify($abc)
	{	   
		$userData = $this->common->select("*",TB_USERS,array("token"=>trim($abc)));  
		if ($userData)
		{
			$token=  random_string('alnum',20);	
			$cond1 = array('id' =>  $userData[0]['id']); 
			$datareset = array('status' =>  'active','token'=>$token); 
			$sucess= $this->common->update(TB_USERS,$cond1,$datareset);
			$this->session->set_flashdata('Your account has been activated successfully. You can now login', 'msg');
		    $data= $this->session->set_flashdata('forgot_success_msg', 'Your account has been activated successfully. You can now login');		redirect("frontend-login",$data);    
		}
		else
		{	
			$data = $this->session->set_flashdata('error', 'This link has been expired');	
			redirect("frontend-login",$data); 		
		}
	}

	function resetUserPassword($id)
	{
	    $postData = $this->input->post();
	    $cond1 = array('email' =>  trim($postData['uemail'])); 
	    $userData = $this->common->select("*",TB_USERS,$cond1 );  
        if($postData['email']=="" )
		{
			echo json_encode(array("status" => "error","message"=>"please Enter valid email"));  exit;
		}
        else if(count($userData)==0)
		{
			echo json_encode(array("status" => "error","message"=>"email id not found"));  exit;
		}
        else if($postData['password']=="" )
		{
			echo json_encode(array("status" => "error","message"=>"Please Enter valid password"));  exit;
		}
        else if($postData['confpwd']=="" )
		{
			echo json_encode(array("status" => "error","message"=>"Please Enter valid confirm password"));  exit;
		}
		else if($postData['password']!==  $postData['confirmPassword'])
		{
			echo json_encode(array("status" => "error","message"=>"Password and confirm password not match"));  exit;
		}
        if($postData['key']=="" )
		{
			echo json_encode(array("status" => "error","message"=>"Expire token for reset password"));  exit;
		}

                     //  $data['messagereset'] = $this->session->flashdata('messagereset'); 
			$data['title']  = 'Reset Password';
			$data['meta_description'] = '';
			$data['meta_keyword'] = '';
			$cond1 = array('email' =>  trim($postData['email']) ,'token'=> trim($postData['key']) ,"status"=>"active"); 
                        $userData = $this->common->select("*",TB_USERS,$cond1 );
			// echo $this->db->last_query();die;    
			//   print_r($userData) ;die; 
			if($userData){
		
				$key  =$postData['key']; 
				$useremail    = trim($this->input->post('uemail'));
				$password = md5($this->input->post('password'));
				$datareset = array('password' => $password);

				$cond = array('email' => $useremail, 'token' => trim($postData['key']));				
				$userData = $this->common->select("*",TB_USERS,$cond);
		     
		       		  
				if(count($userData) > 0)
				{	  
					$this->common->update(TB_USERS,$cond,$datareset);	
				
					$cond = array('email' => $useremail,'password' => $password);			       
					 $res = $this->common->select("email,password",TB_USERS,$cond);  
 
				if ( $res !== false ) {
						
						 $cond1 = array('email' => $useremail);
						 $datareset = array('token' => '');
	                               $this->common->update(TB_USERS,$cond,$datareset); 		
				//echo $this->db->last_query();die; 	
				
                   echo json_encode(array("status" => "sucess","message"=>"You have successfully reset the password."));
                     $this->load->view('frontend/reset_password', $data);exit;
				} 

				else{ 
	
				$data['validation_message'] = "The key is invalid or You have already reset the password.";
				echo json_encode(array("status" => "error","message"=>"The key is invalid or You have already reset the password.","path"=>"home"));  
				
				} 

				
            }
            else{
              //  $data['validation_message'] = (validation_errors() ? validation_errors() : $this->session->set_flashdata('message'));
                $this->load->view('frontend/change_password', $data);
            }
        }
        
    }
    
 

// public function emailverify($data)
// 	{
		

		
		
		
// 	}



    public function admin_forgot_password()
    {
    	//redirect("login");
    	$this->load->view('forgot_password');
    }

    public function forgetPassword()
	{
		$this->load->library('form_validation');
		$postData = $this->input->post();
		$this->form_validation->set_rules('username', 'Email', 'trim|required|max_length[50]|valid_email|xss_clean',array('valid_email'=>'Please enter a valid email address'));
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');	
		if( $this->form_validation->run() == FALSE )
	    {  			        	
			$this->load->view('forgot_password');				
		}
		else
		{ 
		$result = $this->common->select("*",TB_ADMIN,array("username"=>trim($postData["username"])));
		//print_r($result);exit;
		$this->load->library('encrypt');
		
		if(count($result)>0)
		{
			$decrypted_string = $this->encrypt->decode($result[0]['password']);
			$hostname = $this->config->item('hostname');
			$config['mailtype'] ='html';
			$config['charset'] ='iso-8859-1';
			$this->email->initialize($config);
			$from  = EMAIL_FROM; 
			$email = $postData['username'];
			$this->messageBody  = email_header();
			$base_url = $this->config->item('base_url');           
	            $this->messageBody  .= '<tr>
	                <td style="word-wrap:break-all;padding:10px 20px;border:1px solid #dfdfdf;border-top:0;">
	                    <p>Dear Admin,</p>
	                        <p>
	                        You password has '.$decrypted_string.'</p>
	                        <p>
	                        </p><p>If the above link does not work, please copy and paste the following link into your browser.</p>
	                    </td>
	                </tr>
	                <tr>';                      
	            $this->messageBody  .= email_footer();
	            $this->email->from($from, $from);
	            $this->email->to($email);
	            $this->email->subject('Admin Password');
	            $this->email->message($this->messageBody);
	            $this->email->send();
	            if($this->email->send()) 
			    {
			    	$this->session->set_flashdata('success', 'Your password has been sent to your email. please check your email.');
			    	$this->load->view('forgot_password');
				}       
	        	else
	        	{
	        	    $this->session->set_flashdata('fail', 'Error occured,Please try again.');
	        	    $this->load->view('forgot_password');
				}	
		  // echo json_encode(array("status" => "success" ,"message"=>"Your password has been sent to your email. please check your email.")); exit;
		}
		else
		{
			//echo json_encode(array("status" => "error","message"=>"Please provide valid login credential.	")); exit;
			$this->session->set_flashdata('fail', 'Please enter valid credential.');
			$this->load->view('forgot_password');
		}
	}
}

    public function logout(){
       $this->session->sess_destroy();
        redirect("home"); exit;
    }





}
