<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Subscription Plans
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> Subscription Plans</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="listPlans(0,'user','DESC');">
                      Search
                    </a>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getPlan(this);">
                      Add New
                    </a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listPlans" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="25%" data-name="plan_name" data-order="DESC" class="sorting">Title</th>
                        
                        <th width="15%" data-name="plan_price" data-order="ASC" >Price</th>
                        <th width="15%" data-name="days" data-order="ASC" >Days limit</th>
                        <th width="15%" >Activation</th>
                        <th width="10%" class="">Action</th>
                      </tr>
                    </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="userModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>admin/plans/savePlan" id="userForm" name="userForm" class="user_validation">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Manage Plan</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="first_name">Name</label>
                  <input type="hidden" id="user_key" name="user_key">
                  <input type="text" placeholder="Plan Title" id="plan_title" required="true" name="plan_title" class="form-control" autocomplete="off">
                </div>
              </div>
                         
             <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="contact_number">Price</label>
                  <input type="text" placeholder="Plan price"  required="true" id="plan_price"  filter="decimal" name="plan_price" class="form-control">
                </div>
              </div>
           
             
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="contact_number">Days of plan</label>
                  <input type="text" placeholder="Days of plan" filter="number"  required="true"  id="days" name="days" class="form-control">
                </div>
              </div>
            
            </div>
            
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/plans.js"></script>
<?php $this->load->view('footer'); ?>
