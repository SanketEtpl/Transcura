<?php $this->load->view('header'); ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
     Driver Listing
   </h1>
   <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Driver Listing</li>
  </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-body">
          <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
            <div class="row">
              <div class="col-sm-6">
                <div class="dataTables_length" id="example1_length">
                </div>
              </div>
              <div class="col-sm-6 resp_share">
                <div id="example1_filter" class="dataTables_filter">
                  <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                  <a class="btn btn-primary" href="javascript:void(0);"  onclick="listDrivers(0,'user','DESC');">
                    Search
                  </a>
                  <!-- <a class="btn btn-primary" href="javascript:void(0);" onclick="getDriver(this);">
                    Add New
                  </a> -->
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
               <div class="table-responsive">	
                <table class="table table-bordered table-striped dataTable" id="listDrivers" role="grid" aria-describedby="example1_info">
                  <thead>
                    <tr role="row">
                      <th width="1%">SrNo.</th>
                      <th width="25%" data-name="fname" data-order="DESC" class="sorting">Name</th>
                      <th width="25%" data-name="email" data-order="ASC" class="sorting">Email</th>
                      <th width="10%" data-name="phone" data-order="ASC" class="sorting">Phone</th>
                      <th width="15%" data-name="phone" data-order="ASC" class="sorting">Category</th>
                      <th width="25%">Activation</th>
                      <th width="5%">Action</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </div>	
            </div>
          </div>
          <div class="row">
            <div class="col-sm-5">
              <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite" class="valida"></div>
            </div>
            <div class="col-sm-7">
              <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</section>
</div>
<div id="userModal" class="modal fade" style="display: none;" aria-hidden="false">
  <form method="post" action="<?php echo base_url(); ?>admin/Drivers/saveDriver" id="userForm" name="userForm" class="user_validation" enctype="multipart/form-data"  valida>
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
          <h4 class="modal-title">Manage drivers/ trasportation provider
          </h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div id="first_form">
              <div id="error_message"></div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="full_name1">Full name *</label>
                  <input type="hidden" id="user_key" name="user_key">
                  <input type="text" placeholder="full name" id="full_name" maxlength="50" name="full_name" filter="name"  required="true" class="form-control fullname" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group" id="dateissue">
                  <label class="control-label" for="date_of_birth">Date of birth *</label>
                  <input type="text" placeholder="Date of birth" id="driverDate" required="true" name="birth" class="form-control">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="lu">Username *</label>                  
                  <input type="text" placeholder="Username" id="username" filter="username" required="true" name="username" class="form-control" maxlength="50" autocomplete="off" readonly="">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="lemail_address">Email address *</label>
                  <input type="text" placeholder="Email address" id="email" maxlength="50" name="email_address" required="true" filter="email" class="form-control" autocomplete="off" readonly="">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="lphone">Phone *</label>
                  <input type="text" placeholder="Phone" id="phone" name="phone" class="form-control" maxlength="20" filter="phone" required="true">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="lcountry" class="control-label">Country *</label>
                  <br />
                  <input type="hidden" name="country" value="231">
                  United States
                  <br /><br />
                    <!-- <select class="form-control" name="country" id="country" required="true">
                    <option value="" class="selecttxt"  >Select country</option> -->
                    <?php 
                      // if(!empty($countryData)){
                      // foreach($countryData as $country_name) {
                    ?>
                     <!--  <option value="<?php //echo $country_name['id']; ?>"><?php //echo $country_name['country_name']?></option>
                      <?php //} } else { ?> 
                      <option value=""></option> -->
                      <?php //} ?>  
                      <!-- </select>              -->
                    </div>  
                  </div>
                  <div class="col-md-6">
                    <div class="form-group"> 
                      <label for="lstate" class="control-label">State *</label>
                      <select class="form-control" id="state" name="state" required="true">
                        <option class="selecttxt" value="" >Select state</option>
                      </select>                 
                    </div>
                  </div>
                  <div class="col-md-6">             
                    <div class="form-group">
                      <label for="lcity" class="control-label">City *</label>
                      <select class="form-control" id="city" name="city" required="true">
                        <option class="selecttxt" value="">Select city</option>
                      </select>                   
                    </div>    
                  </div>

                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label" for="lstreet">Street *</label>
                      <textarea placeholder="Street" id="address" rows="5" name="address" class="form-control street" autocomplete="off" filter="street" required="true" maxlength="255"></textarea>
                    </div>
                  </div>

                  <div class="col-md-6">  
                    <div class="form-group">
                      <label for="lzipcode" class="control-label">Zip code *</label>
                      <input type="text" name="zipcode" class="form-control" id="zipcode" placeholder="Zip code" maxlength="15" autocomplete="off" filter="zipcode" required="true">
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <div class="col-md-6">
                    <div class="form-group" id="imgChange">
                      <label class="control-label" for="subTitle"><span>Add new image</span></label>
                      <input type="file" placeholder="Profile image" id="pimage" name="pimage"  size="20" class="form-control" autocomplete="off" onchange="document.getElementById('pimage').src = window.URL.createObjectURL(this.files[0])"> 
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group lst_mem" id="theDiv"></div>
                  </div>
                  <div class="col-md-12 next_btn">
                    <input type="hidden" id="oldimg" name="oldimg">
                    <button class="btn btn-default" type="button" name="next" id="next">Next</button>                  
                  </div> 
                </div>
                <div id="second_form">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="lcounty" class="control-label">County *</label>
                      <input type="text" name="county" class="form-control" id="county" placeholder="County" maxlength="50" autocomplete="off" filter="county" required="true">
                    </div>
                  </div>                                         
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label" for="linsuid">Insurance id *</label>                    
                      <input type="text" class="form-control" name="insurance_id" id="insurance_id" placeholder="Insurance id" maxlength="20" autocomplete="off" filter="insuranceid" required="true">                
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group"> 
                      <label for="lper_doc" class="control-label">Personal Documents</label>
                      <select class="form-control" id="pers_doc" name="pers_doc" required="true">
                        <option class="selecttxt" value="" disable selected hidden>Select</option>
                        <option value="1">YES</option>
                        <option value="2">NO</option>
                      </select>                 
                    </div>
                  </div>
                  <div id="persoal_doc_view">
                    <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label" for="lssc"><span>Social security card</span></label>  
                        <input type="file" id="socSecrtCd" name="socSecrtCd"  size="20" class="form-control" autocomplete="off"> 
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group" id="dl_doc">
                        <label class="control-label" for="ldl"><span>Driver license</span></label>  
                        <input type="file" id="drivrLicDoc" name="drivrLicDoc"  class="form-control" autocomplete="off"> 
                      </div>
                    </div>

                    <div class="col-md-6" id="DrvLiceED">
                      <div class="form-group">
                        <label class="control-label" for="ldled">Driver license expire date</label>
                        <input type="text" placeholder="Driver license expire date" id="dldedShow" name="drivrLicED" class="form-control expireDate" readonly>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group" id="pdtr_doc">
                        <label class="control-label" for="lpdtr"><span>5 panel drug test results</span></label>  
                        <input type="file" id="panelDrgTstResltDoc" name="panelDrgTstResltDoc" class="form-control" autocomplete="off"> 
                      </div>
                    </div>

                    <div class="col-md-6" id="panDrugTtED">
                      <div class="form-group">
                        <label class="control-label" for="lldled">5 panel drug test results expire date</label>
                        <input type="text" placeholder="5 panel drug test results expire date" id="pdtredShow" name="panelDrugTsED" class="form-control expireDate" readonly>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group" id="criBG_doc">
                        <label class="control-label" for="llpdtr"><span>Criminal background</span></label>  
                        <input type="file" id="crimnlBGDoc" name="crimnlBGDoc" class="form-control" autocomplete="off"> 
                      </div>
                    </div>

                    <div class="col-md-6" id="crimBGED">
                      <div class="form-group">
                        <label class="control-label" for="lcb">Criminal background expire date </label>
                        <input type="text" placeholder="Criminal background expire date" id="cbedShow" name="crimnlBGED" class="form-control expireDate" readonly>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group" id="motrVehRec_doc">
                        <label class="control-label" for="lmvr"><span>Motor vehicle record</span></label>  
                        <input type="file" id="MotrVehRecDoc" name="MotrVehRecDoc" class="form-control"  autocomplete="off"> 
                      </div>
                    </div>

                    <div class="col-md-6" id="motVehRecED">
                      <div class="form-group">
                        <label class="control-label" for="llcb">Motor vehicle record expire date </label>
                        <input type="text" placeholder="Motor vehicle record expire date" id="mvredShow" name="motrVehRecED" class="form-control expireDate" readonly>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group" id="clearance_doc">
                        <label class="control-label" for="lac"><span>Act 33 & 34 clearance</span></label>  
                        <input type="file" id="clearanceDoc" name="clearanceDoc" class="form-control" autocomplete="off"> 
                      </div>
                    </div>

                    <div class="col-md-6" id="cleaED">
                      <div class="form-group">
                        <label class="control-label" for="lllcb">Act 33 & 34 clearance expire date </label>
                        <input type="text" placeholder="Act 33 & 34 clearance expire date" id="acedShow" name="clearanceED" class="form-control expireDate" readonly>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <div class="form-group"> 
                      <label for="lvr" class="control-label">Vehicle registration </label>
                      <select class="form-control" id="vehRegDropdown" name="vehRegDropdown" required="true">
                        <option class="selecttxt" value="" disable selected hidden>Select</option>
                        <option value="1">YES</option>
                        <option value="2">NO</option>
                      </select>                 
                    </div>
                  </div>

                  <div id="vehicle_reg_veiw">
                    <div class="col-md-6">
                      <div class="form-group" id="veh_insp_doc">
                        <label class="control-label" for="lvi1"><span>Vehicle Inspections</span></label>  
                        <input type="file" id="vehInspDoc" name="vehInspDoc" class="form-control" autocomplete="off"> 
                      </div>
                    </div>
                    <div class="col-md-6" id="vehiInspED">
                      <div class="form-group">
                        <label class="control-label" for="lvied">Vehicle Inspections expire date</label>
                        <input type="text" placeholder="Vehicle Inspections expire date" id="viedShow" name="vehInspeED" class="form-control expireDate" readonly>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group" id="veh_maintn_doc">
                        <label class="control-label" for="lvi2"><span>Vehicle maintenance records</span></label>  
                        <input type="file" id="vehMaintRecDoc" name="vehMaintRecDoc" class="form-control" autocomplete="off"> 
                      </div>
                    </div>
                    <div class="col-md-6" id="vehMaintRecED">
                      <div class="form-group">
                        <label class="control-label" for="lvmred">Vehicle maintenance records expire date</label>
                        <input type="text" placeholder="Vehicle maintenance records expire date" id="vmredShow" name="vehMaintRecED" class="form-control expireDate" readonly>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group" id="veh_maintn_doc">
                        <label class="control-label" for="lvi3"><span>Vehicle insurance</span></label>  
                        <input type="file" id="vehInsrnDoc" name="vehInsrnDoc" class="form-control" autocomplete="off"> 
                      </div>
                    </div>
                    <div class="col-md-6" id="vehInsurnED">
                      <div class="form-group">
                        <label class="control-label" for="lvired4">Vehicle insurance expire date</label>
                        <input type="text" placeholder="Vehicle insurance expire date" name="vehInsrnED" id="viedShow2" class="form-control expireDate" readonly>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group" id="veh_maintn_doc">
                        <label class="control-label" for="lvi5"><span>Vehicle registration</span></label>  
                        <input type="file" id="vehRegDoc" name="vehRegDoc" class="form-control" autocomplete="off"> 
                      </div>
                    </div>
                    <div class="col-md-6" id="vehRegED">
                      <div class="form-group">
                        <label class="control-label" for="lvired6">Vehicle registration expire date</label>
                        <input type="text" placeholder="Vehicle registration expire date" name="vehRegED" id="vredShow" class="form-control expireDate" readonly>
                      </div>
                    </div>
                  </div> 
                  <div class="col-md-6">
                    <div class="form-group"> 
                      <label for="lbl_ovb" class="control-label">Do you operate your own vehicle for your own business?</label>
                      <select class="form-control" id="own_vehicle_dd" name="own_vehicle_dd">
                        <option class="selecttxt" value="" disable selected hidden>Select</option>
                        <option value="1">YES</option>
                        <option value="2">NO</option>
                      </select>                 
                    </div>
                  </div>
                  <div id="own_bussiness_show">
                    <div class="col-md-6">
                      <div class="form-group"> 
                        <label for="lbl_type_of_sevices" class="control-label">Types of service for own bussiness</label>
                        <select class="form-control" id="type_of_sevices" name="type_of_sevices">
                          <option class="selecttxt" value="" disable selected hidden>Select</option>
                          <?php 
                          if(!empty($typeOfServices)){
                            foreach($typeOfServices as $services) {
                              ?>
                              <option value="<?php echo $services['type_id']; ?>"><?php echo $services['type_title']?></option>
                              <?php } } else { ?> 
                              <option value=""></option>
                              <?php } ?>
                            </select>                 
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label" for="ownvrlable"><span>Vehicle registration</span></label>  
                            <input type="file" id="own_vehicle_reg_doc" name="own_vehicle_reg_doc" class="form-control" autocomplete="off"> 
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label" for="lbl_insu_pol">Insurance policy</label>
                            <input type="file" id="insurance_policy_doc" name="insurance_policy_doc" class="form-control">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label" for="lbl_inspection">Inspection</label>  
                            <input type="file" id="inspection_doc" name="inspection_doc" class="form-control" autocomplete="off"> 
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label" for="lbl_tax_id">Tax ID or EIN number</label>
                            <input type="file" id="tax_id_or_ein_no" name="tax_id_or_ein_no" class="form-control">
                          </div>
                        </div>

                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label" for="lbl_own_bs_vehicle_details"><span>Vehicle type, year, make and model</span></label>  
                            <input type="text" placeholder="Vehicle type, year, make and model" id="own_buss_vehicle_details" name="own_buss_vehicle_details" class="form-control" autocomplete="off"> 
                          </div>
                        </div>                                    
                      </div> 
                      <div class="col-md-6">
                        <div class="form-group"> 
                          <label for="lbl_comp_vehic" class="control-label">Do you operate company vehicle?</label>
                          <select class="form-control" id="company_vehicle_dropdown" name="company_vehicle_dropdown">
                            <option class="selecttxt" value="" disable selected hidden>Select</option>
                            <option value="1">YES</option>
                            <option value="2">NO</option>
                          </select>                 
                        </div>
                      </div>
                      <div id="company_vehicle_show">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label" for="lbl_trans_comp1">Name of transportation company</label>  
                            <input type="text" placeholder="Name of transportation company" id="transport_comp" name="transport_comp" class="form-control" autocomplete="off"> 
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="control-label" for="lbl_vehicle_type">Vehicle type, year, make and model</label>  
                            <input type="text" placeholder="Vehicle type, year, make and model" id="vehicle_details" name="vehicle_details" class="form-control" autocomplete="off"> 
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group"> 
                            <label for="lbl_com_type_of_sevices" class="control-label">Types of service for company vehicle</label>
                            <select class="form-control" id="com_veh_type_of_sevices" name="com_veh_type_of_sevices">
                              <option class="selecttxt" value="" disable selected hidden>Select</option>
                              <?php 
                              if(!empty($typeOfServices)){
                                foreach($typeOfServices as $services) {
                                  ?>
                                  <option value="<?php echo $services['type_id']; ?>"><?php echo $services['type_title']?></option>
                                  <?php } } else { ?> 
                                  <option value=""></option>
                                  <?php } ?>
                                </select>                 
                              </div>
                            </div>
                          </div> 

                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label" for="emer_cont_name">Emergency contact name *</label>
                              <!-- <input type="hidden" id="user_key" name="user_key"> -->
                              <input type="text" placeholder="Emergency contact name" id="emer_contc_name" maxlength="50" name="emer_contc_name" filter="name"  required="true" class="form-control fullname" autocomplete="off">
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group">
                              <label class="control-label" for="emer_cont_number">Emergency contact number *</label>
                              <input type="text" placeholder="Emergency contact number" id="econtact_no" name="emer_contc_number" class="form-control" maxlength="20" filter="phone" required="true">
                            </div>
                          </div>

                          <div class="col-md-6">
                            <div class="form-group"> 
                              <label for="lsign" class="control-label">Are you able to provide signatures *</label>
                              <select class="form-control" id="signature" name="signature" required="true">
                                <option class="selecttxt" value="" disable selected hidden>Select signature</option>
                                <option value="1">YES</option>
                                <option value="2">NO</option>
                              </select>                 
                            </div>
                          </div>

                          <div class="col-md-12" >
                            <!-- <button class="btn btn-default" name="back" id="back" type="button">Back</button> -->
                            <!-- <button data-dismiss="modal" class="btn btn-default" type="reset">Close</button> -->
                            <!--<button class="btn btn-info tiny" name="btn_submit" id="btn_submit" type="button">Save</button>-->
                            <button class="btn btn-info tiny" id="sumbitBtn" style="float:left;" type="submit">Save</button>
                          </div>
                          <div class="col-md-12 next_btn">
                            <button class="btn btn-default"  style="float:right;" type="button" name="back" id="back">Back</button>                  
                          </div>


                        </div>  
                      </div>
                    </div>        
                  </div>
                </form>
              </div>

            </div>


            <div id="driverDocModal" class="modal fade" style="display: none;" aria-hidden="false">   
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                    <h4 class="modal-title">Driver Documents
                    </h4>
                  </div>
                  <div class="modal-body">
                    <div class="row">
                      <div class="driver_doc"></div>
                    </div>
                  </div>        
                </div>
              </div>
            </div>

            <script src="<?php echo base_url(); ?>js/admin/drivers.js"></script>
            <?php $this->load->view('footer'); ?>
