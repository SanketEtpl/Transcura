<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Service Type
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> Service Type</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6 resp_share">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="listService_type(0,'user','DESC');">
                      Search
                    </a>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getService_type(this);"  >
                      Add New
                    </a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
				 <div class="table-responsive">	
					 <table class="table table-bordered table-striped dataTable" id="listService_type" role="grid" aria-describedby="example1_info">
						<thead>
						  <tr role="row">
							<th width="60%" data-name="cat_title" data-order="DESC" class="sorting">Service Types</th>
							<th width="20%" class="">Images</th>
							<th width="20%" class="">Action</th>
						  </tr>
						</thead>
						<tbody>                        
						</tbody>
						</table>
					</div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="userModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post"  enctype="multipart/form-data" action="<?php echo  base_url(); ?>admin/Service_types/saveService_type" id="userForm" name="userForm" class="valida">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Manage Service type</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="category_title">Service Title</label>
                  <input type="hidden" id="user_key" name="user_key">
                      <input type="hidden" id="oldimg" name="oldimg">
                  <input type="text" placeholder="Title" id="type_title" required="true" name="type_title" class="form-control" autocomplete="off">
                </div>
              </div>
           </div> 
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="subTitle">Image</label>                  
                  <input type="file" placeholder="Image" id="image" name="userfile"  size="20" class="form-control" autocomplete="off" onchange="document.getElementById('blah').src = window.URL.createObjectURL(this.files[0])">                 
                </div>
              </div>
              <div id="theDiv">
         
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/service_type.js"></script>
<?php $this->load->view('footer'); ?>
