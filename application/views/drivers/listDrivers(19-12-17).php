<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
       Driver/Transportation provider 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Driver/Transportation provider</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6 resp_share">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);"  id="search" disabled="true" onclick="listDrivers(0,'user','DESC');">
                      Search
                    </a>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getDriver(this);">
                      Add New
                    </a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
				   <div class="table-responsive">	
                  <table class="table table-bordered table-striped dataTable" id="listDrivers" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="25%" data-name="fname" data-order="DESC" class="sorting">Name</th>                       
                        <th width="25%" data-name="email" data-order="ASC" class="sorting">Email</th>
                        <th width="15%" data-name="phone" data-order="ASC" class="sorting">Phone</th>                       
                        <th width="15%" >Activation</th>
                        <th width="10%" class="">Action</th>
                      </tr>
                    </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
				   </div>	
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite" class="valida"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="userModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>admin/Drivers/saveDriver" id="userForm" name="userForm" class="user_validation" enctype="multipart/form-data"  valida>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Manage Drivers/ Trasportation Provider
            </h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div id="first_form">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="full_name">Full Name *</label>
                  <input type="hidden" id="user_key" name="user_key">
                  <input type="text" placeholder="full Name" onkeypress="keypresstext('full_name')" id="full_name" maxlength="50" name="full_name" class="form-control" autocomplete="off">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="date_of_birth">Date of birth *</label>
                  <input type="text" placeholder="click to show datepicker" filter="date" onchange="keypresstext('example1')" id="example1" name="birth" class="form-control" >
                </div>
              </div>
              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="email_address">Email-Id *</label>
                  <input type="text" placeholder="Email-Id" id="email" maxlength="50" name="email_address" onkeypress="keypresstext('email')" class="form-control" autocomplete="off">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="contact_number">Contact Number *</label>
                  <input type="text" placeholder="Contact Number" onkeypress="keypresstext('phone')" id="phone" name="phone" class="form-control" min="10" maxlength="20">
                </div>
              </div>             

              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="address">Address *</label>
                  <textarea placeholder="Address" onkeypress="keypresstext('address')" id="address" name="address" class="form-control" autocomplete="off"></textarea>
                </div>
              </div>
            
              <div class="col-md-6">
                <div class="form-group">
                  <label for="country" class="text-uppercase">Country *</label>
                  <select class="form-control" name="country" id="country" onchange="keypresstext('country')">
                    <option value="" class="selecttxt"  >Select Country</option>
                    <?php 
                    if(!empty($countryData)){
                    foreach($countryData as $country_name) {
                    ?>
                    <option value="<?php echo $country_name['id']; ?>"><?php echo $country_name['country_name']?></option>
                    <?php } } else { ?> 
                    <option value=""></option>
                    <?php } ?>  
                  </select>             
                </div>  
              </div>
              <div class="col-md-6">
                <div class="form-group"> 
                  <label for="state" class="text-uppercase">State *</label>
                  <select class="form-control" id="state" name="state" onchange="keypresstext('state')">
                    <option class="selecttxt" value="" >Select State</option>
                  </select>                 
                </div>
              </div>
              <div class="col-md-6">             
                <div class="form-group">
                  <label for="city" class="text-uppercase">City *</label>
                  <select class="form-control" id="city" name="city" onchange="keypresstext('city')">
                    <option class="selecttxt" value="">Select City</option>
                  </select>                   
                </div>    
              </div>
              <div class="col-md-6">  
                <div class="form-group">
                  <label for="street" class="text-uppercase">Street *</label>
                  <input type="text" name="street" class="form-control" id="street" onkeypress="keypresstext('street')" placeholder="Street" maxlength="255" autocomplete="off">
                </div>      
              </div>
              <div class="col-md-6">  
                <div class="form-group">
                  <label for="zipcode" class="text-uppercase">Zip Code *</label>
                  <input type="text" name="zipcode" class="form-control" onkeypress="keypresstext('zipcode')" id="zipcode" placeholder="Zip Code" maxlength="6" autocomplete="off">
                </div>
              </div>  
              <div class="col-md-12">
                <div class="form-group">
                  <label for="county" class="text-uppercase">County *</label>
                  <input type="text" name="county" class="form-control" id="county" onkeypress="keypresstext('county')" placeholder="County" maxlength="5" autocomplete="off">
                </div>
              </div> 
              <div class="col-md-12 next_btn">
                  <button class="btn btn-default" type="button" name="next" id="next">Next</button>                  
              </div> 
            </div>
              <div id="second_form">                                        
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="Company_name">Company Name *</label>
                    <input type="text" placeholder="company name" onkeypress="keypresstext('company_name')" id="company_name" name="company_name" maxlength="100" class="form-control" autocomplete="off">
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="company_address"> Company Address *</label>
                    <textarea placeholder="Company Address" id="caddress" onkeypress="keypresstext('caddress')" name="caddress" class="form-control" autocomplete="off"></textarea>                  
                  </div>
                </div>
                
                <div class="chan_wrapp">
					<div class="col-md-6">
					  <div class="form-group" id="imgChange">
						<label class="control-label" for="subTitle"><span>Change Photo *</span></label>  
						<input type="hidden" name="oldimg" value="" id="oldimg">                
						<input type="file" onchange="keypresstext('pimage')" id="pimage" name="pimage"  size="20" class="form-control" autocomplete="off"> 
					  </div>
					</div>
					<div class="col-md-6">
					  <div class="form-group">
						<div id="theDiv" class="mang_dri">                
						</div>
					  </div>
					</div>
				</div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="Company Contact">Company contact No *</label>
                   <input type="text" placeholder="Company Contact" onkeypress="keypresstext('company_contactno')" id="company_contactno" name="company_contactno" class="form-control" min="10" maxlength="20">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group"> 
                    <label for="cvehicle" class="text-uppercase">Do you operate a company vehicle *</label>
                    <select class="form-control" id="company_vehicle" name="company_vehicle" onchange="keypresstext('company_vehicle')">
                      <option class="selecttxt" value="" disable selected hidden>Select company vehicle</option>
                      <option value="Yes">YES</option>
                      <option value="No">NO</option>
                    </select>                 
                  </div>
                </div>
                <div id="compVehicle">
					<div class="col-md-6">
					  <div class="form-group">
						<label class="control-label" for="Personal">Document</label>
						<input type="hidden" name="vehicle_docold" value="" id="vehicle_docold">  
						<input type="file" id="comp_vehicle_doc" name="comp_vehicle_doc"  class="form-control" autocomplete="off"> 
					  </div>
					</div>
                </div>
                <div class="col-md-6">
                  <div class="form-group"> 
                    <label for="own_vehicle" class="text-uppercase">Do you operate your own vehicle exclusively for your own business? *</label>
                    <select class="form-control" id="own_vehicle" name="own_vehicle" onchange="keypresstext('own_vehicle')">
                      <option class="selecttxt" value="" disable selected hidden>Select own vehicle</option>
                      <option value="Yes">YES</option>
                      <option value="No">NO</option>
                    </select>                 
                  </div>
                </div>
                <div id="ownVehicle">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label" for="Personal">Document</label>
                      <input type="hidden" name="own_docold" value="" id="own_docold">  
                      <input type="file" id="own_doc" name="own_doc" class="form-control" autocomplete="off"> 
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group"> 
                    <label for="vehicle_for_company" class="text-uppercase">Do you operate your own vehicle for a company*</label>
                    <select class="form-control" id="vehicle_for_company" name="vehicle_for_company" onchange="keypresstext('vehicle_for_company')">
                      <option class="selecttxt" value="" disable selected hidden>Select</option>
                      <option value="Yes">YES</option>
                      <option value="No">NO</option>
                    </select>                 
                  </div>
                </div>
                <div id="ownVehicleForComp">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label" for="Personal">Document</label>
                      <input type="hidden" name="vehicle_for_company_docold" value="" id="vehicle_for_company_docold">  
                      <input type="file" id="vehicle_for_company_doc" name="vehicle_for_company_doc" class="form-control" autocomplete="off"> 
                    </div>
                  </div>    
                </div>     
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="Personal">Personal document</label>
                     <input type="hidden" name="personal_docold" value="" id="personal_docold">   
                     <input type="file" id="personal_doc" name="personal_doc"  class="form-control" autocomplete="off"> 
                  </div>
                </div>              
               <!-- <div class="col-md-6">
                  <div class="form-group">
                    <div id="pdf">      
                    </div>
                  </div>
                </div>-->
                <div class="col-md-6">
                  <div class="form-group"> 
                    <label for="paratransit" class="text-uppercase">Paratransit *</label>
                    <select class="form-control" onchange="keypresstext('paratransit')" id="paratransit" name="paratransit">
                      <option class="selecttxt" value="" disable selected hidden>Select paratransit</option>
                      <option value="1">Emergency ambulance</option>
                      <option value="2">wheel chair lift</option>
                      <option value="3">vehicle stretcher</option>
                      <option value="4">van</option>                    
                    </select>                 
                  </div>
                </div>
                <div class="col-md-12 modal-footer">
                  <button class="btn btn-default" name="back" id="back" type="button">Back</button>
                  <button class="btn btn-info tiny" name="btn_submit" id="btn_submit" type="button">Save</button>
                </div>
              </div>  
          </div>
        </div>        
      </div>
    </form>
  </div>
</div>
<script src="<?php echo base_url(); ?>js/admin/drivers.js"></script>
<?php $this->load->view('footer'); ?>
