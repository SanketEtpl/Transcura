<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Notification 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Notification</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6 resp_share">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="list_of_notification(0,'user','DESC');">
                      Search
                    </a>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="get_notification(this);">
                      Add New
                    </a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table class="table table-bordered table-striped dataTable" id="list_of_notification" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                      <th width="30%" data-name="id" data-order="DESC" class="sorting">ID</th>
                      <th width="30%" data-name="n_title" data-order="DESC" class="sorting">Title</th>
                      <th width="30%" data-name="n_full_description" data-order="DESC" class="sorting">Description</th>
                      <th width="20%" class="">Action</th>
                      </tr>
                    </thead>
                      <tbody>                     
                      </tbody>
                    </table>
                  </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="notificationModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo  base_url(); ?>admin/Notification/save_notification" id="notificationForm" name="notificationForm" class="valida">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Manage Notification</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="category_title">Title</label>
                  <input type="hidden" id="user_key" name="user_key">
                  <input type="text" placeholder="Title" id="notification_title" required="true" name="notification_title" maxlength="255" filter="title_name" class="form-control" autocomplete="off">
                </div>
              </div>
           </div>
           <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="car_rate">Description</label>
                  <textarea rows="4" cols="50" placeholder="Description" id="notification_description" required="true" name="notification_description" maxlength="500" filter="description" class="form-control" autocomplete="off"></textarea>                  
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="country" class="control-label">Driver</label>
                  <select class="form-control" name="user" id="user" required="true"  >
                    <option value="" class="selecttxt" disable selected hidden>Select driver user</option>
                    <?php 
                    if(!empty($driverData)){ ?>
                    <option value="allDrivers">All drivers</option>
                    <?php foreach($driverData as $driver_user_name) { ?>
                    <option value="<?php echo $driver_user_name['id']; ?>"><?php echo $driver_user_name['full_name']?></option>
                    <?php } } else { ?> 
                    <option value="">Drivers not available</option>
                    <?php } ?>  
                  </select>             
                </div>  
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/notification.js"></script>
<?php $this->load->view('footer'); ?>
