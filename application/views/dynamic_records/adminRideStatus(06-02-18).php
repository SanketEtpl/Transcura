<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Ride request 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Ride request management</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                      
                    </div>
                  </div>

                <div class="col-sm-6 resp_share">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="listOfScheduleRide(0,'tm_id','DESC');">
                      Search
                    </a>
                    <!-- <a class="btn btn-primary" href="javascript:void(0);" onclick="getScheduleRide();">
                      Add New
                    </a> -->
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
        					  <table class="table table-bordered table-striped dataTable" id="listOfScheduleRide" role="grid" aria-describedby="example1_info">
          						<thead>
          						  <tr role="row">
            							<th width="20%" data-name="name" data-order="DESC" class="sorting">Name</th>
                          <th width="15%" data-name="urgency" data-order="DESC" class="sorting">Urgency</th>
                          <th width="20%" data-name="sr_trip_id" data-order="DESC" class="sorting">Trip ID</th>
            							<th width="15%" data-name="sr_date" data-order="DESC" class="sorting">Trip date</th>
            							<th width="10%" data-name="sr_pick_up_time" data-order="DESC" class="sorting">Pick up time</th>
            							<th width="25%" data-name="source_address" data-order="DESC" class="sorting">Source address</th>
                          <th width="15%" data-name="admin_status" data-order="DESC" class="sorting">Ride status</th>
                          <th width="10%" class="">Action</th>
          						  </tr>
          						</thead>
        						  <tbody>                        
        						  </tbody>
        						</table>
        					</div>
                </div>
              </div>
              <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="adminRideReqModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>admin/Admin_ride_status/saveRideStatus" id="adminRideReqForm" name="adminRideReqForm" class="valida">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Ride request view</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="lblTI">Trip id</label>
                    <div id="sr_trip_id"></div>                    
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="lblTD">Trip Date</label>
                    <div id="sr_date"></div>                    
                  </div>
                </div>
              </div> 

              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="lblPUP">Pick up point</label>
                    <div id="sr_pick_up_time"></div>                    
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="lblTDT">Total distance</label>
                    <div id="sr_total_distance"></div>                    
                  </div>
                </div>
              </div>   
               <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="lblSA">Source address</label>
                    <div id="source_address"></div>                    
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="lblDA">Destination address</label>
                    <div id="destination_address"></div>                    
                  </div>
                </div>
              </div>    

               <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="lblSA">Duration</label>
                    <div id="duration"></div>                    
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="lblUT">Urgency type</label>
                    <div id="sr_urgency_type"></div>                    
                  </div>
                </div>
              </div> 
              <div class="col-md-12">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="control-label" for="lblTT">Trip again</label>
                    <div id="sr_trip_type"></div>                    
                  </div>
                </div> 
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="lblTS" class="control-label">Trip Status</label>
                    <select class="form-control" name="admin_status" id="admin_status" required="true"  >
                      <option value="" class="selecttxt" disable selected hidden>Select status</option>
                      <option value="1">Open ride</option>
                      <option value="2">Accepted ride</option>
                      <option value="3">Closed ride</option>
                      <option value="4">Rejected ride</option>
                    </select>             
                  </div>  
                </div>                               
              </div>

            </div>
            <div class="modal-footer">
             <input type="hidden" id="user_key" name="user_key">
             <input type="hidden" id="status_key" name="status">            
              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
              <button class="btn btn-info tiny" type="submit" id="saveBtn">Save</button>
            </div>
            </div>       
        </div>
      </div>

      </form>
    </div>
  <script src="<?php echo base_url(); ?>js/admin/adminRideStatus.js"></script>
<?php $this->load->view('footer'); ?>