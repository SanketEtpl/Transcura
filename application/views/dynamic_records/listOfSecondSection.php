<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Second section
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Second section management</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6 resp_share">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="listAllSecondSectionData(0,'id','DESC');">
                      Search
                    </a>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getSecondSection();">
                      Add New
                    </a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">  
                    <table class="table table-bordered table-striped dataTable" id="listAllSecondSectionData" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">             
                      <th width="25%" data-name="ss_title" data-order="DESC" class="sorting">Title</th>
                      <th width="25%" data-name="ss_description" data-order="DESC" class="sorting">Description</th>
                      <th width="25%" data-name="ss_icon_image" data-order="DESC" class="sorting">Icon</th>                        
                      <th width="10%" class="">Action</th>
                      </tr>
                    </thead>
                      <tbody>                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="SSModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>save_second_content" id="SSForm" name="SSForm" class="valida" enctype="multipart/form-data">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Second section</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="Title">Name of title</label>
                  <input type="hidden" id="page_key" name="page_key">
                  <input type="text" placeholder="Name of title" maxlength="255" id="title_name" filter="title_name" name="title_name"  required="true" class="form-control"  autocomplete="off">
                </div>
              </div>              
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="descpt">Description</label>
                  <textarea placeholder="Description" id="description" name="description" filter="description" class="form-control at-required" required="true" rows="5" cols="3" autocomplete="off"></textarea>
                  <!-- <input type="text" placeholder="Description" id="description" name="description"  required="true" class="form-control"  autocomplete="off"> -->
                </div>
              </div>              
            </div>
            <div class="row" id="imgChange">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="subTitle">Image icon</label>                  
                  <input type="file" placeholder="Image icon" id="icon_image" name="icon_image" size="20" class="form-control" autocomplete="off" onchange="document.getElementById('icon_image').src = window.URL.createObjectURL(this.files[0])">                 
                </div>
              </div>
             <!--  <div id="theDiv">                 
              </div> -->
               <div class="col-md-6" >
                 <div class="form-group pati_user" id="theDiv">
                 </div>     
              </div> 
            </div>
          </div>
          <div class="modal-footer">
            <input type="hidden" id="oldimg" name="oldimg">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="submit">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/secondSection.js"></script>
<?php $this->load->view('footer'); ?>
