<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Ride request 
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Ride request management</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                      
                    </div>
                  </div>

                <div class="col-sm-6 resp_share">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="listOfScheduleRide(0,'tm_id','DESC');">
                      Search
                    </a>
                    <!-- <a class="btn btn-primary" href="javascript:void(0);" onclick="getScheduleRide();">
                      Add New
                    </a> -->
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <table class="table table-bordered table-striped dataTable" id="listOfAssignRides" role="grid" aria-describedby="example1_info">
                      <thead>
                        <tr role="row">
                          <th width="20%" data-name="name" data-order="DESC" class="sorting">Name</th>
                          <th width="20%" data-name="sr_trip_id" data-order="DESC" class="sorting">Trip ID</th>
                          <th width="15%" data-name="sr_date" data-order="DESC" class="sorting">Trip date</th>
                          <th width="10%" data-name="sr_pick_up_time" data-order="DESC" class="sorting">Pick up time</th>
                          <th width="25%" data-name="source_address" data-order="DESC" class="sorting">Source address</th>
                          <th width="25%" data-name="source_address" data-order="DESC" class="sorting">Assign To</th>
                          
                          <th width="10%" class="">Action</th>
                        </tr>
                      </thead>
                      <tbody>                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>

  <<script src="<?php echo base_url(); ?>js/admin/assign_to_tp.js" type="text/javascript"></script>