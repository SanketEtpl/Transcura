<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Contact-us
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Contact-us</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                  <div class="col-sm-6 resp_share">
                    <div id="example1_filter" class="dataTables_filter">
                      <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                      <a class="btn btn-primary" href="javascript:void(0);" onclick="listOfContactUs(0,'user','DESC');">
                        Search
                      </a>
                   
                    </div>
                  </div>
                </div>
              <div class="row">
                <div class="col-sm-12">
                   <div class="table-responsive"> 
                      <table class="table table-bordered table-striped dataTable" id="listOfContactUs" role="grid" aria-describedby="example1_info">
                        <thead>
                          <tr role="row">
                            <th width="25%" data-name="fname" data-order="DESC" class="sorting">Name</th>                      
                            <th width="25%" data-name="cu_email" data-order="ASC" class="sorting">email</th>
                            <th width="15%" data-name="cu_phone" data-order="ASC" class="sorting">Contact number</th>
                            <th width="15%" >Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="contactUsModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="" id="contactUsForm" name="contactUsForm" class="valida">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">View contact us</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="c_name">Name</label>                  
                  <div id="full_name"></div>
                </div>
              </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="trip_id">Email ID</label>
                  <div id="cu_email"></div>
                </div>
              </div>
           </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="trip_date">Phone number</label>
                  <div id="cu_phone"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="put">Message</label>
                  <div id="cu_message"></div>
                </div>
              </div>
            </div>            
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <!-- <button class="btn btn-info tiny" type="submit">Save</button> -->
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/contact_us.js"></script>
  <?php $this->load->view('footer'); ?>
