<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Schedule ride details
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Schedule ride details management</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6 resp_share">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="listOfScheduleRide(0,'tm_id','DESC');">
                      Search
                    </a>
                 <!--    <a class="btn btn-primary" href="javascript:void(0);" onclick="getTestimonial();">
                      Add New
                    </a> -->
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
        					  <table class="table table-bordered table-striped dataTable" id="listOfScheduleRide" role="grid" aria-describedby="example1_info">
          						<thead>
          						  <tr role="row">
            							<th width="25%" data-name="sr_trip_id" data-order="DESC" class="sorting">Trip ID</th>
            							<th width="25%" data-name="sr_date" data-order="DESC" class="sorting">Trip date</th>
            							<th width="25%" data-name="sr_pick_up_time" data-order="DESC" class="sorting">Pick up time</th>
            							<th width="25%" data-name="driver_status" data-order="DESC" class="sorting">Status</th>
            							<!-- <th width="25%" data-name="tm_stars" data-order="DESC" class="sorting">Stars</th>  -->                       
            							<th width="10%" class="">Action</th>
          						  </tr>
          						</thead>
        						  <tbody>                        
        						  </tbody>
        						</table>
        					</div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="rideDetailsModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="" id="rideDetailsForm" name="rideDetailsForm" class="valida">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Schedule ride Details</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="lbl_trip">Trip ID : </label>
                  <span id="trip_id"></span>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-lable" for="lbl_trip_dt">Trip date : </label>
                  <span id="trip_date"></span>
                </div>
              </div>                                                 
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-lable" for="lbl_driver_status">Driver status : </label>                  
                  <span id="driver_status"></span>
                </div>                
              </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label class="control-lable" for="lbl_pt">Actual Pick up time : </label>                  
                  <span id="pick_up_time"></span>
                </div>                
              </div>   
            </div>
             <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-lable" for="lbl_driver_status">Driver pick up time : </label>                  
                  <span id="driver_pick_up_time"></span>
                </div>                
              </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label class="control-lable" for="lbl_pt">Driver drop time : </label>                  
                  <span id="driver_drop_time"></span>
                </div>                
              </div>   
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-lable" for="lbl_from">Source address : </label>                  
                  <span id="source_address"></span>
                </div>                
              </div> 
               <div class="col-md-6">
                <div class="form-group">
                  <label class="control-lable" for="lbl_to">Destination address : </label>                  
                  <span id="destination_address"></span>
                </div>                
              </div>   
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-lable" for="lbl_duration">Duration : </label>                  
                  <span id="duration"></span>
                </div>                
              </div> 
               <div class="col-md-6">
                <div class="form-group">
                  <label class="control-lable" for="lbl_distance">Distance : </label>                  
                  <span id="distance"></span>
                </div>                
              </div>   
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-lable" for="lbl_first_ride">First ride : </label>                  
                  <span id="first_ride"></span>
                </div>                
              </div> 
               <div class="col-md-6">
                <div class="form-group">
                  <label class="control-lable" for="lbl_second_ride">Second ride : </label>                  
                  <span id="second_ride"></span>
                </div>                
              </div>   
            </div>
            
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/ride_details.js"></script>
<?php $this->load->view('footer'); ?>