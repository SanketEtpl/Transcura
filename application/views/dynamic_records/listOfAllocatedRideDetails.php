<?php 
/* Author : Ram K 
   Page : listOfAllocatedRideDetails.php 
   Description:  NEMT allocated ride
   Date: 09 Feb 2018 
*/
   $this->load->view('header');
 ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        NEMT allocated ride
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">NEMT allocated ride</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                  <div class="col-sm-6 resp_share">
                    <div id="example1_filter" class="dataTables_filter">
                      <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                      <a class="btn btn-primary" href="javascript:void(0);" onclick="listOfAllocatedRideDetails(0,'user','DESC');">
                        Search
                      </a>
                   
                    </div>
                  </div>
                </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive"> 
                    <table class="table table-bordered table-striped dataTable" id="listOfAllocatedRideDetails" role="grid" aria-describedby="example1_info">
                      <thead>
                        <tr role="row">
                          <th width="25%" data-name="fname" data-order="DESC" class="sorting">User</th>                      
                          <th width="25%" data-name="free_ride" data-order="ASC" class="sorting">Ride Allocated</th>               
                          <th width="15%" >Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="allocatedModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" id="allocatedrideForm" name="allocatedrideForm" class="valida">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Edit allocated ride</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group" id="fullnameissue">
                  <label class="control-label" for="full_name">Full name</label>
                  <input type="hidden" id="user_key" name="user_key">
                  <input type="text" placeholder="Full name" id="full_name" filter="name" required="true" name="full_name" class="form-control fullname" maxlength="50" autocomplete="off">
                </div>
              </div>  
               <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="trip_id">Allocated Ride</label>
                  <input type="number" placeholder="Ride" id="free_ride" filter="number" required="true" name="free_ride" class="form-control fullname" maxlength="50" autocomplete="off">
                </div>
              </div>
           </div>
            
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="button" id="btn_submit">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/nemt_allocated_ride.js"></script>
  <?php $this->load->view('footer'); ?>
