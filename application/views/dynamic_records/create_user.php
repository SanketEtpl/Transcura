<?php $this->load->view('header'); ?>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Create User
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Create User</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
             <form name="userForm" id="userForm" method="post" action="<?php echo base_url(); ?>admin/user/saveUsers" enctype="multipart/form-data" class="valida">

              <div class="row">
                <div class="col-lg-12">
                  <div class="col-lg-6">
                    <label class="control-label" for="selectType">User Type : </label>
                    <select name="selectType" id="selectType" class="form-control" required="true">
                      <option value="">Select Type</option>
                      <?php
                      foreach ($userRole as $urole) {
                        echo "<option value=".$urole['role_id'].">".$urole['role_name']."</option>";
                      }
                      ?>
                    </select>
                  </div>
                  <div class="col-lg-6">
                    <label class="control-label" for="userfile">CSV File : </label>
                    <input type="file" name="userfile" id="userfile" class="form-control" required="true">
                  </div>
                </div>
              </div>
              <br /><br />
              <div class="row">
                <div class="col-lg-12">
                  <div class="col-lg-6">
                    <button class="btn btn-info tiny" type="submit">Save</button>
                  </div>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</div>

<!-- <script src="<?php //echo base_url(); ?>js/admin/customer_care.js"></script> -->
<script type="text/javascript">
  $(document).ready(function(){
    $("#userForm").ajaxForm({
      dataType: 'json', 
      beforeSend: function() 
      {
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {

      },
      success: function(json) 
      {
        if(json.status == "success"){
          toastr.success(json.message,"Success:");
          setTimeout(function(){ location.reload(); }, 3000);
        }else if(json.status == "error"){    
          toastr.error(json.message,"Error:");
        }      
      },
      complete: function(json) 
      {
        $("#userForm").find('[type="submit"]').removeAttr('disabled');
      },
      error: function()
      {
        toastr.error(json.message,"Error:");
        $("#userForm").find('[type="submit"]').removeAttr('disabled');
      }
    });
  });

</script>
<?php $this->load->view('footer'); ?>