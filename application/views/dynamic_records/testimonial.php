<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Testmonial
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Testmonial management</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6 resp_share">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="listOfTestimonialData(0,'tm_id','DESC');">
                      Search
                    </a>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getTestimonial();">
                      Add New
                    </a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
					  <table class="table table-bordered table-striped dataTable" id="listOfTestimonialData" role="grid" aria-describedby="example1_info">
						<thead>
						  <tr role="row">
							<th width="25%" data-name="tm_id" data-order="DESC" class="sorting">ID</th>
							 <th width="25%" data-name="tm_name" data-order="DESC" class="sorting">Name</th>
							 <th width="25%" data-name="tm_description" data-order="DESC" class="sorting">description</th>
							 <th width="25%" data-name="tm_image" data-order="DESC" class="sorting">Images</th>
							 <!-- <th width="25%" data-name="tm_stars" data-order="DESC" class="sorting">Stars</th>  -->                       
							<th width="10%" class="">Action</th>
						  </tr>
						</thead>
						  <tbody>                        
						  </tbody>
						</table>
					</div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="testimonialModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>admin/Testimonial/save_testimonial" enctype="multipart/form-data" id="testimonialForm" name="testimonialForm" class="valida">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Testimonial</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="name">Name</label>
                  <input type="hidden" id="page_key" name="page_key">
                  <input type="text" placeholder="name" id="tm_name" name="tm_name" maxlength=50 required="true" class="form-control"  autocomplete="off">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-lable" for="description">Description</label>
                  <textarea id="tm_description" name="tm_description" placeholder="description" required="true" calss="form-control" autocomplete="off"></textarea>
                </div>
              </div>
              <div class="col-md-12 upl_test">
                <div class="form-group">
                  <label class="control-lable" for="image">Upload image</label>
                  <input type="hidden" id="oldimage" name="oldimage">
                  <input type="file" id="tm_image" name="tm_image"  class="form-control" autocomplete="off">                  
                </div>                
              </div>  
              <div class="col-md-12">
                <div class="form-group">
                  <div id="testimg"></div>
                </div>
              </div>                         
            </div>
            <!-- <div class="col-md-6">
              <div class="form-group">
                <label class="control-lable" for="stars">Upload stars</label>
                <input type="hidden" id="oldstars" name="oldstars">
                <input type="file" id="tm_stars" name="tm_stars" class="form-control" autocomplete="off">
              </div>              
            </div> 
            <div id="starsimg"></div>
          -->
            
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="submit">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/testimonial.js"></script>
<?php $this->load->view('footer'); ?>
