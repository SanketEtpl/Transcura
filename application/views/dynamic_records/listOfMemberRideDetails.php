<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Member schedule ride
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Member Schedule ride</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                  <div class="col-sm-6 resp_share">
                    <div id="example1_filter" class="dataTables_filter">
                      <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                      <a class="btn btn-primary" href="javascript:void(0);" onclick="listOfMemberScheduleRideDetails(0,'user','DESC');">
                        Search
                      </a>
                   
                    </div>
                  </div>
                </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive"> 
                    <table class="table table-bordered table-striped dataTable" id="listOfMemberScheduleRideDetails" role="grid" aria-describedby="example1_info">
                      <thead>
                        <tr role="row">
                          <th width="25%" data-name="fname" data-order="DESC" class="sorting">User</th>                      
                          <th width="25%" data-name="sr_date" data-order="ASC" class="sorting">Date</th>
                          <th width="15%" data-name="sr_appointment_time" data-order="ASC" class="sorting">Time</th>
                          <th width="15%" data-name="source_address" data-order="ASC" class="sorting">Source address</th>
                          <th width="15%" data-name="destination_address" data-order="ASC" class="sorting">Destination address</th>
                          <th width="15%" >Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="memberScheduleModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="" id="memberScheduleRideForm" name="memberScheduleRideForm" class="valida">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">View member schedule ride details</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="c_name">Customer name</label>                  
                  <div id="full_name"></div>
                </div>
              </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="trip_id">Trip ID</label>
                  <div id="sr_trip_id"></div>
                </div>
              </div>
           </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="trip_date">Trip Date</label>
                  <div id="sr_date"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="put">Pick up time</label>
                  <div id="sr_pick_up_time"></div>
                </div>
              </div>
            </div>            
            <div class="row">              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="dest_add">Destination address</label>
                  <div id="destination_address"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="source_add">Source address</label>
                  <div id="source_address"></div>
                </div>
              </div>
            </div>
           <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="s_name">State name</label>
                  <div id="state_name"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="c_name">Country name</label>
                  <div id="country_name"></div>
                </div>
              </div>
            </div>
             <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="appint_time">Appointment time</label>
                  <div id="sr_appointment_time"></div>
                </div>
              </div>
            </div>  
                               
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <!-- <button class="btn btn-info tiny" type="submit">Save</button> -->
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/member_schedule_ride.js"></script>
  <?php $this->load->view('footer'); ?>
