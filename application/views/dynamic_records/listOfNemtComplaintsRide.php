<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        NEMT complaints ride
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">NEMT compaints ride</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                  <div class="col-sm-6 resp_share">
                    <div id="example1_filter" class="dataTables_filter">
                      <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                      <a class="btn btn-primary" href="javascript:void(0);" onclick="listOfNemtComplaintsRide(0,'user','DESC');">
                        Search
                      </a>
                   
                    </div>
                  </div>
                </div>
              <div class="row">
                <div class="col-sm-12">
                   <div class="table-responsive"> 
                      <table class="table table-bordered table-striped dataTable" id="listOfNemtComplaintsRide" role="grid" aria-describedby="example1_info">
                        <thead>
                          <tr role="row">
                            <th width="25%" data-name="full_name" data-order="DESC" class="sorting">Customer name </th>                      
                            <th width="25%" data-name="src_driver_name" data-order="ASC" class="sorting">Driver name</th>
                            <th width="15%" data-name="src_date_of_service" data-order="ASC" class="sorting">Date of service</th>
                            <th width="15%" >Action</th>
                          </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="complaintsModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="" id="complaintsRideForm" name="complaintsRideForm" class="valida">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">View complaint ride details</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="c_name">Customer name</label>                  
                  <div id="full_name"></div>
                </div>
              </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="trip_id">Driver name</label>
                  <div id="src_driver_name"></div>
                </div>
              </div>
           </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="trip_date">Trip Date</label>
                  <div id="src_date_of_service"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="put">Transportation provider</label>
                  <div id="src_name_of_transportation_provider"></div>
                </div>
              </div>
            </div>            
            <div class="row">              
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="dest_add">Complaint type</label>
                  <div id="ct_name"></div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="source_add">Description</label>
                  <div id="src_description"></div>
                </div>
              </div>
            </div>             
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <!-- <button class="btn btn-info tiny" type="submit">Save</button> -->
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/nemt_complaints_ride.js"></script>
  <?php $this->load->view('footer'); ?>
