<?php
	$getdata = $this->uri->segment(2);
?>
<div class="rides_tab">
	<ul class="nav nav-tabs">
		  <li <?php if($getdata =="schedule-ride"){ echo 'class="active"'; } ?>><a data-toggle="tab" href="#sched_ride">Scheduled Ride </a></li>
		  <li <?php if($getdata =="schedule-completed"){ echo 'class="active"'; } ?>><a data-toggle="tab" href="#comp_ride">Completed Ride</a></li>
		  <li <?php if($getdata =="schedule-cancelled"){ echo 'class="active"'; } ?>><a data-toggle="tab" href="#cancel_ride">Cancelled Ride</a></li>
		  <li class="cust_care">
			<a data-toggle="tab" href="#custom">customer care</a>
			<ul class="cust_sub_menu">
				<li><a href="#">Complaints</a></li>
				<li><a href="#">Technical Support</a></li>
				<li><a href="#">Rate a Trip</a></li>
			</ul>	
		  </li>
	</ul>
</div>