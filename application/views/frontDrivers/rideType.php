<?php $this->load->view("user/header"); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<div class="driver_sched_strt">
	<div class="container">
		<div class="row">
			<div class="driver_strt_inn">
				<h3 class="heal_head">Schedule A Trip</h3>
				<div class="dri_main">
					<!-- <div class="col-md-6">						 -->
					<div class="rightform">
						<div class="sch_log">								
							<form name="schedule_ride" action="<?php echo base_url(); ?>ride-type" method="POST" autocomplete="off">
								<div class="row col_padding">	
									
									<div class="new-frm">

										<div class="form-group">
											<label for="ride_type" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Trip Type <div class="req-red">*</div></label>
											<select class="form-control" id="ride_type" name="ride_type">
												<option value="" disable selected hidden>Select Trip Type</option>
												<?php 
												$ride_type_all = unserialize(RIDE_TYPE);
												
												foreach ($ride_type_all as $key => $value) {
													echo '<option value="'.$key.'">'.$value.'</option>';
												}
												?>
											</select>
											<?php echo form_error('ride_type', '<div class="error">', '</div>'); ?>
										</div>

										<div class="proc_btn">
											<input type="submit" value="PROCEED" class="btn btn-block btn-primary" id="btnDisable"/>
										</div>														
									</div>
								</div>
							</form>										
						</div>
					</div>
					<!-- </div> -->
				<!-- <div class="col-md-6">
					<div class="sch_img" style="background-image:url('<?php echo base_url(); ?>assets/img/sch_car.png');"></div>
				</div> -->		
			</div>					
		</div>
	</div>
</div>
</div>
</div>	
<?php $this->load->view("user/footer"); ?>