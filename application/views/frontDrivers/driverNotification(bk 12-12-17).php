<?php //print_r($notification);exit;?>
<?php $this->load->view("user/header"); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<div class="noti_wrapp">
	<div class="container">
		<div class="row">
			<div class="noti_inner">
				<h3 class="noti_head">NOtification</h3>
				<div id="success_message"></div>
				<div id="error_message"></div>
				<div class="noti_box">
					<div class="row">
						<div class="noti_sec">
							<div class="col-md-4 lt_noti">
								<div class="lt_noti_wrap">
									<div class="lt_not_tit">All Notification List <span class="delete_sec"><a href="javascript:void(0)" class="delete_notification"><i class="fa fa-trash-o" aria-hidden="true"></i></a></span></div>
									<div class="not_tit_wrap">
										<div id="noti_box">
											<?php 
												$i=1;
												if(!empty($notification)){ 
													foreach($notification as $key => $value)
													{ ?>	
													<a href="" data-related="<?php echo 'id_'.$i; ?>">	
													<input type="hidden" id="delete_notification_value" value="">
													<div class="notif_val" id="<?php echo $value['id'];?>"></div>									
													<div class="noti_box_inn <?php echo 'div_id_'.$i; ?> <?php if($i == 1){ echo "act_noti"; }elseif($i%2==0){ echo "bg_chan"; } ?>">
													<div class="lt_rat"><i class="fa fa-star" aria-hidden="true"></i></div>
													<div class="rt_box">
														<div class="not_head"><?php echo $value['n_title']; ?></div>
														<div class="not_txt">
															<?php
																$string = $value['n_full_description'];
																if (strlen($string) > 60) {													
																	$stringCut = substr($string, 0, 100);
																	$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
																}
																echo $string;
															?>
														</div>
														<div class="not_ago"><?php echo $value['created_on']; ?></div>
													</div>
												</div>
											</a>
											<?php $i++; }  } ?>			
										</div>	
									</div>
								</div>
							</div>
							<div class="col-md-8 rt_noti">
								<div class="not_mess_wrapp">
									<div class="rt_not_tit">
										message box										
										<span class="delete_sec"><a href="javascript:void(0)" class="delete_notification" id="delete_notification"><i class="fa fa-trash-o" aria-hidden="true"></i></a></span>
										<span class="delete_rat"><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></span>
									</div>
									<?php 
										$i=1;
										if(!empty($notification)){ 
										foreach($notification as $key => $value)
										{ ?>									
									<div class="noti_mess" id="<?php echo 'id_'.$i; ?>">
										
										<div class="not_new"><?php echo $value['n_title']; ?></div>
										<div class="not_mes_txt">
											<?php echo $value['n_full_description']; ?>
										</div>
									</div>
									<?php $i++; }  } ?>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<?php $this->load->view("user/footer"); ?>
