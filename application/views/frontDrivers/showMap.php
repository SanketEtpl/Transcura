<?php $this->load->view("user/header"); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<div class="driver_sched_strt">
	<div class="container">
		<div class="row">
			<div class="driver_strt_inn">
				<h3 class="heal_head">Schedule A Trip</h3>
				<div class="backdiv col-md-12 text-right" style="margin-bottom:15px; margin-top: -50px;">
					<a href="<?php echo base_url() ?>ride-type" class="btn btn-primary">Back</a>
				</div>
				<div class="dri_main">
					<div class="rightform">
						<!-- <div class="sch_log">								 -->
						<form name="nemt_schedule_ride" id="nemt_schedule_ride" action="<?php echo base_url(); ?>schedule-nemt-ride" method="POST" autocomplete="off">
							<div class="row col_padding">								
								<div class="col-md-12">
									<p class="error" style="color: red;"></p>
								</div>

								<!-- PICK UP ADDRESS -->
								<div class="col-md-4">
									<label><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>&nbsp; Pick Up Address :  <div class="req-red">*</div></label>
									<input type="text" name="pick_up_address" id="pick_up_address" placeholder="Pick Up Address">
								</div>

								<!-- DROP ADDRESS -->
								<div class="clearfix"></div><br />
								<?php
								if($this->session->userdata('schedulaRideData')['sr_ride_type'] == "4")
								{

									$dcnt = 1;
								}else
								{
									$dcnt="";
								}
								?>
								<div class="col-md-6 dropLoc">
									<label><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>&nbsp; Drop Off Address <?php echo $dcnt;?> :  <div class="req-red">*</div></label>
									<input type="text" class="drop_point" name="drop_address[]" placeholder="Drop Address">
									<!-- <span id="addBtnDrop" class="addNewDestination"><i class="fa fa-plus-circle fa-2x"></i></span> -->
								</div>
								<input type="hidden" name="txt_srcnt" id="txt_srcnt" value="1" />

								<!-- DYNAMIC DROP ADDRESS -->
								<div class="clearfix"></div><br />
								<div class="allDropLocations"></div>

								<!-- ADD BUTTON -->
								<div class="clearfix"></div>
								<div class="col-md-1 addNewDestination"><span id="addBtnDrop"><i class="fa fa-plus-circle fa-2x"></i></span></div>

								<!-- ANOTHER RETURN ADDRESS -->
								<div class="clearfix"></div>
								<div class="finalDrop"></div>								

								<!-- PHARMACY ADDRESS -->
								<div class="clearfix"></div><br />
								<div class="col-md-4 pharmacy_det">
									<label><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>&nbsp; Pharmacy Address :  <div class="req-red">*</div></label>
									<input type="text" name="pharmacy_address" id="pharmacy_address" placeholder="Pharmacy Address">
								</div>
								<div class="clearfix"></div><br />
								<div class="col-md-12" id="returnPharmacy" style="display:none;">
									<label for="">Do You want to end the trip at the Pharmacy?</label>
									<input type="radio" name="pharmacy_stop" value="yes"> Yes
									<input type="radio" name="pharmacy_stop" value="no" checked> No
								</div>
									
								</div>

								<div class="clearfix"></div><br />
								<div class="col-md-2">
									<input type="button" name="" value="Get Address" class="btn btn-block btn-primary" onclick="getRoute()">
								</div>
								<div class="clearfix"></div><br />
								<div id="map" style="width: 1000px;height: 400px;"></div>
							</div>
							<div class="clearfix"></div><br />
							<div class="col-md-2">
								<input type="submit" id="proceed_map" value="PROCEED" class="btn btn-block btn-primary"/>
							</div>														
							<!-- </div> -->
						</div>
					</form>										
				</div>
				<!-- </div> -->
			</div>					
		</div>
	</div>
</div>
</div>
</div>
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>   
<script type="text/javascript">
	$(document).ready(function(){		
		//------ autocomplete location -------
		//
		var mulripleLocation = '<?php  echo $mulripleLocation[0]["multiple_location"]; ?>'; 
		function initialize_source() {
			var input = document.getElementById('pick_up_address');
			var drop = document.getElementsByClassName("drop_point");
			var pharm = document.getElementById('pharmacy_address');
			var autocomplete = new google.maps.places.Autocomplete(input);
			var autocomplete = new google.maps.places.Autocomplete(pharm);
			// ------- autocomplete for dynamically added drop points ---------
			for (var i = 0; i < drop.length; i++) {
				var autocomplete = new google.maps.places.Autocomplete(drop[i]);
				autocomplete.inputId = drop[i].id;
			}
		}

		// ----- show default map ------
		function initMap() {
			var directionsService = new google.maps.DirectionsService;
			var directionsDisplay = new google.maps.DirectionsRenderer;
			var myLatLng = {lat: 18.559003, lng: 73.786763};
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 13,
				center: myLatLng
			});
			directionsDisplay.setMap(map);
		}
		google.maps.event.addDomListener(window, 'load', initialize_source);
		google.maps.event.addDomListener(window, "load", initMap);

		$("#addBtnDrop").hide();
		$(".pharmacy_det").hide();
		var ride_type = '<?php echo $this->session->userdata('schedulaRideData')['sr_ride_type'] ?>';


		//  Sanket Code Start 
		if(ride_type == "2" || ride_type == "6")
		{
			$('.pharmacy_det').show();

			
		}//  Sanket Code End 
		else if(ride_type == "3")
		{
			$("<br><div id='beforeCheck'><div class='clearfix'></div><input type='checkbox' id='checkFDrop' checked='true'>Are you sure you want to return to the same pick up address ?</div>").insertBefore(".addNewDestination");	
		}else if(ride_type == "4")
		{
			$(".pharmacy_det").show();
			$("#returnPharmacy").show();
		}
		else if(ride_type == "5" || ride_type == "6")
		{
			$("#addBtnDrop").show();
		}
		else
		{
			$("#addBtnDrop").hide();
			$(".pharmacy_det").hide();
		}

		//--------  dynamically add drop address ----------
		$(".allDropLocations").html('');

		// var dcnt =2;
		$("#addBtnDrop").click(function(){	

			var srno = ($('.dropLoc').length) + 1;

			/*if(srno <= mulripleLocation)
			{*/
				$(".allDropLocations").append('<div class="dropLoc"><div class="col-md-6">\n\
					<label><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>\n\
						&nbsp; Drop Off Address <span class="sNo">'+srno+'</span> :  <div class="req-red">*</div></label>\n\
						<input type="text" class="drop_point" name="drop_address[]" placeholder="Drop Address">\n\
						<span class="remove_drop"><i class="fa fa-minus-circle fa-2x"></i></span>\n\
					</div><div class="col-md-1 "></div>\n\
					<div class="clearfix"></div><br /></div>');
				initialize_source();
				$("#txt_srcnt").val(srno);

				// dcnt++;	
			/*}
			else
			{
				alert('You can add upto Max '+ mulripleLocation + ' locations');
			}*/
		});

		//----- remove drop address ---------
		$('body').on('click','.remove_drop', function(){
			$(this).parent().parent('.dropLoc').remove();
			arrangeSno();
		});

		// ------- Add Final Drop point if not checked --------
		$("#checkFDrop").click(function(){	
			if(!$(this).is(':checked'))
			{
				$(".finalDrop").html('<br><div class="col-md-6">\n\
					<label><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>\n\
						&nbsp; Final Drop Off Address :  <div class="req-red">*</div></label>\n\
						<input type="text" class="drop_point" name="drop_address[]" placeholder="Drop Address">\n\
					</div>\n\
					<div class="clearfix"></div><br />');
				initialize_source();
			}		
			else
			{
				$(".finalDrop").html("");
			}
			
		});		

		//----- proceed button ----------
		$("#proceed_map").click(function(){
			if(checkPick() && checkDrop() && checkPharmacy())
			{
			}else
			{
				alert("Please enter all required details");
				return false;
			}
		});

	});

</script>
<script>

// function newInit_src() {
// 	var input = document.getElementById('pick_up_address');
// 	var drop = document.getElementsByClassName("drop_point");
// 	var pharm = document.getElementById('pharmacy_address');
// 	var autocomplete = new google.maps.places.Autocomplete(input);
// 	var autocomplete = new google.maps.places.Autocomplete(pharm);
// 	// ------- autocomplete for dynamically added drop points ---------
// 	for (var i = 0; i < drop.length; i++) {
// 		var autocomplete = new google.maps.places.Autocomplete(drop[i]);
// 		autocomplete.inputId = drop[i].id;
// 	}
// }

// Sanket Code Start
// function testDemo($this)
// {
// 	if (!$this.checked) 
// 	{

// 		var len = $('#multiDest').length;
//     	/*$('<div id="newDestAddr"><br><br><div class="clearfix"></div><div class="col-md-4" >\n\
// 				<label><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>\n\
// 					&nbsp; Drop Address :  <div class="req-red">*</div></label>\n\
// 					<input type="text" class="drop_point" name="drop_address[]" placeholder="Drop Address">\n\
// 				</div>\n\
// 				<div class="clearfix"></div><br /></div></div>').insertBefore("#beforeCheck");*/

// 				if(len < 1)
// 				{
// 					$('<div id="multiDest"><br><div class="col-md-4"><label><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>&nbsp; Drop Address :  <div class="req-red">*</div></label><input type="text" class="drop_point" name="drop_address[]" placeholder="Drop Address"></div><div class="col-md-1 addNewDestination"><span onclick="testddd()"><i class="fa fa-plus-circle fa-2x"></i></span></div></div><br><br>').insertBefore("#beforeCheck")

// 						newInit_src();       			
// 				}



//     }
//     else
//     {
//     	$('#multiDest').remove();
//     }
// }


// function testddd()
// {

// 	var len = $('.dropLoc').length;
// 	console.log(len);

// 	if(len < 19)
// 	{
// 		$('#multiDest').append('<div class="dropLoc"><br>\n\
// 							<div class="col-md-4">\n\
// 				<label><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>\n\
// 					&nbsp; Drop Address :  <div class="req-red">*</div></label>\n\
// 					<input type="text" class="drop_point" name="drop_address[]" placeholder="Drop Address">\n\
// 				</div><div class="col-md-1 remove_drop"><span><i class="fa fa-minus-circle fa-2x"></i></span></div>\n\
// 				<div class="clearfix"></div><br /></div>');
// 		newInit_src();       	
// 	}
// 	else
// 	{
// 		alert('Only 20 Destinations are allowed');
// 	}

// }

// Sanket Code End

function arrangeSno()
{
	var i = 1;
	$('.dropLoc').each(function () {
		$(this).find(".sNo").html(i + ".");
		$("#txt_srcnt").val(i);
		i++;
	});
}


function checkPick()
{
	var pick = document.getElementById('pick_up_address').value;
	if(pick == "")
	{
		$(".error").html("Please enter Pick Up Address");
		return false;
	}else
	{
		$(".error").html("");
		return true;
	}
}

function checkDrop()
{
	var x = $("input[name='drop_address[]']");
	var t = '';
	$(x).each(function(key,val)
	{
		if($(val).val().length==0)
		{
			$(".error").html("Please enter Drop Address");
			t = 'false';
			return t;

		}else
		{
			$(".error").html("");
			t = 'true';
			return t;
		}
	});
	if(t == 'true')
	{
		return true;
	}else{
		return false;
	}
}

function checkPharmacy()
{
	var ride_type = '<?php echo $this->session->userdata('schedulaRideData')['sr_ride_type'] ?>';
	/*if(ride_type == "3")
	{
		var pharmacy = document.getElementById('pharmacy_address').value;	
		if(pharmacy == "")
		{
			$(".error").html("Please enter Pharmacy Address");
			return false;
		}else
		{
			$(".error").html("");
			return true;
		}
	}else
	{
		$(".error").html("");

		return true;
	}*/

	if(ride_type == "4")
	{
		var pharmacy = document.getElementById('pharmacy_address').value;	
		if(pharmacy == "")
		{
			$(".error").html("Please enter Pharmacy Address");
			return false;
		}else
		{
			$(".error").html("");
			return true;
		}
	}else
	{
		$(".error").html("");

		return true;
	}
}

// ------- get Route when click on get address buton -------------
var geocoder;
var map;
var directionsDisplay;
var directionsService;
var all_loc=[];
var flag=1;

function getRoute()
{	
	var ride_type = '<?php echo $this->session->userdata('schedulaRideData')['sr_ride_type'] ?>';
	all_loc=[];
	var pick = document.getElementById('pick_up_address').value;

	// ------- validations -------
	if(checkPick() && checkDrop() && checkPharmacy())
	{
		var locations=[];
		locations.push([pick]);
		var drop = document.getElementsByClassName('drop_point');
		for (var i = 0; i < drop.length; i++) {
			locations.push([drop[i].value]);
		}

		if(ride_type == "2" || ride_type == "4" || ride_type == "6")
		{
			var pharmacy = document.getElementById('pharmacy_address').value;	
			locations.push([pharmacy]);	

			if($('input[name=pharmacy_stop]:checked').val() == 'no')
			{

				var lastEl = locations.length-1;  // get the last element of array
				locations.push(locations[0]);  // for return ride add first location at last position of array
			}

			
		}
		/*if(ride_type == "3")
		{
			var pharmacy = document.getElementById('pharmacy_address').value;	
			locations.push([pharmacy]);	

			if($('input[name=pharmacy_stop]:checked').val() == 'no')
			{

				var lastEl = locations.length-1;  // get the last element of array
				locations.push(locations[0]);  // for return ride add first location at last position of array
			}

			
		}*/

		for (i = 0; i < locations.length; i++) 
		{
			geocodeAddress(locations,i);
		}
	}else
	{
		return false;
	}
}

function geocodeAddress(locations,i) 
{

	geocoder = new google.maps.Geocoder();
	var newData = [];
	// var address = locations[i][0];
	//console.log("all... "+locations[i][0]);
	geocoder.geocode({
		'address': locations[i][0]
	},
	

	function(results, status) {
		var latlong = results[0].geometry.location;
		// console.log("//// "+locations[i][0]);
		var res = latlong.toString().slice(1, -1);
		var str_array = res.split(',');
		TestRes(str_array[0],str_array[1],locations,i);
	});
}

function TestRes(lat,lng,locations,i)
{
	var data = [locations[i][0],lat,lng,i];		
	all_loc.push(data); 

	console.log('all Loc ==>'+all_loc);

	//return false;

	initialize(all_loc);
}

/*function initialize(locations) 
{

	
	directionsDisplay = new google.maps.DirectionsRenderer();
	directionsService = new google.maps.DirectionsService();

	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 10,
		center: new google.maps.LatLng(18.559003, 73.786763),
		mapTypeId: google.maps.MapTypeId.ROADMAP
	});
	directionsDisplay.setMap(map);

	var infowindow = new google.maps.InfoWindow();

	var marker, i;
	var request = {
		travelMode: google.maps.TravelMode.DRIVING
	};
	for (i = 0; i < locations.length; i++) {

		marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
		});

		google.maps.event.addListener(marker, 'click', (function(marker, i) {
			return function() {
				infowindow.setContent(locations[i][0]);
				infowindow.open(map, marker);
			}
		})(marker, i));

		if (i == 0) request.origin = marker.getPosition();
		else if (i == locations.length - 1) request.destination = marker.getPosition();
		else {
			if (!request.waypoints) request.waypoints = [];
			request.waypoints.push({
				location: marker.getPosition(),
				stopover: true
			});
		}

	}
	directionsService.route(request, function(result, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(result);
		}
	});
}*/

function initialize(locations) 
{
	var infowindow = new google.maps.InfoWindow();
	var marker, i;
	var request = {
		travelMode: google.maps.TravelMode.DRIVING
	};
	//----- for dotted line ------
	 // var lineSymbol = {
  //       path: google.maps.SymbolPath.CIRCLE,
  //       fillOpacity: 1,
  //       scale: 3
  //   };

  var lineSymbol = {
  	path: 'M 0,-1 0,1',
  	strokeOpacity: 1,
  	scale: 4
  };
  var polylineDotted = {
  	strokeColor: '#000000',
  	strokeOpacity: 0,
  	fillOpacity: 0,
  	icons: [{
  		icon: lineSymbol,
  		offset: '0',
  		repeat: '15px'
  	}],
  };
  var rendererOptions = {
  	map: map,
  	suppressMarkers: false,
  	polylineOptions: polylineDotted
  };
    //----- for dotted line end ------

    directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
    directionsService = new google.maps.DirectionsService();

    var map = new google.maps.Map(document.getElementById('map'), {
    	zoom: 10,
    	center: new google.maps.LatLng(18.559003, 73.786763),
    	mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    directionsDisplay.setMap(map);


    for (i = 0; i < locations.length; i++) {
    	marker = new google.maps.Marker({
    		position: new google.maps.LatLng(locations[i][1], locations[i][2]),
    	});

    	google.maps.event.addListener(marker, 'click', (function(marker, i) {
    		return function() {
    			infowindow.setContent(locations[i][0]);
    			infowindow.open(map, marker);
    		}
    	})(marker, i));

    	if (i == 0) request.origin = marker.getPosition();
    	else if (i == locations.length - 1) request.destination = marker.getPosition();
    	else {
    		if (!request.waypoints) request.waypoints = [];
    		request.waypoints.push({
    			location: marker.getPosition(),
    			stopover: true
    		});
    	}

    }
    directionsService.route(request, function(result, status) {
    	if (status == google.maps.DirectionsStatus.OK) {
    		directionsDisplay.setDirections(result);
    	}
    });
}
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY ?>&v=3.exp&sensor=false&libraries=places"></script>
<?php $this->load->view("user/footer"); ?>