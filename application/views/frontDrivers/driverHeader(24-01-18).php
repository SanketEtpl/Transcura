<?php  if($this->session->userdata('user_id')) { ?>
<div class="inner_dashboard">
	<div class="inner_img" style="background-image:url('<?php echo base_url(); ?>assets/img/inner_banner.png');"></div>
	<div class="inner_top_wrapp">
		<div class="inner_head">
			<?php
				$getdata = $this->uri->segment(1);
				$explode = explode('-',$getdata);
				foreach ($explode as $key => $value) {
					echo strtoupper($value).' ';
				}			
				$user_id =$this->session->userdata('user_id');				
				$notifFlag = $this->db->query("SELECT count( * ) AS notification FROM `tbl_notifications` WHERE user_id =$user_id AND `notification_flag` = '2'");
				$totalNotif1 = $notifFlag->result_array();					   	 				
			?>
		</div>
	</div>
</div>
<div class="inner_menu_section">
	<div class="container">
		<div class="row">
			<div class="innermenus">
				<button class="btn btn-navbar" data-toggle="collapse" data-target="#innermenus">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<div class="dash_inn_menu" id="innermenus">
				<ul>	<?php //print_r(count($totalNotif[0]['notification']));?>									
					<li><a href="<?php echo base_url()?>my-dashboard" <?php if($getdata =="my-dashboard" || $getdata =="view-map"){ echo $class = 'class="act_inn_menu"'; } ?>>My Dashboard</a></li>
					<li><a href="<?php echo base_url()?>my-profile" <?php if($getdata =="my-profile"){ echo $class = 'class="act_inn_menu"'; } ?>>My Profile</a></li>
					<li><a href="<?php echo base_url()?>schedule-a-ride" <?php if($getdata =="schedule-a-ride"){ echo $class = 'class="act_inn_menu"'; } ?>>Schedule A Ride</a></li>
<li><a href="<?php echo base_url()?>request-ride" <?php if($getdata =="request-ride"){ echo $class = 'class="act_inn_menu"'; } ?>>Request Ride</a></li>
					<!--<li><a href="<?php echo base_url()?>return-schedule-a-ride" <?php if($getdata =="return-schedule-a-ride"){ echo $class = 'class="act_inn_menu"'; } ?>>Request to Return Rides</a></li>-->
					<li><a href="<?php echo base_url()?>my-rides/schedule-ride" <?php if($getdata =="my-rides"){ echo $class = 'class="act_inn_menu"'; } ?>>My Rides</a></li>
					<li><a href="<?php echo base_url()?>notification" <?php if($getdata =="notification"){ echo $class = 'class="act_inn_menu"'; } ?> id="count_notification">Notifications 

				<?php if($totalNotif1[0]['notification']!=0)

			{     echo "<span class='not_cnt'>".$totalNotif1[0]['notification']."</span>";} ?></a></li>
				</ul>	
			</div>
		</div>
	</div>
</div>
<?php } else { echo "<h3>No direct script access allowed</h3>"; }?>
