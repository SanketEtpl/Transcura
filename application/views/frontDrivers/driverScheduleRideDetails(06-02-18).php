<?php $this->load->view("user/header"); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<div style="float:left;width:100%;padding-top: 10px;">
<div class="container">
	<div class="row" style="padding-top:10px">
		<div class="responsive">
			<div class="">
			<table border='1' class="table">  	
			    <thead>
			      <tr>
			        <th>Trip id</th>
			        <th>Pick up point</th>
			        <th>Drop point</th>
			        <th>Distance</th>
			        <th>Duration</th>
			        <th>Status</th>
			      </tr>
			    </thead>
			    <tbody>
			    	<?php    	
						foreach ($results as $key => $value) {	
			 		?>
			      <tr>
			        <td><?php echo $value['sr_trip_id']; ?></td>
			        <td><?php echo $value['source_address']; ?></td>
			        <td><?php echo $value['destination_address']; ?></td>
			        <td><?php echo $value['sr_total_distance']; ?></td>
			        <td><?php echo $value['duration']; ?></td>  
			        <td>
			        	<?php 
			        		if($value['driver_status'] == 1)
			        			echo "Open ride";
						else if($value['driver_status'] == 2)
							echo "Acceted ride";
			        		else
			        			echo "Closed ride";
			        	?>
			        </td> 
			      </tr>

				<?php  } ?>
			    </tbody>
			</table>
			<ul class="pagination">
		    	<li><?php echo $links; ?></li>			  
		  	</ul>
		</div>
		</div>
	</div>
</div>
</div>

<?php $this->load->view("user/footer"); ?>

