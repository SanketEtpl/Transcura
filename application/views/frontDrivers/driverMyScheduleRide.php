<?php $this->load->view("user/header"); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<div class="driver_sched_strt">
	<div class="container">
		<div class="row">
			<div class="driver_strt_inn">
				<h3 class="heal_head">Schedule A Trip</h3>
				<div class="dri_main">
					<div class="col-md-6">						
						<?php if($this->session->userdata('user_id')){ ?>				
						<div class="rightform">
							<div class="sch_log">								
								<form name="schedule_ride" action="<?php echo base_url(); ?>schedule-a-ride" method="POST" autocomplete="off">
									<div class="row col_padding">	
										<?php
										$success = $this->session->flashdata("success");
										$fail = $this->session->flashdata('fail');
										$timestamp =  strtotime(date('h:i a')) - 60*60;
										$returnTime =  date('h:i a',strtotime(date('h:i a')) + 60*60);
										$pickUpTIme = date('h:i a', $timestamp);
										if($success) { ?>
										<div class="alert alert-success">
											<?php echo $success; ?>
										</div>
										<?php
									}
									if($fail){ ?>
									<div class="alert alert-danger">
										<?php echo $fail; ?>
									</div>
									<?php } ?>
									<!-- <div class="col-md-12 col_padding">
										<div class="top-form text-left">


												<div class="form-group">
													<label class="control-label col-sm-6 pdlr-0" for="datepicker1"><span class="glyphicon glyphicon-calendar"></span>&nbsp;Appointment Date
														<div class="req-red">*</div></label>
														<div class="col-sm-6 pdlr-0">
															<div class="input-group">
																<input type="text" class="form-control" id="datepicker1" value="<?php  echo date("Y-m-d"); ?>" name="date" placeholder="<?php echo date("m-d-Y");?>" readonly>													
															</div>
															<?php //echo form_error('date', '<div class="error">', '</div>'); ?>															
														</div>
													</div>
													<div class="form-group">
														<label class="control-label col-sm-6 pdlr-0" for="aptime"><span class="glyphicon glyphicon-time"></span>&nbsp;Appointment Time <div class="req-red">*</div></label>
														<div class="col-sm-6">
															<div class="input-group">
																<input type="text" name="appointment_time" id="aptime" value="<?php echo date("h:i a"); ?>" class="timepicker form-control" placeholder="<?php echo date("h:i a");?>" readonly/>												
															</div>		
															<?php //echo form_error('appointment_time', '<div class="error">', '</div>'); ?>													
														</div>
													</div>													
													<div class="form-group">
														<label class="control-label col-sm-6 pdlr-0" for="pick-up-time"><span class="glyphicon glyphicon-time"></span>&nbsp;Pick a Time <div class="req-red">*</div></label>
														<div class="col-sm-6">
															<div class="input-group">
																<input type="text" name="pick_up_time" id="timepicker" value="<?php echo $pickUpTIme; ?>" class="timepicker form-control" placeholder="<?php echo $pickUpTIme;?>" readonly/>												
															</div>		
															<?php //echo form_error('pick_up_time', '<div class="error">', '</div>'); ?>													
														</div>
													</div>														
													<div class="form-group pickaddress">
														<label class="control-label col-sm-12 pdlr-0" for="pick-up-address"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>&nbsp;Pick Up Address</label>
														<div class="col-sm-12">
															<div class="input-group">
																<label class="control-label col-sm-6">country <div class="req-red">*</div></label>
																<label><?php //echo $countryData[0]['country_name']; ?></label>
																<input type="hidden" id="country" name="country" value="<?php echo $countryData[0]['id']; ?>">										
															</div>															
														</div>	
														<div class="col-sm-12">
															<div class="input-group">
																<label class="control-label col-sm-6">State <div class="req-red">*</div></label>
																<div class="col-sm-6">
																	<select class="form-control" id="state" name="state">
																		<option class="selecttxt" value="" disable selected hidden>Select State</option>																			
																	</select>	
																	<?php //echo form_error('state', '<div class="error">', '</div>'); ?>
																</div>

															</div>															
														</div>	
														<div class="col-sm-12">
															<div class="input-group">
																<label class="control-label col-sm-6">Street <div class="req-red">*</div></label>
																<div class="col-sm-6">
																	<input type="text" name="pick_up_street" id="street" value="<?php echo set_value('pick_up_street'); ?>" maxlength="100" class="form-control input-sm" placeholder="Enter Street"/>						<?php echo form_error('pick_up_street', '<div class="error">', '</div>'); ?>						
																</div>

															</div>															
														</div>
														<div class="col-sm-12">
															<div class="input-group">
																<label class="control-label col-sm-6">Zipcode <div class="req-red">*</div></label>
																<div class="col-sm-6">
																	<input type="text" id="zipcode" name="zipcode" value="<?php echo set_value('zipcode'); ?>" class="form-control input-sm" maxlength="10" placeholder="Enter Zipcode"/>										<?php echo form_error('zipcode', '<div class="error">', '</div>'); ?>	
																</div>

															</div>															
														</div>														
													</div>
												</div>
											</div> -->
											<!--form-->

											<div class="new-frm">
											<div class="col-sm-12"><h4>Trip Details</h4></div>

												<div class="form-group">
													<label for="datepicker1" class="text-uppercase"><span class="glyphicon glyphicon-calendar"></span>&nbsp;Appointment Date <div class="req-red">*</div></label>
													<input type="text" class="form-control" id="datepicker1" value="<?php  echo date("Y-m-d"); ?>" name="date" placeholder="<?php echo date("m-d-Y");?>" readonly>													
													<?php echo form_error('date', '<div class="error">', '</div>'); ?>
												</div>

												<?php
												$drop_cnt = sizeof($this->session->userdata('schedulaRideData')['drop_address']);

												$cnt=1;
												for($i=0;$i<$drop_cnt;$i++)
													{ ?>
												<div class="form-group">
													<label for="aptime" class="text-uppercase"><span class="glyphicon glyphicon-time"></span>&nbsp;Appointment Time <?php echo $cnt; ?> <div class="req-red">*</div></label>
													<input type="text" name="appointment_time[]" id="aptime" class="timepicker form-control selectTime" placeholder="<?php echo date("h:i a");?>" readonly required="" />												
													<?php echo form_error('appointment_time', '<div class="error">', '</div>'); ?>
												</div>

												<?php 	
												$cnt++;
											}
											?>


											<div class="form-group">
												<label for="equipment" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Required Equipment </label>
												<select class="form-control" id="equipment" name="equipment">
													<option value="" disable selected hidden>Select Equipment</option>	
													<?php
													if(!empty($equipment)) {
														foreach($equipment as $key => $vequip) { ?>
														<option value="<?php echo $vequip['eq_id']; ?>"<?php echo set_select('equipment', $vequip['eq_id'], FALSE); ?>><?php echo $vequip['equipment']; ?></option>
														<?php } } else { ?>
														<option value=""></option>
														<?php } ?>													
													</select>
													<?php //echo form_error('equipment', '<div class="error">', '</div>'); ?>
												</div>

												<!--element-->
												<div class="form-group">
													<label for="usertype" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Desired Healthcare Services </label>
													<select class="form-control" name="health_service" id="health_service">
														<option value="" disable selected hidden>Select</option>
														<?php
														if(!empty($hc_service)) {
															foreach($hc_service as $key => $value) { ?>
															<option value="<?php echo $value['id']; ?>"<?php echo set_select('health_service', $value['id'], FALSE); ?>><?php echo $value['hs_name']; ?></option>
															<?php } } else { ?>
															<option value=""></option>
															<?php } ?>												
														</select>
														<?php //echo form_error('health_service', '<div class="error">', '</div>'); ?>
													</div>	
													<!--element-->
													<div class="form-group">
														<label for="usertype" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Special Requests </label>
														<select class="form-control" id="special_request" name="special_request">
															<option value="" disable selected hidden>Select</option>
															<?php 
															if(!empty($sp_request)) {	
																foreach($sp_request as $key => $value) { ?>
																<option value="<?php echo $value['id']; ?>"<?php echo set_select('special_request', $value['id'], FALSE); ?>><?php echo $value['sr_name']; ?></option>
																<?php } } else { ?>	
																<option value=""></option>
																<?php } ?>										
															</select>
															<?php //echo form_error('special_request', '<div class="error">', '</div>'); ?>
														</div> 	
														<!--element-->
														<div class="form-group">
															<label for="usertype" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Special Instructions </label>
															<input type="text" name="special_instruction" class="form-control" id="special_instruction" value="<?php echo set_value('special_instruction'); ?>" maxlength="50">
															<?php //echo form_error('special_instruction', '<div class="error">', '</div>'); ?>
														</div> 
														<!--element-->
														<div class="form-group">
															<label for="usertype" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Desired Transportation Services </label>
															<select class="form-control" id="desire_transp" name="desire_transp">
																<option value="" disable selected hidden>Select</option>
																<?php
																if(!empty($desire_tran)) {
																	foreach($desire_tran as $key => $value) { ?>
																	<option value="<?php echo $value['id']; ?>"<?php echo set_select('desire_transp', $value['id'], FALSE); ?>><?php echo $value['dt_name']; ?></option>
																	<?php } } else { ?>	
																	<option value=""></option>
																	<?php } ?>	
																</select>
																<?php //echo form_error('desire_transp', '<div class="error">', '</div>'); ?>
															</div>

															<!-- <div class="form-group">
																<label for="lblUrgeType" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Urgency Type<div class="req-red">*</div></label>
																<select class="form-control" id="urgency_type" name="urgency_type">
																	<option value="" disable selected hidden>Select</option>
																	<option value="1" <?php echo set_select('urgency_type', 1, FALSE); ?>>Urgent</option>
																	<option value="2" <?php echo set_select('urgency_type', 2, FALSE); ?>>Normal (Within 72 hours)</option>
																</select>
																<?php echo form_error('urgency_type', '<div class="error">', '</div>'); ?>
															</div> -->

															<!-- <div class="form-group1" id="show_urgnt">
																<label for="usertype" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Explanation</label>
																<input type="text" name="urgency_exp" class="form-control name" id="urgency_exp" value="<?php echo set_value('urgency_exp'); ?>" maxlength="50">
																<?php echo form_error('urgency_exp', '<div class="error">', '</div>'); ?>
															</div> -->
															<!--element-->
															<!-- <div class="form-group">
																<label for="lblrestreturn" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Request return<div class="req-red">*</div></label>
																<select class="form-control" id="req_again_ride" name="req_again_ride">
																	<option value="" disable selected hidden>Select</option>
																	<option value="2" <?php echo set_select('req_again_ride', 2, FALSE); ?>>Yes</option>
																	<option value="1" <?php echo set_select('req_again_ride', 1, FALSE); ?>>No</option>
																</select>
																<?php echo form_error('req_again_ride', '<div class="error">', '</div>'); ?>
															</div> -->		
															<!-- <div class="form-group1" id="return_time1">
																<label for="lbltime" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Return time</label>
																<input type="text" name="return_time" id="return_time" value="<?php echo $returnTime; ?>" class="timepicker form-control" placeholder="<?php echo $returnTime;?>" readonly/>												
																<?php echo form_error('return_time', '<div class="error">', '</div>'); ?>
															</div> -->											
															<div class="proc_btn">
																<input type="submit" value="PROCEED" class="btn btn-block btn-primary" id="btnDisable"/>
															</div>														
														</div>
													</div>
												</form>										
											</div>
										</div>
									</div>
									<?php } ?>	
									<div class="col-md-6">
										<div class="sch_img" style="background-image:url('<?php echo base_url(); ?>assets/img/sch_car.png');"></div>
									</div>		
								</div>					
							</div>
						</div>
					</div>
				</div>
			</div>	
			<?php $this->load->view("user/footer"); ?>