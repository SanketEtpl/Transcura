<?php $this->load->view("user/header"); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<div class="dri_comp_wrapp">
	<div class="container">
		<div class="row">
			<?php //$this->load->view('frontDrivers/inner_tabs'); ?>	
			<div class="dri_inner">
				<h3 class="heal_head">
					Driver  Details
					<span class="back_box"><button class="back_btn" id="driver_details_bk" onClick="window.location.reload();" name="driver_details_bk">Back</button></span>
				</h3>				
			</div>
		</div>
	</div>
	<div class="dri_det_sec">
		<div class="probgimg" style="background-image:url('<?php echo base_url(); ?>assets/img/profile_bg.png');"></div>
		<div class="dri_box_wrapp">
			<div class="container">
				<div class="row">
					<div class="tp_pro_sect">
						<div class="pro_img_box" style="background-image:url('<?php echo base_url(); ?>assets/img/pro_pic1.png');">
							<span class="edit_icon"><a href="#"><i class="fa fa-pencil" aria-hidden="true"></i></a></span>
						</div>
						<div class="pro_img_name">
							<div class="pro_name_box">
								<div class="pro_nme">David J. Barb</div>
							</div>
						</div>
					</div>
					<div class="dri_bsc_wrap">
						<div class="info-basic">
							<h2 class="dri_bsc_name">Basic Information :</h2>
							<div class="row">
								<div class="col-md-4 info_sec">
									<label class="pick_lab"><i class="fa fa-car" aria-hidden="true"></i>Type of vehicle :</label>
									<span class="pick_info">Ambulance</span>
								</div>
								<div class="col-md-4 info_sec">
									<label class="pick_lab"><i class="fa fa-mobile" aria-hidden="true"></i>Mobile Number :</label>
									<span class="pick_info">9123 456 789</span>
								</div>
								<div class="col-md-4 info_sec">
									<label class="pick_lab"><i class="fa fa-star" aria-hidden="true"></i>Rate :</label>
									<span class="pick_info">Coming Soon rating Star</span>
								</div>
							</div>
						</div>
						<div class="info-basic">
							<h2 class="dri_bsc_name">Driver Location Map :</h2>
							<div class="driver_deta_map">
								<!--<img src="<?php echo base_url(); ?>assets/img/driv_route.png"/>-->
								  <div id="googleMap">
									<iframe src="https://www.google.com/maps/embed/v1/place?q=3935%20Avoin%20Park%20Ct.Suite%20A-108%20Chantilly%2CVA%2020151&key=AIzaSyAO9q2RgUAWQAbrwW_Z716Py56Lh68s0ZM" frameborder="0"></iframe> 
								  </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
	</div>
	
</div>	
<?php $this->load->view("user/footer"); ?>