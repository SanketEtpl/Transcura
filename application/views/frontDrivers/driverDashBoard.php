<?php 
	$total = 0;
	if(!empty($myRideCount[0]) && !empty($myRideCount[1])) 
	{
		$total =$myRideCount[0]['total'] + $myRideCount[1]['total']; 
	}
?>
<?php $this->load->view("user/header"); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>

<div class="inner_body_wrapp">
	<div class="container">
		<div class="row">
			<div class="ts_dash_wrap">
				<div class="top_dash_sec">
					<div class="col-md-6 lt_prof">
						<div class="pro_wrap">
							<div class="prof_cir"><img src="<?php if(!empty($userDetails[0])){ if(!empty($userDetails[0]['picture'])){ echo base_url().$userDetails[0]['picture'];}else{ echo base_url()."assets/img/no_img.png"; } } ?>" /></div>
							<div class="prof_info">
								<div class="mypro_head">My Profile</div>
								<div class="mypro_name">
									<?php if(!empty($userDetails[0])){ echo $userDetails[0]['full_name']; }?></br>
									<span><?php if(!empty($userDetails[0])){ echo $userDetails[0]['email']; }?></span>
								</div>
								<div class="view_pro_btn"><a href="<?php echo base_url();?>my-profile">View</a></div>
							</div>
						</div>
					</div>
					<div class="col-md-6 rt_dri">
						<div class="driv_map">
							<div class="driv_sec" style="background-image:url('<?php echo base_url(); ?>assets/img/driver_map.png');">
								<div class="driv_rute">Driver in Route.</div>
								<div class="view_map_btn"><a href="#">View Map</a></div>	
							</div>							
						</div>
					</div>
				</div>
			</div>
			<div class="my_rides_sec">
				<div class="inn_tit_wrap">
					<h1 class="inn_head">My Rides <span>My Rides</span></h1>
				</div>
				<div class="rides_box">
					<div class="col-md-4">
						<div class="inn_rides">
							<div class="ltrid_img">
								<img src="<?php echo base_url(); ?>assets/img/rides_box.png" />
								<span class="rid_cnt"><?php echo $total; ?></span>
							</div>
							<div class="tot_wrapp">
								<div class="tot_name">Total</br>Scheduled Ride</div>
								<div class="view_rides_btn"><a href="<?php echo base_url();?>my-rides/schedule-ride">View</a></div>	
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="inn_rides">
							<div class="ltrid_img">
								<img src="<?php echo base_url(); ?>assets/img/rides_box.png" />
								<span class="rid_cnt"><?php if(!empty($myRideCount[0])){ echo $myRideCount[0]['total']; }else{ echo "0"; }?></span>
							</div>
							<div class="tot_wrapp">
								<div class="tot_name">Total</br>Completed Ride</div>
								<div class="view_rides_btn"><a href="<?php echo base_url();?>my-rides/schedule-completed">View</a></div>	
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="inn_rides">
							<div class="ltrid_img">
								<img src="<?php echo base_url(); ?>assets/img/rides_box.png" />
								<span class="rid_cnt"><?php if(!empty($myRideCount[1])){ echo $myRideCount[1]['total']; }else{ echo "0"; }?></span>
							</div>
							<div class="tot_wrapp">
								<div class="tot_name">Total</br>Cancelled Ride</div>
								<div class="view_rides_btn"><a href="<?php echo base_url();?>my-rides/schedule-cancelled">View</a></div>	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("user/footer"); ?>