<?php  if($this->session->userdata('user_id')) { ?>
<div class="inner_dashboard">
	<div class="inner_img" style="background-image:url('<?php echo base_url(); ?>assets/img/inner_banner.png');"></div>
	<div class="inner_top_wrapp">
		<div class="inner_head">
			<?php
				$getdata = $this->uri->segment(1);
				$explode = explode('-',$getdata);
				foreach ($explode as $key => $value) {
					echo strtoupper($value).' ';
				}			
				$user_id =$this->session->userdata('user_id');				
				$notifFlag = $this->db->query("SELECT count( * ) AS notification FROM `tbl_notifications` WHERE user_id =$user_id AND `notification_flag` = '1'");
				$totalNotif = $notifFlag->result_array();							   	 				
			?>
		</div>
	</div>
</div>
<div class="inner_menu_section">
	<div class="container">
		<div class="row">
			<div class="dash_inn_menu">
				<ul>										
					<li><a href="<?php echo base_url()?>my-dashboard" <?php if($getdata =="my-dashboard"){ echo $class = 'class="act_inn_menu"'; } ?>>My Dashboard</a></li>
					<li><a href="<?php echo base_url()?>my-profile" <?php if($getdata =="my-profile"){ echo $class = 'class="act_inn_menu"'; } ?>>My Profile</a></li>
					<li><a href="<?php echo base_url()?>schedule-a-ride" <?php if($getdata =="schedule-a-ride"){ echo $class = 'class="act_inn_menu"'; } ?>>Schedule A Ride</a></li>
					<li><a href="#">Request to Return Rides</a></li>
					<li><a href="<?php echo base_url()?>my-rides/schedule-ride" <?php if($getdata =="my-rides"){ echo $class = 'class="act_inn_menu"'; } ?>>My Rides</a></li>
					<li><a href="<?php echo base_url()?>notification" <?php if($getdata =="notification"){ echo $class = 'class="act_inn_menu"'; } ?>>Notifications <span class="not_cnt"><?php if(count($totalNotif[0]['notification'])>0){ echo count($totalNotif[0]['notification']);}  ?></span></a></li>
				</ul>	
			</div>
		</div>
	</div>
</div>
<?php } else { echo "<h3>No direct script access allowed</h3>"; }?>