<?php $this->load->view("user/header"); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<div class="all_comp_wrapp">
	<div class="container">
		<div class="row">
			<?php $this->load->view('frontDrivers/inner_tabs'); ?>
			<div class="comp_inner">
				<h3 class="heal_head">
					Complaint
					<div id="error_message"></div>
					<span class="back_box"><button class="back_btn customer_complaints" id="customer_complaints_bk" name="customer_complaints_bk">Back</button></span>
				</h3>
				<div class="comp_box">
					<form name="customer_complaint_form" method="POST" action="" autocomplete="off">
						<div class="row">
							<div class="col-md-6 comp_lab">
								<label>User Name :</label>
								<div class="comp_inpt">
									<input placeholder="User Name" name="user_name" maxlength="100" id="user_name" value="<?php echo set_value('user_name'); ?>" class="form-control" type="text">
								</div>								
							</div>
							<div class="col-md-6 comp_lab">
								<label>Date Of Service :</label>
								<div class="comp_inpt">
									<input placeholder="DD-MM-YYYY" name="date_of_service" id="date_of_service" value="<?php echo set_value('date_of_service'); ?>" class="form-control" type="text">
								</div>								
							</div>
							<div class="col-md-6 comp_lab">
								<label>Drivers Name :</label>
								<div class="comp_inpt">
									<input placeholder="Drivers Name" name="driver_name" maxlength="100" id="driver_name" value="<?php echo set_value('driver_name'); ?>" class="form-control" type="text">
								</div>								
							</div>
							<div class="col-md-6 comp_lab">
								<label>Name Of Transportation Provider :</label>
								<div class="comp_inpt">
									<input placeholder="Name Of Transportation Provider" name="name_of_transport_prov" id="name_of_transport_prov" value="<?php echo set_value('name_of_transport_prov'); ?>" class="form-control" maxlength="100" type="text">
								</div>								
							</div>
							<div class="col-md-6 comp_lab">
								<label>Complaint Type :</label>
								<div class="comp_inpt">
									<select class="form-control" name="complaint_type" id="complaint_type">
										<option value="" class="selecttxt" disable selected hidden>Select type</option>
										<?php 
											if(!empty($complainData)){ 
											foreach($complainData as $complaint_name) {
										?>
										<option value="<?php echo $complaint_name['id']; ?>"<?php echo set_select('complaint_type',$complaint_name['id'],FALSE);?>><?php echo $complaint_name['ct_name']?></option>
										<?php } }else { ?>
										<option value=""></option>
										<?php } ?>

									</select>
								</div>								
							</div>
							<div class="col-md-6 comp_lab">
								<label>Description :</label>
								<div class="comp_inpt">
									<textarea placeholder="Description" name="description" maxlength="255" id="description" class="form-control"><?php echo set_value('description'); ?></textarea>
								</div>								
							</div>
						</div>	
					</form>	
					<div class="pay_btn_box">
						<button type="button" class="req_btn cancel_btn customer_complaints" id="customer_complaints_cancel" data-toggle="modal" data-target="#succpay_pop">Cancel</button>
						<button type="button" class="req_btn comp_btn" id="btn_customer_complaint" data-toggle="modal">Submit Complaint</button>	<!-- data-target="#subcomp_pop" -->				
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
<!--submit complaints popup-->
<div id="subcomp_pop" class="modal fade in" role="dialog">
  <div class="modal-dialog">    
    <div class="modal-content sma_pop">
      <div class="modal-header">
        <button type="button" class="close complaint_ok" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>       
      </div>
      <div class="modal-body">
		<div class="succ_icon"><img src="<?php echo base_url(); ?>assets/img/succpay_icon.png"></div>
			<h4 class="succ_tit">Successful !!!</h4>
			<h2 class="read_trip">We apologize for the inconvenience;</br>your complaint has been</br>successfully submitted</h2>	
		</div>
      <div class="modal-footer ok_btn">
			<button type="button" id="complaint_ok" class="btn btn-default cont_btn complaint_ok" data-dismiss="modal">OK</button>			
      </div>
    </div>
  </div>
</div>
<?php $this->load->view("user/footer"); ?>