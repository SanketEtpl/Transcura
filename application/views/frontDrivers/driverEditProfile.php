<?php $this->load->view("user/header"); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<div class="myprof_wrapp">
	<div class="probgimg" style="background-image:url('<?php echo base_url(); ?>assets/img/profile_bg.png');"></div>
	<div class="mypro_info_wrapp edit_pro_wrapp">
		<div class="container">
			<div class="row">
				 
				<form name="edit_profile" action="<?php echo base_url(); ?>update-driver-edit-profile" method="POST" autocomplete="off" enctype="multipart/form-data">
					<div class="tp_pro_sect">
						<div class="pro_img_box image-upload" id="preview_image" style="background-image:url('<?php if(!empty($getDriverDetails[0])){ if(!empty($getDriverDetails[0]['picture'])){ echo base_url().$getDriverDetails[0]['picture']; }else{ echo base_url()."assets/img/no_img.png"; } } ?>');">						
							<label class="edit_icon" for="file-input">
                                <i class="fa fa-pencil" aria-hidden="true"></i>	
                            </label>							
							<input type="hidden" id="profile_image_old" name="profile_image_old" value="<?php if(!empty($getDriverDetails[0])){ echo $getDriverDetails[0]['picture']; } ?>">
							<input id="file-input" name="profile_image" type="file" />
						</div>
						<div class="pro_img_name">
							<div class="pro_name_box">
								<div class="pro_nme_input"><input type="text" placeholder="Stevie Mcmenamin"  maxlength="50" name="full_name" id="fullname" value="<?php if(!empty($getDriverDetails[0])){ echo $getDriverDetails[0]['full_name']; } ?>" class="form-control"></div>
								<?php echo form_error('full_name', '<div class="error">', '</div>'); ?>
								<div class="edit_prof_btn"><a href="<?php echo base_url();?>my-profile">View Profile</a></div>
							</div>
						</div>
					</div>
					<div class="basic_info_wrapp">
						<div class="row">
							<div class="col-md-6">
								<div class="info_head">Basic Information :</div>
								<div class="basic_box">
									<div class="row">
										<div class="col-md-6 edit_box">
											<label><img src="<?php echo base_url(); ?>assets/img/date_icon.png">Date of Birth:</label>	
											<div class="edit_inpt"><input type="text" placeholder="Date of birth" name="date_of_birth" id="date_of_birth" value="<?php if(!empty($getDriverDetails[0])){ echo $getDriverDetails[0]['date_of_birth']; } ?>" class="form-control date-picker"></div>
											<?php echo form_error('date_of_birth', '<div class="error">', '</div>'); ?>
										</div>
										<div class="col-md-6 edit_box">
											<label><img src="<?php echo base_url(); ?>assets/img/mob_no.png">Mobile Number:</label>	
											<div class="edit_inpt"><input type="text" placeholder="Mobile number" name="phone_no" id="phnnumber" maxlength="20" value="<?php if(!empty($getDriverDetails[0])){ echo $getDriverDetails[0]['phone']; } ?>" class="form-control"></div>
											<?php echo form_error('phone_no', '<div class="error">', '</div>'); ?>
										</div>
										<div class="col-md-6 edit_box">
											<label><img src="<?php echo base_url(); ?>assets/img/email_add.png">Email Address:</label>	
											<div class="edit_inpt"><input type="text" placeholder="Email ID" name="email" id="emailaddrss" maxlength="50" value="<?php if(!empty($getDriverDetails[0])){ echo $getDriverDetails[0]['email']; } ?>" class="form-control"></div>
											<?php echo form_error('email', '<div class="error">', '</div>'); ?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="info_head">Address :</div>
								<div class="basic_box">
									<div class="row">
										<div class="col-md-6 edit_box">
											<label><img src="<?php echo base_url(); ?>assets/img/coun.png">Country:</label>	
											<div class="edit_inpt">
												<select class="form-control" name="country" id="country">
													<option value="" class="selecttxt" disable selected hidden>Select Country</option>
													<?php 
													if(!empty($countryData)){
													foreach($countryData as $country_name) {
													?>
													<option value="<?php echo $country_name['id']; ?>" <?php if($getDriverDetails[0]['country'] == $country_name['id']){ echo "selected"; } ?> ><?php echo $country_name['country_name']?></option>
													<?php } } else { ?>	
													<option value=""></option>
													<?php } ?>
												</select>
											</div>
											<?php echo form_error('country', '<div class="error">', '</div>'); ?>
										</div>
										<div class="col-md-6 edit_box">
											<label><img src="<?php echo base_url(); ?>assets/img/state.png">State:</label>	
											<div class="edit_inpt">
												<select class="form-control" name="state" id="state">
													<option value="" class="selecttxt" disable selected hidden>Select State</option>
													<?php 
													if(!empty($stateData)){
													foreach($stateData as $state_name) {
													?>
													<option value="<?php echo $state_name['id']; ?>" <?php if($getDriverDetails[0]['state'] == $state_name['id']){ echo "selected"; } ?> ><?php echo $state_name['state_name']?></option>
													<?php } } else { ?>	
													<option value=""></option>
													<?php } ?>
												</select>
											</div>
											<?php echo form_error('state', '<div class="error">', '</div>'); ?>
										</div>
										<div class="col-md-6 edit_box">
											<label><img src="<?php echo base_url(); ?>assets/img/state.png">City:</label>	
											<div class="edit_inpt">
												<select class="form-control" name="city" id="city">												
													<option class="selecttxt" value="" disable selected hidden>Select City</option>
													<?php 
													if(!empty($cityData)){
													foreach($cityData as $city_name) {
													?>
													<option value="<?php echo $city_name['id']; ?>" <?php if($getDriverDetails[0]['city'] == $city_name['id']){ echo "selected"; } ?> ><?php echo $city_name['city_name']?></option>
													<?php } } else { ?>	
													<option value=""></option>
													<?php } ?>
												</select>
											</div>
											<?php echo form_error('city', '<div class="error">', '</div>'); ?>
										</div>
										<div class="col-md-6 edit_box">
											<div class="row">
												<div class="col-md-6">
													<label><img src="<?php echo base_url(); ?>assets/img/zip.png">Zip Code:</label>	
													<div class="edit_inpt">
														<input type="text" placeholder="Zipcode" maxlength="15" name="zipcode" id="zipcode" value="<?php if(!empty($getDriverDetails[0])){ echo $getDriverDetails[0]['zipcode']; } ?>" class="form-control">
													</div>
													<?php echo form_error('zipcode', '<div class="error">', '</div>'); ?>
												</div>
												<div class="col-md-6">
													<label><img src="<?php echo base_url(); ?>assets/img/zip.png">County:</label>	
													<div class="edit_inpt">
														<input type="text" placeholder="County" maxlength="50" name="county" id="county" value="<?php if(!empty($getDriverDetails[0])){ echo $getDriverDetails[0]['county']; } ?>" class="form-control">
													</div>
													<?php echo form_error('county', '<div class="error">', '</div>'); ?>
												</div>
											</div>
										</div>
										<div class="col-md-12 edit_box">
											<label><img src="<?php echo base_url(); ?>assets/img/zip.png">Street:</label>	
											<div class="edit_inpt">
												<textarea placeholder="Street" name="street" maxlength="255" id="street" class="form-control"><?php if(!empty($getDriverDetails[0])){ echo $getDriverDetails[0]['street']; } ?></textarea>
											</div>		
											<?php echo form_error('street', '<div class="error">', '</div>'); ?>																									
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="info_head">Personal Information :</div>
								<div class="basic_box">
									<div class="row">
										<div class="col-md-6 edit_box">
											<label><img src="<?php echo base_url(); ?>assets/img/medical.png">Medical Insurance ID:</label>	
											<div class="edit_inpt"><input type="text" placeholder="Medical insurance ID" name="insurance_id" id="insurance_id" value="<?php if(!empty($getDriverDetails[0])){ echo $getDriverDetails[0]['insurance_id']; } ?>" class="form-control"></div>
											<?php echo form_error('insurance_id', '<div class="error">', '</div>'); ?>
										</div>										
										<div class="col-md-6 edit_box">
											<label><img src="<?php echo base_url(); ?>assets/img/per_doc.png">Personal Documents:</label>	
											<div class="edit_inpt">
												<div class="input-group image-preview">
													<!-- <input class="form-control image-preview-filename" placeholder="Choose File" disabled="disabled" type="text"> --> <!-- don't give a name === doesn't send on POST/GET -->
													<span class="input-group-btn">
														<!-- image-preview-input -->
														<div class="btn btn-primary image-preview-input">
															<span class="image-preview-input-title">Upload</span> 
															<input type="file" name="personal_doc" id="personal_doc"> <!-- rename it -->
															<input type="hidden" name="personal_doc_old" id="personal_doc_old" value="<?php if(!empty($getDriverDetails[0])){ echo $getDriverDetails[0]['personal_doc']; } ?>"> 
														</div>
														<?php echo form_error('personal_doc', '<div class="error">', '</div>'); ?>
													</span>
												</div>
											</div>
										</div>
										<div class="col-md-6 edit_box">
											<label><img src="<?php echo base_url(); ?>assets/img/emer_no.png">Emergency Contact Name:</label>	
											<div class="edit_inpt"><input type="text" placeholder="Emergency contact name" maxlength="50" name="emergency_contact_name" id="eme_name" value="<?php if(!empty($getDriverDetails[0])){ echo $getDriverDetails[0]['emergency_contactname']; } ?>" class="form-control"></div>
											<?php echo form_error('emergency_contact_name', '<div class="error">', '</div>'); ?>
										</div>
										<div class="col-md-6 edit_box">
											<label><img src="<?php echo base_url(); ?>assets/img/mob_no.png">Emergency Contact Number:</label>	
											<div class="edit_inpt"><input type="text" placeholder="Emergency contact number" maxlength="20" name="emergency_contact_number" id="eme_number" value="<?php if(!empty($getDriverDetails[0])){ echo $getDriverDetails[0]['emergency_contactno']; } ?>" class="form-control"></div>
											<?php echo form_error('emergency_contact_number', '<div class="error">', '</div>'); ?>
										</div>
										<div class="col-md-12 edit_box">
											<label><img src="<?php echo base_url(); ?>assets/img/sign.png">Are you able to Provide Signatures:</label>	
											<div class="edit_inpt_radio radio_cust">												  
												<span class="radio_btn">													
													<input type="radio" id="yes" name="signature" value="1" <?php if(!empty($getDriverDetails[0])){ if($getDriverDetails[0]['signature'] == "1"){ echo "checked=checked"; } } ?>>
													<label for="yes">Yes</label>
												</span>
												<span class="radio_btn">
													<input type="radio" id="no" name="signature" value="0" <?php if(!empty($getDriverDetails[0])){ if($getDriverDetails[0]['signature'] == "0"){ echo "checked=checked"; } } ?>>
													<label for="no">No</label>
												</span>
											</div>
											<?php echo form_error('signature', '<div class="error">', '</div>'); ?>											
											<div id="file_signature">
												<div class="upld_sign">
													<span class="input-group-btn">
														<!-- image-preview-input -->
														<div class="btn btn-primary image-preview-input">
															<span class="image-preview-input-title">Upload Signatures</span>
															<input class="upsign" type="file" name="sign_file_upload" id="sign_file_upload"> <!-- rename it -->
															 <input class="upsign" type="hidden" name="sign_file_upload_old" id="sign_file_upload" value="<?php if(!empty($getDriverDetails[0])){ echo $getDriverDetails[0]['signature_file_upload']; } ?>"> 
														</div>													
													</span>												
												</div>
											</div>											
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="info_head">Insurance Provider :</div>
								<div class="basic_box">
									<div class="ins_head">Search for Insurance Provider</div>
									<div class="ins_search">
										<input type="text" name="search_insurance_prov" id="search_insurance_prov"  class="form-control"/>
									</div>
								</div>
							</div>
							<div class=" col-md-12">								
								<div class="subedit_form">
									<input class="req_btn comp_btn" type="submit" name="edit_profile_save" value="Submit">
								</div>									
							</div>
						</div>
					</div>	
				</form>
			</div>
		</div>
	</div>
</div>
<style type="text/css">
        a {
            cursor: pointer !important; 
        }
        .image-upload > input
        {
            display: none;
        }

        .image-upload img
        {
            width: 50px;
            cursor: pointer;
        }

    </style>
<?php $this->load->view("user/footer"); ?>
