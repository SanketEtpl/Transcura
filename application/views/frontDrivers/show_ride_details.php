<?php $this->load->view('user/header'); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<?php 
$selected_ride_type = $this->session->userdata('schedulaRideData')['sr_ride_type'];
?>
<div class="all_comp_wrapp">
	<div class="container">
		<div class="row">
			<div class="comp_inner">
				<h3 class="heal_head">
					TRIP DETAILS		
					<div id="error_message"></div>
				</h3>
				<div class="comp_box bluebg showridedetails">
					<form name="customer_complaint_form" class="form-inline" method="POST" action="" autocomplete="off">
					<?php 
						//echo "<pre>";print_r($this->session->userdata('schedulaRideData'));die;
						?>
						<div class="form-group">
							<div class="col-md-2 comp_lab">
								<label>Trip Date:</label>
								<div class="comp_inpt">
									<?php if(!empty($this->session->userdata('schedulaRideData')['sr_date'])) echo $this->session->userdata('schedulaRideData')['sr_date']; else echo ''; ?>
								</div>								
							</div>

							<!-- <div class="col-md-2 comp_lab">
								<label>Ride Time :</label>
								<div Trip="comp_inpt">
									<?php //if(!empty($this->session->userdata('schedulaRideData')['sr_appointment_time'])) echo $this->session->userdata('schedulaRideData')['sr_appointment_time'][0]; else echo ''; ?>
								</div>								
							</div> -->

							<div class="col-md-6 comp_lab">
								<label>Trip Type :</label>
								<div class="comp_inpt">
									<?php 									
									$ride_type_all = unserialize(RIDE_TYPE);
									foreach ($ride_type_all as $key => $value) {
										if($selected_ride_type == $key)
										{
											echo $value;
										}
									}
									?>
								</div>								
							</div>

						</div>
						
						<div class="form-group">
							<div class="col-md-12 pdl0">
								<label class="col-md-12 row" for="lblname">Pick Up Address :</label>
								<div class="comp_inpt">
									<?php if(!empty($this->session->userdata('schedulaRideData')['pick_up_address'])) echo $this->session->userdata('schedulaRideData')['pick_up_address']; else echo ''; ?>
								</div> 
								<!-- <div class="col-md-4 comp_lab innrechild pdl0">
									<label>Street :</label>
									<div class="comp_inpt">
										<?php if(!empty($this->session->userdata('schedulaRideData')['from_address'])) echo $this->session->userdata('schedulaRideData')['from_address']; else echo ''; ?>
									</div>		
								</div> -->		
								<!-- <div class="col-md-4 comp_lab innrechild pdl0">
									<label>State :</label>
									<div class="comp_inpt">
										<?php if(!empty($states[0]['state_name'])) echo $states[0]['state_name']; else echo '';?>
									</div>		
								</div> -->	
								<!-- <div class="col-md-4 comp_lab innrechild pdl0">
									<label>Zipcode :</label>
									<div class="comp_inpt">
										<?php //echo $this->session->userdata('scheduleRide')['sr_zipcode'];?>
										<?php if(!empty($this->session->userdata('schedulaRideData')['sr_zipcode'])) echo $this->session->userdata('schedulaRideData')['sr_zipcode']; else echo '';?>
									</div>		
								</div> -->					
							</div>
						</div>

						<?php
						/*if($selected_ride_type == "4" || $selected_ride_type == "2" || $selected_ride_type == "5"){*/
							$cnt=1;
							$drop_add_all = $this->session->userdata('schedulaRideData')['drop_address'];
							$dropCount = count($drop_add_all);
							for($i=0;$i<$dropCount;$i++)
								{?>
							<div class="form-group">
								<div class="col-md-6 pdl0">
									<label class="col-md-12 row" for="lblname">Drop Off Address <?php echo $cnt; ?> :</label>
									<div class="comp_inpt">
										<?php echo $drop_add_all[$i]; ?>
									</div> 				
								</div>

								<?php 

								/*print_r($this->session->userdata('schedulaRideData'));

								exit();*/
								//$count = count($this->session->userdata('schedulaRideData')['sr_appointment_time']);

								$app_time_all = $this->session->userdata('schedulaRideData')['sr_appointment_time'];

								if($dropCount > 0)
								{
									if($dropCount == 1)
									{ ?>

										<div class="col-md-6 pdl0">
											<label class="col-md-12 row" for="lblname">Appointment Time </label>
											<div class="clearfix"></div>
											<div class="comp_inpt">
												<?php echo $this->session->userdata('schedulaRideData')['sr_appointment_time'][0]; ?>
											</div>
										</div>


								<?php	}
									else if($dropCount > 1)
									{?>

										<div class="col-md-6 pdl0">
											<label class="col-md-12 row" for="lblname">Appointment Time <?php echo $cnt; ?> :</label>
											<div class="clearfix"></div>
											<div class="comp_inpt">
												<?php echo $app_time_all[$i]; ?>
											</div> 				
										</div> 
									<?php }
								}


								
								/*if($app_time_all[$i+1] != "")						
								{*/
								?>
								<!-- <div class="col-md-6 pdl0">
									<label class="col-md-12 row" for="lblname">Appointment Time <?php echo $cnt; ?> :</label>
									<div class="clearfix"></div>
									<div class="comp_inpt">
										<?php echo $app_time_all[$i+1]; ?>
									</div> 				
								</div> -->
								<?php //} ?>
							</div>		
							<?php 
							$cnt++;
						}
					/*}else{ */
						?>
						<!-- <div class="form-group">
							<div class="col-md-12 pdl0">
								<label class="col-md-12 row" for="lblname">Drop Off Address :</label>
								<div class="comp_inpt">
									<?php if(!empty($this->session->userdata('schedulaRideData')['drop_address'][0])) echo $this->session->userdata('schedulaRideData')['drop_address'][0]; else echo ''; ?>
								</div> 				
							</div>
						</div> -->
						<?php //}
						?>

						<?php
						if($selected_ride_type == "2" || $selected_ride_type == "4" || $selected_ride_type == "6"){
							?>						
							<div class="form-group">
								<div class="col-md-12 pdl0">
									<label class="col-md-12 row" for="lblname">Pharmacy Address :</label>
									<div class="comp_inpt">
										<?php if(!empty($this->session->userdata('schedulaRideData')['pharmacy_address'])) echo $this->session->userdata('schedulaRideData')['pharmacy_address']; else echo ''; ?>
									</div> 				
								</div>
							</div>
							<?php } ?>

							<div class="form-group">
								<div class="col-md-12 pdl0">
									<div class="col-md-4 comp_lab ">
										<label> Required Equipment :</label>
										<div class="comp_inpt">
											<?php if(!empty($hc_service[0]))echo $equipment[0]['equipment']; else echo '-';?>
										</div>								
									</div>	


									<div class="col-md-4 comp_lab ">
										<label> Desired Healthcare Service :</label>
										<div class="comp_inpt">
											<?php if(!empty($hc_service[0]))echo $hc_service[0]['hs_name']; else echo '-';?>
										</div>								
									</div>

									<div class="col-md-4 comp_lab">
										<label> Desired Transportation :</label>
										<div class="comp_inpt">
											<?php if(!empty($desire_tran[0])) echo $desire_tran[0]['dt_name']; else echo "-"; ?>
										</div>								
									</div>

							<!-- <div class="col-md-4 comp_lab pdl0">
								<label> Health Issues you would want your transportation provider to be aware of:</label>
								<div class="comp_inpt">
									<?php //echo $hc_service[0]['hs_name'];?>
								</div>								
							</div>
						</div> -->
					</div>

					<div class="form-group">
						<br />
						<div class="col-md-12 pdl0">
							<div class="col-md-4 comp_lab pdl0">
								<label> Special Requests :</label>
								<div class="comp_inpt">
									<?php if(!empty($sp_request[0])) echo $sp_request[0]['sr_name']; else echo "-"; ?>
								</div>								
							</div>

							<div class="col-md-4 comp_lab pdl0">
								<label> Special Instructions :</label>
								<div class="comp_inpt">
									<?php if(!empty($this->session->userdata('schedulaRideData')['sr_special_instruction'])) echo $this->session->userdata('schedulaRideData')['sr_special_instruction']; else echo '-'; ?>
								</div>								
							</div>

							<div class="col-md-4 comp_lab pdl0">
								<label> Request Return :</label>
								<div class="comp_inpt">
									<?php
									if($selected_ride_type == "3")
									{
										$return =  "Yes";
									}else if($selected_ride_type == "2")
									{
										if(sizeof($this->session->userdata('schedulaRideData')['drop_address']) > 1)
										{
											$return = "No";
										}else
										{
											$return = "Yes";
										}
									}else
									{
										$return = "No";
									}
									echo $return;
									?>
								</div>								
							</div>
						</div>
					</div>


				</form>	
			</div>
			<div class="pay_btn_box mb15">					
				<!-- <input type="hidden" id="form_data" name="form_data" value="<?php //echo json_encode($form_data); ?>"> -->
				<button type="button" class="req_btn cancel_btn " id="request_ride_cancel" data-toggle="modal" data-target="#succpay_pop">Cancel</button>
				<button type="button" class="req_btn comp_btn" id="nemtsubmitScheduleRequest" data-toggle="modal">Book a Trip</button>								
			</div>	
		</div>
	</div>
</div>
</div>
<!--submit complaints popup-->
<div id="subcomp_pop" class="modal fade in" role="dialog">
	<div class="modal-dialog">    
		<div class="modal-content sma_pop">
			<div class="modal-header">
				<button type="button" class="close complaint_ok" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>       
			</div>
			<div class="modal-body">
				<div class="succ_icon"><img src="<?php echo base_url(); ?>assets/img/succpay_icon.png"></div>
				<h4 class="succ_tit">Successful !!!</h4>
				<h2 class="read_trip">Your trip has been sent for approval.</h2>	
			</div>
			<div class="modal-footer ok_btn">
				<button type="button" id="nemt_schedule_ride_ok" class="btn btn-default cont_btn" data-dismiss="modal">OK</button>			
			</div>
		</div>
	</div>
</div>
<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){	
		$("#nemtsubmitScheduleRequest").click(function(){
			$.ajax({
				dataType: "json",
				type:"POST",
				url:baseURL+"frontend/DriverController/request_to_schedule_ride",
          // data:{'form_data':'<?php echo json_encode($form_data); ?>'},
          success:function(response){
          	if(response.status == true){                      
          		$('#subcomp_pop').modal('show');                
          	}
          	else
          	{
          		alert(response.message);        
          	}            
          }
      });
		});
	});
</script>
<?php $this->load->view('user/footer'); ?>
