<?php $this->load->view("user/header"); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<div class="driver_sched_wrapp" id="section_first">
	<div class="container">
		<div class="row">
			<div class="driver_sched_inner">			
				<h3 class="heal_head">Select An Healthcare</h3>
				<div class="search_filt_wrap">
					<div class="sera_title">Search Filter</div>
					<div class="sear_bt_box">
						<div class="row">
							<div class="col-md-3 sea_box">
								<label>Search</label>	
								<div class="edit_inpt">
									<input placeholder="Search Name" name="search_name" id="search_name" value="" class="form-control" type="text">
								</div>
							</div>
							<div class="col-md-3 sea_box">
								<label>Category</label>	
								<div class="cat_inpt">
									<select class="form-control" name="search_cat" id="search_cat">
										<option value="">Search Category</option>
										<?php 
										foreach ($drcat as $cat) {
											echo "<option value='".$cat['cat_id']."'>".$cat['cat_title']."</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="col-md-4 sea_box">
								<label>Nearest to Me</label>	
								<div id="" class='slider-example'>
									Coming Soon Js slider
								</div>
							</div>
							<div class="col-md-2 apply_btn">
								<button type="button" id="search_healthcare" class="">Apply</button>
							</div>
						</div>
					</div>
				</div>	
				<form name="healthData" method="POST" id="healthData" action="<?php echo base_url(); ?>health-details">
			
				<input type="hidden" id="test_data" name="test_data" value="<?php print_r($form_data); ?>">
				<div class="heal_wrapp">					
					<?php 
					//echo "<pre>";print_r();die;
					//echo $form_data;die;
					foreach ($healthcarelist as $healthcare) {
						$userid = $this->encrypt->encode($healthcare['id']);
						$uid = strtr($userid, '+/', '-_');
					?>
					<div class="col-md-4">
						<div class="heal_rides">
							<div class="heal_img">
								<img src="<?php echo base_url() ?>assets/img/healt_bg.png">
								<div class="hela_logo_box">
									<span class="hela_logo" style="background-image:url('<?php echo base_url() ?>assets/img/hea_logo.png');"></span>
								</div>
							</div>
							<div class="heal_inn">
								<div class="hel_name"><?php echo $healthcare['username']; ?></div>
								<div class="hel_txt"><?php echo $healthcare['email']; ?></div>
								<div class="hel_txt"><?php echo $healthcare['phone']; ?></div>
								<input type="hidden" name="id" value="<?php echo $uid; ?>">
								<div class="heal_btn">
								<span><button type="submit">Apply</button></span>
									<!-- <span><a href="<?php //echo base_url(); ?>health-details?id=<?php echo $uid; ?>">Apply</a></span> -->
									<span><button type="button">View Details</button></span>
								</div>	
							</div>
						</div>
					</div>
					
				<?php } ?>
					
					
					
					
				</div>
				</form>

			</div>
		</div>
	</div>
</div>


<!-- healthcare details popup -->
<div id="health_pop" class="modal fade" role="dialog">
  <div class="modal-dialog">    
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>
        <h4 class="modal-title">Alta Bates Medical Group</h4>
      </div>
      <div class="modal-body">
		<h4 class="inner-title">Address Of Healthcare</h4>
		<div class="add_details">
			<div class="add_box">
				<label>Name :</label>
				<div class="lab_txt">Alta Bates Medical Group</div>
			</div>
			<div class="add_box">
				<label>Address :</label>
				<div class="lab_txt">
					<div class="add_sec">
						<div class="add_lt">Street :</div>
						<div class="add_rt">Michael I. Days 3756 Preston Street Wichita, KS 67213</div>
					</div>
					<div class="add_sec">
						<div class="add_lt">State :</div>
						<div class="add_rt">Alaska</div>
					</div>
					<div class="add_sec">
						<div class="add_lt">Zip Code :</div>
						<div class="add_rt">000</div>
					</div>
					<div class="map_pop"><a href="#" class="com_btn">View Map</a></div>
				</div>
			</div>
			<div class="add_box">
				<label>Mobile Number :</label>
				<div class="lab_txt">
					<div class="add_sec">
						<div class="row">
							<div class="col-md-3">
								<div class="lt_mob">Office Number :</div>
								<div class="rt_mob">2020 0201200</div>
							</div>
							<div class="col-md-3">
								<div class="lt_mob">Phone :</div>
								<div class="rt_mob">857-778-1265</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
			<div class="add_box">
				<label>Opening Hours :</label>
				<div class="lab_txt">
					<div class="add_sec">
						<div class="row">
							<div class="col-md-4">
								<div class="lt_mob">Monday To Saturday :</div>
								<div class="rt_mob">8 AM to 8 PM</div>
							</div>
							<div class="col-md-4">
								<div class="lt_mob">Sunday :</div>
								<div class="rt_mob">Closed</div>
							</div>
						</div>
					</div>					
				</div>
			</div>
		</div>
      </div>
      <div class="modal-footer">
			<button type="button" id="btn_continue" class="btn btn-default cont_btn" data-dismiss="modal">Continue</button>
      </div>
    </div>
  </div>
</div>


<?php $this->load->view("user/footer"); ?>
