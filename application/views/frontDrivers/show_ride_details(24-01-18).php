<?php $this->load->view('user/header'); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<div class="all_comp_wrapp">
	<div class="container">
		<div class="row">
			<div class="comp_inner">
				<h3 class="heal_head">
					SCHEDULE A RIDE TRIP DETAILS		
					<div id="error_message"></div>
					<!-- <span class="back_box"><button class="back_btn customer_complaints" id="customer_complaints_bk" name="customer_complaints_bk">Back</button></span> -->
				</h3>
				<div class="comp_box">
					<form name="customer_complaint_form" method="POST" action="" autocomplete="off">
						<div class="row">
							<div class="col-md-6 comp_lab">
								<label>Ride Date:</label>
								<div class="comp_inpt">
									<?php echo $this->session->userdata('scheduleRide')['sr_date'];?>
								</div>								
							</div>
							<div class="col-md-6 comp_lab">
								<label>Ride Time :</label>
								<div class="comp_inpt">
									<?php echo $this->session->userdata('scheduleRide')['sr_pick_up_time'];?>
								</div>								
							</div>
							<div class="col-md-12 comp_lab">
								<label>Pick Up Address :</label>
								<div class="col-md-4 comp_lab">
									<label>Street :</label>
									<div class="comp_inpt">
										<?php echo $this->session->userdata('scheduleRide')[0]['from_address'];?>
									</div>		
								</div>		
								<div class="col-md-4 comp_lab">
									<label>State :</label>
									<div class="comp_inpt">
										<?php if(!empty($states[0])) echo $states[0]['state_name']; else echo '';?>
									</div>		
								</div>	
								<div class="col-md-4 comp_lab">
									<label>Zipcode :</label>
									<div class="comp_inpt">
										<?php echo $this->session->userdata('scheduleRide')['sr_zipcode'];?>
									</div>		
								</div>					
							</div>
							<div class="col-md-4 comp_lab">
								<label> Type of Ride:</label>
								<div class="comp_inpt">
									<?php if(!empty($hc_service[0]))echo $hc_service[0]['hs_name']; else echo '';?>
								</div>								
							</div>
							<div class="col-md-4 comp_lab">
								<label> Special Requests:</label>
								<div class="comp_inpt">
									<?php if(!empty($sp_request[0])) echo $sp_request[0]['sr_name']; else echo ""; ?>
								</div>								
							</div>
							<div class="col-md-4 comp_lab">
								<label> Health Issues you would want your transportaion provider to be aware of:</label>
								<div class="comp_inpt">
									<?php //echo $hc_service[0]['hs_name'];?>
								</div>								
							</div>
							<div class="col-md-4 comp_lab">
								<label> Desired Transportation :</label>
								<div class="comp_inpt">
									<?php if(!empty($desire_tran[0])) echo $desire_tran[0]['dt_name']; else echo ""; ?>
								</div>								
							</div>
						</div>	
					</form>	
					<div class="pay_btn_box">
						<button type="button" class="req_btn cancel_btn " id="customer_complaints_cancel" data-toggle="modal" data-target="#succpay_pop">Cancel</button>
						<button type="button" class="req_btn comp_btn" id="submitScheduleRequest" data-toggle="modal">Submit</button>	<!-- data-target="#subcomp_pop" -->				
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>
<!--submit complaints popup-->
<div id="subcomp_pop" class="modal fade in" role="dialog">
  <div class="modal-dialog">    
    <div class="modal-content sma_pop">
      <div class="modal-header">
        <button type="button" class="close complaint_ok" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>       
      </div>
      <div class="modal-body">
		<div class="succ_icon"><img src="<?php echo base_url(); ?>assets/img/succpay_icon.png"></div>
			<h4 class="succ_tit">Successful !!!</h4>
			<h2 class="read_trip" id="tripIDSuccess"></h2>	
		</div>
      <div class="modal-footer ok_btn">
			<button type="button" id="schedule_ride_ok" class="btn btn-default cont_btn complaint_ok" data-dismiss="modal">OK</button>			
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('user/footer'); ?>