<?php $this->load->view("user/header"); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<div style="float:left;width:100%;padding-top: 10px;">
<div class="container">
	<div class="row schedule-details" style="padding-top:10px">
		<div class="responsive" id="schedule_rides">
			<div id="success_message" class="alert alert-success" style="display:none"></div>
			<div id="error_message" class="alert alert-danger" style="display:none"></div>
			<div class="table-responsive">
			<table border='1' class="table table-striped">  	
			    <thead>
			      <tr>
			        <th>Trip id</th>
			        <th>Pick up point</th>
			        <th>Drop point</th>
			        <th>Distance</th>
			        <th>Duration</th>
			        <th>Status</th>
			        <th>Action</th>
			      </tr>
			    </thead>
			    <tbody>
			    	<?php    	
						foreach ($results as $key => $value) {	
							if($value['driver_status'] == "1")
							{
								$trip_status = "<strong><span style='color: orange;'>Request Sent</span></strong>";
								$action = "";
							}else if($value['driver_status'] == "2")
							{
								$action = '<button type="button" class="req_btn comp_btn confirm_button" data-id="'.$value['id'].'" id="btn_confirm_driver" data-toggle="modal">Confirm</button>
								<button type="button" class="req_btn comp_btn reject_button" data-id="'.$value['id'].'" id="btn_reject_driver" data-toggle="modal" title="If you reject driver, your booking request is again posted except this driver.">Reject</button>';
								$trip_status = "<strong><span style='color: green;'>Accepted</span></strong>";
							}else if($value['driver_status'] == "3")
							{
								$trip_status = "<strong><span style='color: green;'>Trip Confirmed</span></strong>";
								$action = "";
							}													
							?>
			 		<tr>
			      	<td style="display:none" row-data-id="<?php echo $value['id'];?>"><?php echo $value['id'];?></td>
			        <td><?php echo $value['sr_trip_id']; ?></td>
			        <td><?php echo $value['source_address']; ?></td>
			        <td><?php echo $value['destination_address']; ?></td>
			        <td><?php echo $value['sr_total_distance']; ?></td>
			        <td><?php echo $value['duration']; ?></td>  
			        <td><?php echo $trip_status;?></td>
			        <td><?php echo $action ?></td>
			      </tr>

				<?php  } ?>
			    </tbody>
			</table>
		 	<ul class="pagination">
		    	<li><?php echo $links; ?></li>			  
		  	</ul>		
		</div>

		</div>

	</div>
</div>
</div>

<?php $this->load->view("user/footer"); ?>

