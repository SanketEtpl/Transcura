<div class="inner_dashboard">
	<div class="inner_img" style="background-image:url('<?php echo base_url(); ?>assets/img/inner_banner.png');"></div>
	<div class="inner_top_wrapp">
		<div class="inner_head">
			<?php
				$getdata = $this->uri->segment(1);	
				if($getdata == 'healthcare-dashboard')
				{
					echo strtoupper('dashboard');
				}else if($getdata == 'healthcare-profile')
				{
					echo strtoupper('my profile');
				}
														   	 				
			?>
		</div>
	</div>
</div>
<div class="inner_menu_section">
	<div class="container">
		<div class="row">
			<div class="dash_inn_menu">
				<ul>										
					<li><a href="<?php echo base_url(); ?>healthcare-dashboard" <?php if($getdata =="healthcare-dashboard"){ echo $class = 'class="act_inn_menu"'; } ?>>Dashboard</a></li>
					
					<li><a href="<?php echo base_url(); ?>healthcare-profile" <?php if($getdata =="healthcare-profile"){ echo $class = 'class="act_inn_menu"'; } ?>>My Profile</a></li>					
				</ul>	
			</div>
		</div>
	</div>
</div>
