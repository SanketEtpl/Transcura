<?php $this->load->view("user/header"); ?>
<?php $this->load->view('frontHealthCare/healthcareHeader'); ?>

<div class="inner_body_wrapp">
	<div class="container">
		<div class="row">
			<div class="ts_dash_wrap">
				<div class="top_dash_sec">
					<div class="col-md-6 lt_prof">
						<div class="pro_wrap">


							<div class="prof_cir">
							<img src="<?php if(!empty($userDetails[0])){ if(!empty($userDetails[0]['picture'])){ echo base_url().$userDetails[0]['picture'];}else{ echo base_url()."assets/img/no_img.png"; } } ?>" />
							</div>

							<div class="prof_info">
								<div class="mypro_head">My Profile</div>
								<div class="mypro_name">
									<?php if(!empty($userDetails[0])){ echo $userDetails[0]['full_name']; }?></br>
									<span><?php if(!empty($userDetails[0])){ echo $userDetails[0]['email']; }?></span>
								</div>
								<div class="view_pro_btn"><a href="<?php echo base_url();?>healthcare-profile">View</a></div>
							</div>

						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
<?php $this->load->view("user/footer"); ?>