<?php $this->load->view("user/header"); ?>
<?php $this->load->view("driverUser/driverHeader"); ?>
<div class="myprof_wrapp">
   <div class="probgimg" style="background-image:url('<?php echo base_url(); ?>assets/img/profile_bg.png');"></div>
   <div class="mypro_info_wrapp edit_pro_wrapp">

      <div class="container">
         <div class="row">
            <form name="edit_profile" action="<?php echo base_url(); ?>update-driver-edit-user-profile" method="POST" autocomplete="off" enctype="multipart/form-data">
               <div class="tp_pro_sect">
                  <div class="pro_img_box image-upload" id="preview_image" style="background-image:url('<?php if(!empty($driveUserProfile[0])){ if(!empty($driveUserProfile[0]['picture'])){ echo base_url()."uploads/Driver/doc/".$driveUserProfile[0]['picture']; }else{ echo "uploads/Driver/doc/default.png"; } } ?>');">						
                     <label class="edit_icon" for="file-input">
                        <i class="fa fa-pencil" aria-hidden="true"></i>	
                     </label>							
                     <input type="hidden" id="profile_image_old" name="profile_image_old" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['picture']; }else{ echo "assets/img/no_img.png"; } ?>">
                     <input id="file-input" name="profile_image" type="file" />
                  </div>
                  <div class="pro_img_name">
                     <div class="pro_name_box">
                        <div class="pro_nme_input"><input type="text" placeholder="Stevie Mcmenamin"   maxlength="50" name="full_name" id="fullname" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['full_name']; } ?>" class="form-control"></div>
                        <?php echo form_error('full_name', '<div class="error">', '</div>'); ?>
                        <div id="fullname_error"></div>
                        <div class="edit_prof_btn"><a href="<?php echo base_url();?>driver-my-profile">View Profile</a></div>
                     </div>
                  </div>
               </div>
               <div class="basic_info_wrapp">
                  <div class="row">
                     <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="info_head">Basic Information :</div>
                        <div class="basic_box">
                           <div class="row">
                              <div class="col-md-6 edit_box">
                                 <label><img src="<?php echo base_url(); ?>assets/img/date_icon.png">Date of Birth:</label>	
                                 <div class="edit_inpt">
                                    <input type="text" placeholder="Date of birth" name="date_of_birth" id="date_of_birth" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['date_of_birth']; } ?>" class="form-control date-picker">
                                    <div id="date_of_birth_error"></div>
                                 </div>
                                 <?php echo form_error('date_of_birth', '<div class="error">', '</div>'); ?>
                              </div>
                              <input type="hidden" name="user_id" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['id']; } ?>" id="user_id"> 
                              
                              <div class="col-md-6 edit_box">
                                 <label><img src="<?php echo base_url(); ?>assets/img/mob_no.png">Mobile Number:</label>	
                                 <div class="edit_inpt"><input type="text" placeholder="Mobile number" name="phone_no" id="phnnumber" maxlength="20" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['phone']; } ?>" class="form-control">
                                    <div id="phnnumber_error"></div>
                                 </div>
                                 <?php echo form_error('phone_no', '<div class="error">', '</div>'); ?>
                              </div>
                              <div class="col-md-6 edit_box">
                                 <label><img src="<?php echo base_url(); ?>assets/img/email_add.png">Email Address:</label>	
                                 <div class="edit_inpt"><input type="text" placeholder="Email ID" disabled name="email" id="emailaddrss" maxlength="50" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['email']; } ?>" class="form-control">
                                    <div class="emailaddrss_error"></div>
                                 </div>
                                 <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                     <div class="info_head">Address :</div>
                     <div class="basic_box">
                        <div class="row">
                           <div class="col-md-6 edit_box">
                              <label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/coun.png">Country:</label>	
                              <div class="edit_inpt">			
                                    <select class="form-control" name="country" id="country">
                                    <option value="" class="selecttxt" disable selected hidden>Select Country</option>
                                    <?php						
                                    if(!empty($countryData)){
                                    foreach($countryData as $country_name) {
                                    ?>
                                    <option value="<?php echo $country_name['id']; ?>"
                                    <?php 
                                     if(!empty($driveUserProfile[0])){  
                                    echo ($driveUserProfile[0]['country'] == $country_name['id'])?"selected":'';
                                  }
                                  ?> ><?php echo $country_name['country_name']?>
                                    </option>
                                    <?php } } else { ?>	
                                    <option value=""></option>
                                    <?php } ?>
                                 </select>
                                 <div id="country_error"></div>
                              </div>
                              <?php echo form_error('country', '<div class="error">', '</div>'); ?>
                           </div>
                           <div class="col-md-6 edit_box">
                              <label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/state.png">State:</label>	
                              <div class="edit_inpt">			 
                                 <select class="form-control" name="state" id="state1">

                                    <option value="" class="selecttxt" disable selected hidden>Select State</option>
                                    <?php if(!empty($stateData)){
                                    foreach($stateData as $state_name) {
                                    ?>
                                    <option value="<?php echo $state_name['id']; ?>" <?php if($driveUserProfile[0]['state'] == $state_name['id']){ echo "selected"; }else{ ''; }?>>
                                    <?php echo $state_name['state_name']?>   
                                    </option>
                                    <?php } } else { ?>	
                                    <option value=""></option>
                                    <?php } ?>
                                 </select>
                                 <div id="state_error"></div>
                              </div>
                              <?php echo form_error('state', '<div class="error">', '</div>'); ?>
                           </div>
                           <div class="col-md-6 edit_box">
                              <label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/state.png">City:</label>	
                              <div class="edit_inpt">         
                                 <select class="form-control" name="city" id="city1">
                                    <option class="selecttxt" value="" disable selected hidden>Select City
                                    </option>
                                    <?php 
                                    if(!empty($cityData)){
                                    foreach($cityData as $city_name) {
                                    ?>
                                    <option value="<?php echo $city_name['id']; ?>" <?php if($driveUserProfile[0]['city'] == $city_name['id']){ echo "selected"; } ?> ><?php echo $city_name['city_name']?></option>
                                    <?php } } else { ?>	
                                    <option value=""></option>
                                    <?php } ?>
                                 </select>
                                 <div id="city_error"></div>
                              </div>
                              <?php echo form_error('city', '<div class="error">', '</div>'); ?>
                           </div>
                           <div class="col-md-6 edit_box zipcode-country">
                              <div class="row">
                                 <div class="col-md-6 zipcode-div">
                                    <label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/zip.png">Zip Code:</label>	
                                    <div class="edit_inpt">
                                       <input type="text" placeholder="Zipcode" maxlength="6" name="zipcode" id="zipcode" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['zipcode']; } ?>" class="form-control">
                                       <div id="zipcode_error"></div>
                                    </div>
                                    <?php echo form_error('zipcode', '<div class="error">', '</div>'); ?>
                                 </div>
                                 <div class="col-md-6 counrtydiv">
                                    <label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/zip.png">County:</label>	
                                    <div class="edit_inpt">
                                       <input type="text" placeholder="County" maxlength="5" name="county" id="county" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['county']; } ?>" class="form-control">
                                       <div id="county_error"></div>
                                    </div>
                                    <?php echo form_error('county', '<div class="error">', '</div>'); ?>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-12 edit_box">
                              <label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/zip.png">Street:</label>	
                              <div class="edit_inpt">
                                 <textarea placeholder="Street" name="street" maxlength="100" id="street" class="form-control"><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['street']; } ?></textarea>
                                 <div id="street_error"></div>
                              </div>		
                              <?php echo form_error('street', '<div class="error">', '</div>'); ?>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div><span id="fileErrorType"> <?php echo $this->session->flashdata('filetypeError'); ?></span></div>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                     <div class="info_head">Personal Information :</div>
                     <div class="basic_box">

                     <div class="row">
                     <div class="col-md-6 edit_box">
                     <label><img src="<?php echo base_url(); ?>assets/img/companyname.png" height=18>Company Name:</label>	
                     <div class="edit_inpt"><input type="text" placeholder="Company Name" id="company_name" name="company_name" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['company_name']; } ?>" class="form-control"></div>
                     <?php echo form_error('company_name', '<div class="error">', '</div>'); ?>
                     </div>	

                     <div class="col-md-6 edit_box">
                     <label><img src="<?php echo base_url(); ?>assets/img/companyname.png" height=18>Company address :</label>	
                     <div class="edit_inpt"><input type="text" placeholder="Company Address"  name="company_address" id="company_name" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['company_address']; } ?>" class="form-control"></div>
                     <?php echo form_error('company_address', '<div class="error">', '</div>'); ?>
                     </div>										
                     <!-- <div  id="personal_doc_id"> -->
                     <div class="col-md-6 edit_box">	
                     <label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/per_doc.png">Personal Documents:</label>	
                     <div class="edit_inpt">
                     <select class="form-control" id="pers_doc_dd" name="pers_doc_dd">
                     <option value="" diable seleted hidden>Select personal documents</option>
                     <option value="1" <?php 
                     if(!empty($driveUserProfile[0])){  
                              echo ($driveUserProfile[0]['personal_doc_status'] == '1')?'selected':'';
                            }?>
                      >YES</option>
                     <option value="2"  <?php 
                      if(!empty($driveUserProfile[0])){  
                              echo ($driveUserProfile[0]['personal_doc_status'] == '2')?'selected':'';
                            }

                      ?> >NO</option>
                     </select>
                     <div id="pers_doc_dd_error"></div>
                     <span class="error_usertype"></span>
                     </div>
                     </div>
                     <!-- </div> -->
                     <div id="drivers">
                     <div class="col-md-6 edit_box">	
                     <label><img src="<?php echo base_url(); ?>assets/img/medical.png">medical insurance id</label>

                     <input type="text" class="form-control" name="insurance_id" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['insurance_id']; } ?>" id="insurance_id" placeholder="Insurance id" maxlength="20" autocomplete="off">	
                     <div id="insurance_id_error"></div>
                     </div>

                     <div class="col-md-6 edit_box">	
                     <label><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Social security card</label>
                     <div class="form-group brows-doc">                     
                        <input class="file" type="file" name="social_security_card_doc" id="social_security_card_doc" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['social_security_card_doc']; } ?>">
                        <div class="input-group col-xs-12">
                           <input type="text" class="form-control" style="pointer-events: none;" placeholder="Social security card" name="" value="">
                            <input type="hidden" class="form-control"  placeholder="Social security card" name="social_security_card_doc_old" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['social_security_card_doc']; } ?>">

                           <span class="input-group-btn">                 
                           <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>                  
                           </span>
                        </div>
                        <div id="social_security_card_doc_error"><?php 
                        //echo $social_security_card_doc_error;exit;
                         //if(isset($social_security_card_doc_error)){ echo $social_security_card_doc_error } ?>
                           
                        </div>
                        <div><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['social_security_card_doc']; } ?></div>
                        </div>	
                     </div>

                     <div class="select-nemt">
                     <div class="col-md-6 edit_box">	
                     <label>
                     <img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Driver license</label>
                     <div class="form-group brows-doc">
                     <input class="file" type="file" name="driving_license_doc" id="driving_license_doc" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['driver_license_doc']; } ?>" >
                     <div class="input-group col-xs-12">
                     <input type="text" class="form-control"  style="pointer-events: none;" placeholder="Driving license documents" name="" value="">
                      <input type="hidden" class="form-control"  style="pointer-events: none;" placeholder="Driving license documents" name="driver_license_doc_old" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['driver_license_doc']; } ?>">
                     <span class="input-group-btn">
                     <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
                     </span>
                     </div>

                     <div><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['driver_license_doc']; } ?></div>
                     </div>
                     </div>
                     <div class="row-comm" id="dledShow_date_id">
                     <div class="col-md-6 edit_box">

                     <label for="driving_license" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Driving license expiry date</label>
                     <input type="text" name="driving_license_expiry_date" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['driver_license_expire_dt']; } ?>" id="driving_license_expiry_date" class="form-control date-picker pointer fourYearDate" placeholder="Driving license expire date" autocomplete="off" readonly>
                     </div>
                     </div>
                     </div>
                     <div class="select-nemt">
                     <div class="col-md-6 edit_box">	
                     <label><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />5-Panel drug test results</label>
                     <div class="form-group brows-doc">

                     <input class="file" type="file" name="drug_test_doc" id="drug_test_doc" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['drug_test_results_doc']; } ?>" >
                     <div class="input-group col-xs-12">
                     <input type="text" class="form-control"  style="pointer-events: none;" placeholder="5-Panel drug test results" name="" value="">
                     <input type="hidden" class="form-control"  style="pointer-events: none;" placeholder="5-Panel drug test results" name="drug_test_results_doc_old" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['drug_test_results_doc']; } ?>">

                     <span class="input-group-btn">
                     <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
                     </span>
                     </div>

                     <div><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['drug_test_results_doc']; } ?></div>
                     </div>
                     </div>

                     <div class="row-comm" id="drug_test_date_id">
                     <div class="col-md-6 edit_box">	
                     <label for="drug_test" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />5-Panel drug test results expiry date</label>
                     <input type="text" name="drug_test_expiry_date" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['drug_test_results_expire_dt']; } ?>" id="drug_test_expiry_date" class="form-control date-picker sixMonthDate" placeholder="Drug test expire date" autocomplete="off" readonly>
                     </div>
                     </div>				
                     </div>



                     <div class="select-nemt">
                     <div class="col-md-6 edit_box">	
                     <label><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Criminal gack ground</label>
                     <div class="form-group brows-doc">

                     <input class="file" type="file" name="criminal_bk_doc" id="criminal_bk_doc" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['criminal_back_ground_doc']; } ?>" >
                     <div class="input-group col-xs-12">
                     <input type="text" class="form-control"  style="pointer-events: none;" placeholder="Criminal gack ground" name="" value="">
                      <input type="hidden" class="form-control"  style="pointer-events: none;" placeholder="Criminal gack ground" name="criminal_back_ground_doc_old" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['criminal_back_ground_doc']; } ?>">

                     <span class="input-group-btn">
                     <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
                     </span>
                     </div>
                     <div><?php //if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['criminal_back_ground_expire_dt']; } ?></div>
                     </div>
                     </div>
                     <div class="row-comm" id="criminal_bk_date_id">
                     <div class="col-md-6 edit_box">
                        <?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['criminal_back_ground_doc']; } ?>
                     <label for="criminal_bk" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Criminal back ground expiry date</label>
                     <input type="text" name="criminal_bk_expiry_date" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['criminal_back_ground_expire_dt']; } ?>" id="criminal_bk_expiry_date" class="form-control date-picker oneYearDate" placeholder="Criminal Back Ground expiry date" autocomplete="off" readonly>
                     </div>
                     </div>	
                     </div>


                     <div class="select-nemt">
                     <div class="col-md-6 edit_box">	
                     <label><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Motor vehicle record</label>
                     <div class="form-group brows-doc">
                     <input class="file" type="file" name="motor_vehicle_details_doc" id="motor_vehicle_details_doc" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['motor_vehicle_record_doc']; } ?>">
                     <div class="input-group col-xs-12">
                     <input type="text" class="form-control"  style="pointer-events: none;" placeholder="Motor vehicle record"  name="" value="">
                     <input type="hidden" class="form-control"  style="pointer-events: none;" placeholder="Motor vehicle record"  name="motor_vehicle_record_doc_old" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['motor_vehicle_record_doc']; } ?>">
                     <span class="input-group-btn">
                     <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
                     </span>
                     </div>
                     <div><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['motor_vehicle_record_doc']; } ?></div>
                     </div>
                     </div>
                     <div class="row-comm" id="motor_vehicle_date_id">
                     <div class="col-md-6 edit_box">

                     <label for="motor_vd" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Motor vehicle record expiry date</label>
                     <input type="text" name="motor_vehile_record_expiry_date" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['motor_vehicle_record_expire_dt']; } ?>" id="motor_vehile_record_expiry_date" class="form-control date-picker oneYearDate" placeholder="Motor vehicle record expiry date" autocomplete="off" readonly>
                     </div>
                     </div>
                     </div>
                     <div class="select-nemt">
                     <div class="col-md-6 edit_box">	
                     <label><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Act 33 & 34 clearance</label>
                     <div class="form-group brows-doc">
                     <input class="file" type="file" name="act_33_34_clearance_doc" id="act_33_34_clearance_doc">
                     <div class="input-group col-xs-12">
                     <input type="text" class="form-control"  style="pointer-events: none;" placeholder="Act 33 & 34 clearance" name="" value="">

                     <input type="hidden" class="form-control"  style="pointer-events: none;" placeholder="Act 33 & 34 clearance" name="act_33_and_34_clearance_doc_old" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['act_33_and_34_clearance_doc']; } ?>">
                     <span class="input-group-btn">
                     <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
                     </span>
                     </div>
                     <div><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['act_33_and_34_clearance_doc']; } ?></div>
                     </div>
                     </div>
                     <div class="row-comm" id="act_33_34_clearance_date_id">
                     <div class="col-md-6 edit_box">

                     <label for="ac_vd" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Act 33 & 34 clearance expiry date</label>
                     <input type="text" name="act_33_34_clearance_expiry_date" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['act_33_and_34_clearance_expire_dt']; } ?>" id="act_33_34_clearance_expiry_date" class="form-control date-picker oneYearDate" placeholder="Act 33 & 34 clearance expiry date" autocomplete="off" readonly>
                     </div>
                     </div>
                     </div>
                     </div>


                     <div class="col-md-6 edit_box">	
                        <label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Vehicle registration</label>
                        <select class="form-control" id="vehicle_reg_documents_id" name="vehicle_reg_documents_id">
                           <option value="" diable seleted hidden>Select vehicle registration</option>
                           <option value="1" <?php 

                           if(!empty($driveUserProfile[0])){  
                              echo ($driveUserProfile[0]['vehicle_doc_status'] == '1')?'selected':'';
                            } ?>>Yes</option>
                           <option value="2" <?php 
                           if(!empty($driveUserProfile[0])){  
                              echo ($driveUserProfile[0]['vehicle_doc_status'] == '2')?'selected':'';
                            }
                           ?>>NO</option>
                        </select>
                        <div id="vehicle_reg_documents_id_error"></div>
                     </div>

                     <div id="vehicle_registrat_div_id">
                     <div class="select-nemt">
                     <div class="col-md-6 edit_box">	
                     <label><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Vehicle inspections</label>
                     <div class="form-group brows-doc">
                     <input class="file" type="file" name="vehicle_inspection_doc" id="vehicle_inspection_doc">
                     <div class="input-group col-xs-12">
                        <input type="text" class="form-control"  style="pointer-events: none;" placeholder="Vehicle inspections" name="" value="">
                        <input type="hidden" class="form-control"  style="pointer-events: none;" placeholder="Vehicle inspections" name="vehicle_inspections_doc_old" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['vehicle_inspections_doc']; } ?>">
                        <span class="input-group-btn">
                           <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
                        </span>
                     </div>

                     <div><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['vehicle_inspections_doc']; } ?></div>
                     </div>
                     </div>

                     <div class="row-comm" id="vehicle_inspection_date_id">
                     <div class="col-md-6 edit_box">	
                     <label for="vied" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Vehicle inspection expiry date</label>
                     <input type="text" name="vehicle_inspection_expiry_date" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['vehicle_inspections_expire_dt']; } ?>" id="vehicle_inspection_expiry_date" class="form-control date-picker oneYearDate" placeholder="vehicle inspection expiry date" autocomplete="off" readonly>
                     </div>
                     </div>																									
                     </div>	


                     <div class="col-md-6 edit_box">	
                     <label><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Vehicle maintenance records</label>
                     <div class="form-group brows-doc">
                     <input class="file" type="file" name="vehicle_maintainance_record_doc" id="vehicle_maintainance_record_doc">
                     <div class="input-group col-xs-12">
                     <input type="text" class="form-control"  style="pointer-events: none;" placeholder="Vehicle maintenance records" name="" value="">
                     
                     <input type="hidden" class="form-control"  style="pointer-events: none;" placeholder="Vehicle maintenance records" name="vehicle_maintnc_record_doc_old" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['vehicle_maintnc_record_doc']; } ?>">
                     <span class="input-group-btn">
                     <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
                     </span>
                     </div>
                     <div><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['vehicle_maintnc_record_doc']; } ?></div>
                     </div>
                     </div>


                     <div class="select-nemt">	
                     <div class="col-md-6 edit_box">	
                     <label><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Vehicle insurance</label>
                     <div class="form-group brows-doc">
                     <input class="file" type="file" name="vehicle_insurance_record_doc" id="vehicle_insurance_record_doc">
                     <div class="input-group col-xs-12">
                     <input type="text" name="" class="form-control"  style="pointer-events: none;" placeholder="Vehicle insurance" value="">
                     <input type="hidden" name="vehicle_insurance_doc_old" class="form-control"  style="pointer-events: none;" placeholder="Vehicle insurance" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['vehicle_insurance_doc']; } ?>">
                     <span class="input-group-btn">
                     <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
                     </span>
                     </div>
                     <div><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['vehicle_insurance_doc']; } ?></div>
                     </div>
                     </div>
                     <div class="row-comm" id="vehicle_insurance_record_date_id">
                     <div class="col-md-6 edit_box">
                     <label for="vied1" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Vehicle insurance expiry date</label>
                     <input type="text" name="vehicle_insurance_expiry_date" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['vehicle_insurance_expire_dt']; } ?>" id="vehicle_insurance_expiry_date" class="form-control date-picker oneYearDate" placeholder="vehicle insurance expiry date" autocomplete="off" readonly>
                     </div>
                     </div>				
                     </div>


                     <div class="select-nemt">	
                     <div class="col-md-6 edit_box">	
                     <label><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Vehicle registration</label>
                     <div class="form-group brows-doc">
                        <input class="file" type="file" name="vehicle_registration_doc" id="vehicle_registration_doc">
                        <div class="input-group col-xs-12">
                           <input type="text" class="form-control" id="" name=""  style="pointer-events: none;" placeholder="Vehicle insurance" value="">
                            <input type="hidden" class="form-control" id="vehicle_regi_doc_old" name="vehicle_regi_doc_old"  style="pointer-events: none;" placeholder="Vehicle insurance" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['vehicle_regi_doc']; } ?>">
                           <span class="input-group-btn">
                           <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
                           </span>
                        </div>
                        <div><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['vehicle_regi_doc']; } ?></div>
                     </div>
                     </div>

                     <div class="row-comm" id="vehicle_registrat_date_id">
                     <div class="col-md-6 edit_box">	
                     <label for="vied1" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Vehicle registration expiry date</label>
                     <input type="text" name="vehicle_registration_expiry_date" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['vehicle_regi_expire_dt']; } ?>" id="vehicle_registration_expiry_date" class="form-control date-picker oneYearDate" placeholder="vehicle registration expiry date" autocomplete="off" readonly>
                     </div>
                     </div>

                     </div>
                     </div>

                     <div class="col-md-6 edit_box">	

                     <label for="lbl_ovb" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Do you operate your own vehicle for your own business? </label>
                     <select class="form-control" id="own_vehicle_dd" name="own_vehicle_dd">
                     <option value="" diable seleted hidden>Select</option>
                     <option value="1" <?php 
                     if(!empty($driveUserProfile[0])){ 
               echo ($driveUserProfile[0]['own_business_status']== '1')?'selected':'';
                      } ?>
                     >YES</option>
                     <option value="2" <?php 
                     if(!empty($driveUserProfile[0])){ 
               echo ($driveUserProfile[0]['own_business_status']== '2')?'selected':'';
                      }?>
                  >NO</option>				
                     </select>
                     <div id="own_vehicle_dd_error"></div>
                     <span class="error_usertype"></span>
                     </div>	

                     <div id="own_bussin_show">
                     <div class="col-md-6 edit_box">	
                     <label for="lbl_type_of_sevices" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon1.png" alt="" />Types of service for own bussiness</label>
                     <select class="form-control" name="type_of_sevices" id="type_of_sevices">
                     <option value="" class="selecttxt" disable selected hidden>Select</option>
                     <?php 
                     if(!empty($typeOfServices)){
                     foreach($typeOfServices as $services) {
                     ?>
                     <option value="<?php echo $services['type_id']; ?>" <?php 
                   if(!empty($driveUserProfile[0])){ 
                     echo ($driveUserProfile[0]['own_business_status'] ==  $services['type_id'])?'selected':'';
                   } ?>
                      ><?php echo $services['type_title']?></option>
                     <?php } } else { ?>	
                     <option value=""></option>
                     <?php } ?>	
                     </select>							
                     </div>


                     <div class="col-md-6 edit_box">
                     <label for="lbl_veh_reg" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Vehicle registration</label>
                     <div class="form-group brows-doc">
                     <input class="file" type="file" name="own_vehicle_reg_doc" id="own_vehicle_reg_doc" value="<?php echo set_value('own_vehicle_reg_doc'); ?>">
                     <div class="input-group col-xs-12">
                     <input type="text" name="" class="form-control"  style="pointer-events: none;" placeholder="Vehicle registration doc" value="">
                     <input type="hidden" name="own_bus_vehicle_reg_doc_old" class="form-control"  style="pointer-events: none;" placeholder="Vehicle registration doc" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['own_bus_vehicle_reg_doc']; } ?>">

                     <span class="input-group-btn">
                     <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
                     </span>
                     </div>

                     <div><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['own_bus_vehicle_reg_doc']; } ?></div>
                     </div>
                     </div>

                     <div class="col-md-6 edit_box">
                     <label for="lbl_insu_pol" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Insurance policy</label>
                     <div class="form-group brows-doc">
                     <input class="file" type="file" name="insurance_policy_doc" id="insurance_policy_doc" value="<?php echo set_value('insurance_policy_doc'); ?>"> 
                     <div class="input-group col-xs-12">
                     <input type="text" class="form-control" name=""  style="pointer-events: none;"  placeholder="Insurance policy" value="">
                     <input type="hidden" class="form-control" name="own_bus_insurance_pol_doc_old"  style="pointer-events: none;"  placeholder="Insurance policy" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['own_bus_insurance_pol_doc']; } ?>">

                     <span class="input-group-btn">
                     <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
                     </span>
                     </div>
                     <div><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['own_bus_insurance_pol_doc']; } ?></div>
                     </div>
                     </div>

                     <div class="col-md-6 edit_box">
                     <label for="lbl_inspection" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Inspection</label>
                     <div class="form-group brows-doc">
                     <input class="file" type="file" name="inspection_doc" id="inspection_doc" value="<?php echo set_value('inspection_doc'); ?>">
                     <div class="input-group col-xs-12">
                     <input type="text" class="form-control" name=""  style="pointer-events: none;" placeholder="Inspection" value="">
                     <input type="hidden" class="form-control" name="own_bus_inspection_doc_old"  style="pointer-events: none;" placeholder="Inspection" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['own_bus_inspection_doc']; } ?>">
                     <span class="input-group-btn">
                     <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
                     </span>
                     </div>
                     <div><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['own_bus_inspection_doc']; } ?></div>
                     </div>
                     </div>

                     <div class="col-md-6 edit_box">
                     <label for="lbl_tax_id" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Tax ID or EIN number</label>
                     <div class="form-group brows-doc">
                     <input class="file" type="file" name="tax_id_or_ein_no" id="tax_id_or_ein_no" value="<?php echo set_value('tax_id_or_ein_no'); ?>">
                     <div class="input-group col-xs-12">
                     <input type="text" class="form-control" name=""  style="pointer-events: none;" placeholder="Tax ID or EIN number" value="">
                     <input type="hidden" class="form-control" name="own_bus_taxid_einno_doc_old"  style="pointer-events: none;" placeholder="Tax ID or EIN number" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['own_bus_taxid_einno_doc']; } ?>">
                     <span class="input-group-btn">
                     <button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
                     </span>
                     </div>
                     <div><?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['own_bus_taxid_einno_doc']; } ?></div>
                     </div>		
                     </div>

                     <div class="col-md-6 edit_box">
                     <label for="lbl_own_bs_vehicle_details" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Vehicle type, year, make and model</label>
                     <input type="text" class="form-control vehicle_details" id="own_buss_vehicle_details" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['own_bus_vehicle_detiail']; } ?>" name="own_buss_vehicle_details" placeholder="Vehicle type, year, make and model" maxlength="255" autocomplete="off">
                     </div>
                     </div>

                     <div class="col-md-6 edit_box">
                     <label for="lbl_comp_vehic" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Do you operate company vehicle? </label>
                     <select class="form-control" id="company_vehicle_dropdown" name="company_vehicle_dropdown">
                     company_vehicle_dropdown
                     <?php //if(isset($driveUserProfile[0]->company_vehicle_status)){ ?>
                        <option value="" diable seleted hidden>Select</option>
                         <option value="1" <?php 
                      if(!empty($driveUserProfile[0])){  
                         echo ($driveUserProfile[0]['company_vehicle_status'] == '1')?'selected':'';
                      } ?>
                        >YES</option>
                        <option value="2" <?php 

                        if(!empty($driveUserProfile[0])){  
                         echo ($driveUserProfile[0]['company_vehicle_status'] == '2')?'selected':'';
                      }?>
                      >NO</option> 
                        <?php //}else{ ?>
                          <!--  <option value="" diable seleted hidden>Select</option> 
                           <option value="1">YES</option>
                        <option value="2">NO</option>   -->    
                          <?php //} ?>
                     
                     
                     </select>
                     <div id="company_vehicle_dropdown_error"></div>
                     <span class="error_usertype"></span>
                     </div>	

                     <div class="row-comm" id="compa_vehicle_show">
                     <div class="col-md-6 edit_box">
                     <div class="form-group">
                     <label for="lbl_trans_comp1" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Name of transportation company</label>
                     <input type="text" class="form-control name" id="transport_comp" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['comp_veh_transp_comp']; } ?>" name="transport_comp" placeholder="Name of transportation company" maxlength="100" autocomplete="off">
                     </div>	
                     <div class="form-group">
                     <label for="lbl_vehicle_type" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Vehicle type, year, make and model</label>
                     <input type="text" class="form-control vehicle_details" id="vehicle_details" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['comp_veh_vehicle_details']; } ?>" name="vehicle_details" placeholder="Vehicle type, year, make and model" maxlength="255" autocomplete="off">
                     </div>												
                     </div>

                     <div class="col-md-6 edit_box">
                     <div class="form-group">
                     <label for="lbl_com_type_of_sevices" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon1.png" alt="" />Types of service for company vehicle</label>
                     <select class="form-control" name="com_veh_type_of_sevices" id="com_veh_type_of_sevices">
                     <option value="" class="selecttxt" disable selected hidden>Select</option>
                     <?php 
                     if(!empty($typeOfServices)){
                     foreach($typeOfServices as $services) {
                     ?>
                     
                     <option value="<?php echo $services['type_id']; ?>" <?php 
                     if(!empty($driveUserProfile[0])){ 
                        $comp_vel_services = $driveUserProfile[0]['comp_veh_services'];
                        echo ($comp_vel_services == $services['type_id'])?'selected':''; 
                     } 
                     ?>><?php echo $services['type_title']?></option>
                     <?php } } else { ?>	
                     <option value=""></option>
                     <?php } ?>	
                     </select>							
                     </div>			
                     </div>								
                     </div>	




                     <div class="col-md-6 edit_box">
                     <label><img src="<?php echo base_url(); ?>assets/img/emer_no.png">Emergency Contact Name:</label>	
                     <div class="edit_inpt"><input type="text" placeholder="Emergency contact name" maxlength="50" name="emergency_contact_name" id="eme_name" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['emergency_contactname']; } ?>" class="form-control">
                     <div id="eme_name_error"></div>
                     </div>
                     <?php echo form_error('emergency_contact_name', '<div class="error">', '</div>'); ?>
                     </div>
                     <div class="col-md-6 edit_box">
                     <label><img src="<?php echo base_url(); ?>assets/img/mob_no.png">Emergency Contact Number:</label>	
                     <div class="edit_inpt"><input type="text" placeholder="Emergency contact number" maxlength="20" name="emergency_contact_number" id="eme_number" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['emergency_contactno']; } ?>" class="form-control">
                     <div id="eme_number_error"></div>

                     </div>
                     <?php echo form_error('emergency_contact_number', '<div class="error">', '</div>'); ?>
                     </div>
                     <div class="col-md-12 edit_box">
                     <label><img src="<?php echo base_url(); ?>assets/img/sign.png">Are you able to Provide Signatures:</label>	
                     <div class="edit_inpt_radio radio_cust">												  
                     <span class="radio_btn">													
                     <input type="radio" id="yes" name="signature" value="1" <?php if(!empty($driveUserProfile[0])){ if($driveUserProfile[0]['signature'] == "1"){ echo "checked=checked"; } } ?>>
                     <label for="yes">Yes</label>
                     </span>
                     <span class="radio_btn">
                     <input type="radio" id="no" name="signature" value="0" <?php if(!empty($driveUserProfile[0])){ if($driveUserProfile[0]['signature'] == "0"){ echo "checked=checked"; } } ?>>
                     <label for="no">No</label>
                     </span>
                     </div>
                     <?php echo form_error('signature', '<div class="error">', '</div>'); ?>											
                     <div id="file_signature">
                     <div class="upld_sign">
                     <span class="input-group-btn">
                     <!-- image-preview-input -->
                     <div class="btn btn-primary image-preview-input">
                     <span class="image-preview-input-title">Upload Signatures</span>
                     <input class="upsign" type="file" name="sign_file_upload" id="sign_file_upload"> <!-- rename it -->
                     <input class="upsign" type="hidden" name="sign_file_upload_old" id="sign_file_upload" value="<?php if(!empty($driveUserProfile[0])){ echo $driveUserProfile[0]['signature_file_upload']; } ?>"> 
                     </div>													
                     </span>												
                     </div>
                     </div>											
                     </div>
                     </div>
                     </div>
                  </div>
                  <div class=" col-md-12">								
                  <div class="subedit_form">
                  <input class="req_btn comp_btn" type="submit" name="edit_profile_save" value="Submit" id="driver_edit_profile">
                  </div>									
                  </div>
                  </div>
               </div>	
            </form>
         </div>
      </div>
   </div>
</div>
<style type="text/css">
a {
cursor: pointer !important; 
}
.image-upload > input
{
display: none;
}

.image-upload img
{
width: 50px;
cursor: pointer;
}

#fileErrorType p{
   color: red;
}
</style>
<?php $this->load->view("user/footer"); ?>
<script src="<?php echo base_url(); ?>assets/js/driverEditPage.js"></script>

