<?php $this->load->view("user/header"); ?>
<?php $this->load->view("driverUser/driverHeader"); ?>

<div class="noti_wrapp">
	<div class="container">
		<div class="row">
			<div class="noti_inner">
				<h3 class="noti_head">NOtification</h3>
				<div id="success_message"></div>
				<div id="error_message"></div>
				<div class="noti_box">
					<div class="row">
						<div class="noti_sec">
							<div class="col-md-4 lt_noti">
								<div class="lt_noti_wrap">
									<div class="lt_not_tit">All Notification List <span class="delete_sec"><a href="javascript:void(0)" class="delete_notification"><i class="fa fa-trash-o" aria-hidden="true"></i></a></span></div>
									<div class="not_tit_wrap">
										<div id="noti_box">
												
													<a href="" data-related="id_1">	
													<input type="hidden" id="delete_notification_value" value="">
													<div class="notif_val" id="8"></div>									
													<div class="noti_box_inn div_id_1 act_noti">
													<div class="lt_rat"><i class="fa fa-star" aria-hidden="true"></i></div>
													<div class="rt_box">
														<div class="not_head">notification title 1</div>
														<div class="not_txt">
															Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...														</div>
														<div class="not_ago">2017-11-16 23:19:39</div>
													</div>
												</div>
											</a>
												
													<a href="" data-related="id_2">	
													<input type="hidden" id="delete_notification_value" value="">
													<div class="notif_val" id="11"></div>									
													<div class="noti_box_inn div_id_2 bg_chan">
													<div class="lt_rat"><i class="fa fa-star" aria-hidden="true"></i></div>
													<div class="rt_box">
														<div class="not_head">notification 5</div>
														<div class="not_txt">
															Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut...														</div>
														<div class="not_ago">2017-11-16 23:20:25</div>
													</div>
												</div>
											</a>
														
										</div>	
									</div>
								</div>
							</div>
							<div class="col-md-8 rt_noti">
								<div class="not_mess_wrapp">
									<div class="rt_not_tit">
										message box										
										<span class="delete_sec"><a href="javascript:void(0)" class="delete_notification" id="delete_notification"><i class="fa fa-trash-o" aria-hidden="true"></i></a></span>
										<span class="delete_rat"><a href="#"><i class="fa fa-star" aria-hidden="true"></i></a></span>
									</div>
																		
									<div class="noti_mess" id="id_1">
										
										<div class="not_new">notification title 1</div>
										<div class="not_mes_txt">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum										</div>
									</div>
																		
									<div class="noti_mess" id="id_2">
										
										<div class="not_new">notification 5</div>
										<div class="not_mes_txt">
											Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum										</div>
									</div>
										
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<?php $this->load->view("user/footer"); ?>