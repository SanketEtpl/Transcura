<?php $this->load->view("user/header"); ?>
<?php $this->load->view("driverUser/driverHeader"); ?>

<div class="noti_wrapp">
	<div class="container">
		<div class="row">
			<div class="noti_inner">
				<h3 class="noti_head">Notification</h3>
				<div id="success_message"></div>
				<div id="error_message"></div>
				<div class="noti_box">
					<div class="row">
						<div class="noti_sec">
							<div class="col-md-4 lt_noti" id="mynofilst">
								<div class="lt_noti_wrap">
									<div class="lt_not_tit">All Notification List <span class="delete_sec"><a href="javascript:void(0)" class="driver_delete_notification"><i class="fa fa-trash-o" aria-hidden="true"></i></a></span></div>
									<div class="not_tit_wrap">
										<div id="noti_box">
											<?php 
												$i=1;
												if(!empty($notification)){ 
													foreach($notification as $key => $value)
													{ 
															if(count($notification)>1 && $value['notification_flag']==2 && $i!=1){
																$myclasscus = "unread_not";
															}elseif($i==1 && $value['notification_flag']==2){
																$myclasscus='';
															}else{ $myclasscus='';}
													?>	
													<a href="" data-related="<?php echo 'id_'.$i; ?>">	
													<input type="hidden" id="driver_delete_notification_value" value="">
													<div class="notif_val" id="<?php echo $value['id'];?>"></div>									
													<div class="noti_box_inn <?php echo $myclasscus;?> <?php echo 'div_id_'.$i; ?> <?php if($i == 1){ echo "act_noti"; }elseif($i%2==0){ echo "bg_chan"; } ?> " data-notId="<?php echo $value['id'];?>">
													<div class="lt_rat"><i class="fa fa-star" aria-hidden="true"></i></div>
													<div class="rt_box">
														<div class="not_head"><?php echo $value['n_title']; ?></div>
														<div class="not_txt">
															<?php
																$string = $value['n_full_description'];
																if (strlen($string) > 60) {													
																	$stringCut = substr($string, 0, 100);
																	$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
																}
																echo $string;
															?>
														</div>
														<div class="not_ago"><?php echo $value['created_on']; ?></div>
													</div>
												</div>
											</a>
											<?php $i++; }  } ?>			
										</div>	
									</div>
								</div>
							</div>
							<div class="col-md-8 rt_noti">
								<div class="not_mess_wrapp">
									<div class="rt_not_tit">
										message box										
										<span class="delete_sec"><a href="javascript:void(0)" class="driver_delete_notification" id="driver_delete_notification"><i class="fa fa-trash-o" aria-hidden="true"></i></a></span>
										<span class="delete_rat"><a href="javascript:void(0)"><i class="fa fa-star" aria-hidden="true"></i></a></span>
									</div>
									<?php 
										$i=1;
										if(!empty($notification)){ 
										foreach($notification as $key => $value)
										{
									?>									
										<div class="noti_mess" id="<?php echo 'id_'.$i; ?>">
										<div class="not_new"><?php echo $value['n_title']; ?></div>
										<div class="not_mes_txt">
											<?php echo $value['n_full_description']; ?>
										</div>
									</div>
									<?php $i++; }  } ?>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>
<?php $this->load->view("user/footer"); ?>
<script>
jQuery(document).ready(function(){
	var noti_id = $(".notif_val:first").attr('id');
	jQuery.ajax({
			url:baseURL+'frontend/DriverUserTypeController/notificationRead',
			type: "POST",
			dataType :"json",
			data: { id: noti_id },
			success: function(data) {				
				if(data.status=="true"){ 
					jQuery(this).removeClass("unread_not");
					if(data.count==0)
					{
						$('#count_notification').find('span').remove();						
					}
					else
					{
						$('#count_notification').append("<span class='not_cnt'>"+data.count+"<span>");			
					}
					
					//alert(data.count+"hello by");
					//location.reload();
					/*if (window.location.href.indexOf('reload')==-1) {
					     window.location.replace(window.location.href+'?reload');
					}*/
				}
				
			}
		});
	jQuery('#mynofilst .not_tit_wrap #noti_box .noti_box_inn').click(function(){
		var notificationId = jQuery(this).attr('data-notid');
		jQuery(this).removeClass("unread_not");
		jQuery.ajax({
			url:baseURL+'frontend/DriverUserTypeController/notificationRead',
			type: "POST",
			dataType :"json",
			data: { id: notificationId },
			success: function(data) {
				if(data.status=="true"){ 				
					jQuery(this).removeClass("unread_not");
					if(data.count==0)
					{
						$('#count_notification').find('span').remove();						
					}	
					else
					{
						$('#count_notification').append("<span class='not_cnt'>"+data.count+"<span>");						
					}				
				}				
			}
		});
	});
});
</script>