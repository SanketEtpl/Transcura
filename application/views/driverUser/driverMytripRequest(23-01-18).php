<?php $this->load->view("user/header"); ?>
<?php $this->load->view("driverUser/driverHeader"); ?>
<div class="my_rides_wrapp">
	<div class="container">
		<div class="row">
			<div class="all_rides_box">
				<div class="rides_tab_new">	
					<div id="ridetab">						
						<div class="rides_tab">
							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#ride_request_trip">RIDE REQUEST TRIPS</a></li>
								  <li><a data-toggle="tab" href="#accepted_trip">MY ACCEPTED TRIPS</a></li>
								  <li ><a data-toggle="tab" href="#rejcted_trip">MY REJECTED TRIPS</a></li>
							</ul>
						</div>					
					</div>
					<div id="success_message" class="err_mes" style="color: green;"></div>
					<div id="error_message" class="err_mes" style="color: red;"></div>
					<div class="tab-content all_content" id="all_content">
					<div id="ride_request_trip" class="tab-pane fade in active">
						<h3 class="rid_head">Ride request</h3>
						<div class="rides_table">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th>Name</th>
										<th>Pick up point</th>
										<th>Drop point</th>
										<th>Distance</th>
										<th>Date / Time</th>
										<th>Action</th>
									  </tr>
									</thead>
									<tbody>										
											<?php 
											foreach ($ride_request as $key => $req) { ?>
											<tr>
												<td width="15%"><span class="rid_nme"><?php echo $req['full_name'];?></span></td>
												<td width="15%"><?php echo $req['source_address'];?></td>
												<td width="15%"><?php echo $req['destination_address'];?></td>
												<td width="8%"><?php echo $req['sr_total_distance'];?></td>
												<td width="15%" id="ride_req_get_id" style="display:none;"><?php echo $this->session->userdata('user_id'); ?></td>
												<td width="15%" id="ride_req_trip_id" style="display:none;"><?php echo $req['sched_id']; ?></td>
												<td width="15%"><?php echo $req['created_date'];?></td>
												<td width="15%">
													<button class="btn btn-ride-request-accepted" id="ride_req_id_<?php echo $req['user_id']; ?>">Accept</button>
													<button class="btn btn-ride-request-rejected" id="ride_req_rej_id_<?php echo $req['user_id']; ?>">Reject</button>
												</td>
											</tr>
											<?php } ?>																			
										</tbody>
								  </table>
							</div>
						</div>
					  </div>
					  <div id="accepted_trip" class="tab-pane fade">
						<h3 class="rid_head">My Accepted Trips</h3>
						<div class="rides_table">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th>Name</th>
										<th>Trip ID</th>
										<th>Date / Time</th>
										<th>View Details</th>
									  </tr>
									</thead>
									<tbody>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>29-Jan-2018 2:00 PM</td>
											
											<td><button class="btn btn-view" >View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>29-Jan-2018 2:00 PM</td>
											
											<td><button class="btn btn-view">View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>29-Jan-2018 2:00 PM</td>
											
											<td><button class="btn btn-view">View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>29-Jan-2018 2:00 PM</td>
											
											<td><button class="btn btn-view">View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>29-Jan-2018 2:00 PM</td>
											
											<td><button class="btn btn-view">View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>29-Jan-2018 2:00 PM</td>
											<td><button class="btn btn-view">View</button></td>
										</tr>										
									</tbody>
								  </table>
							</div>
						</div>
					  </div>
					  <div id="rejcted_trip" class="tab-pane fade">
						<h3 class="rid_head">My Rejected Trip</h3>
						<div class="rides_table">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th>Name</th>
										<th>Trip ID</th>
										<th>Date / Time</th>
										
										<th>View Details</th>
									  </tr>
									</thead>
									<tbody>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>01-Aug-2017 2:00 PM</td>
											
											<td><button class="btn btn-view" >View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>01-Aug-2017 2:00 PM</td>
											
											<td><button class="btn btn-view" >View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>01-Aug-2017 2:00 PM</td>
											
											<td><button class="btn btn-view" >View</button></td>
										</tr>										
									</tbody>
								  </table>
							</div>
						</div>
					  </div>	
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php $this->load->view("user/footer"); ?>
