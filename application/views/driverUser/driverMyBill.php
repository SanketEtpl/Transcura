<?php $this->load->view("user/header"); ?>
<?php $this->load->view("driverUser/driverHeader"); ?>
<div class="my_rides_wrapp">
	<div class="container">
		<div class="row">
			<div class="all_rides_box">
				<div class="rides_tab_new">	
					<div id="ridetab">						
						<div class="rides_tab">
							<ul class="nav nav-tabs">
								  <li class="active"><a data-toggle="tab" href="#billing_date">BILLING DATE</a></li>
								  <li ><a data-toggle="tab" href="#payment_statement">PAYMENT STATEMENTS</a></li>
								  <li ><a data-toggle="tab" href="#trip_discrepancy">TRIP DISCREPANCY</a></li>
							</ul>
						</div>					
					</div>
					<div id="success_message" class="err_mes"></div>	
					<div id="error_message" class="err_mes"></div>
					<div class="tab-content billing_all_content" id="billing_all_content">

					<div id="billing_date" class="tab-pane fade in active">
						<div class="form_tsb_date">
							<form class="form-inline form_biilling_date" id="billing_data" action="frontend/DriverUserTypeController/exportPdf" method="post">
							    <div class="form-group col-md-3 padd-left0">
									<label class="form_label">START DATE</label>
								    <input id="start_date" type="text" class="form-control date-picker start_date" name="start_date" autocomplete="off" readonly/>
							    </div>
							    <div class="form-group col-md-3">
									<label class="form_label">END DATE</label>
									<input id="end_date" type="text" class="form-control date-picker end_date" name="end_date" autocomplete="off" readonly/>	
							    </div>
							    <input type="hidden" name="billingIdData" id="billingIdData">
							    <button type="button" class="btn btn_searchdatehere" id="billing_date_search">SEARCH</button>
							</form>
						</div>
						<div class="table_dat_time_board">
							<div class="serch_re_btn">
								<h3 class="rid_head">SEARCH RESULT</h3>
								<button type="button" class="btn BTN_download" id="pdf_download_billing">DOWNLOAD PDF</button>
							</div>
							<div class="rides_table" id="driver_billing">
								<div class="table-responsive">
									<table class="table table-striped" id="listOfDriverBilling">
										<thead>
										  <tr>
											<th>DATE/TIME</th>
											<th>Trip ID</th>
											<th>BILLING AMOUNT</th>
											<th>DISTANCE</th>
											<th>VIEW DETAILS</th>
										  </tr>
										</thead>
										<tbody>
											
										</tbody>
									  </table>
								</div>
								

							</div>
						</div>
					</div>
					<div id="payment_statement" class="tab-pane fade">
						<div class="form_tsb_date">
							<form class="form-inline form_biilling_date" id="payment_data" action="frontend/DriverUserTypeController/paymentExportPdf" method="post">
								<!-- <div class="form-group col-md-3 padd-left0">
									<label class="form_label">PLEASE SELECT THE DATE</label>
									<input id="date_of_birth" type="text" class="form-control date-picker hasDatepicker" name="date_of_birth" autocomplete="off" readonly="" value=""/>
								</div> -->
								<div class="form-group col-md-3 padd-left0">
									<label class="form_label">START DATE</label>
								    <input id="pstart_date" type="text" class="form-control date-picker start_date" name="start_date" autocomplete="off" readonly/>
							    </div>
							    <div class="form-group col-md-3">
									<label class="form_label">END DATE</label>
									<input id="pend_date" type="text" class="form-control date-picker end_date" name="end_date" autocomplete="off" readonly/>	
							    </div>
							    <input type="hidden" name="paymentIdData" id="paymentIdData">
								<button type="button" class="btn btn_searchdatehere" id="payment_search">SEARCH</button>
							</form>
						</div>
						<div class="table_dat_time_board">
							<div class="serch_re_btn">
								<h3 class="rid_head">SEARCH RESULT</h3>
								<button type="button" class="btn BTN_download" id="pdf_download_payment_billing">DOWNLOAD PDF</button>
							</div>
							<div class="rides_table">
								<div class="table-responsive">
									<table class="table table-striped" id="listOfPaymentStatement">
										<thead>
										  <tr>
											<th>DATE/TIME</th>
											<th>Trip ID</th>
											<th>BILLING AMOUNT</th>
											<th>DISTANCE</th>
											<th>VIEW DETAILS</th>
										  </tr>
										</thead>
										<tbody>
										
										</tbody>
									  </table>
								</div>
							</div>
						</div>
					  </div>
					  <div id="trip_discrepancy" class="tab-pane fade">
							<div class="form_tsb_trip">
								<div class="trip_form_head">
									<h3>TRIP SERVICE DATE:</h3>
								</div>
								<div class="trip_form_here">
									<form class="form-inline form_biilling_date">
										<div class="form-group col-md-3 padd-left0">
											<label class="form_label">TRIP SERVICE DATE:</label>
											<input id="" type="text" class="form-control date-picker hasDatepicker" name="date_of_birth" autocomplete="off" readonly="" value=""/>
										
										  </div>
										  <div class="form-group col-md-3">
												<label class="form_label">TRIP ID NUMBER</label>
												<input type="text" class="form-control" placeholder="Enter ID Number" />
										  </div>
										  <div class="form-group col-md-3">
												<label class="form_label">DISPUTE PAID TRIP AMOUNTS</label>
												<input type="text" class="form-control" placeholder="Enter Amount" />
										  </div>
										  <div class="form-group col-md-3 padd-right0">
												<label class="form_label">NON-PAYMENT</label>
												<input type="text" class="form-control"/>
										  </div>
									</form>
								</div>
								<div class="trip_service_btn">
									<button type="button" class="btn servic_btn">Proceed</button>
									<button type="button" class="btn servic_btn_sec">Cancel</button>
								</div>
							</div>
					  </div>
					  <div id="custom" class="tab-pane fade">
						<h3 class="rid_head">Customer Care Information</h3>
						<p>Coming Soon</p>
					  </div>
					</div>
					<!--schedule trip view-->
					<div id="driver_billing_view" style="display:none">
						<div class="ride_trip_deta">
							<h3 class="heal_head">
								Billing View
								<span class="back_box"><button class="back_btn" id="billing_view_bk" name="billing_view_bk">Back</button></span>
							</h3>
							<div class="sched_deta_box">								
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip ID :</label>
											<div class="rid_info" id="billing_trip_id"></div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Name :</label>
											<div class="rid_info" id="billing_name"><span class="view_sec viewnew"><a href="#" class="com_btn" title="View"><i class="fa fa-eye"></i></a></span></div>
										</div>
										<!--<div class="col-md-7">
											<span class="view_sec"><a href="#" class="com_btn">View Details</a></span>
										</div>-->
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip Date :</label>
											<div class="rid_info" id="billing_trip_date"></div>
										</div>
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Pick Up Point :</label>
											<div class="rid_info" id="billing_pick_up_point"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Drop Point Healthcare Providers Location :</label>
											<div class="rid_info" id="billing_drop_point"></div>
										</div>
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Total Distance :</label>
											<div class="rid_info" id="billing_total_distance"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Cost :</label>
											<div class="rid_info" id="billing_cost"></div>
										</div>
									</div>
								</div>									
							</div>	
						</div>
					</div>
					<!--complete trip view-->
					<div id="payment_view" style="display:none">						
						<div class="ride_trip_deta">
							<h3 class="heal_head">
								Billing View
								<span class="back_box"><button class="back_btn" id="payment_view_bk" name="billing_view_bk">Back</button></span>
							</h3>
							<div class="sched_deta_box">								
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip ID :</label>
											<div class="rid_info" id="payment_trip_id"></div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Name :</label>
											<div class="rid_info" id="payment_name"><span class="view_sec viewnew"><a href="#" class="com_btn" title="View"><i class="fa fa-eye"></i></a></span></div>
										</div>
										<!--<div class="col-md-7">
											<span class="view_sec"><a href="#" class="com_btn">View Details</a></span>
										</div>-->
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip Date :</label>
											<div class="rid_info" id="payment_trip_date"></div>
										</div>
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Pick Up Point :</label>
											<div class="rid_info" id="payment_pick_up_point"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Drop Point Healthcare Providers Location :</label>
											<div class="rid_info" id="payment_drop_point"></div>
										</div>
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Total Distance :</label>
											<div class="rid_info" id="payment_total_distance"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Cost :</label>
											<div class="rid_info" id="payment_cost"></div>
										</div>
									</div>
								</div>									
							</div>	
						</div>
					</div>					
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view("user/footer"); ?>
<script type="text/javascript">
$(document).ready(function(){
	$.ajax({
		type:"POST",
		url:baseURL+"frontend/DriverUserTypeController/driver_billing_details",
		data:{},
		datType:"json",
		async:false,
		success:function(response){
			if(response.status == true){	
			  $("#listOfDriverBilling tbody").html(response.rows);	    
      		}		
		}
	});	
	$.ajax({
		type:"POST",
		url:baseURL+"frontend/DriverUserTypeController/get_all_payment_statement",
		data:{},
		datType:"json",
		async:false,
		success:function(response){
			if(response.status == true){	
			  $("#listOfPaymentStatement tbody").html(response.rows);	    
      		}		
		}
	});	

});
</script>