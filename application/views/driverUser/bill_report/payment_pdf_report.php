<div class="DU_lsttb_wrapp">
    <div id="spy3"> 
        <div id="example1_wrapper" class="dataTables_wrapper">
            <div class="row ">
                <div class="col-sm-12">
                     <div class="DU_tp_lst">
                        <div class="back_wrapper">
                            <div class="back_left">
                               <img src="<?php echo base_url();?>images/imgpsh_fullsize14.png" width="200px">
                            </div>
                            <div class="back_left">
                               <center><strong>Payment Total Billing </strong></center>
                            </div>
                            <hr>
                        </div>
                    </div>
                    <br />
                    <table class="table dataTable div_job_load_outer" id="listJobs" role="grid" aria-describedby="example1_info" style="font-size:14px;">
                        <thead>
                            <tr role="row">
                                <th data-name="index" data-order="ASC" class="sorting" width="4%" align="left" style="font-size:14px;">Sr. No.</th>
                                <th data-name="trip_id" data-order="ASC" class="sorting" width="8%" align="left" style="font-size:14px;">Trip ID</th>
                                <th data-name="date" data-order="ASC" class="sorting" width="8%" align="left" style="font-size:14px;">Trip Date</th>
                                <th data-name="from" data-order="ASC" class="sorting" width="10%" align="left" style="font-size:14px;">From</th>
                                <th width="6%" class="sorting" width="10%" align="left" style="font-size:14px;">To</th>
                                <th width="8%" class="sorting" width="5%" align="left" style="font-size:14px;">Distance</th>
                                <th width="9%" class="sorting" width="5%" align="left" style="font-size:14px;">Total</th>                                                                 
                            </tr>

                        </thead>

                        <tbody class="div_job_load_inner">                          
                            <?php                          
                            $k = 1;
                            $totalBill ='';
                            foreach ($report as $value) {
                                $totalBill += $value['sr_total_cost'];
                                      ?> <tr>
                                    <td class="text-left"><?php echo $k; ?> </td>
                                    <td class="text-left"><?php echo $value['sr_trip_id']; ?> </td>
                                    <td class="text-left"><?php echo $value['sr_date']; ?> </td> 
                                    <td class="text-left"><?php echo $value['source_address']; ?> </td>
                                    <td class="text-left"><?php echo $value['destination_address']; ?> </td> 
                                    <td class="text-left"><?php echo $value['sr_total_distance']; ?> </td>
                                    <td class="text-left"><?php echo $value['sr_total_cost']; ?> </td>		                                    
                                </tr>
                                <?php
                                $k++;
                            }                            
                            ?>
                           
                        </tbody>
                    </table>  
                     <hr>
                     <table align="right">
                        <tbody> 
                            <tr><td>Total bill = <?php echo $totalBill; ?></td></tr>                  
                         </tbody>
                    </table> 
                </div>
            </div>
            <div class="DU_tab_foot">
                <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                </div>
                <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                </div>
            </div>
        </div>						
    </div>
</div>				 
</div>
</div>  