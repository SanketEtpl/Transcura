<?php $this->load->view("user/header"); ?>
<?php $this->load->view("driverUser/driverHeader"); ?>
<div class="myprof_wrapp">	
	<div class="probgimg" style="background-image:url('<?php echo base_url();?>assets/img/profile_bg.png');"></div>
		<div class="mypro_info_wrapp">		
		<div class="container">
			<div class="row">
				<div class="tp_pro_sect">
					<div class="pro_img_box" style="background-image:url('uploads/Driver/doc/<?php if(!empty($driveUserProfile[0]['picture'])) echo $driveUserProfile[0]['picture']; else echo "default.png"; ?>');">
					</div>
					<div class="pro_img_name">
						<div class="pro_name_box">
							<div class="pro_nme"><?php if(!empty($driveUserProfile[0]['full_name'])) echo $driveUserProfile[0]['full_name']; else echo "N/A"; ?></div>
							<div class="edit_prof_btn"><a href="<?php echo base_url()?>driver-edit-user-profile">Edit profile</a></div>
						</div>
						<div class="alert alert-success col-md-offset-1 col-md-8" id="success_message" style="display:none"></div>
						<div class="alert alert-danger col-md-offset-1 col-md-8" id="error_message" style="display:none"></div>			
					</div>
				</div>
				<div class="basic_info_wrapp driveruserinfo">
					<div class="row">
						<div class="col-md-6">
							<div class="info_head">Basic Information :</div>
							<div class="basic_box">
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/date_icon.png"><span>Date of birth:</span></div>
									<div class="bas_rt"><?php if(!empty($driveUserProfile[0]['date_of_birth'])) echo $driveUserProfile[0]['date_of_birth']; else echo 'N/A'; ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/mob_no.png"><span>Mobile number:</span></div>
									<div class="bas_rt"><?php if(!empty($driveUserProfile[0]['phone'])) echo $driveUserProfile[0]['phone']; else echo 'N/A'; ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/email_add.png"><span>Email address:</span></div>
									<div class="bas_rt"><?php if(!empty($driveUserProfile[0]['email'])) echo $driveUserProfile[0]['email']; else echo 'N/A'; ?></div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="info_head">Address :</div>
							<div class="basic_box add_box">
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/coun.png"><span>Country:</span></div>
									<div class="bas_rt"><?php if(!empty($driveUserProfile[0]['country_name']))  echo $driveUserProfile[0]['country_name']; else echo 'N/A'; ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/state.png"><span>State:</span></div>
									<div class="bas_rt"><?php echo (!empty($driveUserProfile[0]['state_name']))?$driveUserProfile[0]['state_name']: 'N/A'; ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/state.png"><span>City:</span></div>
									<div class="bas_rt"><?php echo (!empty($driveUserProfile[0]['city_name']))?$driveUserProfile[0]['city_name']: 'N/A'; ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/state.png"><span>Street:</span></div>
									<div class="bas_rt"><?php echo (!empty($driveUserProfile[0]['street']))?$driveUserProfile[0]['street']: 'N/A'; ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/zip.png"><span>Zip Code:</span></div>
									<div class="bas_rt"><?php echo (!empty($driveUserProfile[0]['zipcode']))?$driveUserProfile[0]['zipcode']: 'N/A'; ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/zip.png"><span>County:</span></div>
									<div class="bas_rt"><?php echo (!empty($driveUserProfile[0]['county']))?$driveUserProfile[0]['county']: 'N/A'; ?></div>
								</div>
							</div>
						</div>
						<div class="col-md-6 owninfo">
							<div class="info_head">Personal Information :</div>
							<div class="basic_box personal_box">
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/companyname.png" height="18"><span>Company name :</span></div>
									<div class="bas_rt"><?php echo (!empty($driveUserProfile[0]['company_name']))?$driveUserProfile[0]['company_name']:'N/A' ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/companyname.png" height="18"><span>Company address :</span></div>
									<div class="bas_rt per_doc"><?php echo (!empty($driveUserProfile[0]['company_address']))?$driveUserProfile[0]['company_address']: 'N/A'; ?></div>
								</div>
								<!-- <div class="basic_inner">
									<div class="bas_lt"><img src="<?php //echo base_url();?>assets/img/vehicledoc.png" height="18"><span>Vehicle documents :</span></div>
									<?php // if(!empty($driveUserProfile[0]['vehicle_doc'])) { ?>
									<div class="bas_rt"><span></span>
									<a href="<?php //echo base_url()."uploads/Driver/doc/".$driveUserProfile[0]['vehicle_doc'];?>" class="view-down" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a>
									<a href="<?php //echo base_url()."uploads/Driver/doc/".$driveUserProfile[0]['vehicle_doc'];?>" id="vehicle_doc_download" download="vehicle_doc" class="view-down">
									<i class="fa fa-download" aria-hidden="true"></i></a>
									</div>
									<?php //} else echo "N/A"; ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php //echo base_url();?>assets/img/per_doc.png"><span>Personal documents :</span></div>
									<?php //if(!empty($driveUserProfile[0]['personal_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="<?php //echo base_url()."uploads/Driver/doc/".$driveUserProfile[0]['personal_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="<?php //echo base_url()."uploads/Driver/doc/".$driveUserProfile[0]['personal_doc']; ?>" download="personal_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php //} else echo "N/A";  ?>
								</div> -->
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Social security card document :</span></div>
									<?php if(!empty($driveUserProfile[0]['social_security_card_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['social_security_card_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['social_security_card_doc']; ?>" download="social_serurity_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Driver license document :</span></div>
									<?php if(!empty($driveUserProfile[0]['driver_license_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['driver_license_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['driver_license_doc']; ?>" download="driver_license_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Drug test results document :</span></div>
									<?php if(!empty($driveUserProfile[0]['drug_test_results_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['drug_test_results_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['drug_test_results_doc']; ?>" download="drug_test_result_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Criminal back ground document :</span></div>
									<?php if(!empty($driveUserProfile[0]['criminal_back_ground_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['criminal_back_ground_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['criminal_back_ground_doc']; ?>" download="criminal_bg_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Motor vehicle record document :</span></div>
									<?php if(!empty($driveUserProfile[0]['motor_vehicle_record_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['motor_vehicle_record_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['motor_vehicle_record_doc']; ?>" download="motor_vehicle_record_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Act 33 and 34 clearance document :</span></div>
									<?php if(!empty($driveUserProfile[0]['act_33_and_34_clearance_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['act_33_and_34_clearance_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['act_33_and_34_clearance_doc']; ?>" download="act_33_and_34_clearance_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Vehicle inspections document :</span></div>
									<?php if(!empty($driveUserProfile[0]['vehicle_inspections_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['vehicle_inspections_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['vehicle_inspections_doc']; ?>" download="vehicle_inspections_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Vehicle maintenance record document :</span></div>
									<?php if(!empty($driveUserProfile[0]['vehicle_maintnc_record_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['vehicle_maintnc_record_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['vehicle_maintnc_record_doc']; ?>" download="vehicle_maintnc_record_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Vehicle insurance document :</span></div>
									<?php if(!empty($driveUserProfile[0]['vehicle_insurance_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['vehicle_insurance_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['vehicle_insurance_doc']; ?>" download="vehicle_insurance_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Vehicle register document :</span></div>
									<?php if(!empty($driveUserProfile[0]['vehicle_regi_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['vehicle_regi_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['vehicle_regi_doc']; ?>" download="vehicle_regi_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Vehicle registration document :</span></div>
									<?php if(!empty($driveUserProfile[0]['own_bus_vehicle_reg_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['own_bus_vehicle_reg_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['own_bus_vehicle_reg_doc']; ?>" download="own_bus_vehicle_reg_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Insurance policy document :</span></div>
									<?php if(!empty($driveUserProfile[0]['own_bus_insurance_pol_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['own_bus_insurance_pol_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['own_bus_insurance_pol_doc']; ?>" download="own_bus_insurance_pol_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Inspection document :</span></div>
									<?php if(!empty($driveUserProfile[0]['own_bus_inspection_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['own_bus_inspection_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['own_bus_inspection_doc']; ?>" download="own_bus_inspection_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url();?>assets/img/per_doc.png"><span>Tax ID or EIN number document :</span></div>
									<?php if(!empty($driveUserProfile[0]['own_bus_taxid_einno_doc'])) { ?>
									<div class="bas_rt"><span></span>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['own_bus_taxid_einno_doc']; ?>" target="_blank" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
										<a href="uploads/Driver/doc/<?php echo $driveUserProfile[0]['own_bus_taxid_einno_doc']; ?>" download="own_bus_taxid_einno_doc" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
									<?php } else echo "N/A";  ?>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="info_head">My Services :</div>
							<div class="basic_box">
								<ul class="list-inline services-list">
									<!-- <li class="list-item taxi">
										<div class="serviceimg">
											<img src="assets/img/taxi.png" height="18">
										</div>
										<div class="servicename">
											type_title
										</div>										
									</li> -->
									<li class="list-item">
										<div class="serviceimg">
											<img src="<?php echo base_url();?>assets/img/airport-shuffle.png" height="18">
										</div>
										<div class="servicename">
											<?php echo (!empty($driveUserProfile[0]['type_title']))?$driveUserProfile[0]['type_title']: 'N/A'; ?>
										</div>										
									</li>									
								</ul>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("user/footer"); ?>
