<?php $this->load->view("user/header"); ?>
<?php $this->load->view("driverUser/driverHeader"); ?>

<div class="myprof_wrapp">
	<div class="probgimg" style="background-image:url('http://exceptionaire.co/Apptech_New/assets/img/profile_bg.png');"></div>
	<div class="mypro_info_wrapp">
		<div class="container">
			<div class="row">
				<div class="tp_pro_sect">
					<div class="pro_img_box" style="background-image:url('http://exceptionaire.co/Apptech_New/uploads/Driver/thumb/92024243529886012017-11-26-11:53:06pm.jpg');">
					</div>
					<div class="pro_img_name">
						<div class="pro_name_box">
							<div class="pro_nme">Alex Vazquez</div>
							<div class="edit_prof_btn"><a href="edit-profile.html">Edit Profile</a></div>
						</div>
					</div>
				</div>
				<div class="basic_info_wrapp driveruserinfo">
					<div class="row">
						<div class="col-md-6">
							<div class="info_head">Basic Information :</div>
							<div class="basic_box">
								<div class="basic_inner">
									<div class="bas_lt"><img src="http://exceptionaire.co/Apptech_New/assets/img/date_icon.png"><span>Date of Birth:</span></div>
									<div class="bas_rt">2005-01-04</div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="http://exceptionaire.co/Apptech_New/assets/img/mob_no.png"><span>Mobile Number:</span></div>
									<div class="bas_rt">123456131346</div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="http://exceptionaire.co/Apptech_New/assets/img/email_add.png"><span>Email Address:</span></div>
									<div class="bas_rt">kiran@exceptionaire.co</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="info_head">Address :</div>
							<div class="basic_box add_box">
								<div class="basic_inner">
									<div class="bas_lt"><img src="http://exceptionaire.co/Apptech_New/assets/img/coun.png"><span>Country:</span></div>
									<div class="bas_rt">India</div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="http://exceptionaire.co/Apptech_New/assets/img/state.png"><span>State:</span></div>
									<div class="bas_rt">Goa</div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="http://exceptionaire.co/Apptech_New/assets/img/state.png"><span>City:</span></div>
									<div class="bas_rt">Bambolim</div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="http://exceptionaire.co/Apptech_New/assets/img/state.png"><span>Street:</span></div>
									<div class="bas_rt">dfgdfgf</div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="http://exceptionaire.co/Apptech_New/assets/img/zip.png"><span>Zip Code:</span></div>
									<div class="bas_rt">123564</div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="http://exceptionaire.co/Apptech_New/assets/img/zip.png"><span>County:</span></div>
									<div class="bas_rt">12131</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 owninfo">
							<div class="info_head">Personal Information :</div>
							<div class="basic_box personal_box">
								<div class="basic_inner">
									<div class="bas_lt"><img src="assets/img/companyname.png"><span>Company Name :</span></div>
									<div class="bas_rt">Alta Bates Medical Group</div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="assets/img/companyname.png"><span>Company address :</span></div>
									<div class="bas_rt per_doc">514 S. Magnolia St. Orlando, FL 32806</div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="assets/img/vehicledoc.png"><span>Vehicle Documents :</span></div>
									<div class="bas_rt"><span>My-Details.doc</span>
									<a href="#" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
									<a href="#" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>
									</div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="http://exceptionaire.co/Apptech_New/assets/img/per_doc.png"><span>Personal Documents :</span></div>
									<div class="bas_rt"><span>My-Details.doc</span>
									<a href="#" class="view-down"><i class="fa fa-eye" aria-hidden="true"></i></a>
									<a href="#" class="view-down"><i class="fa fa-download" aria-hidden="true"></i></a>									
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="info_head">My Services :</div>
							<div class="basic_box">
								<ul class="list-inline services-list">
									<li class="list-item taxi">
										<div class="serviceimg">
											<img src="assets/img/taxi.png">
										</div>
										<div class="servicename">
											Taxi
										</div>										
									</li>
									<li class="list-item">
										<div class="serviceimg">
											<img src="assets/img/airport-shuffle.png">
										</div>
										<div class="servicename">
											Airport Shuffle
										</div>										
									</li>									
								</ul>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>

<?php $this->load->view("user/footer"); ?>