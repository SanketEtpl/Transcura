<?php $this->load->view("user/header"); ?>
<?php $this->load->view("driverUser/driverHeader"); ?>
<div class="all_comp_wrapp">
	<div class="container">
		<div class="row">
			
			<div class="comp_inner">
				<h3 class="heal_head">Change setting</h3>
					<!-- <div id="error_message"></div> -->
					
				
				<div class="comp_box">
					<form name="chantge_pwd_form" method="POST" action="<?php echo base_url();?>driver-setting" autocomplete="off">
						<?php 
						$success = $this->session->flashdata('success'); 
						$fail = $this->session->flashdata('fail');
						if($success){
		                    ?>
		                    <div class="alert alert-success">
		                      <?php echo $success; ?>
		                    </div>
		                    <?php
		                }

		                if($fail){
		                  	?>
		                  	<div class="alert alert-danger">
		                  		<?php echo $fail; ?>
		                  	</div>	
		                 <?php }
					?>
						<div class="row">
							<div class="col-md-6 comp_lab">
								<label>Change password :</label>
								<div class="comp_inpt">
									<input placeholder="Change password" name="change_pwd" maxlength="100" value="<?php echo set_value('change_pwd'); ?>" class="form-control password" type="password">
								</div>	
								<?php echo form_error('change_pwd', '<div class="error">', '</div>'); ?>								
							</div>
							<div class="col-md-6 comp_lab">
								<label>confirm password :</label>
								<div class="comp_inpt">
									<input placeholder="Confirm password" name="confirm_pwd" maxlength="100" value="<?php echo set_value('confirm_pwd'); ?>" class="form-control password" type="password">
								</div>	
								<?php echo form_error('confirm_pwd', '<div class="error">', '</div>'); ?>	
							</div>
						</div>	
						<div class="pay_btn_box">
						<!-- <button type="button" class="req_btn cancel_btn customer_complaints" id="customer_complaints_cancel" data-toggle="modal" data-target="#succpay_pop">Cancel</button> -->
						<!-- <input type="submit" class="req_btn comp_btn" id="btn_change_pwd" value="Submit"> -->	
						  <input type="submit" class="req_btn comp_btn" value="Submit">		
					</div>	
					</form>	
					
				</div>
			</div>
		</div>
	</div>
</div>
<!-- <div id="subcomp_pop" class="modal fade in" role="dialog">
  <div class="modal-dialog">    
    <div class="modal-content sma_pop">
      <div class="modal-header">
        <button type="button" class="close complaint_ok" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>       
      </div>
      <div class="modal-body">
		<div class="succ_icon"><img src="<?php echo base_url(); ?>assets/img/succpay_icon.png"></div>
			<h4 class="succ_tit">Successful !!!</h4>
			<h2 class="read_trip">We apologize for the inconvenience;</br>your complaint has been</br>successfully submitted</h2>	
		</div>
      <div class="modal-footer ok_btn">
			<button type="button" id="complaint_ok" class="btn btn-default cont_btn complaint_ok" data-dismiss="modal">OK</button>			
      </div>
    </div>
  </div>
</div> -->

<?php $this->load->view("user/footer"); ?>