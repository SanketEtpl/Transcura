<?php $this->load->view("user/header"); ?>
<?php
	$this->load->view("driverUser/driverHeader"); 
	$getdata = $this->uri->segment(2);	
	//print_r($scheduleRide);exit;
?>
<div class="my_rides_wrapp">
	<div class="container">
		<div class="row">
			<div class="all_rides_box">
				<div class="rides_tab_new">	
					<div id="ridetab">						
						<div class="rides_tab">
							<ul class="nav nav-tabs">
								  <li class="<?php if(empty($getdata) || $getdata == " " || $getdata == "1") echo "active"; else echo ""; ?>"><a data-toggle="tab" href="#assign_scheduel_trip">Scheduled TRip </a></li>
								  <li class="<?php if($getdata == "2") echo "active"; else echo ""; ?>"><a data-toggle="tab" href="#assignment_complete_id">Completed TRip </a></li>
								  <li class="<?php if($getdata == "3") echo "active"; else echo ""; ?>"><a data-toggle="tab" href="#cancel_ride">Cancelled TRip</a></li>
							</ul>
						</div>					
					</div>
					<div id="success_message" class="err_mes"></div>
					<div id="error_message" class="err_mes"></div>
					<div class="tab-content assign_all_content" id="assign_all_content">

					  <div id="assign_scheduel_trip" class="tab-pane fade in <?php if(empty($getdata) || $getdata == " " || $getdata == "1") echo "active"; else echo ""; ?>">
						<h3 class="rid_head">My Trip Assignments</h3>
						<div class="rides_table">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th>Name</th>
										<th>Trip ID</th>
										<th>Date / Time</th>
										<th>View Map</th>
										<th>View Details</th>
									  </tr>
									</thead>
									<tbody>
										<?php
										if(!empty($tripDetails)) {
											foreach ($tripDetails as $key => $value) {		
											$postId = $this->encrypt->encode($value["id"]);
											$tid = strtr($postId, '+/', '-_');
											$latlong = explode(',', $value['sr_lat_lng_source']);
										?>
										<tr>
											<td><span class="rid_nme"><?php echo $value['full_name']; ?></span></td>
											<td id="assign_schedule_trip_id" style="display:none"><?php echo $value['id']; ?></td>
											<td><?php echo $value['sr_trip_id']; ?></td>
											<td><?php echo $value['sr_date'].' '.$value['sr_pick_up_time']; ?></td>
											<td>
												<input type="hidden" name="lat" id="lat" value="<?php echo $latlong[0]; ?>">
												<input type="hidden" name="long" id="long" value="<?php echo $latlong[1];?>">
												<button type="button" class="btn" data-toggle="modal" id="btnMap" data-target="#map_show<?php echo $value['id']; ?>">Map</button>
												
												<div class="modal fade" id="map_show<?php echo $value['id']; ?>" role="dialog">
												    <div class="modal-dialog">
												<div class="modal-content">
												        <div class="modal-header">
												          <button type="button" class="close" data-dismiss="modal">&times;</button>
												          <h4 class="modal-title">Map</h4>
												        </div>
												        <div class="modal-body">
												        <iframe
															  width="550"
															  height="350"
															  frameborder="0" style="border:0"
															  src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyAO9q2RgUAWQAbrwW_Z716Py56Lh68s0ZM&origin=<?php echo $value['source_address'];?>&destination=<?php echo $value['destination_address'];?>">
															</iframe>       
												        </div>
												        <div class="modal-footer">
												          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												        </div>
												      </div>      
												    </div>
												  </div>
												<!-- <input id = "btnShow" type="button" value="Show Maps"/>
													<div id="dialog" style="display: none">
													<div id="dvMap" style="height: 380px; width: 580px;">
													</div>
													</div> -->
											</td>
											<!-- <td><a href="<?php //echo base_url()?>assign-ride-trip-map?trip_id=<?php echo $tid;?>"><button class="btn " id="<?php echo 'schedule_ride_track_id_'.$value['id']; ?>" name="assign_schedule_track_id" >Map</button></a></td> -->
											<td><button class="btn btn-view">View</button></td>
										</tr>
										<?php } } ?>
										<!--   <div id="map"></div> -->
									</tbody>
								  </table>
							</div>
						</div>
					  </div>
					  <div id="assignment_complete_id" class="tab-pane fade in <?php if($getdata == "2") echo "active"; else echo ""; ?>">
						<h3 class="rid_head">My Completed Trip</h3>
						<div class="rides_table">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th>Name</th>
										<th>Trip ID</th>
										<th>Date / Time</th>
										<th>Status</th>
										<th>View Details</th>
									  </tr>
									</thead>
									<tbody>
										<?php if(!empty($completedTripDetails)) { 
												foreach ($completedTripDetails as $key => $value) {													
											?>
										<tr>
											<td><span class="rid_nme"><?php echo $value['full_name']; ?></span></td>
											<td id="assignment_complete_view_id" style="display:none"><?php echo $value['id']; ?></td>
											<td><?php echo $value['sr_trip_id']; ?></td>
											<td><?php echo $value['sr_date'].' '.$value['sr_pick_up_time']; ?></td>
											<td><button class="btn status-done">Done</button></td>
											<td><button class="btn assign-com-btn-view" >View</button></td>
										</tr>
										<?php } } ?>																		
									</tbody>
								  </table>
							</div>
						</div>
					  </div>
					  <div id="cancel_ride" class="tab-pane fade in <?php if($getdata == "3") echo "active"; else echo ""; ?>">
						<h3 class="rid_head">Cancelled Ride List</h3>
						<div class="rides_table">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th>Name</th>
										<th>Trip ID</th>
										<th>Date / Time</th>
										<th>Status</th>
										<th>View Details</th>
									  </tr>
									</thead>
									<tbody>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>01-Aug-2017 2:00 PM</td>
											<td><button class="btn status-cancel">cancel</button></td>
											<td><button class="btn btn-view" >View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>01-Aug-2017 2:00 PM</td>
											<td><button class="btn status-cancel">cancel</button></td>
											<td><button class="btn btn-view" >View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>01-Aug-2017 2:00 PM</td>
											<td><button class="btn status-cancel">cancel</button></td>
											<td><button class="btn btn-view" >View</button></td>
										</tr>										
									</tbody>
								  </table>
							</div>
						</div>
					  </div>
					  <div id="custom" class="tab-pane fade">
						<h3 class="rid_head">Customer Care Information</h3>
						<p>Coming Soon</p>
					  </div>
					</div>	
					<!--schedule trip view-->
					<div id="assign_sched_view" style="display:none">
						<div class="ride_trip_deta">
							<h3 class="heal_head">
								My trip assignment view
								<div id="success_message"></div>
								<div id="error_message"></div>
								<span class="back_box"><button class="back_btn" id="assign_schedule_bk" name="assign_schedule_bk">Back</button></span>
							</h3>
							<div class="sched_deta_box">								
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip ID :</label>
											<div class="rid_info" id="ASV_trip_id"></div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Name :</label>
											<div class="rid_info" id="ASV_name"><span class="view_sec viewnew"><a href="#" class="com_btn" title="View"><i class="fa fa-eye"></i></a></span></div>
										</div>
										<!--<div class="col-md-7">
											<span class="view_sec"><a href="#" class="com_btn">View Details</a></span>
										</div>-->
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip Date :</label>
											<div class="rid_info" id="ASV_trip_date"></div>
										</div>
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Pick Up Point :</label>
											<div class="rid_info" id="trip_pick_up_point"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Drop Point Healthcare Providers Location :</label>
											<div class="rid_info" id="trip_drop_point"></div>
										</div>
									</div>
								</div>		
								<!-- <div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Patient's Phone Number :</label>
											<div class="rid_info">
												<label class="pick_lab_new">Name :</label>
												<div class="pick_info infofloat" id="driver_name"></div>
												<div class="view_pop viewnew"><button class="com_btn" id="ride_trip_details_view" name="ride_trip_details_view" title="View"><i class="fa fa-eye"></i></button></div>
											</div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Cost Estimate :</label>
											<div class="rid_info">--</div>
										</div>
									</div>
								</div>	 -->
							</div>	
							<!-- <div class="pay_btn_box">								
								<button type="button" class="req_btn cancel_btn" id="">Cancel Ride</button>
								<button type="button" class="req_btn comp_btn" id="customer_complaints">Submit a Complaint</button>					
								<button type="button" class="req_btn" id="" data-toggle="modal" data-target="#returnride_pop">Request a Return Ride</button>
							</div> -->	
						</div>
					</div>
					<!--complete trip view-->
					<div id="assign_complete_view">						
						<div class="ride_trip_deta">
							<h3 class="heal_head">
								Completed Ride Trip Details
								<span class="back_box"><button class="back_btn" id="assing_complete_bk" name="assing_complete_bk">Back</button></span>
							</h3>
							<div class="sched_deta_box">								
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip ID :</label>
											<div class="rid_info" id="assign_complete_trip_id"></div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Name :</label>
											<div class="rid_info" id="assign_complete_name"> <span class="view_sec viewnew"><a href="#" class="com_btn" title="View"><i class="fa fa-eye"></i></a></span></div>
										</div>
										<!--<div class="col-md-7">
											<span class="view_sec"><a href="#" class="com_btn">View Details</a></span>
										</div>-->
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Pick Up Point :</label>
											<div class="rid_info" id="assign_complete_pick_up_point"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Drop Point :</label>
											<div class="rid_info" id="assign_drop_point"></div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Ride Date :</label>
											<div class="rid_info" id="assign_complete_trip_date"></div>
										</div>
									</div>
								</div>									
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Total Distance :</label>
											<div class="rid_info" id="assign_total_distance">10KM</div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Total Cost :</label>
											<div class="rid_info" id="assign_total_cost">$230</div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Rate Trip :</label>
											<div class="rid_info"> Star rating Coming Soon</div>
										</div>
									</div>
								</div>	
							</div>	
						</div>
					</div>
					<!--cancel trip view-->
					<div id="cancel_ride_view">						
						<div class="ride_trip_deta">
							<h3 class="heal_head">
								Cancelled Ride Trip Details
								<span class="back_box"><button class="back_btn" id="cancel_bk" name="cancel_bk" onclick="showStuff1();">Back</button></span>
							</h3>
							<div class="sched_deta_box">								
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip ID :</label>
											<div class="rid_info" id="cancelled_trip_id"></div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Name :</label>
											<div class="rid_info" id="cancel_name"></div>
										</div>
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Pick Up Point :</label>
											<div class="rid_info" id="cancelled_pick_up_point"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Drop Point :</label>
											<div class="rid_info">2780 Little Acres Lane Jacksonville, IL 62650</div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Ride Date and Time :</label>
											<div class="rid_info" id="cancelled_trip_date"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Ride Cancelled Date and Time :</label>
											<div class="rid_info">28-August-2017 Time: 05:50 PM</div>
										</div>
									</div>
								</div>									
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Type of Cancellation Marked by Driver : :</label>
											<div class="rid_info">---</div>
										</div>
									</div>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="schedule_ride_cancelled_value" value="">
<!--cancel ride popup-->
<div id="canride_pop" class="modal fade in" role="dialog">
  <div class="modal-dialog">    
    <div class="modal-content sma_pop">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>       
      </div>
      <div class="modal-body">
		<div class="succ_icon"><img src="http://192.168.100.12/AppTech/assets/img/info_icon.png"></div>
		<h2 class="read_trip">Are you sure want to</br>cancel this Ride?</h2>	
		</div>
      <div class="modal-footer ok_btn">
      	
		<button type="button" id="schedule_ride_cancelled_yes" class="btn btn-default cont_btn" data-dismiss="modal">Yes</button>
		<button type="button" id="schedule_ride_cancelled_no" class="btn btn-default cont_btn" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<!--return ride popup-->
<div id="returnride_pop" class="modal fade in" role="dialog">
  <div class="modal-dialog">    
    <div class="modal-content sma_pop">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>       
      </div>
      <div class="modal-body">
		<div class="succ_icon"><img src="http://192.168.100.12/AppTech/assets/img/info_icon.png"></div>
			<h2 class="read_trip">Are you Ready</br>for a Return Ride</h2>	
		</div>
      <div class="modal-footer ok_btn">
			<button type="button" id="" class="btn btn-default cont_btn" data-dismiss="modal">Yes</button>
			<button type="button" id="" class="btn btn-default cont_btn" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
 
<?php $this->load->view("user/footer"); ?>