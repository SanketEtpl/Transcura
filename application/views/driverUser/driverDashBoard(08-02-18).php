<?php $this->load->view("user/header"); ?>
<?php $this->load->view("driverUser/driverHeader"); ?>

<div class="inner_body_wrapp">
	<div class="container">
		<div class="row">
			<div class="ts_dash_wrap">
				<div class="top_dash_sec">
					<div class="col-md-6 lt_prof">
						<div class="pro_wrap">
							<div class="prof_cir"><img src="upload/driver/thumb/92024243529886012017-11-26-11_53_06pm.jpg"></div>
							<div class="prof_info">
								<div class="mypro_head">My Profile</div>
								<div class="mypro_name">
									David J. Barb<br>
									<span>davidbarb@xyz.com</span><br>
									<span>Driver User</span>
								</div>
								<div class="view_pro_btn"><a href="http://exceptionaire.co/Apptech_New/my-profile">View</a></div>
							</div>
						</div>
					</div>
				
					<div class="col-md-6 rt_dri">
						<div class="pro_wrap driv_map my_billing_history pull-left">
							<div class="prof_cir text-center">58$<br><small>Last bills</small></div>
							<div class="prof_info">
								<div class="mypro_head">My Billing History</div>
								<div class="mypro_name">
									Total Bills
									<div class="block">508$</div>
								</div>
								<div class="view_pro_btn"><a href="http://exceptionaire.co/Apptech_New/my-profile">View</a></div>
							</div>
						</div>		
					</div>
				</div>
			</div>
			<div class="my_rides_sec">
				<div class="inn_tit_wrap">
					<h1 class="inn_head">My Trip Assignments <span>My Trip Assignments</span></h1>
				</div>
				<div class="rides_box">
					<div class="col-md-4">
						<div class="inn_rides">
							<div class="ltrid_img">
								<img src="assets/img/rides_box.png">
								<span class="rid_cnt">135</span>
							</div>
							<div class="tot_wrapp">
								<div class="tot_name">Total<br>Scheduled Trips</div>
								<div class="view_rides_btn"><a href="http://exceptionaire.co/Apptech_New/my-rides/schedule-ride">View</a></div>	
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="inn_rides">
							<div class="ltrid_img">
								<img src="assets/img/rides_box.png">
								<span class="rid_cnt">120</span>
							</div>
							<div class="tot_wrapp">
								<div class="tot_name">Total<br>Completed Trips</div>
								<div class="view_rides_btn"><a href="http://exceptionaire.co/Apptech_New/my-rides/schedule-completed">View</a></div>	
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="inn_rides">
							<div class="ltrid_img">
								<img src="assets/img/rides_box.png">
								<span class="rid_cnt">15</span>
							</div>
							<div class="tot_wrapp">
								<div class="tot_name">Total<br>Cancelled Trips</div>
								<div class="view_rides_btn"><a href="http://exceptionaire.co/Apptech_New/my-rides/schedule-cancelled">View</a></div>	
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="my_rides_sec mt0imp mb40">
				<div class="inn_tit_wrap">
					<h1 class="inn_head">My Trip Requests <span>My Trip Requests</span></h1>
				</div>
				<div class="rides_box">
					<div class="col-md-4 col-md-offset-2">
						<div class="inn_rides">
							<div class="ltrid_img">
								<img src="assets/img/rides_box.png">
								<span class="rid_cnt">135</span>
							</div>
							<div class="tot_wrapp">
								<div class="tot_name">My Accepted<br>Trips</div>
								<div class="view_rides_btn"><a href="http://exceptionaire.co/Apptech_New/my-rides/schedule-ride">View</a></div>	
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="inn_rides">
							<div class="ltrid_img">
								<img src="assets/img/rides_box.png">
								<span class="rid_cnt">120</span>
							</div>
							<div class="tot_wrapp">
								<div class="tot_name">My Rejected<br>Trips</div>
								<div class="view_rides_btn"><a href="http://exceptionaire.co/Apptech_New/my-rides/schedule-completed">View</a></div>	
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>

<?php $this->load->view("user/footer"); ?>