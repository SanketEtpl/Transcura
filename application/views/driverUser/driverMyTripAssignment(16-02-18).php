<?php $this->load->view("user/header"); ?>
<?php $this->load->view("driverUser/driverHeader"); ?>
<div class="my_rides_wrapp">
	<div class="container">
		<div class="row">
			<div class="all_rides_box">
				<div class="rides_tab_new">	
					<div id="ridetab">						
						<div class="rides_tab">
							<ul class="nav nav-tabs">
								  <li class="active"><a data-toggle="tab" href="#sched_trip">Scheduled TRip </a></li>
								  <li ><a data-toggle="tab" href="#comp_ride">Completed TRip </a></li>
								  <li ><a data-toggle="tab" href="#cancel_ride">Cancelled TRip</a></li>
							</ul>
						</div>					
					</div>
					<div id="success_message" class="err_mes"></div>
					<div id="error_message" class="err_mes"></div>
					<div class="tab-content all_content" id="all_content">

					  <div id="sched_trip" class="tab-pane fade in active">
						<h3 class="rid_head">My Trip Assignments</h3>
						<div class="rides_table">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th>Name</th>
										<th>Trip ID</th>
										<th>Date / Time</th>
										<th>View Map</th>
										<th>View Details</th>
									  </tr>
									</thead>
									<tbody>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>29-Jan-2018 2:00 PM</td>
											<td><button class="btn btn-map">map</button></td>
											<td><button class="btn btn-view" >View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>29-Jan-2018 2:00 PM</td>
											<td><button class="btn btn-map">map</button></td>
											<td><button class="btn btn-view">View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>29-Jan-2018 2:00 PM</td>
											<td><button class="btn btn-map">map</button></td>
											<td><button class="btn btn-view">View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>29-Jan-2018 2:00 PM</td>
											<td><button class="btn btn-map">map</button></td>
											<td><button class="btn btn-view">View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>29-Jan-2018 2:00 PM</td>
											<td><button class="btn btn-map">map</button></td>
											<td><button class="btn btn-view">View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>29-Jan-2018 2:00 PM</td>
											<td><button class="btn btn-map">map</button></td>
											<td><button class="btn btn-view">View</button></td>
										</tr>										
									</tbody>
								  </table>
							</div>
						</div>
					  </div>
					  <div id="comp_ride" class="tab-pane fade">
						<h3 class="rid_head">My Completed Trip</h3>
						<div class="rides_table">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th>Name</th>
										<th>Trip ID</th>
										<th>Date / Time</th>
										<th>Status</th>
										<th>View Details</th>
									  </tr>
									</thead>
									<tbody>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>01-Aug-2017 2:00 PM</td>
											<td><button class="btn status-done">Done</button></td>
											<td><button class="btn btn-view" >View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>01-Aug-2017 2:00 PM</td>
											<td><button class="btn status-done">Done</button></td>
											<td><button class="btn btn-view" >View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>01-Aug-2017 2:00 PM</td>
											<td><button class="btn status-done">Done</button></td>
											<td><button class="btn btn-view" >View</button></td>
										</tr>										
									</tbody>
								  </table>
							</div>
						</div>
					  </div>
					  <div id="cancel_ride" class="tab-pane fade">
						<h3 class="rid_head">Cancelled Ride List</h3>
						<div class="rides_table">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th>Name</th>
										<th>Trip ID</th>
										<th>Date / Time</th>
										<th>Status</th>
										<th>View Details</th>
									  </tr>
									</thead>
									<tbody>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>01-Aug-2017 2:00 PM</td>
											<td><button class="btn status-cancel">cancel</button></td>
											<td><button class="btn btn-view" >View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>01-Aug-2017 2:00 PM</td>
											<td><button class="btn status-cancel">cancel</button></td>
											<td><button class="btn btn-view" >View</button></td>
										</tr>
										<tr>
											<td><span class="rid_nme">Alex Vazquez</span></td>
											<td id="completed_ride_view_id" style="display:none">18</td>
											<td>TRIP000000012</td>
											<td>01-Aug-2017 2:00 PM</td>
											<td><button class="btn status-cancel">cancel</button></td>
											<td><button class="btn btn-view" >View</button></td>
										</tr>										
									</tbody>
								  </table>
							</div>
						</div>
					  </div>
					  <div id="custom" class="tab-pane fade">
						<h3 class="rid_head">Customer Care Information</h3>
						<p>Coming Soon</p>
					  </div>
					</div>	
					<!--schedule trip view-->
					<div id="sched_view">
						<div class="ride_trip_deta">
							<h3 class="heal_head">
								scheduled Trip Details
								<div id="success_message"></div>
								<div id="error_message"></div>
								<span class="back_box"><button class="back_btn" id="schedule_bk" name="schedule_bk"  onclick="showStuff1();">Back</button></span>
							</h3>
							<div class="sched_deta_box">								
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip ID :</label>
											<div class="rid_info" id="trip_id">TRIP123456</div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Name :</label>
											<div class="rid_info" id="name">Mario Speedwagon <span class="view_sec viewnew"><a href="#" class="com_btn" title="View"><i class="fa fa-eye"></i></a></span></div>
										</div>
										<!--<div class="col-md-7">
											<span class="view_sec"><a href="#" class="com_btn">View Details</a></span>
										</div>-->
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip Date :</label>
											<div class="rid_info" id="trip_date">29-Aug-2017</div>
										</div>
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Pick Up Point :</label>
											<div class="rid_info" id="trip_pick_up_point">1510 Cook Hill Road Meriden, CT 06450</div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Drop Point Healthcare Providers Location :</label>
											<div class="rid_info">Flores 1516 Holt Street West Palm Beach, FL 33401</div>
										</div>
									</div>
								</div>		
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Patient's Phone Number :</label>
											<div class="rid_info">
												<label class="pick_lab_new">Name :</label>
												<div class="pick_info infofloat" id="driver_name"></div>
												<div class="view_pop viewnew"><button class="com_btn" id="ride_trip_details_view" name="ride_trip_details_view" title="View"><i class="fa fa-eye"></i></button></div>
											</div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Cost Estimate :</label>
											<div class="rid_info">--</div>
										</div>
									</div>
								</div>	
							</div>	
							<div class="pay_btn_box">								
								<button type="button" class="req_btn cancel_btn" id="">Cancel Ride</button>
								<button type="button" class="req_btn comp_btn" id="customer_complaints">Submit a Complaint</button>					
								<button type="button" class="req_btn" id="" data-toggle="modal" data-target="#returnride_pop">Request a Return Ride</button>
							</div>	
						</div>
					</div>
					<!--complete trip view-->
					<div id="complete_view">						
						<div class="ride_trip_deta">
							<h3 class="heal_head">
								Completed Ride Trip Details
								<span class="back_box"><button class="back_btn" id="complete_bk" name="complete_bk" onclick="showStuff1();">Back</button></span>
							</h3>
							<div class="sched_deta_box">								
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip ID :</label>
											<div class="rid_info" id="complete_trip_id"></div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Name :</label>
											<div class="rid_info" id="complete_name"> <span class="view_sec viewnew"><a href="#" class="com_btn" title="View"><i class="fa fa-eye"></i></a></span></div>
										</div>
										<!--<div class="col-md-7">
											<span class="view_sec"><a href="#" class="com_btn">View Details</a></span>
										</div>-->
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Pick Up Point :</label>
											<div class="rid_info" id="complete_pick_up_point"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Drop Point :</label>
											<div class="rid_info">2780 Little Acres Lane Jacksonville, IL 62650</div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Ride Date :</label>
											<div class="rid_info" id="complete_trip_date"></div>
										</div>
									</div>
								</div>									
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Total Distance :</label>
											<div class="rid_info">10KM</div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Total Cost :</label>
											<div class="rid_info">$230</div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Rate Trip :</label>
											<div class="rid_info"> Star rating Coming Soon</div>
										</div>
									</div>
								</div>	
							</div>	
						</div>
					</div>
					<!--cancel trip view-->
					<div id="cancel_ride_view">						
						<div class="ride_trip_deta">
							<h3 class="heal_head">
								Cancelled Ride Trip Details
								<span class="back_box"><button class="back_btn" id="cancel_bk" name="cancel_bk" onclick="showStuff1();">Back</button></span>
							</h3>
							<div class="sched_deta_box">								
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip ID :</label>
											<div class="rid_info" id="cancelled_trip_id"></div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Name :</label>
											<div class="rid_info" id="cancel_name"></div>
										</div>
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Pick Up Point :</label>
											<div class="rid_info" id="cancelled_pick_up_point"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Drop Point :</label>
											<div class="rid_info">2780 Little Acres Lane Jacksonville, IL 62650</div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Ride Date and Time :</label>
											<div class="rid_info" id="cancelled_trip_date"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Ride Cancelled Date and Time :</label>
											<div class="rid_info">28-August-2017 Time: 05:50 PM</div>
										</div>
									</div>
								</div>									
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Type of Cancellation Marked by Driver : :</label>
											<div class="rid_info">---</div>
										</div>
									</div>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" id="schedule_ride_cancelled_value" value="">
<!--cancel ride popup-->
<div id="canride_pop" class="modal fade in" role="dialog">
  <div class="modal-dialog">    
    <div class="modal-content sma_pop">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>       
      </div>
      <div class="modal-body">
		<div class="succ_icon"><img src="http://192.168.100.12/AppTech/assets/img/info_icon.png"></div>
		<h2 class="read_trip">Are you sure want to</br>cancel this Ride?</h2>	
		</div>
      <div class="modal-footer ok_btn">
      	
		<button type="button" id="schedule_ride_cancelled_yes" class="btn btn-default cont_btn" data-dismiss="modal">Yes</button>
		<button type="button" id="schedule_ride_cancelled_no" class="btn btn-default cont_btn" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<!--return ride popup-->
<div id="returnride_pop" class="modal fade in" role="dialog">
  <div class="modal-dialog">    
    <div class="modal-content sma_pop">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>       
      </div>
      <div class="modal-body">
		<div class="succ_icon"><img src="http://192.168.100.12/AppTech/assets/img/info_icon.png"></div>
			<h2 class="read_trip">Are you Ready</br>for a Return Ride</h2>	
		</div>
      <div class="modal-footer ok_btn">
			<button type="button" id="" class="btn btn-default cont_btn" data-dismiss="modal">Yes</button>
			<button type="button" id="" class="btn btn-default cont_btn" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view("user/footer"); ?>