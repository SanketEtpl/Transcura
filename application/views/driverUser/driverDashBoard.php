<?php $this->load->view("user/header"); ?>
<?php $this->load->view("driverUser/driverHeader"); ?>
<style type="text/css">
	.btn-danger[disabled]
	{
		background-color: #d9534f !important;	
	}
	.btn-success[disabled]
	{
		background-color: #5cb85c !important;	
	}
</style>
<div class="inner_body_wrapp">
	<div class="container">
		<div class="row">
			<div class="ts_dash_wrap">
				<div class="top_dash_sec">
					<div class="col-md-6 lt_prof">
						<div class="pro_wrap">
							<div class="prof_cir">								
								<img src="<?php echo base_url() ?>uploads/Driver/doc/<?php if(!empty($userDetails[0]['picture'])){ echo $userDetails[0]['picture']; } else { echo "default.png"; } ?>">
							</div>
							<div class="prof_info">
								<div class="mypro_head">My Profile</div>
								<div class="mypro_name">
									<?php if(!empty($userDetails[0]['full_name'])){ echo $userDetails[0]['full_name']; } else { echo ""; }  ?>
									<br>
									<span><?php if(!empty($userDetails[0]['email'])){ echo $userDetails[0]['email']; } else { echo ""; }  ?></span><br>
									<span><?php if(!empty($userDetails[0]['user_type'] == "5")){ echo "Driver user"; } else { echo ""; }  ?></span>
								</div>
								<div class="view_pro_btn"><a href="<?php echo base_url();?>driver-my-profile">View</a></div>
							</div>
						</div>
					</div>

					<div class="col-md-6 rt_dri">
						<div class="pro_wrap driv_map my_billing_history pull-left">
							<div class="prof_cir text-center">58$<br><small>Last bills</small></div>
							<div class="prof_info">
								<div class="mypro_head">My Billing History</div>
								<div class="mypro_name">
									Total Bills
									<div class="block">508$</div>
								</div>
								<div class="view_pro_btn"><a href="http://exceptionaire.co/Apptech_New/my-profile">View</a></div>
							</div>
						</div>		
					</div>
				</div>
			</div>

			<div class="clearfix"></div>
			<br /><br />
			<div class="ts_dash_wrap mainavail">
				<div class="top_dash_sec subavail">
					<div class="col-md-6 lt_prof">
						<div class="pro_wrap">
							<div class="prof_cir">								
								<img src="<?php echo base_url() ?>uploads/Driver/doc/<?php if(!empty($userDetails[0]['picture'])){ echo $userDetails[0]['picture']; } else { echo "default.png"; } ?>">
							</div>
							<div class="prof_info">
								<div class="mypro_head">Availability</div>
								<div class="mypro_name">
									<?php
									$check_available = getDriverAvailability($this->session->userdata('user_id'));
									if($check_available == "availabale")
									{
										$status_active = "btn btn-md btn-success disabled";
										$status_inactive = "btn btn-md  active btn-default";
									}else if($check_available == "unavailabale")
									{
										$status_active = "btn btn-md  active btn-default";
										$status_inactive = "btn btn-md btn-danger disabled";
									}

									echo "Status : ".$check_available;
									?>
								</div>
								<div class="view_pro_btn">
									<div class="btn-group btn-toggle">
										<button onclick="ChangeAvailability('1','<?php echo $this->encrypt->encode($this->session->userdata('user_id')); ?>')" class="<?php echo $status_active; ?>" title="Make driver availabale">On</button>
										<button onclick="ChangeAvailability('0','<?php echo $this->encrypt->encode($this->session->userdata('user_id')); ?>')" class="<?php echo $status_inactive; ?>" title="Make driver unavailabale">Off</button>
									</div>
									<!-- <a href="<?php echo base_url();?>driver-my-profile">View</a> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
			<br />
			<div class="avail_status"></div>
			<div class="my_rides_sec">
				<div class="inn_tit_wrap">
					<h1 class="inn_head">My Trip Assignments <span>My Trip Assignments</span></h1>
				</div>
				<div class="rides_box">
					<div class="col-md-4">
						<div class="inn_rides">
							<div class="ltrid_img">
								<img src="assets/img/rides_box.png">
								<span class="rid_cnt">
									<?php
									if(!empty($myCancelledRideCount[0]) || !empty($myCompleteRideCount[0]) || !empty($myScheduleRideCount[0] )) {
										if($myCancelledRideCount[0]['sr_status'] == "1" || $myCompleteRideCount[0]['sr_status'] == "2" || $myScheduleRideCount[0]['sr_status'] == "3")
											$total1 = $total2 =$total3 ="";
										if(!empty($myCancelledRideCount[0]['total']))
											$total1 = $myCancelledRideCount[0]['total'];
										else
											$total1 = 0;
										if(!empty($myCompleteRideCount[0]['total']))
											$total2 = $myCompleteRideCount[0]['total'];
										else
											$total2 = 0;
										if(!empty($myScheduleRideCount[0]['total']))
											$total3 = $myScheduleRideCount[0]['total'];
										else
											$total3 = 0;	
										echo $total1 + $total2 + $total3;												
									} else {
										echo "0";
									}
									?>
								</span>
							</div>
							<div class="tot_wrapp">
								<div class="tot_name">Total<br>Scheduled Trips</div>
								<div class="view_rides_btn"><a href="<?php echo base_url();?>driver-my-trip-assignments/1">View</a></div>	
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="inn_rides">
							<div class="ltrid_img">
								<img src="assets/img/rides_box.png">
								<span class="rid_cnt">
									<?php
									if(!empty($myCompleteRideCount[0])) {
										if($myCompleteRideCount[0]['sr_status'] == "2" && $myCompleteRideCount[0]['total'] !=0)
											echo $myCompleteRideCount[0]['total'];	
										else
											echo "0";
									} else {
										echo "0";
									}
									?>
								</span>
							</div>
							<div class="tot_wrapp">
								<div class="tot_name">Total<br>Completed Trips</div>
								<div class="view_rides_btn"><a href="<?php echo base_url();?>driver-my-trip-assignments/2">View</a></div>	
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="inn_rides">
							<div class="ltrid_img">
								<img src="assets/img/rides_box.png">
								<span class="rid_cnt">
									<?php
									if(!empty($myCancelledRideCount[0])) {
										if($myCancelledRideCount[0]['sr_status'] == "3" && $myCancelledRideCount[0]['total'] != 0)
											echo $myCancelledRideCount[0]['total'];												
										else
											echo "0";
									} else {
										echo "0";
									}
									?>
								</span>
							</div>
							<div class="tot_wrapp">
								<div class="tot_name">Total<br>Cancelled Trips</div>
								<div class="view_rides_btn"><a href="<?php echo base_url();?>driver-my-trip-assignments/3">View</a></div>	
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="my_rides_sec mt0imp mb40">
				<div class="inn_tit_wrap">
					<h1 class="inn_head">My Trip Requests <span>My Trip Requests</span></h1>
				</div>
				<div class="rides_box">
					<div class="col-md-4 col-md-offset-2">
						<div class="inn_rides">
							<div class="ltrid_img">
								<img src="assets/img/rides_box.png">
								<span class="rid_cnt">
									<?php
									if(!empty($myScheduleRideCount[0])) {
										if($myScheduleRideCount[0]['sr_status'] == "1" && $myScheduleRideCount[0]['total'] != 0)
											echo $myScheduleRideCount[0]['total'];	
										else
											echo "0";											
									} else {
										echo "0";
									}
									?></span>
								</div>
								<div class="tot_wrapp">
									<div class="tot_name">My Accepted<br>Trips</div>
									<div class="view_rides_btn"><a href="<?php echo base_url();?>driver-my-trip-requests/1">View</a></div>	
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="inn_rides">
								<div class="ltrid_img">
									<img src="assets/img/rides_box.png">
									<span class="rid_cnt">120</span>
								</div>
								<div class="tot_wrapp">
									<div class="tot_name">My Rejected<br>Trips</div>
									<div class="view_rides_btn"><a href="<?php echo base_url();?>driver-my-trip-requests/2">View</a></div>	
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
<script type="text/javascript">
			$(document).ready(function()
			{
			$('.disabled').prop('disabled', true); //TO DISABLED 
			});			

			//--------- Change driver availabality -----------
			function ChangeAvailability(status,drid)
			{
				if(confirm('Are you sure you want to change availabality ?'))
				{
					$.ajax({
						type:"POST",
						url:baseURL+"change-driver-avail",
						data:{'status':status,'drid':drid,'user_type':'5'},
						dataType:"json",
						async:false,					
						success:function(response){
							if(response.status == "success")
							{
								$(".mainavail").load(window.location.href + " .subavail");
								$(".avail_status").html("<div class='alert alert-success'><strong>Success! </strong>"+response.message+"</div>");

							}
							else if(response.status == "warning")
							{
								$(".avail_status").html("<div class='alert alert-warning'><strong>Warning! </strong>"+response.message+"</div>");				
							}			
						}
					});
				}	
			}
		</script>
	<?php $this->load->view("user/footer"); ?>