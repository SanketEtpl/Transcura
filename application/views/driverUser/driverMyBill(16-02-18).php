<?php $this->load->view("user/header"); ?>
<?php $this->load->view("driverUser/driverHeader"); ?>
<div class="my_rides_wrapp">
	<div class="container">
		<div class="row">
			<div class="all_rides_box">
				<div class="rides_tab_new">	
					<div id="ridetab">						
						<div class="rides_tab">
							<ul class="nav nav-tabs">
								  <li class="active"><a data-toggle="tab" href="#sched_trip">BILLING DATE</a></li>
								  <li ><a data-toggle="tab" href="#comp_ride">PAYMENT STATEMENTS</a></li>
								  <li ><a data-toggle="tab" href="#cancel_ride">TRIP DISCREPANCY</a></li>
							</ul>
						</div>					
					</div>
					<div id="success_message" class="err_mes"></div>	
					<div id="error_message" class="err_mes"></div>
					<div class="tab-content all_content" id="all_content">

					<div id="sched_trip" class="tab-pane fade in active">
						<div class="form_tsb_date">
							<form class="form-inline form_biilling_date">
								  <div class="form-group col-md-3 padd-left0">
										<label class="form_label">START DATE</label>
										    <input id="date_of_birth" type="text" class="form-control date-picker hasDatepicker" name="date_of_birth" autocomplete="off" readonly="" value=""/>
								  </div>
								  <div class="form-group col-md-3">
										<label class="form_label">END DATE</label>
										<input id="date_of_birth" type="text" class="form-control date-picker hasDatepicker" name="date_of_birth" autocomplete="off" readonly="" value=""/>	
								  </div>
								  <button type="button" class="btn btn_searchdatehere">SEARCH</button>
							</form>
						</div>
						<div class="table_dat_time_board">
							<div class="serch_re_btn">
								<h3 class="rid_head">SEARCH RESULT</h3>
								<button type="btn" class="btn BTN_download">DOWNLOAD PDF</button>
							</div>
							<div class="rides_table">
								<div class="table-responsive">
									<table class="table table-striped">
										<thead>
										  <tr>
											<th>DATE/TIME</th>
											<th>Trip ID</th>
											<th>BILLING AMOUNT</th>
											<th>DISTANCE</th>
											<th>VIEW DETAILS</th>
										  </tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<span class="rid_nme">
														<div class="cheak_box_here">
															<div class="checkbox">
																<input type="checkbox" id="checkbox1">
																<label for="checkbox1"></label>
															</div>
														</div>
														<span class="text_chek">21-july-2017 10:25 AM</span>
													</span>
												</td>
												<td id="completed_ride_view_id" style="display:none">18</td>
												<td>TRIP000000012</td>
												<td>$ 232</td>
												<td>15 km</td>
												<td><button class="btn btn-view" >View</button></td>
											</tr>
											<tr>
												<td>
													<span class="rid_nme">
														<div class="cheak_box_here">
															<div class="checkbox">
																<input type="checkbox" id="checkbox2">
																<label for="checkbox2"></label>
															</div>
														</div>
														<span class="text_chek">21-july-2017 2:20 AM</span>
													</span>
												</td>
												<td id="completed_ride_view_id" style="display:none">18</td>
												<td>TRIP000000012</td>
												<td>$ 150</td>
												<td>10 km</td>
												<td><button class="btn btn-view">View</button></td>
											</tr>
											<tr>
												<td>
													<span class="rid_nme">
														<div class="cheak_box_here">
															<div class="checkbox">
																<input type="checkbox" id="checkbox3">
																<label for="checkbox3"></label>
															</div>
														</div>
														<span class="text_chek">21-july-2017 3:45 AM</span>
													</span>
												</td>
												<td id="completed_ride_view_id" style="display:none">18</td>
												<td>TRIP000000012</td>
												<td>$ 56</td>
												<td>5 km</td>
												<td><button class="btn btn-view">View</button></td>
											</tr>
											<tr>
												<td>
													<span class="rid_nme">
														<div class="cheak_box_here">
															<div class="checkbox">
																<input type="checkbox" id="checkbox4">
																<label for="checkbox4"></label>
															</div>
														</div>
														<span class="text_chek">21-july-2017 6:28 AM</span>														
													</span>
												</td>
												<td id="completed_ride_view_id" style="display:none">18</td>
												<td>TRIP000000012</td>
												<td>$ 632</td>
												<td>16 km</td>
												<td><button class="btn btn-view">View</button></td>
											</tr>
											<tr>
												<td>
													<span class="rid_nme">
														<div class="cheak_box_here">
															<div class="checkbox">
																<input type="checkbox" id="checkbox5">
																<label for="checkbox5"></label>
															</div>
														</div>
														<span class="text_chek">21-july-2017 9:40 AM</span>	

													</span>
												</td>
												<td id="completed_ride_view_id" style="display:none">18</td>
												<td>TRIP000000012</td>
												<td>$ 84</td>
												<td>6 km</td>
												<td><button class="btn btn-view">View</button></td>
											</tr>
										</tbody>
									  </table>
								</div>
								<div class="pagination_tab">
									<ul class="pagination pagination_link">
									  <li><button type="button" class="btn first_page_btn">PREVIOUS</button></li>
									  <li><a href="#." class="active_pagination">1</a></li>
									  <li><a href="#.">2</a></li>
									  <li><a href="#.">3</a></li>
									  <li><a href="#.">4</a></li>
									  <li><a href="#.">5</a></li>
									  <li><button type="button" class="btn last_page_btn">NEXT</button></li>
									</ul> 
								</div>
							</div>
						</div>
					</div>
					  <div id="comp_ride" class="tab-pane fade">
						<div class="form_tsb_date">
							<form class="form-inline form_biilling_date">
								  <div class="form-group col-md-3 padd-left0">
										<label class="form_label">PLEASE SELECT THE DATE</label>
										    <input id="date_of_birth" type="text" class="form-control date-picker hasDatepicker" name="date_of_birth" autocomplete="off" readonly="" value=""/>
											
								  </div>
								  <button type="button" class="btn btn_searchdatehere">SEARCH</button>
							</form>
						</div>
						<div class="table_dat_time_board">
							<div class="serch_re_btn">
								<h3 class="rid_head">SEARCH RESULT</h3>
								<button type="btn" class="btn BTN_download">DOWNLOAD PDF</button>
							</div>
							<div class="rides_table">
								<div class="table-responsive">
									<table class="table table-striped">
										<thead>
										  <tr>
											<th>DATE/TIME</th>
											<th>Trip ID</th>
											<th>BILLING AMOUNT</th>
											<th>DISTANCE</th>
											<th>VIEW DETAILS</th>
										  </tr>
										</thead>
										<tbody>
											<tr>
												<td>
													<span class="rid_nme">
														<div class="cheak_box_here">
															<div class="checkbox">
																<input type="checkbox" id="checkbox6">
																<label for="checkbox6"></label>
															</div>
														</div>
														<span class="text_chek">21-july-2017 10:25 AM</span>
													</span>
												</td>
												<td id="completed_ride_view_id" style="display:none">18</td>
												<td>TRIP000000012</td>
												<td>$ 232</td>
												<td>15 km</td>
												<td><button class="btn btn-view" >View</button></td>
											</tr>
										</tbody>
									  </table>
								</div>
							</div>
						</div>
					  </div>
					  <div id="cancel_ride" class="tab-pane fade">
							<div class="form_tsb_trip">
								<div class="trip_form_head">
									<h3>TRIP SERVICE DATE:</h3>
								</div>
								<div class="trip_form_here">
									<form class="form-inline form_biilling_date">
										  <div class="form-group col-md-3 padd-left0">
												<label class="form_label">TRIP SERVICE DATE:</label>
							
													<input id="" type="text" class="form-control date-picker hasDatepicker" name="date_of_birth" autocomplete="off" readonly="" value=""/>
												
										  </div>
										  <div class="form-group col-md-3">
												<label class="form_label">TRIP ID NUMBER</label>
												<input type="text" class="form-control" placeholder="Enter ID Number" />
										  </div>
										  <div class="form-group col-md-3">
												<label class="form_label">DISPUTE PAID TRIP AMOUNTS</label>
												<input type="text" class="form-control" placeholder="Enter Amount" />
										  </div>
										  <div class="form-group col-md-3 padd-right0">
												<label class="form_label">NON-PAYMENT</label>
												<input type="text" class="form-control"/>
										  </div>
									</form>
								</div>
								<div class="trip_service_btn">
									<button type="button" class="btn servic_btn">Proceed</button>
									<button type="button" class="btn servic_btn_sec">Cancel</button>
								</div>
							</div>
					  </div>
					  <div id="custom" class="tab-pane fade">
						<h3 class="rid_head">Customer Care Information</h3>
						<p>Coming Soon</p>
					  </div>
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>

<?php $this->load->view("user/footer"); ?>