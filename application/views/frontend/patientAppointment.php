 <?php $this->load->view("frontend/header"); ?>
<!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <i class="fa fa-envelope"></i>
                        <h1>Appointment scheduling /</h1>
                        <p>Here is patient appointment scheduling</p>
                    </div>
                </div>
            </div>
        </div>
          <style type="text/css">
	        .error {
		      color:red;
		      font-size:13px;
		      
		      }
        </style>
        <!-- Contact Us -->
        <div class="contact-us-container">
        	<div class="container">
	            <div class="row">
	                <div class="col-sm-7">	                   
	                    <form action="<?php echo  base_url(); ?>frontend/patient/appointmentScheduling" method="post">
	                    <?php $success_msg = $this->session->flashdata('success');
	                    $fail = $this->session->flashdata('fail'); 
			                if($success_msg){
		                    ?>
		                    <div class="alert alert-success">
		                      <?php echo $success_msg; ?>
		                    </div>
		                  <?php
		                  }
		                  if($fail){
		                    ?>
		                    <div class="alert alert-danger">
		                      <?php echo $fail; ?>
		                    </div>
		                    <?php
		                  }
	                    ?>
	                    <h3><center>Appointment scheduling</center></h3>
	                    	<div class="form-group">
	                    		<h4>Appointment Date</h4>
	                        	<input type="text" style=" height:40px; font-size:14pt;" name="appointment_date" placeholder="click to show datepicker" id="datepicker">
	                        	 <?php echo form_error('appointment_date', '<div class="error">', '</div>'); ?>
	                        </div>
	                        <div class="form-group">
	                        	<h4>Appointment Time</h4>
	                        	<input type="text" style=" height:40px; font-size:14pt;" name="time" class="timepicker">
	                        	 <?php echo form_error('time', '<div class="error">', '</div>'); ?>
	                        </div>
	                         <div class="form-group">
	                        	<h4>Pick up address</h4>
	                        	<textarea name="pick_up_address" rows="4" cols="85" placeholder="Pick up address"></textarea>
	                        	 <?php echo form_error('pick_up_address', '<div class="error">', '</div>'); ?>
	                        </div>	                        
	                        <div class="form-group">
	                    		<h4>Type of healthcare services</h4>
	                        	<select name="h_services">
	                        		<option value="" disabled selected hidden>select healthcare services</option>
	                        		<option value="Wheel chair">Wheel chair</option>
	                        		<option value="Breathing Apparatus">Breathing Apparatus</option>
	                        	</select>
	                        	 <?php echo form_error('h_services', '<div class="error">', '</div>'); ?>
	                        </div>
	                        <div class="form-group">
	                    		<h4>Special Requests</h4>
	                        	<select name="special_request">
	                        		<option value="" disabled selected hidden>select special request</option>
	                        		<option value="Need to sit upfront">Need to sit upfront</option>
	                        		<option value="need to have ac on">need to have ac on</option>
	                        		<option value="need a wheel chair">need a wheel chair</option>
	                        		<option value="Request additional passenger assistance">Request additional passenger assistance</option>
	                        		<option value="Need a low riding vehicle">Need a low riding vehicle</option>
	                        		<option value="Need a larger vehicle">Need a larger vehicle</option>
	                        	</select>
	                        	 <?php echo form_error('special_request', '<div class="error">', '</div>'); ?>
	                        </div>
	                        <div class="form-group">
	                    		<h4>Special Instruction</h4>
	                        	<input type="text" name="special_instruction" style=" height:40px; font-size:14pt;" placeholder="Special Instruction" class="contact-email" id="special_inst">
	                        	 <?php echo form_error('special_instruction', '<div class="error">', '</div>'); ?>
	                        </div>	
	                         <div class="form-group">
	                    		<h4>Desired Transportation</h4>
	                        	<select name="trans_services">
	                        		<option value="" disabled selected hidden>select desired transportation</option>
	                        		<option value="Ambulance Emergency">Ambulance Emergency</option>
	                        		<option value="Paratransit vehicles">Paratransit vehicles</option>
	                        		<option value="large SUV">large SUV</option>
	                        		<option value="wheel chair lift">wheel chair lift</option>
	                        		<option value="vehicle stretcher">vehicle stretcher</option>
	                        		<option value="Taxi">Taxi</option>
	                        		<option value="Other">Other</option>
	                        	</select>
	                        	 <?php echo form_error('trans_services', '<div class="error">', '</div>'); ?>
	                        </div>
	                    	                        
	                       <button type="submit" name="submit" class="button button-block"/>Submit</button> 
	                      
	                    </form>
	                </div>
	               
	            </div>
	        </div>
        </div>

<script type="text/javascript">
	var today = new Date();
    $( "#datepicker" ).datepicker({
	changeMonth:true,
	changeYear:true,
	minDate:today
	}); 
	$('.timepicker').timepicker({
	    timeFormat: 'h:mm p',
	    interval: 60,
	    minTime: '10',
	    maxTime: '10:00pm',
	    defaultTime: '11',
	    startTime: '10:00',
	    dynamic: false,
	    dropdown: true,
	    scrollbar: true
	});     
</script>
        <!-- Footer -->
<?php $this->load->view("frontend/footer"); ?>
