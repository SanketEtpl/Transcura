 <?php 

 $this->load->view("frontend/header"); ?>
<!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <i class="fa fa-envelope"></i>
                        <h1>Driver Transportation Company</h1>
                        <p>Here is driver transportation company</p>
                    </div>
                </div>
            </div>
        </div>

        <style type="text/css">
			.error {
		      color:red;
		      font-size:13px;
	      	}

        </style>
       
        <!-- Contact Us -->
        <div class="contact-us-container">
        	<div class="container">
	            <div class="row">
	                <div class="col-sm-7">	                   
	                    <form action="<?php echo  base_url(); ?>frontend/driver/driverTranComp" method="post" enctype="multipart/form-data">
	                    <h3>Driver Transportation Company</h3>
	                    	<?php
	                    		$success_msg = $this->session->flashdata("success_msg");
	                    		$error_msg = $this->session->flashdata('error_msg');
	                    		if($success_msg){
				                    ?>
				                    <div class="alert alert-success">
				                      <?php echo $success_msg; ?>
				                    </div>
				                  <?php
				                  }
				                  if($error_msg){
				                    ?>
				                    <div class="alert alert-danger">
				                      <?php echo $error_msg; ?>
				                    </div>
				                    <?php
				                  }
	                    	?>
	                    	<div class="form-group">
	                    		<h4>type of services</h4>
	                        	<select name="trans_services">
	                        		<option value="" disabled selected hidden>select services</option>
	                        		<option value="Paratransit"<?php echo set_select('trans_services', 'Paratransit', TRUE); ?>>Paratransit</option>
	                        		<option value="Bus"<?php echo set_select('trans_services', 'Bus', TRUE); ?>>Bus</option>
	                        		<option value="Airport runs"<?php echo set_select('trans_services', 'Airport runs', TRUE); ?>>Airport runs</option>
	                        		<option value="School transportation"<?php echo set_select('trans_services', 'School transportation', TRUE); ?>>School transportation</option>
	                        		<option value="Medical transport"<?php echo set_select('trans_services', 'Medical transport', TRUE); ?>>Medical transport</option>
	                        		<option value="Taxi"<?php echo set_select('trans_services', 'Taxi', TRUE); ?>>Taxi</option>
	                        		<option value="Limousine"<?php echo set_select('trans_services', 'Limousine', TRUE); ?>>Limousine</option>
	                        		<option value="Ride Share"<?php echo set_select('trans_services', 'Ride Share', TRUE); ?>>Ride Share</option>
	                        		<option value="Group and Party"<?php echo set_select('trans_services', 'Group and Party', TRUE); ?>>Group and Party</option>
	                        		<option value="Shuttle Services"<?php echo set_select('trans_services', 'Shuttle Services', TRUE); ?>>Shuttle Services</option>
	                        		<option value="Other"<?php echo set_select('trans_services', 'Other', TRUE); ?>>Other</option>
	                        	</select>
	                        	<?php echo form_error('trans_services', '<div class="error">', '</div>'); ?>
	                        </div>
	                    	<div class="form-group">
	                    		<h4>Date of birth</h4>
	                        	<input type="text" style="height:40px; font-size:14pt;" name="date_of_birth" value="<?php echo set_value('date_of_birth'); ?>" placeholder="click to show datepicker" id="datepicker">
	                        	<?php echo form_error('date_of_birth', '<div class="error">', '</div>'); ?>
	                        </div>
	                        <div class="form-group">
	                        	<h4>Address</h4>
	                        	<textarea name="driver_address" rows="4" cols="85" placeholder="address"><?php echo set_value('driver_address'); ?></textarea>
	                        	<?php echo form_error('driver_address', '<div class="error">', '</div>'); ?>
	                        </div>	
	                        <div class="form-group">
	                        	<h4>Profile Picture</h4>
	                        	<input type="file" style="height:40px; width:250px;font-size:14pt;" value="<?php echo set_value('profile_pic'); ?>" name="profile_pic">
	                        	<?php echo form_error('profile_pic', '<div class="error">', '</div>'); ?>
	                        </div>	                                                 
	                        <div class="form-group">
	                    		<h4>Mobile number</h4>
	                        	<input type="text" style="height:40px; font-size:14pt;" name="mobile_number" value="<?php echo set_value('mobile_number'); ?>" placeholder="enter you mobile number">
	                        	<?php echo form_error('mobile_number', '<div class="error">', '</div>'); ?>
	                        </div>
	                        <div class="form-group">
	                        	<h4>Vehicle documents</h4>
	                        	<input type="file" style="height:40px; width:250px;font-size:14pt;" name="vehicle_doc" value="<?php echo set_value('vehicle_doc'); ?>">
	                        	<?php echo form_error('vehicle_doc', '<div class="error">', '</div>'); ?>
	                        </div>
	                        <div class="form-group">
	                    		<h4>Company name</h4>
	                        	<input type="text" style="height:40px; font-size:14pt;" name="company_name" value="<?php echo set_value('company_name'); ?>" placeholder="enter you company name">
	                        	<?php echo form_error('company_name', '<div class="error">', '</div>'); ?>
	                        </div>
	                        <div class="form-group">
	                        	<h4>Company Address</h4>
	                        	<textarea name="comp_address" rows="4" cols="85" placeholder="Comapany address"><?php echo set_value('comp_address'); ?></textarea>
	                        	<?php echo form_error('comp_address', '<div class="error">', '</div>'); ?>
	                        </div>	
	                        <div class="form-group">
	                        	<h4>Personal documents</h4>
	                        	<input type="file" style="height:40px;width:250px; font-size:14pt;" value="<?php echo set_value('personal_doc'); ?>" name="personal_doc">
	                        	<?php echo form_error('personal_doc', '<div class="error">', '</div>'); ?>
	                        </div>
	                       <button type="submit" name="submit" class="button button-block"/>Submit</button> 
	                      
	                    </form>
	                </div>	               
	            </div>
	        </div>
        </div>

<script type="text/javascript">
	
    $( "#datepicker" ).datepicker({
    	
    });    
</script>
        <!-- Footer -->
<?php $this->load->view("frontend/footer"); ?>