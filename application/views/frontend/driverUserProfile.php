 <?php $this->load->view("frontend/header"); ?>
 <!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <i class="fa fa-user"></i>
                        <h1>User Profile /</h1>
                        <p>Below you can find more information about user profile.</p>
                    </div>
                </div>
            </div>
        </div>        
        <!-- About Us Text -->
        <div class="about-us-container">
        	<div class="container">
	            <div class="row">
	                <div class="col-sm-12 about-us-text wow fadeInLeft">
	                    <h3 align="center">User Profile</h3>
	                    <table border="1" align="center">
	                   		 <tr>
								<th style="width:300px;height:40px;text-align: center;">Picture</th>
								<td style="width:300px;height:40px;text-align: center;"><img src="<?php echo base_url(); ?><?PHP if(!empty($userData[0]['picture']))ECHO $userData[0]['picture']; else echo 'uploads/Driver/thumb/default_avatar_male.jpg';?>" height="70px" width="70px"></td>
							</tr>
							<tr>
								<th style="width:300px;height:40px;text-align: center;">User Name</th>
								<td style="width:300px;height:40px;text-align: center;"><?php echo ucfirst($userData[0]['first_name']).' '.ucfirst($userData[0]['last_name']); ?></td>
							</tr>
							<tr>
								<th style="width:300px;height:40px;text-align: center;">Phone Number</th>
								<td style="width:300px;height:40px;text-align: center;"><?php echo $userData[0]['phone']; ?></td>
							</tr>
							<tr>
								<th style="width:300px;height:40px;text-align: center;">Email ID</th>
								<td style="width:300px;height:40px;text-align: center;"><?php echo $userData[0]['email']; ?></td>
							</tr>							
							<tr>
								<th style="width:300px;height:40px;text-align: center;">Date of birth</th>
								<td style="width:300px;height:40px;text-align: center;"><?php echo $userData[0]['date_of_birth']; ?></td>
							</tr>
							<tr>
								<th style="width:300px;height:40px;text-align: center;">Address</th>
								<td style="width:300px;height:40px;text-align: center;"><?php echo $userData[0]['address']; ?></td>
							</tr>
							
							<tr>
								<th style="width:300px;height:40px;text-align: center;">Zipcode</th>
								<td style="width:300px;height:40px;text-align: center;"><?php echo $userData[0]['zipcode']; ?></td>
							</tr>
							<tr>
								<th style="width:300px;height:40px;text-align: center;">Company contact no</th>
								<td style="width:300px;height:40px;text-align: center;"><?php echo $userData[0]['company_contactno']; ?></td>
							</tr>
							<tr>
								<th style="width:300px;height:40px;text-align: center;">Company name</th>
								<td style="width:300px;height:40px;text-align: center;"><?php echo $userData[0]['company_name']; ?></td>
							</tr>
							<tr>
								<th style="width:300px;height:40px;text-align: center;">Company address</th>
								<td style="width:300px;height:40px;text-align: center;"><?php echo $userData[0]['company_address']; ?></td>
							</tr>
							<tr>
								<th style="width:300px;height:40px;text-align: center;">Vehicle doc</th>
								<td style="width:300px;height:40px;text-align: center;"><?php if(!empty($userData[0]['vehicle_doc'])): ?><a href="<?php echo base_url(); ?><?php echo $userData[0]['vehicle_doc']; ?>" target="_blank">Download Fiile</a><?php endif; ?></td>
							</tr>
							<tr>
								<th style="width:300px;height:40px;text-align: center;">Personal doc</th>
								<td style="width:300px;height:40px;text-align: center;"><?php if(!empty($userData[0]['personal_doc'])): ?><a href="<?php echo base_url(); ?><?php echo $userData[0]['personal_doc']; ?>" target="_blank">Download Fiile</a><?php endif; ?></td>
							</tr>	
													
						</table>
						<br/><br/>
	                </div>
	            </div>
	        </div>
        </div>

        <?php $this->load->view("frontend/footer"); ?>
