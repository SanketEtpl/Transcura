
    <?php $this->load->view("user/header"); 

    ?>
    <style type="text/css" >
      .form-let{
        padding: 40px;
        max-width: 600px;
        margin: 60px auto;
        border-radius: 4px;
        box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);
      }
      .form-let h1{
        color: #000;
      }
      .form-let input, .form-let select{
        color: #000;
      } 
      .form-let label , .form-let label .req{
        color: rgba(0, 0, 0, 0.38);
      }
      .tab-group .active a, .tab-group li a:hover, .btnlog, .btnlog:hover {
        background: #0a8bc0;
      }
     .error {
      color:red;
      font-size:13px;
      
      }

    </style>

    <div class="presentation-container">
          <div class="container">
            <div class="row">
            
        <div class="form-let">
            <ul class="tab-group">
                <li class="tab active"><a href="#signup">Sign Up</a></li>
                <li class="tab"><a href="#login">Log In</a></li>
            </ul>
      
      <div class="tab-content">
          <div id="signup">   
            <?php
             $success_msg = $this->session->flashdata('auth_reg');   
             $auth_error  = $this->session->flashdata('auth_error');
             $confirm_msg = $this->session->flashdata('confirm_msg'); 
             $log_s = $this->session->flashdata('forgot_success_msg');
             $change_success_msg =$this->session->flashdata('change_success_msg');
             $forgot__error_msg =$this->session->flashdata('forgot__error_msg'); 
             if($change_success_msg){
                    ?>
                    <div class="alert alert-success">
                      <?php echo $change_success_msg; ?>
                    </div>
                  <?php
                  }
              
             if($log_s){
                    ?>
                    <div class="alert alert-success">
                      <?php echo $log_s; ?>
                    </div>
                  <?php
                  }
                  if($forgot__error_msg){
                    ?>
                    <div class="alert alert-danger">
                      <?php echo $forgot__error_msg; ?>
                    </div>
                    <?php
                  }  
                     if($success_msg){
                    ?>
                    <div class="alert alert-success">
                      <?php echo $success_msg; ?>
                    </div>
                  <?php
                  }
                  if($auth_error){
                    ?>
                    <div class="alert alert-danger">
                      <?php echo $auth_error; ?>
                    </div>
                    <?php
                  }
                  if($confirm_msg){
                    ?>
                    <div class="alert alert-success">
                      <?php echo $confirm_msg; ?>
                    </div>
                  <?php
                  }

                  ?>
              <h1>Registration</h1>          
               <form action="<?php echo  base_url(); ?>frontend/login/registration" method="post" id="registrationForm" name="registrationForm">          
                  <div class="top-row">
                    <div class="field-wrap">
                      <!-- <label>
                        Full Name<span class="req">*</span>
                      </label> -->
                      <input type="text" name="first_name" placeholder="Full name" value="<?php echo set_value('first_name'); ?>" autocomplete="off" />
                      <?php echo form_error('first_name', '<div class="error">', '</div>'); ?>

                    </div> 
                    <div class="field-wrap">
                      <!-- <label>
                        Last Name<span class="req">*</span>
                      </label> -->
                      <input type="text" name="last_name" placeholder="Last Name" value="<?php echo set_value('last_name'); ?>" autocomplete="off"/>
                      <?php echo form_error('last_name', '<div class="error">', '</div>'); ?>
                    </div>
                  </div>       

                   <div class="field-wrap">
                    
                     <select name="user_type">
                        <option value="">Select</option>
                         <?php foreach($userData as $ut_val): ?>                         
                        <option value="<?php echo $ut_val['role_name']; ?>"<?php echo set_select('user_type', $ut_val['role_name'], FALSE); ?>><?php echo $ut_val['role_name']; ?></option>
                      <?php endforeach; ?>
                      </select>
                       <?php echo form_error('user_type', '<div class="error">', '</div>'); ?>
                    </div>

                  <div class="field-wrap">
                   <!--  <label>
                      Email Address<span class="req">*</span>
                    </label> -->
                    <input type="email" placeholder="Email Address" value="<?php echo set_value('email'); ?>" name="email" autocomplete="off"/>
                    <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                  </div>

                 <div class="field-wrap">
                 <!--  <label>
                    User Name<span class="req">*</span>
                  </label> -->
                  <input type="text" name="username" placeholder="User Name" value="<?php echo set_value('username'); ?>" autocomplete="off"/>
                  <?php echo form_error('username', '<div class="error">', '</div>'); ?>
                </div>
          
                <div class="field-wrap">
                  <!-- <label>
                    Set A Password<span class="req">*</span>
                  </label> -->
                  <input type="password" name="password" placeholder="Set A Password" autocomplete="off"/>
                  <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                </div>

                <div class="field-wrap">
                  <!-- <label>
                    Confirm Password<span class="req">*</span>
                  </label> -->
                  <input type="password" placeholder="Confirm Password" name="confirmpwd" autocomplete="off"/>
                  <?php echo form_error('confirmpwd', '<div class="error">', '</div>'); ?>
                </div>

              <div class="field-wrap">
                <!-- <label>
                  Phone Number<span class="req">*</span>
                </label> -->
                <input type="text" name="phone_no" placeholder="Phone Number" value="<?php echo set_value('phone_no'); ?>" autocomplete="off"/>
                <?php echo form_error('phone_no', '<div class="error">', '</div>'); ?>
              </div>          
              <button type="submit" name="submit" class="btnlog button button-block"/>Registration</button>        
              </form>
            </div>
        
        <div id="login">   
          <h1>Welcome Back!</h1>          
          <form action="<?php echo  base_url(); ?>frontend/login/login" method="post" id="loginForm" name="loginForm">
          <?php
           $log_error_msg = $this->session->flashdata('log_error_msg');
            if($log_error_msg){
              ?>
              <div class="alert alert-danger">
                <?php echo $log_error_msg; ?>
              </div>
            <?php
            }
            ?>   

            <div class="field-wrap">
            <!-- <label>
              Email Address<span class="req">*</span>
            </label> -->
            <input type="email" name="lemail" placeholder="Email Address" required value="<?php echo set_value('lemail'); ?>" autocomplete="off"/>
             <?php echo form_error('lemail', '<div class="error">', '</div>'); ?>
          </div>          
          <div class="field-wrap">
            <!-- <label>
              Password<span class="req">*</span>
            </label> -->
            <input type="password" name="lpassword" placeholder="Password" required autocomplete="off"/>
             <?php echo form_error('lpassword', '<div class="error">', '</div>'); ?>
          </div>   
          <div class="field-wrap">
                    <!-- <label>
                     User Type<span class="reg">*</span> 
                    </label> -->
                     <select name="luser_type" required>
                        <option value="">Select</option>
                        <?php foreach($userData as $ut_val): ?>
                        <option value="<?php echo $ut_val['role_name']; ?>"<?php echo set_select('luser_type', $ut_val['role_name'], FALSE); ?>><?php echo $ut_val['role_name']; ?></option>
                      <?php endforeach; ?>
                      </select>
                       <?php echo form_error('luser_type', '<div class="error">', '</div>'); ?>
                    </div>       
          <p class="forgot"><a href="<?php echo base_url()?>forgot-pwd">Forgot Password?</a></p>          
          <button class="button button-block btnlog" name="login"/>Log In</button>
          </form>
        </div>        
      </div><!-- tab-content -->      
    </div> <!-- /form -->
    </div>
    </div>
    </div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src="<?php echo base_url(); ?>js/index.js"></script>
 
 <?php $this->load->view("frontend/footer"); ?>
