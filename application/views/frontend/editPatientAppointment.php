 <?php $this->load->view("frontend/header"); 
 ?>
<!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <i class="fa fa-envelope"></i>
                        <h1>Appointment scheduling /</h1>
                        <p>Here is patient appointment scheduling</p>
                    </div>
                </div>
            </div>
        </div>
          <style type="text/css" media="screen">
	      /*  form {
			    display: inline-block;
			    text-align: center;
			    }	*/
        </style>
        <!-- Contact Us -->
        <div class="contact-us-container">
        	<div class="container">
	            <div class="row">
	                <div class="col-sm-7">	                   
	                    <form action="<?php echo  base_url(); ?>frontend/patient/editappointmentScheduling" method="post">
	                    <?php echo $this->session->flashdata('success'); ?>
	                    <h3><center>Appointment scheduling</center></h3>
	                    	<div class="form-group">
	                    		<h4>Appointment Date</h4>
	                        	<input type="text" style=" height:30px; font-size:14pt;" name="appointment_date" value="<?php echo $editAppointData[0]['appoint_date']; ?>" placeholder="click to show datepicker" id="datepicker">
	                        </div>
	                        <div class="form-group">
	                        	<h4>Appointment Time</h4>
	                        	<input type="text" value="<?php echo $editAppointData[0]['appoint_time']; ?>"  style=" height:30px; font-size:14pt;" name="time" class="timepicker">
	                        </div>
	                         <div class="form-group">
	                        	<h4>Pick up address</h4>
	                        	<textarea name="pick_up_address" rows="4" cols="85" placeholder="Pick up address"><?php echo $editAppointData[0]['appoint_pick_up_add']; ?></textarea>
	                        </div>	                        
	                        <div class="form-group">
	                    		<h4>Type of healthcare services</h4>
	                        	<select name="h_services">
	                        		<option value="" disabled selected hidden>select healthcare services</option>
	                        		<option value="Wheel chair"<?php if($editAppointData[0]['appoint_health_service']=="Wheel chair") echo "selected=selected"; else echo ''; ?>>Wheel chair</option>
	                        		<option value="Breathing Apparatus"<?php if($editAppointData[0]['appoint_health_service']=="Breathing Apparatus") echo "selected=selected"; else echo ''; ?>>Breathing Apparatus</option>
	                        	</select>
	                        </div>
	                        <div class="form-group">
	                    		<h4>Special Requests</h4>
	                        	<select name="special_request">
	                        		<option value="" disabled selected hidden>select special request</option>
	                        		<option value="Need to sit upfront"<?php if($editAppointData[0]['appoint_special_request']=="Need to sit upfront") echo "selected=selected"; else echo ''; ?>>Need to sit upfront</option>
	                        		<option value="need to have ac on"<?php if($editAppointData[0]['appoint_special_request']=="need to have ac on") echo "selected=selected"; else echo ''; ?>>need to have ac on</option>
	                        		<option value="need a wheel chair"<?php if($editAppointData[0]['appoint_special_request']=="need a wheel chair") echo "selected=selected"; else echo ''; ?>>need a wheel chair</option>
	                        		<option value="Request additional passenger assistance"<?php if($editAppointData[0]['appoint_special_request']=="Request additional passenger assistance") echo "selected=selected"; else echo ''; ?>>Request additional passenger assistance</option>
	                        		<option value="Need a low riding vehicle"<?php if($editAppointData[0]['appoint_special_request']=="Need a low riding vehicle") echo "selected=selected"; else echo ''; ?>>Need a low riding vehicle</option>
	                        		<option value="Need a larger vehicle"<?php if($editAppointData[0]['appoint_special_request']=="Need a larger vehicle") echo "selected=selected"; else echo ''; ?>>Need a larger vehicle</option>
	                        	</select>
	                        </div>
	                        <div class="form-group">
	                    		<h4>Special Instruction</h4>
	                        	<input type="text" value="<?php echo $editAppointData[0]['appoint_special_instruction']; ?>" name="special_instruction" style=" height:30px; font-size:14pt;" placeholder="Special Instruction" class="contact-email" id="special_inst">
	                        </div>	
	                         <div class="form-group">
	                    		<h4>Desired Transportation</h4>
	                        	<select name="trans_services">
	                        		<option value="" disabled selected hidden>select desired transportation</option>
	                        		<option value="Ambulance Emergency"<?php if($editAppointData[0]['appoint_desire_trans']=="Ambulance Emergency") echo "selected=selected"; else echo ''; ?>>Ambulance Emergency</option>
	                        		<option value="Paratransit vehicles"<?php if($editAppointData[0]['appoint_desire_trans']=="Paratransit vehicles") echo "selected=selected"; else echo ''; ?>>Paratransit vehicles</option>
	                        		<option value="large SUV"<?php if($editAppointData[0]['appoint_desire_trans']=="large SUV") echo "selected=selected"; else echo ''; ?>>large SUV</option>
	                        		<option value="wheel chair lift"<?php if($editAppointData[0]['appoint_desire_trans']=="wheel chair lift") echo "selected=selected"; else echo ''; ?>>wheel chair lift</option>
	                        		<option value="vehicle stretcher"<?php if($editAppointData[0]['appoint_desire_trans']=="vehicle stretcher") echo "selected=selected"; else echo ''; ?>>vehicle stretcher</option>
	                        		<option value="Taxi"<?php if($editAppointData[0]['appoint_desire_trans']=="Taxi") echo "selected=selected"; else echo ''; ?>>Taxi</option>
	                        		<option value="Other"<?php if($editAppointData[0]['appoint_desire_trans']=="Other") echo "selected=selected"; else echo ''; ?>>Other</option>
	                        	</select>
	                        </div>
	                    	    <input type="hidden" name="userid" value="<?php echo $editAppointData[0]['appoint_id']; ?>">                      
	                       <button type="submit" name="submit" class="button button-block"/>Submit</button> 
	                      
	                    </form>
	                </div>
	               
	            </div>
	        </div>
        </div>

<script type="text/javascript">
	var today = new Date();
    $( "#datepicker" ).datepicker({
	changeMonth:true,
	changeYear:true,
	minDate:today
	}); 
	$('.timepicker').timepicker({
	    timeFormat: 'h:mm p',
	    interval: 60,
	    minTime: '10',
	    maxTime: '10:00pm',
	    //defaultTime: '11',
	    startTime: '10:00',
	    dynamic: false,
	    dropdown: true,
	    scrollbar: true
	});     
</script>
        <!-- Footer -->
<?php $this->load->view("frontend/footer"); ?>
