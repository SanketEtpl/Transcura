
<!DOCTYPE html>
<html lang="en">

    <head>
        
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Transcura</title>

        <!-- CSS -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"> 
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Sans">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster">
        <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/fonts/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>bootstrap/fonts/stylesheet.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/animate.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/magnific-popup.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>flexslider/flexslider.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/form-elements.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/media-queries.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->

        <link rel="shortcut icon" href="<?php echo base_url(); ?>ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo base_url(); ?>ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url(); ?>ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo base_url(); ?>ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo base_url(); ?>ico/apple-touch-icon-57-precomposed.png">
             <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css" >
             <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
      <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
       <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
       <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>

    </head>

    <body>
        
        <!-- Top menu -->
		<nav class="navbar" role="navigation">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#top-navbar-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="<?php echo base_url(); ?>homepage"><img src="<?php echo base_url(); ?>images/imgpsh_fullsize.png" alt="Mountain View" style="width:217px;height:105px;"></a>
					<!-- <a class="navbar-brand" href="index.html">Andia - a super cool design agency...</a> -->
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="top-navbar-1">
					<ul class="nav navbar-nav navbar-right">
						
								<li class="active"><a href="<?php echo base_url();?>homepage"><i class="fa fa-home"></i><br>Home</a></li>
						<?php if($this->session->userdata('userType')=='Driver'){ ?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000">
								<i class="fa fa-camera"></i><br>Driver
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?php echo base_url(); ?>transportation-company">Transportation company</a></li>
							 	<li><a href="<?php echo base_url(); ?>appointment-trip">Appointment Trip Details</a></li>
							 	<li><a href="<?php echo base_url(); ?>trip-request">My Trip Request Details</a></li>
							</ul>
						</li>
						<?php } ?>
						<?php if($this->session->userdata('userType')=='Patient') { ?>
						<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000">
								<i class="fa fa fa-bed"></i><br>Patient
							</a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="<?php echo base_url();?>patient">Appointment scheduling</a></li>
								<li><a href="<?php echo base_url(); ?>complaint">Complaints</a></li>
								<li><a href="<?php echo base_url(); ?>patient-appointment">Appointment Details</a></li>
							</ul>
						</li>
						<?php } ?>
						<!-- <li>
							<a href="<?php //echo base_url();?>frontend/login/service"><i class="fa fa-tasks"></i><br>Services</a>
						</li> -->
						<?php if(empty($this->session->userdata('userType'))){ ?>
						<li>
							<a href="<?php echo base_url(); ?>patient-info"><i class="fa fa-bed"></i><br>Patient</a>
						</li>
					<?php } ?>
						<?php if(empty($this->session->userdata('userType'))){ ?>
						<li>
							<a href="<?php echo base_url(); ?>driver-info"><i class="fa fa-car"></i><br>Driver</a>
						</li>
							<?php } ?>
						<li>
							<a href="<?php echo base_url(); ?>doctor-info"><i class="fa fa-user-md"></i><br>Doctor</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>about"><i class="fa fa-user"></i><br>About</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>contact"><i class="fa fa-envelope"></i><br>Contact</a>
						</li>
						<?php if(empty($this->session->userdata('userType'))){ ?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000">
								<i class="fa fa-sign-in"></i><br><span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
							 	<li><a href="<?php echo base_url();?>frontend/login">Login & registration</a></li>
							</ul>
						</li>
					<?php } ?>
						<?php if($this->session->userdata('userType')){ ?>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000">
								<i class="fa fa-sign-in"></i><br><span class="caret"></span>
							</a>
							<ul class="dropdown-menu" role="menu">
							 		Welcome <?php echo $this->session->userdata('firstName');?>
							 				&nbsp;<?php echo $this->session->userdata('lastName'); ?> 
							 	<li><a href="<?php echo base_url();?>user-profile">User Profile</a></li>
							 	<li><a href="<?php echo base_url();?>edit-profile">Edit Profile</a></li>			
								<li><a href="<?php echo base_url();?>front-login">logout</a></li>
							</ul>
						</li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</nav>
