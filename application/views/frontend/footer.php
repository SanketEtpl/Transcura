 <!-- Footer -->

        <footer>
            <div class="container">                
                <div class="row">
                    <div class="col-sm-7 footer-copyright wow fadeIn">
                        <p>Copyright 2012 ETPL - All rights reserved. Template by <a href="http://www.exceptionaire.com/">ETPL</a>.</p>
                    </div>
                    <div class="col-sm-5 footer-social wow fadeIn">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-dribbble"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-pinterest"></i></a>
                    </div>
                </div>
            </div>
        </footer>


        <!-- Javascript -->
         
        <script src="<?php echo base_url(); ?>js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url(); ?>bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap-hover-dropdown.min.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.backstretch.min.js"></script>
        <script src="<?php echo base_url(); ?>js/wow.min.js"></script>
        <script src="<?php echo base_url(); ?>js/retina-1.1.0.min.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url(); ?>flexslider/jquery.flexslider-min.js"></script>
        <script src="<?php echo base_url(); ?>js/jflickrfeed.min.js"></script>
        <script src="<?php echo base_url(); ?>js/masonry.pkgd.min.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="<?php echo base_url(); ?>js/jquery.ui.map.min.js"></script>
        <script src="<?php echo base_url(); ?>js/scripts.js"></script>

    </body>

</html>