 <?php $this->load->view("frontend/header"); ?>
 <!-- Page Title -->
<div class="page-title-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 wow fadeIn">
                <i class="fa fa-user"></i>
                <h1>My Trip Request</h1>
                <p>Below fields are trip request</p>
            </div>
        </div>
    </div>
</div>        
<!-- About Us Text -->
<div class="about-us-container">
	<div class="container">
        <div class="row">
            <div class="col-sm-12 about-us-text wow fadeInLeft">
                <h3 align="center">My trip request</h3>
                <table border="1" align="center">
					<tr>
						<th style="height:40px; width:139px;text-align: center;">My accepts trips</th>
						<th style="height:40px; width:139px;text-align: center;">My rejects trips</th>
						<th style="height:40px; width:139px;text-align: center;">My completed trips</th>						
					</tr>
					<tr>
						<td style="height:40px; width:139px;text-align: center;"><a href="<?php echo base_url(); ?>my-accepted-trips">Accept trips</a></td>
						<td style="height:40px; width:139px;text-align: center;"><a href="<?php echo base_url(); ?>my-rejected-trips">Reject trips</a></td>
						<td style="height:40px; width:139px;text-align: center;"><a href="<?php echo base_url(); ?>my-completed-trips">Completed trips</a></td>
					</tr>
				</table>
				<br/><br/>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view("frontend/footer"); ?>
