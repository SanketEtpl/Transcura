 <?php $this->load->view("frontend/header"); 

 ?>
 <!-- Page Title -->
<div class="page-title-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 wow fadeIn">
                <i class="fa fa-user"></i>
                <h1>My Appointments</h1>
                <p>Below fields are appointments</p>
            </div>
        </div>
    </div>
</div>        
<!-- About Us Text -->
<div class="about-us-container">
	<div class="container">
        <div class="row">
            <div class="col-sm-12 about-us-text wow fadeInLeft">
            	<?php 
	                   $success_msg = $this->session->flashdata("success");
	                   $fail_msg = $this->session->flashdata("fail");

	                   	if($success_msg){
		                    ?>
		                    <div class="alert alert-success">
		                      <?php echo $success_msg; ?>
		                    </div>
		                  <?php
		                  }
		                  if($fail_msg){
		                    ?>
		                    <div class="alert alert-danger">
		                      <?php echo $fail_msg; ?>
		                    </div>
		                  <?php
		                  }
	                    ?>
                <h3 align="center">My Appointments</h3>
                <table border="1" align="center">
					<tr>
						<th style="width:120px;height:40px;text-align: center;">Trip ID</th>
						<th style="text-align: center;width:120px;height:40px;">Appointment Date</th>	
						<th style="width:120px;text-align: center;height:40px;">Appointment Time</th>
						<th style="width:120px;height:40px;text-align: center;">Appoint pick up address</th>
						<th style="width:120px;height:40px;text-align: center;">Appoint health service</th>
						<th style="width:120px;height:40px;text-align: center;">Appoint special request</th>
						<th style="width:120px;height:40px;text-align: center;">Appoint desire transportation</th>
						<th style="width:120px;height:40px;text-align: center;">Status</th>
						<th style="text-align: center;width:120px;height:40px;">Action</th>
					</tr>
					<?php foreach($tripData as $val): ?>
					<tr>
						<td style="width:120px;height:40px;text-align: center;"><?php echo $val["tripid"]; ?></td>
						<td style="width:120px;height:40px;text-align: center;"><?php echo $val["appoint_date"]; ?></td>
						<td style="width:120px;height:40px;text-align: center;"><?php echo $val["appoint_time"]; ?></td>
						<td style="width:120px;height:40px;text-align: center;"><?php echo $val["appoint_pick_up_add"]; ?></td>
						<td style="width:120px;height:40px;text-align: center;"><?php echo $val["appoint_health_service"]; ?></td>
						<td style="width:120px;height:40px;text-align: center;"><?php echo $val["appoint_special_request"]; ?></td>
						<td style="width:120px;height:40px;text-align: center;"><?php echo $val["appoint_desire_trans"]; ?></td>
						<td style="width:120px;height:40px;text-align: center;"><?php echo $val["status"]; ?></td>
						<td style="width:120px;height:40px;text-align: center;"><a href="<?php echo base_url();?>editAppointment/<?php echo $val["appoint_id"]; ?>">&nbsp;&nbsp;&nbsp;Edit</a><a href="javascript:void(0);" onclick="cancel(<?php echo $val["appoint_id"]; ?>)">&nbsp;&nbsp;&nbsp;Cancel</a></td>
					</tr>
					<?php endforeach; ?>
				</table>
				<br/><br/>
            </div>
        </div>
    </div>
</div>
<!-- <?php //echo base_url();?>frontend/patient/cancelAppointment/<?php //echo $val["appoint_id"]; ?>
 -->
<?php $this->load->view("frontend/footer"); ?>
<script type="text/javascript">
	var url="<?php echo base_url(); ?>";
	function cancel(id)
	{
		var r = confirm("Do you want to cancel Appointment");
		if(r==true)
		{
			window.location = url+"frontend/patient/cancelAppointment/"+id;
		}
		else
		{
			return false;
		}
	}
</script>
