 <?php $this->load->view("frontend/header"); ?>
<!-- Page Title -->
    <div class="page-title-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 wow fadeIn">
                    <i class="fa fa-envelope"></i>
                    <h1>Complaints/</h1>
                    <p>Here is patient complaint</p>
                </div>
            </div>
        </div>
    </div>
    <style type="text/css">
            .error {
              color:red;
              font-size:13px;
              
              }
        </style>
    <!-- Contact Us -->
    <div class="contact-us-container">
    	<div class="container">
            <div class="row">
                <div class="col-sm-7">	                   
                    <form action="<?php echo  base_url(); ?>comlaint-on-driver" method="post">
                    <h3><center>Complaint</center></h3>
                    <?php 
                    $success_msg = $this->session->flashdata('success'); 
                    $fail_msg = $this->session->flashdata('fail_msg');
                     if($success_msg){
                            ?>
                            <div class="alert alert-success">
                              <?php echo $success_msg; ?>
                            </div>
                          <?php
                          }
                          if($fail_msg){
                            ?>
                            <div class="alert alert-danger">
                              <?php echo $fail_msg; ?>
                            </div>
                            <?php
                          }
                        ?>
                    	<div class="form-group">
                    		<h4>Patient Name</h4>
                        	<input type="text" name="patient_name" style=" height:40px; font-size:14pt;" placeholder="Patient Name" class="complaint-pname" id="patient_name">
                             <?php echo form_error('patient_name', '<div class="error">', '</div>'); ?>
                        </div>
                    	<div class="form-group">
                    		<h4>Date of service</h4>
                        	<input type="text" style=" height:40px; font-size:14pt;" name="date_of_service" placeholder="click to show datepicker" id="datepicker">
                             <?php echo form_error('date_of_service', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                        	<h4>Driver Name</h4>
                        	<input type="text" placeholder="Driver Name" style=" height:40px; font-size:14pt;" name="complaint_driver_name" class="complaint-dn">
                             <?php echo form_error('complaint_driver_name', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                        	<h4>Name of Transportation provider</h4>
                        	<input type="text" placeholder="Name of transportaion provider" style="height:40px; font-size:14pt;" name="complaint_notp" class="complaint-notp">
                             <?php echo form_error('complaint_notp', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                    		<h4>Complaint type</h4>
                        	<select name="complaint_type">
                        		<option value="" disabled selected hidden>select complaint type</option>
                        		<option value="inappropriate physical contact">inappropriate physical contact</option>
                        		<option value="rude">Rude</option>
                        	</select>
                             <?php echo form_error('complaint_type', '<div class="error">', '</div>'); ?>
                        </div>
                         <div class="form-group">
                        	<h4>Description</h4>
                        	<textarea name="complaint_descrp" rows="2" cols="85" placeholder="Description"></textarea>
                             <?php echo form_error('complaint_descrp', '<div class="error">', '</div>'); ?>
                        </div>	      
                       <button type="submit" name="submit" class="button button-block"/>Submit</button> 
                      
                    </form>
                </div>
               
            </div>
        </div>
    </div>

<script type="text/javascript">
	var today = new Date();
    $( "#datepicker" ).datepicker({
	changeMonth: true,
	    changeYear: true,
	    minDate: today 
}); 
</script>
        <!-- Footer -->
<?php $this->load->view("frontend/footer"); ?>
