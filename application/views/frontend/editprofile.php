 <?php $this->load->view("frontend/header"); ?>
<!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <i class="fa fa-envelope"></i>
                        <h1>Edit Profile /</h1>
                        <p>Here is user edit profile</p>
                    </div>
                </div>
            </div>
        </div>
        <style type="text/css">
	       .error {
		      color:red;
		      font-size:13px;
		      }
        </style>
        <!-- Contact Us -->
        <div class="contact-us-container">
        	<div class="container">
	            <div class="row">
	                <div class="col-sm-7">
	                   <?php 
	                   $success_msg = $this->session->flashdata("success");
	                   	if($success_msg){
		                    ?>
		                    <div class="alert alert-success">
		                      <?php echo $success_msg; ?>
		                    </div>
		                  <?php
		                  }
	                    ?>
	                    <form action="<?php echo  base_url(); ?>frontend/login/changeEditProfile" method="post" enctype="multipart/form-data">
	                    
	                    <h3><center>Edit Profile</center></h3>
				 <div class="form-group">
					<h4>Date of birth</h4>
	                        	<input type="text" value="<?php echo $userData[0]['date_of_birth']; ?>" name="date_of_birth" placeholder="click to show datepicker" id="datepicker">
	                        	<?php echo form_error('date_of_birth', '<div class="error">', '</div>'); ?>
	                        </div>
				
	                       
	                        <div class="form-group">
					<h4>Address</h4>
	                        	<textarea name="address" rows="4" cols="85" placeholder="Your address"><?php echo $userData[0]['address']; ?></textarea>
	                        	<?php echo form_error('address', '<div class="error">', '</div>'); ?>
	                        </div>
	                        <div class="form-group">
					<h4>Zipcode</h4>
	                        	<input type="text" value="<?php echo $userData[0]['zipcode']; ?>" name="zipcode" placeholder="Enter your zipcode" class="contact-zipcode" id="edit-profile-zipcode">
	                        	<?php echo form_error('zipcode', '<div class="error">', '</div>'); ?>
	                        </div>
	                        <div class="form-group">
					<h4>Mobile no</h4>
	                        	<input type="text" name="mobile" placeholder="Enter your mobile number" value="<?php echo $userData[0]['mobile']; ?>" class="contact-name" id="edit_profile_phone">
	                        	<?php echo form_error('mobile', '<div class="error">', '</div>'); ?>
	                        </div>	   
	                    	<div class="form-group">
	                    		<h4>Medical insurance ID</h4>
	                        	<input type="text" name="medical_insurance_id" placeholder="Enter your medical insurance ID" value="<?php echo $userData[0]['insurance_id']; ?>" class="contact-name" id="medical_insurance_id">
	                        	<?php echo form_error('medical_insurance_id', '<div class="error">', '</div>'); ?>
	                        </div>
				 <div class="form-group">
	                        	<h4>Personal documents</h4>
	                        	<input type="file" style="height:40px;width:250px; font-size:14pt;" name="personal_doc">
	                        	<input type="hidden" name="personal_doc1" value="<?php echo $userData[0]['personal_doc']; ?>">
	                        </div>
				<div class="form-group">
	                        	<h4>Picture profile</h4>
	                        	<input type="file" style="height:40px;width:250px; font-size:14pt;" name="picture_profile">
	                        	<input type="hidden" name="picture_profile1" value="<?php echo $userData[0]['picture']; ?>">
	                        </div>
	                        <div class="form-group">
					<h4>Emergency contact name</h4>
	                        	<input type="text" name="emergency_contact_name" placeholder="Enter your emergency contact name" class="contact-name" value="<?php echo $userData[0]['emergency_contactname']; ?>">
	                        	<?php echo form_error('emergency_contact_name', '<div class="error">', '</div>'); ?>
	                        </div>
	                    	 <div class="form-group">
					<h4>Emergency contact number</h4>
	                        	<input type="text" name="emergency_contact_number" value="<?php echo $userData[0]['emergency_contactno']; ?>" placeholder="Enter your emergency contact number" class="contact-name" id="emergency_contact_number">
	                        	<?php echo form_error('emergency_contact_number', '<div class="error">', '</div>'); ?>
	                        </div>
				<div class="form-group">
					<h4>are you able to provide signature</h4>
	                        	<select name="signature">
	                        		<option value="" disabled selected hidden>select</option>
	                        		<option value="yes" <?php if($userData[0]['signature']=="yes")echo "selected=selected"; else ''; ?>>Yes</option>
	                        		<option value="no" <?php if($userData[0]['signature']=="no")echo "selected=selected"; else ''; ?>>No</option>
	                        	</select>
	                        	<?php echo form_error('signature', '<div class="error">', '</div>'); ?>
	                        </div>
	                       <button type="submit" name="submit" class="button button-block"/>Submit</button> 
	                      
	                    </form>
	                </div>
	               
	            </div>
	        </div>
        </div>
	
<script type="text/javascript">
    $( "#datepicker" ).datepicker();      
</script>
        <!-- Footer -->
<?php $this->load->view("frontend/footer"); ?>
