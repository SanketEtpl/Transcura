<!-- <!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Forgot Password</title>
        <link href='https://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">   
        <link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css">
    </head>
  
    <body> -->
      <?php $this->load->view("frontend/header"); ?>
    <style type="text/css" >
      .form-let{
        padding: 40px;
        max-width: 600px;
        margin: 60px auto;
        border-radius: 4px;
        box-shadow: 0 4px 10px 4px rgba(19, 35, 47, 0.3);
      }
      .form{
        background: none repeat scroll 0% 0% rgba(249, 251, 252, 0.9);
        padding: 40px;
        max-width: 600px;
        margin: 40px auto;
        border-radius: 4px;
        box-shadow: 0px 4px 10px 4px rgba(19, 35, 47, 0.3);
      }
     .form , .form-let h1{
        color: #000;
      }
      .form-let input, .form-let select{
        color: #000;
      } 
      .form-let label , .form-let label .req{
        color: rgba(0, 0, 0, 0.38);
      }
      .tab-group .active a, .tab-group li a:hover, .btnlog, .btnlog:hover {
        background: #0a8bc0;
      }

    </style>
     <div class="presentation-container">
          <div class="container">
            <div class="row">
        <div class="form">
          
        <div id="login">   
          <h1>Password</h1>          
          <form action="<?php echo  base_url(); ?>frontend/login/changeUserPwd" method="post" id="loginForm" name="loginForm">          

            <div class="field-wrap">
            <h4>
              Enter you password<span class="req">*</span>
            </h4>
            <input type="password" name="password" required placeholder="Enter your password">
          </div>                             
                  
          <button class="button button-block" name="sumbit"/>Submit</button>
          </form>
        </div>        
         
    </div> <!-- /form -->
  </div>
</div>
</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src="<?php echo base_url(); ?>js/index.js"></script>
 <!--  </body>
</html> -->
<?php $this->load->view("footer"); ?>

