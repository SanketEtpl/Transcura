 <?php $this->load->view("frontend/header"); ?>
 <!-- Page Title -->
        <div class="page-title-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 wow fadeIn">
                        <i class="fa fa-user"></i>
                        <h1>Driver /</h1>
                        <p>Below you can find more information driver our hospital</p>
                    </div>
                </div>
            </div>
        </div>

        <!-- About Us Text -->
        <div class="about-us-container">
        	<div class="container">
	            <div class="row">
	                <div class="col-sm-12 about-us-text wow fadeInLeft">
	                   
	                    <p>
	                    	The hospital does not assume responsibility for any valuables, clothing or other personal items. Please leave valuables at home or send them home with members of your family. Eyeglasses, contact lenses and dentures should be kept in protective containers when not in use. If you have prosthetic devices such as dentures, hearing aids, etc., and have no special containers for them, please ask your nurses for containers. If you should misplace any of your belongings, please notify the nurse manager of the floor you occupied. Items left after you are discharged are kept at the security office for 30 days before they are discarded.
	                    </p>
	                    <h3>Facilities</h3>
	                    <p>
	                    	 <ul>	
						    <li>Navigation systems </li>
						    <li>neoSense - the new feel behind your touch </li>
						    <li>Infotainment </li>
						    <li>Eco.Logic Coasting </li>
						    <li>Software services and smartphone apps </li>
						   
						</ul>
	                    </p>
	                </div>
	            </div>
	        </div>
        </div>

        <!-- Meet Our Team -->
        <div class="team-container">
        	<div class="container">
	            <div class="row">
		            <div class="col-sm-12 team-title wow fadeIn">
		                <h2>Meet Our Team</h2>
		            </div>
	            </div>
	            <div class="row">	            	
	            	<div class="col-sm-3">
		                <div class="team-box wow fadeInUp">
		                    <img src="<?php echo base_url(); ?>img/team/1.jpg" alt="" data-at2x="<?php echo base_url(); ?>img/team/1.jpg">
		                    <h3>John Doe</h3>
		                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor...</p>
		                    <div class="team-social">		                        
		                        <a href="#"><i class="fa fa-facebook"></i></a>
		                        <a href="#"><i class="fa fa-twitter"></i></a>
		                        <a href="#"><i class="fa fa-linkedin"></i></a>
		                        <a href="#"><i class="fa fa-envelope"></i></a>
		                    </div>
		                </div>
	                </div>
	                <div class="col-sm-3">
		                <div class="team-box wow fadeInDown">
		                    <img src="<?php echo base_url(); ?>img/team/2.jpg" alt="" data-at2x="<?php echo base_url(); ?>img/team/2.jpg">
		                    <h3>Jane Doe</h3>
		                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor...</p>
		                    <div class="team-social">		                        
		                        <a href="#"><i class="fa fa-facebook"></i></a>
		                        <a href="#"><i class="fa fa-twitter"></i></a>
		                        <a href="#"><i class="fa fa-linkedin"></i></a>
		                        <a href="#"><i class="fa fa-envelope"></i></a>
		                    </div>
		                </div>
	                </div>
	                <div class="col-sm-3">
		                <div class="team-box wow fadeInUp">
		                    <img src="<?php echo base_url(); ?>img/team/3.jpg" alt="" data-at2x="<?php echo base_url(); ?>img/team/3.jpg">
		                    <h3>Tim Brown</h3>
		                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor...</p>
		                    <div class="team-social">		                        
		                        <a href="#"><i class="fa fa-facebook"></i></a>
		                        <a href="#"><i class="fa fa-twitter"></i></a>
		                        <a href="#"><i class="fa fa-linkedin"></i></a>
		                        <a href="#"><i class="fa fa-envelope"></i></a>
		                    </div>
		                </div>
	                </div>
	                <div class="col-sm-3">
		                <div class="team-box wow fadeInDown">
		                    <img src="<?php echo base_url(); ?>img/team/4.jpg" alt="" data-at2x="<?php echo base_url(); ?>img/team/4.jpg">
		                    <h3>Sarah Red</h3>
		                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor...</p>
		                    <div class="team-social">		                        
		                        <a href="#"><i class="fa fa-facebook"></i></a>
		                        <a href="#"><i class="fa fa-twitter"></i></a>
		                        <a href="#"><i class="fa fa-linkedin"></i></a>
		                        <a href="#"><i class="fa fa-envelope"></i></a>
		                    </div>
		                </div>
	                </div>
	            </div>
	        </div>
        </div>

        <!-- Testimonials -->
        <div class="testimonials-container">
	        <div class="container">
	        	<div class="row">
		            <div class="col-sm-12 testimonials-title wow fadeIn">
		                <h2>Testimonials</h2>
		            </div>
	            </div>
	            <div class="row">
	                <div class="col-sm-10 col-sm-offset-1 testimonial-list">
	                	<div role="tabpanel">
	                		<!-- Tab panes -->
	                		<div class="tab-content">
	                			<div role="tabpanel" class="tab-pane fade in active" id="tab1">
	                				<div class="testimonial-image">
	                					<img src="<?php echo base_url(); ?>img/testimonials/1.jpg" alt="" data-at2x="<?php echo base_url(); ?>img/testimonials/1.jpg">
	                				</div>
	                				<div class="testimonial-text">
		                                <p>
		                                	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. 
		                                	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. 
		                                	Lorem ipsum dolor sit amet, consectetur..."<br>
		                                	<a href="#">Lorem Ipsum, dolor.co.uk</a>
		                                </p>
	                                </div>
	                			</div>
	                			<div role="tabpanel" class="tab-pane fade" id="tab2">
	                				<div class="testimonial-image">
	                					<img src="<?php echo base_url(); ?>img/testimonials/2.jpg" alt="" data-at2x="<?php echo base_url(); ?>img/testimonials/2.jpg">
	                				</div>
	                				<div class="testimonial-text">
		                                <p>
		                                	"Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip 
		                                	ex ea commodo consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit 
		                                	lobortis nisl ut aliquip ex ea commodo consequat..."<br>
		                                	<a href="#">Minim Veniam, nostrud.com</a>
		                                </p>
	                                </div>
	                			</div>
	                			<div role="tabpanel" class="tab-pane fade" id="tab3">
	                				<div class="testimonial-image">
	                					<img src="<?php echo base_url(); ?>img/testimonials/3.jpg" alt="" data-at2x="<?php echo base_url(); ?>img/testimonials/3.jpg">
	                				</div>
	                				<div class="testimonial-text">
		                                <p>
		                                	"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. 
		                                	Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. 
		                                	Lorem ipsum dolor sit amet, consectetur..."<br>
		                                	<a href="#">Lorem Ipsum, dolor.co.uk</a>
		                                </p>
	                                </div>
	                			</div>
	                			<div role="tabpanel" class="tab-pane fade" id="tab4">
	                				<div class="testimonial-image">
	                					<img src="<?php echo base_url(); ?>img/testimonials/1.jpg" alt="" data-at2x="<?php echo base_url(); ?>img/testimonials/1.jpg">
	                				</div>
	                				<div class="testimonial-text">
		                                <p>
		                                	"Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip 
		                                	ex ea commodo consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit 
		                                	lobortis nisl ut aliquip ex ea commodo consequat..."<br>
		                                	<a href="#">Minim Veniam, nostrud.com</a>
		                                </p>
	                                </div>
	                			</div>
	                		</div>
	                		<!-- Nav tabs -->
	                		<ul class="nav nav-tabs" role="tablist">
	                			<li role="presentation" class="active">
	                				<a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab"></a>
	                			</li>
	                			<li role="presentation">
	                				<a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab"></a>
	                			</li>
	                			<li role="presentation">
	                				<a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab"></a>
	                			</li>
	                			<li role="presentation">
	                				<a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab"></a>
	                			</li>
	                		</ul>
	                	</div>
	                </div>
	            </div>
	        </div>
        </div>
        <?php $this->load->view("frontend/footer"); ?>