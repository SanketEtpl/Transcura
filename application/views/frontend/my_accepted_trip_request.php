 <?php $this->load->view("frontend/header"); ?>
 <!-- Page Title -->
<div class="page-title-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 wow fadeIn">
                <i class="fa fa-user"></i>
                <h1>My Accepted Trip Request</h1>
                <p>Below fields are my accepted trip request</p>
            </div>
        </div>
    </div>
</div>        
<!-- About Us Text -->
<div class="about-us-container">
	<div class="container">
        <div class="row">
            <div class="col-sm-12 about-us-text wow fadeInLeft">
                <h3 align="center">My accepted trip request</h3>
                <table border="1" align="center">
					<tr>
						<th style="height:40px; width:139px;text-align: center;">Trip ID</th>
                        <th style="height:40px; width:139px;text-align: center;">Appointment Date</th>                       
                        <th style="height:40px; width:139px;text-align: center;">Appointment Time</th>
                        <th style="height:40px; width:139px;text-align: center;">Appoint pick up address</th>
                        <th style="height:40px; width:139px;text-align: center;">Appoint health service</th>
                        <th style="height:40px; width:139px;text-align: center;">Appoint special request</th>
                        <th style="height:40px; width:139px;text-align: center;">Appoint desire transportation</th>
					</tr>
                    <?php foreach($accetedTR as $val): ?>
                    <tr>
						<td style="height:40px; width:139px;text-align: center;"><?php echo $val["tripid"]; ?></td>
                        <td style="height:40px; width:139px;text-align: center;"><?php echo $val["appoint_date"]; ?></td>
                        <td style="height:40px; width:139px;text-align: center;"><?php echo $val["appoint_time"]; ?></td>
                        <td style="height:40px; width:139px;text-align: center;"><?php echo $val["appoint_pick_up_add"]; ?></td>
                        <td style="height:40px; width:139px;text-align: center;"><?php echo $val["appoint_health_service"]; ?></td>
                        <td style="height:40px; width:139px;text-align: center;"><?php echo $val["appoint_special_request"]; ?></td>
                        <td style="height:40px; width:139px;text-align: center;"><?php echo $val["appoint_desire_trans"]; ?></td>
					</tr>
                <?php endforeach; ?>
				</table>
				<br/><br/>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view("frontend/footer"); ?>