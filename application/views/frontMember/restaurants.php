<?php $this->load->view('user/header'); ?>
<?php $this->load->view('frontMember/memberHeader'); ?>
<div id="schLoadingDiv" style="position: fixed;z-index: 999;background-color: rgba(0, 0, 0, 0.35);width: 100%;height: 100%;top: 0;text-align: center;left:0;">
	<img id="loader-img" alt="loading" src="<?php echo base_url(); ?>assets/img/loading.gif" align="center" style="margin: 0 auto;width: 60px;margin-top: 218px;
	"/>
</div>
<div>
	<div class="theme-breadcrumb pad-50">
		<div class="theme-container container ">
			<div class="row">
				<div class=" col-md-12">
					<div class="title-wrap">
						<br />
						<!-- <h2 class="section-title no-margin">Restaurants</h2> -->
						<p class="fs-16 no-margin">Search for <?php echo $this->session->userdata('service_here'); ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="theme-container container ">

		<div class="row">
			<div class="col-md-12">

				<div class="row">
					<div class="col-md-4">
						<div class="form-group">
							<label for="exampleFormControlInput1">Enter Location</label>
							<input type="text" class="form-control" id="location" placeholder="Enter Location">
						</div>


					</div>

					<div class="col-md-6">
						<div class="form-group">
							<label for="exampleFormControlInput1">Search <?php echo $this->session->userdata('service_here');; ?></label>
							<input type="text" class="form-control" id="search_query" placeholder="Search <?php echo $this->session->userdata('service_here');; ?>">
						</div>

					</div>

					<div class="col-md-2" style="padding-top: 25px;">
						<div class="form-group">
							<button type="button" id="seach_restaurant" class="btn btn-primary">Search</button>
						</div>
					</div>
					
				</div>
				
			</div>
			
		</div>

		<div id="responseData">
			
		</div>

		<div id="recordLength"></div>

	</div>


	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Restaurant Details</h4>
				</div>
				<div class="modal-body">

					<div class="container" id="tourpackages-carousel">

						<div class="row">

							<div class="col-xs-18 col-sm-6 col-md-3">
								<div class="thumbnail">
									<div class="caption" id="restContent">

									</div>
								</div>
							</div>
						</div>
					</div>


				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="button" class="btn btn-primary">Book Ride</button>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->


	<!-- <section class="about-wrap">
			<div class="theme-container container">
				<div class="row">
					<div class="col-md-12">
						<div class="about-us pt-10">							
							<?php //if(!empty($aboutData)){ echo $aboutData[0]['page_detail']; } else { echo ''; } ?>
						</div>
					</div>					
				</div>
			</div>
		</section> -->
	</div>
	<?php $this->load->view('user/footer'); ?>
	<script type="text/javascript">
		function initialize_location() {
			var input_loc = document.getElementById('location');
			var input_srch = document.getElementById('search_query');
			var autocomplete = new google.maps.places.Autocomplete(input_loc);
			var autocomplete = new google.maps.places.Autocomplete(input_srch);
		}
		$(document).ready(function(){			
			google.maps.event.addDomListener(window, 'load', initialize_location);
		});
	</script>
	<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBe-UniUGzwJHL5kCOU4hsMj4z-ptyDuyw&v=3.exp&sensor=false&libraries=places&callback=initialize_location"></script>