<?php $this->load->view("user/header"); ?>
<?php $this->load->view('frontMember/memberHeader'); ?>
<div class="driver_sched_strt">
	<div class="container">
		<div class="row">
			<div class="driver_strt_inn">
				<h3 class="heal_head">Schedule A Ride</h3>
				<div class="dri_main">
					<div class="col-md-6">						
						<?php if($this->session->userdata('user_id')){ ?>				
						<div class="rightform">
							<div class="sch_log">								
								<form name="schedule_ride" action="<?php echo base_url(); ?>member-dashboard" method="POST" autocomplete="off">
									<div class="row col_padding">	
										<!---->
										<?php
										$success = $this->session->flashdata("success");
										$fail = $this->session->flashdata('fail');
										if($success) { ?>
										<div class="alert alert-success">
											<?php echo $success; ?>
										</div>
										<?php
									}
									if($fail){ ?>
									<div class="alert alert-danger">
										<?php echo $fail; ?>
									</div>
									<?php } ?>
									<div class="col-md-12 col_padding">
										<div class="top-form text-left">
											<!---->
											<div class="form-group">
												<label class="control-label col-sm-6 pdlr-0" for="datepicker1"><span class="glyphicon glyphicon-calendar"></span>&nbsp;Date of Service 
													<div class="req-red">*</div></label>
													<div class="col-sm-6 pdlr-0">
														<div class="input-group">
															<input type="text" class="form-control" id="datepicker1" value="<?php echo date("m/d/Y"); ?>" name="date" placeholder="mm-dd-yyyy">													
														</div>
														<?php echo form_error('date', '<div class="error">', '</div>'); ?>															
													</div>
												</div>
												<!---->	 
												<!-- <div class="form-group">
													<label class="control-label col-sm-6 pdlr-0" for="aptime"><span class="glyphicon glyphicon-time"></span>&nbsp;Appointment Time <div class="req-red">*</div></label>
													<div class="col-sm-6">
														<div class="input-group">
															<input type="text" name="appointment_time" id="aptime" value="<?php echo set_value('appointment_time'); ?>" class="timepicker form-control" placeholder="00: 00: 00"/>												
														</div>		
														<?php echo form_error('appointment_time', '<div class="error">', '</div>'); ?>													
													</div>
												</div> -->													
												<!---->	
												<div class="form-group">
													<label class="control-label col-sm-6 pdlr-0" for="pick-up-time"><span class="glyphicon glyphicon-time"></span>&nbsp;Pick a Time <div class="req-red">*</div></label>
													<div class="col-sm-6">
														<div class="input-group">
															<input type="text" name="pick_up_time" id="timepicker" value="<?php echo date("H:i A"); ?>" class="timepicker form-control" placeholder="00: 00: 00"/>												
														</div>		
														<?php echo form_error('pick_up_time', '<div class="error">', '</div>'); ?>													
													</div>
												</div>														
												<div class="form-group pickaddress">
													<label class="control-label col-sm-12 pdlr-0" for="pick-up-address"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>&nbsp;Pick Up Address</label>
													<div class="col-sm-12">
														<div class="input-group">
															<label class="control-label col-sm-6">country <div class="req-red">*</div></label>
															<div class="col-sm-6">
																<!-- <select class="form-control" id="country" name="country">
																	<option value="" class="selecttxt" disable selected hidden>Select Country</option>
																	<?php 
																	if(!empty($countryData)){
																		foreach($countryData as $country_name) {
																			?>
																			<option value="<?php echo $country_name['id']; ?>"<?php echo set_select('country',$country_name['id'],FALSE);?>><?php echo $country_name['country_name']?></option>
																			<?php } } else { ?>	
																			<option value=""></option>
																			<?php } ?>
																</select> -->

																<label>United States</label>
																		<?php echo form_error('country', '<div class="error">', '</div>'); ?>
																	</div>											
																</div>															
															</div>	
															<div class="col-sm-12">
																<div class="input-group">
																	<label class="control-label col-sm-6">State <div class="req-red">*</div></label>
																	<div class="col-sm-6">
																		<select class="form-control" id="state" name="state">
																			<option class="selecttxt" value="" disable selected hidden>Select State</option>
																			<?php 
																				//print_r($states);
																			foreach ($states as $state){?>

																						<option class="selecttxt" value="<?php echo $state['id'] ?>" ><?php echo $state['state_name'] ?></option>


																			<?php } ?>
																																						
																		</select>	
																		<?php echo form_error('state', '<div class="error">', '</div>'); ?>
																	</div>
																	
																</div>															
															</div>	
															<div class="col-sm-12">
																<div class="input-group">
																	<label class="control-label col-sm-6">Street <div class="req-red">*</div></label>
																	<div class="col-sm-6">
																		<input type="text" name="pick_up_street" id="street" value="<?php echo set_value('pick_up_street'); ?>" maxlength="100" class="form-control input-sm" placeholder="Enter Street"/>						<?php echo form_error('pick_up_street', '<div class="error">', '</div>'); ?>						
																	</div>
																	
																</div>															
															</div>
															<div class="col-sm-12">
																<div class="input-group">
																	<label class="control-label col-sm-6">Zipcode <div class="req-red">*</div></label>
																	<div class="col-sm-6">
																		<input type="text" id="zipcode" name="zipcode" value="<?php echo set_value('zipcode'); ?>" class="form-control input-sm" maxlength="10" placeholder="Enter Zipcode"/>										<?php echo form_error('zipcode', '<div class="error">', '</div>'); ?>	
																	</div>
																	
																</div>															
															</div>														
														</div>
														<!---->	
													</div>
												</div>
												<!--form-->
												<div class="new-frm">
													<!--element-->
													<div class="form-group">
														<label for="usertype" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Type of Services <div class="req-red">*</div></label>
														<select class="form-control" name="member_service" id="member_service">
															<option value="" disable selected hidden>Select</option>
															<?php
															if(!empty($mem_service)) {
																foreach($mem_service as $key => $value) { ?>
																<option value="<?php echo $value['ms_id']; ?>"<?php echo set_select('member_service', $value['ms_id'], FALSE); ?>><?php echo $value['ms_type']; ?></option>
																<?php } } else { ?>
																<option value=""></option>
																<?php } ?>												
															</select>
															<?php echo form_error('member_service', '<div class="error">', '</div>'); ?>
														</div>

														<div class="form-group">
															<label for="usertype" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Special Instructions <div class="req-red">*</div></label>
															<input type="text" name="special_instruction" class="form-control" id="special_instruction" value="<?php echo set_value('special_instruction'); ?>" maxlength="50">
															<?php echo form_error('special_instruction', '<div class="error">', '</div>'); ?>
														</div>

														<div class="form-group">
															<label for="usertype" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Desired Transportation <div class="req-red">*</div></label>
															<select class="form-control" id="desire_transp" name="desire_transp">
																<option value="" disable selected hidden>Select</option>
																<?php
																if(!empty($desire_tran)) {
																	foreach($desire_tran as $key => $value) { ?>
																	<option value="<?php echo $value['carcat_id']; ?>"<?php echo set_select('desire_transp', $value['carcat_id'], FALSE); ?>><?php echo $value['car_category']." &nbsp;(".$value['car_capacity']." passenger)"; ?></option>
																	<?php } } else { ?>	
																	<option value=""></option>
																	<?php } ?>	
																</select>
																<?php echo form_error('desire_transp', '<div class="error">', '</div>'); ?>
															</div>



															<!--element-->
															<div class="proc_btn">
																<input type="submit" value="CONTINUE" class="btn btn-block btn-primary"/>
															</div>														
														</div>
													</div>
												</form>										
											</div>
										</div>
									</div>
									<?php } ?>	
									<div class="col-md-6">
										<div class="sch_img" style="background-image:url('<?php echo base_url(); ?>assets/img/sch_car.png');top:0px;"></div>
									</div>		
								</div>					
							</div>
						</div>
					</div>
				</div>
			</div>	
			<?php $this->load->view("user/footer"); ?>