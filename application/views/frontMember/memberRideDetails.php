<?php $this->load->view("user/header"); ?>
<?php $this->load->view('frontMember/memberHeader'); ?>
<div class="all_comp_wrapp">
	<div class="container">
		<div class="row">
			<div class="comp_inner">
				<h3 class="heal_head">
					SCHEDULE TRIP DETAILS		
					<div id="error_message"></div>
					<!-- <span class="back_box"><button class="back_btn customer_complaints" id="customer_complaints_bk" name="customer_complaints_bk">Back</button></span> -->

					
				</h3>
				<div class="comp_box bluebg showridedetails">					
					<form name="member_bookride_form" id="member_bookride_form" method="POST" action="" autocomplete="off">
						<input type="hidden" name="memberSourceAddr" id="memberSourceAddr" value="<?php  echo $this->session->userdata('memberSource');?>">
						<input type="hidden" name="memberDestinationAddr" id="memberDestinationAddr" value="<?php  echo $this->session->userdata('memberDestination');?>">
						<input type="hidden" name="end_lat" id="end_lat" value="<?php  echo $this->session->userdata('end_lat');?>">
						<input type="hidden" name="end_long" id="end_long" value="<?php  echo $this->session->userdata('end_long');?>">
						<!-- <div class="row"> -->

						<div class="form-group">
							<div class="col-md-2 comp_lab">
								<label>Ride Date:</label>
								<div class="comp_inpt">
									<?php echo $this->session->userdata('memberRestaurant')['sr_date'];?>
								</div>								
							</div>
							<div class="col-md-2 comp_lab">
								<label>Ride Time :</label>
								<div class="comp_inpt">
									<?php echo $this->session->userdata('memberRestaurant')['sr_pick_up_time'];?>
								</div>								
							</div>
						</div>


						<div class="form-group">
							<div class="col-md-12 pdl0">
								<label class="col-md-12 row" for="lblname">Pick Up Address :</label> 
								<div class="col-md-4 comp_lab innrechild pdl0">
									<label>Street :</label>
									<div class="comp_inpt">
										<?php echo $this->session->userdata('memberRestaurant')['sr_street'];?>
									</div>		
								</div>		
								<div class="col-md-4 comp_lab innrechild pdl0">
									<label>State :</label>
									<div class="comp_inpt">
										<?php if(!empty($state[0])) echo $state[0]['state_name']; else echo '';?>
									</div>		
								</div>	
								<div class="col-md-4 comp_lab innrechild pdl0">
									<label>Zipcode :</label>
									<div class="comp_inpt">
										<?php echo $this->session->userdata('memberRestaurant')['sr_zipcode'];?>
									</div>		
								</div>					
							</div>
						</div>


						<div class="form-group">
							<div class="col-md-4 comp_lab">
								<label> Total Distance :</label>
								<div class="comp_inpt">
									<input type="hidden" name="tot_distance" id="tot_distance" value="<?php echo $totdistance ?>">
									<?php echo $totdistance ." miles"; ?>
								</div>								
							</div>
							<div class="col-md-4 comp_lab">
								<label> Desired Transportation :</label>
								<div class="comp_inpt">
									<?php echo $transportation[0]['car_category']; ?>
								</div>								
							</div>
							<div class="col-md-4 comp_lab">
								<label> Rate per Mile : </label>
								<div class="comp_inpt">
									<?php echo $transportation[0]['car_rate_per_mile']; ?>
								</div>								
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-4 comp_lab">
								<label> Duration  :</label>
								<div class="comp_inpt">
								<input type="hidden" name="trip_duration" id="trip_duration" value="<?php echo $duration;  ?>"> 	<?php echo $duration;  ?>
								</div>								
							</div>
							<div class="col-md-4 comp_lab">
								<label> Total Amount  :</label>
								<div class="comp_inpt">
									<input type="hidden" name="tot_amt" id="tot_amt" value="<?php echo round(floatval($totdistance * $transportation[0]['car_rate_per_mile']),2);  ?>"> 
									<?php echo round(floatval($totdistance * $transportation[0]['car_rate_per_mile']),2);  ?>
								</div>								
							</div>
						</div>

					</form>	
					<div class="pay_btn_box mb15">
						<!-- 	<button type="button" class="req_btn comp_btn" id="btn_book_ride" data-toggle="modal">PAY</button>	 -->					
						<button type="button" class="req_btn comp_btn" id="bookMemberRequest" data-toggle="modal">Book Ride</button>	<!-- data-target="#subcomp_pop" -->				
					</div>	
				</div>


			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="memberRideModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog">    
		<div class="modal-content sma_pop">
			<div class="modal-header">
				<button type="button" class="close complaint_ok" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>       
			</div>
			<div class="modal-body">
				<div class="succ_icon"><img src="<?php echo base_url(); ?>assets/img/succpay_icon.png"></div>
				<h4 class="succ_tit">Successful !!!</h4>
				<h2 class="read_trip" id="responseMsg"></h2>
				<center><div id="rideTripId"></div></center>
			</div>
			<div class="modal-footer ok_btn">
				<button type="button" id="schedule_ride_ok" class="btn btn-default cont_btn complaint_ok" data-dismiss="modal">OK</button>			
			</div>
		</div>
	</div>



  <!-- <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <div id="responseMsg"></div>
        <div id="rideTripId"></div>
      </div>
      
    </div>
</div> -->
</div>


<!--submit complaints popup-->

<?php $this->load->view('user/footer'); ?>