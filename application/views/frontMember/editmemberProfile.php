<?php $this->load->view("user/header"); ?>
<?php $this->load->view('frontMember/memberHeader'); ?>
<div id="schLoadingDiv" style="position: fixed;z-index: 999;background-color: rgba(0, 0, 0, 0.35);width: 100%;height: 100%;top: 0;text-align: center;left:0;">
	<img id="loader-img" alt="loading" src="<?php echo base_url(); ?>assets/img/loading.gif" align="center" style="margin: 0 auto;width: 60px;margin-top: 218px;
	"/>
</div>
<div class="myprof_wrapp">
	<div class="probgimg" style="background-image:url('<?php echo base_url(); ?>assets/img/profile_bg.png');"></div>
	<div class="mypro_info_wrapp edit_pro_wrapp">
		<div class="container">
			<div class="row">
				<form name="update_profile" action="<?php echo base_url(); ?>update-member-profile" method="POST" autocomplete="off" enctype="multipart/form-data">
					<div class="tp_pro_sect">
						<div class="pro_img_box image-upload" id="preview_image" style="background-image:url('<?php if(!empty($member_profile[0])){ if(!empty($member_profile[0]['picture'])){ echo base_url().$member_profile[0]['picture']; }else{ echo base_url()."assets/img/no_img.png"; } } ?>');">						
							<label class="edit_icon" for="file-input">
								<i class="fa fa-pencil" aria-hidden="true"></i>	
							</label>							
							<input type="hidden" id="profile_image_old" name="profile_image_old" value="<?php if(!empty($member_profile[0])){ echo $member_profile[0]['picture']; } ?>">
							<input id="file-input" name="profile_image" type="file" />
						</div>
						<div class="pro_img_name">
							<div class="pro_name_box">
								<div class="pro_nme_input"><input type="text" placeholder="Name"  maxlength="50" name="full_name" id="fullname" value="<?php if(!empty($member_profile[0])){ echo $member_profile[0]['full_name']; } ?>" class="form-control"></div>
								<?php echo form_error('full_name', '<div class="error">', '</div>'); ?>
								<div class="edit_prof_btn"><a href="<?php echo base_url();?>member-profile">View Profile</a></div>
							</div>
						</div>
					</div>
					<div class="basic_info_wrapp">
						<?php
						$success = $this->session->flashdata("success");
						$fail = $this->session->flashdata('error');
						if($success) { ?>
						<div class="alert alert-success col-md-offset-1 col-md-8">
							<?php echo $success; ?>
						</div>
						<?php
					}
					if($fail){ ?>
					<div class="alert alert-danger col-md-offset-1 col-md-8">
						<?php echo $fail; ?>
					</div>
					<?php } ?>
					<div class="row">
						<div class="col-md-6">
							
							<div class="info_head">Basic Information :</div>
							<div class="basic_box">
								<div class="row">
									<div class="col-md-6 edit_box">
										<label><img src="<?php echo base_url(); ?>assets/img/date_icon.png">Date of Birth:</label>	
										<div class="edit_inpt"><input type="text" placeholder="Date of birth" name="date_of_birth" id="date_of_birth" value="<?php if(!empty($member_profile[0])){ echo $member_profile[0]['date_of_birth']; } ?>" class="form-control date-picker"></div>
										<?php echo form_error('date_of_birth', '<div class="error">', '</div>'); ?>
									</div>
									<div class="col-md-6 edit_box">
										<label><img src="<?php echo base_url(); ?>assets/img/mob_no.png">Mobile Number:</label>	
										<div class="edit_inpt"><input type="text" placeholder="Mobile number" name="phone_no" id="phnnumber" maxlength="20" value="<?php if(!empty($member_profile[0])){ echo $member_profile[0]['phone']; } ?>" class="form-control"></div>
										<?php echo form_error('phone_no', '<div class="error">', '</div>'); ?>
									</div>
									<div class="col-md-6 edit_box">
										<label><img src="<?php echo base_url(); ?>assets/img/email_add.png">Email Address:</label>	
										<div class="edit_inpt"><input type="text" placeholder="Email ID" name="email" id="emailaddrss" maxlength="50" value="<?php if(!empty($member_profile[0])){ echo $member_profile[0]['email']; } ?>" class="form-control" readonly="readonly"></div>
										<?php echo form_error('email', '<div class="error">', '</div>'); ?>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="info_head">Address :</div>
							<div class="basic_box">
								<div class="row">
									<div class="col-md-6 edit_box">
										<label><img src="<?php echo base_url(); ?>assets/img/coun.png">Country:</label>	
										<div class="edit_inpt">
											<label><?php if(!empty($member_profile[0])){echo $member_profile[0]['country_name'];} ?></label>
										</div>
										<?php echo form_error('country', '<div class="error">', '</div>'); ?>
									</div>

									<div class="col-md-6 edit_box">
										<label><img src="<?php echo base_url(); ?>assets/img/state.png">State:</label>	
										<div class="edit_inpt">
											<select class="form-control" name="state" id="state_edit">
												<option value="<?php echo $member_profile[0]['state']; ?>" class="selecttxt" disable selected hidden><?php echo $member_profile[0]['state_name']; ?></option>
												<?php 
												if(!empty($stateData)){
													foreach($stateData as $state_name) {
														?>
														<option value="<?php echo $state_name['id']; ?>" <?php if($transport_profile[0]['state'] == $state_name['id']){ echo "selected"; } ?> ><?php echo $state_name['state_name']?></option>
														<?php } } else { ?>	
														<option value=""></option>
														<?php } ?>
													</select>
												</div>
												<?php echo form_error('state', '<div class="error">', '</div>'); ?>
											</div>

											<div class="col-md-6 edit_box">
												<label><img src="<?php echo base_url(); ?>assets/img/state.png">City:</label>	
												<div class="edit_inpt">
													<select class="form-control" name="city" id="city_edit">
														<option class="selecttxt" value="<?php echo $member_profile[0]['city']; ?>" disable selected hidden><?php echo $member_profile[0]['city_name']; ?></option>
														<?php 
														if(!empty($cityData)){
															foreach($cityData as $city_name) {
																?>
																<option value="<?php echo $city_name['id']; ?>" <?php if($member_profile[0]['city'] == $city_name['id']){ echo "selected"; } ?> ><?php echo $city_name['city_name']?></option>
																<?php } } else { ?>	
																<option value=""></option>
																<?php } ?>
															</select>
														</div>
														<?php echo form_error('city', '<div class="error">', '</div>'); ?>
													</div>
													<div class="col-md-6 edit_box">
														<div class="row">
															<div class="col-md-6">
																<label><img src="<?php echo base_url(); ?>assets/img/zip.png">Zip Code:</label>	
																<div class="edit_inpt">
																	<input type="text" placeholder="Zipcode" maxlength="15" name="zipcode" id="zipcode" value="<?php if(!empty($member_profile[0])){ echo $member_profile[0]['zipcode']; } ?>" class="form-control">
																</div>
																<?php echo form_error('zipcode', '<div class="error">', '</div>'); ?>
															</div>
															<div class="col-md-6">
																<label><img src="<?php echo base_url(); ?>assets/img/zip.png">County:</label>	
																<div class="edit_inpt">
																	<input type="text" placeholder="County" maxlength="50" name="county" id="county" value="<?php if(!empty($member_profile[0])){ echo $member_profile[0]['county']; } ?>" class="form-control">
																</div>
																<?php echo form_error('county', '<div class="error">', '</div>'); ?>
															</div>
														</div>
													</div>
													<div class="col-md-12 edit_box">
														<label><img src="<?php echo base_url(); ?>assets/img/zip.png">Street:</label>	
														<div class="edit_inpt">
															<textarea placeholder="Street" name="street" maxlength="255" id="street" class="form-control"><?php if(!empty($member_profile[0])){ echo $member_profile[0]['street']; } ?></textarea>
														</div>		
														<?php echo form_error('street', '<div class="error">', '</div>'); ?>																									
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="info_head">Personal Information :</div>
											<div class="basic_box">
												<div class="row">								

													<div class="col-md-6 edit_box">
														<label><img src="<?php echo base_url(); ?>assets/img/emer_no.png">Emergency Contact Name:</label>	
														<div class="edit_inpt"><input type="text" placeholder="Emergency contact name" maxlength="50" name="emergency_contact_name" id="eme_name" value="<?php if(!empty($member_profile[0])){ echo $member_profile[0]['emergency_contactname']; } ?>" class="form-control"></div>
														<?php echo form_error('emergency_contact_name', '<div class="error">', '</div>'); ?>
													</div>
													<div class="col-md-6 edit_box">
														<label><img src="<?php echo base_url(); ?>assets/img/mob_no.png">Emergency Contact Number:</label>	
														<div class="edit_inpt"><input type="text" placeholder="Emergency contact number" maxlength="20" name="emergency_contact_number" id="eme_number" value="<?php if(!empty($member_profile[0])){ echo $member_profile[0]['emergency_contactno']; } ?>" class="form-control"></div>
														<?php echo form_error('emergency_contact_number', '<div class="error">', '</div>'); ?>
													</div>
													
												</div>
											</div>
										</div>

										<div class="col-md-6">
											<div class="info_head"><br/></div>



											<div class="col-md-12 edit_box">
												<label><img src="<?php echo base_url(); ?>assets/img/sign.png">Are you able to Provide Signatures:</label>	
												<div class="edit_inpt_radio radio_cust">												  
													<span class="radio_btn">													
														<input type="radio" id="yes" name="signature" value="1" <?php if(!empty($member_profile[0])){ if($member_profile[0]['signature'] == "1"){ echo "checked=checked"; } } ?>>
														<label for="yes">Yes</label>
													</span>
													<span class="radio_btn">
														<input type="radio" id="no" name="signature" value="2" <?php if(!empty($member_profile[0])){ if($member_profile[0]['signature'] == "2"){ echo "checked=checked"; } } ?>>
														<label for="no">No</label>
													</span>
												</div>
												<?php echo form_error('signature', '<div class="error">', '</div>'); ?>											
												<div id="file_signature">
													<div class="upld_sign">
														<span class="input-group-btn">
															<!-- image-preview-input -->
															<div class="btn btn-primary image-preview-input">
																<span class="image-preview-input-title">Upload Signatures</span>
																<input class="upsign" type="file" name="sign_file_upload" id="sign_file_upload"> <!-- rename it -->
																<input class="upsign" type="hidden" name="sign_file_upload_old" id="sign_file_upload" value="<?php if(!empty($member_profile[0])){ echo $member_profile[0]['signature_file_upload']; } ?>"> 
															</div>													
														</span>												
													</div>
												</div>											
											</div>
										</div>
										<div class="clearfix"></div>
										<div class=" col-md-12">								
											<div class="subedit_form">
												<input class="req_btn comp_btn" type="submit" name="edit_profile_save" value="Submit">
											</div>									
										</div>
									</form>
									<div class="clearfix"></div>
									<br /><br />

									<div class="col-md-6">
										<div class="info_head">Card Details :</div>
										<div class="basic_box edit_box">
											<?php
											if(!empty($card_data))
											{		
												$card_details = json_encode($card_data[0]);
												$card_details_new=json_decode($card_details,true);
												?>

												<div class="basic_inner">
													<div class="bas_lt"><i class="fa fa-credit-card"></i><span>Card Type:</span></div>
													<div class="bas_rt"><?php echo $card_details_new['brand']; ?></div>
												</div>
												<div class="basic_inner">
													<div class="bas_lt"><i class="fa fa-credit-card"></i><span>Card Number:</span></div>
													<div class="bas_rt"><?php echo "****". $card_details_new['last4']; ?></div>
												</div>
												<div class="basic_inner">
													<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/date_icon.png"><span>Card Expires:</span></div>
													<div class="bas_rt"><?php echo $card_details_new['exp_month']."/".$card_details_new['exp_year']; ?></div>
												</div>
												<!-- update credit card details -->
												<form action="<?php echo base_url(); ?>update-stripe-details" method="POST">
													<input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>">
													<script
													src="https://checkout.stripe.com/checkout.js" class="stripe-button"
													data-key="<?php echo STRIPE_PUBLISHABLE_KEY ?>"
													data-email="<?php echo $this->session->userdata('user_email'); ?>"
													data-name="Card Details"
													data-panel-label="Update Card Details"
													data-label="Update Card Details"
													data-allow-remember-me=false
													data-locale="auto">
												</script>
											</form> 

											<?php } else
											{
												echo '<div class="basic_inner">No details found.</div>';
											}
											?>
										</div>
									</div>


								</div>
							</div>	
							
						</div>
					</div>
				</div>
			</div>
			<style type="text/css">
				a {
					cursor: pointer !important; 
				}
				.image-upload > input
				{
					display: none;
				}

				.image-upload img
				{
					width: 50px;
					cursor: pointer;
				}

			</style>
			<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
			<script type="text/javascript">
				$(document).ready(function(){
					$("#schLoadingDiv").show();
					$(".edit_prof_btn,.stripe-button").click(function(){
						$("#schLoadingDiv").show();
					});	
					});
			</script>
			<?php $this->load->view("user/footer"); ?>
