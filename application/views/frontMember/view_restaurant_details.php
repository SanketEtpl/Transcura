<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('user/header'); ?>
<?php $this->load->view('frontMember/memberHeader'); ?>
<div id="schLoadingDiv" style="position: fixed;z-index: 999;background-color: rgba(0, 0, 0, 0.35);width: 100%;height: 100%;top: 0;text-align: center;left:0;">
	<img id="loader-img" alt="loading" src="<?php echo base_url(); ?>assets/img/loading.gif" align="center" style="margin: 0 auto;width: 60px;margin-top: 218px;
	"/>
</div>


<div class="contact_main innerpages address_main">
	<div class="inner-wrapper contact-us">
		<div class="container">
			<div class="row"> 
				<br /> 
				<h2 class="mb15">Schedule Ride</h2> 
				<div class="backdiv col-md-12 text-right" style="margin-bottom:15px; margin-top: -42px;">
					<a href="<?php echo base_url() ?>restaurants" class="btn btn-primary">Back</a>
				</div>
				<div class="col-sm-4 col-md-4 col-xs-12 search-health-form" onload="initialize()">      
					<div class="forms">
						<form name="frm_book_rest" id="frm_book_rest" action="#" method="POST">
							<div class="controls field">                                  
								<div class="row">
									<div class="col-md-12">                       
										<div class="form-group">
											<label for="exampleFormControlInput1">Source Point</label>
											<input id="source" type="text" name="source" value="<?php echo set_value('source'); ?>" class="form-control name" placeholder="Source Point" maxlength="255">
											<div class="help-block with-errors"></div>
										</div>  


										<div class="form-group">
											<label for="exampleFormControlInput1">Destination Point</label>
											<input id="destination" type="text" name="destination" value="<?php echo $restaurant_details['result']['formatted_address']; ?>" class="form-control name" placeholder="Destination Point" maxlength="255" readonly="">

											<!-- <input id="destination" type="text" name="destination" value="<?php echo $restaurant_details->result->address; ?>" class="form-control name" placeholder="Destination Point" maxlength="255" readonly=""> -->
											<div class="help-block with-errors"></div>
										</div>               
									</div>
								</div>                  
							</div>
							<div class="row">                               
								<div class="col-md-12 submitbtn">
									<input type="hidden" id="start_lat" name="start_lat">
									<input type="hidden" id="start_long" name="start_long">
									<input type="hidden" id="end_lat" name="end_lat" value="<?php echo $restaurant_details['result']['geometry']['location']['lat']; ?>">
									<input type="hidden" id="end_long" name="end_long" value="<?php echo $restaurant_details['result']['geometry']['location']['lng']; ?>">

             <!--  <input type="hidden" id="end_lat" name="end_lat" value="<?php echo $restaurant_details->location->latitude; ?>">
             <input type="hidden" id="end_long" name="end_long" value="<?php echo $restaurant_details->location->longitude; ?>"> -->
             <input type="hidden" id="distance" name="distance">
             <input type="hidden" id="duration" name="duration">

             <input id="getRestaAddress" type="button" name="back" value="Get address"/>
         </div>
     </div>                                 
 </form>
</div>
</div>

<div class="col-sm-8 col-md-8 col-xs-12 search-health-map">   
	<div id="map" style="width:100%;height: 270px;"></div>
</div>
</div> 


</div>
</div>
</div> 


<!-- <div>
  <div class="theme-breadcrumb pad-50">
    <div class="theme-container container ">
      <div class="row">
        <div class=" col-md-12">
          <div class="title-wrap">
            <br />
            <p class="fs-16 no-margin">Schedule Ride</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="theme-container container ">    
    <div class="row">
      <a href="<?php echo base_url() ?>restaurants"><button>Back</button></a>
      <hr />
      <form name="frm_book_rest" id="frm_book_rest" action="#" method="POST">
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleFormControlInput1">Source Point</label>
                <input id="source" type="text" name="source" value="<?php echo set_value('source'); ?>" class="form-control name" placeholder="Source Point" maxlength="255">
                <div class="help-block with-errors"></div>
              </div>


            </div>

            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleFormControlInput1">Destination Point</label>
                <input id="destination" type="text" name="destination" value="<?php echo $restaurant_details->location->address; ?>" class="form-control name" placeholder="Destination Point" maxlength="255" readonly="">
                <div class="help-block with-errors"></div>
              </div>
            </div>

            <div class="col-md-12">
             <input type="hidden" id="start_lat" name="start_lat">
             <input type="hidden" id="start_long" name="start_long">
             <input type="hidden" id="end_lat" name="end_lat" value="<?php echo $restaurant_details->location->latitude; ?>">
             <input type="hidden" id="end_long" name="end_long" value="<?php echo $restaurant_details->location->longitude; ?>">
             <input type="hidden" id="distance" name="distance">
             <input type="hidden" id="duration" name="duration">
           </div>

           <div class="col-md-12 submitbtn">                    
            <input id="getRestaAddress" type="button" name="back" value="Get address"/>
            <!-- <input id="scheduleRestRideBtn" type="button" name="schedule_ride" value="Schedule ride"/> -->
        </div>                  
    </div>              
</div>
</form>
</div>
<br /> 
   <!--  <div class="col-sm-12" id="drop_point_details">
     <div class="table-responsive">
         <table class="table table-bordered table-striped dataTable" id="listAddress" role="grid" aria-describedby="example1_info">
            <thead>
              <tr role="row">
                  <th width="25%">Start Point</th>
                  <th width="25%">Destination Point</th>    
                  <th width="25%">Distance</th>
                  <th width="25%">Duration</th>
              </tr>              
          </thead>
          <tbody>                   
          </tbody>
      </table>
  </div>
</div> -->
<br />
<div id="map" style="width: 800px;height: 400px;"></div>
</div>
</div> 



<!-- Modal -->
<div class="modal address-show fade" id="restaurantModal" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<form method="POST" action="<?php echo base_url(); ?>member_ride_details">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><span class="jsonName" id="rest_name"></span></h4>
				</div>
				<div class="modal-body">
					<h4 class="text-center addrs">Address of <?php echo $this->session->userdata('service_here'); ?></h4>

					<input type="hidden" name="source" id="source_point">
					<input type="hidden" name="destination" id="destination_point">
					<div class="bluebg">
						<div class="form-group" >
							<label class="col-md-12" for="lblname">Address</label> 
							<span class="col-md-12" id="rest_address"></span>               
						</div>  
						<div class="form-group" >
							<label for="lblname" class="col-md-12">Mobile Number</label> 
							<span id="rest_mob_number" class="col-md-12"></span>               
						</div>  
						<div class="form-group" >
							<label for="lblname" class="col-md-12">Opening Hours</label> 
							<span id="rest_open_hours" class="col-md-12"></span>               
						</div>  
					</div>


				</div>

				<div class="modal-footer">
					<input type="submit" name="" value="Continue" class="req_btn comp_btn">
				</div>
			</form>
		</div>

	</div>
</div>

<!-- <div class="modal fade" id="restaurantModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
    <form method="POST" action="<?php echo base_url(); ?>/member_ride_details">  
      <input type="hidden" name="source" id="source_point">
      <input type="hidden" name="destination" id="destination_point">      
      <div class="modal-body">
        <div class="jumbotron">
          <label>Name :</label>
          <span id="rest_name"></span>
          <!--  <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p> -->
          <!-- <hr class="my-4">
          <label>Address</label>
          <span id="rest_address"></span>

          <hr class="my-4">
          <label>Mobile Number</label>
          <span id="rest_mob_number"></span>

          <hr class="my-4">
          <label>Opening Hours</label>
          <span id="rest_open_hours"></span>
          
        </div>
      </div>
      <div class="modal-footer"> -->
      	<!-- <button type="button" class="btn btn-primary">Save changes</button> -->

        <!-- <input type="submit" name="" value="Continue" class="btn btn-primary">

      </div>
    </form>
    </div>
  </div>
</div>  -->



<!-- JS to show map --> 
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>   
<script type="text/javascript">
// ------ autocomplete for source point ----------
$(document).ready(function(){
	function initialize_source() {
		var input = document.getElementById('source');
		var autocomplete = new google.maps.places.Autocomplete(input);
	}
	google.maps.event.addDomListener(window, 'load', initialize_source);


//---------- get current location and show in source point ------------

  /*var geocoder = new google.maps.Geocoder();
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
  } 
  //Get the latitude and the longitude;
  function successFunction(position) {
    var lat = position.coords.latitude;
    var lng = position.coords.longitude;
    codeLatLng(lat, lng)
  }

  function errorFunction(){
    alert("Geocoder failed");
  }

  function initialize() {
    geocoder = new google.maps.Geocoder();
  }

  function codeLatLng(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        console.log(results)
        if (results[1]) {         
          for (var i=0; i<results[0].address_components.length; i++) {
            for (var b=0;b<results[0].address_components[i].types.length;b++) {
              if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                //this is the object you are looking for
                city= results[0].address_components[i];
                break;
              }
            }
          }
          if (confirm("Do you want get current location !")) {
            $("#source").val(results[0].formatted_address);
          }

        } else {
          alert("No results found");
        }
      } else {
        alert("Geocoder failed due to: " + status);
      }
    });
}*/


// sanket geolocation code
  /*getLocation();

  function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
        alert("Geolocation is not supported by this browser.");
    }

    function showPosition(position) {
     alert("Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude);
  }
}*/



});



function initMap() {
	var directionsService = new google.maps.DirectionsService;
	var directionsDisplay = new google.maps.DirectionsRenderer;
	var myLatLng = {lat: <?php echo $restaurant_details['result']['geometry']['location']['lat']; ?>, lng: <?php echo $restaurant_details['result']['geometry']['location']['lng']; ?>};
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 13,
		center: myLatLng
	});
  // var marker = new google.maps.Marker({
  //   position: myLatLng,
  //   map: map,
  //   title: '<?php //echo $restaurant_details['result']['name']; ?>'
  // });
  directionsDisplay.setMap(map);
  var onChangeHandler = function () {
  	calculateAndDisplayRoute(directionsService, directionsDisplay);
  };
  // document.getElementById('source').addEventListener('change', onChangeHandler);
  // document.getElementById('destination').addEventListener('change', onChangeHandler);
  document.getElementById('getRestaAddress').addEventListener('click', onChangeHandler);
}


// function initMap() {
//   var directionsService = new google.maps.DirectionsService;
//   var directionsDisplay = new google.maps.DirectionsRenderer;
//   var myLatLng = {lat: <?php //echo $restaurant_details->location->latitude; ?>, lng: <?php //echo $restaurant_details->location->longitude; ?>};
//   var map = new google.maps.Map(document.getElementById('map'), {
//     zoom: 13,
//     center: myLatLng
//   });
//   var marker = new google.maps.Marker({
//     position: myLatLng,
//     map: map,
//     title: '<?php //echo $restaurant_details->location->locality_verbose; ?>'
//   });
//   directionsDisplay.setMap(map);
//   var onChangeHandler = function () {
//     calculateAndDisplayRoute(directionsService, directionsDisplay);
//   };
//   document.getElementById('source').addEventListener('change', onChangeHandler);
//   document.getElementById('destination').addEventListener('change', onChangeHandler);
//   document.getElementById('getRestaAddress').addEventListener('click', onChangeHandler);
// }


if (navigator.geolocation) {
	navigator.geolocation.getCurrentPosition(function (p) {
		var LatLng = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);
		var mapOptions = {
			center: LatLng,
			zoom: 13,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
            // document.getElementById("current_lat").value = p.coords.latitude.toFixed(6);
            // document.getElementById("current_lng").value = p.coords.longitude.toFixed(6);
        });
} else {
	alert('Geo Location feature is not supported in this browser.');
}

function calculateAndDisplayRoute(directionsService, directionsDisplay) {
	directionsService.route({
		origin: document.getElementById('source').value,
		destination: document.getElementById('destination').value,
		travelMode: 'DRIVING'
	}, function (response, status) {
		console.log(response);
		//console.log(status);
		if (status === 'OK') {
			directionsDisplay.setDirections(response);
      $("#restaurantModal").modal('show');
		} else {
			window.alert('Directions request failed. Please enter valid source address.');			
      $('#restaurantModal').modal('hide');
      $('#source').val('');
      return false;
		}
	});
}



</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBe-UniUGzwJHL5kCOU4hsMj4z-ptyDuyw&v=3.exp&sensor=false&libraries=places&callback=initMap"></script>
<!-- <script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBe-UniUGzwJHL5kCOU4hsMj4z-ptyDuyw&v=3.exp&sensor=false&libraries=places&callback=initMap"></script> -->
	<?php $this->load->view('user/footer'); ?>