<div class="inner_dashboard">
	<div class="inner_img" style="background-image:url('<?php echo base_url(); ?>assets/img/inner_banner.png');"></div>
	<div class="inner_top_wrapp">
		<div class="inner_head">
			<?php
				$getdata = $this->uri->segment(1);	
				// echo strtoupper($getdata).' ';
				if($getdata == 'member-dashboard')
				{
					echo strtoupper('Schedule Ride').' ';
				}
				if($getdata == 'member-my-ride')
				{
					echo strtoupper('My Ride').' ';
				}
				if($getdata == 'member-profile' || $getdata == 'edit-member-profile')
				{
					echo strtoupper('My Profile').' ';
				}
				// if($getdata == 'member-notification')
				// {
				// 	echo strtoupper('notification').' ';
				// }						   	 				
			?>
		</div>
	</div>
</div>
<div class="inner_menu_section">
	<div class="container">
		<div class="row">
			<div class="dash_inn_menu">
				<ul>
					<li><a href="<?php echo base_url(); ?>member-profile" <?php if($getdata =="member-profile" || $getdata == "edit-member-profile"){ echo $class = 'class="act_inn_menu"'; } ?>>My Profile</a></li>										
					<li><a href="<?php echo base_url(); ?>member-dashboard" <?php if($getdata =="member-dashboard"){ echo $class = 'class="act_inn_menu"'; } ?>>Schedule Ride</a></li>
					
					<li><a href="<?php echo base_url(); ?>member-my-ride" <?php if($getdata =="member-my-ride"){ echo $class = 'class="act_inn_menu"'; } ?>>My Rides</a></li>

					<!-- <li><a href="<?php echo base_url(); ?>member-notification" <?php if($getdata =="member-notification"){ echo $class = 'class="act_inn_menu"'; } ?>>Notifications</a></li> -->					
				</ul>	
			</div>
		</div>
	</div>
</div>
