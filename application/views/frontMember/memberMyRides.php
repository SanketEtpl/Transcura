<?php $this->load->view("user/header"); ?>
<?php $this->load->view("frontMember/memberHeader"); ?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (PayPalMode == 'sandbox') {
	$action = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
} else {
	$action = 'https://www.paypal.com/cgi-bin/webscr';
}
?>
<script>
/*jQuery(document).ready(function(){ 	 

	var srstat = '<?php echo $srstatus; ?>';  
	if(srstat!=""){
		if(srstat==1){			
			jQuery("#sched_ride").addClass("active in");
			jQuery("#ridetab .sched_ride").addClass("active");
		}else if(srstat==2){
			jQuery("#comp_ride").addClass("active in");
			jQuery("#ridetab .comp_ride").addClass("active");
		}else if(srstat==3){
			jQuery("#cancel_ride").addClass("active in");
			jQuery("#ridetab .cancel_ride").addClass("active");
		}
	}else{ 
		jQuery("#sched_ride").addClass("active in");
		jQuery("#ridetab .sched_ride").addClass("active");
	}


	$("#send").click(function()
	{
		alert("Hi");       
	     $.ajax({
	         type: "POST",
	         url: base_url + "chat/post_action", 
	         data: {textbox: $("#textbox").val()},
	         dataType: "text",  
	         cache:false,
	         success: 
	              function(data){
	                alert(data);  //as a debugging message.
	              }
	          });// you have missed this bracket
	     return false;
	});

	
});*/
</script>
<div class="my_rides_wrapp">
	<div class="container">
		<div class="row">
			<div class="all_rides_box">
				<div class="rides_tab_new">	
					<div id="ridetab">						
						<?php $this->load->view('frontMember/inner_tabs'); ?>
					</div>
					<div id="success_message" style="color: green;"></div>
					<div id="error_message" style="color: red;"></div>
					<?php
					if ($this->session->flashdata('pay_msg') != '')
						echo "<div class='alert alert-success'><div>" . $this->session->flashdata('pay_msg') . "</div></div>";
					if ($this->session->flashdata('cancel_pay_msg') != '')
						echo "<div class='alert alert-warning'><div>" . $this->session->flashdata('cancel_pay_msg') . "</div></div>";
					?>
					<div class="tab-content all_content" id="all_content">
						<div id="sched_ride" class="tab-pane fade in active">
							<h3 class="rid_head">Scheduled Ride List</h3>
							<div class="rides_table">
								<div class="table-responsive">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>Name</th>
												<!-- <th>Trip ID</th> -->
												<th>Date / Time</th>
												<th>Driver Tracking</th>
												<th>Cancel Trip</th>
												<th>View Details</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											if(!empty($tripDetails)) {
												foreach ($tripDetails as $key => $value) {
													if($value['driver_status'] == "1")
													{
														$trip_status = "<strong><span style='color: orange;'>Request Sent</span></strong>";
														$action = "";
													}else if($value['driver_status'] == "2")
													{
														$action = '<button type="button" class="req_btn comp_btn" data-id="'.$value['id'].'" id="btn_confirm_driver" data-toggle="modal">Confirm</button>
														<button type="button" class="req_btn comp_btn" data-id="'.$value['id'].'" id="btn_reject_driver" data-toggle="modal" title="If you reject driver, your booking request is again posted except this driver.">Reject</button>';
														// $action = '<a href="javascript:void(0)" onclick="openPayModel(\''.$value['id'].'\')"><button>Confirm Driver !</button></a>';
														$trip_status = "<strong><span style='color: green;'>Accepted</span></strong>";
													}else if($value['driver_status'] == "3")
													{
														$trip_status = "<strong><span style='color: green;'>Trip Confirmed</span></strong>";
														$action = "";
													}													
													?>
													<tr>
														<td><span class="rid_pic" style="background-image:url('<?php if(!empty($value['picture'])){ echo base_url().$value['picture'];}else{ echo base_url()."assets/img/no_img.png"; } ?>');"></span><span class="rid_nme"><?php echo $value['full_name']; ?></span></td>
														<td id="schedule_get_view_id" style="display:none"><?php echo $value['id']; ?></td>
														<!-- <td><?php //echo $value['sr_trip_id']; ?></td> -->
														<td><?php echo date("d-M-Y", strtotime($value['sr_date'])).' '.$value['sr_pick_up_time']; ?></td>
														<td><button class="btn btn-track disabled" id="<?php echo 'schedule_ride_track_id_'.$value['id']; ?>" name="schedule_ride_track_id">Tracking</button></td>
														<td><button class="btn btn-cancel disabled" id="<?php echo 'schedule_ride_cancel_id_'.$value['id']; ?>" name="schedule_ride_cancel_id" data-toggle="modal" data-target="#canride_pop">Cancel</button></td>
														<td><button class="btn req_btn comp_btn" id="view_member_ride" onclick="openPayModel('<?php echo $value['id']; ?>')">View</button></td>
														<td><?php echo $trip_status;?></td>
														<td><?php echo $action ?></td>
														<!-- <td><a href="<?php echo base_url()?>return-schedule-a-ride" class="disabled"><button class="btn btn-return disabled" id="<?php //echo 'schedule_ride_view_id_'.$value['id']; ?>" name="return_schedule_ride_view_id" return false; >Return Ride</button></a></td> -->
													</tr>
													<?php } }else{ ?>
													<tr>
														<td colspan="6" style="text-align:center">You have no existing scheduled ride.</td>
													</tr>
													<?php } ?>									  
												</tbody>
											</table>
											<div class="pagination-dive" >
												<?php //echo $nav_scheduled_ride;?>
											</div>

										</div>
									</div>
								</div>
								<div id="comp_ride" class="tab-pane fade">
									<h3 class="rid_head">Completed Ride List</h3>
									<div class="rides_table">
										<div class="table-responsive">
											<table class="table table-striped">
												<thead>
													<tr>
														<th>Name</th>
														<th>Trip ID</th>
														<th>Date / Time</th>
														<th>View Details</th>
													</tr>
												</thead>
												<tbody>
													<?php 
													if(!empty($completedTripDetails)) {
														foreach ($completedTripDetails as $key => $value) {													
															?>
															<tr>
																<td><span class="rid_pic" style="background-image:url('<?php if(!empty($value['picture'])){ echo base_url().$value['picture'];}else{ echo base_url()."assets/img/no_img.png"; } ?>');"></span><span class="rid_nme"><?php echo $value['full_name']; ?></span></td>
																<td id="completed_ride_view_id" style="display:none"><?php echo $value['id']; ?></td>
																<td><?php echo $value['sr_trip_id']; ?></td>
																<td><?php echo date('d-M-Y',strtotime($value['sr_date'])).' '.$value['sr_pick_up_time']; ?></td>
																<td><button class="btn btn-view" id="<?php echo 'complete_ride_view_id_'.$value['id']; ?>" name="complete_ride_view_id" onclick="showStuff('<?php echo 'schedule_ride_view_id_'.$value['id']; ?>'); return false;">View</button></td>
															</tr>
															<?php } } else{ ?>
															<tr>
																<td colspan="4" style="text-align:center">You have no existing completed ride.</td>
															</tr>
															<?php } ?>									 
														</tbody>
													</table>							  						  
												</div>
											</div>
										</div>
										<div id="cancel_ride" class="tab-pane fade">
											<h3 class="rid_head">Cancelled Ride List</h3>
											<div class="rides_table">
												<div class="table-responsive">
													<table class="table table-bordered table-striped table-hover" id="cancelledTrip">
														<thead>
															<tr>
																<th>Name</th>
																<th>Trip ID</th>
																<th>Date / Time</th>
																<!-- <th>Status</th> -->
																<th>View Details</th>
															</tr>
														</thead>
														<tbody>
															<?php
										    //echo "<pre>"; print_r($cancelledTripDetails_paginated); 
															if(!empty($cancelledTripDetails)) {
																foreach ($cancelledTripDetails as $key => $value) {	
																	?>
																	<tr>
																		<td><span class="rid_pic" style="background-image:url('<?php if(!empty($value['picture'])){ echo base_url().$value['picture'];}else{ echo base_url()."assets/img/no_img.png"; } ?>');"></span><span class="rid_nme"><?php echo $value['full_name']; ?></span></td>
																		<td><?php echo $value['sr_trip_id']; ?></td>
																		<td id="cancelled_ride_view_id" style="display:none"><?php echo $value['id']; ?></td>
																		<td><?php echo date('d-M-Y',strtotime($value['sr_date'])).' '.$value['sr_pick_up_time']; ?></td>
																		<!-- <td><a href="#" class="btn btn-cancel">Cancel</a></td>	 -->										
																		<td><button class="btn btn-view" id="<?php echo 'cancelled_ride_view_id_'.$value['id']; ?>" name="cancelled_ride_view" onclick="showStuff('<?php echo 'schedule_ride_view_id_'.$value['id']; ?>); return false;" >View</button></td>
																	</tr>
																	<?php } } else{ ?>
																	<tr>
																		<td colspan="4" style="text-align:center">You have no existing cancelled ride.</td>
																	</tr>
																	<?php } ?>									 
																</tbody>
															</table>								  
														</div>
													</div>
												</div>
												<div id="custom" class="tab-pane fade">
													<h3 class="rid_head">Customer Care Information</h3>
													<p>Coming Soon</p>
												</div>
											</div>					
											<div id="sched_view">
												<div class="ride_trip_deta">
													<h3 class="heal_head">
														Ride Trip Details
														<div id="success_message" style="color: green;"></div>
														<div id="error_message" style="color: red;"></div>
														<span class="back_box"><button class="back_btn" id="schedule_bk" name="schedule_bk"   onclick="showStuff1()";>Back</button></span>
													</h3>
													<div class="sched_deta_box">								
														<div class="schdeta_sec">
															<div class="row">
																<div class="col-md-5	 rid_tim">
																	<label>Trip ID :</label>
																	<div class="rid_info" id="trip_id"></div>
																</div>
																<div class="col-md-5 rid_tim">
																	<label>Driver Status :</label>
																	<div class="rid_info" id="driver_status"></div>
																</div>
															</div>
														</div>
														<div class="schdeta_sec">
															<div class="row">
																<div class="col-md-5 rid_tim">
																	<label>Driver Name :</label>
																	<div class="rid_info" id="driver_name"> <span class="view_sec viewnew"><a href="#" class="com_btn" title="View"><i class="fa fa-eye"></i></a></span></div>
																</div>
																<div class="col-md-5 rid_tim">
																	<label>Distancce:</label>
																	<div class="rid_info" id="trip_duration"></div>
																</div>
										<!-- <div class="col-md-7">
											<span class="view_sec"><a href="#" class="com_btn">View Details</a></span>
										</div> -->
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Trip Date :</label>
											<div class="rid_info" id="trip_date"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Total distance in KM:</label>
											<div class="rid_info" id="total_distance"></div>
										</div>
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Pick Up Point :</label>
											<div class="rid_info" id="trip_pick_up_point"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Drop Point Healthcare Providers Location :</label>
											<div class="rid_info" id="trip_drop_point"></div>
										</div>
									</div>
								</div>		
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Driver Tracking :</label>
											<div class="rid_info">
												<label class="pick_lab_new">Name :</label>
												<div class="pick_info infofloat" id="driver_tracking"></div>
												<div class="view_pop viewnew"><button class="com_btn" id="ride_trip_details_view" name="ride_trip_details_view" title="View"><i class="fa fa-eye"></i></button></div>
											</div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Cost Estimate :</label>
											<div class="rid_info">--</div>
										</div>
									</div>
								</div>	
							</div>	
							<div class="pay_btn_box">								
								<button type="button" class="req_btn cancel_btn" id="">Cancel Ride</button>
								<button type="button" class="req_btn comp_btn" id="customer_complaints">Submit a Complaint</button>					
								<button type="button" class="req_btn" id="" data-toggle="modal" data-target="#returnride_pop">Request a Return Ride</button>
							</div>	
						</div>
					</div>
					<div id="complete_view">						
						<div class="ride_trip_deta">
							<h3 class="heal_head">
								Completed Ride Trip Details
								<span class="back_box"><button class="back_btn" id="complete_bk" name="complete_bk">Back</button></span>
							</h3>
							<div class="sched_deta_box">								
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip ID :</label>
											<div class="rid_info" id="complete_trip_id"></div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Name :</label>
											<div class="rid_info" id="complete_name"> <span class="view_sec viewnew"><a href="#" class="com_btn" title="View"><i class="fa fa-eye"></i></a></span></div>
										</div>
										<!--<div class="col-md-7">
											<span class="view_sec"><a href="#" class="com_btn">View Details</a></span>
										</div>-->
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Pick Up Point :</label>
											<div class="rid_info" id="complete_pick_up_point"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Drop Point :</label>
											<div class="rid_info">2780 Little Acres Lane Jacksonville, IL 62650</div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Ride Date :</label>
											<div class="rid_info" id="complete_trip_date"></div>
										</div>
									</div>
								</div>									
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Total Distance :</label>
											<div class="rid_info">10KM</div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Total Cost :</label>
											<div class="rid_info">$230</div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Rate Trip :</label>
											<div class="rid_info"> Star rating Coming Soon</div>
										</div>
									</div>
								</div>	
							</div>	
						</div>
					</div>
					<div id="cancel_ride_view">						
						<div class="ride_trip_deta">
							<h3 class="heal_head">
								Cancelled Ride Trip Details
								<span class="back_box"><button class="back_btn" id="cancel_bk" name="cancel_bk">Back</button></span>
							</h3>
							<div class="sched_deta_box">								
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Trip ID :</label>
											<div class="rid_info" id="cancelled_trip_id"></div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Name :</label>
											<div class="rid_info" id="cancel_name"></div>
										</div>
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Pick Up Point :</label>
											<div class="rid_info" id="cancelled_pick_up_point"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Drop Point :</label>
											<div class="rid_info">2780 Little Acres Lane Jacksonville, IL 62650</div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Ride Date and Time :</label>
											<div class="rid_info" id="cancelled_trip_date"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Ride Cancelled Date and Time :</label>
											<div class="rid_info">28-August-2017 Time: 05:50 PM</div>
										</div>
									</div>
								</div>									
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-12 rid_tim">
											<label>Type of Cancellation Marked by Driver : :</label>
											<div class="rid_info">---</div>
										</div>
									</div>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="schedule_ride_cancelled_value" value="">
<!--cancel ride popup-->
<div id="canride_pop" class="modal fade in" role="dialog">
	<div class="modal-dialog">    
		<div class="modal-content sma_pop">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>       
			</div>
			<div class="modal-body">
				<div class="succ_icon"><img src="http://192.168.100.12/AppTech/assets/img/info_icon.png"></div>
				<h2 class="read_trip">Are you sure want to</br>cancel this Ride?</h2>	
			</div>
			<div class="modal-footer ok_btn">

				<button type="button" id="schedule_ride_cancelled_yes" class="btn btn-default cont_btn" data-dismiss="modal">Yes</button>
				<button type="button" id="schedule_ride_cancelled_no" class="btn btn-default cont_btn" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>
<!--return ride popup-->
<div id="returnride_pop" class="modal fade in" role="dialog">
	<div class="modal-dialog">    
		<div class="modal-content sma_pop">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i></button>       
			</div>
			<div class="modal-body">
				<div class="succ_icon"><img src="http://192.168.100.12/AppTech/assets/img/info_icon.png"></div>
				<h2 class="read_trip">Are you Ready</br>for a Return Ride</h2>	
			</div>
			<div class="modal-footer ok_btn">
				<button type="button" id="" class="btn btn-default cont_btn" data-dismiss="modal">Yes</button>
				<button type="button" id="" class="btn btn-default cont_btn" data-dismiss="modal">No</button>
			</div>
		</div>
	</div>
</div>


<!-- PAY NOW MODEL -->
<!-- Modal -->
<div class="modal address-show fade" id="payModal" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<form method="POST" action="" id="pay_member_ride" name="pay_member_ride">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title"><span class="jsonName">Ride Details</span></h4>
				</div>
				<div class="modal-body">
					<!-- <h4 class="text-center addrs">Ride Details</h4> -->
					<div class="bluebg">
						<div class="form-group" >
							<label class="col-md-12" for="lblname">Schedule Date</label> 
							<span class="col-md-12" id="ride_date"></span>
						</div>
						<div class="form-group" >
							<label class="col-md-12" for="lblname">Trip ID</label> 
							<span class="col-md-12" id="tripid"></span>
						</div>
						<div class="form-group" >
							<label class="col-md-12" for="lblname">Source</label> 
							<span class="col-md-12" id="source_address"></span>               
						</div>  
						<div class="form-group" >
							<label for="lblname" class="col-md-12">Destination</label> 
							<span id="dest_address" class="col-md-12"></span>               
						</div> 
						<div class="form-group" >
							<label for="lblname" class="col-md-12">Driver</label> 
							<span id="driver_nm" class="col-md-12"></span>

						</div> 
						<div class="form-group" >
							<label for="lblname" class="col-md-6">Total Distance</label> 
							<span id="tot_dist" class="col-md-6"></span>
						</div>
						<div class="form-group" >
							<label for="lblname" class="col-md-6">Total Amount</label> 
							<span id="tot_amt" class="col-md-6"></span>							              
						</div> 
					</div>

       <!--  <div id="hidden_pay_data">
			<input type='hidden' name='cmd' value='_cart'>
			<input type='hidden' name='upload' value='1'>
			<input type='hidden' name='business' value='<?php //echo PayPalTestApiEmail ?>'>
			<input type='hidden' name='item_name_1' value='Book ride'>
			<input type='hidden' name='amount_1' id='ride_amt'>
			<input type='hidden' name='quantity_1' value='1'>
			<input type='hidden' name='rm' value='2'>
			<input type='hidden' name='currency_code' value='USD'>
			<input type='hidden' name='email' value='<?php //echo $this->session->userdata('user_email'); ?>'>
			<input type='hidden' name='charset' value='utf-8'>
			<input type='hidden' name='return' value='<?php //echo PayPalTestReturnURLEmployer ?>'>
			<input type='hidden' name='notify_url' value='<?php //echo PayPalTestCancelURLEmployer ?>'>
			<input type='hidden' name='cancel_return' value='<?php //echo PayPalTestCancelURLEmployer ?>'>
		</div> -->


	</div>

	<div class="modal-footer">
       <!--  <button type="button" class="req_btn comp_btn" id="btn_confirm_driver" data-toggle="modal">Confirm</button>
       <button type="button" class="req_btn comp_btn" id="btn_reject_driver" data-toggle="modal">Reject</button> -->
   </div>
</form>
</div>

</div>
</div>

<?php $this->load->view("user/footer"); ?>
<script type="text/javascript">
	function showStuff(id) {

		
		document.getElementById('ridetab').style.display = 'none';



	}

	function showStuff1() {

		document.getElementById('ridetab').style.display = 'block';


	}
</script>
<style type="text/css">
	.disabled {
		pointer-events: none;
		cursor: default;
	}
</style>