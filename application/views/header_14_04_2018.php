<?php
  $segmentMenu = $this->uri->segment(1); $segmentMenu2 = $this->uri->segment(2);
  $curDate = date('Y-m-d');
  $cond3 = array("tbl_users.user_type"=> 2,"tbl_schedule_ride.admin_status" => 1,"tbl_schedule_ride.sr_date >="=>$curDate);
  $jointype=array("tbl_users"=>"INNER");
  $join = array("tbl_users"=>"tbl_users.id = tbl_schedule_ride.user_id");
  $requestCount=$this->Common_model->selectQuery("count(*) as openRides",TB_SCHEDULE_RIDE,$cond3,array(),$join,$jointype);  
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Transcura | Admin Panel</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" href="<?php echo base_url(); ?>images/res-icon.png">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap/css/datepicker.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/plugin/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/toastr.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <link  rel="stylesheet" href="<?php echo base_url(); ?>css/admin_media.css">
  <!--<script src="<?php echo base_url(); ?>js/jquery-2.2.3.min.js"></script>-->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script type="text/javascript">
    var BASEURL = '<?php echo base_url(); ?>';
    $(function() {
      var today = new Date();
      var n = today.getFullYear();
      var y = n-70;
      //Datepicker
      $( "#example1" ).datepicker({ 
        //defaultDate: new Date("1988-02-25"),
        dateFormat: 'yy-mm-dd',
        changeMonth:true,
        changeYear:true,
        maxDate:0,                
        yearRange: y+":{ n }" 
      });
	  $( ".expireDate" ).datepicker({ 
      //defaultDate: new Date("1988-02-25"),
      changeMonth:true,
      changeYear:true,
      minDate:0,  
      maxDate:"+5y"                      
    }); 
	  $("#driverDate" ).datepicker({ 
      //defaultDate: new Date("1988-02-25"),
      changeMonth:true,
      changeYear:true,
      maxDate:"-18y",                
      //yearRange: y+":{ n }" 
    }); 
  });
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <header class="main-header">
      <!-- Logo -->
      <a href="#" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->          
        <img src="<?php echo base_url(); ?>images/imgpsh_fullsize.png" class="main">
        <img src="<?php echo base_url(); ?>images/res-icon.png" style="display: none" class="res">
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>App</b>-Tech</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <?php  $username = $this->session->auth_admin;?>
          <?php //echo $username['profile'] ;?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="user_img">
                 <?php if($username['role'] == 2)
                {?>
                <img src="<?php echo base_url();?>assets/img/2906996202655137.png" class="user-image" alt="User Image"/>
                <?php } 
                if($username['role'] == 1)
                {?>
                  <img src="<?php echo base_url().$username['profile'] ;?>" class="user-image" alt="User Image"/>
                <?php }
                ?>
              </span>
              <span class="hidden-xs"><?php echo $username['username'] ;?> </span>

            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <?php if($username['role'] == 2)
                {?>
                   <img src="<?php echo base_url();?>assets/img/2906996202655137.png"  class="img-circle" alt="User Image" />
                <?php } 
                if($username['role'] == 1)
                {?>
                <img src="<?php echo  base_url().$username['profile'] ;?>"  class="img-circle" alt="User Image" />
                <?php } ?>
                <p><?php echo $username['first_name']." ".$username['last_name'];  ?> 
              
                  <small> <?php echo "Member since ".date('M Y', strtotime($username['created_date']));  ?> </small>
                </p>
              </li>              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url(); ?>admin/Profile/" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url(); ?>admin/login/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->          
        </ul>
      </div>
    </nav>
  </header>
<aside class="main-sidebar"><?php //print_r($_SESSION['first_name'].$_SESSION['last_name']); ?>
  <section class="sidebar">
    <ul class="sidebar-menu">
      <li class="treeview<?php if(in_array($segmentMenu, array("admindashboard",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-tachometer" aria-hidden="true"></i> <span>Dashboard</span></a>
      </li>             
    </ul>


    <ul class="sidebar-menu member_user">
      <li class="treeview<?php if(in_array($segmentMenu, array("create-user",""))){echo " active";} ?>">
        <a href="#">
          <i class="fa fa-user"></i> <span>Create User</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-down pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="treeview <?php if(in_array($segmentMenu.'/'.$segmentMenu2, array("create-user",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>create-user"><i class="fa fa-user" aria-hidden="true"></i> <span>Create User</span></a></li> 
        <!--   <li class="treeview <?php //if(in_array($segmentMenu.'/'.$segmentMenu2, array("upload-profile-picture",""))){echo " active";} ?>"><a href="<?php //echo base_url(); ?>upload-profile-picture"><i class="fa fa-user" aria-hidden="true"></i> <span>Upload Profile Picture</span></a></li> -->
        </ul>
      </li>     
    </ul>


    <?php if($username['role'] == 1)
      {?>
    <ul class="sidebar-menu member_user">
      <li class="treeview<?php if(in_array($segmentMenu, array("member_category",""))){echo " active";} ?>">
        <a href="#">
          <i class="fa fa-user"></i> <span>Member</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-down pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="treeview <?php if(in_array($segmentMenu.'/'.$segmentMenu2, array("admin/Car_category",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>admin/Car_category"><i class="fa fa-database" aria-hidden="true"></i> <span>Car category</span></a></li> 
          <li class="treeview <?php if(in_array($segmentMenu.'/'.$segmentMenu2, array("admin/Member_service_type",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>admin/Member_service_type"><i class="fa fa-database" aria-hidden="true"></i> <span>Member service type</span></a></li>
          <li class="treeview <?php if(in_array($segmentMenu, array("member",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>member"><i class="fa fa-fw fa-users"></i> <span>Member register </span></a></li>
          <li class="treeview <?php if(in_array($segmentMenu, array("member-schedule-ride",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>member-schedule-ride"><i class="fa fa-fw fa-users"></i> <span>Member schedule ride details</span></a></li>
          <li class="treeview <?php if(in_array($segmentMenu, array("member-complete-ride",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>member-complete-ride"><i class="fa fa-fw fa-users"></i> <span>Member complete ride details</span></a></li>
        </ul>
      </li>     
    </ul>
    <?php }
      if($username['role'] == 1 || $username['role'] == 2)
      {
    ?>
    <ul class="sidebar-menu nemt_category">
      <li class="treeview<?php if(in_array($segmentMenu, array("nemt_category",""))){echo " active";} ?>">
        <a href="#">
          <i class="fa fa-user"></i> <span>NEMT</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-down pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
         <li class="treeview <?php if(in_array($segmentMenu, array("patients",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>patients"><i class="fa fa-fw fa-users"></i> <span>NEMT register </span></a></li>
         <li class="treeview <?php if(in_array($segmentMenu, array("nemt-schedule-ride",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>nemt-schedule-ride"><i class="fa fa-fw fa-users"></i> <span>NEMT schedule ride details</span></a></li>
         <li class="treeview <?php if(in_array($segmentMenu, array("nemt-complete-ride",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>nemt-complete-ride"><i class="fa fa-fw fa-users"></i> <span>NEMT complete ride details</span></a></li>
         <li class="treeview <?php if(in_array($segmentMenu, array("nemt-schedule-ride-complaints",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>nemt-schedule-ride-complaints"><i class="fa fa-fw fa-users"></i> <span>NEMT schedule ride complaint details</span></a></li>
           <!-- Ram K added menu -->
         <li class="treeview <?php if(in_array($segmentMenu, array("nemt-allocated-ride",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>nemt-allocated-ride"><i class="fa fa-fw fa-users"></i> <span>NEMT allocated ride</span></a></li>

         <li class="treeview <?php if(in_array($segmentMenu, array("nemt-allocated-ride",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>nemt-allocated-ride"><i class="fa fa-fw fa-users"></i> <span>Multiple Locations</span></a></li>
      </ul>
      </li>     
    </ul>
    <?php } 
  if($username['role'] == 1)
      {
  ?>
    <ul class="sidebar-menu doctors_category">
      <li class="treeview<?php if(in_array($segmentMenu, array("doctors_category",""))){echo " active";} ?>">
        <a href="#">
          <i class="fa fa-user-md"></i> <span>Doctors/Healthcare Providers </span>
          <span class="pull-right-container">
            <i class="fa fa-angle-down pull-right"></i>
          </span>
        </a>
         <ul class="treeview-menu">           
        <li class="treeview <?php if(in_array($segmentMenu, array("doctors_category",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>doctors_category"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Category</span></a></li>
       <li class="treeview <?php if(in_array($segmentMenu, array("doctors",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>doctors"><i class="fa fa-user-md" aria-hidden="true"></i><span>Doctors/Healthcare providers</span></a></li>
      </ul>
      </li>     
    </ul>
    <?php }
      if($username['role'] == 1 || $username['role'] == 2)
      {
    ?>

    <ul class="sidebar-menu member_user">
      <li class="treeview<?php if(in_array($segmentMenu, array("transportation-listing",""))){echo " active";} ?>">
        <a href="#">
          <i class="fa fa-user"></i> <span>Transportation Provider</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-down pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="treeview <?php if(in_array($segmentMenu.'/'.$segmentMenu2, array("transportation-listing",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>transportation-listing"><i class="fa fa-user" aria-hidden="true"></i> <span>Transportation Provider Listing</span></a></li>
        </ul>
      </li>     
    </ul>


    <ul class="sidebar-menu driver">
      <li class="treeview<?php if(in_array($segmentMenu, array("transportation",""))){echo " active";} ?>">
        <a href="#">
          <i class="fa fa-user" aria-hidden="true"></i><span>Driver </span>
          <span class="pull-right-container">
            <i class="fa fa-angle-down pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
       
        <li class="treeview <?php if(in_array($segmentMenu, array("service_types",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>service_types"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Service types</span></a></li>
       <li class="treeview <?php if(in_array($segmentMenu, array("drivers",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>drivers"><i class="fa fa-fw fa-ambulance"></i> <span>Driver Listing</span></a></li>
      </ul>
      </li>
    </ul>
    <?php } ?>
    <?php 
      if($username['role'] == 1 || $username['role'] == 2)
      {
    ?>
    <ul class="sidebar-menu schedule_management">        
      <li class="treeview<?php if(in_array($segmentMenu, array("schedule_management",""))){echo " active";} ?>">
        <a href="#">
          <i class="fa fa-list" aria-hidden="true"></i><span>Schedule management</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-down pull-right"></i>
          </span>
        </a>
       <ul class="treeview-menu">           
        <li class="treeview <?php if(in_array($segmentMenu.'/'.$segmentMenu2, array("admin/Destination",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>admin/Destination"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Destination</span></a></li>
        <li class="treeview <?php if(in_array($segmentMenu.'/'.$segmentMenu2, array("admin/Special_request",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>admin/Special_request"><i class="fa fa-fw fa-ambulance"></i> <span>Special request</span></a></li>
        <li class="treeview <?php if(in_array($segmentMenu.'/'.$segmentMenu2, array("admin/Desired_transportation",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>admin/Desired_transportation"><i class="fa fa-fw fa-ambulance"></i> <span>Desired transportation</span></a></li>
        <li class="treeview <?php if(in_array($segmentMenu, array("schedule",""))){echo " active";} ?>"> <a href="<?php echo base_url(); ?>schedule">  <i class="fa fa-commenting-o" aria-hidden="true"></i>
        <span>Schedule management</span>
        </a></li>
      </ul>
      </li>
    </ul>
    <?php }
      if($username['role'] == 1)
      {
  ?>
    <ul class="sidebar-menu home_content">        
      <li class="treeview<?php if(in_array($segmentMenu, array("home_content",""))){echo " active";} ?>">
        <a href="#">
          <i class="fa fa-list" aria-hidden="true"></i><span>Home content management</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-down pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">           
           <li class="treeview <?php if(in_array($segmentMenu, array("first_section",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>first_section"><i class="fa fa-sitemap" aria-hidden="true"></i><span>First section</span></a></li>
            <li class="treeview <?php if(in_array($segmentMenu, array("second_section",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>second_section"><i class="fa fa-fw fa-ambulance"></i> <span>Second section</span></a></li>
      </ul>
      </li>
    </ul>
    <?php } 
      if($username['role'] == 1)
      {
  ?>
    <ul class="sidebar-menu"><li class="treeview <?php if(in_array($segmentMenu.'/'.$segmentMenu2, array("admin/Admin_ride_status",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>admin/Admin_ride_status"><i class="fa fa-database" aria-hidden="true"></i>Ride request<?php if(empty($requestCount[0]['openRides'])) { }else { ?><div class="pull-right not_cnt"><?php echo $requestCount[0]['openRides']; ?></div><?php } ?></a></li></ul>  
    <ul class="sidebar-menu"><li class="treeview <?php if(in_array($segmentMenu, array("contents",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>contents"><i class="fa fa-database" aria-hidden="true"></i> <span>Content Management</span></a></li></ul>
    <ul class="sidebar-menu"><li class="treeview <?php if(in_array($segmentMenu, array("Setting",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>Setting">  <i class="fa fa-cog" aria-hidden="true"></i><span>Setting</span></a></li></ul>
    <!-- <ul class="sidebar-menu"><li class="treeview <?php if(in_array($segmentMenu, array("Payment",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>member-payment">  <i class="fa fa-cog" aria-hidden="true"></i><span>Payment</span></a></li></ul> -->
    <ul class="sidebar-menu"><li class="treeview <?php if(in_array($segmentMenu.'/'.$segmentMenu2, array("admin/Notification",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>admin/Notification"><i class="fa fa-database" aria-hidden="true"></i> <span>Notification</span></a></li></ul>  
    <ul class="sidebar-menu"><li class="treeview <?php if(in_array($segmentMenu, array("faq",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>faq">  <i class="fa fa-question-circle" aria-hidden="true"></i><span>FAQ Management</span></a></li></ul>
    <ul class="sidebar-menu"><li class="treeview <?php if(in_array($segmentMenu, array("testimonial",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>testimonial">  <i class="fa fa-commenting-o" aria-hidden="true"></i><span>Testimonial</span></a></li></ul>
    <ul class="sidebar-menu"><li class="treeview <?php if(in_array($segmentMenu, array("admin-contact-us",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>admin-contact-us"><i class="fa fa-database" aria-hidden="true"></i> <span>Contact us</span></a></li></ul> 
    <ul class="sidebar-menu"><li class="treeview <?php if(in_array($segmentMenu, array("admin-complaint-type",""))){echo " active";} ?>"><a href="<?php echo base_url(); ?>admin-complaint-type"><i class="fa fa-database" aria-hidden="true"></i> <span>Complaint type</span></a></li></ul> 
  <ul class="sidebar-menu"> <!-- Ram K -->
    <li class="treeview <?php if(in_array($segmentMenu, array("customer_care",""))){echo " active";} ?>">
      <a href="<?php echo base_url(); ?>customer-care">  <i class="fa fa-phone" aria-hidden="true"></i>
        <span>Customer Care</span>
      </a>
    </li>
  </ul>
    <ul class="sidebar-menu"> <!-- Kiran -->
    <li class="treeview <?php if(in_array($segmentMenu, array("ride-details",""))){echo " active";} ?>">
      <a href="<?php echo base_url(); ?>ride-details">  <i class="fa fa-phone" aria-hidden="true"></i>
        <span>Ride Details</span>
      </a>
    </li>
  </ul>
  <?php } ?>
  </section>
</aside>
<script type="text/javascript">
  $(document).ready(function(){
    var pathname = window.location.pathname; // Returns path only
    var baseURL = $(location).attr('hostname');
   //alert(BASEURL);
    // alert(baseURL+'/AppTech/admin/Car_category');
    var url      = window.location.href; 
    if(url == BASEURL+'admin/Car_category' || url == BASEURL+'admin/Member_service_type' || url == BASEURL+'member' || url == BASEURL+'member-schedule-ride' || url == BASEURL+'member-complete-ride')
    {
      $('.member_user li:first').addClass('active');
    }

    if(url == BASEURL+'patients' || url == BASEURL+'nemt-schedule-ride' || url == BASEURL+'nemt-complete-ride' || url == BASEURL+'nemt-schedule-ride-complaints')
    {
      $('.nemt_category li:first').addClass('active');
    }

    if(url == BASEURL+'doctors_category' || url == BASEURL+'doctors')
    {
      $('.doctors_category li:first').addClass('active');
    }

    if(url == BASEURL+'service_types' || url == BASEURL+'drivers')
    {
      $('.transportation li:first').addClass('active');
    }

    if(url == BASEURL+'admin/Destination' || url == BASEURL+'admin/Special_request' || url == BASEURL+'admin/Desired_transportation' || url == BASEURL+'schedule')
    {
      $('.schedule_management li:first').addClass('active');
    }

    if(url == BASEURL+'first_section' || url == BASEURL+'second_section')
    {
      $('.home_content li:first').addClass('active');
    }
  });    
</script>

