<div class="inner_dashboard">
	<div class="inner_img" style="background-image:url('<?php echo base_url(); ?>assets/img/inner_banner.png');"></div>
	<div class="inner_top_wrapp">
		<div class="inner_head">
			<?php
				$getdata = $this->uri->segment(1);	
				if($getdata == 'transport-dashboard')
				{
					echo strtoupper('dashboard');
				}else if($getdata == 'transport-profile')
				{
					echo strtoupper('my profile');
				}else if($getdata == 'registered-driver-listing')
				{
					echo strtoupper('Registered Driver');
				}else if($getdata == 'add-driver')
				{
					echo strtoupper('Add Driver');
				}else if($getdata == 'transport-trips')
				{
					echo strtoupper('My Trips');
				}else if($getdata == 'get-driver')
				{
					echo strtoupper('Assign to Driver');
				}
														   	 				
			?>
		</div>
	</div>
</div>
<div class="inner_menu_section">
	<div class="container">
		<div class="row">
			<div class="dash_inn_menu">
				<ul>										
					<li><a href="<?php echo base_url(); ?>transport-dashboard" <?php if($getdata =="transport-dashboard"){ echo $class = 'class="act_inn_menu"'; } ?>>Dashboard</a></li>
					
					<li><a href="<?php echo base_url(); ?>transport-profile" <?php if($getdata =="transport-profile"){ echo $class = 'class="act_inn_menu"'; } ?>>My Profile</a></li>

					<li><a href="<?php echo base_url(); ?>registered-driver-listing" <?php if($getdata =="registered-driver-listing"){ echo $class = 'class="act_inn_menu"'; } ?>>Registered Driver</a></li>

					<li><a href="<?php echo base_url(); ?>transport-trips" <?php if($getdata =="transport-trips"){ echo $class = 'class="act_inn_menu"'; } ?>>My Trips</a></li>					
				</ul>	
			</div>
		</div>
	</div>
</div>
