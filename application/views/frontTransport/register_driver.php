<?php $this->load->view("user/header"); ?>
<?php $this->load->view('frontTransport/transportHeader'); ?>

<div class="myprof_wrapp">
	<div class="probgimg"></div>
	<div class="mypro_info_wrapp edit_pro_wrapp">
		<div class="container">
			<div class="row">
				<div class="basic_info_wrapp">
					<div class="row">
						<?php
						$success = $this->session->flashdata("success");
						$fail = $this->session->flashdata('error');
						if($success) { ?>
						<div class="alert alert-success col-md-offset-1 col-md-8">
							<?php echo $success; ?>
						</div>
						<?php
					}
					if($fail){ ?>
					<div class="alert alert-danger col-md-offset-1 col-md-8">
						<?php echo $fail; ?>
					</div>
					<?php } ?>
					<div class="message"></div>
					<div class="col-md-6">
						<div class="info_head">Basic Information :</div>
						<div class="basic_box">
							<div class="row">
								<input type="hidden" name="usertype" id="usertype" value="5" readonly="">
								<div class="col-md-6 edit_box">
									<label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/emer_no.png">Full Name:</label>	
									<div class="edit_inpt"><input type="text" placeholder="Full name" name="fullname" id="fullname" value="<?php echo set_value('fullname'); ?>" class="form-control" maxlength="100" required="required"></div>
									<?php echo form_error('fullname', '<div class="error">', '</div>'); ?>
								</div>

								<div class="col-md-6 edit_box">
									<label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/emer_no.png">User Name:</label>	
									<div class="edit_inpt"><input type="text" placeholder="User name" name="username1" id="username" value="<?php echo set_value('username'); ?>" class="form-control" maxlength="100" onBlur="checkusername()" required="required"></div>
									<?php echo form_error('username1', '<div class="error">', '</div>'); ?>
								</div>

								<div class="col-md-6 edit_box">
									<label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/email_add.png">Email Address:</label>	
									<div class="edit_inpt"><input type="text" placeholder="Email ID" name="email" id="emailaddrss" maxlength="50" value="<?php echo set_value('email'); ?>" class="form-control" onBlur="checkemail()" required="required"></div>
									<?php echo form_error('email', '<div class="error">', '</div>'); ?>
								</div>

								<div class="col-md-6 edit_box">
									<label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/mob_no.png">Mobile Number:</label>	
									<div class="edit_inpt"><input type="text" placeholder="Mobile number" name="phone_no" id="phnnumber" maxlength="20" value="<?php echo set_value('phone_no'); ?>" class="form-control" required="required"></div>
									<?php echo form_error('phone_no', '<div class="error">', '</div>'); ?>
								</div>

								<div class="col-md-6 edit_box">
									<label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/mob_no.png">Password:</label>	
									<div class="edit_inpt"><input type="password" placeholder="Password" name="password" id="password" value="" class="form-control" required="required"></div>
									<?php echo form_error('password', '<div class="error">', '</div>'); ?>
								</div>

								<div class=" col-md-12">
									<input class="req_btn comp_btn" type="button" name="save_driver" id="btn_save_driver" value="Submit">
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-1"><strong>OR</strong></div>
					<div class="col-md-5">
						<div class="info_head">Upload Driver details :</div>
						<div class="basic_box">
							<div class="row">
								<form name="addDriverForm" id="addDriverForm" action="<?php echo base_url(); ?>upload-driver" method="POST" autocomplete="off" enctype="multipart/form-data">
									<div class="col-md-12 edit_box">
										<label class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png">Choose File :</label> &nbsp; (CSV, Xls, Xlsx File)	
										<div class="edit_inpt"><input type="file" name="user_file" id="user_file" value="" class="form-control" required="required"></div>
										<?php echo form_error('user_file', '<div class="error">', '</div>'); ?>
									</div>

									<input class="req_btn comp_btn" type="submit" name="upload_driver" id="upload_driver" value="Submit">
								</form>
							</div>

						</div>
					</div>						
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<?php echo base_url() ?>/assets/js/jquery.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#btn_save_driver").click(function(){
				var fullname = $("#fullname").val();
				var emailaddrss = $("#emailaddrss").val();
				var phnnumber = $("#phnnumber").val();
				var username = $('#username').val();
				var pass = $("#password").val();

				var usrFlag =false;
				var fullnmFlag =false;
				var emailFlag =false;	
				var phoneFlag =false;
				var passFlag =false;	

				var minlength = 6;
				var emailPattern = /^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,50}$/i;
				if(pass == ''){
					$(".password").remove();	
					$("#password").parent().append("<div class='password' style='color:red;'>Password is required.</div>");
					passFlag =false;
				}else{	
					if(pass.length < minlength)
					{
						$(".password").remove();	
						$("#password").parent().append("<div class='password' style='color:red;'>Password must contain minimum 6 characters .</div>");
						passFlag =false;
					}
					else
					{	
						passFlag =true;
						$(".password").remove();				
					}
				}

				if(phnnumber == ''){
					$(".phnnumber").remove();
					phoneFlag =false;	
					$("#phnnumber").parent().append("<div class='phnnumber' style='color:red;'>Mobile no is required.</div>");
				}else{
					if(phnnumber.length < 8)
					{
						$(".phnnumber").remove();	
						$("#phnnumber").parent().append("<div class='phnnumber' style='color:red;'>Mobile no must contain minimum 8 digits .</div>");
						phoneFlag =false;
					}else{
						phoneFlag =true;
						$(".phnnumber").remove();	
					}				
				}


				if(username == ''){
					$(".username").remove();
					$(".username1").remove();
					usrFlag =false;	

					$("#username").parent().append("<div class='username' style='color:red;'>User name is required.</div>");
				}else{
					$(".username").remove();	
					usrFlag =true;
				}

				if(fullname == ''){
					$(".fullname").remove();
					fullnmFlag =false;	
					$("#fullname").parent().append("<div class='fullname' style='color:red;'>Full name is required.</div>");
				}else{
					if(fullname.length < 3)
					{
						$(".fullname").remove();	
						$("#fullname").parent().append("<div class='fullname' style='color:red;'>Full name must contain minimum 3 characters .</div>");
						fullnmFlag =false;
					}
					else
					{	
						$(".fullname").remove();
						fullnmFlag =true;
					}					
				}

				if(emailaddrss == ''){			
					emailFlag = false;			
					$(".emailaddrss1").remove();	
					$("#emailaddrss").parent().append("<div class='emailaddrss1' style='color:red;'>Email is required.</div>");
				}else{
					if( !emailPattern.test(emailaddrss)){
						emailFlag = false;				
						$(".emailaddrss1").remove();	
						$("#emailaddrss").parent().append("<div class='emailaddrss1' style='color:red;'>Please enter a valid email address.</div>");
					}else{				
						emailFlag = true;
						$(".emailaddrss1").remove();	
					}
				}

				var checkmailflg = checkemail();
				var checkUsernameflag = checkusername();
				if(fullnmFlag && usrFlag && emailFlag && phoneFlag && passFlag && checkmailflg && checkUsernameflag)
				{
					var formData = new FormData($('#addDriverForm')[0]);
					$.ajax({
						type:"POST",
						url: baseURL+"save-driver",
						dataType :"json",
						data:formData,
						processData: false,
						contentType: false,
						success:function(response){ 					 					
							if(response.status === "success")
							{
								$('.message').html('<div class="alert alert-success">'+response.message+'</div>');
								setTimeout(function(){
									window.location.href = base_url+"add-driver";	

								},2000);  	    									
							}
							else
							{
								$('.message').html('<div class="alert alert-danger">'+response.message+'</div>');
								setTimeout(function(){
									window.location.href = base_url+"add-driver";	

								},2000);      					
							}
						}
					});	
				}

			});
		});

	</script>
	<?php $this->load->view("user/footer"); ?>