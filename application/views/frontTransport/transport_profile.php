<?php $this->load->view("user/header"); ?>
<?php $this->load->view('frontTransport/transportHeader'); ?>
<div class="myprof_wrapp">
	<div class="probgimg" style="background-image:url('<?php echo base_url(); ?>assets/img/profile_bg.png');"></div>
	<div class="mypro_info_wrapp">
		<div class="container">
			<div class="row">
				<div class="tp_pro_sect">
					<div class="pro_img_box" style="background-image:url('<?php if(!empty($transport_profile[0])){ if(!empty($transport_profile[0]['picture'])){ echo base_url().$transport_profile[0]['picture']; } else{ echo base_url()."assets/img/no_img.png"; } } ?>');">
					</div>
					<div class="pro_img_name">
						<div class="pro_name_box">
							<div class="pro_nme"><?php  if(!empty($transport_profile[0])){echo $transport_profile[0]['full_name'];} ?></div>
							 <div class="edit_prof_btn"><a href="<?php echo base_url();?>edit-transport-profile">Edit Profile</a></div> 
						</div>
					</div>
					<?php
						$success = $this->session->flashdata("success");
						$fail = $this->session->flashdata('fail');
						if($success) { ?>
							<div class="alert alert-success col-md-offset-1 col-md-8">
								<?php echo $success; ?>
							</div>
						  <?php
						  }
						if($fail){ ?>
							<div class="alert alert-danger col-md-offset-1 col-md-8">
								<?php echo $fail; ?>
							</div>
					<?php } ?>
				</div>
				<div class="basic_info_wrapp">
					<div class="row">
						<div class="col-md-6">
							<div class="info_head">Basic Information :</div>
							<div class="basic_box">
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/date_icon.png"><span>Date of Birth:</span></div>
									<div class="bas_rt"><?php  if(!empty($transport_profile[0])){echo $transport_profile[0]['date_of_birth'];} ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/mob_no.png"><span>Mobile Number:</span></div>
									<div class="bas_rt"><?php  if(!empty($transport_profile[0])){echo $transport_profile[0]['phone'];} ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/email_add.png"><span>Email Address:</span></div>
									<div class="bas_rt"><?php  if(!empty($transport_profile[0])){echo $transport_profile[0]['email'];} ?></div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="info_head">Address :</div>
							<div class="basic_box add_box">
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/coun.png"><span>Country:</span></div>
									<div class="bas_rt"><?php if(!empty($transport_profile[0])){echo $transport_profile[0]['country_name'];} ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/state.png"><span>State:</span></div>
									<div class="bas_rt"><?php if(!empty($transport_profile[0])){echo $transport_profile[0]['state_name'];} ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/state.png"><span>City:</span></div>
									<div class="bas_rt"><?php if(!empty($transport_profile[0])){echo $transport_profile[0]['city_name'];} ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/state.png"><span>Street:</span></div>
									<div class="bas_rt"><?php if(!empty($transport_profile[0])){echo $transport_profile[0]['street'];} ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/zip.png"><span>Zip Code:</span></div>
									<div class="bas_rt"><?php if(!empty($transport_profile[0])){echo $transport_profile[0]['zipcode'];} ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/zip.png"><span>County:</span></div>
									<div class="bas_rt"><?php if(!empty($transport_profile[0])){echo $transport_profile[0]['county'];} ?></div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="info_head">Personal Information :</div>
							<div class="basic_box personal_box">
								<!-- <div class="basic_inner">
									<div class="bas_lt"><img src="<?php //echo base_url(); ?>assets/img/medical.png"><span>Medical Insurance ID:</span></div>
									<div class="bas_rt"><?php //if(!empty($health_profile[0])){echo $health_profile[0]["insurance_id"];} ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php //echo base_url(); ?>assets/img/per_doc.png"><span>Personal Documents:</span></div>
									<div class="bas_rt per_doc"><a href="<?php //if(!empty($health_profile[0])){echo $health_profile[0]['personal_doc'];} ?>" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i>View</a></div>
								</div> -->
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/emer_no.png"><span>Emergency Contact Name:</span></div>
									<div class="bas_rt"><?php if(!empty($transport_profile[0])){echo $transport_profile[0]['emergency_contactname'];} ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/mob_no.png"><span>Emergency Contact Number:</span></div>
									<div class="bas_rt"><?php if(!empty($transport_profile[0])){echo $transport_profile[0]['emergency_contactno'];} ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/sign.png"><span>Are you able to Provide Signatures:</span></div>
									<div class="bas_rt"><?php if(!empty($transport_profile[0])){ if($transport_profile[0]['signature'] == 1){ echo "YES"; }else{ echo "NO"; }} ?></div>
								</div>
							</div>
						</div>

						<!-- <div class="col-md-6">
							<div class="info_head">Insurance Provider :</div>
							<div class="basic_box">
								<div class="ins_head">Search for Insurance Provider</div>
								<div class="ins_search">
									<input type="text" name="" id=""  class="form-control"/>
								</div>
							</div>
						</div> -->
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
<?php $this->load->view("user/footer"); ?>