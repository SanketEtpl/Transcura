<?php $this->load->view("user/header"); ?>
<?php $this->load->view('frontTransport/transportHeader'); ?>
<style type="text/css">
	.btn-danger[disabled]
	{
		background-color: #d9534f !important;	
	}
	.btn-success[disabled]
	{
		background-color: #5cb85c !important;	
	}
	.form-group i
	{
		/*position: none !important*/
	}
</style>
<div class="my_rides_wrapp">
	<div class="container">
		<div class="row">
			<div class="all_rides_box">
				<div class="rides_tab_new">	
					<div id="ridetab">						
						<a href="<?php echo base_url(); ?>add-driver"><button type="button" class="req_btn comp_btn">Add Driver</button></a>
					</div>
					<div class="tab-content all_content" id="all_content">
						<div id="sched_ride" class="tab-pane fade in active">
							<h3 class="rid_head">Driver List</h3>
							<div class="successmsg" style="color: green"></div>
							<div class="errormsg" style="color: red"></div>
							<div class="rides_table">
								<div class="table-responsive">
									<table class="table table-striped">
										<thead>
											<tr>
												<th>Full Name</th>
												<th>Username</th>
												<th>Email ID</th>
												<th>Phone Number</th>
												<th>Password</th>
												<th>Status</th>
												<th><center>Action</center></th>
												<!-- <th>Action</th> -->
												<th>Availability</th>
												<th>Approval</th>
											</tr>
										</thead>
										<tbody>
											<?php 
											if(!(empty($driverlist)))
											{
												foreach ($driverlist as $dvalue) 
												{ 
													$driverId = $this->encrypt->encode($user['id']);
													$check_available = getDriverAvailability($dvalue['id']);
													if($check_available == "availabale")
													{
														$status_active = "btn btn-xs btn-success disabled";
														$status_inactive = "btn btn-xs  active btn-default";
													}else if($check_available == "unavailabale")
													{
														$status_active = "btn btn-xs  active btn-default";
														$status_inactive = "btn btn-xs btn-danger disabled";
													}

													if($dvalue['is_approve'] == "1")
													{
														$status_active_app = "btn btn-xs btn-success disabled";
														$status_inactive_app = "btn btn-xs  active btn-default";
													} else if($dvalue['is_approve'] == "0")
													{
														$status_active_app = "btn btn-xs  active btn-default";
														$status_inactive_app = "btn btn-xs btn-danger disabled";
													}
													?>
													<tr>
														<td>
															<span class="lblfields<?php echo $dvalue['id']; ?>"><?php echo $dvalue['full_name']; ?></span>
															<input type="text" name="full_name" id="full_name<?php echo $dvalue['id']; ?>" class="form-control editfields<?php echo $dvalue['id']; ?>" value="<?php echo $dvalue['full_name']; ?>" style="display:none;">
														</td>
														<td><?php echo $dvalue['username']; ?></td>
														<td><?php echo $dvalue['email']; ?></td>
														<td><span class="lblfields<?php echo $dvalue['id']; ?>"><?php echo $dvalue['phone']; ?></span>
															<span style="display:none;" class="editfields<?php echo $dvalue['id']; ?>">
																<input type="text" name="phone_no" id="phone_no<?php echo $dvalue['id']; ?>" class="form-control phnnumber" value="<?php echo $dvalue['phone']; ?>"></span>
															</td>
															<td><?php echo $dvalue['actual_password']; ?></td>
															<td><span class="lblfields<?php echo $dvalue['id']; ?>"><?php echo $dvalue['status']; ?></span>
																<select name="dr_status" id="dr_status<?php echo $dvalue['id']; ?>" class="form-control editfields<?php echo $dvalue['id']; ?>" style="display:none;">
																	<option value="active"<?php echo ($dvalue['status'] == 'active')?'selected':'' ?>>Active</option>
																	<option value="inactive" <?php echo ($dvalue['status'] == 'inactive')?'selected':'' ?>>Inactive</option>
																</select>
															</td>
															<td>
																<a href="javascript:void(0)" class="btn btn-primary lblfields<?php echo $dvalue['id']; ?>" onclick="showEditForm(<?php echo $dvalue['id']; ?>)" name="edit_driver" title="Edit"><i class="fa fa-edit"></i></a>
																<a href="javascript:void(0)" class="btn btn-primary editfields<?php echo $dvalue['id']; ?>" onclick="UpdateDriverForm(<?php echo $dvalue['id']; ?>)" style="display:none;" name="update_driver" title="Update"><i class="fa fa-check-square-o"></i></a>
																<a href="javascript:void(0)" class="btn btn-primary" onclick="getDriverDoc(<?php echo $dvalue['id']; ?>)"  name="view_driver" title="View Documents"><i class="fa fa-eye"></i></a>
																<a href="javascript:void(0)" class="btn btn-primary" onclick="deleteTPDriver(<?php echo $dvalue['id']; ?>)" name="dalete_driver" title="Delete"><i class="fa fa-remove"></i></a>
															</td>

															<td>
																<div class="btn-group btn-toggle">
																	<button onclick="ChangeAvailability('1','<?php echo $this->encrypt->encode($dvalue['id']); ?>')" class="<?php echo $status_active; ?>" title="Make driver availabale">On</button>
																	<button onclick="ChangeAvailability('0','<?php echo $this->encrypt->encode($dvalue['id']); ?>')" class="<?php echo $status_inactive; ?>" title="Make driver unavailabale">Off</button>
																</div>

															</td>
															<td>
																<div class="btn-group btn-toggle">
																	<button onclick="ChangeApproval('1','<?php echo $this->encrypt->encode($dvalue['id']); ?>')" class="<?php echo $status_active_app; ?>" title="Approved"><i class="fa fa fa-check"></i></button>
																	<button onclick="ChangeApproval('0','<?php echo $this->encrypt->encode($dvalue['id']); ?>')" class="<?php echo $status_inactive_app; ?>" title="Not Approved"><i class="fa fa-ban"></i></button>
																</div>
															</td>
														</tr>
														<?php }

													}else
													{ ?>
													<tr><td colspan="9">No driver found.</td></tr>
													<?php }
													?>
												</tbody>
											</table>

											<div class="pagination-dive"></div>

										</div>
									</div>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- driver document pop up -->
		<div id="driverDocModal" class="modal fade" style="display: none;" aria-hidden="false">   
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
						<h4 class="modal-title">Driver Documents</h4>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="driver_doc"></div>
						</div>
					</div>        
				</div>
			</div>
		</div>
		
		<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
			$('.disabled').prop('disabled', true); //TO DISABLED 
		});

			function showEditForm(drid)
			{
				$(".editfields"+drid).show();
				$(".lblfields"+drid).hide();
			}

			// ------ Update driver info -----------
			function UpdateDriverForm(drid)
			{
				var fullname = $("#full_name"+drid).val();
				var phoneno = $("#phone_no"+drid).val();
				var drstatus = $("#dr_status"+drid).val();
				if(fullname == "")
				{
					$(".fname"+drid).remove();
					$("#full_name"+drid).parent().append("<div class='fname"+drid+"' style='color:red;'>Name is required.</div>");
				}else if(phoneno == "")
				{
					$(".phno"+drid).remove();
					$("#phone_no"+drid).parent().append("<div class='phno"+drid+"' style='color:red;'>Phone no is required.</div>");
				}else
				{
					$(".fname"+drid).remove();
					$(".phno"+drid).remove();

					$.ajax({
						type:"POST",
						url:baseURL+"update-tpdriver",
						data:{'fullname':fullname,'phoneno':phoneno,'drstatus':drstatus,'drid':drid},
						dataType:"json",
						async:false,					
						success:function(response){
							if(response.status == "success")
							{
								$(".successmsg").html(response.message);
								setTimeout(function(){ $(".successmsg").html(""); }, 2000);										
								$(".editfields"+drid).hide();
								$(".lblfields"+drid).show();
								$(".rides_table").load(window.location.href + " .table-responsive");
							}
							else
							{
								$(".errormsg").html(response.message);				
							}			
						}
					});	
				}
			}



			//--------- Change driver availabality -----------
			function ChangeAvailability(status,drid)
			{
				if(confirm('Are you sure you want to change availabality ?'))
				{
					$.ajax({
						type:"POST",
						url:baseURL+"change-dr-avail",
						data:{'status':status,'drid':drid,'user_type':'4'},
						dataType:"json",
						async:false,					
						success:function(response){
							if(response.status == "success")
							{
								$(".successmsg").html(response.message);
								setTimeout(function(){ $(".successmsg").html(""); }, 2000);	
								$(".rides_table").load(window.location.href + " .table-responsive");
							}
							else
							{
								$(".errormsg").html(response.message);				
							}			
						}
					});
				}	
			}

			// -------- change approval--------------
			function ChangeApproval(status,drid)
			{
				if(confirm('Are you sure you want to change appoval status ?'))
				{
					$.ajax({
						type:"POST",
						url:baseURL+"change-dr-approval",
						data:{'status':status,'drid':drid},
						dataType:"json",
						async:false,					
						success:function(response){
							if(response.status == "success")
							{
								$(".successmsg").html(response.message);
								setTimeout(function(){ $(".successmsg").html(""); }, 2000);	
								$(".rides_table").load(window.location.href + " .table-responsive");
							}
							else
							{
								$(".errormsg").html(response.message);				
							}			
						}
					});
				}	
			}


			// delete Driver by Transpotation Provider
			function deleteTPDriver(id)
			{
				var checkConfirm = confirm("Do you want to continue ?");

				if( true == checkConfirm )
				{

					$.ajax({
						type:"POST",
						url:baseURL+"delete-tp-driver",
						data:{'drid':id},
						dataType:"json",
						async:false,					
						success:function(response)
						{
							if(response.status == "assigned")
							{

								alert(response.message);

							}
							if(response.status == "success")
							{
								alert(response.message);	

								
							}	
							else
							{
								alert(response.message);
							}		
						}
					});

				}

			}

			// ----- show driver documents ---------
			function getDriverDoc(dr_id)
			{
				if(dr_id)
				{
					$.ajax({
						type: "POST",
						dataType: "json",
						url: baseURL+"get-driver-doc",
						data: {"driver_id":dr_id},
					}).success(function (json) {
						if(json.status == "success")
						{ 
							$(".driver_doc").html(json.response);        
							jQuery('#driverDocModal').modal('show', {backdrop: 'static'});
						}
						else
						{
							toastr.error(json.msg,"Error:");
						}
					});    
				}else
				{
					jQuery('#driverDocModal').modal('show', {backdrop: 'static'});
				}
			}
		</script>
		<?php $this->load->view("user/footer"); ?>