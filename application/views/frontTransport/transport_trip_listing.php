<?php $this->load->view("user/header"); ?>
<?php $this->load->view('frontTransport/transportHeader'); ?>
<style>
div.dropOffAddress {
    float: left;
    width: 100%;
}
#RideDetailsModal .modal-body .form-group{
    float: left;
    width: 100%;
        border-bottom: 1px solid #ccc;
    padding-bottom: 15px
}
#RideDetailsModal .modal-body .form-group:last-child{
  border-bottom: none;
}
</style>
<div class="my_rides_wrapp">
	<div class="container">
		<div class="row">
			<div class="all_rides_box">
				<div class="rides_tab_new">	
					<div id="ridetab">						
						<div class="rides_tab">
							<ul class="nav nav-tabs">
								  <li class="active"><a data-toggle="tab" href="#ride_request_trip">RIDE REQUEST TRIPS</a></li>
								  <li ><a data-toggle="tab" href="#accepted_trip">MY ACCEPTED TRIPS</a></li>
								  <li ><a data-toggle="tab" href="#rejcted_trip">MY REJECTED TRIPS</a></li>
							</ul>
						</div>					
					</div>
					<div id="success_message" class="err_mes" style="color: green;"></div>
					<div id="error_message" class="err_mes" style="color: red;"></div>
					<div class="tab-content all_content" id="all_content">
						<!-- <div id="success_message"></div> -->
						<div id="ride_request_trip" class="tab-pane fade in active">
							<!-- <h3 class="rid_head">Ride request</h3> -->
							<div class="rides_table">
								<div class="table-responsive">
									<table class="table table-striped">
										<thead>
										  <tr>
											<th>Name</th>
											<th>Trip ID</th>
											<th>Pick up point</th>
											<th>Drop point</th>
											<th>Distance</th>
											<th>Date / Time</th>
											<th>Ride Details</th>
											<th width="240">Action</th>
										  </tr>
										</thead>
										<tbody>										
											<?php 
											foreach ($ride_request as $key => $req) {
											$schedId = $this->encrypt->encode($req['sched_id']);
											$sched_id = strtr($schedId, '+/', '-_');
											 ?>
											<tr>
												<td><span class="rid_nme"><?php echo $req['full_name'];?></span></td>
												<td ><?php echo $req['sr_trip_id'];?></td>
												<td ><?php echo $req['source_address'];?></td>
												<td ><?php echo $req['destination_address'];?></td>
												<td ><?php echo $req['sr_total_distance']." miles";?></td>
												<td ><?php echo date("d-M-Y",strtotime($req['sr_date']))." ".$req['sr_pick_up_time'];?></td>
												<td><span><a href="javascript:void(0);" onclick="getTripDetails(<?php echo $req['sched_id']; ?>)">view</a></span></td>
												<td >
													<a href="<?php echo base_url() ?>get-driver?sched_id=<?php echo $sched_id; ?>"><button class="btn">Assign to Driver</button></a>
													<button class="btn" onclick="rejectTrip(<?php echo $req['sched_id']; ?>)">Reject</button>													
												</td>
											</tr>
											<?php } ?>																			
										</tbody>
									</table>
								</div>
							</div>
					    </div>
					    <div id="accepted_trip" class="tab-pane fade">
						<h3 class="rid_head">My Accepted Trips</h3>
						<div class="rides_table">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th>Customer</th>
										<th>Driver</th>
										<th>Trip ID</th>
										<th>Date / Time</th>
										<th>View Details</th>
										<th>Status</th>
									  </tr>
									</thead>
									<tbody>
										<?php foreach ($accepted_ride as $key => $value) {
											if($value['driver_status'] == "2")
											{
												$dr_status = "<strong><span style='color: orange;'>Accepted</span></strong>";
											}else if($value['driver_status'] == "3")
											{
												$dr_status = "<strong><span style='color: green;'>Confirmed</span></strong>";
											}
										 ?>										
										<tr>
											<td><?php echo $value['customer_name']; ?></td>
											<td><?php echo $value['driver_name']; ?></td>
											<td id="accepted_ride_view_id" style="display:none"><?php echo $value['sched_id']; ?></td>
											<td><?php echo $value['sr_trip_id']; ?></td>
											<td><?php echo date("d-M-Y H:i:s",strtotime($value['sr_date'])); ?></td>	
											<td><button class="btn btn-view" id="<?php echo 'accepted_ride_view_id_'.$value['sched_id']; ?>" name="accepted_ride_view_id">View</button></td>	
											<td><?php echo $dr_status; ?></td>									
											<!-- <td><button class="btn btn-view" >View</button></td> -->
										</tr>														
										<?php } ?>			
									</tbody>
								  </table>
							</div>
						</div>
					  </div>
					  <div id="rejcted_trip" class="tab-pane fade">
						<h3 class="rid_head">My Rejected Trip</h3>
						<div class="rides_table">
							<div class="table-responsive">
								<table class="table table-striped">
									<thead>
									  <tr>
										<th>Name</th>
										<th>Trip ID</th>
										<th>Date / Time</th>										
										<th>View Details</th>
									  </tr>
									</thead>
									<tbody>
										<?php foreach ($ride_request_closed as $key => $value) { ?>
										<tr>
											<td><span class="rid_nme"><?php echo $value['full_name']; ?></span></td>
											<td id="reject_ride_view_id" style="display:none"><?php echo $value['id']; ?></td>
											<td><?php echo $value['sr_trip_id']; ?></td>
											<td><?php echo $value['datefrmt']; ?></td>											
											<td><button class="btn btn-view" >View</button></td>
										</tr>									
										<?php } ?>
									</tbody>
								  </table>
							</div>
						</div>
					  </div>	
					</div>
					<div id="accepted_view">
						<div class="ride_trip_deta">
							<h3 class="heal_head">
								Ride Trip Details
								<div id="success_message"></div>
								<div id="error_message"></div>
								<span class="back_box"><button class="back_btn" id="accepted_bk" name="accepted_bk"   onclick="showStuff1()";>Back</button></span>
							</h3>
							<div class="sched_deta_box">								
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Customer Name :</label>
											<div class="rid_info" id="full_name"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Driver :</label>
											<div class="rid_info" id="driver_nm"></div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
									<div class="col-md-5 rid_tim">
											<label>Trip Date :</label>
											<div class="rid_info" id="sr_date"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Pick Up Time :</label>
											<div class="rid_info" id="sr_pick_up_time"></div>
										</div>
																			
									</div>
								</div>	
								<div class="schdeta_sec">
									<div class="row">
										<div class="col-md-5 rid_tim">
											<label>Pick Up Point :</label>
											<div class="rid_info" id="source_address"></div>
										</div>
										<div class="col-md-5 rid_tim">
											<label>Drop Point Healthcare Providers Location :</label>
											<div class="rid_info" id="destination_address"></div>
										</div>
									</div>
								</div>
								<div class="schdeta_sec">
									<div class="row">
									<div class="col-md-5 rid_tim">
											<label>Total distance in KM :</label>
											<div class="rid_info" id="sr_total_distance"></div>
										</div>	
										<div class="col-md-5 rid_tim">
											<label>Duration :</label>
											<div class="rid_info" id="duration"></div>
										</div>										
									</div>
								</div>
							</div>	
							<div class="pay_btn_box">								
								<button type="button" class="req_btn cancel_btn" id="">Cancel Ride</button>
								<button type="button" class="req_btn" id="" data-toggle="modal" data-target="#returnride_pop">Completed ride</button>
								<!-- <button type="button" class="req_btn comp_btn" id="customer_complaints">Submit a Complaint</button>					
								<button type="button" class="req_btn" id="" data-toggle="modal" data-target="#returnride_pop">Completed ride</button> -->
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="RideDetailsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        
        <h4 class="modal-title" id="myModalLabel">Trip Details</h4>
      </div>
      <div class="modal-body">
        <div>
              <div class="row" id="addresses">
                <!-- <div class="comp_inner">
                  <h3 class="heal_head">
                    TRIP DETAILS    
                  </h3> -->

                  <!-- <div class="comp_box bluebg showridedetails">
                   -->
                                      

                  <!-- </div> -->
                <!-- </div> -->
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>



<?php $this->load->view("user/footer"); ?>

<script type="text/javascript">
	$(document).ready(function(){

		$('#rejectTripRequest').click(function(){
          
          	alert('ccccccc');

		});

		
	});

	

	function getTripDetails(id)
	{

	    if("" != id)
	    {
	       $.ajax({

	          type : "POST",
	          url : baseURL+"frontend/TransportController/getRideDetails",
	          data :
	          {
	              'id' : id
	          },
	          success : function(response)
	          {
	              //console.log(response);

	              var obj = JSON.parse(response);

	              console.log('source address '+obj.data[0].trip_source_address);
	              $('#addresses').html('');

	              var address = '<div class="form-group">'+
	                      '<div class="col-md-6 pdl0">'+
	                        '<label class="col-md-12 row" for="lblname">Pick Up Address :</label>'+
	                        '<div class="comp_inpt" id="pickUpAddress">'+
	                          obj.data[0].trip_source_address+
	                        '</div>'+
	                      '</div>'+
	                      '<div class="col-md-6 pdl0">'+
	                        '<label class="col-md-12 row" for="lblname">Pick Up Address :</label>'+
	                        '<div class="comp_inpt" id="pickUpAddress">'+
	                          obj.data[0].sr_pick_up_time+
	                        '</div>'+
	                      '</div>'+


	                    '</div>';

	              $.each(obj.data,function(i,val){

	                  address += '<div class="form-group">'+
	                      '<div class="col-md-6 pdl0">'+
	                      '<div class="">'+
	                        '<label class="col-md-12 row" for="lblname">Drop Off Address '+ (i+1)+' :</label>'+
	                        '<div class="comp_inpt dropOffAddress" >'+
	                          val.trip_destination_address+
	                        '</div>'+
	                        '</div>'+
	                      '</div>'+
	                      '<div class="col-md-6 pdl0">';
	                      if("" != val.trip_appointment_time)
	                      {
	                       address += '<div class="">'+
	                        '<label class="col-md-12 row" for="lblname">Trip Appoinment Time '+ (i+1)+' :</label>'+
	                        '<div class="comp_inpt dropOffAddress" >'+
	                          val.trip_appointment_time+
	                        '</div>'+
	                        '</div>'+
	                      '</div>';
	                      }

	                   address += '</div>';
	              });

	              console.log(address);
	              $('#RideDetailsModal').modal('show');
	              $('#addresses').html(address);
	              address = '';
	              //$("#pickUpAddress").text(obj.data[0].trip_source_address);
	          }

	       });
	    }
	    //$('#RideDetailsModal').modal('show');
	}

	function rejectTrip(id)
	{
		if("" != id && undefined !== id)
		{
			$.ajax({

				type : "POST",
				url : baseURL+"frontend/TransportController/rejectTrip",
				data :
				{
					'id' : id
				},
				success : function(response)
				{

					var json = JSON.parse(response);
					if(json.status == "success"){   
			            
			          setTimeout(function(){

			          		window.location.href="<?php echo $this->config->base_url()?>/transport-trips";

			          });
			        }
				}

			});

		}
		
	}



</script>