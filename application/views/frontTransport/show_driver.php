<?php $this->load->view("user/header"); ?>
<?php $this->load->view('frontTransport/transportHeader'); ?>
<style type="text/css">
	.add_box ul li
	{
		list-style: none;
	}
	.add_box input[type=radio]
	{
		vertical-align: middle;
		margin-top: -5px;
		margin-right: 5px;
	}
</style>
<div class="myprof_wrapp">
	<div class="probgimg"></div>
	<div class="mypro_info_wrapp">
		<div class="container">
			<div class="row">
				<span class="error_message" style="color: red;"></span>
				<span class="success_message" style="color: green;"></span>
				<div class="basic_info_wrapp">
					<div class="row">
						<div class="col-md-6">
							<input type="hidden" name="sched_id" id="sched_id" value="<?php echo $ride_detail[0]['id']; ?>">
							<div class="info_head">Trip Details :</div>
							<div class="basic_box">
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/date_icon.png"><span>Trip Date:</span></div>
									<div class="bas_rt"><?php echo $ride_detail[0]['sr_date'] !=""? date("d-M-Y H:i:s",strtotime($ride_detail[0]['sr_date'])) : "-"; ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/sign.png"><span>Trip ID :</span></div>
									<div class="bas_rt"><?php echo $ride_detail[0]['sr_trip_id'] !=""? $ride_detail[0]['sr_trip_id']:"-"; ?></div>
								</div>
								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/zip.png"><span>Pick up point:</span></div>
									<div class="bas_rt"><?php echo $ride_detail[0]['source_address'] !=""? $ride_detail[0]['source_address']:"-"; ?></div>
								</div>

								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/zip.png"><span>Drop point:</span></div>
									<div class="bas_rt"><?php echo $ride_detail[0]['destination_address'] !=""? $ride_detail[0]['destination_address']:"-"; ?></div>
								</div>

								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/sign.png"><span>Distance:</span></div>
									<div class="bas_rt"><?php echo $ride_detail[0]['sr_total_distance'] !=""? $ride_detail[0]['sr_total_distance']." miles":"-"; ?></div>
								</div>

								<div class="basic_inner">
									<div class="bas_lt"><img src="<?php echo base_url(); ?>assets/img/sign.png"><span>Time:</span></div>
									<div class="bas_rt"><?php echo $ride_detail[0]['duration'] !=""? $ride_detail[0]['duration']:"-"; ?></div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="info_head">Select Driver :</div>
							<div class="basic_box add_box">
								<div class="basic_inner">
								<input type="hidden" name="driver_req_id" id="driver_req_id" value="<?php
								echo (!(empty($checked_dr)))?$checked_dr[0]['driver_req_id']:"0";
								 ?>">
									<ul>
										<?php 
										foreach ($driver as $eachdriver) {
											$check_available = getDriverAvailability($eachdriver['id']);
											if($check_available == "availabale")
											{
												if(!empty($checked_dr))
												{
												if($checked_dr[0]['driver_id'] == $eachdriver['id'])
												{
													$checked = "checked";
												}else
												{
													$checked = "";
												}
											}
											?>
											<li><label><input type="radio" name="sel_driver" value="<?php echo $eachdriver['id']; ?>" <?php echo $checked; ?>><span><?php echo $eachdriver['full_name']; ?></span></label></li>
											<!-- <li><label><input type="checkbox" name="sel_driver" class="chk" value="<?php //echo $eachdriver['id']; ?>"><span><?php //echo $eachdriver['full_name']; ?></span></label></li> -->
											<?php } 
											}
											?>
										</ul>
									</div>
								</div>
							</div>


						</div>
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-6">
								<button class="btn" id="assign_driver">Submit</button>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
		<script type="text/javascript" src="<?php echo base_url() ?>assets/js/jquery.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#assign_driver").click(function(){
					var sched_id = $("#sched_id").val();
					var driver_req_id = $("#driver_req_id").val();

					// var chkArray = [];
					// $(".chk:checked").each(function() {
					// 	chkArray.push($(this).val());
					// });
					// var selected;
					// selected = chkArray.join(',');
					// if(selected.length > 0){

						if($("input:radio[name='sel_driver']").is(":checked")) {
							$(".error_message").html("");
							var selected = $("input[name='sel_driver']:checked").val();
							$.ajax({
								type:"POST",
								url:baseURL+"frontend/TransportController/assignDriverToTrip",
								data:{'sched_id':sched_id,"check_driver":selected,"driver_req_id":driver_req_id},
								dataType:"json",
							//async:false,					
							success:function(response){
								if(response.status == "success"){	
									$(".success_message").text(response.message);
									setInterval(function(){
										window.location.href=base_url+"transport-trips";
									},2000);				
								}
								else
									if(response.status == "error"){
										$(".error_message").text(response.message);													
									}				
								}
							});
						}else{
							$(".error_message").html("Please select at least 1 driver.");
							return false;
						}
					});
			});
		</script>
		<?php $this->load->view("user/footer"); ?>