<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Contents
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Contents management</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6 resp_share">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="listPages(0,'page_id','DESC');">
                      Search
                    </a>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getPage();">
                      Add New
                    </a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
				   <div class="table-responsive">	
					  <table class="table table-bordered table-striped dataTable" id="listPages" role="grid" aria-describedby="example1_info">
						<thead>
						  <tr role="row">
							<th width="25%" data-name="page_title" data-order="DESC" class="sorting">Title</th>

							<th width="25%" data-name="sub_title" data-order="DESC" class="sorting" >Sub Title</th>  
							 <th width="25%" >Description</th> 
							                  
							<th width="10%" class="">Action</th>
						  </tr>
						</thead>
						  <tbody>
							
						  </tbody>
						</table>
					 </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="pageModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>admin/pages/savePage" enctype="multipart/form-data" id="pageForm" name="pageForm" class="valida">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Manage Content</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="title">Title</label>
                  <input type="hidden" id="page_key" name="page_key">
                  <input type="text" placeholder="Title" id="title" name="title"  required="true" maxlength="100" class="form-control"  autocomplete="off">
                </div>
              </div>
              
            </div>
             <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="sub_title">Sub Title</label>                 
                  <input type="text" placeholder="Sub Title" id="sub_title" name="sub_title" maxlength="100"  class="form-control"  autocomplete="off">
                </div>
              </div>
              
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="page_name">Page Name</label>                 
                  <input type="text" placeholder="page Name" id="page_name" name="page_name" maxlength="50" required="true" class="form-control"  autocomplete="off">
                </div>
              </div>              
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="page_detail">Description</label>
                  <textarea placeholder="Description" rows="3" name="page_detail" id="page_detail" required="true" class="form-control" autocomplete="off"></textarea>
                </div>
              </div>
            </div>
             <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="image_icon">Image icon upload</label>
                  <input type="file" rows="3" name="image_icon" id="image_icon" class="form-control" autocomplete="off">
                  <input type="hidden" id="oldimage" name="oldimage" value="">
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info" type="submit">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div><script src="<?php echo base_url();?>js/ckeditor-full/ckeditor.js"></script>
  <script src="<?php echo base_url(); ?>js/admin/pages.js"></script>
<?php $this->load->view('footer'); ?>
