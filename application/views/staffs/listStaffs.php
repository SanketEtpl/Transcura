<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Customer Support staff’s
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Customer Support staff’s</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="liststaffs(0,'user','DESC');">
                      Search
                    </a>
                    <!--a class="btn btn-primary" href="javascript:void(0);" onclick="getUser(this);">
                      Add New
                    </a-->
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listStaffs" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="25%" data-name="fname" data-order="DESC" class="sorting">First Name</th>
                        <th width="25%" data-name="lname" data-order="ASC" >Last Name</th>
                        <th width="25%" data-name="email" data-order="ASC">Email</th>
                        <th width="15%" data-name="phone" data-order="ASC" >Phone</th>
                       
                        
                        <th width="15%" >Activation</th>
                        <th width="10%" class="">Action</th>
                      </tr>
                    </thead>
                      <tbody>
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="userModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>admin/staffs/saveStaff" id="userForm" name="userForm" class="user_validation">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Manage Customer Support staff’s</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="first_name">First Name</label>
                  <input type="hidden" id="user_key" name="user_key">
                  <input type="text" placeholder="First Name" id="first_name" name="first_name"  required="true" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="last_name">Last Name</label>
                  <input type="text" placeholder="Last Name" id="last_name" name="last_name" required="true" class="form-control" autocomplete="off">
                </div>
              </div>
            
         
              
              <div class="col-md-6">
                <div class="form-group" >
                  <label class="control-label"  for="User_type">Agencies</label>
                  <div>
                  <select id="agencies" required="true" name="agencies" class="form-control">             
                  </select>
                 </div>
                </div>
              </div>



              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="address">Address</label>
                  <input type="text" placeholder="Address" id="address" name="address" class="form-control" autocomplete="off">
                </div>
              </div>
            
                <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="email_address">Email-Id</label>
                  <input type="text" placeholder="Email-Id" id="email" name="email" required="true" class="form-control" autocomplete="off">
                </div>
              </div>
 <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="contact_number">Contact Number</label>
                  <input type="text" placeholder="Contact Number" id="phone" name="phone" required="true" class="form-control">
                </div>
              </div>
            
                                
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="zipcode">Zip Code</label>
                  <input type="text" placeholder="Zip Code" id="zipcode" name="zipcode" class="form-control" autocomplete="off">
                </div>
              </div>
            </div>

           
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/staff.js"></script>
<?php $this->load->view('footer'); ?>
