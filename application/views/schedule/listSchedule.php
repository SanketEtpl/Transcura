<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Schedule
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> Schedule Users</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6 resp_share">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);"  onclick="listSchedule(0,'user','DESC');">
                      Search
                    </a>
                 
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                   <div class="table-responsive">	
					  <table class="table table-bordered table-striped dataTable" id="listSchedule" role="grid" aria-describedby="example1_info">
						<thead>
						  <tr role="row">
							<th width="25%" data-name="fname" data-order="DESC" class="sorting">User</th>                      
							<th width="25%" data-name="sr_date" data-order="ASC" class="sorting">Date</th>
							<th width="15%" data-name="sr_appointment_time" data-order="ASC" class="sorting">Time</th>
							<th width="15%" >country</th>
							  <th width="15%" >State</th>
							
						  </tr>
						</thead>
						  <tbody>
							
						  </tbody>
						</table>
					</div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="userModal" class="modal fade" style="display: none;" aria-hidden="false">
    
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/schedule.js"></script>
  <?php $this->load->view('footer'); ?>
