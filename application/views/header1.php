<?php $segmentMenu = $this->uri->segment(1); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Les Flammant | Admin Panel</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/plugin/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/toastr.min.css">
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>js/jquery-2.2.3.min.js"></script>
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script type="text/javascript">
    var BASEURL = '<?php echo base_url(); ?>';
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <a href="<?php echo base_url(); ?>" class="logo">
      <span class="logo-mini"><b>Les</b> Flammant</span>
      <span class="logo-lg"><b>Les</b> Flammant</span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li>
            <a href="<?php echo base_url(); ?>logout" title="Logout"><i class="fa fa-sign-out"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="treeview<?php if(in_array($segmentMenu, array("subadmin","airports"))){echo " active";} ?>">
          <a href="#">
            <i class="fa fa-fw fa-plane"></i> <span>Airport Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>subadmin"><i class="fa fa-fw fa-users"></i> <span>Airport Admins</span></a></li>
            <li><a href="<?php echo base_url(); ?>airports"><i class="fa fa-fw fa-plane"></i> <span>Airports</span></a></li>
          </ul>
        </li>

        <li class="treeview<?php if(in_array($segmentMenu, array("store"))){echo " active";} ?>">
          <a href="#">
            <i class="fa fa-fw fa-university"></i> <span>Store Management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>store"><i class="fa fa-fw fa-code-fork"></i> <span>Store Category</span></a></li>
            <li><a href="<?php echo base_url(); ?>store"><i class="fa fa-fw fa-university"></i> <span>Stores</span></a></li>
          </ul>
        </li>
        <li><a href="<?php echo base_url(); ?>users"><i class="fa fa-fw fa-users"></i> <span>Users</span></a></li>
      </ul>
    </section>
  </aside>