<?php $this->load->view('user/header'); ?>
<!--Reset password-->
<div class="loginsignup">
	<div class="logininner">
		<!--login form-->
		<div class="signupformmain">	
			<div class="signupform-inner">					
				<div class="loginform">
					<div class="titlestrip">
						<div class="ttl text-center">
							<h3 class="text-uppercase">Change Password</h3>
						</div>
					</div>
					<!--form-->
					<form id="resetform" class="form-horizontal forgotform" method="POST"role="form" action="<?php echo base_url(); ?>frontend/Homecontroller/changePassword">
					<!-- <div class="message"></div> -->
					<?php 
						$fail_change_msg = $this->session->flashdata('fail_change_msg');
				        if($fail_change_msg) { ?>
	                    <div class="alert alert-danger">
	                      <?php echo $fail_change_msg; ?>
	                    </div>
				    <?php } ?>					
					<div class="form-group mt-20">
						<label for="pwd" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-5.png" alt="" />New password</label>
						<input type="password" class="form-control" id="pass" name="newpassword" autocomplete="off" placeholder="New Password">
						<?php echo form_error('newpassword', '<div class="error">', '</div>'); ?>
					</div>
					<div class="form-group mt-20">
						<label for="confirmpwd" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-5.png" alt="" />Confirm password
						</label>
						<input type="password" class="form-control" id="conpass" name="confirm_password"  autocomplete="off"  placeholder="Confirm Password">
						<?php echo form_error('confirm_password', '<div class="error">', '</div>'); ?>
						<input type="hidden" name="id" value="<?php echo $mydata['id']; ?>">
		          		<input type="hidden" name="token" value="<?php echo $mydata['token']; ?>">
		          		<!-- <input type="hidden" name="id" value="713">
		          		<input type="hidden" name="token" value="6ETSXiCIUNzYswa0lAV1"> -->
		          	</div>					
					<div class="form-group">
						<!-- Button -->
						<div class="controls">
						  <input type="submit" name="change_pwd" value="Submit" class="btn btn-block btn-primary text-uppercase"/>
						</div>
					</div>   
				</form> 
				<!--form-->					
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('user/footer'); ?>
