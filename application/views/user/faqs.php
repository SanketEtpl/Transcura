<?php $this->load->view('user/header'); ?>
<div class="faq_main innerpages">
  <div class="theme-breadcrumb pad-50">
    <div class="theme-container container ">
      <div class="row">
        <div class=" col-md-12">
          <div class="title-wrap">
            <h2 class="section-title no-margin">Faq</h2>
            <p class="fs-16 no-margin">Why choose us?</p>
          </div>
        </div>
      </div>
    </div>
  </div>
<div class="inner-wrapper faq">
  <div class="container">
    <div class="panel-group collaps" id="accordion" role="tablist" aria-multiselectable="true">
      <?php
        $i = 1;
        if(!empty($faqs)){
            foreach($faqs as $value){
              if($i === 1 ){
              
        ?>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>" aria-expanded="true" class=""> <span><?php echo $i; ?>.</span> <?php echo $value['question']; ?> </a> </h4>
        </div>
        <div id="collapse<?php echo $i; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true">
          <div class="panel-body"><?php echo $value['answer']; ?></div>
        </div>
      </div>
      <?php }else{ ?>
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingTwo">
          <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $i; ?>" aria-expanded="false"><span><?php echo $i; ?>.</span> <?php echo $value['question']; ?></a> </h4>
        </div>
        <div id="collapse<?php echo $i; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
          <div class="panel-body"> <?php echo $value['answer']; ?> </div>
        </div>
      </div>
      <?php } $i++; }} else {  } ?>   
      
    </div>
  </div>
</div>
</div>
<?php $this->load->view('user/footer'); ?>