<div class="im_wrap">
	<img src="<?php echo base_url(); ?>assets/img/Untitled-2.png" class="img-responsive img_resp" alt="imgslide"/>
	<div>
		<div class="img_slider">
	     	<div id="carousel-example" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carousel-example" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example" data-slide-to="1"></li>
					<li data-target="#carousel-example" data-slide-to="2"></li>
				</ol>
			  	<div class="carousel-inner">
					<div class="item active">
				  		<a href="#"><img src="<?php echo base_url(); ?>assets/img/Untitled-2.png" class="img-responsive img_resp" alt="imgslide"/></a>
					</div>
					<div class="item">
						<a href="#"><img src="<?php echo base_url(); ?>assets/img/Untitled-2.png" class="img-responsive img_resp" alt="imgslide_first"/></a>
					</div>
					<div class="item">
						<a href="#"><img src="<?php echo base_url(); ?>assets/img/Untitled-2.png" class="img-responsive img_resp" alt="imgslide_third"/></a>
					</div>
				</div>
			  	<a class="left carousel-control" href="#carousel-example" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left"></span>
			  	</a>
			  	<a class="right carousel-control" href="#carousel-example" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right"></span>
			  	</a>
			</div>
		</div>
		<div class="container">
	  		<h2>Modal Example</h2>
	  		<!-- Trigger the modal with a button -->
	  		<!-- Modal -->
	  		<div class="modal fade" id="myModal" role="dialog">
	    		<div class="modal-dialog">	    
	      		<!-- Modal content-->
	     			<div class="modal-content">
	      				<div class="modal-header">
				        	<button type="button" class="close" data-dismiss="modal">&times;</button>
				        	<h4 class="modal-title">Modal Header</h4>
				        </div>
				        <div class="modal-body">
				          <p>Some text in the modal.</p>
				        </div>
				        <div class="modal-footer">
					        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        </div>
				    </div>
	    		</div>
	  		</div>						  
		</div>
		<div class="web_info">
	    	<div class="container">
		        <div class="row">
				    <div class="web_info_forms">
					    <div class="col-md-7 cols_fo">
							<div class="media">
								<div class="media-left">
							 		<img src="<?php echo base_url(); ?>assets/img/01.png" class="media-object media_img" alt="technology"/>
								</div>
								<div class="media-body">
							  		<h3 class="media-heading">TECHNOLOGY</h3>
							  		<p>Technology is the driving force behind this well-designed platform. Using the latest and most up-to-date technology, our software and mobile application features allow us to maximize the flow of information to enhance the communication process, lower’s average cost per trip, and improve relations between providers and their clients.</p>
							  		<div class="media_a">
							      		<a href="#">LEARN MORE</a>
							  		</div>
								</div>
						    </div>
						    <div class="media">
								<div class="media-left">
							    	<img src="<?php echo base_url(); ?>assets/img/02.png" class="media-object media_img" alt="fleet management"/>
								</div>
								<div class="media-body">
								    <h3 class="media-heading">FLEET MANAGEMENT</h3>
								    <p>TransCura encourages and incentivizes ride share programs and independent drivers along with other transportation resources to register to our platform. Our goal is to ensure members the peace of mind knowing there will always be transportation available and ready to meet their on-time demands. </p>
								  	<a href="#">LEARN MORE</a>
								</div>
						    </div>
						    <div class="media">
								<div class="media-left">
								  <img src="<?php echo base_url(); ?>assets/img/03.png" class="media-object media_img" alt="members"/>
								</div>
								<div class="media-body">
								  <h3 class="media-heading">MEMBERS</h3>
								  <p>We have considered the two major problems in the transportation industry with on-time demands and increasing costs. Wherein, to meet our members’ on-time demands, we have designed a 15-minute out scheduling system to improve on-time arrivals. In addition, to help minimize unwanted costs and surges, we offer a cost-rate analysis feature to provide our members with affordable transportation options to choose from.</p>
								  <a href="#">LEARN MORE</a>
								</div>
						    </div>
					        <div class="media">
								<div class="media-left">
							  		<img src="<?php echo base_url(); ?>assets/img/04.png" class="media-object media_img" alt="medical emergency"/>
								</div>
								<div class="media-body">
								    <h3 class="media-heading">NON-EMERGENCY MEDICAL TRANSPORTATION</h3>
								 	<p>The Medicaid and Medicare Non-Emergency Medical Transportation has been one of the most challenging programs to manage. After years of experience working with the nation’s top leading brokerage firms, we have successfully designed a 3-teir technology-based software system with many wonderful features to help minimize patient complaints, wasteful spending and fraud abuse.</p>
								  	<a href="#">LEARN MORE</a>
								</div>
						  	</div>
						</div>
							<div class="col-md-5 pannel_padd">
								<div class="panel panel-default">
								    <div class="panel-heading pannel_head">
										<h2 class="panel-title">SCHEDULING RIDES</h2>
									</div>
									<div class="panel-body">
										<form name="schedule_ride" action="<?php echo base_url(); ?>schedule-ride" method="POST">
											<div class="row">	
											<?php
					                    		$success = $this->session->flashdata("success");
					                    		$fail = $this->session->flashdata('fail');
					                    		if($success) { ?>
								                    <div class="alert alert-success">
								                    	<?php echo $success; ?>
								                    </div>
								                  <?php
								                  }
								                if($fail){ ?>
								                    <div class="alert alert-danger">
								                        <?php echo $fail; ?>
								                    </div>
								                    <?php } ?>																
												<div class="col-md-12">
												    <div class="panel-heading">
													    State
													</div>
													<div class="form-group">
											            <select class="form-control upgrade_box" name="state" id="select_state">
															<option value="" disable selected hidden> Select State</option>
															<?php foreach($states as $key => $value) { ?>
															<option value="<?php echo $value['id']; ?>"<?php echo set_select('state', $value['id'], FALSE); ?>><?php echo $value['state_name']; ?></option>
															<?php } ?>
														</select>
														<?php echo form_error('state', '<div class="error">', '</div>'); ?>
													</div>
												</div>
												<div class="form_resp">
												    <div class="col-md-6">
														<div class="panel-heading">
															First Name
														</div>
														<div class="form-group">
															<input type="text" name="first_name" value="<?php echo set_value('first_name'); ?>" id="first_name" class="form-control input-sm"/>
															<?php echo form_error('first_name', '<div class="error">', '</div>'); ?>
														</div>
													</div>
													<div class="col-md-6">
														<div class="panel-heading">
															Last Name
														</div>
														<div class="form-group">
															<input type="text" name="last_name" id="last_name" value="<?php echo set_value('last_name'); ?>" class="form-control input-sm"/>
															<?php echo form_error('last_name', '<div class="error">', '</div>'); ?>
														</div>
												    </div>
												</div>												
												<div class="form_resp">
												    <div class="col-md-6">
														<div class="panel-heading">
															Date Of Services
														</div>
														<div class="input-group">
															<input type="text" class="form-control" id="datepicker" value="<?php echo set_value('date'); ?>" name="date" placeholder="YYYY-MM-DD">
															<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>															
														</div>
														<?php echo form_error('date', '<div class="error">', '</div>'); ?>
													</div>
													<div class="col-md-6">
														<div class="panel-heading">
															Pick-Up Time
														</div>
														<div class="form-group">
															<input type="text" name="time" id="timepicker" value="<?php echo set_value('time'); ?>" class="form-control input-sm" placeholder="00: 00: 00"/>
															<?php echo form_error('time', '<div class="error">', '</div>'); ?>
														</div>
											    	</div>
												</div>
												<div class="col-md-12">
													<div class="panel-heading">
														Pick Up Address
													</div>
													<div class="form-group">
														<input type="text" name="pick_up_address" value="<?php echo set_value('pick_up_address'); ?>" id="address" class="form-control input-sm"/>
														<?php echo form_error('pick_up_address', '<div class="error">', '</div>'); ?>
													</div>
												</div>
												<div class="col-md-12">
												    <div class="panel-heading">
													    Type Of Healthcare Services
													</div>
													<div class="form-group">
											            <select class="form-control upgrade_box" name="health_service" id="healthcare_services">
															<option value="" disable selected hidden>Select</option>
															<?php foreach($hc_service as $key => $value) { ?>
															<option value="<?php echo $value['id']; ?>"<?php echo set_select('health_service', $value['id'], FALSE); ?>><?php echo $value['hs_name']; ?></option>
															<?php } ?>					
														</select>
														<?php echo form_error('health_service', '<div class="error">', '</div>'); ?>
													</div>
												</div>
												<div class="col-md-12">
												    <div class="panel-heading">
													    Special Requests
													</div>
													<div class="form-group">
											            <select class="form-control upgrade_box" name="special_request" id="Spacial_requests">
															<option value="" disable selected hidden>Select</option>
															<?php foreach($sp_request as $key => $value) { ?>
															<option value="<?php echo $value['id']; ?>"<?php echo set_select('special_request', $value['id'], FALSE); ?>><?php echo $value['sr_name']; ?></option>
															<?php } ?>
														</select>
														<?php echo form_error('special_request', '<div class="error">', '</div>'); ?>
													</div>
												</div>
												<div class="col-md-12">
													<div class="panel-heading">
														Spacial Instruction
													</div>
													<div class="form-group">
														<input type="text" name="special_instruction" value="<?php echo set_value('special_instruction'); ?>" id="instructions" class="form-control input-sm"/>
														<?php echo form_error('special_instruction', '<div class="error">', '</div>'); ?>
													</div>
												</div>
												<div class="col-md-12">
												    <div class="panel-heading">
													    Desired Transportation
													</div>
													<div class="form-group">
											            <select class="form-control upgrade_box" name="desire_transp" id="ambulance_emergency">
															<option value="" disable selected hidden>Select</option>
															<?php foreach($desire_tran as $key => $value) { ?>
															<option value="<?php echo $value['id']; ?>"<?php echo set_select('desire_transp', $value['id'], FALSE); ?>><?php echo $value['dt_name']; ?></option>
															<?php } ?>
														</select>
														<?php echo form_error('desire_transp', '<div class="error">', '</div>'); ?>
													</div>
												</div>
											</div>
										    <div class="form_resp">
												<div class="col-md-6 btn_cols">
													<div class="form-group">
														<input type="submit" value="SUBMIT" class="btn btn_nav"/>
													</div>
												</div>
												<div class="col-md-6 btn_cols">
													<div class="form-group">	
														<input type="button" value="CANCEL" class="btn btn_nav"/>
													</div>
												</div>
											</div>
										</form>										
									</div>
								</div>
							</div>		
						</div>
					</div>
		  		</div>
			</div>
			<div class="web_inst">
				<div class="container">
	      			<div class="row">
				        <div class="about_info">
						    <div class="all_info">
							    <div class="info_header">
									<h1 class="ifo_h">ABOUT US</h1>
									<div class="opacity_text">
										<h1>ABOUT US</h1>
									</div>
								</div>
								<div class="text_ifo">
									<p>TransCura means transportation solution. "Trans", the prefix for transportation, and "Cura"- a Latin word for cure, remedy, solution, is made up of a team of business professionals and technology driven experts committed to excellence and creating innovative and flexible solutions for our clients. We are one of the few transportation management firms to use an integrated solution team and client relationship management to provide a strategic mix of technology and business management. Our experience and unique skill sets enables us to implement performance-based practices to increase productivity and offer the best solutions in logistics, technology and managed services. TransCura's early adoption of new and emerging technology platforms, is pioneering the way for a safer and much cost-effective way to travel to ensure "No Person Is Left Behind!"</p>
									</div>
									<div class="about_btn">
									     <button class="btn about_btn_info">LEARN MORE</button>
									</div>
							  </div>
						</div>
		  			</div>
				</div>
			</div>
			<div class="img_back_video">
				<div class="img_video_section">
				  	<div class="img_section">
						<img src="<?php echo base_url(); ?>assets/img/img-01.jpg" class="img-responsive video_img" alt="video img"/>
						<div class="video_sec">
						   	<h1>PAVING THE WAY</h1>
							<video class="video_sec_sc" controls>
							  	<source src="<?php echo base_url(); ?>assets/video/VideoHD.mp4" type="video/mp4"/>
							  	<source src="<?php echo base_url(); ?>assets/VideoHD.ogg" type="video/ogg"/>
							  	Your browser does not support HTML5 video.
							</video>
						</div>
				  	</div>
				</div>
			</div>
			<div class="solution_section">
	    		<div class="container">
			 		<div class="row">
			        	<div class="info_solution">
					    	<h1 class="solu_sec">PROVIDING THE BEST TRANSPORTATION<br> SOLUTIONS FROM INCEPTION TO COMPLETION</h1>
						    <div class="solu_opacity">
						        <h1>PROVIDING THE BEST TRANSPORTATION<br> SOLUTIONS FROM INCEPTION TO COMPLETION</h1>
						  	</div>
						</div>
						<div class="ifo_slu_pass">
							<p>We are using innovative technology support systems to improve the way transportation services are performed. </p>
						</div>
						<div class="solution_info">
					        <div class="col-md-4 info_min_height">
								<div class="img_info_view">
								    <div class="logo_with_info">
										<img src="<?php echo base_url(); ?>assets/img/05.png" class="img-responsive main_resp" alt="safety img"/>
								    </div>
								    <div class="log_wi_head">
										<h4>SAFETY AND TRAINING PROCESS</h4>
								    </div>
								    <div class="log_wi_p">
										<p>Safety and Training is essential to registering with our platform. To ensure all our providers/independent drivers remain HIPPA and DOT compliant,....</p>
								    </div>	
								    <div class="log_wi_btn">
										<button class="btn btn_logo_img">VIEW MORE</button>
								    </div>
	                            </div>													   
						    </div>
						    <div class="col-md-4 info_min_height">
						        <div class="img_info_view">
								    <div class="logo_with_info">
									   	<img src="<?php echo base_url(); ?>assets/img/06.png" class="img-responsive main_resp" alt="time track"/>
								    </div>
								    <div class="log_wi_head">
										<h4>REAL TIME TRACKING</h4>
								    </div>
								    <div class="log_wi_p">
										<p>Real Time Tracking allows members to see precisely were their driver is at all times giving them the peace of mind they deserve knowing their ride is on the way. ......</p>
								    </div>	
								    <div class="log_wi_btn">
										<button class="btn btn_logo_img">VIEW MORE</button>
								    </div>
	                            </div>	
						    </div>
						    <div class="col-md-4 info_min_height">
						        <div class="img_info_view">
							  	    <div class="logo_with_info">
										<img src="<?php echo base_url(); ?>assets/img/07.png" class="img-responsive main_resp" alt="data"/>
								    </div>
								    <div class="log_wi_head">
									 	<h4>REPORTS AND DATA</h4>
								    </div>
								    <div class="log_wi_p">
										<p>Improved Reports and Data system allows completed oversight into the transportation process. Tracking and recording service data from beginning to end ......</p>
								    </div>	
								    <div class="log_wi_btn">
										<button class="btn btn_logo_img">VIEW MORE</button>
								    </div>
	                            </div>	
						    </div>
						    <div class="col-md-4 info_min_height">
								<div class="img_info_view">
							  	    <div class="logo_with_info">
										<img src="<?php echo base_url(); ?>assets/img/08.png" class="img-responsive main_resp" alt="loading"/>
								    </div>
								    <div class="log_wi_head">
										<h4>BENEFITS OF MULTI-LOADING</h4>
								    </div>
								    <div class="log_wi_p">
								        <p>Our Multi-loadfeature is designed to maximize vehicle capacity and eliminate single route scheduling for members being picked up and dropped off at the same or close by locations.</p>
								    </div>	
								    <div class="log_wi_btn">
										<button class="btn btn_logo_img">VIEW MORE</button>
								   </div>
	                            </div>													   
						    </div>
						    <div class="col-md-4 info_min_height">
						        <div class="img_info_view">
								    <div class="logo_with_info">
										<img src="<?php echo base_url(); ?>assets/img/09.png" class="img-responsive main_resp" alt="time scheduling"/>
								    </div>
								    <div class="log_wi_head">
										<h4>AUTOMATED ON-TIME<br> SCHEDULING FEATURES</h4>
								    </div>
								    <div class="log_wi_p">
								        <p>Automated On-Time Scheduling System will schedule transportation services so that members will arrive fifteen minutes early to their destination. ......</p>
								    </div>	
								    <div class="log_wi_btn">
										<button class="btn btn_logo_img">VIEW MORE</button>
								    </div>
	                            </div>	
						    </div>
						    <div class="col-md-4 info_min_height">
						        <div class="img_info_view">
								    <div class="logo_with_info">
										<img src="<?php echo base_url(); ?>assets/img/10.png" class="img-responsive main_resp" alt="notifiaction"/>
								    </div>
								    <div class="log_wi_head">
										<h4>NOTIFICATION FEATURES</h4>
								    </div>
								    <div class="log_wi_p">
								        <p>Enhanced Communications is the cornerstone of our system’s success. After listening and identifying the vast number of miscommunications in the transportation process,.......</p>
								    </div>	
								    <div class="log_wi_btn">
										<button class="btn btn_logo_img">VIEW MORE</button>
								    </div>
	                            </div>	
						    </div>
						    <div class="col-md-4 info_min_height">
						        <div class="img_info_view">
								    <div class="logo_with_info">
										<img src="<?php echo base_url(); ?>assets/img/10.png" class="img-responsive main_resp" alt="features"/>
								    </div>
								    <div class="log_wi_head">
										<h4>NOTIFICATION FEATURES</h4>
								    </div>
								    <div class="log_wi_p">
								        <p>Member Support begins with the easily flow or transfer of information. To give our Members 24/7/365 days a year access to information and assistance with eligibility .....</p>
								    </div>	
								    <div class="log_wi_btn">
										<button class="btn btn_logo_img">VIEW MORE</button>
								    </div>
	                            </div>	
						    </div>
						</div>
						<div class="ifo_lastbtn">
						      <button class="btn ifo_btn_l">VIEW ALL</button>
						</div>
			  		</div>
				</div>
			</div>
			<div class="mobile_app">
	    		<div class="mobile_apps_sec">
					<div class="col-md-6 col_m">
						<div class="img_re">
							 <img src="<?php echo base_url(); ?>assets/img/banerimg_01.jpg" class="img-responsive mob_app_img" alt="banner"/>
						</div>
						<div class="app_text_btn">
			            	<p>
			            		Allows transportation providers 
								and independent drivers to register and<br> upload 
								credentialing docs to our platform. Once ready to go,<br> 
								drivers will have access to turn by turn navigation 
								with access to ALL <br>relevant information and instructions 
								making <br>transportation manageable, simple and fun. 
						 	</p>
	                        <div class="btn_lat">							
								<button class="btn btn-outline" type="button">VIEW MORE</button>
							</div>
						</div>
					</div>
					<div class="col-md-6 col_m">
						<div class="img_re">
							<img src="<?php echo base_url(); ?>assets/img/banerimg_02.jpg" class="img-responsive mob_app_img" alt="banner first"/>
						</div>
						<div class="app_text_btn">
				            <p>
				            	Allows members to schedule
								and manage trips, edit personal<br> information, 
								access driver information, track rides, <br>
								cancel rides, request for a return ride 
								and rate a trip all<br> within the comforts 
								oftheir smart phone.
							</p>
	                        <div class="btn_lat">							
								<button class="btn btn-outline" type="button">VIEW MORE</button>
							</div>
						</div>
					</div>
				</div>
				<div class="mobile_app_head">
				    <div class="container">
					    <div class="row">
							<div class="app_head">
								<h2>MOBILE APP SOLUTIONS <br>FOR DRIVERS AND MEMBERS</h2>
							</div>
						</div>
					</div>
				</div>
			</div>	
		    <div class="info_img_gallery">
		        <div class="container">
				    <div class="row">
					    <div class="img_gallery_head">
							<div class="img_head_f">
								<h1 class="img_head_sac">WHAT EVERYONE SAYING<br> ABOUT TRANSCURA</h1>
								<div class="head_gallery">
									<h1>WHAT EVERYONE SAYING<br> ABOUT TRANSCURA</h1>
								</div>
							</div>
						</div>
					</div>	
				</div>
		    </div>  
			<div class="img_slider_sec">
	    		<div class="container">
		        	<div class="row">
					    <div class="col-md-12" data-wow-delay="0.2s">
							<div class="carousel slide" data-ride="carousel" id="quote-carousel">
								<!-- Bottom Carousel Indicators -->
								<ol class="carousel-indicators">
									<li data-target="#quote-carousel" data-slide-to="0" ><img class="img-responsive " src="<?php echo base_url(); ?>assets/img/img03.png" alt="client_first">
									</li>
									<li data-target="#quote-carousel" data-slide-to="1" class="active"><img class="img-responsive" src="<?php echo base_url(); ?>assets/img/img.png" alt="client_second">
									</li>
									<li data-target="#quote-carousel" data-slide-to="2"><img class="img-responsive" src="<?php echo base_url(); ?>assets/img/img02.png" alt="client_third">
									</li>
								</ol> 
								<div class="carousel-inner text-center">
									<!-- Quote 1 -->
									<div class="item active">
										<div class="row">
											<div class="col-sm-8 col-sm-offset-2 sm_box">
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut<br> labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
												<h4>Jason Smath</h4>
												<div class="img_star">
												        <img src="<?php echo base_url(); ?>assets/img/stars.png" class="img-responsive lsta_slider" alt="star_client"/>
												</div>
											</div>
										</div>
									</div>
									<!-- Quote 2 -->
									<div class="item">
										<div class="row">
											<div class="col-sm-8 col-sm-offset-2 sm_box">
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut<br> labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
												<h4>Jason Smath</h4>
												<div class="img_star">
											        <img src="<?php echo base_url(); ?>assets/img/stars.png" class="img-responsive lsta_slider" alt="star_client_first"/>
												</div>
											</div>
										</div>
									</div>
									<!-- Quote 3 -->
									<div class="item">
										<div class="row">
											<div class="col-sm-8 col-sm-offset-2 sm_box">
												<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut<br> labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
												<h4>Jason Smath</h4>
												<div class="img_star">
												    <img src="<?php echo base_url(); ?>assets/img/stars.png" class="img-responsive lsta_slider" alt="star_client_second"/>
												</div>
											</div>
										</div>
									</div>
								</div>
								<!-- Carousel Slides / Quotes -->
								<!-- Carousel Buttons Next/Prev -->
								<a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
								<a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
							</div>
						</div>
		 			</div>
		 		</div>	
			</div>
		</div>
	</div>	
	<script type="text/javascript">
		$( "#timepicker" ).timepicker({
			startHour:10,
			maxHour : 22
		});
		$( "#datepicker" ).datepicker();
	</script>