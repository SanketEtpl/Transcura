<?php $this->load->view('user/header'); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<style>
  #map {
    height: 270px;
    width: 100%;
  }
</style> 

    
<div class="contact_main innerpages address_main">
  <div class="theme-breadcrumb pad-50">
    <div class="theme-container container ">
      <div class="row">
        <div class=" col-md-12">
          <div class="title-wrap">
            <h2 class="section-title no-margin"><?php if(!empty($contactUs)) { echo $contactUs[0]['page_title']; } else { echo ''; } ?></h2>
            <p class="fs-16 no-margin"><?php if(!empty($contactUs)) { echo $contactUs[0]['sub_title']; } else { echo ''; } ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="inner-wrapper contact-us">
    <div class="container">
      <!-- <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=<?php// echo GOOGLE_API_KEY; ?>&callback=initMap&region=USA&v=3.exp&sensor=false&libraries=places">
      </script> -->
      <div id="schLoadingDiv" style="position: fixed;z-index: 999;background-color: rgba(0, 0, 0, 0.35);width: 100%;height: 100%;top: 0;text-align: center;left:0;">
          <img id="loader-img" alt="loading" src="<?php echo base_url(); ?>assets/img/loading.gif" align="center" style="margin: 0 auto;width: 60px;margin-top: 218px;
        "/>
        </div> 
                 
      <div class="row">  
        <h2 class="mb15">Search Health Care</h2>
        <div class="alert alert-success alert-dismissable" id="success_message" style="display:none">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>          
          <strong>Success!</strong> Your trip request has been send to the admin.
        </div>
        <div class="alert alert-warning alert-dismissable" id="error_message" style="display:none">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Fail!</strong> Your trip request has been failed .
        </div>

        <div class="col-sm-4 col-md-4 col-xs-12 search-health-form" onload="initialize()">
      
          <div class="forms">
            <form name="frm_contact_us" action="<?php echo base_url()."frontend/DriverController/get_address"; ?>" method="POST">
            <!-- <form name="frm_contact_us" method="POST" action="<?php// echo base_url(); ?>submit-contact-us"> -->
              <div class="controls field">                                  
                <div class="row">
                  <div class="col-md-12">                  
                   <!--  <div class="form-group">
                      <label for="contact_name">From source point</label>
                      <input id="name" type="text" name="name" value="<?php echo $name; ?>" class="form-control name" placeholder="From name">
                      <div class="help-block with-errors"></div>                      
                    </div> -->
                    
                     <div class="form-group">
                      <label for="contact_name">From source point</label>
                      <input id="name" type="text" name="name" value="<?php echo set_value('name'); ?>" class="form-control name" placeholder="Enter location of source address" maxlength="255">
                      <div class="help-block with-errors"></div>      
                    </div>  
                    <div class="form-group">
                      <label for="contact_name">To destination point</label>
                      <input id="toname" type="text" name="toname" value="<?php echo set_value('toname'); ?>" class="form-control name" placeholder="Enter location of destination address" maxlength="255">
                      <div class="help-block with-errors"></div>      
                    </div>                
                  </div>
                </div>                  
              </div>
              <div class="row">                               
                <div class="col-md-12 submitbtn">
                  <input type="hidden" id="current_lat" name="current_lat">
                  <input type="hidden" id="current_lng" name="current_lng">
                  <input id="getAddress" class="req_btn comp_btn" type="button" name="back" value="Get address"/>

                  <!-- <input id="scheduleRideBtn" class="req_btn comp_btn" type="button" name="schedule_ride" value="Schedule ride"/> -->
                   <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
                    <!-- <button type="button" name="submit" id="submit">Submit</button> -->
                </div>
              </div>                                 
          </form>
        </div>
      </div>
    <div class="col-sm-8 col-md-8 col-xs-12 search-health-map">   
        <div id="map"></div>  
    </div>
    </div>
    <br />
    <!-- <div class="col-sm-12" id="drop_point_details">-->
    <div class="schedule-ridetable" id="drop_point_details">
    <div class="row">
        <div class="table-responsive">
          <table class="table table-striped dataTable" id="listAddress" role="grid" aria-describedby="example1_info">
            <thead>
            <tr role="row">
                <th width="25%">Destination address</th>
                <th width="25%">From address</th> 
              <th width="25%">To address</th>             
                <th width="25%">Current Lat & lng </th>
                <th width="25%">Destination lat & lng</th>
              <th width="25%">Distance</th>
              <th width="25%">Duration</th>
              </tr>        
            </thead>
          <tbody>         
          </tbody>
          </table>
        </div>
      </div>
      </div>
  
  </div>
</div>
</div> 
  <!-- Modal -->
  <div class="modal address-show fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span class="jsonName"></span></h4>
        </div>
        <div class="modal-body">
          <h4 class="text-center addrs">ADDRESS OF HEALTHCARE</h4>
      <div class="bluebg">
       <div class="form-group" >
        <label class="col-md-3" for="lblname">Name</label> 
        <span class="jsonName col-md-9"></span>               
       </div>  
       <div class="form-group" >
        <label for="lblname" class="col-md-3">Address</label> 
        <span id="jsonAddress" class="col-md-9"></span>               
       </div>  
        <div class="form-group" >
        <label for="lblname" class="col-md-3">Mobile no</label> 
        <span id="jsonPhone" class="col-md-9"></span>               
       </div>  
        <div class="form-group" >
        <label class="col-md-12">Opening Hours</label> 
        <div class="col-md-4 innrechild">
          <label for="lblname">Monday to Saturday</label> 
          <span id="" class="">8AM to 8PM</span>     
        </div>
        <div class="col-md-4 innrechild">
          <label for="lblname">Sunday</label> 
          <span id="" class="">Closed</span> 
        </div>
       </div>        
      </div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="req_btn comp_btn" id="popContinue">CONTINUE</button>
        </div>
      </div>
      
    </div>
  </div>

<script src="http://localhost/AppTech/js/jquery.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=<?php //echo GOOGLE_API_KEY; ?>"></script>-->
<script type="text/javascript">
/*function initialize() {
var sourceAddrs = document.getElementById('name');
var destinationAddrs = document.getElementById('toname');
var autocomplete = new google.maps.places.Autocomplete(sourceAddrs);
var autocomplete = new google.maps.places.Autocomplete(destinationAddrs);
}*/
 function initialize() {
  var sourceAddrs = document.getElementById('name');
  var destinationAddrs = document.getElementById('toname');
  var autocomplete = new google.maps.places.Autocomplete(sourceAddrs);
  var autocomplete = new google.maps.places.Autocomplete(destinationAddrs);
  }
  
//google.maps.event.addDomListener(window, 'load', initialize);
 
    $(document).ready(function(){
      $("#schLoadingDiv").hide();
      $("#success_message").hide();
      $("#error_message").hide();
      $("#drop_point_details").hide();

      $("#scheduleRideBtn").click(function(){
        //$("#drop_point_details").show();
        //$('#myModal').modal('show'); 
        window.location.href = baseURL+"show-ride-address";  
      });  
      (function(window, google, address) {
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode({
    'address': address
  }, function(results, status) {
    var options = {
        center: results[0].geometry.location,
        zoom: 5
      },
      element = document.getElementById('map-canvas')
    var map = new google.maps.Map(element, options)
  });
}(window, window.google, "98107"));
      

       var geocoder = new google.maps.Geocoder();
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(successFunction, errorFunction);
  } 
  //Get the latitude and the longitude;
  function successFunction(position) {
      var lat = position.coords.latitude;
      var lng = position.coords.longitude;
      codeLatLng(lat, lng)
  }

  function errorFunction(){
    alert("Geocoder failed");
  }

  function initialize() {
    geocoder = new google.maps.Geocoder();
  }

  function codeLatLng(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
      console.log(results)
        if (results[1]) {         
          for (var i=0; i<results[0].address_components.length; i++) {
          for (var b=0;b<results[0].address_components[i].types.length;b++) {
            if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                //this is the object you are looking for
                city= results[0].address_components[i];
                break;
              }
          }
        }
        if (confirm("Do you want get current location !")) {
            $("#name").val(results[0].formatted_address);
        }

        } else {
          alert("No results found");
        }
      } else {
        alert("Geocoder failed due to: " + status);
      }
    });
  }

  function initialize_source() {
        var name = document.getElementById('name');
        var toname = document.getElementById('toname');
        var autocomplete = new google.maps.places.Autocomplete(name);
        var autocomplete = new google.maps.places.Autocomplete(toname);
         // google.maps.event.addDomListener(window, 'load', initialize_source);
        }
         google.maps.event.addDomListener(window, 'load', initialize_source);


        /*$("#scheduleRideBtn").click(function(){
           // $("#loadingDiv").show()
            var data = [];
            var name = $("#name").val();
            var toname = $("#toname").val();           
            if(toname != "" && name != "")
            {
              $("#schLoadingDiv").show();
              $.ajax({
                type:"POST",
                url:baseURL+"frontend/DriverController/request_to_schedule_ride",
                data:{
                  'name':"scheduleRide"
                },
                dataType:"json",
                async:false,          
                success:function(response){
                  if(response.status == true){
                    $("#success_message").show();  
                    $("#schLoadingDiv").hide();
                    //window.location();
                    //$("#loadingDiv").hide();                           
                  }
                  else
                  {
                    $("#error_message").show();
                    //$("#loadingDiv").hide();
                    $("#schLoadingDiv").hide();
                  }
                }
              });
            }
            else
            {
             // $("#success_message").html(response.message); 
              alert("Please enter source & destination address.");
            }
        });*/
    });


</script>
 
 <script type="text/javascript">
        function initMap() {
          var directionsService = new google.maps.DirectionsService;
          var directionsDisplay = new google.maps.DirectionsRenderer;
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: {lat: 37.09024, lng: -95.712891}
          });
          directionsDisplay.setMap(map);
          var onChangeHandler = function() {
            calculateAndDisplayRoute(directionsService, directionsDisplay);
          };
          document.getElementById('name').addEventListener('change', onChangeHandler);
          document.getElementById('toname').addEventListener('change', onChangeHandler);
          document.getElementById('getAddress').addEventListener('click', onChangeHandler);
        }
        if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (p) {
            var LatLng = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);
            var mapOptions = {
                center: LatLng,
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            document.getElementById("current_lat").value = p.coords.latitude.toFixed(6);
            document.getElementById("current_lng").value = p.coords.longitude.toFixed(6);
        });
       } else {
        alert('Geo Location feature is not supported in this browser.');
      }

        function calculateAndDisplayRoute(directionsService, directionsDisplay) {
          directionsService.route({
            origin: document.getElementById('name').value,
            destination: document.getElementById('toname').value,
            travelMode: 'DRIVING'             
          }, function(response, status) {
            if (status === 'OK') {
              directionsDisplay.setDirections(response);
            } else {
             // window.alert('Directions request failed due to ' + status);
            }
          });
        }
      </script>  
    <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_API_KEY; ?>&callback=initMap&region=USA&v=3.exp&sensor=false&libraries=places">
</script> 
 <?php $this->load->view('user/footer'); ?>
