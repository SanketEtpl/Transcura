<?php $this->load->view('user/header'); ?>
<div class="about-page" id="idabout">
  	<div class="theme-breadcrumb pad-50">
		<div class="theme-container container ">
			<div class="row">
				<div class=" col-md-12">
					<div class="title-wrap">
						<h2 class="section-title no-margin"><?php if(!empty($aboutData)){ echo $aboutData[0]['page_title']; } else { echo ''; } ?></h2>
						<p class="fs-16 no-margin"><?php if(!empty($aboutData)){ echo $aboutData[0]['sub_title']; } else { echo ''; } ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="about-wrap">
			<div class="theme-container container">
				<div class="row">
					<div class="col-md-12">
						<div class="about-us pt-10">							
							<?php if(!empty($aboutData)){ echo $aboutData[0]['page_detail']; } else { echo ''; } ?>
						</div>
					</div>					
				</div>
			</div>
	</section>
  </div>
  <?php $this->load->view('user/footer'); ?>