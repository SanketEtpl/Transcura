<?php $this->load->view('user/header'); ?>
<div class="blog_main innerpages">
  <div class="theme-breadcrumb pad-50">
    <div class="theme-container container ">
      <div class="row">
        <div class=" col-md-12">
          <div class="title-wrap">
            <h2 class="section-title no-margin"><?php if(!empty($blogs)) { echo $blogs[0]['page_title']; } else { echo ''; } ?></h2>
          </div>
        </div>
      </div>
    </div>
  </div>
<div class="inner-wrapper news">
  <div class="container">
   <?php if(!empty($blogs)) { echo $blogs[0]['page_detail']; } else { echo ''; } ?>
  </div>
</div>

 </div>   
 <?php $this->load->view('user/footer'); ?>