<!DOCTYPE html>
<html lang="en">
    <head>
		<title>Transcura</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<link rel="shortcut icon" href="<?php echo base_url(); ?>images/res-icon.png">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />		
		<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css"/> -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome/css/font-awesome.css" type="text/css"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/stylesheet.css" type="text/css"/>
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.3/jquery.timepicker.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap-slider.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css"/>
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/intlTelInput.css">
  		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/telphone.css">
		<link type="text/css" href="<?php echo base_url(); ?>assets/css/media.css" rel="stylesheet"/>	

			
    </head>
    <body>
		<div class="wrapper">
			<header>
				<div class="navbar_header">
					<div class="container">
						<div class="row">
							<div class="logo_info">
								<div class="col-md-4 col_md_logo">
									<div class="logo_bar">
										<h1>
											<a href="<?php echo base_url();?>user">
												<img src="<?php if(!empty($footer[0])){ echo base_url().$footer[0]['logo'];} else { echo ''; } ?>" class="img-responsive" alt="transcura logo"/>
											</a>
										</h1>
									</div>
								</div>
								<div class="col-md-8 col_md_cont <?php if(!$this->session->userdata('user_id')) echo 'without_login'; else echo ''; ?>">
									<?php if($this->session->userdata('user_id')) {  ?>
									<div class="logg_sec">
										<div class="bs-example">
											<div class="dropdown">							    		
												<a data-target="#" href="#" data-toggle="dropdown" class="dropdown-toggle">
													<span class="user_ico"><i class="fa fa-user" aria-hidden="true"></i></span>
													 <span class="user_nme">Welcome</br><?php list($firstName) = explode(' ',$this->session->userdata('fullName')); echo $firstName; ?></span> 
												</a>
												<ul class="dropdown-menu">
													<?php 
													$userType = $this->session->userdata('userType');
													if($userType == 2) { ?>
													<li><a href="<?php echo base_url()?>my-dashboard"><i class="fa fa-user" aria-hidden="true"></i>NEMT profile</a></li>
													<li><a href="<?php echo base_url()?>nemt-setting"><i class="fa fa-cog" aria-hidden="true"></i>Settings</a></li>
													<?php }elseif($userType == 5){ ?>
													<li><a href="<?php echo base_url()?>driver-my-dashboard"><i class="fa fa-user" aria-hidden="true"></i>Driver profile</a></li>
													<li><a href="<?php echo base_url()?>driver-setting"><i class="fa fa-cog" aria-hidden="true"></i>Settings</a></li>
													<?php } ?>
													<li><a href="<?php echo base_url()?>user-logout"><i class="fa fa-sign-out" aria-hidden="true"></i>Log-out</a></li>
												</ul>   
											</div>
										</div>
									</div>
									<?php } ?>	
									<div class="mail_info">
										<ul>
											<li class="fullwrapp"><a href="javascript:void(0)"><i class="fa fa-phone font_l" aria-hidden="true"></i>&nbsp;|&nbsp;<?php if(!empty($footer[0])){ echo $footer[0]['company_phone']; } else { echo ''; } ?></a></li>
											<li class="fullwrapp"><a href="mailto:info@transcura.com"><i class="fa fa-envelope font_l" aria-hidden="true"></i>&nbsp;|&nbsp;<?php if(!empty($footer[0])){ echo $footer[0]['office_email']; } else { echo ''; } ?></a></li>
											<?php if(!$this->session->userdata('user_id')) {  ?>
											<li><button type="button" class="btn btn_nav" data-toggle="modal" id="regBtn">Register</button></li>
											<li><button type="button" id="loginBtn" class="btn btn_nav">Login</button></li>
											<?php } ?>
										</ul>
									</div>
									
								</div>
								<div class="col-md-8 col_md_cont_sec">
									<div class="mail_info">
										<div class="main_info_cont_f"><a href="<?php if(!empty($footer[0])){ echo $footer[0]['company_phone']; } else { echo ''; } ?>"><i class="fa fa-phone font_l" aria-hidden="true"></i>&nbsp;|&nbsp;<?php if(!empty($footer)){ echo $footer[0]['company_phone'];} else { echo ''; } ?></a>
										</div>
										<div class="main_info_cont_s"><a href="mailto:info@transcura.com"><i class="fa fa-envelope font_l" aria-hidden="true"></i>&nbsp;|&nbsp;<?php if(!empty($footer[0])){ echo $footer[0]['office_email']; } else { echo ''; } ?></a>
										</div>
										<div class="main_info_cont_t">
											<div class="btn_btn_visible">
												<button id="regBtn" class="btn btn_na" data-toggle="modal">Register</button>														
											</div>
											<div class="btn_btn_hidden">
												<button type="button" id="loginBtn" class="btn btn_nav">Login</button>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="navbar_section">
								<div class="col-md-12">
									<div class="navbar navbar_sec">
										<ul>
											<li><a href="<?php echo base_url(); ?>user">Home</a></li>
											<li><a href="<?php echo base_url(); ?>about-us">About Us</a></li>
											<li><a href="<?php echo base_url(); ?>services">Services</a></li>
											<li><a href="<?php echo base_url(); ?>contact-us">Contact Us</a></li>
											<!-- <li><a href="<?php //echo base_url(); ?>news">News</a></li>
											<li><a href="<?php //echo base_url(); ?>blogs">Blogs</a></li> -->

											<!-- <li><a href="<?php echo base_url(); ?>restaurants">Restaurants</a></li>  -->
										</ul>
								    </div>
								    <div id="showmenu">
								    <span id="menu-toggle"><i class="fa fa-bars" aria-hidden="true"></i></span>
								    <div class="tab_menu" id="mobileview">
								        <ul>
									       	<li><a href="<?php echo base_url(); ?>user">Home</a></li>
									        <li><a href="<?php echo base_url(); ?>about-us">About us</a></li>
									        <li><a href="<?php echo base_url(); ?>services">Services</a></li>
									        <li><a href="<?php echo base_url(); ?>contact-us">Contact us</a></li>
									        <li><a href="<?php echo base_url(); ?>news">News</a></li>
									        <li><a href="<?php echo base_url(); ?>blogs">Blogs</a></li>
								        </ul>
							        </div>
							    </div>
						    </div>
						</div>
					</div>
		 		</div>
			</div>
		</header>	
			

						
