<?php $this->load->view('user/header'); ?>
<?php $this->load->view("frontDrivers/driverHeader"); ?>
<style>
  #map {
    height: 300px;
    width: 1000px;
  }
</style>      
<div class="contact_main innerpages">
  <div class="theme-breadcrumb pad-50">
    <div class="theme-container container ">
      <div class="row">
        <div class=" col-md-12">
          <div class="title-wrap">
            <h2 class="section-title no-margin"><?php if(!empty($contactUs)) { echo $contactUs[0]['page_title']; } else { echo ''; } ?></h2>
            <p class="fs-16 no-margin"><?php if(!empty($contactUs)) { echo $contactUs[0]['sub_title']; } else { echo ''; } ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="inner-wrapper contact-us">
    <div class="container">
      <!-- <script async defer
      src="https://maps.googleapis.com/maps/api/js?key=<?php// echo GOOGLE_API_KEY; ?>&callback=initMap&region=USA&v=3.exp&sensor=false&libraries=places">
      </script> -->
      <div id="schLoadingDiv" style="position: fixed;z-index: 999;background-color: rgba(0, 0, 0, 0.35);width: 100%;height: 100%;top: 0;text-align: center;left:0;">
          <img id="loader-img" alt="loading" src="<?php echo base_url(); ?>assets/img/loading.gif" align="center" style="margin: 0 auto;width: 60px;margin-top: 218px;
        "/>
        </div> 
                 
      <div class="row">  
        <div class="alert alert-success alert-dismissable" id="success_message" style="display:none">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>          
          <strong>Success!</strong> Your trip request has been send to the admin.
        </div>
        <div class="alert alert-warning alert-dismissable" id="error_message" style="display:none">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Fail!</strong> Your trip request has been failed .
        </div>

        <div class="col-sm-12 col-md-6">
          <h2>Search address</h2>
          <div class="forms">
            <form name="frm_contact_us" action="<?php echo base_url()."frontend/DriverController/get_address"; ?>" method="POST">
            <!-- <form name="frm_contact_us" method="POST" action="<?php// echo base_url(); ?>submit-contact-us"> -->
              <div class="controls field">                                  
                <div class="row">
                  <div class="col-md-6">                  
                   <!--  <div class="form-group">
                      <label for="contact_name">From source point</label>
                      <input id="name" type="text" name="name" value="<?php echo $name; ?>" class="form-control name" placeholder="From name">
                      <div class="help-block with-errors"></div>                      
                    </div> -->
                    
                     <div class="form-group">
                      <label for="contact_name">From source point</label>
                      <input id="name" type="text" name="name" value="<?php echo set_value('name'); ?>" class="form-control name" placeholder="Enter location of source address" maxlength="255">
                      <div class="help-block with-errors"></div>      
                    </div>  
                    <div class="form-group">
                      <label for="contact_name">To destination point</label>
                      <input id="toname" type="text" name="toname" value="<?php echo set_value('toname'); ?>" class="form-control name" placeholder="Enter location of destination address" maxlength="255">
                      <div class="help-block with-errors"></div>      
                    </div>                
                  </div>
                </div>                  
              </div>
              <div class="row">                               
                <div class="col-md-12 submitbtn">
                  <input type="hidden" id="current_lat" name="current_lat">
                  <input type="hidden" id="current_lng" name="current_lng">
                  <input id="getAddress" type="button" name="back" value="Get address"/>

                  <input id="scheduleRideBtn" type="button" name="schedule_ride" value="Schedule ride"/>
                   <!-- <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button> -->
                    <!-- <button type="button" name="submit" id="submit">Submit</button> -->
                </div>
              </div>                                 
          </form>
        </div>
      </div>
    </div>
    <br /><br /> 
     <div class="col-sm-12" id="drop_point_details">
        <div class="table-responsive">
          <table class="table table-bordered table-striped dataTable" id="listAddress" role="grid" aria-describedby="example1_info">
            <thead>
              <tr role="row">
                <th width="25%">Destination address</th>
                <th width="25%">From address</th> 
                <th width="25%">To address</th>             
                <th width="25%">Current Lat & lng </th>
                <th width="25%">Destination lat & lng</th>
                <th width="25%">Distance</th>
                <th width="25%">Duration</th>
              </tr>        
            </thead>
            <tbody>         
            </tbody>
          </table>
        </div>
      </div>
    <div id="map"></div>  
  </div>
</div>
</div> 
  <!-- Modal -->
  <div class="modal address-show fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><span class="jsonName"></span></h4>
        </div>
        <div class="modal-body">
          <h4 class="text-center addrs">ADDRESS OF HEALTHCARE</h4>
      <div class="bluebg">
       <div class="form-group" >
        <label class="col-md-12" for="lblname">Name</label> 
        <span class="jsonName col-md-12"></span>               
       </div>  
       <div class="form-group" >
        <label for="lblname" class="col-md-3">Address</label> 
        <span id="jsonAddress" class="col-md-9"></span>               
       </div>  
        <div class="form-group" >
        <label for="lblname" class="col-md-3">Mobile no</label> 
        <span id="jsonPhone" class="col-md-9"></span>               
       </div>  
      </div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="req_btn comp_btn" id="popContinue">CONTINUE</button>
        </div>
      </div>
      
    </div>
  </div>

<script src="http://localhost/AppTech/js/jquery.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places&key=<?php //echo GOOGLE_API_KEY; ?>"></script>-->
<script type="text/javascript">
function initialize() {
var sourceAddrs = document.getElementById('name');
var destinationAddrs = document.getElementById('toname');
var autocomplete = new google.maps.places.Autocomplete(sourceAddrs);
var autocomplete = new google.maps.places.Autocomplete(destinationAddrs);
}
//google.maps.event.addDomListener(window, 'load', initialize);
 
    $(document).ready(function(){
      $("#schLoadingDiv").hide();
      $("#success_message").hide();
      $("#error_message").hide();


      $("#scheduleRideBtn").click(function(){
        window.location.href = baseURL+"show-ride-address";  
      });  

        /*$("#scheduleRideBtn").click(function(){
           // $("#loadingDiv").show()
            var data = [];
            var name = $("#name").val();
            var toname = $("#toname").val();           
            if(toname != "" && name != "")
            {
              $("#schLoadingDiv").show();
              $.ajax({
                type:"POST",
                url:baseURL+"frontend/DriverController/request_to_schedule_ride",
                data:{
                  'name':"scheduleRide"
                },
                dataType:"json",
                async:false,          
                success:function(response){
                  if(response.status == true){
                    $("#success_message").show();  
                    $("#schLoadingDiv").hide();
                    //window.location();
                    //$("#loadingDiv").hide();                           
                  }
                  else
                  {
                    $("#error_message").show();
                    //$("#loadingDiv").hide();
                    $("#schLoadingDiv").hide();
                  }
                }
              });
            }
            else
            {
             // $("#success_message").html(response.message); 
              alert("Please enter source & destination address.");
            }
        });*/
    });


</script>
 <?php $this->load->view('user/footer'); ?>
