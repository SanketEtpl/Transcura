<?php $this->load->view('user/header'); ?>
<!--registration form-->
<div class="loginsignup">
	<div class="logininner">
		<!--registration form-->
		<div class="signupformmain">	
			<div class="signupform-inner">				
				<div class="loginform">
					<div class="titlestrip">
						<div class="ttl text-center">
							<h3 class="text-uppercase">Registration</h3>
						</div>
					</div>	<?php //echo base_url();set-registration ?>
					<form id="signupform" class="form-horizontal" role="form" method="POST" action="" enctype="multipart/form-data">
						<div class="message"></div>
						<div id="frm-first">
							<div class="row-comm">
								<div class="form-group">
									<label for="usertype" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />User type</label>
									<select class="form-control" id="usertype" name="user_type">
										<option value="" diable seleted hidden>select user-type</option>
										<?php 
										if(!empty($userData)) {
											foreach($userData as $key => $value) { ?>
											<option value="<?php echo $value['role_id']; ?>"<?php echo set_select('user_type', $value['role_id'], FALSE); ?>><?php echo $value['role_name']; ?></option>
										<?php } } else { ?>	
										<option value=""></option>
										<?php } ?>					
									</select>
									<span class="error_usertype"></span>
								</div>		
								<div class="form-group">
									<label for="fullname" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Full name</label>
									<input type="text" class="form-control" id="fullname" value="<?php echo set_value('full_name'); ?>" name="full_name" placeholder="Full name" maxlength="100" autocomplete="off">
								</div>	
							</div>
							<div class="row-comm">
								<div class="form-group">
									<label for="dob" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Date of birth</label>
									<input type="text" name="date_of_birth" value="<?php echo set_value('date_of_birth'); ?>" id="date_of_birth" class="form-control date-picker" placeholder="Date of birth" autocomplete="off" readonly>
									<!-- <i class="fa fa-calendar" aria-hidden="true"></i> -->
									
								</div>
								<div class="form-group">
									<label for="username" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />User name</label>
									<input type="text" class="form-control" id="username" value="<?php echo set_value('username1'); ?>" onBlur="checkusername()" name="username1" placeholder="User name" maxlength="100" autocomplete="off">
									
								</div>	
							</div>
							<div class="row-comm">
								<div class="form-group">
									<label for="emailaddrss" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/Email-id.png" alt="" />Email address</label>
									<input type="email" name="email" value="<?php echo set_value('email'); ?>" class="form-control" id="emailaddrss" onBlur="checkemail()" placeholder="Email address" maxlength="100" autocomplete="off">
									
								</div>	
								<div class="phnnumbernew">
									<label for="phnnumber required" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-6.png" alt="" />Phone number</label>
									<input type="tel" class="form-control" value="<?php echo set_value('phone_no'); ?>" id="phnnumber" name="phone_no" placeholder="Phone number" maxlength="20" autocomplete="off">
								</div>	
								
							</div>
							<div class="row-comm">
								<div class="form-group">
									<label for="pass" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-5.png" alt="" />Password</label>
									<input type="password" class="form-control" id="pass" name="password" placeholder="Password" maxlength="100" autocomplete="new-password" onblur="this.setAttribute('readonly', 'readonly');" onfocus="this.removeAttribute('readonly');" readonly>
								</div>		
								<div class="form-group">
									<label for="conpass" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-5.png" alt="" />Confirm password</label>
									<input type="password" class="form-control" id="conpass" name="confirm_pwd" placeholder="Confirm password" maxlength="100" autocomplete="new-password" onblur="this.setAttribute('readonly', 'readonly');" onfocus="this.removeAttribute('readonly');" readonly>							
								</div>	
							</div>
							<div class="row-comm">						 						
								<div class="form-group contrwrapp">
									<!-- Button -->
									<div class="controls">
										<input id="btn-proceed" type="button" name="btn_first" value="Proceed" class="btn btn-block btn-primary text-uppercase"/>							 
									</div>
								</div>
							</div>
							<!-- <div class="form-group"> -->
									
								<!-- </div> -->
						</div> 
						<div id="frm-second">
							<div class="row-comm">
								<div class="form-group">
									<label for="country" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/proc-icon1.png" alt="" />Country</label>
									<select class="form-control" name="country" id="country">
										<option value="" class="selecttxt" disable selected hidden>Select country</option>
										<?php 
										if(!empty($countryData)){
										foreach($countryData as $country_name) {
										?>
										<option value="<?php echo $country_name['id']; ?>"><?php echo $country_name['country_name']?></option>
										<?php } } else { ?>	
										<option value=""></option>
										<?php } ?>	
									</select>							
								</div>	
								<div class="form-group"> 
									<label for="state" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/proc-icon1.png" alt="" />State</label>
									<select class="form-control" id="state" name="state">
										<option class="selecttxt" value="" disable selected hidden>Select state</option>
									</select>									
								</div>
							</div>
							<div class="row-comm">							
								<div class="form-group">
									<label for="city" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/proc-icon1.png" alt="" />City</label>
									<select class="form-control" id="city" name="city">
										<option class="selecttxt" value="">Select City</option>
									</select>
									<!--<input type="text" name="city" class="form-control" id="city" value="<?php echo set_value('city'); ?>" placeholder="City" maxlength="100" autocomplete="off">-->
									
								</div>		
								<div class="form-group">
									<label for="street" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/proc-icon6.png" alt="" />Street</label>
									<input type="text" name="street" class="form-control" id="street" value="<?php echo set_value('street'); ?>" placeholder="Street" maxlength="255" autocomplete="off">
								</div>
							</div>
							<div class="row-comm">							
								<div class="form-group zip-county">
									<label for="zipcode" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/proc-icon6.png" alt="" />Zip code</label>
									<input type="text" name="zipcode" class="form-control" id="zipcode" value="<?php echo set_value('zipcode'); ?>" placeholder="Zip code" maxlength="15" autocomplete="off">
								</div>
								<div class="form-group" >
									<label for="county" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/proc-icon6.png" alt=""/>County</label>
									<input type="text" name="county" value="<?php echo set_value('county'); ?>" class="form-control" id="county" placeholder="County" maxlength="20" autocomplete="off">													
								</div>	
							</div>
							<!--NEMT user-->							
							<div class="row-comm">
								<div class="row-comm" id="personal_doc_id">
									<div class="form-group">
										<label for="lbl_per_doc" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Personal Documents</label>
										<select class="form-control" id="pers_doc_dd" name="pers_doc_dd">
											<option value="" diable seleted hidden>Select personal documents</option>
											<option value="1">YES</option>
				                     		<option value="2">NO</option>				
										</select>
										<span class="error_usertype"></span>
									</div>									
								</div>								
								<div id="DRIVER">
									<div class="select-nemt">									
										<div class="form-group">
											<label for="emercontact" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/proc-icon3.png" alt="" />medical insurance id</label>
											<input type="text" class="form-control" name="insurance_id" value="<?php echo set_value('insurance_id'); ?>" id="insurance_id" placeholder="Insurance id" maxlength="20" autocomplete="off">								
										</div>								
									</div>
									<div id="persoal_doc_view">
										<div class="form-group">
											<label for="ssc" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Social security card</label>
											<div class="brows-doc">
											    <input class="file" type="file" name="social_security_card_doc" id="social_security_card_doc">
											    <div class="input-group col-xs-12">
											    	<input type="text" class="form-control" disabled placeholder="Social security card">
											      	<span class="input-group-btn">
											        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
											      	</span>
											    </div>
										 	</div>
										</div>	

										<div class="select-nemt">
											<div class="form-group">
												<label for="driver_license" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Driver license</label>
												<div class="brows-doc">
												    <input class="file" type="file" name="driving_license_doc" id="driving_license_doc" value="<?php echo set_value('driving_license_doc'); ?>" >
												    <div class="input-group col-xs-12">
												    	<input type="text" class="form-control" disabled placeholder="Driving license documents">
												      	<span class="input-group-btn">
												        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
												      	</span>
												    </div>
											 	</div>
											</div>
											<div class="form-group" id="dledShow_date_id">
											
													<label for="driving_license" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Driving license expiry date</label>
													<input type="text" name="driving_license_expiry_date" value="<?php echo set_value('driving_license_expiry_date'); ?>" id="driving_license_expiry_date" class="form-control date-picker pointer fourYearDate" placeholder="Driving license expire date" autocomplete="off" readonly>
											
											</div>																										
										</div>
										
										<div class="select-nemt row-comm">
											<div class="form-group">
												<label for="drug_test" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />5-Panel drug test results</label>
												<div class="brows-doc">
												    <input class="file" type="file" name="drug_test_doc" id="drug_test_doc" value="<?php echo set_value('drug_test_doc'); ?>" >
												    <div class="input-group col-xs-12">
												    	<input type="text" class="form-control" disabled placeholder="5-Panel drug test results">
												      	<span class="input-group-btn">
												        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
												      	</span>
												    </div>
											 	</div>
											</div>	
											<div class="form-group" id="drug_test_date_id">
												
													<label for="drug_test" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />5-Panel drug test results expiry date</label>
													<input type="text" name="drug_test_expiry_date" value="<?php echo set_value('drug_test_expiry_date'); ?>" id="drug_test_expiry_date" class="form-control date-picker sixMonthDate" placeholder="Drug test expire date" autocomplete="off" readonly>
										
											</div>																									
										</div>	
										<div class="select-nemt">
											<div class="form-group">
												<label for="criminal_back_ground" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Criminal gack ground</label>
												<div class="brows-doc">
												    <input class="file" type="file" name="criminal_bk_doc" id="criminal_bk_doc" value="<?php echo set_value('criminal_bk_doc'); ?>" >
												    <div class="input-group col-xs-12">
												    	<input type="text" class="form-control" disabled placeholder="Criminal gack ground">
												      	<span class="input-group-btn">
												        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
												      	</span>
												    </div>
											 	</div>
											</div>	
											<div class="form-group" id="criminal_bk_date_id">
											
													<label for="criminal_bk" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Criminal back ground expiry date</label>
													<input type="text" name="criminal_bk_expiry_date" value="<?php echo set_value('criminal_bk_expiry_date'); ?>" id="criminal_bk_expiry_date" class="form-control date-picker oneYearDate" placeholder="Criminal Back Ground expiry date" autocomplete="off" readonly>
									
											</div>																									
										</div>	
										<div class="select-nemt">
											<div class="form-group">
												<label for="mvr" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Motor vehicle record</label>
												<div class="brows-doc">
												    <input class="file" type="file" name="motor_vehicle_details_doc" id="motor_vehicle_details_doc">
												    <div class="input-group col-xs-12">
												    	<input type="text" class="form-control" disabled placeholder="Motor vehicle record">
												      	<span class="input-group-btn">
												        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
												      	</span>
												    </div>
											 	</div>
											</div>	
											<div class="form-group" id="motor_vehicle_date_id">
											
													<label for="motor_vd" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Motor vehicle record expiry date</label>
													<input type="text" name="motor_vehile_record_expiry_date" value="<?php echo set_value('motor_vehile_record_expiry_date'); ?>" id="motor_vehile_record_expiry_date" class="form-control date-picker oneYearDate" placeholder="Motor vehicle record expiry date" autocomplete="off" readonly>
												
											</div>																									
										</div>		
										<div class="select-nemt">
											<div class="form-group">
												<label for="aca" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Act 33 & 34 clearance</label>
												<div class="brows-doc">
												    <input class="file" type="file" name="act_33_34_clearance_doc" id="act_33_34_clearance_doc">
												    <div class="input-group col-xs-12">
												    	<input type="text" class="form-control" disabled placeholder="Act 33 & 34 clearance">
												      	<span class="input-group-btn">
												        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
												      	</span>
												    </div>
											 	</div>
											</div>	
											<div class="form-group" id="act_33_34_clearance_date_id">
												
													<label for="ac_vd" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Act 33 & 34 clearance expiry date</label>
													<input type="text" name="act_33_34_clearance_expiry_date" value="<?php echo set_value('act_33_34_clearance_expiry_date'); ?>" id="act_33_34_clearance_expiry_date" class="form-control date-picker oneYearDate" placeholder="Act 33 & 34 clearance expiry date" autocomplete="off" readonly>
												
											</div>																									
										</div>
									</div>
								</div>
								<div class="row-comm" id="vehicle_doc_id">
									<div class="form-group">
										<label for="vehicleregiDoc" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Vehicle registration</label>
										<select class="form-control" id="vehicle_reg_documents_id" name="vehicle_reg_documents_id">
											<option value="" diable seleted hidden>Select vehicle registration</option>
											<option value="1">Yes</option>
											<option value="2">NO</option>															
										</select>									
									</div>											
								</div>
								<div id="vehicle_registration_div_id">
									<div class="select-nemt">
										<div class="form-group">
											<label for="viLabel" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Vehicle inspections</label>
											<div class="brows-doc">
											    <input class="file" type="file" name="vehicle_inspection_doc" id="vehicle_inspection_doc">
											    <div class="input-group col-xs-12">
											    	<input type="text" class="form-control" disabled placeholder="vehicle inspection">
											      	<span class="input-group-btn">
											        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
											      	</span>
											    </div>
										 	</div>
										</div>	
										<div class="form-group" id="vehicle_inspection_date_id">
										
												<label for="vied" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Vehicle inspection expiry date</label>
												<input type="text" name="vehicle_inspection_expiry_date" value="<?php echo set_value('vehicle_inspection_expiry_date'); ?>" id="vehicle_inspection_expiry_date" class="form-control date-picker oneYearDate" placeholder="vehicle inspection expiry date" autocomplete="off" readonly>
											
										</div>
										<div class="form-group">
											<label for="vmrLabel" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Vehicle maintenance records</label>
											<div class="brows-doc">
											    <input class="file" type="file" name="vehicle_maintainance_record_doc" id="vehicle_maintainance_record_doc">
											    <div class="input-group col-xs-12">
											    	<input type="text" class="form-control" disabled placeholder="Vehicle maintainance records">
											      	<span class="input-group-btn">
											        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
											      	</span>
											    </div>
										 	</div>
										</div>																										
									</div>	

									<div class="select-nemt">
										
										<!-- <div class="form-group">
											<label for="lbl_vehicle_type" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Vehicle type, year, make and model</label>
											<input type="text" class="form-control" id="vehicle_details" value="<?php echo set_value('vehicle_details'); ?>" name="vehicle_details" placeholder="Vehicle type, year, make and model" maxlength="255" autocomplete="off">
										</div> -->
										<!-- <div class="row-comm" id="vehicle_maintaince_record_date_id">
											<div class="form-group">
												<label for="vied" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Vehicle maintainance record expiry date</label>
												<input type="text" name="vehicle_maintainance_record_expiry_date" value="<?php echo set_value('vehicle_maintainance_record_expiry_date'); ?>" id="vehicle_maintainance_record_expiry_date" class="form-control date-picker oneYearDate" placeholder="vehicle maintainance record expiry date" autocomplete="off" readonly>
											</div>
										</div>-->
									</div>	
									<div class="select-nemt">
										<div class="form-group">
											<label for="vilLabel" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Vehicle insurance</label>
											<div class="brows-doc">
											    <input class="file" type="file" name="vehicle_insurance_record_doc" id="vehicle_insurance_record_doc">
											    <div class="input-group col-xs-12">
											    	<input type="text" class="form-control" disabled placeholder="Vehicle insurance records">
											      	<span class="input-group-btn">
											        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
											      	</span>
											    </div>
										 	</div>
										</div>	
										<div class="form-group" id="vehicle_insurance_record_date_id">
											
												<label for="vied1" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Vehicle insurance expiry date</label>
												<input type="text" name="vehicle_insurance_expiry_date" value="<?php echo set_value('vehicle_insurance_expiry_date'); ?>" id="vehicle_insurance_expiry_date" class="form-control date-picker oneYearDate" placeholder="vehicle insurance expiry date" autocomplete="off" readonly>
										
										</div>																									
									</div>	
									<div class="select-nemt">
										<div class="form-group">
											<label for="vrlable" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Vehicle registration</label>
											<div class="brows-doc">
											    <input class="file" type="file" name="vehicle_registration_doc" id="vehicle_registration_doc">
											    <div class="input-group col-xs-12">
											    	<input type="text" class="form-control" disabled placeholder="Vehicle registration doc">
											      	<span class="input-group-btn">
											        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
											      	</span>
											    </div>
										 	</div>
										</div>	
										<div class="form-group" id="vehicle_registration_date_id">
										
												<label for="vied1" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Vehicle registration expiry date</label>
												<input type="text" name="vehicle_registration_expiry_date" value="<?php echo set_value('vehicle_registration_expiry_date'); ?>" id="vehicle_registration_expiry_date" class="form-control date-picker oneYearDate" placeholder="vehicle registration expiry date" autocomplete="off" readonly>
										
										</div>																																				
									</div>									
								</div>

								<div class="row-comm" id="own_vehicle_id">
									<div class="form-group">
										<label for="lbl_ovb" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Do you operate your own vehicle for your own business? </label>
										<select class="form-control" id="own_vehicle_dd" name="own_vehicle_dd">
											<option value="" diable seleted hidden>Select</option>
											<option value="1">YES</option>
				                     		<option value="2">NO</option>				
										</select>
										<span class="error_usertype"></span>
									</div>									
								</div>
								<div id="own_bussiness_show">
									<div class="row-comm">
										<div class="form-group">
											<label for="lbl_type_of_sevices" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon1.png" alt="" />Types of service for own bussiness</label>
											<select class="form-control" name="type_of_sevices" id="type_of_sevices">
												<option value="" class="selecttxt" disable selected hidden>Select</option>
												<?php 
												if(!empty($typeOfServices)){
												foreach($typeOfServices as $services) {
												?>
												<option value="<?php echo $services['type_id']; ?>"><?php echo $services['type_title']?></option>
												<?php } } else { ?>	
												<option value=""></option>
												<?php } ?>	
											</select>							
										</div>
										<div class="form-group">
											<label for="ownvrlable" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Vehicle registration</label>
											<div class="brows-doc">
											    <input class="file" type="file" name="own_vehicle_reg_doc" id="own_vehicle_reg_doc">
											    <div class="input-group col-xs-12">
											    	<input type="text" class="form-control" disabled placeholder="Vehicle registration doc">
											      	<span class="input-group-btn">
											        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
											      	</span>
											    </div>
										 	</div>
										</div>														
									</div>
									<div class="row-comm">
										<div class="form-group">
											<label for="lbl_insu_pol" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Insurance policy</label>
											<div class="brows-doc">
											    <input class="file" type="file" name="insurance_policy_doc" id="insurance_policy_doc">
											    <div class="input-group col-xs-12">
											    	<input type="text" class="form-control" disabled placeholder="Insurance policy">
											      	<span class="input-group-btn">
											        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
											      	</span>
											    </div>
										 	</div>
										</div>
										<div class="form-group">
											<label for="lbl_inspection" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Inspection</label>
											<div class="brows-doc">
											    <input class="file" type="file" name="inspection_doc" id="inspection_doc">
											    <div class="input-group col-xs-12">
											    	<input type="text" class="form-control" disabled placeholder="Inspection">
											      	<span class="input-group-btn">
											        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
											      	</span>
											    </div>
										 	</div>
										</div>														
									</div>
									<div class="row-comm">
										<div class="form-group">
											<label for="lbl_tax_id" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Tax ID or EIN number</label>
											<div class="brows-doc">
											    <input class="file" type="file" name="tax_id_or_ein_no" id="tax_id_or_ein_no">
											    <div class="input-group col-xs-12">
											    	<input type="text" class="form-control" disabled placeholder="Tax ID or EIN number">
											      	<span class="input-group-btn">
											        	<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-paperclip"></i></button>
											      	</span>
											    </div>
										 	</div>
										</div>	
										<div class="form-group">
											<label for="lbl_own_bs_vehicle_details" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Vehicle type, year, make and model</label>
											<input type="text" class="form-control vehicle_details" id="own_buss_vehicle_details" value="<?php echo set_value('own_buss_vehicle_details'); ?>" name="own_buss_vehicle_details" placeholder="Vehicle type, year, make and model" maxlength="255" autocomplete="off">
										</div>																										
									</div>
								</div>
								<div class="row-comm" id="company_vehicle_id">
									<div class="form-group">
										<label for="lbl_comp_vehic" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Do you operate company vehicle? </label>
										<select class="form-control" id="company_vehicle_dropdown" name="company_vehicle_dropdown">
											<option value="" diable seleted hidden>Select</option>
											<option value="1">YES</option>
				                     		<option value="2">NO</option>				
										</select>
										<span class="error_usertype"></span>
									</div>									
								</div>
								<div class="row-comm" id="company_vehicle_show">
									<div class="row-comm">
						            	<div class="form-group">
											<label for="lbl_trans_comp1" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Name of transportation company</label>
											<input type="text" class="form-control name" id="transport_comp" value="<?php echo set_value('transport_comp'); ?>" name="transport_comp" placeholder="Name of transportation company" maxlength="100" autocomplete="off">
										</div>	
										<div class="form-group">
											<label for="lbl_vehicle_type" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Vehicle type, year, make and model</label>
											<input type="text" class="form-control vehicle_details" id="vehicle_details" value="<?php echo set_value('vehicle_details'); ?>" name="vehicle_details" placeholder="Vehicle type, year, make and model" maxlength="255" autocomplete="off">
										</div>												
									</div>
									<div class="row-comm">
										<div class="form-group">
											<label for="lbl_com_type_of_sevices" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon1.png" alt="" />Types of service for company vehicle</label>
											<select class="form-control" name="com_veh_type_of_sevices" id="com_veh_type_of_sevices">
												<option value="" class="selecttxt" disable selected hidden>Select</option>
												<?php 
												if(!empty($typeOfServices)){
												foreach($typeOfServices as $services) {
												?>
												<option value="<?php echo $services['type_id']; ?>"><?php echo $services['type_title']?></option>
												<?php } } else { ?>	
												<option value=""></option>
												<?php } ?>	
											</select>							
										</div>																											
									</div>								
								</div>	

							<div class="row-comm">
								<div class="form-group">
									<label for="emercontact" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/proc-icon2.png" alt="" />Emergency contact name</label>
									<input type="text" class="form-control" name="eme_name" value="<?php echo set_value('eme_name'); ?>" id="eme_name" placeholder="Emergency contact name" maxlength="100" autocomplete="off">
								</div>	
								<div class="form-group">
									<label for="emernumber" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/proc-icon7.png" alt="" />Emergency contact number</label>
									<input type="text" class="form-control" name="eme_number" value="<?php echo set_value('eme_number'); ?>" id="eme_number" placeholder="Emergency contact number" maxlength="20" autocomplete="off">
								</div>	
							</div>	
							<div class="row-comm">					
								<div class="form-group">
									<label for="signature" class="text-uppercase required"><img src="<?php echo base_url(); ?>assets/img/proc-icon5.png" alt="" />Are you able to provide signatures</label>
									<select class="form-control" id="signature" name="signature">
										<option value="" disable selected hidden>select signature</option>
										<option value="1" <?php echo set_select('signature', 'yes', FALSE); ?>>Yes</option>
										<option value="0"<?php echo set_select('signature', 'no', FALSE); ?>>No</option>
									</select>									
								</div>	
							</div>
							<div class="row-comm buton-group">								
								<div class="form-group">
									<!-- Button -->
									<div class="controls">
										<input id="btnSubmit" type="button" name="register" value="Sign Up" class="btn btn-block btn-primary text-uppercase">
									    <input id="btn-back" type="button" name="back" value="Previous Form" class="btn btn-block btn-primary text-uppercase" data-toggle="modal" data-target="#signupsuccess" />
									</div>
								</div>
							</div>				
						</div>     
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="loadingDiv" style="position: fixed;z-index: 1;background-color: rgba(0, 0, 0, 0.35);width: 100%;height: 100%;top: 0;text-align: center; display:none">
	<img id="loader-img" alt="loading" src="<?php echo base_url(); ?>assets/img/loading.gif" align="center" style="margin: 0 auto;width: 60px;margin-top: 218px;
"/>
</div>
<?php $this->load->view('user/footer'); ?>

