<?php $this->load->view('user/header');?>
<div class="im_wrap">
	<img src="<?php echo base_url(); ?>assets/img/Untitled-2.png" class="img-responsive img_resp" alt="imgslide"/>
</div>
<div class="img_slider">
   	<div id="carousel-example" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carousel-example" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example" data-slide-to="1"></li>
			<li data-target="#carousel-example" data-slide-to="2"></li>
		</ol>
	  	<div class="carousel-inner">
			<div class="item active">
		  		<a href="#"><img src="<?php echo base_url(); ?>assets/img/Untitled-2.png" class="img-responsive img_resp" alt="imgslide"/></a>
			</div>
			<div class="item">
				<a href="#"><img src="<?php echo base_url(); ?>assets/img/Untitled-2.png" class="img-responsive img_resp" alt="imgslide_first"/></a>
			</div>
			<div class="item">
				<a href="#"><img src="<?php echo base_url(); ?>assets/img/Untitled-2.png" class="img-responsive img_resp" alt="imgslide_third"/></a>
			</div>
		</div>
	  	<a class="left carousel-control" href="#carousel-example" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
	  	</a>
	  	<a class="right carousel-control" href="#carousel-example" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
	  	</a>
	</div>
</div>
<!--section-->
<div class="web_info">
	<div class="container">
	    <div class="row">
		    <div class="web_info_forms">
			    <div class="col-md-7 cols_fo cols_fofirst" id="first_section">
			    	<?php
			    		$i=1;
			    		if(!empty($home_section1)){ 
			    		foreach ($home_section1 as $key => $value) {			    			
			    		?>
			    	<div class="media first_section_content">
						<div class="media-left"><img alt="technology" class="media-object media_img" src="<?php if(!empty($value['fs_icon_image'])) echo $value['fs_icon_image']; else echo "assets/img/no_image.jpg"; ?>"></div>
							<div class="media-body">
								<h3><?php echo $value['fs_title']; ?></h3>
								<?php
									$string = $value['fs_description'];
									if (strlen($string) > 250) {													
										$stringCut = substr($string, 0, 250);
										$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
									}
									echo $string;									
									?>
								 <div class="log_wi_btn view_more"><a href="javascript:void(0)" id="show_more_id_<?php echo $i; ?>">VIEW MORE</a></div>
								 <div class="hide_first_section_content">
									<?php  echo $value['fs_description']; ?>
								</div>
							</div>
						</div>
						<?php $i++; } } ?>									
				</div>				
				<div class="col-md-5 pannel_padd rightform">
					<div class="funct_image">
						<img src="<?php echo base_url(); ?>assets/img/schedule_ride.png" />
					</div>
				</div>					
			</div>
  		</div>
	</div>
	<!--section-->
	<div class="web_inst">
		<div class="container">
  			<div class="row">
		        <div class="about_info">
				    <div class="all_info">
					    <div class="info_header">
							<div class="section-ttl">
								<h1 class="ifo_h"><?php if(!empty($home_section_about[0])){ echo $home_section_about[0]['page_title']; }else{ echo ""; } ?></h1>
								<div class="innerttl">
									<h1><?php if(!empty($home_section_about[0])){ echo $home_section_about[0]['page_title']; }else{ echo ""; } ?></h1>
								</div>
							</div>
						</div>
						<div class="text_ifo">
							<?php if(!empty($home_section_about)){ 
								$string = $home_section_about[0]['page_detail'];
											if (strlen($string) > 250) {													
											$stringCut = substr($string, 0, 700);
											$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
											}
								echo $string; }?>
							</div>
							<div class="about_btn">								
							    <button class="btn about_btn_info" onClick="window.location='<?php echo base_url('about-us');?>'">LEARN MORE</button>
							</div>
					  </div>
				</div>
  			</div>
		</div>
	</div>
	<!--section-->
	<div class="img_back_video">
		<div class="img_video_section">
		  	<div class="img_section">
				<img src="<?php echo base_url(); ?>assets/img/img-01.jpg" class="img-responsive video_img" alt="video img"/>
				<div class="video_sec">
				   	<h1>PAVING THE WAY</h1>
					<video class="video_sec_sc" controls>
					  	<source src="<?php echo base_url(); ?>assets/video/VideoHD.mp4" type="video/mp4"/>
					  	<source src="<?php echo base_url(); ?>assets/video/VideoHD.ogg" type="video/ogg"/>
					  	Your browser does not support HTML5 video.
					</video>
				</div>
		  	</div>
		</div>
	</div>
	
	<!--transportation solution section-->
	<div class="solution_section section-pad" style="display:none;">
		<div class="container">
	 		<div class="row">
	        	<div class="info_solution">
					<div class="section-ttl">
						<h1 class="solu_sec"><?php if(!empty($home_section_about[1])){ echo $home_section_about[1]['page_title']; }else{ echo ""; } ?></h1>
						<div class="innerttl">
							<h1><?php if(!empty($home_section_about[1])){ echo $home_section_about[1]['page_title']; }else{ echo ""; } ?></h1>
						</div>
					</div>
				</div>
				<div class="ifo_slu_pass">
					<p class="text-uppercase"><?php if(!empty($home_section_about[1])){ echo $home_section_about[1]['sub_title']; }else{ echo ""; } ?></p>
				</div>					
			</div>
			<!--tabs-->
			<div class="row">
				<div class="tabsdiv">
					<!--tabs nav-->
					<div class="tabsmenu">
						<ul class="nav nav-tabs nav-menu">
							<li class="active">
							  <a data-toggle="tab" href="#safetyprocess">
								<img alt="safety img" class="img-responsive main_resp" src="<?php if(!empty($home_section1[7])){ echo $home_section1[7]['image_icon_upload']; }else{ echo ""; } ?>">
								<h4><?php if(!empty($home_section1[7])){ echo $home_section1[7]['page_title']; }else{ echo ""; } ?></h4>
							  </a>
							</li>
							<li>
								<a data-toggle="tab" href="#timetracking">
								<img alt="time track" class="img-responsive main_resp" src="<?php if(!empty($home_section1[8])){ echo $home_section1[8]['image_icon_upload']; }else{ echo ""; } ?>">
								<h4><?php if(!empty($home_section1[8])){ echo $home_section1[8]['page_title']; }else{ echo ""; } ?></h4>
								</a>
							</li>
							<li>
							  <a data-toggle="tab" href="#reportsdata">
								<img alt="data" class="img-responsive main_resp" src="<?php if(!empty($home_section1[9])){ echo $home_section1[9]['image_icon_upload']; }else{ echo ""; } ?>">
								<h4><?php if(!empty($home_section1[9])){ echo $home_section1[9]['page_title']; }else{ echo ""; } ?></h4>
							  </a>
							</li>
							<li>
								<a data-toggle="tab" href="#benefitesmultiloading">
								<img alt="loading" class="img-responsive main_resp" src="<?php if(!empty($home_section1[10])){ echo $home_section1[10]['image_icon_upload']; }else{ echo ""; } ?>">
								<h4><?php if(!empty($home_section1[10])){ echo $home_section1[10]['page_title']; }else{ echo ""; } ?></h4>
								</a>
							</li>
							<li>
								<a data-toggle="tab" href="#ontimefeatures">
								<img alt="time scheduling" class="img-responsive main_resp" src="<?php if(!empty($home_section1[11])){ echo $home_section1[11]['image_icon_upload']; }else{ echo ""; } ?>">
								<h4><?php if(!empty($home_section1[11])){ echo $home_section1[11]['page_title']; }else{ echo ""; } ?></h4>
								</a>
							</li>
							<li>
								<a data-toggle="tab" href="#notificationfeatures">
								<img alt="notifiaction" class="img-responsive main_resp" src="<?php if(!empty($home_section1[12])){ echo $home_section1[12]['image_icon_upload']; }else{ echo ""; } ?>">
								<h4><?php if(!empty($home_section1[12])){ echo $home_section1[12]['page_title']; }else{ echo ""; } ?></h4>
								</a>
							</li>
							<li>
								<a data-toggle="tab" href="#notificationfeatures1">
								<img alt="features" class="img-responsive main_resp" src="<?php if(!empty($home_section1[13])){ echo $home_section1[13]['image_icon_upload']; }else{ echo ""; } ?>">
								<h4><?php if(!empty($home_section1[13])){ echo $home_section1[13]['page_title']; }else{ echo ""; } ?></h4>
								</a>
							</li>							
						</ul>
					</div>
					<!--tabs content-->
					<div class="tabscont">
						<div class="tab-content">
							<!--tabs item-->
							<div id="safetyprocess" class="tab-pane fade in active">
								<div class="tbinner">
									<?php 
								if(!empty($home_section1[7])){
									$string = $home_section1[7]['page_detail'];
										echo $string;
									 }else{ echo ""; } ?>
								</div>
							</div>
							<!--tabs item-->
							<div id="timetracking" class="tab-pane fade">
								<div class="tbinner">
									<?php 
								if(!empty($home_section1[8])){
									$string = $home_section1[8]['page_detail'];
										echo $string;
									 }else{ echo ""; } ?>
								</div>								
							</div>			
							<!--tabs item-->
							<div id="reportsdata" class="tab-pane fade">
								<div class="tbinner">
									<?php 
								if(!empty($home_section1[9])){
									$string = $home_section1[9]['page_detail'];
										echo $string;
									 }else{ echo ""; } ?>
								</div>								
							</div>	
							<!--tabs item-->
							<div id="benefitesmultiloading" class="tab-pane fade">
								<div class="tbinner">
									<?php 
								if(!empty($home_section1[10])){
									$string = $home_section1[10]['page_detail'];
										echo $string;
									 }else{ echo ""; } ?>
								</div>								
							</div>	
							<!--tabs item-->
							<div id="ontimefeatures" class="tab-pane fade">
								<div class="tbinner">
									<?php 
								if(!empty($home_section1[11])){
									$string = $home_section1[11]['page_detail'];
										echo $string;
									 }else{ echo ""; } ?>
								</div>								
							</div>	
							<!--tabs item-->
							<div id="notificationfeatures" class="tab-pane fade">
								<div class="tbinner">
									<?php 
								if(!empty($home_section1[12])){
									$string = $home_section1[12]['page_detail'];
										echo $string;
									 }else{ echo ""; } ?>
								</div>								
							</div>	
							<!--tabs item-->
							<div id="notificationfeatures1" class="tab-pane fade">
								<div class="tbinner">
									<?php 
								if(!empty($home_section1[13])){
									$string = $home_section1[13]['page_detail'];
										echo $string;
									 }else{ echo ""; } ?>
								</div>								
							</div>								
						</div>
					</div>
					<!--end tabs content-->
				</div>
			</div>
		</div>
	</div>
	<!--section-->
	<div class="solution_section section-pad">
		<div class="container">
	 		<div class="row">
	        	<div class="info_solution">
					<div class="section-ttl">
						<h1 class="solu_sec"><?php if(!empty($home_section_about[1])){ echo $home_section_about[1]['page_title']; }else{ echo ""; } ?></h1>
						<div class="innerttl">
							<h1><?php if(!empty($home_section_about[1])){ echo $home_section_about[1]['page_title']; }else{ echo ""; } ?></h1>
						</div>
					</div>
				</div>
				<div class="ifo_slu_pass">
					<p class="text-uppercase"><?php if(!empty($home_section_about[1])){ echo $home_section_about[1]['sub_title']; }else{ echo ""; } ?></p>
				</div>				
				<div class="solution_info" id="second_section">
					<?php
						$i=1;
					 	if(!empty($home_section2)){
					 		foreach ($home_section2 as $key => $value) {					 								 		
					 ?>
					<div class="col-md-4 info_min_height">
						<div class="img_info_view">
							<div class="logo_with_info"><img alt="safety img" class="img-responsive main_resp" src="<?php if(!empty($value['ss_icon_image'])) echo $value['ss_icon_image']; else echo "assets/img/no_image.jpg"; ?>">
							</div>
							<div class="log_wi_head">
								<h4><?php echo $value['ss_title']; ?></h4>
							</div>
							<div class="log_wi_p">
								<?php 								
									$string = $value['ss_description'];
											if (strlen($string) > 150) {													
											$stringCut = substr($string, 0, 150);
											$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
											}
										echo $string;
								  ?>
							</div>
							<div class="log_wi_btn_second"><a href="javascript:void(0)" id="show_more_view_id_<?php echo $i; ?>">VIEW MORE</a></div>
							<div class="hide_second_section_content">
								<?php echo $value['ss_description']; ?>
							</div>
						</div>
					</div>
					<?php $i++; }	} ?>
				</div>
	  		</div>
		</div>
	</div>
	<!--section-->
	<div class="mobile_app">
		<div class="mobile_apps_sec">
			<div class="col-md-6 col_m">
				<div class="img_re">
					 <img src="<?php echo base_url(); ?>assets/img/banerimg_01.jpg" class="img-responsive mob_app_img" alt="banner"/>
				</div>
				<div class="app_text_btn">
	            	<p>
	            		Allows transportation providers 
						and independent drivers to register and upload 
						credentialing docs to our platform. Once ready to go, 
						drivers will have access to turn by turn navigation 
						with access to ALL relevant information and instructions 
						making transportation manageable, simple and fun. 
				 	</p>
				 	<p>
				 		"A safe and affordable way to travel"
				 	</p>
                    <!-- <div class="btn_lat">							
						<button class="btn btn-outline" type="button" onClick="window.location='<?php echo base_url('home-page-section');?>'">VIEW MORE</button>
					</div> -->
				</div>
			</div>
			<div class="col-md-6 col_m">
				<div class="img_re">
					<img src="<?php echo base_url(); ?>assets/img/banerimg_02.jpg" class="img-responsive mob_app_img" alt="banner first"/>
				</div>
				<div class="app_text_btn btnsecond">
		            <p>
		            	Allows members to schedule
						and manage trips, edit personal information, 
						access driver information, track rides,
						cancel rides, request for a return ride 
						and rate a trip all within the comforts 
						of their smart phone.
					</p>
					<p>
						"Booking rides with ease"
					</p>
                   <!--  <div class="btn_lat">							
						<button class="btn btn-outline" type="button" onClick="window.location='<?php echo base_url('home-page-section');?>'">VIEW MORE</button>
					</div> -->
				</div>
			</div>
		</div>
		<div class="mobile_app_head">
		    <div class="container">
			    <div class="row">
					<div class="app_head">
						<h2>MOBILE APP SOLUTIONS FOR DRIVERS AND MEMBERS</h2>
					</div>
				</div>
			</div>
		</div>
	</div>	
    <div class="info_img_gallery">
        <div class="container">
		    <div class="row">
			    <div class="img_gallery_head">
					<div class="img_head_f">
						<div class="section-ttl">
							<h1 class="img_head_sac">WHAT EVERYONE SAYING ABOUT TRANSCURA</h1>
							<div class="innerttl">
								<h1>WHAT EVERYONE SAYING ABOUT TRANSCURA</h1>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
    </div>  
	<div class="img_slider_sec">
		<div class="container">
        	<div class="row">
			    <div class="col-md-12" data-wow-delay="0.2s">
					<div class="carousel slide" data-ride="carousel" id="quote-carousel">
						<?php 
						$i=0;
						$j=0;
						$active ='';
						if(!empty($testimonial)) {									
						 ?>
						<!-- Bottom Carousel Indicators -->
						<ol class="carousel-indicators">
							<?php foreach ($testimonial as $key => $value) { ?>
							<li data-target="#quote-carousel" data-slide-to="<?php echo $i; ?>" <?php if($i == 0){ echo 'class="active"'; } ?> ><img class="img-responsive " src="<?php echo base_url().$value['tm_image']; ?>">
							</li>
							<?php $i++; } ?>									
						</ol> 
						<div class="carousel-inner text-center">
							<!-- Quote 1 -->
							<?php foreach ($testimonial as $key => $value) { ?>	
							<div class="item <?php if($j == 0){ echo 'active'; } ?>">
								<div class="row">
									<div class="col-sm-12 sm_box">
										<p><?php
											$string = $value['tm_description'];
											if (strlen($string) > 250) {													
											$stringCut = substr($string, 0, 250);
											$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
											}
										 echo $string; ?></p>
										<h4><?php echo $value['tm_name']; ?></h4>
										<!-- <div class="img_star">
										        <img src="<?php //echo base_url().$value['tm_stars']; ?>" class="img-responsive lsta_slider" alt="star_client"/>
										</div> -->
									</div>
								</div>
							</div>
							<?php $j++; }?>									
						</div>
						<?php  } else { }?>
						<!-- Carousel Slides / Quotes -->
						<!-- Carousel Buttons Next/Prev -->
						<div class="navigation-arr">
							<a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
							<a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
 			</div>
 		</div>	
	</div>
</div>
</div>	 
<?php $this->load->view('user/footer');?>
