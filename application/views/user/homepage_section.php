<?php $this->load->view('user/header'); ?>
<div class="about-page" id="idabout">
  	<div class="theme-breadcrumb pad-50">
		<div class="theme-container container ">
			<div class="row">
				<div class=" col-md-12">
					<div class="title-wrap">
						<h2 class="section-title no-margin"><?php if(!empty($home_section1)){ echo $home_section1[0]['page_title']; } ?></h2>
						<p class="fs-16 no-margin"><?php if(!empty($home_section1)){ echo $home_section1[0]['page_title']; } ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="about-wrap">
			<div class="theme-container container">
				<div class="row">
					<div class="col-md-12">
						<div class="about-us pt-10">							
							<?php if(!empty($home_section1)){ echo $home_section1[0]['page_title']; } ?>
						</div>
					</div>					
				</div>
			</div>
	</section>
  </div>
  <?php $this->load->view('user/footer'); ?>