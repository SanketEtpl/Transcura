<!--registration form-->
<div class="loginsignup">
	<div class="logininner">
		<!--registration form-->
		<div class="signupformmain">
	
			<div class="signupform-inner">
				
				<div class="loginform">
					<div class="titlestrip">
					<div class="ttl text-center">
						<h3 class="text-uppercase">Registration</h3>
					</div>
					</div>	<?php //echo base_url();set-registration ?>
					<form id="signupform" class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data">
						<div class="message"></div>
						<div id="frm-first">
							<div class="row-comm">
						<div class="form-group">
							<label for="usertype" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />User Type</label>
							<select class="form-control" id="usertype" name="user_type">
								<option value="" diable seleted hidden>select user-type</option>
								<?php foreach($userData as $key => $value) { ?>
									<option value="<?php echo $value['role_id']; ?>"<?php echo set_select('user_type', $value['role_id'], FALSE); ?>><?php echo $value['role_name']; ?></option>
								<?php } ?>					
							</select>
							<span class="error_usertype"></span>
						</div>		
						<div class="form-group">
							<label for="fullname" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />Full Name</label>
							<input type="text" class="form-control" id="fullname" value="<?php echo set_value('full_name'); ?>" name="full_name" placeholder="Full name" maxlength="100" autocomplete="off">
							
      			
						</div>	
					</div>
					<div class="row-comm">
						<div class="form-group">
							<label for="dob" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-3.png" alt="" />Date Of Birth</label>
							<input type="text" name="date_of_birth" value="<?php echo set_value('date_of_birth'); ?>" id="date_of_birth" class="form-control" id="dob" placeholder="Date of birth" autocomplete="off">
							<i class="fa fa-calendar" aria-hidden="true"></i>
							
						</div>
						<div class="form-group">
							<label for="username" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="" />User Name</label>
							<input type="text" class="form-control" id="username" value="<?php echo set_value('username'); ?>" name="username" placeholder="Username" maxlength="100" autocomplete="off">
							
						</div>	
					</div>
					<div class="row-comm">
						<div class="form-group">
							<label for="emailaddrss" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/Email-id.png" alt="" />Email Address</label>
							<input type="email" name="email" value="<?php echo set_value('email'); ?>" class="form-control" id="emailaddrss" placeholder="Email" maxlength="100" autocomplete="off">
							
						</div>	
						<div class="form-group">
							<label for="phnnumber" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-6.png" alt="" />Phone Number</label>
							<input type="text" class="form-control" value="<?php echo set_value('phone_no'); ?>" id="phnnumber" name="phone_no" placeholder="Phone Number" maxlength="20" autocomplete="off">
							
						</div>	
					</div>
					<div class="row-comm">
						<div class="form-group">
							<label for="pass" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-5.png" alt="" />Password</label>
							<input type="password" class="form-control" id="pass" name="password" placeholder="Password" maxlength="100" autocomplete="off">
							
						</div>		
						<div class="form-group">
							<label for="conpass" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-5.png" alt="" />Confirm password</label>
							<input type="password" class="form-control" id="conpass" name="confirm_pwd" placeholder="Confirm password" maxlength="100" autocomplete="off">							
						</div>	
						</div>
						<div class="row-comm">						 						
						<div class="form-group contrwrapp">
							<!-- Button -->
							<div class="controls">
								<input id="btn-proceed" type="button" name="btn_first" value="Proceed" class="btn btn-block btn-primary text-uppercase"/>							 
							</div>
						</div>
					</div>
						</div> 
						<div id="frm-second">
							<div class="row-comm">
						<div class="form-group">
							<label for="country" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon1.png" alt="" />Country</label>
							<select class="form-control" name="country" id="country">
								<option value="" class="selecttxt" disable selected hidden>Select Country</option>
								<?php foreach($countryData as $country_name) {
								?>
								<option value="<?php echo $country_name['id']; ?>"><?php echo $country_name['country_name']?></option>
								<?php } ?>
							</select>
							
						</div>	
						<div class="form-group"> 
							<label for="state" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon1.png" alt="" />State</label>
							<select class="form-control" id="state" name="state">
								<option class="selecttxt" value="" disable selected hidden>Select State</option>
							</select>
							
						</div>
					</div>
						<div class="row-comm">							
						<div class="form-group">
							<label for="city" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon1.png" alt="" />City</label>
							<!-- <select class="form-control" id="city" name="city">
								<option class="selecttxt" value="">Select City</option>
							</select> -->
							<input type="text" name="city" class="form-control" id="city" value="<?php echo set_value('city'); ?>" placeholder="City" maxlength="100" autocomplete="off">
							
						</div>		
						<div class="form-group">
							<label for="street" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon6.png" alt="" />Street</label>
							<input type="text" name="street" class="form-control" id="street" value="<?php echo set_value('street'); ?>" placeholder="Street" maxlength="255" autocomplete="off">
							
						</div>
					</div>
						<div class="row-comm">							
						<div class="form-group zip-county">
							<div class="col-md-6">
								<label for="zipcode" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon6.png" alt="" />Zip Code</label>
								<input type="text" name="zipcode" class="form-control" id="zipcode" value="<?php echo set_value('zipcode'); ?>" placeholder="Zipcode" maxlength="6" autocomplete="off">
								
							</div>
							<div class="col-md-6">
								<label for="county" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon6.png" alt="" />County</label>
								<input type="text" name="county" value="<?php echo set_value('county'); ?>" class="form-control" id="county" placeholder="County" maxlength="5" autocomplete="off">
								
							</div>							
						</div>	
							</div>
						<!--NEMT user-->
						<div class="row-comm">						
						<div id="NEMT">
							<div class="select-nemt">
								<div class="form-group">
									<label for="emercontact" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon4.png" alt="" />Personal Documents</label>
									<!-- <input type="text" name="per_doc" class="form-control" value="<?php //echo set_value('per_doc'); ?>" readonly> -->
 
									<label class="btn btn-default btn-file">
										<i class="fa fa-paperclip"></i> <!-- <input type="file" name="per_doc" id="per_doc" value="<?php echo set_value('per_doc'); ?>" > -->
									</label>
									<!--  <input type="file" name="per_doc" id="per_doc"> -->
									<div class="form-group brows-doc">
								    <input class="file" type="file" name="per_doc" id="per_doc" value="<?php echo set_value('per_doc'); ?>" >
								    <div class="input-group col-xs-12">
								      <span class="input-group-addon"><i class="glyphicon glyphicon-picture"></i></span>
								      <input type="text" class="form-control input-lg" disabled placeholder="Upload Image">
								      <span class="input-group-btn">
								        <button class="browse btn btn-primary input-lg" type="button"><i class="glyphicon glyphicon-search"></i> Browse</button>
								      </span>
								    </div>
								  </div>



								</div>		
								<div class="form-group">
									<label for="emercontact" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon3.png" alt="" />medical insurance id</label>
									<input type="text" class="form-control" name="insurance_id" value="<?php echo set_value('insurance_id'); ?>" id="insurance_id" placeholder="Insurance id" maxlength="20" autocomplete="off">								
								</div>								
							</div>
						</div>
					</div>
				
						<div class="row-comm">
						<div class="form-group">
							<label for="emercontact" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon2.png" alt="" />Emergency Contact Name</label>
							<input type="text" class="form-control" name="eme_name" value="<?php echo set_value('eme_name'); ?>" id="eme_name" placeholder="Emergency contact name" maxlength="100" autocomplete="off">
							
						</div>	
						<div class="form-group">
							<label for="emernumber" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon7.png" alt="" />Emergency Contact Number</label>
							<input type="text" class="form-control" name="eme_number" value="<?php echo set_value('eme_number'); ?>" id="eme_number" placeholder="Emergency contact number" maxlength="20" autocomplete="off">
							
						</div>	
						</div>	
						<div class="row-comm">					
						<div class="form-group">
							<label for="signature" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/proc-icon5.png" alt="" />Are you able to provide signatures</label>
							<select class="form-control" id="signature" name="signature">
								<option value="" disable selected hidden>select signature</option>
								<option value="yes" <?php echo set_select('signature', 'yes', FALSE); ?>>Yes</option>
								<option value="no"<?php echo set_select('signature', 'no', FALSE); ?>>No</option>
							</select>
							
						</div>	
						</div>
						<div class="row-comm buton-group">								
						<div class="form-group">
							<!-- Button -->
							<div class="controls">
							  <input id="btnSubmit" type="button" name="register" value="Sign Up" class="btn btn-block btn-primary text-uppercase">
							  <input id="btn-back" type="button" name="back" value="Previous Form" class="btn btn-block btn-primary text-uppercase" data-toggle="modal" data-target="#signupsuccess" />
							</div>
						</div>
						</div>
					
						</div>     
					</form> 
					<script type="text/javascript">
						$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});
					</script>   
					
				</div>
			</div>
		</div>
	</div>
</div>

