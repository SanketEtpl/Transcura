			<div class="footer_section">
				<div class="container">
					<div class="row">
						<div class="foot_sec_info">
							<div class="col-md-3 footer_col">
								<div class="foot_logo">
									<img src="<?php echo base_url(); ?>assets/img/logo-01.png" class="img-responsive" alt="footer logo"/>
								</div>
								<div class="cols_main_div">
								<div class="location_add">
									<div class="col-md-2 col_sm">
										<i class="fa fa-map-marker font_a" aria-hidden="true"></i>
									</div>
									<div class="col-md-10 cli_md">
										<a href="#"><p>3935 Avoin Park Ct.Suite A-108 Chantilly,VA 20151</p></a>
									</div>
								</div>
								<div class="location_add">
									<div class="col-md-2 col_sm">
										<i class="fa fa-phone font_a" aria-hidden="true"></i>
									</div>
									<div class="col-md-10 cli_md">
										<p><a href="javascript:void(0)">+912346890</a></p>
									</div>
								</div>
							    <div class="location_add">
									<div class="col-md-2 col_sm">
									    <i class="fa fa-envelope font_a" aria-hidden="true"></i>
									</div>
									<div class="col-md-10 cli_md">
									    <p><a href="mailto:info@transcura.com">info@transcura.com</a></p>
									</div>
							    </div>
							</div>
						</div>
						<div class="col-md-3 footer_col">
							<div class="cols_main_div_sec">
								<div class="col_ulli_head">
									<h4>Usefull Links</h4>
								</div>
								<div class="location_add_sec">
									<ul>
								     	<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>user">Home</a></li>
									 	<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>about-us">About Us</a></li>
									 	<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>blogs">Blogs</a></li>
									 	<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>news">News</a></li>
									 	<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>contact-us">Contact Us</a></li>
								 	</ul>
						   		</div>
							</div>
						</div>
						<div class="col-md-3 footer_col">
							<div class="cols_main_div_sec">
								<div class="col_ulli_head_sec">
									<h4>Customer Services</h4>
								</div>
								<div class="location_add_sec">
									<ul>
									    <li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>faqs">FAQs</a></li>
										<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>terms-and-conditions">Terms and Conditions</a></li>
										<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>
									</ul>
							   	</div>
							</div>
						</div>
						<div class="col-md-3 footer_col">
						    <div class="cols_main_div_sec">
							    <div class="col_ulli_head_third">
							        <h4>Social Links</h4>
							    </div>
								<div class="location_add_sec_link">
									<div class="link_col">
										<a href="https://www.facebook.com">
											<img src="<?php echo base_url(); ?>assets/img/facebook.png" class="img-responsive" alt="facebook"/>    
										</a>
									</div>			
									<div class="link_col">
										<a href="https://www.google.com/">
											<img src="<?php echo base_url(); ?>assets/img/google+.png" class="img-responsive" alt="google"/>    
										</a>
									</div>
									<div class="link_col">
										<a href="https://www.twitter.com/">
											<img src="<?php echo base_url(); ?>assets/img/twitter.png" class="img-responsive" alt="twitter"/>    
										</a>
									</div>
									<div class="link_col">
										<a href="https://www.pintrest.com">
											<img src="<?php echo base_url(); ?>assets/img/pintrest.png" class="img-responsive" alt="pintrest"/>    
										</a>
									</div>
					   			</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
		   	<div class="final_footer">
		        <div class="container">
				    <div class="row">
					    <div class="footer_info_text">
							<p>&copy; 2017 Transcura. All Rights Reserved</p>
						</div>
					</div>
				</div>
		    </div>
		</div>
	    <!--<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>		-->
	    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>        
	    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	    
		<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.3/jquery.timepicker.min.js"></script>

        
        
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	    <script type="text/javascript">
	       // Instantiate the Bootstrap carousel
			$('.multi-item-carousel').carousel({
			  interval: false
			});
			// for every slide in carousel, copy the next slide's item in the slide.
			// Do the same for the next, next item.
			$('.multi-item-carousel .item').each(function(){
				var next = $(this).next();
			  	if (!next.length) {
					next = $(this).siblings(':first');
			  	}
			  	next.children(':first-child').clone().appendTo($(this));			  
			  	if (next.next().length>0) {
					next.next().children(':first-child').clone().appendTo($(this));
			  	} else {
					$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
			  	}
			}); 
			$(document).ready(function () {      
			    $("#regBtn").click(function(){
			         $('#regModal').modal('show');
			    });
			});
			 	
		//Datepicker
	    $( "#datepicker1" ).datepicker({		
			changeMonth: true,
		   	changeYear: true,
			minDate: 0
		});		
	    $(document).ready(function(){
			$('input.timepicker').timepicker({
			    timeFormat: 'hh:mm p',        			    
			    minTime:"1:00",
			    maxTime:"23:30",
			    startTime : '10:00',
			    interval: 30,
			});

		// change select time in timepicker-2. 
		$('#timepicker').timepicker('setTime', '1:00a');
		    
		//change select time in timepicker-1.
		$('#aptime')
		    .timepicker('setTime', '24:00a')
		    .timepicker('option', 'change', function(time) {
		        // update startTime option in timepicker-2
		        $('#timepicker').timepicker('option', 'maxTime', time);
			});		
			 $("#regBtn").click(function(){			    	
		    	window.location.href = "<?php echo base_url().'registration'?>";			    	
		    });

	    	$("#loginBtn").click(function(){
	    		window.location.href = "<?php echo base_url().'frontend-login'?>";			    	
	    	});
		});



$(document).ready(function(){

		
		var emailPattern = /^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,40}$/i;
		var stringPatten =	/^[a-zA-Z''-'\s]{6,40}$/;
		var phonePattern = /^[\s()+-]*([0-9][\s()+-]*){20}$/;
		var password =/^(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,20})$/;
		//var addressPattern =/^[a-zA-Z\s](\d)?$/;
		var zipcodePattern =/^\d{5,6}$/;
		var countyPattern =/^\d{5}$/;
		var insuranceIDPattern = /^[\s()+-]*([0-9][\s()+-]*){12,16}$/;
		var usernamePattern =/^[a-zA-Z0-9]{8,20}$/;
		var passwordPattern =/^[A-Za-z0-9!@#$%^&*_]{6,20}$/;
		
		//var focusSet =false;
		$("#frm-second").hide();
		$("#btn-back").click(function(){
					$("#frm-second").hide();
					$("#frm-first").show();
					return false;
				});		
		$('#country').on('change',function(){

			
    	var countryID = $(this).val();	       
        if(countryID){
            $.ajax({
                type:'POST',
                url:"<?php echo $this->config->base_url();?>frontend/Homecontroller/get_state",
                data:{countryID:countryID},
                dataType:'json',
                success:function(json){                	                    
                     $('#state').html(json.state);
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
    
    $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>frontend/Homecontroller/get_city',
                data:{'state_id':stateID},
                dataType:'json',
                success:function(json){
                    $('#city').html(json.city);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
   // alert('hello');
	$("#phnnumber").keydown(function(event) {
	  k = event.which;
	  if ((k >= 96 && k <= 105) || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 20) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	});
	$("#fullname").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	});

	$("#btn-proceed").click(function(e){
		var usertype = $("#usertype").val();
		var fullname = $("#fullname").val();
		var date_of_birth = $("#date_of_birth").val();
		var emailaddrss = $("#emailaddrss").val();
		var phnnumber = $("#phnnumber").val();
		var username = $('#username').val();
		var pass = $("#pass").val();
		var conpass = $("#conpass").val();		
		var passFlag =false;
		var confpwdFlag =false;
		var usrFlag =false;
		var dobFlag =false;
		var fullnmFlag =false;
		var emailFlag =false;	
		var phoneFlag =false;	

		if(pass == ''){
			$(".pass").remove();	
			$("#pass").parent().append("<div class='pass' style='color:red;'>The password field is required.</div>");
			passFlag =false;
		}else{
			if( !passwordPattern.test(pass)){
				$(".pass").remove();	
				passFlag =false;
				$("#pass").parent().append("<div class='pass' style='color:red;'>The password field may only contain 6 to 20 characters.</div>");
			}else{
				passFlag =true;
				$(".pass").remove();	
			}	
		}
		if(conpass == ''){
			$(".conpass").remove();	
			$("#conpass").parent().append("<div class='conpass' style='color:red;'>The confirm password field is required.</div>");
			confpwdFlag =false;
		}else{
			if( conpass != pass){
				$(".conpass").remove();	
				confpwdFlag =false;
				$("#conpass").parent().append("<div class='conpass' style='color:red;'>The Passwords do not match.</div>");
			}else{
				confpwdFlag =true;
				$(".conpass").remove();	
			}	
		} 

		if(usertype == ''){
			$(".usertype").remove();	
			$("#usertype").parent().append("<div class='usertype' style='color:red;'>The user type field is required.</div>");
			usrFlag =false;
		}else{
			$(".usertype").remove();
			usrFlag =true;					
		}

		if(date_of_birth == ''){
			$(".date_of_birth").remove();	
			$("#date_of_birth").parent().append("<div class='date_of_birth' style='color:red;'>The date of birth field is required.</div>");
			dobFlag =false;
		}else{
			$(".date_of_birth").remove();
			dobFlag =true;			
		}

		if(phnnumber == ''){
			$(".phnnumber").remove();
			phoneFlag =false;	
			$("#phnnumber").parent().append("<div class='phnnumber' style='color:red;'>The phone no field is required.</div>");
		}else{
			/*if( !phonePattern.test(phnnumber)){
				$(".phnnumber").remove();	
				phoneFlag =false;
				$("#phnnumber").parent().append("<div class='phnnumber' style='color:red;'>The phone no field may only contain 10 to 20 digit.</div>");
			}else{*/
				phoneFlag =true;
				$(".phnnumber").remove();	
			//}
		}


		if(username == ''){
			$(".username").remove();
			usrFlag =false;	
			$("#username").parent().append("<div class='username' style='color:red;'>The user name field is required.</div>");
		}else{
			if( !usernamePattern.test(username)){
				$(".username").remove();	
				usrFlag =false;
				$("#username").parent().append("<div class='username' style='color:red;'>The username field may only contain 8 to 20 characters.</div>");
			}else{
				usrFlag =true;
				$(".username").remove();	
			}
		}

		if(fullname == ''){
			$(".fullname").remove();
			fullnmFlag =false;	
			$("#fullname").parent().append("<div class='fullname' style='color:red;'>The full name field is required.</div>");
		}else{
			/*if( !stringPatten.test(fullname)){
				$(".fullname").remove();	
				fullnmFlag =false;
				$("#fullname").parent().append("<div class='fullname' style='color:red;'>The full name field may only 6 to 40 characters.</div>");
			}else{*/
				fullnmFlag =true;
				$(".fullname").remove();	
			//}
		}

		//Ankush		
		if(emailaddrss == ''){			
			emailFlag = false;			
			$(".emailaddrss").remove();	
			$("#emailaddrss").parent().append("<div class='emailaddrss' style='color:red;'>The email field is required.</div>");
		}else{
			if( !emailPattern.test(emailaddrss)){
				emailFlag = false;				
				$(".emailaddrss").remove();	
				$("#emailaddrss").parent().append("<div class='emailaddrss' style='color:red;'>The email invalid is required.</div>");
			}else{				
				emailFlag = true;
				$(".emailaddrss").remove();	
			}
		}

		if(emailFlag && fullnmFlag && usrFlag && phoneFlag && dobFlag && usrFlag && confpwdFlag && passFlag)
		{
			$("#frm-second").show();
			$("#frm-first").hide();
			return false;	
		}		
	});
	
	$('#usertype').change(function(){		
	       	if($(this).val() === '2') {
	           $('#NEMT').show();           
	        }
	       else
	       	{
	           $('#NEMT').hide();           
	       	}
	    });
	 $('#btnSubmit').click(function(){
	 	var usertype = $("#usertype").val();
		var fullname = $("#fullname").val();
		var date_of_birth = $("#date_of_birth").val();
		var emailaddrss = $("#emailaddrss").val();
		var phnnumber = $("#phnnumber").val();
		var username = $('#username').val();
		var pass = $("#pass").val();
		var conpass = $("#conpass").val();		
		
	 	//var usertype = $("#usertype").val();
		var country = $("#country").val();
		var state = $("#state").val();
		var city = $("#city").val();
		var street = $("#street").val();
		var zipcode = $("#zipcode").val();
		var county = $("#county").val();					
		var eme_name = $("#eme_name").val();
		var eme_number = $("#eme_number").val();
		var insurance_id = $("#insurance_id").val();
		var per_doc = $("#per_doc").val();
		var signature = $('#signature').val();
		var countryFlag = false;
		var stateFlag = false;
		var cityFlag = false; 
		var streetFlag = false;
		var zipcodeFlag = false;
		var countyFlag = false;	
		var emeNameFlag = false;
		var emeNumberFlag = false;
		var insuranceIDFlag = false;
		var perDocFlag = false;
		var signatureFlag = false;


		if(signature == ''){
			$(".signature").remove();	
			$("#signature").parent().append("<div class='signature' style='color:red;'>The signature field is required.</div>");
			signatureFlag =false;
		}else{
			$(".signature").remove();
			signatureFlag =true;					
		} 

		if(eme_number == ''){			
			emeNumberFlag = false;			
			$(".eme_number").remove();	
			$("#eme_number").parent().append("<div class='eme_number' style='color:red;'>The emergency contact no field is required.</div>");
		}else{
			if( !phonePattern.test(eme_number)){
				emeNumberFlag = false;				
				$(".eme_number").remove();	
				$("#eme_number").parent().append("<div class='eme_number' style='color:red;'>The emergency contact no field must be contain integer.</div>");
			}else{				
				emeNumberFlag = true;
				$(".eme_number").remove();	
			}
		}

		if(insurance_id == ''){			
			insuranceIDFlag = false;			
			$(".insurance_id").remove();	
			$("#insurance_id").parent().append("<div class='insurance_id' style='color:red;'>The insurance id field is required.</div>");
		}else{
			if( !insuranceIDPattern.test(insurance_id)){
				insuranceIDFlag = false;				
				$(".insurance_id").remove();	
				$("#insurance_id").parent().append("<div class='insurance_id' style='color:red;'>The insurance id field must be 12 to 16 integer.</div>");
			}else{				
				insuranceIDFlag = true;
				$(".insurance_id").remove();	
			}
		}
		if(eme_name == ''){			
			emeNameFlag = false;			
			$(".eme_name").remove();	
			$("#eme_name").parent().append("<div class='eme_name' style='color:red;'>The emergency contact name field is required.</div>");
		}else{
			if( !stringPatten.test(eme_name)){
				emeNameFlag = false;				
				$(".eme_name").remove();	
				$("#eme_name").parent().append("<div class='eme_name' style='color:red;'>The emergency contact name field must be contain characters.</div>");
			}else{				
				emeNameFlag = true;
				$(".eme_name").remove();	
			}
		}

		if(per_doc == ''){
			$(".per_doc").remove();	
			$("#per_doc").parent().append("<div class='per_doc' style='color:red;'>The personal document field is required.</div>");
			perDocFlag =false;
		}else{
			$(".per_doc").remove();
			perDocFlag =true;					
		}
		if(country == ''){ 
			$(".country").remove();	
			$("#country").parent().append("<div class='country' style='color:red;'>The country field is required.</div>");
			countryFlag =false;
		}else{
			$(".country").remove();
			countryFlag =true;					
		}

		if(state == ''){
			$(".state").remove();	
			$("#state").parent().append("<div class='state' style='color:red;'>The state field is required.</div>");
			stateFlag =false;
		}else{
			$(".state").remove();
			stateFlag =true;					
		}

		if(city == ''){
			$(".city").remove();	
			$("#city").parent().append("<div class='city' style='color:red;'>The city field is required.</div>");
			cityFlag =false;
		}else{
			$(".city").remove();
			cityFlag =true;					
		}

		if(street == ''){			
			streetFlag = false;			
			$(".street").remove();	
			$("#street").parent().append("<div class='street' style='color:red;'>The street field is required.</div>");
		}else{
			/*if( !addressPattern.test(street)){
				streetFlag = false;				
				$(".street").remove();	
				$("#street").parent().append("<div class='street' style='color:red;'>The street field is invalid.</div>");
			}else{		*/		
				streetFlag = true;
				$(".street").remove();	
			//}
		}

		if(zipcode == ''){			
			zipcodeFlag = false;			
			$(".zipcode").remove();	
			$("#zipcode").parent().append("<div class='zipcode' style='color:red;'>The zipcode field is required.</div>");
		}else{
			if( !zipcodePattern.test(zipcode)){
				zipcodeFlag = false;				
				$(".zipcode").remove();	
				$("#zipcode").parent().append("<div class='zipcode' style='color:red;'>The zipcode field is must be 6 integer.</div>");
			}else{				
				zipcodeFlag = true;
				$(".zipcode").remove();	
			}
		}

		if(county == ''){			
			countyFlag = false;			
			$(".county").remove();	
			$("#county").parent().append("<div class='county' style='color:red;'>The county field is required.</div>");
		}else{
			if( !countyPattern.test(county)){
				countyFlag = false;				
				$(".county").remove();	
				$("#county").parent().append("<div class='county' style='color:red;'>The county field is must be 5 integer.</div>");
			}else{				
				countyFlag = true;
				$(".county").remove();	
			}
		}
		//signatureFlag emeNumberFlag insuranceIDFlag emeNameFlag perDocFlag stateFlag  cityFlag streetFlag zipcodeFlag countyFlag
		alert(usertype);
		var userTypeCheck ='';
		if(usertype == 2)
		{
			 userTypeCheck = "perDocFlag && insuranceIDFlag";
		}
		else
		{
			userTypeCheck =true;
		}
		if( signatureFlag && emeNumberFlag  && emeNameFlag  && countryFlag &&stateFlag && cityFlag && streetFlag && zipcodeFlag && countyFlag && userTypeCheck)
		{
			$.ajax({
    				type:"POST",
    				url: "<?php echo base_url(); ?>set-registration",
    				data:{'usertype':usertype,'fullname':fullname,'date_of_birth':date_of_birth,'emailaddrss':emailaddrss,'username':username,'phnnumber':phnnumber,'pass':pass,'country':country,'state':state,'city':city,'street':street,'zipcode':zipcode,'county':county,'eme_name':eme_name,'eme_number':eme_number,'signature':signature,'per_doc':per_doc ,'insurance_id':insurance_id},
    				dataType :"json",
    				success:function(json){
    					alert(json); return false;
    					if(json.flag == 'success')
    					$('.message').html('<div class="alert alert-success">'+json.status+'</div>');
    					else
    					$('.message').html('<div class="alert alert-danger">'+json.status+'</div>');	
    					//alert("success ntmt"+json.status);
    				}
			});	
		}			   	
    }); 
	var today = new Date();
	var n = today.getFullYear();
	var y = n-50;
	//Datepicker
	$( "#date_of_birth" ).datepicker({	
		changeMonth:true,
		changeYear:true,
		maxDate:0,
		//defaultDate: "10/10/1980",
		yearRange: "1970:{ n }"	
	});	
}); 
		
	</script>

</body>
</html>