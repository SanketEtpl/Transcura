<!--registration form-->
<div class="loginsignup">
	<div class="logininner">
		<!--registration form-->
		<div class="signupformmain">
	
			<div class="signupform-inner">
				<div class="titlestrip">
					<div class="ttl text-center">
						<h3 class="text-uppercase">Registration</h3>
					</div>
				</div>	
				<div class="loginform">
					<form id="signupform" class="form-horizontal" role="form">
						
						<div class="form-group">
							<label for="usertype" class="text-uppercase"><img src="../../img/registration-icons/usertype.png" alt="" />User Type</label>
							<select class="form-control" id="usertype">
								<option>MEMBER</option>
								<option>NEMT</option>
								<option>DRIVER</option>
							</select>
						</div>		
						<div class="form-group">
							<label for="fullname" class="text-uppercase"><img src="../../img/registration-icons/usertype.png" alt="" />Full Name</label>
							<input type="text" class="form-control" id="fullname">
						</div>							
						<div class="form-group">
							<label for="dob" class="text-uppercase"><img src="../../img/registration-icons/dob.png" alt="" />Date Of Birth</label>
							<input type="text" class="form-control" id="dob">
						</div>	
						<div class="form-group">
							<label for="emailaddrss" class="text-uppercase"><img src="../../img/registration-icons/email.png" alt="" />Email Address</label>
							<input type="email" class="form-control" id="emailaddrss">
						</div>	
						<div class="form-group">
							<label for="phnnumber" class="text-uppercase"><img src="../../img/registration-icons/phone.png" alt="" />Phone Number</label>
							<input type="text" class="form-control" id="phnnumber">
						</div>	
						<div class="form-group">
							<label for="pass" class="text-uppercase"><img src="../../img/registration-icons/password-gray.png" alt="" />Password</label>
							<input type="password" class="form-control" id="pass">
						</div>		
						<div class="form-group">
							<label for="conpass" class="text-uppercase"><img src="../../img/registration-icons/password-gray.png" alt="" />Password</label>
							<input type="password" class="form-control" id="conpass">
						</div>							
						<div class="form-group">
							<!-- Button -->
							<div class="controls">
							  <a id="btn-login" href="#" class="btn btn-block btn-primary text-uppercase">Proceed</a>
							</div>
						</div>   
					</form>    
					
				</div>
			</div>
		</div>
	</div>
</div>
