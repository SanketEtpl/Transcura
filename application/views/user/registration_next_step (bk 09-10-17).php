<!--Registration next step-->
<div class="loginsignup">
	<div class="logininner">
		<!--registration form-->
		<div class="signupformmain">
	
			<div class="signupform-inner">
				<div class="titlestrip">
					<div class="ttl text-center">
						<h3 class="text-uppercase">Registration</h3>
					</div>
				</div>	
				<div class="loginform">
					<form id="signupform" class="form-horizontal" role="form" method="post" action="<?php echo base_url(); ?>set-registration">						
						<div class="form-group">
							<label for="country" class="text-uppercase"><img src="img/registration-icons/state.png" alt="" />Country</label>
							<select class="form-control" name="country" id="country">
								<option class="selecttxt">Select Country</option>
								<?php foreach($countryData as $country_name) {
								?>
								<option value="<?php echo $country_name['id']; ?>"><?php echo $country_name['country_name']?></option>
								<?php } ?>
							</select>
							<?php echo form_error('country', '<div class="error">', '</div>'); ?>
						</div>	
						<div class="form-group">
							<label for="state" class="text-uppercase"><img src="img/registration-icons/state.png" alt="" />State</label>
							<select class="form-control" id="state" name="state">
								<option class="selecttxt" value="">Select State</option>
							</select>
							<?php echo form_error('state', '<div class="error">', '</div>'); ?>
						</div>							
						<div class="form-group">
							<label for="city" class="text-uppercase"><img src="img/registration-icons/state.png" alt="" />City</label>
							<select class="form-control" id="city" name="city">
								<option class="selecttxt" value="">Select City</option>
							</select>
							<?php echo form_error('city', '<div class="error">', '</div>'); ?>
						</div>		
						<div class="form-group">
							<label for="street" class="text-uppercase"><img src="img/registration-icons/street.png" alt="" />Street</label>
							<input type="text" name="street" class="form-control" id="street" value="<?php echo set_value('street'); ?>">
							<?php echo form_error('street', '<div class="error">', '</div>'); ?>
						</div>							
						<div class="form-group zip-county">
							<div class="col-md-6">
								<label for="zipcode" class="text-uppercase"><img src="img/registration-icons/street.png" alt="" />Zip Code</label>
								<input type="text" name="zipcode" class="form-control" id="zipcode" value="<?php echo set_value('zipcode'); ?>">
								<?php echo form_error('zipcode', '<div class="error">', '</div>'); ?>
							</div>
							<div class="col-md-6">
								<label for="county" class="text-uppercase"><img src="img/registration-icons/street.png" alt="" />County</label>
								<input type="text" name="county" value="<?php echo set_value('county'); ?>" class="form-control" id="county">
								<?php echo form_error('county', '<div class="error">', '</div>'); ?>
							</div>							
						</div>	
						<!--NEMT user-->
						<?php 
							//echo $firstFormData['user_type'];
						if($firstFormData['user_type'] === '8') { ?>
						<div class="select-nemt">
							<div class="form-group">
								<label for="emercontact" class="text-uppercase"><img src="img/registration-icons/personal-doc.png" alt="" />Personal Documents</label>
								<input type="text" name="per_doc" class="form-control" id="emercontact">
								
								<label class="btn btn-default btn-file">
									<i class="fa fa-paperclip" aria-hidden="true"></i> <input type="file" name="per_doc" style="display: none;">
								</label>								
							</div>		
							<div class="form-group">
								<label for="emercontact" class="text-uppercase"><img src="img/registration-icons/medical-insurance.png" alt="" />medical insurance id</label>
								<input type="text" class="form-control" name="insurance_id" value="<?php echo set_value('insurance_id'); ?>" id="insurance_id">								
							</div>								
						</div>
						<?php } ?>
						<div class="form-group">
							<label for="emercontact" class="text-uppercase"><img src="img/registration-icons/usertype.png" alt="" />Emergency Contact Name</label>
							<input type="text" class="form-control" name="eme_name" value="<?php echo set_value('eme_name'); ?>" id="eme_name">
							<?php echo form_error('eme_name', '<div class="error">', '</div>'); ?>
						</div>	
						
						<div class="form-group">
							<label for="emernumber" class="text-uppercase"><img src="img/registration-icons/contact.png" alt="" />Emergency Contact Number</label>
							<input type="text" class="form-control" name="eme_number" value="<?php echo set_value('eme_number'); ?>" id="eme_number">
							<?php echo form_error('eme_number', '<div class="error">', '</div>'); ?>
						</div>							
						<div class="form-group">
							<label for="signature" class="text-uppercase">Are you able to provide signatures</label>
							<select class="form-control" id="signature" name="signature">
								<option value="">select</option>
								<option value="yes"<?php echo set_select('signature', 'yes', FALSE); ?>>Yes</option>
								<option value="no"<?php echo set_select('signature', 'no', FALSE); ?>>No</option>
							</select>
							<?php echo form_error('signature', '<div class="error">', '</div>'); ?>
						</div>
						<div class="form-group">
							<input type="hidden" name="user_type" value="<?php echo $firstFormData['user_type']; ?>">
							<input type="hidden" name="full_name" value="<?php echo $firstFormData['full_name']; ?>">
							<input type="hidden" name="date_of_birth" value="<?php echo $firstFormData['date_of_birth']; ?>">
							<input type="hidden" name="email" value="<?php echo $firstFormData['email']; ?>">
							<input type="hidden" name="phone_no" value="<?php echo $firstFormData['phone_no']; ?>">
							<input type="hidden" name="password" value="<?php echo $firstFormData['password']; ?>">
						</div>						
						<div class="form-group">
							<!-- Button -->
							<div class="controls">
							  <input id="btn-login" type="submit" name="register" value="Sign Up" class="btn btn-block btn-primary text-uppercase" data-toggle="modal" data-target="#signupsuccess" />
							</div>
						</div>   
					</form> 
					
					<!--signup success popup-->
					<div id="signupsuccess" class="modal fade success-modal" role="dialog">
					  <div class="modal-dialog modal-sm">

						<!-- Modal content-->
						<div class="modal-content">

						  <div class="modal-body text-center">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<img src="img/registration-icons/success-check.png" alt="" width="50"/>
							<h4 class="text-uppercase successtxt">Successful</h4>
							<p class="successmsg">Your Registration is Successful</p>
						  </div>
						  <div class="modal-footer text-center">
							<a href="login.html" class="btn btn-primary">OK</a>
						  </div>
						</div>

					  </div>
					</div>					
					<!--signup success popup-->
					
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){	
    $('#country').on('change',function(){
    	var countryID = $(this).val();	       
        if(countryID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>frontend/Homecontroller/get_state',
                data:{countryID:countryID},
                dataType:'json',
                success:function(json){                	                    
                     $('#state').html(json.state);
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
    
    $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:'<?php echo base_url();?>frontend/Homecontroller/get_city',
                data:{'state_id':stateID},
                dataType:'json',
                success:function(json){
                    $('#city').html(json.city);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
});
</script>
