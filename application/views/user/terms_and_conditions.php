<?php $this->load->view('user/header'); ?>
<div class="privacy_main innerpages" id="idpry">
  <div class="theme-breadcrumb pad-50">
    <div class="theme-container container ">
      <div class="row">
        <div class=" col-md-12">
          <div class="title-wrap">
            <h2 class="section-title no-margin"><?php if(!empty($terms_and_conditions)){ echo $terms_and_conditions[0]['page_title']; } else { echo ''; } ?></h2>
            <p class="fs-16 no-margin"><?php if(!empty($terms_and_conditions)){ echo $terms_and_conditions[0]['sub_title']; } else { echo ''; } ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
<div class="inner-wrapper privacy">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-lg-12">
        <div class="row1">
        	<?php if(!empty($terms_and_conditions)){ echo $terms_and_conditions[0]['page_detail']; } else { echo ''; } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php $this->load->view('user/footer'); ?>