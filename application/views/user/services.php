<?php $this->load->view('user/header'); ?>
<div class="services-page" id="idservices">
  	<div class="theme-breadcrumb pad-50">
		<div class="theme-container container ">
			<div class="row">
				<div class=" col-md-12">
					<div class="title-wrap">
						<h2 class="section-title no-margin"><?php if(!empty($servicesData)){ echo $servicesData[0]['page_title']; } else { echo ''; } ?></h2>
						<p class="fs-16 no-margin"><?php if(!empty($servicesData)){ echo $servicesData[0]['sub_title']; } else { echo ''; } ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>	
	<?php if(!empty($servicesData)){ echo $servicesData[0]['page_detail']; } else { echo ''; } ?>
</div>
<?php $this->load->view('user/footer'); ?>