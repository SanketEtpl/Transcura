<?php $this->load->view('user/header'); ?>
<!--login-->
<div class="loginsignup">
	<div class="logininner">
		<!--login form-->
		<div class="loginformmain">	
			<div class="loginform-inner">				
				<div class="loginform">
					<div class="titlestrip">
						<div class="ttl text-center">
							<h3 class="text-uppercase">Login</h3>
						</div>
					</div>	
					<form id="loginform" class="" method="POST" action="<?php echo base_url(); ?>user-login" autocomplete="off">
					<?php
								
						$change_success_msg = $this->session->flashdata("change_success_msg");
                		$success_msg = $this->session->flashdata("success_msg");
                		$log_error_msg = $this->session->flashdata('log_error_msg');
                		$auth_reg_success = $this->session->flashdata('auth_reg_success');
                		$forgot_success_msg = $this->session->flashdata('forgot_success_msg');
                		$error = $this->session->flashdata('error');
                		
                		if($change_success_msg){
		                    ?>
		                    <div class="alert alert-success">
		                      <?php echo $change_success_msg; ?>
		                    </div>
		                  <?php
		                  }
                		
                		if($forgot_success_msg){
		                    ?>
		                    <div class="alert alert-success">
		                      <?php echo $forgot_success_msg; ?>
		                    </div>
		                  <?php
		                }
                		
                		if($auth_reg_success){
		                    ?>
		                    <div class="alert alert-success">
		                      <?php echo $auth_reg_success; ?>
		                    </div>
		                  <?php
		                }
                		
                		if($success_msg){
		                    ?>
		                    <div class="alert alert-success">
		                      <?php echo $success_msg; ?>
		                    </div>
		                  <?php
		                }

		                if($log_error_msg){
		                    ?>
		                    <div class="alert alert-danger">
		                      <?php echo $log_error_msg; ?>
		                    </div>
		                    <?php
		                }

		                if($error){
		                  	?>
		                  	<div class="alert alert-danger">
		                  		<?php echo $error; ?>
		                  	</div>	
		                 <?php }
                	?>
	                <div class="message"> </div>					
					<div class="form-group">
						<img src="<?php echo base_url();?>assets/img/Email-id.png" alt="" />
						<input id="emailaddrss" type="text" class="form-control" value="<?php echo set_value('username'); ?>" name="username" placeholder="Email id / Username" autocomplete="off" onblur="this.setAttribute('readonly', 'readonly');" onfocus="this.removeAttribute('readonly');">                                        
						<?php echo form_error('username', '<div class="error">', '</div>'); ?>
					</div>
					<div class="form-group">
						<img src="<?php echo base_url();?>assets/img/icon-5.png" alt="" />
						<input id="login-password" type="password" class="form-control" name="password" placeholder="Password" autocomplete="new-password" onblur="this.setAttribute('readonly', 'readonly');" onfocus="this.removeAttribute('readonly');">
						<?php echo form_error('password', '<div class="error">', '</div>'); ?>
					</div>
					<div class="form-group">			
						<img src="<?php echo base_url();?>assets/img/icon-9.png" alt="" />										
						<select class="form-control" id="usertype" name="user_type" autocomplete="off">
							<option value="" disable selected hidden>select user-type</option>
							<?php 
							if(!empty($userData)){								
							foreach($userData as $key => $value) { ?>
								<option value="<?php echo $value['role_id']; ?>"<?php echo set_select('user_type', $value['role_id'], FALSE); ?>><?php echo $value['role_name']; ?></option>	
							<?php } } else{ ?>
							<option value=""></option>
							<?php } ?>
						</select>
						<?php echo form_error('user_type', '<div class="error">', '</div>'); ?>							
					</div>								
					<div class="form-group">
						<!-- Button -->
						<div class="controls text-center">
							<input id="btn-login" type="submit" value="Log in" class="btn btn-primary text-uppercase"/>							  
						  	<span class="or">OR</span>
						  	<a id="btn-signup" href="<?php echo base_url(); ?>registration" class="btn btn-success text-uppercase">Sign Up</a>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12 control">
							<div class="forgot text-center">
							<a href="<?php echo base_url();?>reset_password">
								Forgot password 
							</a>/
						<a href="<?php echo base_url();?>reset_username">
								Forgot username ?
							</a>
							</div>
						</div>
					</div>    
					</form>     			
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('user/footer'); ?>
