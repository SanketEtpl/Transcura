<?php $this->load->view('user/header'); ?>
<div class="contact_main innerpages">
	<div class="theme-breadcrumb pad-50">
	    <div class="theme-container container ">
	    	<div class="row">
		        <div class=" col-md-12">
		          	<div class="title-wrap">
		           		<h2 class="section-title no-margin">News</h2>
		            	<p class="fs-16 no-margin">News & our mission</p>
		          	</div>
		        </div>
			</div>
	    </div>
	</div>
	<div class="inner-wrapper contact-us">
		<section class="blog-section style-two">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="item-holder">
							<div class="image-box">
								<figure>
									<img src="assets/img/1.jpg" alt="">
								</figure>
							</div>
							<div class="image-text">
								<span>Oct 24, 2017</span>
								<a href="#">
									<h4>
									Maecenas Pulvinar
									<br>
									Nunc Condiment Ris.
									</h4>
								</a>
								<p>Lorem ipsum dolor amet c...idunt labore et dolore.</p>
								<div class="text">
								<p>
								by Adam Smith
								<span>5 Comments</span>
								</p>
								</div>
								<a class="btn readwrapp" href="#">Read More</a>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="item-holder">
							<div class="image-box">
								<figure>
									<img src="assets/img/2.jpg" alt="">
								</figure>
							</div>
							<div class="image-text">
								<span>Oct 24, 2017</span>
								<a href="#">
									<h4>
									Maecenas Pulvinar
									<br>
									Nunc Condiment Ris.
									</h4>
								</a>
								<p>Lorem ipsum dolor amet c...idunt labore et dolore.</p>
								<div class="text">
								<p>
								by Adam Smith
								<span>5 Comments</span>
								</p>
								</div>
								<a class="btn readwrapp" href="#">Read More</a>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="item-holder">
							<div class="image-box">
								<figure>
									<img src="assets/img/6.jpg" alt="">
								</figure>
							</div>
							<div class="image-text">
								<span>Oct 24, 2017</span>
								<a href="#">
									<h4>
									Maecenas Pulvinar
									<br>
									Nunc Condiment Ris.
									</h4>
								</a>
								<p>Lorem ipsum dolor amet c...idunt labore et dolore.</p>
								<div class="text">
								<p>
								by Adam Smith
								<span>5 Comments</span>
								</p>
								</div>
								<a class="btn readwrapp" href="#">Read More</a>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="item-holder">
							<div class="image-box">
								<figure>
									<img src="assets/img/1.jpg" alt="">
								</figure>
							</div>
							<div class="image-text">
								<span>Oct 24, 2017</span>
								<a href="#">
									<h4>
									Maecenas Pulvinar
									<br>
									Nunc Condiment Ris.
									</h4>
								</a>
								<p>Lorem ipsum dolor amet c...idunt labore et dolore.</p>
								<div class="text">
								<p>
								by Adam Smith
								<span>5 Comments</span>
								</p>
								</div>
								<a class="btn readwrapp" href="#">Read More</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>  
	</div>
</div> 
 <?php $this->load->view('user/footer'); ?>