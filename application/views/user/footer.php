			
<div class="footer_section">
	<div class="container">
		<div class="row">
			<div class="foot_sec_info">
				<div class="col-md-3 footer_col">
					<div class="foot_logo">
						<a href="<?php echo base_url();?>user"><img src="<?php if(!empty($footer[0])){ echo base_url().$footer[0]['logo']; }else { echo ''; } ?>" class="img-responsive" alt="footer logo"/>
						</a>
					</div>
					<div class="cols_main_div">
						<div class="location_add">
							<div class="col_sm">
								<i class="fa fa-map-marker font_a" aria-hidden="true"></i>
							</div>
							<div class="cli_md">
								<a href="javascript:void(0)"><?php if(!empty($footer[0])){ echo $footer[0]['company_address']; }else{ echo ''; } ?></a>
							</div>
						</div>
						<div class="location_add">
							<div class="col_sm">
								<i class="fa fa-phone font_a" aria-hidden="true"></i>
							</div>
							<div class="cli_md">
								<a href="javascript:void(0)"><?php if(!empty($footer[0])){ echo $footer[0]['company_phone']; }else{ echo ''; }  ?></a>
							</div>
						</div>
						<div class="location_add">
							<div class="col_sm">
								<i class="fa fa-envelope font_a" aria-hidden="true"></i>
							</div>
							<div class="cli_md">
								<a href="mailto:<?php if(!empty($footer[0])){ echo $footer[0]['office_email']; } else { echo ''; } ?>"><?php if(!empty($footer)){ echo $footer[0]['office_email'];} else { echo ''; } ?></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3 footer_col">
					<div class="cols_main_div_sec">
						<div class="col_ulli_head">
							<h4>Usefull Links</h4>
						</div>
						<div class="location_add_sec">
							<ul>
								<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>user">Home</a></li>
								<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>about-us">About Us</a></li>
									 	<!-- <li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php //echo base_url(); ?>blogs">Blogs</a></li>
									 	<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php // echo base_url(); ?>news">News</a></li> -->
									 	<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>contact-us">Contact Us</a></li>
									 </ul>
									</div>
								</div>
							</div>
							<div class="col-md-3 footer_col">
								<div class="cols_main_div_sec">
									<div class="col_ulli_head_sec">
										<h4>Customer Services</h4>
									</div>
									<div class="location_add_sec">
										<ul>
											<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>faqs">FAQs</a></li>
											<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>terms-and-conditions">Terms and Conditions</a></li>
											<li><span class="arro_w">&raquo;&nbsp;</span><a href="<?php echo base_url(); ?>privacy-policy">Privacy Policy</a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-md-3 footer_col">
								<div class="cols_main_div_sec">
									<div class="col_ulli_head_third">
										<h4>Social Links</h4>
									</div>
									<div class="location_add_sec_link">
										<div class="link_col">
											<a title="Facebook" href="<?php if(!empty($footer[0])){ echo $footer[0]['facebook']; } else { echo ''; } ?>">
												<img src="<?php echo base_url(); ?>assets/img/facebook.png" class="img-responsive" alt="Facebook"/>    
											</a>
										</div>			
										<div class="link_col">
											<a title="Google Plus" href="<?php if(!empty($footer[0])){ echo $footer[0]['google']; } else { echo ''; } ?>">
												<img src="<?php echo base_url(); ?>assets/img/google+.png" class="img-responsive" alt="Google Plus"/>    
											</a>
										</div>
										<div class="link_col">
											<a title="Twitter" href="<?php if(!empty($footer[0])){ echo $footer[0]['twitter']; } else { echo ''; } ?>">
												<img src="<?php echo base_url(); ?>assets/img/twitter.png" class="img-responsive" alt="Twitter"/>    
											</a>
										</div>
										<div class="link_col">
											<a title="Pintrest" href="<?php if(!empty($footer[0])){ echo $footer[0]['pintrest']; } else { echo ''; } ?>">
												<img src="<?php echo base_url(); ?>assets/img/pintrest.png" class="img-responsive" alt="pintrest"/>    
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="final_footer">
				<div class="container">
					<div class="row">
						<div class="footer_info_text">
							<p>&copy; <?php echo date('Y'); ?> Transcura. All Rights Reserved</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	    <!--<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>		
	    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>-->
	    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
	    
	    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>	    
	    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.3/jquery.timepicker.min.js"></script>
	    <!-- <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> -->
	    
	    <script type="text/javascript">
	    	var baseURL = "<?php echo base_url(); ?>";
	    </script>

	    <script src="<?php echo base_url(); ?>js/jquery.payform.min.js"></script>
	    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>

	    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>  
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
  
 
  
	    
	    <script src="<?php echo base_url(); ?>assets/js/nemt.js"></script>
	    <script src="<?php echo base_url(); ?>assets/js/driverDashboard.js"></script>
	    <script src="<?php echo base_url(); ?>assets/js/memberDashboard.js"></script>
	    <script src="<?php echo base_url(); ?>assets/js/homepage.js"></script>
	    <script src="http://formvalidation.io/vendor/formvalidation/js/formValidation.min.js"></script>
	    <script src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>

	    <script src="http://formvalidation.io/vendor/formvalidation/js/formValidation.min.js"></script>
	    <script src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
	    <script src="<?php echo base_url(); ?>assets/js/intlTelInput.js"></script>  
	    <!--<script src="<?php echo base_url(); ?>assets/js/intlTelInput.min.js"></script>-->
	    <script>
	    	var base_url = '<?php echo base_url(); ?>';
	    </script>
	    
	    <script type="text/javascript">
	       // Instantiate the Bootstrap carousel
	       $('.multi-item-carousel').carousel({
	       	interval: false
	       });
			// for every slide in carousel, copy the next slide's item in the slide.
			// Do the same for the next, next item.
			$('.multi-item-carousel .item').each(function(){
				var next = $(this).next();
				if (!next.length) {
					next = $(this).siblings(':first');
				}
				next.children(':first-child').clone().appendTo($(this));			  
				if (next.next().length>0) {
					next.next().children(':first-child').clone().appendTo($(this));
				} else {
					$(this).siblings(':first').children(':first-child').clone().appendTo($(this));
				}
			}); 
			$(document).ready(function () {  			  
				$("#regBtn").click(function(){
					$('#regModal').modal('show');
				});
			});
			
		//Datepicker
		$( "#datepicker1" ).datepicker({		
			changeMonth: true,
			changeYear: true,		   	
			minDate: 0
		});		

		$(document).ready(function(){
			// for autocomplete source position in member
	    	// google.maps.event.addDomListener(window, 'load', initialize);
	    	
	    	var newTime = '';
	    	
	    	/*$('#datepicker1').change(function(){

	    		var today = new Date();
				var dd = today.getDate();
				var mm = today.getMonth()+1; //January is 0!

				var yyyy = today.getFullYear();
				if(dd<10){
				    dd='0'+dd;
				} 
				if(mm<10){
				    mm='0'+mm;
				} 
				var today = mm+'/'+dd+'/'+yyyy;

	    		if($(this).val() == today)
	    		{
	    			newTime = '<?php //echo date("h:i a",strtotime('+1 hour'))?>';
	    			//$('input.timepicker').timepicker();
	    			//$('input.timepicker').timepicker();
	    			$('.selectTime.timepicker').timepicker({
						timeFormat: 'h:mm p',        			    
						minTime: newTime,
						//maxTime:"23:30",
						maxHour: 23,
						maxMinutes: 59,
						startTime : '12:00am',
						interval: 30,
					});
	    		}
	    		else
	    		{
	    			//$('input.timepicker').timepicker();
	    			$('.selectTime').timepicker({
						timeFormat: 'h:mm p',        			    
						//minTime: '',
						//maxTime:"23:30",
						maxHour: 23,
						maxMinutes: 59,
						startTime : '12:00am',
						interval: 30,
					});

					//$('.selectTime').removeClass('selectTime');
	    		}
	    	});*/


			$('input.timepicker').timepicker({
				timeFormat: 'h:mm p',        			    
				minTime:"12:00am",
				//maxTime:"23:30",
				maxHour: 23,
				maxMinutes: 59,
				//startTime : '12:00am',
				interval: 30,
			});

		// change select time in timepicker-2. 
		$('#timepicker').timepicker('setTime', '00:00:00');
		
		//change select time in timepicker-1.
		$('.selectTime')
		.timepicker('setTime', '24:00a')
		.timepicker('option', 'change', function(time) {
		        // update startTime option in timepicker-2
		        $('#timepicker').timepicker('option', 'maxTime' , time);		        
		    });	

		$('#return_time')
		.timepicker('setTime', '24:00a')
		    .timepicker('option', 'change', function(time) {
		        // update startTime option in timepicker-2
		        $('#aptime').timepicker('option', 'maxTime' , time);		        
			});	
		var today = new Date();
		var n = today.getFullYear();
		var y = n-70;
			//alert(y);
			//Datepicker
			$( "#date_of_birth" ).datepicker({	
				//defaultDate: new Date("1988-02-25"),
				changeMonth:true,
				changeYear:true,
				maxDate:0,								
				yearRange: y+":{ n }"	
			});	
			setInterval(function(){
				$(".alert, .alert-success, .alert-danger").fadeOut('fast');
			},10000);
		});	
		
	</script>
	<script type="text/javascript">
		$(document).on('click', '.browse', function(){
			var file = $(this).parent().parent().parent().find('.file');
			file.trigger('click');
		});
		$(document).on('change', '.file', function(){
			$(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$("#menu-toggle").click(function(){
				$("#showmenu .tab_menu").toggle();
			});
		});
	</script>  
	<!-- scroll js -->
	<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>	
	<script>
		$(document).ready(function() {
					$("#noti_box").niceScroll({touchbehavior:false,cursorcolor:"#0090ca",cursorwidth:5,background:"#ccc", cursorborderradius: "5px", enablekeyboard:true, autohidemode:true, cursorfixedheight: 150 }); // left scrollable inbox message  
				});		
			</script>		
			<!--scroll js -->
			
		</script>
		<script src="<?php echo base_url(); ?>js/restaurant.js"></script>
		
		<!-- <script src="<?php //echo base_url(); ?>js/card_script.js"></script> -->


	</body>
	</html>
