<?php $this->load->view('user/header'); ?>
<div class="contact_main innerpages">
  <div class="theme-breadcrumb pad-50">
    <div class="theme-container container ">
      <div class="row">
        <div class=" col-md-12">
          <div class="title-wrap">
            <h2 class="section-title no-margin"><?php if(!empty($contactUs)) { echo $contactUs[0]['page_title']; } else { echo ''; } ?></h2>
            <p class="fs-16 no-margin"><?php if(!empty($contactUs)) { echo $contactUs[0]['sub_title']; } else { echo ''; } ?></p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="inner-wrapper contact-us">
    <div class="container">
      <div class="row">
        <?php if(!empty($contactUs)) { echo $contactUs[0]['page_detail']; } else { echo ''; } ?>
        <div class="col-sm-12 col-md-6">
          <h2>Leave a Message</h2>
          <div class="forms">
            <form name="frm_contact_us" action="<?php echo base_url(); ?>submit-contact-us" method="POST" enctype="multipart/form-data">
            <!-- <form name="frm_contact_us" method="POST" action="<?php// echo base_url(); ?>submit-contact-us"> -->
              <div class="controls field">
                <?php 
                  $success = $this->session->flashdata('success');
                  $fail = $this->session->flashdata('fail');
                  if($success)
                  { ?>
                    <div class="alert alert-success">
                      <?php echo $success; ?>
                    </div>
                <?php }                
                if($fail)
                { ?>
                  <div class="alert alert-danger">
                    <?php echo $fail; ?>
                  </div>
                <?php }
                ?>                      
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="contact_name">First name *</label>
                      <input id="fname" type="text" name="first_name" value="<?php echo set_value('first_name'); ?>" class="form-control name" placeholder="First name" maxlength="50" autocomplete="off">
                      <div class="help-block with-errors"></div>
                      <?php echo form_error('first_name', '<div class="error">', '</div>'); ?>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="form_lastname">Last name *</label>
                      <input id="lname" type="text" name="last_name" class="form-control name" value="<?php echo set_value('last_name'); ?>" placeholder="Last name" maxlength="50" autocomplete="off">
                      <div class="help-block with-errors"></div>
                      <?php echo form_error('last_name', '<div class="error">', '</div>'); ?>
                    </div>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="form_email">Email *</label>
                    <input id="emailaddrss" type="text" name="email" class="form-control" value="<?php echo set_value('email'); ?>" placeholder="Email" maxlength="100" autocomplete="off">
                    <div class="help-block with-errors"></div>
                    <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="form_phone">Phone *</label>
                    <input id="phnnumber" type="tel" name="phone" class="form-control" value="<?php echo set_value('phone'); ?>" placeholder="Phone" maxlength="20" autocomplete="off">
                    <div class="help-block with-errors"></div>
                    <?php echo form_error('phone', '<div class="error">', '</div>'); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 msg-grp">
                  <div class="form-group">
                    <label for="form_message">Message *</label>
                    <textarea id="message" name="message" class="form-control" placeholder="Message" rows="4" maxlength="255" autocomplete="off"><?php echo set_value('message'); ?></textarea>
                    <div class="help-block with-errors"></div>
                    <?php echo form_error('message', '<div class="error">', '</div>'); ?>
                  </div>
                </div>                  
                <div class="col-md-12 submitbtn">
                    <input type="submit" name="submit_contact"  class="btn btn-success btn-send" value="Send message">
                </div>
              </div>          
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="offset-top-30">
      <div id="googleMap">
       <iframe src="https://www.google.com/maps/embed/v1/place?q=3935%20Avoin%20Park%20Ct.Suite%20A-108%20Chantilly%2CVA%2020151&key=AIzaSyAO9q2RgUAWQAbrwW_Z716Py56Lh68s0ZM" allowfullscreen></iframe> 
      </div>
    </div>
  </div>
</div>
</div> 
 <?php $this->load->view('user/footer'); ?>
