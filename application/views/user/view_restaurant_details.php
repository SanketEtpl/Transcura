<?php $this->load->view('user/header'); ?>
<div id="schLoadingDiv" style="position: fixed;z-index: 999;background-color: rgba(0, 0, 0, 0.35);width: 100%;height: 100%;top: 0;text-align: center;left:0;">
          <img id="loader-img" alt="loading" src="<?php echo base_url(); ?>assets/img/loading.gif" align="center" style="margin: 0 auto;width: 60px;margin-top: 218px;
        "/>
        </div>
<div class="about-page" id="idabout">
    <div class="theme-breadcrumb pad-50">
        <div class="theme-container container ">
            <div class="row">
                <div class=" col-md-12">
                    <div class="title-wrap">
                        <h2 class="section-title no-margin">Schedule Ride</h2>
                        <p class="fs-16 no-margin">search for address</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="theme-container container ">	
        <div class="row">
        <form name="frm_book_rest" id="frm_book_rest" action="#" method="POST">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Source Point</label>
                            <input id="source" type="text" name="source" value="<?php echo set_value('source'); ?>" class="form-control name" placeholder="Source Point" maxlength="255">
                            <div class="help-block with-errors"></div>
                        </div>


                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="exampleFormControlInput1">Destination Point</label>
                            <input id="destination" type="text" name="destination" value="<?php echo $restaurant_details->location->address; ?>" class="form-control name" placeholder="Destination Point" maxlength="255" readonly="">
                            <div class="help-block with-errors"></div>
                        </div>
                    </div>

                    <div class="col-md-12">
                    	<input type="hidden" id="start_lat" name="start_lat">
                  		<input type="hidden" id="start_long" name="start_long">
                    	<input type="hidden" id="end_lat" name="end_lat" value="<?php echo $restaurant_details->location->latitude; ?>">
                  		<input type="hidden" id="end_long" name="end_long" value="<?php echo $restaurant_details->location->longitude; ?>">
                  		<input type="hidden" id="distance" name="distance">
                  		<input type="hidden" id="duration" name="duration">
                    </div>

                    <div class="col-md-12 submitbtn">					
                        <input id="getRestaAddress" type="button" name="back" value="Get address"/>
                        <input id="scheduleRestRideBtn" type="button" name="schedule_ride" value="Schedule ride"/>
                    </div>					
                </div>				
            </div>
            </form>
        </div>
        <br /> 
     <div class="col-sm-12" id="drop_point_details">
	      <div class="table-responsive">
  			  <table class="table table-bordered table-striped dataTable" id="listAddress" role="grid" aria-describedby="example1_info">
    				<thead>
    				  <tr role="row">
    						<th width="25%">Start Point</th>
    						<th width="25%">Destination Point</th>	
			                <th width="25%">Distance</th>
			                <th width="25%">Duration</th>
    					</tr>				 
    				</thead>
  				  <tbody>					
  				  </tbody>
  				</table>
			  </div>
	    </div>
        <br />
        <div id="map" style="width: 800px;height: 400px;"></div>
    </div>
</div>
<!-- JS to show map -->    
<script type="text/javascript">
    function initMap() {
        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var myLatLng = {lat: <?php echo $restaurant_details->location->latitude; ?>, lng: <?php echo $restaurant_details->location->longitude; ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            center: myLatLng
        });
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: '<?php echo $restaurant_details->location->locality_verbose; ?>'
        });
        directionsDisplay.setMap(map);
        var onChangeHandler = function () {
            calculateAndDisplayRoute(directionsService, directionsDisplay);
        };
        document.getElementById('source').addEventListener('change', onChangeHandler);
        document.getElementById('destination').addEventListener('change', onChangeHandler);
        document.getElementById('getRestaAddress').addEventListener('click', onChangeHandler);
    }
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (p) {
            var LatLng = new google.maps.LatLng(p.coords.latitude, p.coords.longitude);
            var mapOptions = {
                center: LatLng,
                zoom: 13,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            // document.getElementById("current_lat").value = p.coords.latitude.toFixed(6);
            // document.getElementById("current_lng").value = p.coords.longitude.toFixed(6);
        });
    } else {
        alert('Geo Location feature is not supported in this browser.');
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        directionsService.route({
            origin: document.getElementById('source').value,
            destination: document.getElementById('destination').value,
            travelMode: 'DRIVING'
        }, function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
            } else {
                // window.alert('Directions request failed due to ' + status);
            }
        });
    }
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBe-UniUGzwJHL5kCOU4hsMj4z-ptyDuyw&callback=initMap"></script>
<?php $this->load->view('user/footer'); ?>