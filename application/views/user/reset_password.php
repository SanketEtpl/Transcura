<?php $this->load->view('user/header'); ?>
<!--Reset password-->
<div class="loginsignup">
	<div class="logininner">
		<!--login form-->
		<div class="signupformmain">	
			<div class="signupform-inner">					
				<div class="loginform">
					<div class="titlestrip">
						<div class="ttl text-center">
							<h3 class="text-uppercase">Reset Password</h3>
						</div>
					</div>
					<!--form-->
					<form id="resetform" class="form-horizontal forgotform" method="POST" role="form" action="">
						<div class="message"></div>
						<?php $forgot__error_msg = $this->session->flashdata('forgot__error_msg');
				           if($forgot__error_msg){ ?>
				                    <div class="alert alert-danger">
				                      <?php echo $forgot__error_msg; ?>
				                    </div>
				            <?php } ?>	
						<div class="resetpass-txt text-center">						
							<img src="<?php echo base_url();?>assets/img/forget-pass-1.png" alt="" width="50"/>
							<!-- <h4 class="text-uppercase successtxt">Reset Your Password</h4> -->
							<p class="successmsg">Please Enter Your Registered Email Address Below.</p>						
						</div>
						<div class="form-group mt-20">
							<label for="email" class="text-uppercase"><img src="<?php echo base_url();?>assets/img/Email-id.png" alt="" />Email</label>
							<input type="email" class="form-control" id="emailaddrss" name="email" placeholder="Email Id" value="<?php echo set_value('email');?>">
						</div>					
						<div class="form-group">
							<!-- Button -->
							<div class="controls">
							  <input id="btn-reset" type="button" name="reset_pwd" value="Continue" class="btn btn-block btn-primary text-uppercase"/>
							</div>
						</div>   
					</form> 
					<!--form-->
					<!--reset success popup-->
					<div id="resetsuccess" class="modal fade success-modal" role="dialog">
					  	<div class="modal-dialog modal-sm">
							<!-- Modal content-->
							<div class="modal-content">
							  	<div class="modal-body text-center">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<img src="<?php echo base_url();?>assets/img/forget-pass-2.png" alt="" width="50"/>
									<h4 class="text-uppercase successtxt">Successful</h4>
									<p class="successmsg">A link to reset your password<br> has been sent to this<br> email address.</p>
						  		</div>
							    <div class="modal-footer text-center">						  	
									<a href="<?php echo base_url(); ?>user-login" class="btn btn-primary">OK</a>
							    </div>
							</div>
		          		</div>
					</div>					
					<!--signup success popup-->					
				</div>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('user/footer'); ?>