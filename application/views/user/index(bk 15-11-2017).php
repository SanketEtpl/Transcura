<?php $this->load->view('user/header');?>
<div class="im_wrap">
	<img src="<?php echo base_url(); ?>assets/img/Untitled-2.png" class="img-responsive img_resp" alt="imgslide"/>
</div>
<div class="img_slider">
   	<div id="carousel-example" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carousel-example" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example" data-slide-to="1"></li>
			<li data-target="#carousel-example" data-slide-to="2"></li>
		</ol>
	  	<div class="carousel-inner">
			<div class="item active">
		  		<a href="#"><img src="<?php echo base_url(); ?>assets/img/Untitled-2.png" class="img-responsive img_resp" alt="imgslide"/></a>
			</div>
			<div class="item">
				<a href="#"><img src="<?php echo base_url(); ?>assets/img/Untitled-2.png" class="img-responsive img_resp" alt="imgslide_first"/></a>
			</div>
			<div class="item">
				<a href="#"><img src="<?php echo base_url(); ?>assets/img/Untitled-2.png" class="img-responsive img_resp" alt="imgslide_third"/></a>
			</div>
		</div>
	  	<a class="left carousel-control" href="#carousel-example" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
	  	</a>
	  	<a class="right carousel-control" href="#carousel-example" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
	  	</a>
	</div>
</div>
<!--section-->
<div class="web_info">
	<div class="container">
	    <div class="row">
		    <div class="web_info_forms">
			    <div class="col-md-7 cols_fo cols_fofirst">
					<?php if(!empty($home_section1)) { echo $home_section1[2]['page_detail']; } ?>
				</div>
				<div class="col-md-5 pannel_padd rightform">
					<div class="panel panel-default">
						<div class="panel-heading pannel_head">
							<h2 class="panel-title">SCHEDULE A RIDE</h2>
						</div>
						<div class="panel-body">
							<form name="schedule_ride" action="<?php echo base_url(); ?>schedule-ride" method="POST" autocomplete="off">
								<div class="row col_padding">	
								<!---->
								<?php
			                		$success = $this->session->flashdata("success");
			                		$fail = $this->session->flashdata('fail');
			                		if($success) { ?>
					                    <div class="alert alert-success">
					                    	<?php echo $success; ?>
					                    </div>
					                  <?php
					                  }
					                if($fail){ ?>
					                    <div class="alert alert-danger">
					                        <?php echo $fail; ?>
					                    </div>
					                <?php } ?>
									<div class="col-md-12 col_padding">
										<div class="top-form text-left">
											<!---->
											<div class="form-group">
												<label class="control-label col-sm-6 pdlr-0" for="datepicker1"><span class="glyphicon glyphicon-calendar"></span>&nbsp;Date of Service 
													<div class="req-red">*</div></label>
												<div class="col-sm-6 pdlr-0">
													<div class="input-group">
														<input type="text" class="form-control" id="datepicker1" value="<?php echo set_value('date'); ?>" name="date" placeholder="YYYY-MM-DD">													
													</div>
													<?php echo form_error('date', '<div class="error">', '</div>'); ?>															
												</div>
											</div>
											<!---->	 
											<div class="form-group">
												<label class="control-label col-sm-6 pdlr-0" for="aptime"><span class="glyphicon glyphicon-time"></span>&nbsp;Appointment Time <div class="req-red">*</div></label>
												<div class="col-sm-6">
													<div class="input-group">
														<input type="text" name="appointment_time" id="aptime" value="<?php echo set_value('appointment_time'); ?>" class="timepicker form-control" placeholder="00: 00: 00"/>												
													</div>		
													<?php echo form_error('appointment_time', '<div class="error">', '</div>'); ?>													
												</div>
											</div>													
											<!---->	
											<div class="form-group">
												<label class="control-label col-sm-6 pdlr-0" for="pick-up-time"><span class="glyphicon glyphicon-time"></span>&nbsp;Pick a Time <div class="req-red">*</div></label>
												<div class="col-sm-6">
													<div class="input-group">
														<input type="text" name="pick_up_time" id="timepicker" value="<?php echo set_value('pick_up_time'); ?>" class="timepicker form-control" placeholder="00: 00: 00"/>												
													</div>		
													<?php echo form_error('pick_up_time', '<div class="error">', '</div>'); ?>													
												</div>
											</div>														
											<div class="form-group pickaddress">
												<label class="control-label col-sm-12 pdlr-0" for="pick-up-address"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>&nbsp;Pick Up Address</label>
												<div class="col-sm-12">
													<div class="input-group">
														<label class="control-label col-sm-6">country <div class="req-red">*</div></label>
														<div class="col-sm-6">
															<select class="form-control" id="country" name="country">
															<option value="" class="selecttxt" disable selected hidden>Select Country</option>
															<?php 
															if(!empty($countryData)){
															foreach($countryData as $country_name) {
															?>
															<option value="<?php echo $country_name['id']; ?>"<?php echo set_select('country',$country_name['id'],FALSE);?>><?php echo $country_name['country_name']?></option>
															<?php } } else { ?>	
															<option value=""></option>
															<?php } ?>
															</select>
														<?php echo form_error('country', '<div class="error">', '</div>'); ?>
														</div>											
													</div>															
												</div>	
												<div class="col-sm-12">
													<div class="input-group">
														<label class="control-label col-sm-6">State <div class="req-red">*</div></label>
														<div class="col-sm-6">
															<select class="form-control" id="state" name="state">
																<option class="selecttxt" value="" disable selected hidden>Select State</option>																			
															</select>	
															<?php echo form_error('state', '<div class="error">', '</div>'); ?>
														</div>
														
													</div>															
												</div>	
												<div class="col-sm-12">
													<div class="input-group">
														<label class="control-label col-sm-6">Street <div class="req-red">*</div></label>
														<div class="col-sm-6">
															<input type="text" name="pick_up_street" id="street" value="<?php echo set_value('pick_up_street'); ?>" maxlength="100" class="form-control input-sm" placeholder="Enter Street"/>						<?php echo form_error('pick_up_street', '<div class="error">', '</div>'); ?>						
														</div>
														
													</div>															
												</div>
												<div class="col-sm-12">
													<div class="input-group">
														<label class="control-label col-sm-6">Zipcode <div class="req-red">*</div></label>
														<div class="col-sm-6">
															<input type="text" id="zipcode" name="zipcode" value="<?php echo set_value('zipcode'); ?>" class="form-control input-sm" maxlength="6" placeholder="Enter Zipcode"/>										<?php echo form_error('zipcode', '<div class="error">', '</div>'); ?>	
														</div>
														
													</div>															
												</div>														
											</div>
											<!---->	
											</div>
										</div>
										<!--form-->
										<div class="new-frm">
											<!--element-->
											<div class="form-group">
												<label for="usertype" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Type of Services <div class="req-red">*</div></label>
												<select class="form-control" name="health_service" id="health_service">
													<option value="" disable selected hidden>Select</option>
													<?php
													if(!empty($hc_service)) {
													 foreach($hc_service as $key => $value) { ?>
													<option value="<?php echo $value['id']; ?>"<?php echo set_select('health_service', $value['id'], FALSE); ?>><?php echo $value['hs_name']; ?></option>
													<?php } } else { ?>
													<option value=""></option>
													<?php } ?>												
												</select>
												<?php echo form_error('health_service', '<div class="error">', '</div>'); ?>
											</div>	
											<!--element-->
											<div class="form-group">
												<label for="usertype" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Special Requests <div class="req-red">*</div></label>
												<select class="form-control" id="special_request" name="special_request">
													<option value="" disable selected hidden>Select</option>
													<?php 
													if(!empty($sp_request)) {	
													foreach($sp_request as $key => $value) { ?>
													<option value="<?php echo $value['id']; ?>"<?php echo set_select('special_request', $value['id'], FALSE); ?>><?php echo $value['sr_name']; ?></option>
													<?php } } else { ?>	
													<option value=""></option>
													<?php } ?>										
												</select>
												<?php echo form_error('special_request', '<div class="error">', '</div>'); ?>
											</div>	
											<!--element-->
											<div class="form-group">
												<label for="usertype" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Special Instructions <div class="req-red">*</div></label>
												<input type="text" name="special_instruction" class="form-control" id="special_instruction" value="<?php echo set_value('special_instruction'); ?>" maxlength="50">
												<?php echo form_error('special_instruction', '<div class="error">', '</div>'); ?>
											</div>
											<!--element-->
											<div class="form-group">
												<label for="usertype" class="text-uppercase"><img src="<?php echo base_url(); ?>assets/img/icon-9.png" alt="">Desired Transportation <div class="req-red">*</div></label>
												<select class="form-control" id="desire_transp" name="desire_transp">
													<option value="" disable selected hidden>Select</option>
													<?php
													if(!empty($desire_tran)) {
													foreach($desire_tran as $key => $value) { ?>
													<option value="<?php echo $value['id']; ?>"<?php echo set_select('desire_transp', $value['id'], FALSE); ?>><?php echo $value['dt_name']; ?></option>
													<?php } } else { ?>	
													<option value=""></option>
													<?php } ?>	
												</select>
												<?php echo form_error('desire_transp', '<div class="error">', '</div>'); ?>
											</div>
											<!--element-->
											<div class="form-group">
												<input type="submit" value="SUBMIT" class="btn btn-block btn-primary"/>
											</div>														
										</div>
									</div>
								</form>										
							</div>
						</div>
					</div>
					<div class="col-md-7 cols_fo cols_fosecond">
						<div class="media">
							<div class="media-left">
						 		<img src="<?php echo base_url(); ?>assets/img/01.png" class="media-object media_img" alt="technology"/>
							</div>
							<div class="media-body">
						  		<h3 class="media-heading">TECHNOLOGY</h3>
						  		<p>Technology is the driving force behind this well-designed platform. Using the latest and most up-to-date technology, our software and mobile application features allow us to maximize the flow of information to enhance the communication process, lower’s average cost per trip, and improve relations between providers and their clients.</p>
						  		<div class="media_a">
						      		<a href="<?php echo base_url();?>home-page-section">LEARN MORE</a>
						  		</div>
							</div>
					    </div>
					    <div class="media">
							<div class="media-left">
						    	<img src="<?php echo base_url(); ?>assets/img/02.png" class="media-object media_img" alt="fleet management"/>
							</div>
							<div class="media-body">
							    <h3 class="media-heading">FLEET MANAGEMENT</h3>
							    <p>TransCura encourages and incentivizes ride share programs and independent drivers along with other transportation resources to register to our platform. Our goal is to ensure members the peace of mind knowing there will always be transportation available and ready to meet their on-time demands. </p>
							  	<a href="<?php echo base_url();?>home-page-section">LEARN MORE</a>
							</div>
					    </div>
					    <div class="media">
							<div class="media-left">
							  <img src="<?php echo base_url(); ?>assets/img/03.png" class="media-object media_img" alt="members"/>
							</div>
							<div class="media-body">
							  <h3 class="media-heading">MEMBERS</h3>
							  <p>We have considered the two major problems in the transportation industry with on-time demands and increasing costs. Wherein, to meet our members’ on-time demands, we have designed a 15-minute out scheduling system to improve on-time arrivals. In addition, to help minimize unwanted costs and surges, we offer a cost-rate analysis feature to provide our members with affordable transportation options to choose from.</p>
							  <a href="<?php echo base_url();?>home-page-section">LEARN MORE</a>
							</div>
					    </div>
				        <div class="media">
							<div class="media-left">
						  		<img src="<?php echo base_url(); ?>assets/img/04.png" class="media-object media_img" alt="medical emergency"/>
							</div>
							<div class="media-body">
							    <h3 class="media-heading">NON-EMERGENCY MEDICAL TRANSPORTATION</h3>
							 	<p>The Medicaid and Medicare Non-Emergency Medical Transportation has been one of the most challenging programs to manage. After years of experience working with the nation’s top leading brokerage firms, we have successfully designed a 3-teir technology-based software system with many wonderful features to help minimize patient complaints, wasteful spending and fraud abuse.</p>
							  	<a href="<?php echo base_url();?>home-page-section">LEARN MORE</a>
							</div>
					  	</div>
					</div>		
				</div>
			</div>
  		</div>
	</div>
	<!--section-->
	<div class="web_inst">
		<div class="container">
  			<div class="row">
		        <div class="about_info">
				    <div class="all_info">
					    <div class="info_header">
							<div class="section-ttl">
								<h1 class="ifo_h"><?php if(!empty($home_section1)){ echo $home_section1[0]['page_title']; }?></h1>
								<div class="innerttl">
									<h1><?php if(!empty($home_section1)){ echo $home_section1[0]['page_title']; }?></h1>
								</div>
							</div>
						</div>
						<div class="text_ifo">
							<p><?php if(!empty($home_section1)){ 
								$string = $home_section1[0]['page_detail'];
											if (strlen($string) > 250) {													
											$stringCut = substr($string, 0, 700);
											$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
											}
								echo $string; }?></p>
							</div>
							<div class="about_btn">
								<!-- <button type="button" class="btn btn-success" 
								onclick="<?php echo base_url()?>home-page-section">Sign Up</button> -->
							    <button class="btn about_btn_info" onClick="window.location='<?php echo base_url('about-us');?>'">LEARN MORE</button>
							</div>
					  </div>
				</div>
  			</div>
		</div>
	</div>
	<!--section-->
	<div class="img_back_video">
		<div class="img_video_section">
		  	<div class="img_section">
				<img src="<?php echo base_url(); ?>assets/img/img-01.jpg" class="img-responsive video_img" alt="video img"/>
				<div class="video_sec">
				   	<h1>PAVING THE WAY</h1>
					<video class="video_sec_sc" controls>
					  	<source src="<?php echo base_url(); ?>assets/video/VideoHD.mp4" type="video/mp4"/>
					  	<source src="<?php echo base_url(); ?>assets/video/VideoHD.ogg" type="video/ogg"/>
					  	Your browser does not support HTML5 video.
					</video>
				</div>
		  	</div>
		</div>
	</div>
	<!--section-->
	<div class="solution_section section-pad">
		<div class="container">
	 		<div class="row">
	        	<div class="info_solution">
					<div class="section-ttl">
						<h1 class="solu_sec"><?php if(!empty($home_section1)){ echo $home_section1[1]['page_title']; } ?></h1>
						<div class="innerttl">
							<h1><?php if(!empty($home_section1)){ echo $home_section1[1]['page_title']; } ?></h1>
						</div>
					</div>
				</div>
				<div class="ifo_slu_pass">
					<p class="text-uppercase"><?php if(!empty($home_section1)){ echo $home_section1[1]['sub_title']; } ?></p>
				</div>
				
				<?php if(!empty($home_section1)){ echo $home_section1[1]['page_detail']; } ?>
				<div class="ifo_lastbtn">
				      <button class="btn ifo_btn_l">VIEW ALL</button>
				</div>
	  		</div>
		</div>
	</div>
	<!--section-->
	<div class="mobile_app">
		<div class="mobile_apps_sec">
			<div class="col-md-6 col_m">
				<div class="img_re">
					 <img src="<?php echo base_url(); ?>assets/img/banerimg_01.jpg" class="img-responsive mob_app_img" alt="banner"/>
				</div>
				<div class="app_text_btn">
	            	<p>
	            		Allows transportation providers 
						and independent drivers to register and upload 
						credentialing docs to our platform. Once ready to go, 
						drivers will have access to turn by turn navigation 
						with access to ALL relevant information and instructions 
						making transportation manageable, simple and fun. 
				 	</p>
                    <div class="btn_lat">							
						<button class="btn btn-outline" type="button" onClick="window.location='<?php echo base_url('home-page-section');?>'">VIEW MORE</button>
					</div>
				</div>
			</div>
			<div class="col-md-6 col_m">
				<div class="img_re">
					<img src="<?php echo base_url(); ?>assets/img/banerimg_02.jpg" class="img-responsive mob_app_img" alt="banner first"/>
				</div>
				<div class="app_text_btn btnsecond">
		            <p>
		            	Allows members to schedule
						and manage trips, edit personal information, 
						access driver information, track rides,
						cancel rides, request for a return ride 
						and rate a trip all within the comforts 
						oftheir smart phone.
					</p>
                    <div class="btn_lat">							
						<button class="btn btn-outline" type="button" onClick="window.location='<?php echo base_url('home-page-section');?>'">VIEW MORE</button>
					</div>
				</div>
			</div>
		</div>
		<div class="mobile_app_head">
		    <div class="container">
			    <div class="row">
					<div class="app_head">
						<h2>MOBILE APP SOLUTIONS FOR DRIVERS AND MEMBERS</h2>
					</div>
				</div>
			</div>
		</div>
	</div>	
    <div class="info_img_gallery">
        <div class="container">
		    <div class="row">
			    <div class="img_gallery_head">
					<div class="img_head_f">
						<div class="section-ttl">
							<h1 class="img_head_sac">WHAT EVERYONE SAYING ABOUT TRANSCURA</h1>
							<div class="innerttl">
								<h1>WHAT EVERYONE SAYING ABOUT TRANSCURA</h1>
							</div>
						</div>
					</div>
				</div>
			</div>	
		</div>
    </div>  
	<div class="img_slider_sec">
		<div class="container">
        	<div class="row">
			    <div class="col-md-12" data-wow-delay="0.2s">
					<div class="carousel slide" data-ride="carousel" id="quote-carousel">
						<?php 
						$i=0;
						$j=0;
						$active ='';
						if(!empty($testimonial)) {									
						 ?>
						<!-- Bottom Carousel Indicators -->
						<ol class="carousel-indicators">
							<?php foreach ($testimonial as $key => $value) { ?>
							<li data-target="#quote-carousel" data-slide-to="<?php echo $i; ?>" <?php if($i == 0){ echo 'class="active"'; } ?> ><img class="img-responsive " src="<?php echo base_url().$value['tm_image']; ?>">
							</li>
							<?php $i++; } ?>									
						</ol> 
						<div class="carousel-inner text-center">
							<!-- Quote 1 -->
							<?php foreach ($testimonial as $key => $value) { ?>	
							<div class="item <?php if($j == 0){ echo 'active'; } ?>">
								<div class="row">
									<div class="col-sm-12 sm_box">
										<p><?php
											$string = $value['tm_description'];
											if (strlen($string) > 250) {													
											$stringCut = substr($string, 0, 250);
											$string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 
											}
										 echo $string; ?></p>
										<h4><?php echo $value['tm_name']; ?></h4>
										<!-- <div class="img_star">
										        <img src="<?php //echo base_url().$value['tm_stars']; ?>" class="img-responsive lsta_slider" alt="star_client"/>
										</div> -->
									</div>
								</div>
							</div>
							<?php $j++; }?>									
						</div>
						<?php  } else { }?>
						<!-- Carousel Slides / Quotes -->
						<!-- Carousel Buttons Next/Prev -->
						<div class="navigation-arr">
							<a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
							<a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
						</div>
					</div>
				</div>
 			</div>
 		</div>	
	</div>
</div>
</div>	 
<?php $this->load->view('user/footer');?>