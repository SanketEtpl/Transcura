<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        FAQ Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">FAQ management</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6 resp_share">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="listFaq(0,'id','DESC');">
                      Search
                    </a>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getFaq();">
                      Add New
                    </a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
				  <table class="table table-bordered table-striped dataTable" id="listFaq" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th width="25%" data-name="question" data-order="DESC" class="sorting">Question</th>                      
                         <th width="25%" >Answer</th>                        
                        <th width="10%" class="">Action</th>
                      </tr>
                    </thead>
                      <tbody>                        
                      </tbody>
                    </table>
					</div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="faqModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>admin/FAQ/saveFaq" id="FaqForm" name="FaqForm" class="valida">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Manage FAQ's</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="Question">Question</label>
                  <input type="hidden" id="page_key" name="page_key">
                  <input type="text" placeholder="Question" id="Question" name="Question" filter="question" maxlength="100" required="true" class="form-control"  autocomplete="off">
                </div>
              </div>
              
            </div>
          
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="control-label" for="answer">Answer</label>                 
                 <textarea placeholder="answer" style='resize:none;' rows="3" name="answer" id="answer" maxlength="255" filter="answer" required="true" class="form-control" autocomplete="off"></textarea>
                </div>
              </div>            
            </div>

          
          </div>
          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button"  data-dismiss="modal" data-toggle="modal" >Close</button>
            <button class="btn btn-info" type="submit">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/faq.js"></script>
<?php $this->load->view('footer'); ?>
