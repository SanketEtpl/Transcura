  <footer class="main-footer">
   <div style='text-align:center;'>
    <strong>Copyright &copy; <?php echo date("Y"); ?> Transcura.</strong> All rights reserved.
    </div>
  </footer>
</div>

<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>-->
<!--script src="<?php //echo base_url(); ?>js/jquery-1.9.1.min.js"></script-->
<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>


<!--<script src="<?php echo base_url();?>js/bootstrap-datepicker.js"></script>-->
<script src="<?php echo base_url();?>js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>

<script src="<?php echo base_url(); ?>js/app.min.js"></script>
<script src="<?php echo base_url(); ?>js/demo.js"></script>
<script src="<?php echo base_url(); ?>js/toastr.min.js"></script>
<script src="<?php echo base_url(); ?>js/admin/customFormValidation.js"></script>

<!--<script src='https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js'></script>-->
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script type="text/javascript">
	$(".modal .modal-footer .btn-default, .modal .modal-header").click(function(){
		toastr.warning("You have cancelled your request.","Info:");
	});
</script>

<script src="<?php echo base_url(); ?>js/valida.2.1.7.js"></script>
  <script>
	  $('.valida').valida();
	  $('.user_validation').valida();
	 // $("#example1").datepicker({ dateFormat: 'yy-mm-dd'});

var today = new Date();
			var n = today.getFullYear();
			var y = n-70;
			//alert(y);
			//Datepicker
			$( "#example1" ).datepicker({	
				//defaultDate: new Date("1988-02-25"),
				changeMonth:true,
				changeYear:true,
				maxDate:0,								
				yearRange: y+":{ n }"	
			});	
</script>
</body>
</html>
