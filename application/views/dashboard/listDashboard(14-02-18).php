 <?php $this->load->view('header'); ?>
 <style>





.panel-yellow > .panel-heading {background-color: #f0ad4e; border-color: #f0ad4e; color: white; } .panel-yellow > a {color: #f0ad4e; } .panel-yellow {border-color: #f0ad4e; } .panel-red {border-color: #d9534f; } .panel-red > .panel-heading {background-color: #d9534f; border-color: #d9534f; color: white; } .panel-red > a {color: #d9534f; } .panel-green {border-color: #5cb85c; } .panel-green > .panel-heading {background-color: #5cb85c; border-color: #5cb85c; color: white; } .panel-green > a {color: #5cb85c; } .huge {font-size: 40px; }
.headerName{font-size: 18px !important;}
#usersChart, #feedChart, #recordingChart, #chapterChart {padding:0px; width:530px; height:400px;}
#wrapper
{
 margin:0 auto;
 padding:0px;
 text-align:center;
 width:995px;
}
#wrapper h1
{
 margin-top:50px;
 font-size:45px;
 color:#585858;
}
#wrapper h1 p
{
 font-size:18px;
}
#employee_piechart
{
 padding:0px;
 width:600px;
 height:400px;
 margin-left:190px;
}
 </style> <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard" ><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
          <div class="col-xs-12">
              <div class="box">
                  <div class="box-body">
                      <div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-user fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge"><?php echo $driver_count[0]['cnt']; ?></div>
                                            <div class="headerName">Drivers</div>
                                        </div>
                                    </div>
                                </div>
                                <a href="<?php echo base_url(); ?>drivers">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="panel panel-green">
                              <div class="panel-heading">
                                  <div class="row">
                                      <div class="col-xs-3">
                                           <i class="fa fa-users fa-5x"></i>
                                      </div>
                                      <div class="col-xs-9 text-right">
                                          <div class="huge"><?php echo $patient_count[0]['cnt']; ?></div>
                                          <div class="headerName">NEMT User</div>
                                      </div>
                                  </div>
                              </div>
                            <a href="<?php echo base_url(); ?>patients">
                                  <div class="panel-footer">
                                      <span class="pull-left">View Details</span>
                                      <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                      <div class="clearfix"></div>
                                  </div>
                              </a>
                          </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="panel panel-yellow">
                              <div class="panel-heading">
                                  <div class="row">
                                      <div class="col-xs-3">
                                          <i class="fa fa-user-md fa-5x"></i>
                                      </div>
                                      <div class="col-xs-9 text-right">
                                          <div class="huge"><?php echo $member_count[0]['cnt']; ?></div>
                                          <div class="headerName">Member User</div>
                                      </div>
                                  </div>
                              </div>
                              <a href="<?php echo base_url(); ?>member">
                                  <div class="panel-footer">
                                      <span class="pull-left">View Details</span>
                                      <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                      <div class="clearfix"></div>
                                  </div>
                              </a>
                          </div>
                      </div>
  <div class="col-lg-4 col-md-4 col-sm-4">
                          <div class="panel panel-yellow">
                              <div class="panel-heading">
                                  <div class="row">
                                      <div class="col-xs-3">
                                          <i class="fa fa-user-md fa-5x"></i>
                                      </div>
                                      <div class="col-xs-9 text-right">
                                          <div class="huge"><?php echo $serviceprovider_count; ?></div>
                                          <div class="headerName">Health care provider</div>
                                      </div>
                                  </div>
                              </div>
                              <a href="<?php echo base_url(); ?>doctors">
                                  <div class="panel-footer">
                                      <span class="pull-left">View Details</span>
                                      <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                      <div class="clearfix"></div>
                                  </div>
                              </a>
                          </div>
                      </div>

                   <!--  serviceprovider_count  <div class="col-lg-3 col-md-3 col-sm-3">
                          <div class="panel panel-red">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                          <i class="fa fa-users fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge"><?php //echo $serviceprovider_count[0]['cnt']; ?></div>
                                            <div class="headerName">Customer support </div>
                                        </div>
                                    </div>
                                </div>
                                <a href="#">
                                    <div class="panel-footer">
                                        <span class="pull-left">View Details</span>
                                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                        <div class="clearfix"></div>
                                    </div>
                                </a>
                            </div>
                      </div> -->
                  </div>
              </div>
          </div>
      </div>
 <!--      <div class="row">
        <div class="col-xs-12">
              <div class="box">
                  <div class="box-body">
                    <div class="col-lg-6 col-md-6 col-sm-6" id="PatientChart"></div>
                    <div class="col-lg-6 col-md-6 col-sm-6" id="DriverChart"></div>
                    <div class="col-lg-6 col-md-6 col-sm-6" id="DoctorChart"></div>
                    <div class="col-lg-6 col-md-6 col-sm-6" id="serviceproviderChart"></div>
                  </div>
              </div>
        </div>
      </div> -->
    </section>
  </div><script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script src="<?php echo base_url(); ?>js/admin/dashboard.js"></script>
    <?php $this->load->view('footer'); ?>