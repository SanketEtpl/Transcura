 <?php $this->load->view('header'); ?>

 </style> <div class="content-wrapper">
    <section class="content-header">
      <h1>
        User profile <?php //print_r($_SESSION);die;?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">User profile</li>
      </ol>
    </section>
    <section class="content">
    <div class="row">
      <div class="col-xs-6 col-sm-7 col-md-8 profile-left">
        <!-- Profile Image -->
        <div class="box box-primary">
          <div class="box-body box-profile">
            <img class="profile-user-img img-responsive img-circle" src="<?php echo site_url('images/avatar5.png');?>" alt="User profile picture">
            <h3 class="profile-username text-center"><?php echo $_SESSION['auth_admin']['username'] ;?></h3>
            <p class="text-muted text-center"><?php echo print_r($_SESSION['auth_admin']['first_name']."&nbsp".$_SESSION['auth_admin']['first_name']);?></p>
            <ul class="list-group list-group-unbordered">
              <li class="list-group-item">
                <b>Email </b> <a class="pull-right"><?php echo $_SESSION['auth_admin']['email'] ;?></a>
              </li>
              <li class="list-group-item">
                <b>Phone </b> <a class="pull-right"><?php echo $_SESSION['auth_admin']['phone'] ;?></a>
              </li>
              <li class="list-group-item">
                <b>Status </b> <a class="pull-right">Active</a>
              </li>
            </ul>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->



<div class="col-xs-12 col-sm-7 col-md-8 profile-right ">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#activity" data-toggle="tab">User Info</a></li>
            <li><a href="#settings" data-toggle="tab">Change password</a></li>
          </ul>
          <div class="tab-content">
            <div class="active tab-pane" id="activity">
              <form class="form-horizontal" id="userinfo_form" method="post">
                <div class="form-group">
                  <label for="inputName" class="col-xs-12 col-sm-4 col-md-3 control-label">First Name<span class="required-star">*</span></label>
                  <div class="col-xs-12 col-sm-8 col-md-9">
                    <input type="text" class="form-control validate[required] only-alphabets" value="<?php //echo $userinfo[0]->fname;?>" name="username" id="username" placeholder="First Name">
                    <div class="add-form-error-msg" id="error_username"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-xs-12 col-sm-4 col-md-3 control-label">Last Name<span class="required-star">*</span></label>
                  <div class="col-xs-12 col-sm-8 col-md-9">
                    <input type="text" value="<?php //echo $userinfo[0]->lname;?>" class="form-control only-alphabets" name="fullname" id="fullname" placeholder="Last Name">
                    <div class="add-form-error-msg" id="error_fullname"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-xs-12 col-sm-4 col-md-3 control-label">Emailid</label>
                  <div class="col-xs-12 col-sm-8 col-md-9">
                    <input type="text" class="form-control" value="<?php //echo $userinfo[0]->email;?>" name="emailid" id="emailid" placeholder="Emailid" disabled>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputExperience" class="col-xs-12 col-sm-4 col-md-3 control-label">Mobile Number<span class="required-star">*</span></label>
                  <div class="col-xs-12 col-sm-8 col-md-9">
                    <input type="text" class="form-control" id="mobileno" name="mobileno" value="<?php //echo $userinfo[0]->mobile_no;?>"  placeholder="Mobile Number">
                    <div class="add-form-error-msg" id="error_mobileno"></div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-offset-2 col-md-10">
                    <button type="submit" class="btn btn-primary pull-right" value="userinfo" name="userinfo">Submit</button>
                  </div>
                </div>
              </form>
            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="settings">
              <form id="change_password" class="form-horizontal" method="post">
                <div class="form-group">
                  <label for="inputName" class="col-xs-12 col-sm-4 col-md-3 control-label">Old Password</label>
                  <div class="col-xs-12 col-sm-8 col-md-9">
                    <input type="password" class="form-control" name="oldpassword" id="oldpassword" placeholder="Old Password">
                    <div class="add-form-error-msg" id="error_oldpassword"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-xs-12 col-sm-4 col-md-3 control-label">New Password</label>
                  <div class="col-xs-12 col-sm-8 col-md-9 ">
                    <input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="New Password">
                    <div class="add-form-error-msg" id="error_newpassword"></div>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputName" class="col-xs-12 col-sm-4 col-md-3  control-label">Confirm New Password</label>
                  <div class="col-xs-12 col-sm-8 col-md-9">
                    <input type="password" class="form-control" name="cpassword" id="cpassword" placeholder="Confirm New Password">
                    <div class="add-form-error-msg" id="error_cpassword"></div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <input type="hidden" name="hform" id="hform" value="1">
                    <button type="submit" name="forgot_pass" value="forgot_pass" class="btn btn-primary pull-right">Submit</button>
                  </div>
                </div>
              </form>
              <div class="old-pass-error error"></div>



    </div><!-- /.row --> 
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<!-- <script src="<?php echo site_url()?>assets/js/admin/custome/user.js"></script>
 -->    <script src="<?php echo base_url(); ?>js/admin/user.js"></script>
  </div>
    <?php $this->load->view('footer'); ?>