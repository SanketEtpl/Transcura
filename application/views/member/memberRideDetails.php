<?php $this->load->view("user/header"); ?>
<?php $this->load->view('frontMember/memberHeader'); ?>

<div class="all_comp_wrapp">
	<div class="container">
		<div class="row">
			<div class="comp_inner">
				<h3 class="heal_head">
					SCHEDULE A RIDE TRIP DETAILS		
					<div id="error_message"></div>
					<!-- <span class="back_box"><button class="back_btn customer_complaints" id="customer_complaints_bk" name="customer_complaints_bk">Back</button></span> -->

					<input type="hidden" name="" id="memberSourceAddr" value="<?php  echo $this->session->userdata('memberSource');?>">
					<input type="hidden" name="" id="memberDestinationAddr" value="<?php  echo $this->session->userdata('memberDestination');?>">
				</h3>
				<div class="comp_box">
					<form name="customer_complaint_form" method="POST" action="" autocomplete="off">
						<div class="row">
							<div class="col-md-6 comp_lab">
								<label>Ride Date:</label>
								<div class="comp_inpt">
									<?php echo $this->session->userdata('memberRestaurant')['sr_date'];?>
								</div>								
							</div>
							<div class="col-md-6 comp_lab">
								<label>Ride Time :</label>
								<div class="comp_inpt">
									<?php echo $this->session->userdata('memberRestaurant')['sr_pick_up_time'];?>
								</div>								
							</div>
							<div class="col-md-12 comp_lab">
								<label>Pick Up Address :</label>
								<div class="col-md-4 comp_lab">
									<label>Street :</label>
									<div class="comp_inpt">
										<?php echo $this->session->userdata('memberRestaurant')['sr_street'];?>
									</div>		
								</div>		
								<div class="col-md-4 comp_lab">
									<label>State :</label>
									<div class="comp_inpt">
										<?php if(!empty($state[0])) echo $state[0]['state_name']; else echo '';?>
									</div>		
								</div>	
								<div class="col-md-4 comp_lab">
									<label>Zipcode :</label>
									<div class="comp_inpt">
										<?php echo $this->session->userdata('memberRestaurant')['sr_zipcode'];?>
									</div>		
								</div>					
							</div>
							<!-- <div class="col-md-4 comp_lab">
								<label> Type of Ride:</label>
								<div class="comp_inpt">
									<?php if(!empty($hc_service[0]))echo $hc_service[0]['hs_name']; else echo '';?>
								</div>								
							</div>
							<div class="col-md-4 comp_lab">
								<label> Special Requests:</label>
								<div class="comp_inpt">
									<?php if(!empty($sp_request[0])) echo $sp_request[0]['sr_name']; else echo ""; ?>
								</div>								
							</div>
							<div class="col-md-4 comp_lab">
								<label> Health Issues you would want your transportaion provider to be aware of:</label>
								<div class="comp_inpt">
									<?php //echo $hc_service[0]['hs_name'];?>
								</div>								
							</div>
							<div class="col-md-4 comp_lab">
								<label> Desired Transportation :</label>
								<div class="comp_inpt">
									<?php if(!empty($desire_tran[0])) echo $desire_tran[0]['dt_name']; else echo ""; ?>
								</div>								
							</div> -->
						</div>	
					</form>	
					<div class="pay_btn_box">
						
						<button type="button" class="req_btn comp_btn" id="bookMemberRequest" data-toggle="modal">Book Ride</button>	<!-- data-target="#subcomp_pop" -->				
					</div>	
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="memberRideModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
        <div id="responseMsg"></div>
        <div id="rideTripId"></div>
      </div>
      
    </div>
  </div>
</div>


<!--submit complaints popup-->

<?php $this->load->view('user/footer'); ?>