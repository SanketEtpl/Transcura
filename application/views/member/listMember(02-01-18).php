<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Member
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active"> Member users</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6 resp_share">
                  <div id="example1_filter" class="dataTables_filter">
                    <label>Search:<input type="text" class="form-control input-sm" placeholder="Search" id="txt_search" name="txt_search" aria-controls="example1"></label>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="listMember(0,'user','DESC');">
                      Search
                    </a>
                    <a class="btn btn-primary" href="javascript:void(0);" onclick="getMember(this);">
                      Add New
                    </a>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
				   <div class="table-responsive">	
					  <table class="table table-bordered table-striped dataTable" id="listMember" role="grid" aria-describedby="example1_info">
						<thead>
						  <tr role="row">
							<th width="25%" data-name="fname" data-order="DESC" class="sorting">Name</th>                      
							<th width="25%" data-name="email" data-order="ASC" class="sorting">Email</th>
							<th width="15%" data-name="phone" data-order="ASC" class="sorting">Phone</th>
							<th width="15%" >Activation</th>
							<th width="10%" class="">Action</th>
						  </tr>
						</thead>
						  <tbody>                        
						  </tbody>
						</table>
					</div>
				  </div>
                </div>
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite"></div>
                  </div>
                  <div class="col-sm-7">
                    <div class="dataTables_paginate paging_simple_numbers" id="paginate_links"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <div id="userModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>admin/Member/saveMember" id="userForm" name="userForm" class="user_validation" enctype="multipart/form-data" > <?php //print_r($insurancelist);?>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Manage Member User</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="full_name">Full name</label>
                  <input type="hidden" id="user_key" name="user_key">
                  <input type="text" placeholder="Full name" id="full_name"  filter="name" required="true" name="full_name" class="form-control fullname" maxlength="50" autocomplete="off">
                </div>
              </div>              
              <div class="col-md-6">
                <div class="form-group" id="dateissue">
                  <label class="control-label" for="date_of_birth">Date of birth</label>
                  <input type="text" placeholder="click to show datepicker" id="example1" required="true" name="birth" class="form-control" readonly>
                </div>
              </div>
          
             <div class="col-md-6">
                <div class="form-group">
                  <label for="country" class="control-label" for="country">Country</label>
                  <select class="form-control" name="country" id="country" required="true" >
                    <option value="" class="selecttxt" disable selected hidden>Select country</option>
                    <?php 
                    if(!empty($countryData)){
                    foreach($countryData as $country_name) {
                    ?>
                    <option value="<?php echo $country_name['id']; ?>"><?php echo $country_name['country_name']?></option>
                    <?php } } else { ?> 
                    <option value=""></option>
                    <?php } ?>  
                  </select>             
                </div>  
              </div>
              <div class="col-md-6">
                <div class="form-group"> 
                  <label for="state" class="control-label" >State</label>
                  <select class="form-control" id="state" name="state" required="true" >
                    <option class="selecttxt" value="" disable selected hidden>Select state</option>
                  </select>                 
                </div>
              </div>
              <div class="col-md-6">             
                <div class="form-group">
                  <label for="city" class="control-label">City</label>
                  <select class="form-control" id="city" name="city" required="true" >
                    <option class="selecttxt" value="">Select city</option>
                  </select> 
                  <!-- <input type="text" name="city" class="form-control" id="city" placeholder="City" maxlength="100" autocomplete="off"> -->
                </div>    
              </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="street">Street</label>
                  <textarea placeholder="Street" id="street" name="street" filter="street" class="form-control" required="true" rows="10" autocomplete="off"></textarea> 
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="countyL">County</label>
                  <input type="text" name="county" class="form-control" id="County" placeholder="County" maxlength="20" autocomplete="off">
                </div>  
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="zipcode">Zip code</label>
                 <input type="text" placeholder="Zip code" id="zipcode"  filter="zipcode"  required="true" maxlength="15" name="zipcode" class="form-control" >
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="email">Email address</label>
                  <input type="text" placeholder="Email address" id="email" filter="email" maxlength="50" required="true" name="email_address" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="phone">Phone</label>
                  <input type="text" placeholder="Contact number" filter="phone" required="true" maxlength="20" id="phone" name="phone" class="form-control">
                </div>
              </div>                       
             
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="econtact_name">Emergency contact name</label>                 
                  <input type="text" placeholder="Emergency contact name" id="econtact_name" filter="name"  required="true" name="econtact_name" maxlength="50" class="form-control fullname" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="econtact_no">Emergency contact no</label>
                  <input type="text" placeholder="Emergency contact number" id="econtact_no" filter="phone" required="true" name="econtact_no"  maxlength="13" class="form-control" autocomplete="off">
                </div>
              </div>          
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="signature">Are you able to provide signature?</label>
                  <select name="signature" class="form-control" id="signature" required="true">
                    <option value="" selected="true">Select signature</option>
                    <option value="1">Yes</option>
                    <option value="0">No</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group" id="imgChange">
                  <label class="control-label" for="subTitle"><span>Add new image</span></label>                  
                  <input type="file" placeholder="Profile image" id="pimage" name="pimage"  size="20" class="form-control" autocomplete="off" onchange="document.getElementById('pimage').src = window.URL.createObjectURL(this.files[0])"> 
                </div>
              </div>
              <div class="col-md-6">
				<div class="form-group lst_mem" id="theDiv"></div>
              </div>     


              <!-- <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="Personal">Personal document</label>
                 <input type="file" placeholder="personal document" id="personal_doc" name="personal_doc"  class="form-control" autocomplete="off" onchange="document.getElementById('person_doc').src = window.URL.createObjectURL(this.files[0])"> 
                </div>
              </div> -->
              <!--<div class="col-md-6" id="pdf"></div>-->    
            </div>
          </div>
          <div class="modal-footer">
            <input type="hidden" id="oldimg" name="oldimg">
            <input type="hidden" id="oldpdf" name="oldpdf">
            <button data-dismiss="modal" class="btn btn-default" type="reset">Close</button>
            <button class="btn btn-info tiny" type="submit">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/member.js"></script>
  <?php $this->load->view('footer'); ?>
