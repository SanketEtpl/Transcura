<?php $segmentMenu = $this->uri->segment(1); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Transcura | Admin Panel</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" href="<?php echo base_url(); ?>images/res-icon.png">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap/css/bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap/css/datepicker.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/plugin/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/toastr.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery-ui.css">
  <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
  <link  rel="stylesheet" href="<?php echo base_url(); ?>css/admin_media.css">
  <!--<script src="<?php echo base_url(); ?>js/jquery-2.2.3.min.js"></script>-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script type="text/javascript">
    var BASEURL = '<?php echo base_url(); ?>';
 $(function() {
      var today = new Date();
      var n = today.getFullYear();
      var y = n-70;
      //alert(y);
      //Datepicker
      $( "#example1" ).datepicker({ 
        //defaultDate: new Date("1988-02-25"),
        changeMonth:true,
        changeYear:true,
        maxDate:0,                
        yearRange: y+":{ n }" 
      }); 

	$( ".expireDate" ).datepicker({ 
        //defaultDate: new Date("1988-02-25"),
        changeMonth:true,
        changeYear:true,
        minDate:0,  
        maxDate:"+5y"                      
      }); 
	$( "#driverDate" ).datepicker({ 
        //defaultDate: new Date("1988-02-25"),
        changeMonth:true,
        changeYear:true,
        maxDate:"-18y",                
        //yearRange: y+":{ n }" 
      }); 
  });
  </script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->          
      <img src="<?php echo base_url(); ?>images/imgpsh_fullsize.png" class="main">
      <img src="<?php echo base_url(); ?>images/res-icon.png" style="display: none" class="res">
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>App</b>-Tech</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <?php  $username = $this->session->auth_admin; 



          ?><?php //echo $username['profile'] ;?>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <span class="user_img"><img src="<?php echo base_url().$username['profile'] ;?>" class="user-image" alt="User Image"/></span>
              <span class="hidden-xs"><?php echo ucfirst($username['first_name']) ;?> </span>

            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo  base_url().$username['profile'] ;?>"  class="img-circle" alt="User Image" />
                <p><?php echo $username['first_name']; ?> 
              
                  <small> <?php echo "Member since ".date('M Y', strtotime($username['created_date']));  ?> </small>
                </p>
              </li>
              <!-- Menu Body -->
              <!-- <li class="user-body">
                <div class="col-xs-4 text-center">
                  <a href="#">Followers</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Sales</a>
                </div>
                <div class="col-xs-4 text-center">
                  <a href="#">Friends</a>
                </div>
              </li> -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo base_url(); ?>admin/Profile/" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo base_url(); ?>admin/login/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->          
        </ul>
      </div>
    </nav>
  </header>
<aside class="main-sidebar"><?php //print_r($_SESSION['first_name'].$_SESSION['last_name']); ?>
  <section class="sidebar">
    <ul class="sidebar-menu">
      <li class="treeview"><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-tachometer" aria-hidden="true"></i> <span>Dashboard</span></a>
        </li>
      <li class="treeview"><a href="<?php echo base_url(); ?>patients"><i class="fa fa-fw fa-users"></i> <span>NEMT Users </span></a></li>
       <li class="treeview"><a href="<?php echo base_url(); ?>member"><i class="fa fa-fw fa-users"></i> <span>Member  </span></a></li>
    
   </ul>
   <ul class="sidebar-menu">
      <li class="treeview<?php if(in_array($segmentMenu, array("doctors_category",""))){echo " active";} ?>">
          <a href="#">
            <i class="fa fa-user-md"></i> <span>Doctors/Healthcare Providers </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           
            <li><a href="<?php echo base_url(); ?>doctors_category"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Category</span></a></li>
           <li><a href="<?php echo base_url(); ?>doctors"><i class="fa fa-user-md" aria-hidden="true"></i><span>Doctors/Healthcare Providers</span></a></li>
          </ul>
        </li>     
      </ul>

      <ul class="sidebar-menu">
        <li class="treeview<?php if(in_array($segmentMenu, array("doctors_category",""))){echo " active";} ?>">
          <a href="#">
            <i class="fa fa-user" aria-hidden="true"></i><span>Driver/Transportation Provider </span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           
            <li><a href="<?php echo base_url(); ?>service_types"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Service types</span></a></li>
           <li><a href="<?php echo base_url(); ?>drivers"><i class="fa fa-fw fa-ambulance"></i> <span>Driver/Transportation Provider</span></a></li>
          </ul>
        </li>
      </ul>
      <ul class="sidebar-menu">        
        <li class="treeview<?php if(in_array($segmentMenu, array("doctors_category",""))){echo " active";} ?>">
          <a href="#">
            <i class="fa fa-list" aria-hidden="true"></i><span>Schedule management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">           
            <li><a href="<?php echo base_url(); ?>admin/Destination"><i class="fa fa-sitemap" aria-hidden="true"></i><span>Destination</span></a></li>
           <li><a href="<?php echo base_url(); ?>admin/Special_request"><i class="fa fa-fw fa-ambulance"></i> <span>Special request</span></a></li>
           <li><a href="<?php echo base_url(); ?>admin/Desired_transportation"><i class="fa fa-fw fa-ambulance"></i> <span>Desired transportation</span></a></li>

<li> <a href="<?php echo base_url(); ?>schedule">  <i class="fa fa-commenting-o" aria-hidden="true"></i>
        <span>Schedule Management</span>
      </a></li>


           

      
          </ul>
        </li>
      </ul>
<ul class="sidebar-menu">        
        <li class="treeview<?php if(in_array($segmentMenu, array("doctors_category",""))){echo " active";} ?>">
          <a href="#">
            <i class="fa fa-list" aria-hidden="true"></i><span>Home content management</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-down pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">           
            <li><a href="<?php echo base_url(); ?>first_section"><i class="fa fa-sitemap" aria-hidden="true"></i><span>First section</span></a></li>
            <li><a href="<?php echo base_url(); ?>second_section"><i class="fa fa-fw fa-ambulance"></i> <span>Second section</span></a></li>
          </ul>
        </li>
      </ul>
    <ul class="sidebar-menu">  <li class="treeview"><a href="<?php echo base_url(); ?>admin/Admin_ride_status"><i class="fa fa-database" aria-hidden="true"></i> <span>Ride request</span></a></li></ul>  
    <ul class="sidebar-menu">  <li class="treeview"><a href="<?php echo base_url(); ?>contents"><i class="fa fa-database" aria-hidden="true"></i> <span>Content Management</span></a></li></ul>
    <ul class="sidebar-menu">  <li class="treeview"><a href="<?php echo base_url(); ?>Setting">  <i class="fa fa-cog" aria-hidden="true"></i>
    <span>Setting</span></a></li></ul>
    <ul class="sidebar-menu">  <li class="treeview"><a href="<?php echo base_url(); ?>admin/Car_category"><i class="fa fa-database" aria-hidden="true"></i> <span>Car category</span></a></li></ul>  
    <ul class="sidebar-menu">  <li class="treeview"><a href="<?php echo base_url(); ?>admin/Member_service_type"><i class="fa fa-database" aria-hidden="true"></i> <span>Member service type</span></a></li></ul>
    <ul class="sidebar-menu">  <li class="treeview"><a href="<?php echo base_url(); ?>admin/Notification"><i class="fa fa-database" aria-hidden="true"></i> <span>Notification</span></a></li></ul>  


  <ul class="sidebar-menu">  <li class="treeview"><a href="<?php echo base_url(); ?>faq">  <i class="fa fa-question-circle" aria-hidden="true"></i>
  <span>FAQ Management</span></a></li></ul>
  <ul class="sidebar-menu">
    <li class="treeview">
      <a href="<?php echo base_url(); ?>testimonial">  <i class="fa fa-commenting-o" aria-hidden="true"></i>
        <span>Testimonial</span>
      </a>
    </li>
  </ul>
 <!-- <ul class="sidebar-menu">
    <li class="treeview">
      <a href="<?php echo base_url(); ?>homecontents">  <i class="fa fa-commenting-o" aria-hidden="true"></i>
        <span>Home page dynamic contents</span>
      </a>
    </li>
  </ul>-->
 <!-- <ul class="sidebar-menu">
    <li class="treeview">
      <a href="<?php echo base_url(); ?>schedule">  <i class="fa fa-commenting-o" aria-hidden="true"></i>
        <span>Schedule Management</span>
      </a>
    </li>
  </ul> -->
  </section>
</aside>

