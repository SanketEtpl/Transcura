<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Profile</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">
                  
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listProfile" role="grid" aria-describedby="example1_info">                   
                      <tbody>                        
                      </tbody>
                    </table>
                  </div>
                </div> 
                <div class="row">
                  <div class="col-md-12">            
                    <div class="row">
                     <div class="col-sm-5">
                     <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite" class="valida"></div>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="content-header">
      <h1>
Change password
      </h1>
      
    </section>
 <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body" id="listProfile">
            <form method="post" action="<?php echo base_url(); ?>admin/Profile/ChangePassword" id="userForm1" name="userForm" class="user_validation" enctype="multipart/form-data"  valida>
             
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">           
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="old password">Old Password</label>
                  <input type="text" placeholder="Old Password" id="oldp" filter="password" required="true" name="oldpassword" class="form-control" autocomplete="off">
                </div>
              </div>

            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label" for="New password">New Password</label>
                <input type="text" placeholder="New Password" id="newp" filter="password" required="true" name="newpassword" class="form-control" autocomplete="off">
              </div>
            </div>

            <div class="col-md-6">
            <div class="form-group">
              <label class="control-label" for="confirm password">Confirm Password</label>
              <input type="text" placeholder="Confirm Password" id="confp" filter="password" required="true" name="confirmpassword" class="form-control" autocomplete="off">
            </div>
            </div>
               <div class="modal-footer">
          
            <button class="btn btn-info tiny" type="submit">Save</button>
          </div>
          </div>
        </div>
        </form>
      </div>
    </section>
  </div>
  <div id="userModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>admin/Profile/saveProfile" id="userForm" name="userForm" class="user_validation" enctype="multipart/form-data"  valida>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Manage Profile
            </h4>
          </div>
          <div class="modal-body">
            <div class="row">
             
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="address">First name</label>
                    <input type="hidden" id="user_key" name="user_key">
                  <textarea placeholder="first Name" id="first_name" required="true" name="first_name" class="form-control" autocomplete="off"></textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="address">Last name</label>
                  
                   <input type="text" placeholder="last_name" id="last_name" filter="last_name" required="true" name="last_name" class="form-control" autocomplete="off">
                </div>
              </div>  
               <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="address">Username</label>                   
                 
                   <input type="text" placeholder="username" id="username" filter="username" required="true" name="username" class="form-control" autocomplete="off">
                </div>
              </div>  

                <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="email_address">Email-Id</label>
                  <input type="text" placeholder="Email-Id" id="email" filter="email" required="true" name="email" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="contact_number">Phone</label>
                  <input type="text" placeholder="phone"  required="true" id="phone" name="phone" filter="phone" class="form-control" min="10" max="13">
                </div>
              </div>        
              
               
              <!--  <div class="col-md-6">
                <div class="form-group" id="imgChange">
                  <label class="control-label" for="subTitle"><span>Change Profile picture</span></label>  
                  <input type="hidden" name="oldimg" value="oldimg" id="oldimg">                
                  <input type="file" placeholder="Profile image" id="pimage" name="pimage"  size="20" class="form-control" autocomplete="off" onchange="document.getElementById('pimage').src = window.URL.createObjectURL(this.files[0])"> 
                
                </div>
                 <div class="col-md-6" id="theDiv">
                   <img src="images/default.png">       
                </div>       
              </div> -->
            </div>
           
          </div>

          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/profile.js"></script>
<?php $this->load->view('footer'); ?>
