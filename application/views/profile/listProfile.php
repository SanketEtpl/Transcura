<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
    General profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">General profile</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">                   
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listProfile" role="grid" aria-describedby="example1_info">                  
                      <tbody>                        
                      </tbody>
                    </table>
                  </div>
                </div> 
                <div class="row">
                  <div class="col-md-12">            
					<div class="row">
					  <div class="col-sm-5">
						<div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite" class="valida"></div>
					  </div>                  
					</div>
					</div>
				</div>
			</div>
        </div>
      </div>
    </section>
	
<section class="content-header">
  <h1>Change Password</h1>      
</section>
 <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body" id="listProfile">
            <form method="post" action="<?php echo base_url(); ?>admin/Profile/ChangePassword" id="userForm1" name="userForm1" class="user_validation" enctype="multipart/form-data"  valida>
             
            <div id="example1_wrapper" class="pwd_sec dataTables_wrapper form-inline dt-bootstrap">           
				  <div class="col-md-4">
					<div class="form-group">
					  <label class="control-label" for="old password">Old Password</label>
					  <input type="password" placeholder="Old password" id="oldp"  required="true" name="oldpassword" class="form-control password" autocomplete="off">
					</div>
				  </div>
				<div class="col-md-4">
				  <div class="form-group">
					<label class="control-label" for="New password">New Password</label>
					<input type="password" placeholder="New password" id="newp" required="true" name="newpassword" class="form-control password"  filter="password" autocomplete="off">
				  </div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					  <label class="control-label" for="confirm password">Confirm Password</label>
					  <input type="password" placeholder="Confirm Password" id="confp"  required="true" name="confirmpassword" class="form-control password" filter="password" autocomplete="off">
					</div>
				</div>
				<div class="sav_pwd col-md-12">          
					<button class="btn btn-info tiny" type="submit">Save</button>
				</div>
          </div>
        </div>
        </form>
      </div>
    </section>
  </div>
  <div id="userModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>admin/Profile/saveProfile" id="userForm" name="userForm" class="user_validation" enctype="multipart/form-data"  valida>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Manage profile
            </h4>
          </div>
          <div class="modal-body">
            <div class="row">             
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="address">First name</label>
                    <input type="hidden" id="user_key" name="user_key">                 
                    <input type="text" placeholder="First name" id="first_name" filter="name" required="true" name="first_name" class="form-control fullname" maxlength="20" autocomplete="off">
                </div>
              </div>
             <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="address">Last name</label>
                  
                   <input type="text" placeholder="Last name" id="last_name" filter="name" required="true" name="last_name" class="form-control fullname" maxlength="20" autocomplete="off">
                </div>
              </div> 
               <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="address">Username</label>                   
                 
                   <input type="text" placeholder="Username" id="username" filter="username" required="true" name="username" class="form-control" maxlength="50" autocomplete="off">
                </div>
              </div>
             <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="email_address">Email address</label>
                  <input type="text" placeholder="Email address" id="email" filter="email" required="true" name="email" class="form-control" autocomplete="off" maxlength="50">
                </div>
              </div>
			  <div class="col-md-6">
  				<div class="form-group">
  				  <label class="control-label" for="contact_number">Phone</label>
  				  <input type="text" placeholder="Phone"  required="true" id="phone" name="phone" filter="phone" class="form-control" min="3" maxlength="20">
  				</div>
			  </div> 
			  <div class="ch_pro_pho">	
				  <div class="col-md-6">
					<div class="form-group" id="imgChange">
					  <label class="control-label" for="subTitle"><span>Change picture</span></label>  
					  <input type="hidden" name="oldimg" value="oldimg" id="oldimg">                
					  <input type="file" placeholder="Profile image" id="pimage" name="pimage"  size="20" class="form-control" autocomplete="off" onchange="document.getElementById('pimage').src = window.URL.createObjectURL(this.files[0])">
					</div>
				  </div>
				  <div class="col-md-6" id="theDiv">
				   <img src="images/default.jpg">       
				 </div>    
			  </div>	
             </div>
            </div>
          <div class="modal-footer">
          <button data-dismiss="modal" ata-dismiss="modal" data-toggle="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/profile.js"></script>
<?php $this->load->view('footer'); ?>
