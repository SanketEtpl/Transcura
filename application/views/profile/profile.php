<?php $this->load->view('header'); ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
		Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url(); ?>admindashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">General Setting</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="dataTables_length" id="example1_length">
                    </div>
                  </div>
                <div class="col-sm-6">
                  <div id="example1_filter" class="dataTables_filter">                   
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12">
                  <table class="table table-bordered table-striped dataTable" id="listSetting" role="grid" aria-describedby="example1_info">
                      <tbody>                        
                      </tbody>
                    </table>
                  </div>
                </div> 
                <div class="row">
                  <div class="col-md-12">
            
                <div class="row">
                  <div class="col-sm-5">
                    <div class="dataTables_info" id="paginate_entries" role="status" aria-live="polite" class="valida"></div>
                  </div>                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>
  
  <div id="userModal" class="modal fade" style="display: none;" aria-hidden="false">
    <form method="post" action="<?php echo base_url(); ?>admin/setting/saveSetting" id="userForm" name="userForm" class="user_validation" enctype="multipart/form-data"  valida>
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
            <h4 class="modal-title">Manage Setting
            </h4>
          </div>
          <div class="modal-body">
            <div class="row">             
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="address">Address</label>
                    <input type="hidden" id="user_key" name="user_key">
                  <textarea placeholder="Address" id="company_address" required="true" name="company_address" class="form-control" autocomplete="off"></textarea>
                </div>
              </div>
            
                <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="email_address">Email-Id</label>
                  <input type="text" placeholder="Email-Id" id="office_email" filter="email" required="true" name="office_email" class="form-control" autocomplete="off">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="contact_number">Mobile</label>
                  <input type="text" placeholder="mobile"  required="true" id="mobile" name="mobile" filter="mobile" class="form-control" min="10" max="13">
                </div>
              </div>           
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="contact_number">Office phone</label>
                  <input type="text" placeholder="mobile"  required="true" id="company_phone" name="company_phone" class="form-control" min="10" max="13">
                </div>
              </div>    
      
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="address">Facebook</label>
                  <input type="text" placeholder="Company Address" id="facebook" name="facebook"  filter="url" class="form-control" autocomplete="off">
                </div>
              </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="address">Twitter</label>
                  <input type="text" placeholder="Twitter" id="twitter" name="twitter" filter="url"  class="form-control" autocomplete="off">
                </div>
              </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="address">google</label>
                  <input type="text" placeholder="google" id="google" name="google"  filter="url" class="form-control" autocomplete="off">
                </div>
              </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label" for="address">Pintrest</label>
                  <input type="text" placeholder="Pintrest" id="pintrest" name="pintrest"  filter="url" class="form-control" autocomplete="off">
                </div>
              </div>

           
               <div class="col-md-6">
                <div class="form-group" id="imgChange">
                  <label class="control-label" for="subTitle"><span>Change logo</span></label>  
                  <input type="hidden" name="oldimg" value="oldimg" id="oldimg">                
                  <input type="file" placeholder="Profile image" id="pimage" name="pimage"  size="20" class="form-control" autocomplete="off" onchange="document.getElementById('pimage').src = window.URL.createObjectURL(this.files[0])"> 
                
                </div>
                 <div class="col-md-6" id="theDiv">
                   <img src="images/imgpsh_fullsize1.png">       
                </div>       
              </div>
            </div>
           
          </div>

          <div class="modal-footer">
            <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
            <button class="btn btn-info tiny" type="submit">Save</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url(); ?>js/admin/setting.js"></script>
<?php $this->load->view('footer'); ?>
