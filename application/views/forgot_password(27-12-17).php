<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>App-Tech | Forgot password</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>images/res-icon.png">  
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/AdminLTE.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/plugin/square/blue.css">
    <link  rel="stylesheet" href="<?php echo base_url(); ?>css/admin_media.css">
  </head>
  <body class="hold-transition login-page">
    <div class="bg_wrapp">
      <div class="login-box">
        <div class="login-logo">
          <a href="<?php echo base_url(); ?>"> <img src="<?php echo base_url(); ?>images/imgpsh_fullsize.png" width="75%"></a>
        </div>
        <div id="login" class="login-box-body">
          <p class="login-box-msg">Forgot password</p>
	<?php 
            $success = $this->session->flashdata('success');
            $fail = $this->session->flashdata('fail');
            
            if($success){
                ?>
                <div class="alert alert-success">
                  <?php echo $success; ?>
                </div>
              <?php
              }

               if($fail){
                ?>
                <div class="alert alert-danger">
                  <?php echo $fail; ?>
                </div>
              <?php
              }

          ?>
          <div id="message"></div>
          <form action="<?php echo base_url(); ?>admin/login/forgetPassword" method="post" id="forgotPwdForm" name="forgotPwdForm" >
            <div class="form-group has-feedback">
              <input type="text" name="username" id="username" class="form-control" placeholder="Email Id">
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
		<?php echo form_error('username', '<div class="error">', '</div>'); ?>
            </div>
            <!-- <div class="form-group has-feedback">
              <input type="password" name="password" id="password" class="form-control" placeholder="Password">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div> -->
            <div class="row">
              <div class="col-md-12 text-center">
<!--    <button type="submit" class="tiny btn btn-primary">Submit</button>-->
            <input type="submit" class="tiny btn btn-primary" name="forgot_pwd" value="submit">

              </div>
              <div class="col-xs-8">
                <a href="<?php echo base_url();?>admin">Sign in</a>               
              </div>
            </div>
          </form>
        </div>   
      </div>
    </div>

  <script src="<?php echo base_url(); ?>js/jquery-2.2.3.min.js"></script>
  <script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
  <script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
  </body>
</html>
