<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = "Home";
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['logout'] = "login/logout";
$route['admin'] = 'admin/login';
$route['users'] = 'admin/users';
$route['admindashboard'] = 'admin/dashboard';
$route['login'] = 'admin/login';
$route['Service_providers'] = 'admin/Service_providers';
$route['Service_providers_category'] = 'admin/Service_providers_cat';
$route['Setting'] = 'admin/Setting';
$route['faq'] = 'admin/FAQ';
$route['testimonial']='admin/Testimonial';
$route['subscription'] = 'admin/plans';
$route['contents'] = 'admin/pages';
$route['properties'] = 'admin/properties';
$route['cities'] = 'admin/cities';
$route['agency'] = 'home/agency';
$route['member'] = 'admin/member';
$route["first_section"] = 'admin/First_section';
$route["second_section"] = 'admin/Second_section';
$route["save_first_content"] ="admin/First_section/save_first_section";
$route["save_second_content"] ="admin/Second_section/save_second_section";


/*$route['staff'] = 'home/staff';
$route['staff_profile'] = 'home/staff_profile';
*/
/*  frontend user  */
$route['forgot-password']='home/forgot_password';
$route['reset-password'] = 'home/reset_password{+1}';
$route['signup'] = 'home/signup';
$route['dashboard'] = 'home/dashboard';
$route['accountverify/(:any)'] = 'home/accountverify/$1';
$route['termsandconditions'] = 'home/termsandconditions';
$route['privacypolicy'] = 'home/privacypolicy';
$route['properties_details'] = 'home/properties_details';

$route['doctors_category'] = 'admin/doctors_cat';
$route['doctors'] = 'admin/Doctors';
$route['patients'] = 'admin/patients';
$route['service_types'] = 'admin/service_types';
$route['drivers'] = 'admin/drivers';
$route['schedule'] = "admin/schedule";
//$route['forgot-admin-password'] = "admin/Login/admin_reset_password";
$route['admin-forgot-pwd']='home/admin_forgot_password';
$route['admin-forgot-password']='home/admin_reset_password';

//$route['home_page_contents'] ="admin/Homepagecontents";

/* ------------------ New theme integration ---------------------------*/
$route['user'] = "frontend/Homecontroller";

$route['schedule-ride'] = "frontend/Homecontroller/schedule_ride";
$route['reset_password'] = "frontend/Homecontroller/reset_password";
$route['reset_username'] = "frontend/Homecontroller/reset_username";

$route['registration-next-step'] = "frontend/Homecontroller/registration_next_step";
$route['user-login'] = "frontend/Homecontroller/user_login";
$route['registration'] = "frontend/Homecontroller/registration";
$route['about-us'] = "frontend/Homecontroller/about";
$route['services'] = "frontend/Homecontroller/services";
$route['contact-us'] = "frontend/Homecontroller/contact";
$route['news'] = "frontend/Homecontroller/news";
$route['blogs'] = "frontend/Homecontroller/blogs";
$route['faqs'] = "frontend/Homecontroller/faqs";
$route['terms-and-conditions'] = "frontend/Homecontroller/terms_and_conditions";
$route['privacy-policy'] = "frontend/Homecontroller/privacy_policy";
$route['set-reset-pwd'] = "frontend/Homecontroller/set_reset_password";
$route['set-reset-username'] = "frontend/Homecontroller/set_reset_username";
$route['set-registration'] = "frontend/Homecontroller/form_registration";
$route['frontend-login'] = "frontend/Homecontroller/login";
$route['registration'] = "frontend/Homecontroller/registration";
$route['submit-contact-us'] = "frontend/Homecontroller/save_contact_us";
$route['reset-pwd/(:num)/(:any)'] = "frontend/Homecontroller/resetPassword/$1/$2";
$route['change-password'] = "frontend/Homecontroller/change_password";
$route['user-change-pwd']="frontend/Homecontroller/changePassword";
$route['checkemailAvailability']="frontend/Homecontroller/check_email_availability";
$route['reset-user-password/(:any)/(:any)']="frontend/Homecontroller/resetPassword/$1/$2";
$route['reset-user-username/(:any)/(:any)']="frontend/Homecontroller/resetUsername/$1/$2";

$route['home-page-section']="frontend/Homecontroller/homepage_section";
$route['my-dashboard']="frontend/DriverController";
$route['my-profile']="frontend/DriverController/driver_profile";
$route['view-map']="frontend/DriverController/view_map";
$route['edit-profile']="frontend/DriverController/driver_edit_profile";
$route['notification']="frontend/DriverController/driver_notification";
$route['driver-my-schedule-rides']="frontend/DriverController/driver_my_schedule_rides";
$route['schedule-a-ride'] = "frontend/DriverController/driver_schedule_an_ride";
$route['return-schedule-a-ride'] = "frontend/DriverController/driver_return_schedule_an_ride";
$route['schedule-an-trip-details']="frontend/DriverController/schedule_an_trip_details";
$route['my-rides/(:any)']="frontend/DriverController/driver_my_ride/$1";
$route['my-cancelled-rides']="frontend/DriverController/my_cancelled_rides";
#$route['my-rides/(:any)/(:any)']="frontend/DriverController/driver_my_ride/$1/$2";
$route['driver_my_ride/(:any)']="frontend/DriverController/driver_my_ride/$1/$2";
$route['user-logout']='frontend/Homecontroller/user_logout';
$route['cumstomer-complaint']='frontend/DriverController/customer_complaints';
$route['ride-trip-details-view'] ="frontend/DriverController/ride_trip_details_view";
$route['driver-my-dashboard']="frontend/DriverUserTypeController";
$route['driver-my-profile']="frontend/DriverUserTypeController/driver_my_profile";
$route['driver-my-trip-assignments']="frontend/DriverUserTypeController/driver_my_trip_assignment";
$route['driver-my-trip-requests']="frontend/DriverUserTypeController/driver_my_trip_request";
$route['driver-my-bills']="frontend/DriverUserTypeController/driver_my_bills";
$route['driver-notification']="frontend/DriverUserTypeController/driver_notifications";
$route['pending-ride/(:any)']="frontend/DriverController/show_schedule_ride_details/$1";
$route['pending-ride']="frontend/DriverController/show_schedule_ride_details";

$route['nemt-setting']="frontend/DriverController/nemt_reset_pwd";

// Memeber
$route['member-dashboard']="frontend/MemberController";
$route['restaurants']="frontend/MemberController/restaurants";
$route['restaurant_details']="frontend/MemberController/restaurants";
$route['restaurant_details/(:any)']="frontend/MemberController/getRestaurant/$1";
$route['member_ride_details']="frontend/MemberController/member_ride_details";
$route['show-ride-address'] ="frontend/DriverController/show_ride_address";
$route['schedule-ride-complete-details'] = "frontend/DriverController/display_schedule_ride_details";

$route['member-my-ride']="frontend/MemberController/member_my_ride";
$route['process_topayment']="frontend/MemberController/process_topayment";
$route['cancel_url']="frontend/MemberController/cancel_url";

$route['my-rides/(:any)']="frontend/DriverController/driver_my_ride";
$route['update-driver-edit-profile'] ="frontend/DriverController/update_driver_edit_profile";
$route['driver-setting']="frontend/DriverUserTypeController/driver_reset_pwd";