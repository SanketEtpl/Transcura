<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


define("PER_PAGE",5);
define("EMAIL_FROM","tanuja.b@exceptionaire.co");
define("TB_ADMIN","tbl_admin");
define("TB_USERS","tbl_users");
define("TB_PAGES","tbl_pages");
define("TB_DOCTORS_CAT","tbl_doctors_cat");
define("TB_DOCTORS","tbl_doctors");
define("TB_PATIENTS","tbl_patients");
define("TB_DRIVERS","tbl_drivers");
define("TB_SERVICE_TYPE","tbl_service_type");
define("TB_INSURANCELIST","tbl_insurancelist");
define("TB_PATIENT_COMPLAINTS","tbl_patient_complaints");
define("TB_DRIVER_TRANSPORT_COMP","tbl_driver_transport_comp");
define("TB_ROLE","tbl_role");
define("TB_APPOINTMENT_SCHEDULE","tbl_appointment_schedule");
define("TB_SETTINGS","tbl_settings");

define("TB_STATE","tbl_states");
define("TB_HEALTHCARE_SERVICES","tbl_healthcare_services");
define("TB_DESIRE_TRANSPORTATION","tbl_desire_transportation");
define("TB_SPECIAL_REQUEST","tbl_special_request");
define("TB_SCHEDULE_RIDE","tbl_schedule_ride");
define("TB_USER_TYPES","tbl_user_types");
define("TB_COUNTRY","tbl_country");
define("TB_DESTINATION","tbl_destination");
define("TB_CITY","tbl_city");
define("TB_CONTACT_US","tbl_contact_us");
define("TB_USERS_NEW","tbl_users_new");
define("TB_FAQ","tbl_faq");
define("TB_TESTIMONIAL","tbl_testimonial");
define("TB_NOTIFICATION","tbl_notifications");
define("TB_COMPLAINTS_TYPE","tbl_complaints_type");
define("TB_SCHEDULE_RIDE_COMPLAINTS","tbl_schedule_ride_complaints");
define("TB_SECOND_SECTION","tbl_second_section");
define("TB_FIRST_SECTION","tbl_first_section");
define("TB_DRIVER_LOCATION","tbl_driver_location");
define("TB_DRIVER_REQUEST","tbl_driver_request");
define("TB_BOOK_RIDE","tbl_book_ride");
define("TB_CAR_CATEGORY","tbl_car_category");
define("TB_MEMBER_SERVICE","tbl_member_service_type");
define("TB_DISTANCE_CALCULATE","tbl_distance_calculate");
define("TB_DRIVER_UNAVAILABLE","tbl_driver_unavailable");
define("TB_DOCTOR_SEARCH_BY_CAT_LOC","tbl_doctor_search_by_cat_loc");
define("TB_MEMBER_PAYMENT","tbl_member_payment");
define("TB_EQUIPMENT","tbl_equipment");
define("TB_SUB_RIDE","tbl_sub_ride");

define("Zomato_key","3b047bd40322469b8a72b79a933e3953");
 define("GOOGLE_API_KEY","AIzaSyB9eN_K4B39MgAjuZOs0HsEW_Yc4un1KTw");
define("STRIPE_SECRET_KEY","sk_test_nFub9ubeXthEjL82rAbIhnLd");
define("STRIPE_PUBLISHABLE_KEY","pk_test_cO7hV0gqLX6nUfF42QqA0ISB");
//define("GOOGLE_API_KEY","AIzaSyDqzRPdIfxpZWTDmM3VBaQ4_An7-e6Xg64");
// define("GOOGLE_API_KEY","AIzaSyBD5NeyT_tKxswCM6YHXRXZCDbb7NDIuko");

// ----- Paypal --------
define("PayPalMode", "sandbox");
define("PayPalLocal", "Yes");
define("PayPalCurrencyCode", "USD");

define("PayPalTestApiEmail", "ram.k-facilitator@exceptionaire.co");
define("PayPalTestApiUsername", "ram.k-facilitator_api1.exceptionaire.co");
define("PayPalTestApiPassword", "R4EE2GVYFYLLGLNT");
define("PayPalTestApiSignature", "AFcWxV21C7fd0v3bYYYRCpSSRl31Afk99fSS7w9nBsyaQzw3iU9.lluq");

define("PayPalTestReturnURLEmployer", "http://".$_SERVER['HTTP_HOST']."/Apptech_New/process_topayment");
define("PayPalTestCancelURLEmployer", "http://".$_SERVER['HTTP_HOST']."/Apptech_New/cancel_url");
define("onesignalKey","ba861a3d-f5d6-47fe-ae93-ef1fe7e64df6");

define ("RIDE_TYPE", serialize (array ("1"=>"One Way","2" => "One Way with pharmacy stop", "3"=>"Round Trip", "4"=>"Round trip with pharmacy stop","5"=>"Multiple appointments","6"=>"Multiple appointments with pharmacy stop")));