<?php defined('BASEPATH') OR exit('No direct script access allowed');
//require_once("facebook/Facebook.php");
class User_AuthSocial
{
    function __construct() { 
    	$this->ci =& get_instance();
		$this->ci->load->model('Login_model');
		$this->ci->load->database();
		$this->ci->load->library('session');
		
    }
    function gl_redirect_url()
    {
        include_once "google-api-php-client/Google_Client.php";
		include_once "google-api-php-client/contrib/Google_Oauth2Service.php";
		
		// Google Project API Credentials
		$clientId = GL_APPS_KEY;
        $clientSecret = GL_APPS_SECRET;
        $redirectUrl = base_url() . 'home/google_login/';
		
		// Google Client Configuration
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to MMHO');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $google_oauthV2 = new Google_Oauth2Service($gClient);

        if (isset($_REQUEST['code'])) {
            $gClient->authenticate();
            $this->ci->session->set_userdata('token', $gClient->getAccessToken());
            redirect($redirectUrl);
        }
        $token = $this->ci->session->userdata('token');
       
    	    $authUrl = $gClient->createAuthUrl();
    		return $authUrl;
    	
    }
    function google(){
		// Include the google api php libraries
		include_once "google-api-php-client/Google_Client.php";
		include_once "google-api-php-client/contrib/Google_Oauth2Service.php";
		
		// Google Project API Credentials
		$clientId = GL_APPS_KEY;
        $clientSecret = GL_APPS_SECRET;
        $redirectUrl = base_url() . 'home/google_login/';
		
		// Google Client Configuration
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login to MMHO');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectUrl);
        $google_oauthV2 = new Google_Oauth2Service($gClient);

        if (isset($_REQUEST['code'])) {
            $gClient->authenticate();
            $this->ci->session->set_userdata('token', $gClient->getAccessToken());
            redirect($redirectUrl);
        }

        $token = $this->ci->session->userdata('token');
        if (!empty($token)) {
            $gClient->setAccessToken($token);
        }
    
        if ($gClient->getAccessToken()) 
        {
            $userProfile = $google_oauthV2->userinfo->get();
          
          
            // Preparing data for database insertion
			// $userData['oauth_provider'] = 'google';
			// $userData['oauth_uid'] = $userProfile['id'];
   //          $userData['first_name'] = $userProfile['given_name'];
   //          $userData['last_name'] = $userProfile['family_name'];
   //          $userData['email'] = $userProfile['email'];
			// $userData['gender'] = $userProfile['gender'];
			// $userData['locale'] = $userProfile['locale'];
   //          $userData['profile_url'] = $userProfile['link'];
   //          $userData['picture_url'] = $userProfile['picture'];
			// Insert or update user data
			//print_r($userData);

            $user_token = substr( md5(rand()), 0, 20);
            $userData['first_name'] = $userProfile['given_name'];
            $userData['status'] = "active";
            $userData['user_token'] = $user_token;
            $userData['last_name'] = $userProfile['family_name'];
            $userData['email'] = $userProfile['email'];
            $userData['gender'] = $userProfile['gender'];
            $userData['address'] = $userProfile['locale'];
           $userData['picture'] = $userProfile['picture'];
          //  print_r($userProfile);die;  
            //print_r($userData);die;  
            $data['userData'] = $userData;
            // Insert or update user data
            $cond = array("email" => $userProfile['email']);
            $user_exist = $this->ci->Login_model->isEmailExist($cond);
			
            // echo $userID; die();
            if(empty($user_exist)){
                //$data['userData'] = $userData;
               //echo "fgjhfgj" ;die;
                
                $user = $this->ci->common->insert(TB_USERS,$userData);
              // echo  $user;die;

               
               
            } else{ //Update
               $data['userData'] = array();
               return $userData;
            }
        }else{
        	return false;
        }
       
    }
    function fb_redirect_url()
     {
     include_once "facebook/Facebook.php";
     $redirectUrl = base_url() . 'home/facebook_login/';
     $fbPermissions = 'email';
      $facebook = new Facebook(array(
				'appId'  => FB_APPS_KEY,
				'secret' => FB_APPS_SECRET
		
		));
		//var_dump($facebook);die; 
		$fbuser = $facebook->getUser();
		if($fuser)
		{
			$authUrl = $facebook->getLoginUrl(array('redirect_uri'=>$redirectUrl,'scope'=>$fbPermissions)); 
			return $authUrl;
		}else 
		{
			$fbuser = '';
			$authUrl = $facebook->getLoginUrl(array('redirect_uri'=>$redirectUrl,'scope'=>$fbPermissions)); 
			return $authUrl;
		}
     	return false;
     }
	 function facebook()
	 {
		// Include the facebook api php libraries
		include_once "facebook/Facebook.php";
		
		// Facebook API Configuration
		$appId = FB_APPS_KEY;
		$appSecret = FB_APPS_SECRET;
		//$redirectUrl = base_url() . 'User_authentication/facebook_login/';
		//$fbPermissions = 'email';
		
		//Call Facebook API
		
		
		$facebook = new Facebook(array(
		  'appId'  => $appId,
		  'secret' => $appSecret
		
		));
		$fbuser = $facebook->getUser();
		//var_dump($fbuser);die();
        if ($fbuser)
        {
			$userProfile = $facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');
			//print_r($userProfile);
            // Preparing data for database insertion\
           // print_r($userProfile);die;
			//$userData['oauth_provider'] = 'facebook';
			//$userData['oauth_uid'] = $userProfile['id'];
			 $user_token = substr( md5(rand("alnum")), 0, 20);
            $userData['first_name'] = $userProfile['first_name'];
            $userData['status'] = "active";
            $userData['user_token'] = $user_token;
            $userData['last_name'] = $userProfile['last_name'];
            $userData['email'] = $userProfile['email'];
			$userData['gender'] = $userProfile['gender'];
			$userData['address'] = $userProfile['locale'];
          //  $userData['profile_url'] = 'https://www.facebook.com/'.$userProfile['id'];
            $userData['picture'] = $userProfile['picture']['data']['url'];
            $data['userData'] = $userData;
			// Insert or update user data
			$cond = array("email" => $userProfile['email']);
            $user_exist = $this->ci->Login_model->isEmailExist($cond);
       
            if(empty($user_exist))
            {
                $data['userData'] = $userData; 
                $data['id'] = $userData; 
                            
                $user = $this->ci->common->insert(TB_USERS,$userData);
       
            //  $userData['id'] = $user ;
            }else 
            {
               $data['userData'] = array();
              // var_dump($user);die;
               return $userData;
            }
          
        }else 
        {
        	return false;
        }
        
    }
    
     
	public function logout() {
		$this->session->unset_userdata('token');
		$this->session->unset_userdata('userData');
        $this->session->sess_destroy();
		redirect('/user_authentication');
    }
}
