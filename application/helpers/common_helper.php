<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------ get driver availabality ----------
if(!function_exists('getDriverAvailability'))
{
	function getDriverAvailability($driver_id)
	{
		$ci = & get_instance();
		$ci->load->model('Common_model');
		$checkavq = $ci->Common_model->select("*",TB_DRIVER_UNAVAILABLE,array("driver_id"=>$driver_id,"status"=>"0"));
		$status = (count($checkavq) > 0) ? "unavailabale" : "availabale";
		return $status;
	}
}

//------ change driver availabality --------
if(!function_exists('changeDriverAvail'))
{
	function changeDriverAvail($status,$driver_id,$user_type)
	{
		$ci = & get_instance();
		$ci->load->model('Common_model');
		if($status == "1")
		{
			//---- make availabale ------
			if($user_type == "5") // For driver
			{
				$getownerq = $ci->Common_model->select("*",TB_DRIVER_UNAVAILABLE,array('driver_id'=>$driver_id));
				$changeby  = $getownerq[0]['changed_by'];
				if($changeby == $ci->session->userdata('user_id'))
				{
					// ------ if TP has changed status conatct him to make active ---------
					$changeq =  $ci->Common_model->delete(TB_DRIVER_UNAVAILABLE,array('driver_id'=>$driver_id));
				}else
				{
					//----------- make direcly active --------
					return json_encode(array('status' => 'warning','message'=>'Transportation Provider has changed your availabality. Please contact your transportation provider.'));
				}
			}else if($user_type == "4") // For Transportation provider
			{
				$changeq =  $ci->Common_model->delete(TB_DRIVER_UNAVAILABLE,array('driver_id'=>$driver_id));
			}			
		}else if($status == "0")
		{
			//------ make unavailabale -------
			$insertArr = array(
				'driver_id'=>$driver_id,
				'status'=>'0',
				'changed_by'=>$ci->session->userdata('user_id'));
			$changeq = $ci->Common_model->insert(TB_DRIVER_UNAVAILABLE,$insertArr);
		}
		if($changeq)
		{
			return json_encode(array('status' => 'success','message'=>'Availability changed successfully.'));
		}else
		{
			return json_encode(array('status' => 'error','message'=>'Oops!!! Something went wrong.'));
		}
	}
}

// ----- get driver parent => transport provider----
if(!function_exists('getDriverParent'))
{
	function getDriverParent($trans_id)
	{
		$ci = & get_instance();
		$ci->load->model('Common_model');
		$result = $ci->Common_model->select("full_name",TB_USERS,array('id' =>$trans_id));
		return $result[0]['full_name'];
	}
}

//---------- get driver documents ------
if(!function_exists('getDriverDochtml'))
{
	function getDriverDochtml($driver_id)
	{
		$ci = & get_instance();
		$ci->load->model('Common_model');
		$userData = $ci->Common_model->select("*",TB_USERS,array('id'=>$driver_id));
		
		$html.='
		

        <div class="col-md-6">
                <label class="control-label">Social security card </label> <br />';
                if($userData[0]['social_security_card_doc'] != "")
                {
                    $html.='<a href="'.base_url().$userData[0]['social_security_card_doc'].'" target="_blank">
                    <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                }else
                {
                    $html.='N/A';
                }
                $html.='</div>

            <div class="col-md-6">
                    <label class="control-label">Driver Licence</label><br />';
                    if($userData[0]['driver_license_doc'] != "")
                    {
                        $html.='<a href="'.base_url().$userData[0]['driver_license_doc'].'" target="_blank">
                        <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                    }else
                    {
                        $html.='N/A';
                    }
                    $html.='</div>

                <div class="col-md-6">
                        <label class="control-label">Drug test result</label><br />';
                        if($userData[0]['drug_test_results_doc'] != "")
                        {
                            $html.='<a href="'.base_url().$userData[0]['drug_test_results_doc'].'" target="_blank">
                            <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                        }else
                        {
                            $html.='N/A';
                        }
                        $html.='</div>

                    <div class="col-md-6">
                            <label class="control-label">Criminal background report</label><br />';
                            if($userData[0]['criminal_back_ground_doc'] != "")
                            {
                                $html.='<a href="'.base_url().$userData[0]['criminal_back_ground_doc'].'" target="_blank">
                                <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                            }else
                            {
                                $html.='N/A';
                            }
                            $html.='</div>

                        <div class="col-md-6">
                                <label class="control-label">Motor vehicle record</label><br />';
                                if($userData[0]['motor_vehicle_record_doc'] != "")
                                {
                                    $html.='<a href="'.base_url().$userData[0]['motor_vehicle_record_doc'].'" target="_blank">
                                    <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                                }else
                                {
                                    $html.='N/A';
                                }
                                $html.='</div>

                            <div class="col-md-6">
                                    <label class="control-label">Act 33-34 clearance report</label><br />';
                                    if($userData[0]['act_33_and_34_clearance_doc'] != "")
                                    {
                                        $html.='<a href="'.base_url().$userData[0]['act_33_and_34_clearance_doc'].'" target="_blank">
                                        <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                                    }else
                                    {
                                        $html.='N/A';
                                    }
                                    $html.='</div>

                                <div class="col-md-6">
                                        <label class="control-label">Vehicle inspection report</label><br />';
                                        if($userData[0]['vehicle_inspections_doc'] != "")
                                        {
                                            $html.='<a href="'.base_url().$userData[0]['vehicle_inspections_doc'].'" target="_blank">
                                            <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                                        }else
                                        {
                                            $html.='N/A';
                                        }
                                        $html.='</div>

                                    <div class="col-md-6">
                                            <label class="control-label">Vehicle maintenance report</label><br />';
                                            if($userData[0]['vehicle_maintnc_record_doc'] != "")
                                            {
                                                $html.='<a href="'.base_url().$userData[0]['vehicle_maintnc_record_doc'].'" target="_blank">
                                                <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                                            }else
                                            {
                                                $html.='N/A';
                                            }
                                            $html.='</div>

                                        <div class="col-md-6">
                                                <label class="control-label">Vehicle insurance report</label><br />';
                                                if($userData[0]['vehicle_insurance_doc'] != "")
                                                {
                                                    $html.='<a href="'.base_url().$userData[0]['vehicle_insurance_doc'].'" target="_blank">
                                                    <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                                                }else
                                                {
                                                    $html.='N/A';
                                                }
                                                $html.='</div>

                                            <div class="col-md-6">
                                                    <label class="control-label">Vehicle registration report</label><br />';
                                                    if($userData[0]['vehicle_regi_doc'] != "")
                                                    {
                                                        $html.='<a href="'.base_url().$userData[0]['vehicle_regi_doc'].'" target="_blank">
                                                        <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                                                    }else
                                                    {
                                                        $html.='N/A';
                                                    }
                                                    $html.='</div>

                                                <div class="col-md-6">
                                                        <label class="control-label">Own Vehicle registration report</label><br />';
                                                        if($userData[0]['own_bus_vehicle_reg_doc'] != "")
                                                        {
                                                            $html.='<a href="'.base_url().$userData[0]['own_bus_vehicle_reg_doc'].'" target="_blank">
                                                            <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                                                        }else
                                                        {
                                                            $html.='N/A';
                                                        }
                                                        $html.='</div>

                                                    <div class="col-md-6">
                                                            <label class="control-label">Insurance policy</label><br />';
                                                            if($userData[0]['own_bus_insurance_pol_doc'] != "")
                                                            {
                                                                $html.='<a href="'.base_url().$userData[0]['own_bus_insurance_pol_doc'].'" target="_blank">
                                                                <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                                                            }else
                                                            {
                                                                $html.='N/A';
                                                            }
                                                            $html.='</div>

                                                        <div class="col-md-6">
                                                                <label class="control-label">Inspection</label><br />';
                                                                if($userData[0]['own_bus_inspection_doc'] != "")
                                                                {
                                                                    $html.='<a href="'.base_url().$userData[0]['own_bus_inspection_doc'].'" target="_blank">
                                                                    <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                                                                }else
                                                                {
                                                                    $html.='N/A';
                                                                }
                                                                $html.='</div>

                                                            <div class="col-md-6">
                                                                    <label class="control-label">Tax IS or EIN Number</label><br />';
                                                                    if($userData[0]['own_bus_taxid_einno_doc'] != "")
                                                                    {
                                                                        $html.='<a href="'.base_url().$userData[0]['own_bus_taxid_einno_doc'].'" target="_blank">
                                                                        <i class="fa fa-fw fa fa-file fa-2x"></i></a>';
                                                                    }else
                                                                    {
                                                                        $html.='N/A';
                                                                    }
                                                                    $html.='</div>';



		return $html;
	}
}

if(!function_exists('getUserType'))
{
	function getUserType($id)
	{
		$ci = & get_instance();
		$ci->load->model('Common_model');
		$userType = $ci->Common_model->select("user_type",TB_USERS,array("id"=>$id));
		return $userType[0]['user_type'];
	}
}

if(!function_exists('isAdmin'))
{
	function isAdmin($email)
	{
		//$email = "tdtdtd@gmail.com";
		$ci = & get_instance();
		$ci->load->model('Common_model');
		$isAdmin = $ci->Common_model->select("id",TB_ADMIN,array("email"=>$email));
		//return $isAdmin[0]['user_type'];

		if(!empty($isAdmin))
		{
			return true;
		}
		else
		{
			return false;	
		}

		//return $isAdmin;


	}
}

?>