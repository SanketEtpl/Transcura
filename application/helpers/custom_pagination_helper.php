<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	function get_pagination_config($start = 0, $total = 0, $column, $order, $clickEvent = "getList") {
		$config['click'] = $clickEvent;
		$config['total_rows'] = $total;
		$config['per_page'] = 1;
		$config['current_page'] = $start;
		$config['order_column'] = $column;
		$config['order'] = $order;
		$config['num_links'] = 2;
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = TRUE;
		$config['last_link'] = TRUE;
		$config['next_link'] = FALSE;
		$config['prev_link'] = FALSE;
    
		$config['first_link'] = 'First';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_link'] = 'Last';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		//$config['next_link'] = 'Next';
		//$config['next_tag_open'] = '<li>';
		//$config['next_tag_close'] = '</li>';
		//$config['prev_link'] = 'Previous';
		//$config['prev_tag_open'] = '<li>';
		//$config['prev_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="paginate_button active"><a>';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li CLASS="paginate_button">';
		$config['num_tag_close'] = '</li>';
		return $config;
    }    

    function pagination_data($arrayColumn=array(),$arrayStatus=array(),$postData=array(),$arrayColumnOrder=array(),$default_column='',$column_name='',$table_name='',$select='',$function_name='listRows',$where=array(), $join = array(),$default_order_column='')
	{
		$CI = get_instance();
		$CI->load->model('common');

	    $column = trim($postData["column"]);

	    if($default_order_column != ''){
	    	$column = $default_column;
	    	$columnOrderBy = $default_order_column;
	    }else if(array_key_exists($column,$arrayColumn)){
	      $column = $column;
	      $columnOrderBy = $arrayColumn[$column];
	    }else{
	      $column = $default_column;
	      $columnOrderBy = $arrayColumn[$default_column];
	    }
	    
	    
	    $columnOrder = trim($postData["order"]);
	    if(!in_array($columnOrder, $arrayColumnOrder)){
	      $columnOrder = "ASC";
	    }

	    $search = trim($postData["search"]);
	    if($search!=""){
	      foreach ($arrayColumn as $key => $value) {
	        if(array_key_exists($key,$arrayStatus)){
	          foreach ($arrayStatus[$key] as $status => $s) {
	            if(strtolower($search) == strtolower($status)){
	              $arrayLike[$value] = $s;
	              break;
	            }
	          }
	        }
	        else
	        {
	          $arrayLike[$value] = $search;
	        }
	      }
	    }
	    else
	    {
	    	$arrayLike = array();
	    }

	    $count = $CI->common->getCount("count(".$column_name.") AS cnt",$table_name,$where,$arrayLike, array(), $join);

	    $data['rows'] = $CI->common->getRowsPerPage($select,$table_name,$where,$arrayLike,array($columnOrderBy => $columnOrder),$postData["page"], $join);
	   

	    $config = get_pagination_config($postData["page"], ($count[0]["cnt"]/PER_PAGE), $column, $columnOrder, $function_name);
//echo $CI->db->last_query();exit;
	    //print_r($config);exit;

		$CI->ajaxpagination->initialize($config);
		$data["pagelinks"] = $CI->ajaxpagination->create_links();
		$from = (($postData["page"]*PER_PAGE)+1);
		$to = (($postData["page"]+1)*PER_PAGE);
		if($to > $count[0]["cnt"])
		{
			$to = $count[0]["cnt"];
		}
		if($to<$from){
			$from = $to;
		}

		$data["entries"] = 'Showing '.$from.' to '.$to.' of '.$count[0]["cnt"].' entries';
       // echo $data["entries"];exit;
		return $data;  
	}
	function pagination_data1($arrayColumn=array(),$arrayStatus=array(),$postData=array(),$arrayColumnOrder=array(),$default_column='',$column_name='',$table_name='',$select='',$function_name='listRows',$where=array(), $join = array(),$default_order_column='',$orderby=array())
	{
		$CI = get_instance();
		$CI->load->model('common');

	    $column = trim($postData["column"]);

	    if($default_order_column != ''){
	    	$column = $default_column;
	    	$columnOrderBy = $default_order_column;
	    }else if(array_key_exists($column,$arrayColumn)){
	      $column = $column;
	      $columnOrderBy = $arrayColumn[$column];
	    }else{
	      $column = $default_column;
	      $columnOrderBy = $arrayColumn[$default_column];
	    }
	    
	    
	    $columnOrder = trim($postData["order"]);
	    if(!in_array($columnOrder, $arrayColumnOrder)){
	      $columnOrder = "ASC";
	    }
	    if(!count($orderby))
	    {
	    	$columnOrderBy = "urjency_explainatin , id ";	
	    }
	    
	    $search = trim($postData["search"]);
	    if($search!=""){
	      foreach ($arrayColumn as $key => $value) {
	        if(array_key_exists($key,$arrayStatus)){
	          foreach ($arrayStatus[$key] as $status => $s) {
	            if(strtolower($search) == strtolower($status)){
	              $arrayLike[$value] = $s;
	              break;
	            }
	          }
	        }
	        else
	        {
	          $arrayLike[$value] = $search;
	        }
	      }
	    }
	    else
	    {
	    	$arrayLike = array();
	    }

	    $count = $CI->common->getCount("count(".$column_name.") AS cnt",$table_name,$where,$arrayLike, array(), $join);

	    $data['rows'] = $CI->common->getRowsPerPage($select,$table_name,$where,$arrayLike,array($columnOrderBy => $columnOrder),$postData["page"], $join);
	   

	    $config = get_pagination_config($postData["page"], ($count[0]["cnt"]/PER_PAGE), $column, $columnOrder, $function_name);
		//echo $CI->db->last_query();exit;
	    //print_r($config);exit;

		$CI->ajaxpagination->initialize($config);
		$data["pagelinks"] = $CI->ajaxpagination->create_links();
		$from = (($postData["page"]*PER_PAGE)+1);
		$to = (($postData["page"]+1)*PER_PAGE);
		if($to > $count[0]["cnt"])
		{
			$to = $count[0]["cnt"];
		}
		if($to<$from){
			$from = $to;
		}

		$data["entries"] = 'Showing '.$from.' to '.$to.' of '.$count[0]["cnt"].' entries';
       // echo $data["entries"];exit;
		return $data;  
	}
?>
