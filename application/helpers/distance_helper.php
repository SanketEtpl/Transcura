<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function calculateDistance($lat1, $lon1, $lat2, $lon2, $unit) 
	{

	  $theta = $lon1 - $lon2;
	  $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
	  $dist = acos($dist);
	  $dist = rad2deg($dist);
	  $miles = $dist * 60 * 1.1515;
	  $unit = strtoupper($unit);

	  if ($unit == "K") 
	  {
	    return ($miles * 1.609344);
	  } 
	  else if ($unit == "N") 
	  {
	      return ($miles * 0.8684);
	  } 
	  else
	  {
	      return $miles;
	  }
	}



        /*$data = array(

                        'user_id' => $this->input->post('user_id'),
                        'lat' => $lat,
                        'long' => $long,
                        'distance' => $distance,
                        'status' => 'created'
      );*/

      /*if($this->common_model->insert(TB_BOOK_RIDE,$data))
      {
         $driversData = $this->common_model->select('*',TB_DRIVER_LOCATION,$arr);

         if(count($driversData) > 0)
         {
            foreach($driversData as $driver)
            {
                $dis  = calculateDistance($this->input->post('start_lat'),$this->input->post('start_long',$driver['loc_lat'],$driver['loc_lat'],'K'));

                if($dis <= 1)
                {
                    $insertArr = array(

                        'user_id' => $this->input->post('user_id'),
                        'driver_id' => $driver['user_id'], // driver_id
                        'driver_id' => $driver['user_id'],


                    );
                }
            }
         }


        
      }*/
