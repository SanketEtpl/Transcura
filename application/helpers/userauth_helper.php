<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function is_user_logged_in()
{
	$CI = & get_instance();
	if($CI->session->userdata("auth_admin"))
	{
		$sessionArr = $CI->session->userdata("auth_admin");
		return true;
	}
	else
	{
		return false;
	}
}

function get_user_data($var="")
{
	if($var == "")
	{
		return false;
	}
	else
	{
		$CI = & get_instance();
		if($CI->session->userdata("auth_admin"))
		{
			$sessionArr = $CI->session->userdata("auth_admin");
			return $sessionArr[$var];
		}
		else
		{
			return false;
		}
	}
}

function get_user_permission($moduleId,$roleId)
{
	if(is_numeric($moduleId))
	{
		$CI = & get_instance();
		if($CI->session->userdata("auth_admin"))
		{
			$sessionArr = $CI->session->userdata("auth_admin");
			if($roleId !="")
			{
				if($roleId == 1)
				{
					// $CI = get_instance();
					// $CI->load->model("common_model");
					// $arrResult = $CI->common_model->selectQuery("*",TB_EMPLOYEE_PRIVILEGE,array('employee_role_id'=>$roleId,'module_id'=>$moduleId));
					// if(isset($arrResult[0])){
					// 	return $arrResult[0];	
					// }else{
					// 	return false;
					// }
					
					return array("add_privilege"=>1,"update_privilege"=>1,"delete_privilege"=>1,"access_privilege"=>1);
				}
				else
				{
					$CI = get_instance();
					$CI->load->model("common_model");
					$arrResult = $CI->common_model->selectQuery("*",TB_EMPLOYEE_PRIVILEGE,array('employee_role_id'=>$roleId,'module_id'=>$moduleId));
					if(isset($arrResult[0])){
						return $arrResult[0];	
					}else{
						return false;
					}
				}
				
			}else
			{
				return false;//return array("permission_add"=>1,"permission_update"=>1,"permission_delete"=>1,"access"=>1);
			}
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}

function is_ajax_request()
{
	$CI = & get_instance();
	if(!$CI->input->is_ajax_request()) {
	  exit('No direct script access allowed');
	}
	else
	{
		return true;
	}
	
}

function formatDate($date, $format='Y-m-d'){
	return date($format,strtotime(str_replace("/", "-", $date)));
}
?>
