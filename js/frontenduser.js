
  $(document).ready(function()
    {
        var options = { 
          beforeSend: function() 
          {
             $("#loginForm").find('[type="submit"]').toggleClass('sending').blur();
          },
          uploadProgress: function(event, position, total, percentComplete) 
          {
              
          },
          success: function(json) 
          {
            if(json.status == "success"){           
                window.location.href = 'home';
            }else{
                $("#message").html('<div class="alert alert-danger alert-dismissible"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+json.message+'</div>');
            }
            $("#loginForm").find('[type="submit"]').removeClass('sending').blur();
          },
          complete: function(json) 
          {
            $("#loginForm").find('[type="submit"]').removeClass('sending').blur();
          },
          error: function()
          {
              $("#loginForm").find('[type="submit"]').removeClass('sending').blur();
          }
      };
      
      options = $.extend(true, {"dataType":"json"}, options);
      $("#loginForm").ajaxForm(options);
     
    });



 function register() {
                    $('#loginForm').submit(function (e)
                    {
                      //  alert("fgf");
                        e.preventDefault();
                         var username = $('#username').val();
                         var email = $('#email').val();
                         var password = $('#password').val();
                         var confirm_password = $('#confirm_password').val();

                        if (username == "" && email == "" && confirm_password =="" && password=="")
                        {
                            $('#message').show().html('All Fields are required');
                        }
                         
                        else if(username==''){
                            $('#message').show().html('Please enter valid username');
                        }  
                         else if(email==''){
                            $('#message').show().html('Please enter valid email');
                        }  
                         else if(password==''){
                            $('#message').show().html('Please enter valid password');
                        } 
                         else if(confirm_password==''){
                            $('#message').show().html('Please enter confirm password');
                        } 
                        else if(IsEmail(email)==false){
                            $('#message').show().html('Please enter valid email');
                        }
                        else if(password !=confirm_password){
                            $('#message').show().html('confirm password not match with password');
                        }

                        else {
                            $('#message').html("").hide();
                            $.ajax({
                                type: "POST", 
                                url: "home/register_user",
                                data: {username: username, email: email ,password: password, confirm_password: confirm_password},
                                  
                                   success: function (data) {
                                     console.log ( data );
                               // var json = $.parseJSON(data);                                
                                $('#message').fadeIn().html(data);
                                setTimeout(function () {
                                    $('#message').fadeOut("slow");
                                }, 2000);
                            }
                            });
                        }
                    })
              
 }






function login() {
                    $('#loginForm').submit(function (e)
                    {
                      //  alert("fgf");
                        e.preventDefault();
                         var username = $('#username').val();
                         var email = $('#email').val();
                         var password = $('#password').val();
                         

                        if (username == "" && password=="")
                        {
                            $('#message').show().html('All Fields are required');
                        }
                         
                        else if(username==''){
                            $('#message').show().html('Please enter valid username');
                        }  
                          
                         else if(password==''){
                            $('#message').show().html('Please enter valid password');
                        } 
                         
                                               
                        else {
                            $('#message').html("").hide();
                            $.ajax({
                                type: "POST", 
                                url: "home/checkLogin/",
                                data: {username: username,password: password},
                                   success: function (data) {
                               // var json = $.parseJSON(data);                                
                                $('#message').fadeIn().html(data);
                                setTimeout(function () {
                                    $('#message').fadeOut("slow");
                                }, 2000);
                            }
                            });
                        }
                    })
              
 }





function resetpassword() {
                    $('#loginForm').submit(function (e)
                    {
                     alert("fgf");
                        e.preventDefault();
                         var confpwd = $('#confpwd').val();
                         var email = $('#uemail').val();
                         var password = $('#password').val();
                         

                        if (email == "" && password=="" && confpwd=="")
                        {
                            $('#message').show().html('All Fields are required');
                        }
                         
                        else if(email==''){
                            $('#message').show().html('Please enter valid email');
                        }  
                          
                         else if(password==''){
                            $('#message').show().html('Please enter valid password');
                        } 
                         
                          else if(confpwd==''){
                            $('#message').show().html('Please enter valid confirm password');
                        }                       
                        else {
                            $('#message').html("").hide();
                            $.ajax({
                                type: "POST", 
                                url: "home/resetUserPassword/",
                                data: {username: email,password:password,confpwd:confpwd},
                                   success: function (data) {
                               // var json = $.parseJSON(data);                                
                                $('#message').fadeIn().html(data);
                                setTimeout(function () {
                                    $('#message').fadeOut("slow");
                                }, 2000);
                            }
                            });
                        }
                    })
              
 }




 function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
          alert("Please valid Email");
           return false;
        }else{
           return true;
        }
      }
