/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//config.extraPlugins = 'qrc';
	//config.extraPlugins = 'filebrowser';
	config.extraPlugins = 'filebrowser';
	extraPlugins: 'codesnippet,widget';
	
	//config.extraPlugins = 'uploadimage';
	//config.font_names = config.font_names + 'Tempsitc;' ;
	//config.font_names = config.font_names + 'Dancing Script;' ;
	//config.font_names = config.font_names + 'Dancing Script Bold;' ;
};
