var pagenumber = 0;
var inprocess = null;
function listDrivers(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Drivers/listDrivers",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {
    
      inprocess = null;
      if(json.status == "success"){
        $("#listDrivers tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

function getDriver($this){
  $("#userForm")[0].reset();
 // $("#userForm")[0].removeClass("error-block");
 $('#theDiv').html();
 $('#pdf').html();
  $('#pdf2').html();
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Drivers/getDriver",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {

          if(json.status == "success"){   
           console.log(json.userData);
            $("#user_key").val($($this).attr("data-row-id"));
            $("#first_name").val(json.userData.first_name);
           
            $("#address").val(json.userData.address);
            $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);  
            $("#caddress").val(json.userData.company_address);  
            $("#company_name").val(json.userData.company_name); 
          $("#company_contactno").val(json.userData.company_contactno);
          $("#caddress").val(json.userData.company_address);
                                              
             $("#phone").val(json.userData.phone);  
             $("#example1").val(json.userData.date_of_birth); 
               $("#oldimg").val(json.userData.picture); 
                $("#personal_docold").val(json.userData.personal_doc); 

      $("#vehicle_docold").val(json.userData.vehicle_doc); 
              
         //   var abc = json.userData.cat_id;          
      
         loc = json.cData;     

            var chk1=""; 

        $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.picture+" />")           
        if(json.userData.personal_doc!='')
            {

             $('#pdf').html("<a  target='_blank' href="+BASEURL+json.userData.personal_doc+">personal Document</a>")           
             
           }
if(json.userData.vehicle_doc!='')
            {

             $('#pdf2').html("<a  target='_blank' href="+BASEURL+json.userData.vehicle_doc+">vehicle Document</a>")           
             
           }
          
            $("#zipcode").val(json.userData.zipcode);
            jQuery('#userModal').modal('show', {backdrop: 'static'});

          //  toastr.success(json.msg,"Success:");
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}


  function changeStatus($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to change status?'))
        {
    $.ajax({
          type: "POST",
          dataType: "json",
         url: BASEURL+"admin/Drivers/change_status",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
              //    $($this).parents("tr:first").change();
     $("#status").innerHTML=json.userData.status;

            $("#status").val(json.userData.status);
            toastr.success(json.msg,"Success:");
location.reload();
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}
    





var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listDrivers(0,column,order);

$("#listDrivers th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listDrivers th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listDrivers th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listDrivers(0,column,order);
  }
});

$(document).ready(function()
{
  /*$("#userForm").validate({
      rules: {
        first_name: {
              required: true,
          },
        last_name: {
              required: true,
          },
           email: {
              required: true,
          },
        address: {
              required: true,
          },
        phone: {
              required: true,
          },
        gender: {
              required: true,
          }
       
      },
      errorClass: "error-block",
      errorElement: "span",
      messages: {
        first_name: "Please enter your first name",
        last_name: "Please enter your last name",
        email_address: "Please enter your email",
        address: "Please enter your address",
        gender: "Please enter  gender",
        phone: "Please enter your phone",
       
      }
  });*/
      
 $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    {
      if(json.status == "success"){    
     // alert("khjk");  
     $("#userForm").find('[type="submit"]').prop("disabled", false);     
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listDrivers(0,'user','DESC'); 

        }else{
          listDrivers(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");

      }else{
        toastr.error(json.msg,"Error:");
      }


      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    },
    complete: function(json) 
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();


    },
    error: function()
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    }
  });
});
