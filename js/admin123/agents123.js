var pagenumber = 0;
var inprocess = null;
function listAgents(page,column,order){
//alert("ghjghj");
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/agents/listAgents",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {
   
      inprocess = null;
      if(json.status == "success"){
        $("#listAgents tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

// function getUser($this){
//   $("#userForm")[0].reset();
//   if($($this).attr("data-row-id")){
//     $.ajax({
//           type: "POST",
//           dataType: "json",
//           url: BASEURL+"users/getUser",
//           data: {"key":$($this).attr("data-row-id")},
//       }).success(function (json) {
//           if(json.status == "success"){
//             $("#user_key").val($($this).attr("data-row-id"));
//             $("#first_name").val(json.userData.first_name);
//             $("#last_name").val(json.userData.last_name);
//             jQuery('#userModal').modal('show', {backdrop: 'static'});
//             toastr.success(json.msg,"Success:");
//           }else{
//               toastr.error(json.msg,"Error:");
//           }
//       });
//   }else{
//     $("#user_key").val('');
//     jQuery('#userModal').modal('show', {backdrop: 'static'});
//   }
// }

// var column = 'user';
// var order = 'DESC';
// var ordercolumn = column;
// var orderby = order;
// listUsers(0,column,order);

// $("#listUsers th").click(function(){
//   if($(this).hasClass("sorting")){
//     $("#listUsers th").removeClass("sorting_asc").removeClass("sorting_desc");
//       column = $(this).attr("data-name");
//       order = $(this).attr("data-order");
//       ordercolumn = column;
//       orderby = order;
//       $("#listUsers th").attr("data-order","ASC");
//       if(order.toLowerCase() == 'asc'){
//          $(this).attr("data-order","DESC");
//       }else{
//           $(this).attr("data-order","ASC");
//       }
//       $(this).addClass("sorting_"+order.toLowerCase());
//       listUsers(0,column,order);
//   }
// });

function getAgent($this){
  //alert("fdhfgh");
  $("#userForm")[0].reset();
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          // dataType: "json",


          url: BASEURL+"admin/agents/getAgent",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
       // console.log(json);
        var response = JSON.parse(json);
          if(response.status == "success"){            
            $("#user_key").val($($this).attr("data-row-id"));
            $("#first_name").val(response.userData.first_name);
            $("#last_name").val(response.userData.last_name);
            $("#address").val(response.userData.address);
            $("#email").val(response.userData.email);
            $("#phone").val(response.userData.phone);
           
        $('#agencies').empty();
        $('#agencies').show();

         loc = response.agentData;
        //$('#agencies').empty();
        //$('#agencies').show();
     //console.log(loc[0]);
           
        for(var i=0 ; i<loc.length; i++)
        {
           $("#agencies").append("<option value='"+loc[i].agencies_id+"'>"+loc[i].agencies_name+"</option>");
          //   $('#agencies').append($("<option></option>").attr("value",loc[i].agencies_id).text(loc[i].agencies_name));
        }

          var chk1=""; 
            if(response.userData.gender=="Male")
            {
            chk1='<input type="radio" name="gender" value="Male" checked> Male<input type="radio" name="gender" value="Female">Female<br>';
            }
            else
            {
            chk1='<input type="radio" name="gender" value="Male" > Male<input type="radio" name="gender" value="Female" checked>Female<br>';
            }
            $("#gender").html(chk1);
            $('input[name=gender]:checked').val();
            $("#zipcode").val(response.userData.zipcode);
             jQuery('#userModal').modal('show', {backdrop: 'static'});
            
          }else{
          toastr.error(response.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}


  function changeStatus($this)
{
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
         url: BASEURL+"admin/agents/change_user_status",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
                  location.reload();
            //$("#status").val(json.userData.status);
            toastr.success(json.msg,"Success:");

          }else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}
    
 function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test(email)) {
          alert("Please valid Email");
           return false;
        }else{
           return true;
        }
      }




var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listAgents(0,column,order);

$("#listAgents th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listAgents th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listAgents th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listAgents(0,column,order);
  }
});

$(document).ready(function()
{
  $("#userForm").validate({
  //  alert(agencies);
      rules: {
        first_name: {
              required: true,
              minlength: 2,
          },
        last_name: {
              required: true,
              minlength: 2,
          },
           email: {
              required: true,
              
          },
        address: {
              required: true,
              minlength: 2,
          },
        phone: {
              required: true,
               number: true,
          },
        gender: {
              required: true,
          }
             
      },
      errorClass: "error-block",
      errorElement: "span",
      messages: {
        first_name: "Please enter your first name",
        last_name: "Please enter your last name",
        email: "Please enter your email",
        address: "Please enter your address",
        gender: "Please enter  gender",
        phone: "Please enter your phone",
       
      }
  });
      
  $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    {
      if(json.status == "success"){           
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){

          listAgents(0,'user','DESC'); 
        }else{
          listAgents(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else{
        toastr.error(json.msg,"Error:");
      }
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    },
    complete: function(json) 
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    },
    error: function()
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    }
  });

});
