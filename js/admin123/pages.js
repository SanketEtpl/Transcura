var pagenumber = 0;
var inprocess = null;
CKEDITOR.replace('page_detail');

var editorElement = CKEDITOR.document.getById( 'page_detail' );
CKEDITOR.instances.page_detail.on('change', function() {
         $("#page_detail").val(CKEDITOR.instances['page_detail'].getData());
    //alert(CKEDITOR.instances['page_detail'].getData());
      });
function listPages(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/pages/listPages",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
    console.log(json);
      inprocess = null;
      if(json.status == "success"){
          $("#listPages tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}



function deletePage($this){

if($($this).attr("data-row-id")){
   // alert($($this).attr("data-row-id"));
   if (confirm("Do you want to delete")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/pages/deletePage",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
           if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
                 $($this).parents("tr:first").remove();
            //$("#status").val(json.userData.status);
            toastr.success(json.msg,"Success:");

          }else{
              toastr.error(json.msg,"Error:");
          }
      });
  }
  
  }
  else
  {
   return false;

  }
}




function getPage($this){
  $("#pageForm")[0].reset();
  $("#pageForm span.error-block").css("display","none");
  $("#pageForm .form-control").removeClass("error-block");
  if($($this).attr("data-row-id")){
    //alert($($this).attr("data-row-id"));
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/pages/getPage",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
           // alert(json.pageData.title);
            $("#page_key").val($($this).attr("data-row-id"));
            $("#title").val(json.pageData.page_title);
             $("#page_detail").val(json.pageData.page_detail);
             CKEDITOR.instances['page_detail'].setData(json.pageData.page_detail)
           jQuery('#pageModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#page_key").val('');
    jQuery('#pageModal').modal('show', {backdrop: 'static'});
  }
}

var column = 'fname';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listPages(0,column,order);

$("#listPages th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listPages th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
    orderby = order;
      $("#listPages th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listPages(0,column,order);
  }
});

$(document).ready(function()
{
  // $("#pageForm").validate({
  //     rules: {
  //       title: {
  //             required: true,
  //         },
  //       page_detail: {
  //             required: true,
  //         }
        
  //     },
  //     errorClass: "error-block",
  //     errorElement: "span",
  //     messages: {
  //       title: "Please enter content title  ",
  //       page_detail: "Please enter content details"
        
  //     }
  // });
  $("#pageForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#pageForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
 $("#pageForm").find('[type="submit"]').prop("disabled", false); 
          toastr.success(json.msg,'Success:');
          jQuery('#pageModal').modal('hide');
          if(json.action == "add"){
            listPages(0,'page_id','DESC'); 
          }else{
            listPages(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
        }
        $("#pageForm").find('[type="submit"]').removeClass('sending').blur();
      },
      complete: function(json) 
      {
          $("#pageForm").find('[type="submit"]').removeClass('sending').blur();
      },
      error: function()
      {
          $("#pageForm").find('[type="submit"]').removeClass('sending').blur();
      }
  });
});
