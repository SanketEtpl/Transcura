var pagenumber = 0;
var inprocess = null;
function listService_type(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Service_types/listService_type",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {
    
      inprocess = null;
      if(json.status == "success"){
        $("#listService_type tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

//$("#addCatBtn").prop("disabled", false); for adding category

function getService_type($this){
   $("#userForm")[0].reset();
    $('#userfile').html();
    $('#userfile1').html();
     $("#oldimg").val(); 
 // $("#userForm")[0].removeClass("error-block");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Service_types/getService_type",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){            
            $("#user_key").val($($this).attr("data-row-id"));
            $("#type_title").val(json.userData.type_title);  
            $("#oldimg").val(json.userData.image); 

          
            $('#theDiv').html("<img name='userfile1' id='userfile1' src="+BASEURL+json.userData.image+" width='80' height='80'/>")           
            jQuery('#userModal').modal('show', {backdrop: 'static'});


           //toastr.success(json.msg,"Success:");
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}



 function deleteService_type($this,id)
{

 
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to delete type?'))
        {
    $.ajax({
          type: "POST",
          dataType: "json",
         url: BASEURL+"admin/Service_types/deleteService_type",
          data: {"key":$($this).attr("data-row-id"),"id":id},
      }).success(function (json) {
          if(json.status == "success"){
             toastr.success(json.msg,"Success:");
           $($this).parents("tr:first").remove();

          }else{
              toastr.error(json.msg,"Error:");
          }
           
      });
 } }else{
    $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}
 


  $("input[name=userfile]").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        var img = $('<img>').attr('src', e.target.result);
                        $('#theDiv').html(img);
                         $('#userfile1').html(img);
                    };

                    reader.readAsDataURL(this.files[0]);
                    // $('#theDiv').hide();
                }
            });



var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listService_type(0,column,order);

$("#listService_type th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listService_type th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listService_type th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listService_type(0,column,order);
  }
});

$(document).ready(function()
{
  /*$("#userForm").validate({
      rules: {
        first_name: {
              required: true,
          },
        last_name: {
              required: true,
          },
           email: {
              required: true,
          },
        address: {
              required: true,
          },
        phone: {
              required: true,
          },
        gender: {
              required: true,
          }
       
      },
      errorClass: "error-block",
      errorElement: "span",
      messages: {
        first_name: "Please enter your first name",
        last_name: "Please enter your last name",
        email_address: "Please enter your email",
        address: "Please enter your address",
        gender: "Please enter  gender",
        phone: "Please enter your phone",
       
      }
  });*/
      
  $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    {
      if(json.status == "success"){    
     // alert("khjk");  
     $("#userForm").find('[type="submit"]').prop("disabled", false);     
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listService_type(0,'user','DESC'); 

        }else{
          listService_type(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");

      }else{
        toastr.error(json.msg,"Error:");
      }


      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    },
    complete: function(json) 
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();


    },
    error: function()
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    }
  });
});
