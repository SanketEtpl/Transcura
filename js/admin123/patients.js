var pagenumber = 0;
var inprocess = null;
function listPatients(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/patients/listPatients",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {
    
      inprocess = null;
      if(json.status == "success"){
        $("#listPatients tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}




function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#theDiv')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }



function viewDetails($this)
{


 $("#userForm")[0].reset();
  $('#insurance_provider').show();
 // $("#userForm")[0].removeClass("error-block");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Patients/getPatient",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {

          if(json.status == "success"){ 
            $("#user_key").val($($this).attr("data-row-id"));
            $("#first_name").val(json.userData.first_name);
            $("#econtact_name").val(json.userData.emergency_contactname);
             $("#econtact_no").val(json.userData.emergency_contactno);             
            $("#address").val(json.userData.address);
            $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);  
            $("#example1").val(json.userData.date_of_birth);
             $("#oldimg").val(json.userData.picture); 
           $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);        
           $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.picture+" />")           
            jQuery('#userModal').modal('show', {backdrop: 'static'});   
         
            $("#zipcode").val(json.userData.zipcode);
            jQuery('#userModal1').modal('show', {backdrop: 'static'});        
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal1').modal('show', {backdrop: 'static'});
  }


}



function getPatient($this){
   $("#userForm")[0].reset();
  $('#insurance_provider').show();
 // $("#userForm")[0].removeClass("error-block");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Patients/getPatient",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
         console.log(json);
          if(json.status == "success"){ 
//console.log(json.insurancelist);
          //console.log(json.userData.date_of_birth.format('d/m/yyyy'));
            $("#user_key").val($($this).attr("data-row-id"));
            $("#first_name").val(json.userData.first_name);
            $("#econtact_name").val(json.userData.emergency_contactname);
             $("#econtact_no").val(json.userData.emergency_contactno);             
            $("#address").val(json.userData.address);
            $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);  
            $("#example1").val(json.userData.date_of_birth);
             $("#oldimg").val(json.userData.picture);
             $("#insurance_id").val(json.userData.insurance_id); 

           $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);  
            $("#signature").val(json.userData.signature);  

            $( "#signature option:selected" ).text();
             $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.picture+" />")           
       
            // if (json.userData.gender == 'Female') {
            // $('input:radio[name="gender"]').filter('[value="Female"]').attr('checked', true);
            // }
            // else
            // {
            // $('input:radio[name="gender"]').filter('[value="Male"]').attr('checked', true);
            // }
           if(json.userData.personal_doc!='')
            {
           $('#pdf').html("<a  target='_blank' href="+BASEURL+json.userData.personal_doc+">personal Document</a>")           
             
            }
            var chk1=""; 
           
            $("#zipcode").val(json.userData.zipcode);
            jQuery('#userModal').modal('show', {backdrop: 'static'});

          //  toastr.success(json.msg,"Success:");
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}





  function changeStatus($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to change status?'))
        {
    $.ajax({
          type: "POST",
          dataType: "json",
         url: BASEURL+"admin/Patients/change_user_status",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
                
           $("#status").innerHTML=json.userData.status;
            toastr.success(json.msg,"Success:");

          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}
    





var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listPatients(0,column,order);

$("#listPatients th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listPatients th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listPatients th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listPatients(0,column,order);
  }
});

$(document).ready(function()
{
  /*$("#userForm").validate({
      rules: {
        first_name: {
              required: true,
          },
        last_name: {
              required: true,
          },
           email: {
              required: true,
          },
        address: {
              required: true,
          },
        phone: {
              required: true,
          },
        gender: {
              required: true,
          }
       
      },
      errorClass: "error-block",
      errorElement: "span",
      messages: {
        first_name: "Please enter your first name",
        last_name: "Please enter your last name",
        email_address: "Please enter your email",
        address: "Please enter your address",
        gender: "Please enter  gender",
        phone: "Please enter your phone",
       
      }
  });*/
      
  $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    { //location.reload();
      if(json.status == "success"){           
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listPatients(0,'user','DESC'); 
        }else{
          listPatients(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else{
        toastr.error(json.msg,"Error:");
      }
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
$("#userForm").find('[type="submit"]').removeAttr('disabled');
    },
    complete: function(json) 
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
$("#userForm").find('[type="submit"]').removeAttr('disabled');
    },
    error: function()
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
$("#userForm").find('[type="submit"]').removeAttr('disabled');
    }
  });
});
