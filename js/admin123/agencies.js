var pagenumber = 0;
var inprocess = null;
CKEDITOR.replace('description');

var editorElement = CKEDITOR.document.getById( 'description' );
CKEDITOR.instances.description.on('change', function() {
         $("#description").val(CKEDITOR.instances['description'].getData());
    //alert(CKEDITOR.instances['page_detail'].getData());
      });
function listAgencies(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/agencies/listAgencies",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {
    
      inprocess = null;
      if(json.status == "success"){
        $("#listAgencies tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

// function getUser($this){
//   $("#userForm")[0].reset();
//   if($($this).attr("data-row-id")){
//     $.ajax({
//           type: "POST",
//           dataType: "json",
//           url: BASEURL+"users/getUser",
//           data: {"key":$($this).attr("data-row-id")},
//       }).success(function (json) {
//           if(json.status == "success"){
//             $("#user_key").val($($this).attr("data-row-id"));
//             $("#first_name").val(json.userData.first_name);
//             $("#last_name").val(json.userData.last_name);
//             jQuery('#userModal').modal('show', {backdrop: 'static'});
//             toastr.success(json.msg,"Success:");
//           }else{
//               toastr.error(json.msg,"Error:");
//           }
//       });
//   }else{
//     $("#user_key").val('');
//     jQuery('#userModal').modal('show', {backdrop: 'static'});
//   }
// }

// var column = 'user';
// var order = 'DESC';
// var ordercolumn = column;
// var orderby = order;
// listUsers(0,column,order);

// $("#listUsers th").click(function(){
//   if($(this).hasClass("sorting")){
//     $("#listUsers th").removeClass("sorting_asc").removeClass("sorting_desc");
//       column = $(this).attr("data-name");
//       order = $(this).attr("data-order");
//       ordercolumn = column;
//       orderby = order;
//       $("#listUsers th").attr("data-order","ASC");
//       if(order.toLowerCase() == 'asc'){
//          $(this).attr("data-order","DESC");
//       }else{
//           $(this).attr("data-order","ASC");
//       }
//       $(this).addClass("sorting_"+order.toLowerCase());
//       listUsers(0,column,order);
//   }
// });

function getAgency($this){
  $("#userForm")[0].reset();
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/agencies/getAgency",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
//console(json.userData);          
            $("#user_key").val($($this).attr("data-row-id"));
            $("#agencies_name").val(json.userData.agencies_name);
            
            $("#address").val(json.userData.address);
            $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone); 
            $("#website").val(json.userData.website);   
            $("#fax").val(json.userData.Fax);   
            $("#description").val(json.userData.description);  
  $('#theDiv').html("<img name='userfile1' id='userfile1' src="+BASEURL+json.userData.logo+" width='150' height='100'/><input type='hidden' name='uploadfile1' value='"+json.userData.logo+"'/>")
              
            CKEDITOR.instances['description'].setData(json.userData.description) 
            $("#zipcode").val(json.userData.zipcode);
            jQuery('#userModal').modal('show', {backdrop: 'static'});            
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}


  function changeStatus($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to change status?'))
        {
    $.ajax({
          type: "POST",
          dataType: "json",
         url: BASEURL+"admin/agencies/change_user_status",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
                  location.reload();
            //$("#status").val(json.userData.status);
            toastr.success(json.msg,"Success:");

          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}
var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listAgencies(0,column,order);

$("#listAgencies th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listAgencies th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listAgencies th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listAgencies(0,column,order);
  }
});

$(document).ready(function()
{
  /*$("#userForm").validate({
      rules: {
        agencies_name: {
              required: true,
          },
        email: {
              required: true,
               email: true,
          },
        address: {
              required: true,
          },
        phone: {
              required: true,
          },
        zipcode: {
              required: true,
          }
      },
      errorClass: "error-block",
      errorElement: "span",
      messages: {
        agencies_name: "Please enter your Agencies name",
        email: "Please enter your email",
        address: "Please enter your address",
        zipcode: "Please enter zipcode",
        phone: "Please enter your phone",
       
      }
  });*/
      
  $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    {
      //location.reload();
      if(json.status == "success"){           
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listAgencies(0,'user','DESC'); 
        }else{
          listAgencies(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else{
        toastr.error(json.msg,"Error:");
      }
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
   $("#userForm").find('[type="submit"]').removeAttr('disabled');
    },
    complete: function(json) 
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
   $("#userForm").find('[type="submit"]').removeAttr('disabled');
    },
    error: function()
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    $("#userForm").find('[type="submit"]').removeAttr('disabled');
    }
  });
});
