//Add Google Place Api
app.directive('googleplace', function($rootScope) {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, model) {
            var options = {
                types: [],
                componentRestrictions: {country: 'au'}
            };
            
            scope.gPlace = new google.maps.places.Autocomplete(element[0], options);
			
            google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                scope.$apply(function() {
                    model.$setViewValue(element.val());
                    scope.details = scope.gPlace.getPlace();
                    $rootScope.addressdetails = scope.details;
                });
            });
        }
    };
});


app.directive('fileModel', ['$parse', function ($parse) {
            return {
               restrict: 'A',
               link: function(scope, element, attrs) {
                  var model = $parse(attrs.fileModel);
                  var modelSetter = model.assign;
                  
                  element.bind('change', function(){
                     scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                     });
                  });
               }
            };
}]);
      
app.service('fileUpload', ['$http', function ($http) {
            this.uploadFileToUrl = function(file, uploadUrl){
               var fd = new FormData();

               fd.append('file', file);
            
               $http.post(uploadUrl, fd, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
               })
            
               .success(function(response){
                  console.log(response);
               })
            
               .error(function(error){
               });
            }
}]);


app.controller('staff', ['$scope', 'fileUpload','$http','$state', '$stateParams', function($scope, fileUpload,$http,$state, $stateParams){
           console.log($stateParams.pageTitle);
          $scope.add_staff = function(){
        if($scope.agencyAddStaffForm.$valid) {
          var file = $scope.myFile;


          var agencyStaffData = {"namefirst":$scope.firstName,"namelast":$scope.lastName,"email":$scope.email,"Contact":$scope.contact, "Address":$scope.address,"Sale":$scope.saleProperty,"Sold":$scope.soldProperty,"aboutMe":$scope.aboutMe,"profile_pic":file['name']}
          $http.post(base_url + 'api/Api/add_agency_staff',agencyStaffData,{timeout: 45000}).then(function successCallback (response){
               
               //Add Profile pic start
               var uploadUrl = base_url+"api/Api/add_profile";
               fileUpload.uploadFileToUrl(file, uploadUrl);
               //End Profile pic start

               $scope.success = response.data.message;
               $scope.priority = 'success';
               $scope.title    = 'Success';
               $.toaster({ priority : $scope.priority, title : $scope.title, message : $scope.success });
               $state.go('home');
            },function errorCallback(error) {
                $scope.success = error.data.message;
                $scope.priority = 'danger';
                $scope.title    = 'Notice';
                $.toaster({ priority : $scope.priority, title : $scope.title, message : $scope.success });
            });
        }
        else {
          $scope.agencyStafferror = true;
        }
      };
}]);


app.controller('news', ['$scope','$filter', 'fileUpload','$http','$state', function($scope,filter, fileUpload,$http,$state){
           
          $scope.add_news = function(){      
        if($scope.agencyAddnewsForm.$valid) {
          //alert($scope);
 
          var file = $scope.myFile;
          //alert(file['name']);
          var agencyNewsData = {"newstitle":$scope.newstitle,"authorname":$scope.authorname,"comment":$scope.comment,"date":$scope.date,"news_image":file['name']}
       //  alert(JSON.stringify(agencyNewsData);

          $http.post(base_url + 'api/Api/add_agency_news',agencyNewsData,{timeout: 45000}).then(function successCallback (response){
               
             
             //Add Profile pic start
               var uploadUrl = base_url+"api/Api/add_newsimage";
               fileUpload.uploadFileToUrl(file, uploadUrl);
               //End Profile pic start
             
               $scope.success = response.data.message;
               $scope.priority = 'success';
               $scope.title    = 'Success';
               $.toaster({ priority : $scope.priority, title : $scope.title, message : $scope.success });
               $state.go('home');
            },function errorCallback(error) {
                $scope.success = error.data.message;
                $scope.priority = 'danger';
                $scope.title    = 'Notice';
                $.toaster({ priority : $scope.priority, title : $scope.title, message : $scope.success });
            });
        }
        else {
          $scope.agencyNewserror = true;
       }
      };
}]);

///// footer logo /////
app.controller('AgencyLogos', ['$scope', 'fileUpload','$http','$state', function($scope, fileUpload,$http,$state){
           
          $scope.AgencyLogoslist = function(){
      

        $http.get(base_url + 'api/Api/get_agencylogos').then(function successCallback (response){ 
        $scope.agencylogos = response.data;
        //  $scope.newsProperty=JSON.stringify($scope.NewsDatas);

        console.log($scope.agencylogos );
        },function errorCallback(error) {});

          
      };
}]);



app.controller('propertyDetails', ['$scope', 'fileUpload','$http','$state', function($scope, fileUpload,$http,$state){
          
          $scope.getPropertyDetails = function(){
  
        $http.get(base_url + 'api/Api/properties_details').then(function successCallback (response){ 
         
        $scope.dataProperty = response.data.propertyList.residential;
         alert(JSON.stringify($scope.dataProperty));
        
        console.log($scope.dataProperty);
        },function errorCallback(error) {});

          
      };
}]);




app.controller('featuredProperties',function($scope){

 $scope.properties = [
        {propertyImage:'img/bg/1.jpg',address:'231 Main Rd, Chewton,',country:'Australia', bed:'4',shower:'4',carParking:'4',amount:'$ 162,000'},
        {propertyImage:'img/bg/2.jpg',address:'231 Main Rd, Chewton,',country:'Australia', bed:'4',shower:'4',carParking:'4',amount:'$ 162,000'},
        {propertyImage:'img/bg/3.jpg',address:'231 Main Rd, Chewton,',country:'Australia', bed:'4',shower:'4',carParking:'4',amount:'$ 162,000'},
        {propertyImage:'img/bg/4.jpg',address:'231 Main Rd, Chewton,',country:'Australia', bed:'4',shower:'4',carParking:'4',amount:'$ 162,000'},
        {propertyImage:'img/bg/5.jpg',address:'231 Main Rd, Chewton,',country:'Australia', bed:'4',shower:'4',carParking:'4',amount:'$ 162,000'},
        {propertyImage:'img/bg/6.jpg',address:'231 Main Rd, Chewton,',country:'Australia', bed:'4',shower:'4',carParking:'4',amount:'$ 162,000'},
        {propertyImage:'img/bg/7.jpg',address:'231 Main Rd, Chewton,',country:'Australia', bed:'4',shower:'4',carParking:'4',amount:'$ 162,000'}
        ];
        
   

});

//app.controller('newsCntrl',function($scope){
app.controller('newsCntrl',['$scope','$http','$window','$rootScope','$state','fileUpload', 
  function($scope,$http,$window,$rootScope,$state,fileUpload){
 
  $scope.getNewsData = function (){

       $http.get(base_url + 'api/Api/get_news_data').then(function successCallback (response){ 
        $scope.NewsDatas = response.data;
         console.log($scope.NewsDatas);
      },function errorCallback(error) {
      });
   


/*$scope.newsProperty = [

        {propertyImage:'img/icons/news1.png',des1:'Australia most in-demand areas', byDes:'by Danielle Cahill', des2:'Looking for a room to rent in Sydney? You are not alone, with 13 ...'},
        {propertyImage:'img/icons/news2.jpg',des1:'Australia most in-demand areas', byDes:'by Danielle Cahill', des2:'Looking for a room to rent in Sydney? You are not alone, with 13 ...'},
        {propertyImage:'img/icons/news3.jpg',des1:'Australia most in-demand areas', byDes:'by Danielle Cahill', des2:'Looking for a room to rent in Sydney? You are not alone, with 13 ...'},
        {propertyImage:'img/icons/news1.png',des1:'Australia most in-demand areas', byDes:'by Danielle Cahill', des2:'Looking for a room to rent in Sydney? You are not alone, with 13 ...'},
        {propertyImage:'img/icons/news2.jpg',des1:'Australia most in-demand areas', byDes:'by Danielle Cahill', des2:'Looking for a room to rent in Sydney? You are not alone, with 13 ...'},
        {propertyImage:'img/icons/news3.jpg',des1:'Australia most in-demand areas', byDes:'by Danielle Cahill', des2:'Looking for a room to rent in Sydney? You are not alone, with 13 ...'}

 ];
*/

}
 }]);





//Controller for Agency registration 
app.controller('agencyRegister',['$scope','$http','$window','$rootScope','$state','fileUpload', function($scope,$http,$window,$rootScope,$state,fileUpload){
 //$rootScope.titles = "Agency List";

 $scope.regAgencyUser =  function(){
  if($scope.agencyReg.$valid) {
  
  var agencyRegData = {"email":$scope.emailAddress,"password":$scope.password}
    $http.post(base_url + 'api/Api/registeragencyUser',agencyRegData,{timeout: 45000}).then(function successCallback (response){
  
      $scope.statussuccess = response.status;

      if($scope.statussuccess == 200){
         $scope.success = response.data.message;

         $scope.priority = 'success';
         $scope.title    = 'Success';
         $.toaster({ priority : $scope.priority, title : $scope.title, message : $scope.success });
      }

    },function errorCallback(error) {
     
       $scope.statuserroe = error.status;

       if($scope.statuserroe == 409){
          $scope.success = error.data.message;

          $scope.priority = 'danger';
          $scope.title    = 'Notice';
          $.toaster({ priority : $scope.priority, title : $scope.title, message : $scope.success });
       }


    });
    }
    else{
      $scope.agencyRegerror = true;
    }
 }

 $scope.loginAgency = function(){
  if($scope.agencySignIn.$valid) {
    
    var loginData = {"username":$scope.emailId,"password":$scope.password}
    $http.post(base_url + 'api/Api/agencyLogin',loginData,{timeout: 45000}).then(function successCallback (response){
     $scope.success = response.data.message;

         $scope.priority = 'success';
         $scope.title    = 'Success';
         $.toaster({ priority : $scope.priority, title : $scope.title, message : $scope.success });

     $window.location.href = base_url+'agency';

    },function errorCallback(error) {
      
      $scope.success = error.data.message;

          $scope.priority = 'danger';
          $scope.title    = 'Notice';
          $.toaster({ priority : $scope.priority, title : $scope.title, message : $scope.success });
       
    });

  }else{
  
    $scope.loginerror = true;
  }
 }
 
     $scope.forgetPass = function(){
      if($scope.forgetPassword.$valid) {
      var params = {"email":$scope.email}
      $http.post(base_url + 'api/Api/forgot_password_agency',params,{timeout: 45000}).then(function successCallback (response){
    
      },function errorCallback(error) {
       $scope.errorEmailId = error.data.message;
      });
    }
    else {
      $scope.forgetPassError = true;
    }
     }

     $scope.errorHandel = function(){
       $scope.errorEmailId ="";
     }

// To get the agency data
     $scope.getAgencyData = function (){
      $http.get(base_url + 'api/Api/get_agency_data').then(function successCallback (response){ 
       $rootScope.agencyData = response.data;
      },function errorCallback(error) {
      });
    }

// To get the agency staff data
    $scope.getAgencyStaffData = function (){
    $http.get(base_url + 'api/Api/get_agency_staff_data').then(function successCallback (response){
         $scope.agencyStaffDatas = response.data;
      },function errorCallback(error) {
      });
    }
// To get the agency staff data
    $scope.getAgencyNewsData = function (){
     // alert("iuyi");
    $http.get(base_url + 'api/Api/get_news_agency_data').then(function successCallback (response){
         $scope.agencyNewsDatas = response.data;
        // alert(JSON.stringify($scope.agencyNewsDatas));
      },function errorCallback(error) {
      });
    }


// To add agency news data
     $scope.addAgencyNewsData = function (){  
    // alert("gghj"); 
     $http.get(base_url + 'api/Api/add_agency_news').then(function successCallback (response){ 
       $scope.agencynewsDatas = response.data;
       console.log($scope.agencynewsDatas);
     },function errorCallback(error) {     
      
      });
    }

    $scope.viewStaffProfile = function(agent_id){
      $http.get(base_url + 'api/Api/view_staff_profile?agent_id='+agent_id).then(function successCallback (response){
        $rootScope.data = response.data[0];
        console.log($rootScope.data);
        $state.go('staffprofile');
      },function errorCallback(error) {
      });
    }

}]);

app.controller('register', ['$scope','$http','$window', function($scope,$http,$window) {
  
	$scope.registeruser = function() {
		
		if($scope.frmregister.$valid) {
		var params = {"email":$scope.email,"password":$scope.password}
		$http.post(base_url + 'api/Api/registerUser',params,{timeout: 45000}).then(function successCallback (response){
     
      $scope.resultMessage  = response.data.message;

         $scope.priority = 'success';
         $scope.title    = 'Success';
         $.toaster({ priority : $scope.priority, title : $scope.title, message : $scope.resultMessage });
      
     
		},function errorCallback(error) {
			//error callback
			
		  $scope.resultMessage  = error.data.message;
     
		  $scope.priority = 'danger';
		  $scope.title    = 'Notice';
		  $.toaster({ priority : $scope.priority, title : $scope.title, message : $scope.resultMessage });
      
		});
		}
		else{
			$scope.showerror = true;
		}
	}

  $scope.errorHandel = function (){
    $scope.resultMessage = "";
    
  }

	$scope.dologin = function() {
		if($scope.frmlogin.$valid) {
		var params = {"username":$scope.loginusername,"password":$scope.loginpassword}
		$http.post(base_url + 'api/Api/loginUser',params,{timeout: 45000}).then(function successCallback (response){
			
			$window.location.reload();

		},function errorCallback(error) {
			$scope.loginerror = true;
			$scope.loginerror = error.data.message;
			
          $scope.priority = 'danger';
          $scope.title    = 'Notice';
          $.toaster({ priority : $scope.priority, title : $scope.title, message : $scope.loginerror });
		});
		}
		else{
			$scope.showerrorlogin = true;
		}
	}

  
	
	$scope.forgotpassword = function() {
		if($scope.frmforgot.$valid) {
			var params = {"email":$scope.email}
			$http.post(base_url + 'api/Api/forgot_password',params,{timeout: 45000}).then(function successCallback (response){
			//success callback
       
        $scope.successmsg = response.data.message;
			},function errorCallback(error) {
				//error callback
				$scope.forgoterror = true;
				$scope.forgoterror = error.data.message;
			});
		}
		else {
			$scope.showforgoterror = true;
		}
	}

	// Fb Login

	window.fbAsyncInit = function() {
	    // FB JavaScript SDK configuration and setup
	    FB.init({
	      appId      : '989960711133928', // FB App ID
	      cookie     : true,  // enable cookies to allow the server to access the session
	      xfbml      : true,  // parse social plugins on this page
	      version    : 'v2.8' // use graph api version 2.8
	    });
	};

	// Load the JavaScript SDK asynchronously
	(function(d, s, id) {
	    var js, fjs = d.getElementsByTagName(s)[0];
	    if (d.getElementById(id)) return;
	    js = d.createElement(s); js.id = id;
	    js.src = "//connect.facebook.net/en_US/sdk.js";
	    fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));


	$scope.fbLogin = function() {
		FB.login(function (response) {
	        if (response.authResponse) {
	            // Get and display the user profile data
	            $scope.getFbUserData();
	        } else {
	            $scope.error = 'User cancelled login or did not fully authorize.';
	        }
    	}, {scope: 'email'});
	}

	$scope.getFbUserData = function() {
		FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
	    function (response) {
	        
	        var params = {"first_name":response['first_name'],"last_name":response['last_name'],"email":response['email'],"provider":'facebook'}
	    	$http.post(base_url + 'api/Api/fblogin',params,{timeout: 45000}).then(function successCallback (response){
			
			$window.location.reload();
			//success callback
			},function errorCallback(error) {
				//error callback
				
			});
	    });
	}

	$scope.googleLogin = function() {
		var myParams = {
			   'clientid' : '115185319857-sv7ok0gadgjq9p7i8n9ipocs1a01t8l0.apps.googleusercontent.com',
			   'cookiepolicy' : 'single_host_origin',
			   'callback' : 'loginCallback',
			   'approvalprompt':'force',
			   'scope' : 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read'
			};
  			gapi.auth.signIn(myParams);
	}

	function loginCallback(result)
	{
    	if(result['status']['signed_in'])
    	{
        	var request = gapi.client.plus.people.get(
        {
            'userId': 'me'
        });
        request.execute(function (resp)
        {
            var email = '';
            if(resp['emails'])
            {
                for(i = 0; i < resp['emails'].length; i++)
                {
                    if(resp['emails'][i]['type'] == 'account')
                    {
                        email = resp['emails'][i]['value'];
                    }
                }
            }
 
            var str = "Name:" + resp['displayName'] + "<br>";
            str += "Image:" + resp['image']['url'] + "<br>";
            str += "<img src='" + resp['image']['url'] + "' /><br>";
 
            str += "URL:" + resp['url'] + "<br>";
            str += "Email:" + email + "<br>";
            document.getElementById("profile").innerHTML = str;
        });
 
    }
 
}


	function onLoadCallback() {
		gapi.client.setApiKey('AIzaSyBknx_Ph67NtfZHiB6nCqVeplPBdDQtWFs');
    	gapi.client.load('plus', 'v1',function(){});
	}

	(function() {
       var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
       po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
       var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
     })();

}]);


app.controller('loginstatus', ['$scope','$http','$window', function($scope,$http,$window) {
	$scope.logoutstatus = true;
	$scope.chkloginstatus = function() {
		$http.get(base_url + "api/Api/checkloginstatus")
        .success(function successCallback(response) {
			$scope.loginstatus = true;
			$scope.logoutstatus = false;
		},function errorCallback(response){
		});
	}

$scope.chkloginstatus1 = function() {
    $http.get(base_url + "api/Api/checkloginstatus1")
        .success(function successCallback(response) {
      $scope.loginstatus = true;
      $scope.logoutstatus = false;
    },function errorCallback(response){
    });
  }


	$scope.logout = function() {
		$http.get(base_url + "api/Api/logout")
        .success(function successCallback(response) {
        	$window.location.href = base_url;
		},function errorCallback(response) {

		});
	}

}]);


app.controller('search',['$scope','$http',function($scope,$http) {

$(function() {
				$scope.min = '2';
        $scope.max = '4';
        $scope.bathmin = '1';
        $scope.bathmax = '3';
        $scope.pricemin = '2500';
        $scope.pricemax = '35000';

        $( "#slider-range" ).slider({
          range: true,
          min: 0,
          max: 5,
          values: [ 2, 4 ],
          slide: function( event, ui ) {
            $scope.min = ui.values[0];
            $scope.max = ui.values[1];
          }
        });

        $( "#slider-range1" ).slider({
          range: true,
          min: 0,
          max: 5,
          values: [ 2, 4 ],
          slide: function( event, ui ) {
            $( "#amount1" ).val( "" + ui.values[ 0 ] + " - " + ui.values[ 1 ] );
          }
        });

        $( "#amount1" ).val( "" + $( "#slider-range1" ).slider( "values", 0 ) +
          " - " + $( "#slider-range1" ).slider( "values", 1 ) );

        $( "#amount2" ).val( "" + 2500 + " - " + 35000 );
        $( "#slider-range2" ).slider({
          range: true,
          min: 0,
          max: 99000,
          values: [2500, 35000],
          slide: function( event, ui ) {
            $( "#amount2" ).val( "" + ui.values[ 0 ] + " - " + ui.values[ 1 ] );
            $scope.pricemin = ui.values[0];
            $scope.pricemax = ui.values[1];
          }
        });

        $( "#slider-range4" ).slider({
          range: true,
          min: 0,
          max: 3,
          values: [1, 3 ],
          slide: function( event, ui ) {
            $scope.bathmin = ui.values[0];
            $scope.bathmax = ui.values[1];
          }
        });


        $( "#slider-range6" ).slider({
          range: true,
          min: 0,
          max: 3,
          values: [1, 3 ],
          slide: function( event, ui ) {
            $( "#amount6" ).val( "" + ui.values[ 0 ] + " - " + ui.values[ 1 ] );
            
          }
        });

        $( "#amount6" ).val( "" + $( "#slider-range6" ).slider( "values", 0 ) +
          " - " + $( "#slider-range6" ).slider( "values", 1 ) );


        $( "#slider-range3" ).slider({
          range: true,
          min: 0,
          max: 99000,
          values: [ 2500, 35000 ],
          slide: function( event, ui ) {
            $( "#amount3" ).val( "" + ui.values[ 0 ] + " - " + ui.values[ 1 ] );
          }
        });

        $( "#amount3" ).val( "" + $( "#slider-range3" ).slider( "values", 0 ) +
          " - " + $( "#slider-range3" ).slider( "values", 1 ) );

      });

	$scope.search = function ($scope) {
    $scope.data = {
        selectedIndex: 0,
        secondLocked: true,
        secondLabel: "2",
        bottom: false
    };
    $scope.next = function() {
        $scope.data.selectedIndex = Math.min($scope.data.selectedIndex + 1, 2);
    };
    $scope.previous = function() {
        $scope.data.selectedIndex = Math.max($scope.data.selectedIndex - 1, 0);
    };
  }

  $scope.search_property = function() {
  	/*alert($scope.search_location);
  	alert($scope.min);
  	alert($scope.max);
  	alert($scope.bathmin);
  	alert($scope.bathmin);
  	alert($scope.pricemin);
  	alert($scope.pricemax);*/
$state.go('propertyList');

  }
}]);


app.controller('background',['$scope','$http',function($scope,$http){
		//get user Location
    var loc = {};
    var geocoder = new google.maps.Geocoder();
    if(google.loader.ClientLocation) {
        loc.lat = google.loader.ClientLocation.latitude;
        loc.lng = google.loader.ClientLocation.longitude;
        var latlng = new google.maps.LatLng(loc.lat, loc.lng);
        geocoder.geocode({'latLng': latlng}, function(results, status) {
            if(status == google.maps.GeocoderStatus.OK) {
                $scope.city = results[0]['address_components'][2]['long_name'];
                //get city image
                  $http.get(base_url + "api/Api/get_client_city?client_city="+$scope.city)
        					.success(function successCallback(response) {
        						
        						if(response['status']==true) {
        							$scope.bgimg = base_url +''+response['data'][0]['image'];
        						}
								   	else {
								   		$scope.bgimg = base_url +'img/icons/aus-vic.png';
								   	}
								  },function errorCallback(error){
								  	
								  });
            };
        });
    }
}]);


app.controller('selectController',['$scope',function($scope){
 $scope.selectController = function(){
$scope.users = [
        { id: 1, name: 'option1' },
        { id: 2, name: 'option1' },
        { id: 3, name: 'option1' }
    ];

 }
}]);

// app.controller("Titlectrl", function($scope, $rootScope){
//   $rootScope.titles = "Home";
// });


