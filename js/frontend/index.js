angular.element(document).ready(function () {

    var sync1 = $("#sync1");
    var sync2 = $("#sync2");
    var syncedSecondary = true;
    var pageWidth = $("html").width();
    if (pageWidth > 800) {
        var slidesPerPage = 3;
    } else if (pageWidth > 767) {
        var slidesPerPage = 2;
    } else if (pageWidth > 480) {
        var slidesPerPage = 1;
    } else {
        var slidesPerPage = 1;
    }

    sync1.owlCarousel({
        items: 1,
        slideSpeed: 3000,
        items: slidesPerPage,
        nav: true,
        autoplay: true,
        dots: false,
        loop: true,
        responsiveRefreshRate: 200,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    }).on('changed.owl.carousel', syncPosition);

    sync2.owlCarousel({
        items: 1,
        slideSpeed: 3000,
        items: slidesPerPage,
        nav: true,
        autoplay: false,
        dots: false,
        loop: true,
        responsiveRefreshRate: 200,
        navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
    }).on('changed.owl.carousel', syncPosition);

    function syncPosition(el) {
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - (el.item.count / 2) - .5);
        if (current < 0) {
            current = count;
        }
        if (current > count)  {
            current = 0;
        }
    }


});
