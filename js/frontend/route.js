app.config(function($stateProvider,$compileProvider, $urlRouterProvider) {
        $stateProvider
         .state('home', {
            url: '/home',
            templateUrl: base_url+"Home/home",
            data : { pageTitle: 'Agency List' },
            /*controller : function($scope, $rootScope){
              $rootScope.titles = "Agency List";
            }*/
          })

         .state('addStaff', {
            url: '/staff',
            templateUrl: base_url+"Home/staff",
            data : { pageTitle: 'Add Staff' }
          })

        .state('addnews', {
            url: '/news',
            templateUrl: base_url+"Home/news",
            data : { pageTitle: 'Add news' }
          })

         .state('staffprofile', {
            url: '/profile',
            templateUrl: base_url+"Home/staff_profile",
            data : { pageTitle: 'Staff Profile' }
        })

         .state('agencyresetpassword', {
            url: '/changepassword',
            templateUrl: base_url+"Home/change_user_password",
            data : { pageTitle: 'Reset password' }
        })
        
        .state('message', {
            url: '/verifyaccount',
            templateUrl: base_url+"Home/emailverify",
            data : { pageTitle: 'emailverify' }
          })

        .state('propertyDetails', {
            url: '/propertyDetails',
            templateUrl: base_url+"Home/properties_details",
            data : { pageTitle: 'properties_details' }
          })


 
        $urlRouterProvider.otherwise('/home');
}).run(function($rootScope, $state) {
      $rootScope.$state = $state;
    });
