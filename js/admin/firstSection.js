var pagenumber = 0;
var inprocess = null;

function listAllFirstSectionData(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/First_section/listAllFirstSectionData",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
    console.log(json);
      inprocess = null;
      if(json.status == "success"){
          $("#listAllFirstSectionData tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}

 $('#search').attr('disabled','disabled');
     $('#txt_search').keyup(function() {
        if($(this).val() != '') {
           $('#search').removeAttr('disabled');
        }
        else {
         $('#search').attr('disabled','disabled');
          listAllFirstSectionData(0,'user','DESC'); 
        }
     });
     
function deleteFirstSection($this){
  if($($this).attr("data-row-id")){    
   if (confirm("Do you want to delete")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/First_section/deleteFirstSection",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
           if(json.status == "success"){
            $("#page_key").val($($this).attr("data-row-id"));
            $($this).parents("tr:first").remove();            
            toastr.success(json.msg,"Success:");
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
    }  
  }
  else
  {
   return false;
  }
}

function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#theDiv')
              .attr('src', e.target.result)
              .width(150)
              .height(200);
      };
      reader.readAsDataURL(input.files[0]);
  }
}

function getFirstSection($this){
  $("#FSForm")[0].reset();  
  $("#FSForm span.error-block").css("display","none");
  $("#FSForm .form-control").removeClass("error-block");
  $('#theDiv').html("<img name='icon_image' id='icon_image' src="+BASEURL+"images/default.jpg"+" />");
  $("#FSForm div.has-error").removeClass("has-error");
  $("#FSForm div.has-warning").removeClass("has-warning");
  $("#FSForm div.has-success").removeClass("has-success");
  $("#FSForm span.glyphicon-remove").removeClass("glyphicon-remove");
  //$('#dateissue').removeClass('has-success'); 
  $('#FSForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#FSForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#FSForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
  $("oldimg").val();
var attr = $($this).attr("data-row-id");
  if(typeof attr !== typeof undefined && attr !== false) {
    $('#imgChange label').html('Change old Icon');  
  }
  else
  {
    $('#imgChange label').html('Add new Icon');
  }

  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/First_section/getFirstSection",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){            
            $("#page_key").val($($this).attr("data-row-id"));
            $("#title_name").val(json.pageData.fs_title);
            $("#description").val(json.pageData.fs_description);
            $("#oldimg").val(json.pageData.fs_icon_image);
           if(json.pageData.fs_icon_image!='')
            {
              $('#theDiv').html("<img name='icon_image' id='icon_image' src="+BASEURL+json.pageData.fs_icon_image+" width='200px' height='200px'/>");           
            }
            else
            {
              $('#theDiv').html("<img name='icon_image' id='icon_image' src="+BASEURL+'uploads/NEMT/thumb/img-dummy.jpg'+" width='200px' height='200px'/>");
            }
          
           jQuery('#FSModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#page_key").val(''); 
    jQuery('#FSModal').modal('show', {backdrop: 'static'});
  }
}


  $("input[name=icon_image]").change(function () {
    if(this.files && this.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          var img = $('<img>').attr('src', e.target.result);
          $('#theDiv').html(img); 
          $("#FSForm div.has-error").removeClass("has-error");
          $("#FSForm div.has-warning").removeClass("has-warning");
          $("#FSForm div.has-success").removeClass("has-success");
          $("#FSForm span.glyphicon-remove").removeClass("glyphicon-remove");
          //$('#dateissue').removeClass('has-success'); 
          $('#FSForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
          $('#FSForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
          $('#FSForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
          $('#FSForm p.at-error').remove();
          $('#FSForm p.has-warning').remove();
      };
      reader.readAsDataURL(this.files[0]);                    
    }
});

var column = 'id';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listAllFirstSectionData(0,column,order);

$("#listAllFirstSectionData th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listAllFirstSectionData th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listAllFirstSectionData th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listAllFirstSectionData(0,column,order);
  }
});

$(document).ready(function()
{ 
  $("#FSForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#FSForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
       $("#FSForm").find('[type="submit"]').prop("disabled", false); 
          toastr.success(json.msg,'Success:');
          jQuery('#FSModal').modal('hide');
          if(json.action == "add"){
            listAllFirstSectionData(0,'id','DESC'); 
          }else{
            listAllFirstSectionData(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
          $("#FSForm").find('[type="submit"]').prop("disabled", false); 
        }
        $("#FSForm").find('[type="submit"]').removeClass('sending').blur();
      },
      complete: function(json) 
      {
          $("#FSForm").find('[type="submit"]').removeClass('sending').blur();
      },
      error: function()
      {
          $("#FSForm").find('[type="submit"]').removeClass('sending').blur();
      }
  });
});
