var pagenumber = 0;
var inprocess = null;
function listPatients(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/patients/listPatients",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {
    
      inprocess = null;
      if(json.status == "success"){
        $("#listPatients tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
          $('#theDiv')
              .attr('src', e.target.result)
              .width(150)
              .height(200);
      };

      reader.readAsDataURL(input.files[0]);
  }
}




function viewDetails($this)
{
  $("#userForm")[0].reset();
  $('#insurance_provider').show(); 
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Patients/getPatient",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
console.log(json);
          if(json.status == "success"){ 
            $("#user_key").val($($this).attr("data-row-id"));
            $("#full_name").val(json.userData.full_name);
            $("#econtact_name").val(json.userData.emergency_contactname);
            $("#econtact_no").val(json.userData.emergency_contactno);             
            $("#address").val(json.userData.address);
            $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);
            $("#county").val(json.userData.county);  
            $("#example1").val(json.userData.date_of_birth);
            $("#oldimg").val(json.userData.picture); 
            $("#oldpdf").val(json.userData.personal_doc);
            $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);        
            $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.picture+" />")           
            jQuery('#userModal').modal('show', {backdrop: 'static'});
            $("#zipcode").val(json.userData.zipcode);
          // $('#country option[value="' + json.userData.country + '"]').prop('selected', true);
           //$('input[name="country"][value="'+ json.userData.country +'"]').prop('checked', true);

            jQuery('#userModal1').modal('show', {backdrop: 'static'});        
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal1').modal('show', {backdrop: 'static'});
  }
}



function getPatient($this){
$("#userForm")[0].reset();
$("#userForm div.has-error").removeClass("has-error");
$("#userForm div.has-warning").removeClass("has-warning");
$("#userForm div.has-success").removeClass("has-success");
$("#userForm span.glyphicon-remove").removeClass("glyphicon-remove");
//$('#dateissue').removeClass('has-success'); 
$('#userForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
$('#userForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
$('#userForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');

  $('#insurance_provider').show();
  $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+"images/default.jpg"+" />");
  $('#pdf').html("<span></span>");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Patients/getPatient",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
         console.log(json);
          if(json.status == "success"){ 
            $("#user_key").val($($this).attr("data-row-id"));
            $("#full_name").val(json.userData.full_name);
            $("#econtact_name").val(json.userData.emergency_contactname);
            $("#econtact_no").val(json.userData.emergency_contactno);             
            $("#street").val(json.userData.street);
            $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);  
	    $("#county").val(json.userData.county); 
            $("#example1").val(json.userData.date_of_birth);
            $("#oldimg").val(json.userData.picture);            
            $("#oldpdf").val(json.userData.personal_doc);
            $("#insurance_id").val(json.userData.insurance_id); 
            $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);  
            $("#signature").val(json.userData.signature);  
            $("#signature option:selected" ).text();
            $("#signature").val(json.userData.signature).attr('selected','selected');
            $("#country").val(json.userData.country).attr('selected','selected'); 
            $('#state').html(json.stateJSON); 
            $('#state').val(json.userData.state).attr('selected','selected'); 
            $('#city').html(json.cityJSON);
            $('#city').val(json.userData.city).attr('selected','selected');           
         
            $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.picture+" width='200px' height='200px'/>")           
            jQuery('#userModal').modal('show', {backdrop: 'static'});
            if(json.userData.personal_doc!='')
            {
              $('#pdf').html("<a  target='_blank' href="+BASEURL+json.userData.personal_doc+">View</a>")           
            }
            var chk1=""; 
            $("#zipcode").val(json.userData.zipcode);
            jQuery('#userModal').modal('show', {backdrop: 'static'});
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}



function changeStatus($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to change status?'))
        {
    $.ajax({
          type: "POST",
          dataType: "json",
         url: BASEURL+"admin/Patients/change_user_status",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){           
            location.reload();
            $("#user_key").val($($this).attr("data-row-id"));                
           $("#status").innerHTML=json.userData.status;           
            toastr.success(json.msg,"Success:");
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');   
  }
}
var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listPatients(0,column,order);

$("#listPatients th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listPatients th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listPatients th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listPatients(0,column,order);
  }
});
$("input[name=pimage]").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        var img = $('<img>').attr('src', e.target.result);
                        $('#theDiv').html(img);
                         $('#pimage1').html(img);
                    };
                    reader.readAsDataURL(this.files[0]);                    
                }
            });

$(document).ready(function()
{

  //  $("#full_name").keydown(function(event) {
  //   k = event.which;
  //   if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
  //     if ($(this).val().length == 100) {
  //       if (k == 8) {
  //         return true;
  //       } else {
  //         event.preventDefault();
  //         return false;
  //       }
  //     }
  //   } else {
  //     event.preventDefault();
  //     return false;
  //   }
  //   if (k === 32 && !this.value.length)
  //       event.preventDefault();
  // });
 

$("#userModal").on("hidden.bs.modal", function () 
{
  $('#userForm div').removeClass('form-group has-error has-feedback').addClass('form-group'); 
  $('#userForm span .glyphicon glyphicon-remove form-control-feedback').remove(); 
  $('#userForm p').remove(); 
  $('#userForm').find('input:text, input:password, select, textarea').val('');
  $('#userForm').find('input:radio, input:checkbox').prop('checked', false);


});
 $("#example1").on("change",function (){ 

  $('#dateissue').removeClass('form-group has-error has-feedback').addClass('form-group has-success has-feedback'); 
  //$('#dateissue span.glyphicon glyphicon-remove form-control-feedback').addClass('glyphicon glyphicon-ok form-control-feedback'); 

   $('#dateissue span').removeClass('glyphicon glyphicon-remove form-control-feedback').addClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#example1').removeClass('form-control hasDatepicker at-required').addClass('form-control at-success');
  $('#dateissue p').remove(); //form-control at-successform-control at-successform-control at-success

            });


$('#country').on('change',function(){
      var countryID = $(this).val();         
        if(countryID){
            $.ajax({
                type:'POST',
                url:BASEURL+"admin/Patients/get_state",
                data:{countryID:countryID},
                dataType:'json',
                success:function(json){                                     
                     $('#state').html(json.state);
                     $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });

   $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:BASEURL+'admin/Patients/get_city',
                data:{'state_id':stateID},
                dataType:'json',
                success:function(json){
                    $('#city').html(json.city);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
 
 
  $('#search').attr('disabled','disabled');
     $('#txt_search').keyup(function() {
        if($(this).val() != '') {
           $('#search').removeAttr('disabled');
        }
        else {
         $('#search').attr('disabled','disabled');
          listPatients(0,'user','DESC'); 
        }
     });  
  $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    { //location.reload();
      if(json.status == "success"){           
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listPatients(0,'user','DESC'); 
        }else{
          listPatients(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else{
        toastr.error(json.msg,"Error:");
      }
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
$("#userForm").find('[type="submit"]').removeAttr('disabled');
    },
    complete: function(json) 
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
$("#userForm").find('[type="submit"]').removeAttr('disabled');
    },
    error: function()
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
$("#userForm").find('[type="submit"]').removeAttr('disabled');
    }
  });
});
