var pagenumber = 0;
var inprocess = null;

function listDesiredTrans(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Desired_transportation/list_of_desired_trans_data",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
    console.log(json);
      inprocess = null;
      if(json.status == "success"){
          $("#listDesiredTrans tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}

 $('#search').attr('disabled','disabled');
     $('#txt_search').keyup(function() {
        if($(this).val() != '') {
           $('#search').removeAttr('disabled');
        }
        else {
         $('#search').attr('disabled','disabled');
          listDesiredTrans(0,'user','DESC'); 
        }
     });

function deleteDesiredTrans($this){
if($($this).attr("data-row-id")){   
   if (confirm("Do you want to delete")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Desired_transportation/deleteDesiredTrans",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
           if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
                 $($this).parents("tr:first").remove();
            //$("#status").val(json.userData.status);
            toastr.success(json.msg,"Success:");

          }else{
              toastr.error(json.msg,"Error:");
          }
      });
    }   
  }
  else
  {
   return false;

  }
}

function getDesiredTrans($this){
  $("#DTForm")[0].reset();  
  $("#DTForm span.error-block").css("display","none");
  $("#DTForm .form-control").removeClass("error-block");
  $("#DTForm div.has-error").removeClass("has-error");
  $("#DTForm div.has-warning").removeClass("has-warning");
  $("#DTForm div.has-success").removeClass("has-success");
  $("#DTForm p.at-error").remove();
  $("#DTForm span.glyphicon-remove").removeClass("glyphicon-remove");

  $('#DTForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#DTForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#DTForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
  if($($this).attr("data-row-id")){   
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Desired_transportation/getDesiredTrans",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            //alert(json.pageData.page_detail);
            $("#page_key").val($($this).attr("data-row-id"));
            $("#dt_name").val(json.pageData.dt_name);             
           jQuery('#DTModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#page_key").val('');
 
    jQuery('#DTModal').modal('show', {backdrop: 'static'});
  }
}

var column = 'fname';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listDesiredTrans(0,column,order);

$("#listDesiredTrans th").click(function(){

  if($(this).hasClass("sorting")){
    $("#listDesiredTrans th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
    orderby = order;
      $("#listDesiredTrans th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listDesiredTrans(0,column,order);
  }
});

$(document).ready(function()
{
   $("#dt_name").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
 
  $("#DTForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#DTForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
       $("#DTForm").find('[type="submit"]').prop("disabled", false); 
          toastr.success(json.msg,'Success:');
          jQuery('#DTModal').modal('hide');
          if(json.action == "add"){
            listDesiredTrans(0,'id','DESC'); 
          }else{
            listDesiredTrans(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
          $("#DTForm").find('[type="submit"]').prop("disabled", false); 
        }
        $("#DTForm").find('[type="submit"]').removeClass('sending').blur();


      },
      complete: function(json) 
      {
          $("#DTForm").find('[type="submit"]').removeClass('sending').blur();
      },
      error: function()
      {
          $("#DTForm").find('[type="submit"]').removeClass('sending').blur();
      }
  });
});
