var pagenumber = 0;
var inprocess = null;
function listProfile(page,column,order){
	pagenumber = page;
	if(inprocess){ 
		inprocess.abort();
	}
	inprocess = $.ajax({
			type: "POST",
			dataType: "json",
			url: BASEURL+"admin/profile/listProfile",
			data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
			
	}).success(function (json) {
		
			inprocess = null;
			if(json.status == "success"){
				$("#listProfile tbody").html(json.rows);
				$("#paginate_links").html(json.pagelinks);
				$("#paginate_entries").html(json.entries);
			}
	});
}

function getProfile($this){

	$("#userForm")[0].reset(); 
	$('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+"images/default.jpg"+" />"); 
	if($($this).attr("data-row-id")){
		$.ajax({
					type: "POST",
					dataType: "json",
					url: BASEURL+"admin/Profile/getProfile",
					data: {"key":$($this).attr("data-row-id")},
			}).success(function (json) {

					if(json.status == "success"){   
						$("#user_key").val($($this).attr("data-row-id"));
						$("#email").val(json.userData.email);           
						$("#first_name").val(json.userData.first_name);
						$("#last_name").val(json.userData.last_name);
						$("#phone").val(json.userData.phone);  
						$("#username").val(json.userData.username);  
						
					 $("#oldimg").val(json.userData.profile);           

				$('#theDiv').html("<img name='pimage' id='pimage' src="+BASEURL+json.userData.profile+" />")           
				 jQuery('#userModal').modal('show', {backdrop: 'static'});

					
					}
					else{
							toastr.error(json.msg,"Error:");
					}
			});
	}else{
	 // $("#user_key").val('');
		jQuery('#userModal').modal('show', {backdrop: 'static'});
	}
}


 
var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listProfile(0,column,order);

$("#listProfile th").click(function(){

	if($(this).hasClass("sorting")){

		$("#listProfile th").removeClass("sorting_asc").removeClass("sorting_desc");
			column = $(this).attr("data-name");
			order = $(this).attr("data-order");
			ordercolumn = column;
			orderby = order;
			$("#listProfile th").attr("data-order","ASC");
			if(order.toLowerCase() == 'asc'){
				 $(this).attr("data-order","DESC");
			}else{
					$(this).attr("data-order","ASC");
			}
			$(this).addClass("sorting_"+order.toLowerCase());
			listProfile(0,column,order);
	}
});




