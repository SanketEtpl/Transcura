var pagenumber = 0;
var inprocess = null;
function listOfMemberCompleteDetails(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Member_complete_ride/listOfMemberCompleteDetails",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {    
      inprocess = null;
      if(json.status == "success"){
        $("#listOfMemberCompleteDetails tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

function getDetails($this){
  $("#completeRideForm")[0].reset();    
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Member_complete_ride/getDetails",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
         console.log(json);
          if(json.status == "success"){ 
            //$("#user_key").val($($this).attr("data-row-id"));
            $("#full_name").text(json.userData.full_name);
            $("#sr_trip_id").text(json.userData.sr_trip_id);
            $("#state_name").text(json.userData.state_name);
            $("#country_name").text(json.userData.country_name);
            $("#sr_date").text(json.userData.sr_date);
            $("#sr_pick_up_time").text(json.userData.sr_pick_up_time);                  
            $("#sr_appointment_time").text(json.userData.sr_appointment_time);
            $("#source_address").text(json.userData.source_address);
            $("#destination_address").text(json.userData.destination_address);         
            jQuery('#completeModal').modal('show', {backdrop: 'static'});          
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#completeModal').modal('show', {backdrop: 'static'});
  }
}

function delete_complete_ride($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are you sure want to delete member schedule ride details ?'))
    {          
      $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Member_complete_ride/delete_complete_ride",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){                      
          $($this).parents("tr:first").remove();  
            toastr.success(json.msg,"Success:");
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}  

var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listOfMemberCompleteDetails(0,column,order);

$("#listOfMemberCompleteDetails th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listOfMemberCompleteDetails th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listOfMemberCompleteDetails th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listOfMemberCompleteDetails(0,column,order);
  }
});

$(document).ready(function()
{
  $('#search').attr('disabled','disabled');
   $('#txt_search').keyup(function() {
      if($(this).val() != '') {
         $('#search').removeAttr('disabled');
      }
      else {
       $('#search').attr('disabled','disabled');
        listOfMemberCompleteDetails(0,'user','DESC'); 
      }
   });    
});
