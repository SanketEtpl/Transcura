var pagenumber = 0;
var inprocess = null;
function listDrivers(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }

  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Drivers/listDrivers",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {
    
      inprocess = null;
      if(json.status == "success"){
        $("#listDrivers tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

function getDriver($this){
  $("#userForm")[0].reset(); 
  $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+"images/default.jpg"+" />");
  $('#pdf').html("<span></span>");
  $('#pdf2').html("<span></span>");  
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Drivers/getDriver",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success")
      {   
        console.log(json.userData);
        $("#user_key").val($($this).attr("data-row-id"));
        $("#full_name").val(json.userData.full_name);
        $("#address").val(json.userData.address);
        $("#email").val(json.userData.email);
        $("#phone").val(json.userData.phone);  
        $("#caddress").val(json.userData.company_address);  
        $("#company_name").val(json.userData.company_name); 
        $("#company_contactno").val(json.userData.company_contactno);
        $("#caddress").val(json.userData.company_address);        
        $("#example1").val(json.userData.date_of_birth); 
        $("#oldimg").val(json.userData.picture); 
        $("#street").val(json.userData.street);
        $("#personal_docold").val(json.userData.personal_doc); 
        $("#vehicle_docold").val(json.userData.vehicle_doc);
        $("#country").val(json.userData.country).attr('selected','selected'); 
        $('#state').html(json.stateJSON); 
        $('#state').val(json.userData.state).attr('selected','selected'); 
        $("#city").html(json.cityJSON);
        $("#city").val(json.userData.city).attr('selected','selected'); 
        $("#zipcode").val(json.userData.zipcode);
        $("#county").val(json.userData.county);
        $("#company_vehicle").val(json.userData.operate_comy_vehicle).attr('selected','selected');
        $("#own_vehicle").val(json.userData.operate_own_vehicle_ownbusiness).attr('selected','selected');
        $("#vehicle_for_company").val(json.userData.operate_own_vehicle_comy).attr('selected','selected');
        $("#paratransit").val(json.userData.paratrasmit).attr('selected','selected');

        //loc = json.cData;    
         
        var chk1=""; 
         $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.picture+" width='200px' height='200px'/>")           
        jQuery('#userModal').modal('show', {backdrop: 'static'});
        //$('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.picture+" />")           
        if(json.userData.personal_doc!='')
        {
          $('#pdf').html("<a  target='_blank' href="+BASEURL+json.userData.personal_doc+">View</a>");           
        }
        if(json.userData.vehicle_doc!='')
        {
          $('#pdf2').html("<a  target='_blank' href="+BASEURL+json.userData.vehicle_doc+">View</a>");           
        }        
        jQuery('#userModal').modal('show', {backdrop: 'static'});
      }
      else
      {
        toastr.error(json.msg,"Error:");
      }
  });
}else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}
  function changeStatus($this)
  {
  if($($this).attr("data-row-id")){
    if(confirm('Are you sure you want to change the status?'))
      {
        $.ajax({
        type: "POST",
        dataType: "json",
        url: BASEURL+"admin/Drivers/change_status",
        data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      if(json.status == "success"){
        location.reload();
        $("#user_key").val($($this).attr("data-row-id"));      
        $("#status").innerHTML=json.userData.status;
        $("#status").val(json.userData.status);
        toastr.success(json.msg,"Success:");
        location.reload();
        }
        else
        {
          toastr.error(json.msg,"Error:");
        }
      });
    }
  }else{
    $("#user_key").val('');   
  }
}
var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listDrivers(0,column,order);

$("#listDrivers th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listDrivers th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listDrivers th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listDrivers(0,column,order);
    }
  });
  $("input[name=pimage]").change(function () 
  {
    if (this.files && this.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var img = $('<img>').attr('src', e.target.result);
            $('#theDiv').html(img);
             $('#pimage1').html(img);
        };
        reader.readAsDataURL(this.files[0]);                    
    }
});
  $('#country').on('change',function(){
    var countryID = $(this).val();         
      if(countryID){
        $.ajax({
                type:'POST',
                url:BASEURL+"admin/Patients/get_state",
                data:{countryID:countryID},
                dataType:'json',
                success:function(json){                                     
                     $('#state').html(json.state);
                    $('#city').html('<option value="">Select state first</option>'); 
                }
            }); 
        }else{
            $('#state').html('<option value="">Select country first</option>');
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });
   $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:BASEURL+'admin/Patients/get_city',
                data:{'state_id':stateID},
                dataType:'json',
                success:function(json){
                    $('#city').html(json.city);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });

   function keypresstext(id){
     $("."+id).text("");
   }
$(document).ready(function()
{
  var emailPattern = /^[_\.0-9a-zA-Z-]+@([0-9a-zA-Z][0-9a-zA-Z-]+\.)+[a-zA-Z]{2,50}$/i;
  $("#first_form").show();
  $("#second_form").hide();
  $("#compVehicle").hide();
  $("#ownVehicle").hide();
  $("#ownVehicleForComp").hide();
  $("#next").click(function(){ 
    var full_name = $("#full_name").val();
    var dob = $("#example1").val();
    var email = $("#email").val();
    var phone = $("#phone").val();
    var address = $("#address").val();
    var country = $("#country").val();
    var state = $("#state").val();
    var city = $("#city").val();
    var street = $("#street").val();
    var zipcode = $("#zipcode").val();
    var county = $("#county").val();

    var fullNameFlag = false;
    var dobFlag = false;
    var emailFlag = false; 
    var phoneFlag = false;
    var addressFlag = false;
    var countryFlag = false; 
    var stateFlag = false;
    var cityFlag = false;
    var streetFlag = false;
    var zipcodeFlag = false;
    var countyFlag = false;   
    
    if(full_name == ''){
      $(".full_name").remove(); 
      $("#full_name").parent().append("<div class='full_name' style='color:red;'>The full name field is required.</div>");
      fullNameFlag =false;
    }else{
      $(".full_name").remove();
      fullNameFlag =true;          
    }

    if(dob == ''){
      $(".example1").remove(); 
      $("#example1").parent().append("<div class='example1' style='color:red;'>The date of birth field is required.</div>");
      dobFlag =false;
    }else{
      $(".example1").remove();
      dobFlag =true;          
    }

    if(email == ''){
      $(".email").remove(); 
      $("#email").parent().append("<div class='email' style='color:red;'>The email field is required.</div>");
      emailFlag =false;
    }else{
      if( !emailPattern.test(email)){
        emailFlag = false;        
        $(".email").remove(); 
        $("#email").parent().append("<div class='email' style='color:red;'>The email invalid is required.</div>");
      }else{        
        emailFlag = true;
        $(".email").remove(); 
      }
      /*
      $(".email").remove();
      emailFlag =true;*/          
    }
    if(phone == ''){
      $(".phone").remove(); 
      $("#phone").parent().append("<div class='phone' style='color:red;'>The phone field is required.</div>");
      phoneFlag =false;
    }else{
      $(".phone").remove();
      phoneFlag =true;          
    }
    if(address == ''){
      $(".address").remove(); 
      $("#address").parent().append("<div class='address' style='color:red;'>The address field is required.</div>");
      addressFlag =false;
    }else{
      $(".address").remove();
      addressFlag =true;  
    }
    if(country == ''){
      $(".country").remove(); 
      $("#country").parent().append("<div class='country' style='color:red;'>The country field is required.</div>");
      countryFlag =false;
    }else{
      $(".country").remove();
      countryFlag =true;          
    }
    if(state == ''){
      $(".state").remove(); 
      $("#state").parent().append("<div class='state' style='color:red;'>The state field is required.</div>");
      stateFlag =false;
    }else{
      $(".state").remove();
      stateFlag =true;          
    }
    if(city == ''){
      $(".city").remove(); 
      $("#city").parent().append("<div class='city' style='color:red;'>The city field is required.</div>");
      cityFlag =false;
    }else{
      $(".city").remove();
      cityFlag =true;          
    }
    if(street == ''){
      $(".street").remove(); 
      $("#street").parent().append("<div class='street' style='color:red;'>The street field is required.</div>");
      streetFlag =false;
    }else{
      $(".street").remove();
      streetFlag =true;          
    }
    if(zipcode == ''){
      $(".zipcode").remove(); 
      $("#zipcode").parent().append("<div class='zipcode' style='color:red;'>The zipcode is required.</div>");
      zipcodeFlag =false;
    }else{
     if(zipcode.length < 5 )
        {
          $(".zipcode").remove(); 
          $("#zipcode").parent().append("<div class='zipcode' style='color:red;'>Zipcode field must contain minimum 5 digits .</div>");
          zipcodeFlag =false;
        }
        else{         
      $(".zipcode").remove();
      zipcodeFlag =true;           
        }        
    }
    if(county == ''){
      $(".county").remove(); 
      $("#county").parent().append("<div class='county' style='color:red;'>The county field is required.</div>");
      countyFlag =false;
    }else{
      $(".county").remove();
      countyFlag =true;          
    } 

    if(fullNameFlag && streetFlag && zipcodeFlag && countyFlag && dobFlag && emailFlag && phoneFlag && addressFlag && countryFlag && stateFlag && cityFlag)
    {   
      $('#second_form').show();
      $('#first_form').hide();
    }
  });

  $("#back").click(function(){  
      $("#first_form").show();
      $("#second_form").hide();          
  });
  $("#btn_submit").click(function(){
     var company_name = $("#company_name").val();
    var caddress = $("#caddress").val();
    var pimage = $("#pimage").val();
    var company_contactno = $("#company_contactno").val();
    var company_vehicle = $("#company_vehicle").val();    
    var own_vehicle = $("#own_vehicle").val();
    var vehicle_for_company = $("#vehicle_for_company").val();
    var paratransit = $("#paratransit").val();
    var personal_doc = $("#personal_doc").val();
    var comp_vehicle_doc = $("#comp_vehicle_doc").val();
    var own_doc = $("#own_doc").val();
    var vehicle_for_company_doc = $("#vehicle_for_company_doc").val();
    var companyNameFlag =false;
    var caddressFlag = false;
    var pimageFlag =false;
    var companyContactNoFlag = false;
    var companyVehicleFlag = false;
    var ownVehicleFlag = false;
    var vehicleForCompanyFlag = false;
    var paratransitFlag = false;
    var compVehicleDocFlag = false;
    var ownDocFlag = false;
    var personalDocFlag = false;
    var vehicleForCompanyDocFlag = false;
    if(company_name == ''){
      $(".company_name").remove(); 
      $("#company_name").parent().append("<div class='company_name' style='color:red;'>The company name field is required.</div>");
      companyNameFlag =false;
    }else{
      $(".company_name").remove();
      companyNameFlag =true;          
    }
    if(caddress == ''){
      $(".caddress").remove(); 
      $("#caddress").parent().append("<div class='caddress' style='color:red;'>The company address field is required.</div>");
      caddressFlag =false;
    }else{
      $(".caddress").remove();
      caddressFlag =true;          
    }
    if(pimage == ''){
      $(".pimage").remove(); 
      $("#pimage").parent().append("<div class='pimage' style='color:red;'>The profile image field is required.</div>");
      pimageFlag =false;
    }else{
      $(".pimage").remove();
      pimageFlag =true;          
    }
    if(company_contactno == ''){
      $(".company_contactno").remove(); 
      $("#company_contactno").parent().append("<div class='company_contactno' style='color:red;'>The company contact no field is required.</div>");
      companyContactNoFlag =false;
    }else{ 
      $(".company_contactno").remove();
      companyContactNoFlag =true;          
    }
    if(company_vehicle == ''){
      $(".company_vehicle").remove(); 
      $("#company_vehicle").parent().append("<div class='company_vehicle' style='color:red;'>The company vehicle field is required.</div>");
      companyVehicleFlag =false;
    }else{
      $(".company_vehicle").remove();
      companyVehicleFlag =true;          
    }
    if(own_vehicle == ''){
      $(".own_vehicle").remove(); 
      $("#own_vehicle").parent().append("<div class='own_vehicle' style='color:red;'>The own vehicle field is required.</div>");
      ownVehicleFlag =false;
    }else{
      $(".own_vehicle").remove();
      ownVehicleFlag =true;          
    }
    if(vehicle_for_company == ''){
      $(".vehicle_for_company").remove(); 
      $("#vehicle_for_company").parent().append("<div class='vehicle_for_company' style='color:red;'>The vehicle for company field is required.</div>");
      vehicleForCompanyFlag =false;
    }else{
      $(".vehicle_for_company").remove();
      vehicleForCompanyFlag =true;          
    }
    if(paratransit == ''){
      $(".paratransit").remove(); 
      $("#paratransit").parent().append("<div class='paratransit' style='color:red;'>The paratransit field is required.</div>");
      paratransitFlag =false;
    }else{
      $(".paratransit").remove();
      paratransitFlag =true;          
    }

    /*if(personal_doc == ''){
      $(".personal_doc").remove(); 
      $("#personal_doc").parent().append("<div class='personal_doc' style='color:red;'>The personal document field is required.</div>");
      personalDocFlag =false;
    }else{
      $(".personal_doc").remove();
      personalDocFlag =true;          
    }
    if($("#company_vehicle").val() == 'Yes')
    {
      if(comp_vehicle_doc == ''){
        $(".comp_vehicle_doc").remove(); 
        $("#comp_vehicle_doc").parent().append("<div class='comp_vehicle_doc' style='color:red;'>The company vehicle doc field is required.</div>");
        compVehicleDocFlag =false;
      }else{
        $(".comp_vehicle_doc").remove();
        compVehicleDocFlag =true;          
      }
    }
    else
    {
      compVehicleDocFlag =true;
    }

    if($("#own_vehicle").val() == 'Yes')
    {
      if(own_doc == ''){
        $(".own_doc").remove(); 
        $("#own_doc").parent().append("<div class='own_doc' style='color:red;'>The own document field is required.</div>");
        ownDocFlag =false;
      }else{
        $(".own_doc").remove();
        ownDocFlag =true;          
      }
    }
    else
    {
      ownDocFlag =true;
    }

    if($("#vehicle_for_company").val() == 'Yes')
    {
      if(vehicle_for_company_doc == ''){
        $(".vehicle_for_company_doc").remove(); 
        $("#vehicle_for_company_doc").parent().append("<div class='vehicle_for_company_doc' style='color:red;'>The vehicle for company doc field is required.</div>");
        vehicleForCompanyDocFlag =false;
      }else{
        $(".vehicle_for_company_doc").remove();
        vehicleForCompanyDocFlag =true;          
      }
    }
    else
    {
      vehicleForCompanyDocFlag =true;
    }*/
    //pimageFlag && personalDocFlag && compVehicleDocFlag && ownDocFlag
    if(companyNameFlag && caddressFlag && companyContactNoFlag && companyVehicleFlag && ownVehicleFlag && vehicleForCompanyFlag && paratransitFlag)
    {    
      var formData = new FormData($('#userForm')[0]); 
      $.ajax({
            type:"POST",
            url: BASEURL+"admin/Drivers/saveDriver",
            dataType :"json",
            data:formData,
            processData: false,
            contentType: false,
            success:function(response){
              if(response.status == 'success')
              {                
                toastr.success(response.msg,"Success:");
                setTimeout(function(){ document.location.href = response.redirect; }, 3000);                               
              }
              else
              {
                toastr.error(response.msg,"Error:");             
              }              
            }
      });
    }
  });
  
  $("select#company_vehicle").change(function(){
    var data = $("select#company_vehicle").val();
    if(data == 'Yes')
    {
      $("#compVehicle").show();
    }
    else
    {
     $("#compVehicle").hide(); 
    }    
  });
  $("select#own_vehicle").change(function(){
    var data = $("select#own_vehicle").val();
    if(data == 'Yes')
    {
      $("#ownVehicle").show();
    }
    else
    {
      $("#ownVehicle").hide();
    }
  });
 $("select#vehicle_for_company").change(function(){
    var data = $("select#vehicle_for_company").val();
    if(data == 'Yes')
    {
      $("#ownVehicleForComp").show();
    }
    else
    {
      $("#ownVehicleForComp").hide();
    }
  });  
  $("#company_name").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
  $("#full_name").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

  $("#email").keydown(function(e) {   
    k = e.which;
    if ((k >= 65 && k <= 90 || k >= 96 && k <= 105) || k == 9 || k == 50 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          e.preventDefault();
          return false;
        }
      }
    } else {
      e.preventDefault();
      return false;
    }
    if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
      e.preventDefault();

      if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();
  });

  $("#phone").keydown(function (e) {
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13 ,107, 109, 57, 48,173,61]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }        
    });
   $("#company_contactno").keydown(function (e) {        
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
          (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
          (e.keyCode >= 35 && e.keyCode <= 40)) {                 
            return;
      }        
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
      }
    });
   $("#zipcode").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
   $("#county").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
   $("#address").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,55,37,39,188,109,173,111,32,191,48,57,190,51,53,54,56,189]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }        
    });

   $("#caddress").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,55,37,39,188,109,173,111,32,191,48,57,190,51,53,54,56,189]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }        
    });
   $("#street").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,55,37,39,188,109,173,111,32,191]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }        
    });
   $('#search').attr('disabled','disabled');
     $('#txt_search').keyup(function() {
        if($(this).val() != '') {
           $('#search').removeAttr('disabled');
        }
        else {
         $('#search').attr('disabled','disabled');
          listDrivers(0,'user','DESC'); 
        }
     });      
 $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
      $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    {
      if(json.status == "success"){            
      $("#userForm").find('[type="submit"]').prop("disabled", false);     
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listDrivers(0,'user','DESC'); 

        }else{
          listDrivers(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else{
        toastr.error(json.msg,"Error:");
      }
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    },
    complete: function(json) 
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    },
    error: function()
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    }
  }); 
});
