var pagenumber = 0;
var inprocess = null;


function listFaq(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/FAQ/listFaq",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
    console.log(json);
      inprocess = null;
      if(json.status == "success"){
          $("#listFaq tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}

 $('#search').attr('disabled','disabled');
     $('#txt_search').keyup(function() {
        if($(this).val() != '') {
           $('#search').removeAttr('disabled');
        }
        else {
         $('#search').attr('disabled','disabled');
          listFaq(0,'user','DESC'); 
        }
     });


function deleteFaq($this){

if($($this).attr("data-row-id")){
   // alert($($this).attr("data-row-id"));

   if (confirm("Do you want to delete")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/FAQ/deleteFaq",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
           if(json.status == "success"){
            location.reload();
            $("#user_key").val($($this).attr("data-row-id"));
                 $($this).parents("tr:first").remove();

            //$("#status").val(json.userData.status);
            toastr.success(json.msg,"Success:");

          }else{
              toastr.error(json.msg,"Error:");
          }
      });
  }
  
  }
  else
  {
   return false;

  }
}




function getFaq($this){
  $("#FaqForm")[0].reset();  
 // $("#FaqForm span.error-block").css("display","none");
 // $("#FaqForm .form-control").removeClass("error-block");

   $("#FaqForm div.has-error").removeClass("has-error");
$("#FaqForm span.glyphicon-remove").removeClass("glyphicon-remove");
$("#FaqForm div.has-error").removeClass("has-error");
  $("#FaqForm div.has-warning").removeClass("has-warning");
  $("#FaqForm div.has-success").removeClass("has-success");
  $("#FaqForm p.at-error").remove();
  $("#FaqForm span.glyphicon-remove").removeClass("glyphicon-remove");
$("has-success form-group").remove();
$("glyphicon glyphicon-ok form-control-feedback").remove();

  $('#FaqForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#FaqForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#FaqForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');



  if($($this).attr("data-row-id")){
   //alert($($this).attr("data-row-id"));
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/FAQ/getFaq",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            //alert(json.pageData.page_detail);
            $("#page_key").val($($this).attr("data-row-id"));
            $("#Question").val(json.pageData.question);
             $("#answer").val(json.pageData.answer);
                      
          
           jQuery('#faqModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#page_key").val('');
 
    jQuery('#faqModal').modal('show', {backdrop: 'static'});
  }
}

var column = 'fname';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listFaq(0,column,order);

$("#listFaq th").click(function(){

  if($(this).hasClass("sorting")){
    $("#listFaq th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
    orderby = order;
      $("#listFaq th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listFaq(0,column,order);
  }
});

$(document).ready(function()
{ 

$("#faqModal").on("hidden.bs.modal", function () 
{
  $('#FaqForm div').removeClass('form-group has-error has-feedback').addClass('form-group'); 
  $('#FaqForm span .glyphicon glyphicon-remove form-control-feedback').remove(); 
  $('#FaqForm p').remove(); 
  $('#FaqForm').find('input:text, input:password, select, textarea').val('');
  $('#FaqForm').find('input:radio, input:checkbox').prop('checked', false);


});



  $("#FaqForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#FaqForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
       $("#FaqForm").find('[type="submit"]').prop("disabled", false); 
          toastr.success(json.msg,'Success:');
          jQuery('#faqModal').modal('hide');
          if(json.action == "add"){
            listFaq(0,'id','DESC'); 
          }else{
            listFaq(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
          $("#FaqForm").find('[type="submit"]').prop("disabled", false); 
        }
        $("#FaqForm").find('[type="submit"]').removeClass('sending').blur();
      },
      complete: function(json) 
      {
          $("#FaqForm").find('[type="submit"]').removeClass('sending').blur();
      },
      error: function()
      {
          $("#FaqForm").find('[type="submit"]').removeClass('sending').blur();
      }
  });
});
