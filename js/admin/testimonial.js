var pagenumber = 0;
var inprocess = null;
function listOfTestimonialData(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }  
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Testimonial/listOfTestimonial",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {    
    console.log(json);
      inprocess = null;
      if(json.status == "success"){
          $("#listOfTestimonialData tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}

 $('#search').attr('disabled','disabled');
     $('#txt_search').keyup(function() {
        if($(this).val() != '') {
           $('#search').removeAttr('disabled');
        }
        else {
         $('#search').attr('disabled','disabled');
          listOfTestimonialData(0,'user','DESC'); 
        }
     });

function deleteTestimonial($this){
if($($this).attr("data-row-id")){   
   if(confirm("Do you want to delete")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Testimonial/deleteTestimonial",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
           if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
            $($this).parents("tr:first").remove();            
            toastr.success(json.msg,"Success:");
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
    }  
  }
  else
  {
   return false;
  }
}

function getTestimonial($this){  
  $('#testimg').html("");  
  $("#testimonialForm")[0].reset();  
  $("#testimonialForm span.error-block").css("display","none");
  $("#testimonialForm .form-control").removeClass("error-block");
  $("#testimonialForm div.has-error").removeClass("has-error");
  $("#testimonialForm div.has-warning").removeClass("has-warning");
  $("#testimonialForm div.has-success").removeClass("has-success");
  $("#testimonialForm p.at-error").remove();
  $("#testimonialForm span.glyphicon-remove").removeClass("glyphicon-remove");

  $('#testimonialForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#testimonialForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#testimonialForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Testimonial/getTestimonial",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){            
            $("#page_key").val($($this).attr("data-row-id"));
            $("#tm_name").val(json.pageData.tm_name); 
            $("#tm_description").val(json.pageData.tm_description);
           // $("#starsimg").html('<img src="'+json.pageData.tm_stars+'" height="50px" width="200px">');
if(json.pageData.tm_image != "")
           $('#testimg').html("<img name='tm_image1' id='tm_image1' src="+BASEURL+json.pageData.tm_image+" width='200px' height='200px'/>");
else
$('#testimg').html("<img name='tm_image1' id='tm_image1' src="+BASEURL+'uploads/NEMT/thumb/img-dummy.jpg'+" width='200px' height='200px'/>");
           
            jQuery('#testimonialModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#page_key").val(''); 
    jQuery('#testimonialModal').modal('show', {backdrop: 'static'});
  }
}
var column = 'fname';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listOfTestimonialData(0,column,order);

$("#listOfTestimonialData th").click(function(){

  if($(this).hasClass("sorting")){
    $("#listOfTestimonialData th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
    orderby = order;
      $("#listOfTestimonialData th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listOfTestimonialData(0,column,order);
  }
});

$("input[name=tm_image]").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        var img = $('<img>').attr('src', e.target.result);
                        $('#testimg').html(img);
                         $('#tm_image1').html(img);
                    };
                    reader.readAsDataURL(this.files[0]);                    
                }
            });


$(document).ready(function()
{ 
  $("#tm_name").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
  
  $("#testimonialForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#testimonialForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
       $("#testimonialForm").find('[type="submit"]').prop("disabled", false); 
          toastr.success(json.msg,'Success:');
          jQuery('#testimonialModal').modal('hide');
          if(json.action == "add"){
            listOfTestimonialData(0,'id','DESC'); 
          }else{
            listOfTestimonialData(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
          $("#testimonialForm").find('[type="submit"]').prop("disabled", false); 
        }
        $("#testimonialForm").find('[type="submit"]').removeClass('sending').blur();
      },
      complete: function(json) 
      {
          $("#testimonialForm").find('[type="submit"]').removeClass('sending').blur();
      },
      error: function()
      {
          $("#testimonialForm").find('[type="submit"]').removeClass('sending').blur();
      }
  });
});
