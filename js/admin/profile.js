var pagenumber = 0;
var inprocess = null;
function listProfile(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/profile/listProfile",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {
    
      inprocess = null;
      if(json.status == "success"){
        $("#listProfile tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

function getProfile($this){
  $("#userForm")[0].reset(); 
  $("#userForm div.has-error").removeClass("has-error");
  $("#userForm div.has-warning").removeClass("has-warning");
  $("#userForm div.has-success").removeClass("has-success");
  $("#userForm span.glyphicon-remove").removeClass("glyphicon-remove");
  $('#userForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#userForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#userForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Profile/getProfile",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {

          if(json.status == "success"){   
           console.log(json.userData);
            $("#user_key").val($($this).attr("data-row-id"));
            $("#email").val(json.userData.email);           
            $("#first_name").val(json.userData.first_name);
            $("#last_name").val(json.userData.last_name);
            $("#phone").val(json.userData.phone);  
            $("#username").val(json.userData.username);  
            $("#oldimg").val(json.userData.profile);                 
            $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.profile+" />")           
            jQuery('#userModal').modal('show', {backdrop: 'static'});          
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}


 
var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listProfile(0,column,order);

$("#listProfile th").click(function(){

  if($(this).hasClass("sorting")){

    $("#listProfile th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listProfile th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listProfile(0,column,order);
  }
});


$("input[name=pimage]").change(function () {
                if (this.files && this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        var img = $('<img>').attr('src', e.target.result);
                        $('#theDiv').html(img);
                         $('#pimage1').html(img);
                    };
                    reader.readAsDataURL(this.files[0]);                    
                }
            });






$(document).ready(function()
{


$("#userModal").on("hidden.bs.modal", function () 
{
  $('#userForm div').removeClass('form-group has-error has-feedback').addClass('form-group'); 
  $('#userForm span .glyphicon glyphicon-remove form-control-feedback').remove(); 
  $('#userForm p').remove(); 
  $('#userForm').find('input:text, input:password, select, textarea').val('');
  $('#userForm').find('input:radio, input:checkbox').prop('checked', false);

});
      
 $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    {
      if(json.status == "success"){    
     // alert("khjk");  
     $("#userForm").find('[type="submit"]').prop("disabled", false);     
        jQuery('#userModal').modal('hide');
        if(json.action == "update"){
          listProfile(0,'user','DESC'); 

        }else{
          listProfile(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");

      }else{
        toastr.error(json.msg,"Error:");
      $("#userForm").find('[type="submit"]').prop("disabled", false); 
      }


      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    },
    complete: function(json) 
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();


    },
    error: function()
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    }
  });



$("#userForm1").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        $("#userForm1").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    {
      if(json.status == "success"){    
     
    $("#userForm1").find('[type="submit"]').prop("disabled", false);     
        jQuery('#userModal').modal('hide');
        if(json.action == "update"){

          listProfile(0,'user','DESC'); 
        $("#userForm1").find('[type="submit"]').prop("disabled", false); 
        }else{
          listProfile(pagenumber,ordercolumn,orderby);  
          $("#userForm1").find('[type="submit"]').prop("disabled", false); 
        }
        toastr.success(json.msg,"Success:");
 location.href = BASEURL+"admin";

      }else{
        toastr.error(json.msg,"Error:");
     $("#userForm1").find('[type="submit"]').prop("disabled", false); 
      }


      $("#userForm1").find('[type="submit"]').removeClass('sending').blur();
    },
    complete: function(json) 
    {
        $("#userForm1").find('[type="submit"]').removeClass('sending').blur();
      

    },
    error: function()
    {
        $("#userForm1").find('[type="submit"]').removeClass('sending').blur();
    }
  });

});



