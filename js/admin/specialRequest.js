var pagenumber = 0;
var inprocess = null;

function listAllSpecialRequestData(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Special_request/list_of_special_request_data",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
    console.log(json);
      inprocess = null;
      if(json.status == "success"){
          $("#listAllSpecialRequestData tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}

 $('#search').attr('disabled','disabled');
     $('#txt_search').keyup(function() {
        if($(this).val() != '') {
           $('#search').removeAttr('disabled');
        }
        else {
         $('#search').attr('disabled','disabled');
          listAllSpecialRequestData(0,'user','DESC'); 
        }
     });

function deleteSpecialRequest($this){
  if($($this).attr("data-row-id")){    
   if (confirm("Do you want to delete")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Special_request/deleteSpecialRequest",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
           if(json.status == "success"){
            $("#page_key").val($($this).attr("data-row-id"));
            $($this).parents("tr:first").remove();            
            toastr.success(json.msg,"Success:");
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
  }
  
  }
  else
  {
   return false;

  }
}

function getSpecialRequest($this){
  $("#SRForm")[0].reset();  
  $("#SRForm span.error-block").css("display","none");
  $("#SRForm .form-control").removeClass("error-block");
  $("#SRForm div.has-error").removeClass("has-error");
  $("#SRForm div.has-warning").removeClass("has-warning");
  $("#SRForm div.has-success").removeClass("has-success");
  $("#SRForm p.at-error").remove();
  $("#SRForm span.glyphicon-remove").removeClass("glyphicon-remove");

  $('#SRForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#SRForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#SRForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Special_request/getSpecialRequest",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){            
            $("#page_key").val($($this).attr("data-row-id"));
            $("#sr_name").val(json.pageData.sr_name);             
           jQuery('#SRModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#page_key").val(''); 
    jQuery('#SRModal').modal('show', {backdrop: 'static'});
  }
}

var column = 'id';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listAllSpecialRequestData(0,column,order);

$("#listAllSpecialRequestData th").click(function(){
 // alert("tutyutyu");

  if($(this).hasClass("sorting")){
    $("#listAllSpecialRequestData th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listAllSpecialRequestData th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listAllSpecialRequestData(0,column,order);
  }
});

$(document).ready(function()
{ 
  $("#sr_name").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });
 
  $("#SRForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#SRForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
       $("#SRForm").find('[type="submit"]').prop("disabled", false); 
          toastr.success(json.msg,'Success:');
          jQuery('#SRModal').modal('hide');
          if(json.action == "add"){
            listAllSpecialRequestData(0,'id','DESC'); 
          }else{
            listAllSpecialRequestData(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
          $("#SRForm").find('[type="submit"]').prop("disabled", false); 
        }
        $("#SRForm").find('[type="submit"]').removeClass('sending').blur();
      },
      complete: function(json) 
      {
          $("#SRForm").find('[type="submit"]').removeClass('sending').blur();
      },
      error: function()
      {
          $("#SRForm").find('[type="submit"]').removeClass('sending').blur();
      }
  });
});
