var pagenumber = 0;
var inprocess = null;
function listSetting(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/setting/listSetting",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {
    
      inprocess = null;
      if(json.status == "success"){
        $("#listSetting tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

function getSetting($this){
  $("#userForm")[0].reset(); 
  $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+"images/default.jpg"+" />");
 
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/setting/getSetting",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {

          if(json.status == "success"){   
           console.log(json.userData);
            $("#user_key").val($($this).attr("data-row-id"));
            $("#office_email").val(json.userData.office_email);           
            $("#company_address").val(json.userData.company_address);
            $("#mobile").val(json.userData.mobile);
            $("#company_phone").val(json.userData.company_phone);  
            $("#facebook").val(json.userData.facebook);  
            $("#twitter").val(json.userData.twitter); 
            $("#google").val(json.userData.google);
           $("#pintrest").val(json.userData.pintrest);
           $("#oldimg").val(json.userData.logo); 
           $("#multipleLocation").val(json.userData.multiple_location);          

        $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.logo+" />")           
         jQuery('#userModal').modal('show', {backdrop: 'static'});

          
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}


 
var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listSetting(0,column,order);

$("#listSetting th").click(function(){

  if($(this).hasClass("sorting")){

    $("#listSetting th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listSetting th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listSetting(0,column,order);
  }
});






$(document).ready(function()
{



      
 $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    {
      if(json.status == "success"){    
     // alert("khjk");  
     $("#userForm").find('[type="submit"]').prop("disabled", false);     
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listSetting(0,'user','DESC'); 

        }else{
          listSetting(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");

      }else{
        toastr.error(json.msg,"Error:");
      $("#userForm").find('[type="submit"]').prop("disabled", false); 
      }


      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    },
    complete: function(json) 
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();


    },
    error: function()
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    }
  });
});
