/* Author : Ram K 
   Page : nemt_allocated_ride.js 
   Description:  NEMT allocated ride
   Date: 09 Feb 2018
*/

var pagenumber = 0;
var inprocess = null;
function listOfAllocatedRideDetails(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Nemt_allocated_ride/listOfAllocatedRideDetails",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {    
      inprocess = null;
      if(json.status == "success"){
        $("#listOfAllocatedRideDetails tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

function getDetails($this){
  $("#allocatedrideForm")[0].reset();    
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Nemt_allocated_ride/getAllocatedDetails",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
         console.log(json);
          if(json.status == "success"){ 
            $("#user_key").val($($this).attr("data-row-id"));
            $("#full_name").val(json.userData.full_name);
            $("#free_ride").val(json.userData.free_ride);               
            jQuery('#allocatedModal').modal('show', {backdrop: 'static'});          
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#allocatedModal').modal('show', {backdrop: 'static'});
  }
}

$(function() { //shorthand document.ready function
    $('#btn_submit').on('click', function(e) { //use on if jQuery 1.7+
         e.preventDefault();  //prevent form from submitting
        var data = $('#allocatedrideForm').serialize()
        if($('#full_name').val() =='' || $('#free_ride').val() =='')
        {
          return false;
        }else{
              $.ajax({
              type: "POST",
              dataType: "json",
              url: BASEURL+"admin/Nemt_allocated_ride/saveAllocatedRide",
              data: {"formData":data},
          }).success(function (json) {
            //console.log(json);
              if(json.status == "success"){                            
                jQuery('#allocatedModal').modal('hide');
                toastr.success(json.msg,"success:"); 
              }
              else{
                  toastr.error(json.msg,"Error:");
              }
          });
        }
         
    });
});


// function saveDetails($this){
//  alert($("#allocatedrideForm")[0].val());    
//   if($($this).attr("data-row-id")){
//     $.ajax({
//           type: "POST",
//           dataType: "json",
//           url: BASEURL+"admin/Nemt_allocated_ride/saveAllocatedRide",
//           data: {"key":$($this).attr("data-row-id")},
//       }).success(function (json) {
//          console.log(json);
//           if(json.status == "success"){ 
//             $("#user_key").val($($this).attr("data-row-id"));
//             $("#full_name").val(json.userData.full_name);
//             $("#free_ride").val(json.userData.free_ride);               
//             jQuery('#allocatedModal').modal('show', {backdrop: 'static'});          
//           }
//           else{
//               toastr.error(json.msg,"Error:");
//           }
//       });
//   }else{
//     $("#user_key").val('');
//     jQuery('#allocatedModal').modal('show', {backdrop: 'static'});
//   }
// }

var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listOfAllocatedRideDetails(0,column,order);

$("#listOfAllocatedRideDetails th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listOfAllocatedRideDetails th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listOfAllocatedRideDetails th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listOfAllocatedRideDetails(0,column,order);
  }
});

$(document).ready(function()
{
  $('#search').attr('disabled','disabled');
   $('#txt_search').keyup(function() {
      if($(this).val() != '') {
         $('#search').removeAttr('disabled');
      }
      else {
       $('#search').attr('disabled','disabled');
        listOfAllocatedRideDetails(0,'user','DESC'); 
      }
   });    
});

