var pagenumber = 0;
var inprocess = null;
function listDrivers(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
    type: "POST",
    dataType: "json",
    url: BASEURL+"admin/Drivers/listDrivers",
    data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},

  }).success(function (json) {

    inprocess = null;
    if(json.status == "success"){
      $("#listDrivers tbody").html(json.rows);
      $("#paginate_links").html(json.pagelinks);
      $("#paginate_entries").html(json.entries);
    }
  });
}

function deleteDriver($this){
  if($($this).attr("data-row-id")){   
   if(confirm("Do you want to delete")){
    $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Drivers/deleteDriver",
      data: {"key":$($this).attr("data-row-id")},
    }).success(function (json) {
     if(json.status == "success"){
      $("#user_key").val($($this).attr("data-row-id"));
      $($this).parents("tr:first").remove();            
      toastr.success(json.msg,"Success:");
    }else{
      toastr.error(json.msg,"Error:");
    }
  });
  }  
}
else
{
 return false;
}
}

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#theDiv')
      .attr('src', e.target.result)
      .width(150)
      .height(200);
    };
    reader.readAsDataURL(input.files[0]);
  }
}

function viewDetails($this)
{
  $("#userForm")[0].reset();
  $('#insurance_provider').show();   
  if($($this).attr("data-row-id")){
    $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Drivers/getDriver",
      data: {"key":$($this).attr("data-row-id")},
    }).success(function (json) {

      if(json.status == "success"){ 
        $("#user_key").val($($this).attr("data-row-id"));
        $("#full_name").val(json.userData.full_name);
        $("#username").val(json.userData.username);
        $("#pass").val(json.userData.password);
        $("#econtact_name").val(json.userData.emergency_contactname);
        $("#econtact_no").val(json.userData.emergency_contactno);             
        $("#address").val(json.userData.address);
        $("#email").val(json.userData.email);
        $("#phone").val(json.userData.phone);
        $("#county").val(json.userData.county);  
        $("#driverDate").val(json.userData.date_of_birth);
        $("#oldimg").val(json.userData.picture); 
          //$("#oldpdf").val(json.userData.personal_doc);
          //$("#email").val(json.userData.email);
          $("#phone").val(json.userData.phone);        
          $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.picture+" />");           
          jQuery('#userModal').modal('show', {backdrop: 'static'});
          $("#zipcode").val(json.userData.zipcode);
          jQuery('#userModal1').modal('show', {backdrop: 'static'});        
        }
        else{
          toastr.error(json.msg,"Error:");
        }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal1').modal('show', {backdrop: 'static'});
  }
}

function getDriver($this){  
  $("#userForm")[0].reset();
  $("#userForm div.has-error").removeClass("has-error");
  $("#userForm div.has-warning").removeClass("has-warning");
  $("#userForm div.has-success").removeClass("has-success");
  $("#userForm span.glyphicon-remove").removeClass("glyphicon-remove");
  //$('#dateissue').removeClass('has-success'); 
  $('#userForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#userForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#userForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
  $('#userForm').find('div.username1').remove();
  $('#userForm').find('div.email1').remove();
  $("#userForm").find('[type="submit"]').removeAttr('disabled');
  $('.username1').remove();
  $('.email1').remove();
  $('#persoal_doc_view').hide();
  $('#vehicle_reg_veiw').hide();
  $(".error_message").remove();
  $("#first_form").show();
  $("#second_form").hide();
  /*$('#dateissue p.at-error').remove();
  $('#dateissue div').removeClass("has-error");*/
  var attr = $($this).attr("data-row-id");
  if(typeof attr !== typeof undefined && attr !== false) {
    $('#imgChange span').html('Change profile picture');  
  }
  else
  {
    $('#imgChange span').html('Add new profile picture');
  }
  //alert($($this).attr("data-row-id"));
  //$('#imgChange span').html('Change old image');
  $('#insurance_provider').show();
  $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+"images/default.jpg"+" />");
  $('#pdf').html("<span></span>");
  if($($this).attr("data-row-id")){
    $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Drivers/getDriver",
      data: {"key":$($this).attr("data-row-id")},
    }).success(function (json) {
         //console.log(json);
         if(json.status == "success"){ 
            //alert($($this).attr("data-row-id"));
            $("#user_key").val($($this).attr("data-row-id"));
            $("#full_name").val(json.userData.full_name);
            $("#driverDate").val(json.userData.date_of_birth);
            $("#username").val(json.userData.username);
            $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);
            $("#address").val(json.userData.street);
            $("#county").val(json.userData.county);
            $("#emer_contc_name").val(json.userData.emergency_contactname);
            $("#econtact_no").val(json.userData.emergency_contactno);             
            $("#zipcode").val(json.userData.zipcode);             
            $("#insurance_id").val(json.userData.insurance_id); 
            if(json.userData.personal_doc_status == 1)
              $("#persoal_doc_view").show();
            $("#pers_doc").val(json.userData.personal_doc_status);
            if(json.userData.vehicle_doc_status == 1)
              $("#vehicle_reg_veiw").show();
            $("#vehRegDropdown").val(json.userData.vehicle_doc_status);
            if(json.userData.own_business_status == 1)
              $("#own_bussiness_show").show();
            $("#own_vehicle_dd").val(json.userData.own_business_status);
            if(json.userData.company_vehicle_status == 1)
              $("#company_vehicle_show").show();            
            $("#company_vehicle_dropdown").val(json.userData.company_vehicle_status);

            if(json.userData.driver_license_expire_dt != '')
              { $("#dldedShow").val(json.userData.driver_license_expire_dt); $('#DrvLiceED').show(); }
            if(json.userData.drug_test_results_expire_dt != '')
              { $('#pdtredShow').val(json.userData.drug_test_results_expire_dt);  $("#panDrugTtED").show(); }
            if(json.userData.criminal_back_ground_expire_dt != '')
              { $('#cbedShow').val(json.userData.criminal_back_ground_expire_dt);  $("#crimBGED").show(); }
            if(json.userData.motor_vehicle_record_expire_dt != '')
              { $('#mvredShow').val(json.userData.motor_vehicle_record_expire_dt);  $("#motVehRecED").show(); }
            //var ced ='act_33_and_34_clearance_expire_dt';
            if(json.userData.act_33_and_34_clearance_expire_dt != '')
              { $('#acedShow').val(json.userData.act_33_and_34_clearance_expire_dt);  $("#cleaED").show(); }
            if(json.userData.vehicle_inspections_expire_dt != '')
              { $('#viedShow').val(json.userData.vehicle_inspections_expire_dt);  $("#vehiInspED").show(); }
            if(json.userData.vehicle_maintnc_record_expire_dt != '')
              { $('#vmredShow').val(json.userData.vehicle_insurance_expire_dt);  $("#vehMaintRecED").show(); }
            if(json.userData.motor_vehicle_record_expire_dt != '')
              { $('#viedShow2').val(json.userData.vehicle_insurance_expire_dt);  $("#vehInsurnED").show(); }
            if(json.userData.vehicle_regi_expire_dt != '')
              { $('#vredShow').val(json.userData.vehicle_regi_expire_dt);  $("#vehRegED").show(); }

            $("#vehRegDropdown").val(json.userData.vehicle_doc_status);
            
            
            $("#oldimg").val(json.userData.picture);            
            $("#oldpdf").val(json.userData.personal_doc);
            $("#signature").val(json.userData.signature);             
            $("#own_buss_vehicle_details").val(json.userData.own_bus_vehicle_detiail);
            
            $("#transport_comp").val(json.userData.comp_veh_transp_comp);
            $("#vehicle_details").val(json.userData.comp_veh_vehicle_details);
            $("#com_veh_type_of_sevices").val(json.userData.comp_veh_services);

            $("#company_vehicle_dropdown").val(json.userData.company_vehicle_status);
            $("#company_vehicle_dropdown option:selected").text();
            $("#company_vehicle_dropdown").val(json.userData.company_vehicle_status).attr('selected','selected');
            
            $("#own_vehicle_dd").val(json.userData.own_business_status);
            $("#own_vehicle_dd option:selected").text();
            $("#own_vehicle_dd").val(json.userData.own_business_status).attr('selected','selected');
            $("#type_of_sevices").val(json.userData.own_bus_services);
            $("#type_of_sevices option:selected").text();
            $("#type_of_sevices").val(json.userData.own_bus_services).attr('selected','selected');
            
            
            $("#signature option:selected" ).text();
            $("#signature").val(json.userData.signature).attr('selected','selected');
            $("#country").val(json.userData.country).attr('selected','selected'); 
            $('#state').html(json.stateJSON); 
            $('#state').val(json.userData.state).attr('selected','selected'); 
            $('#city').html(json.cityJSON);
            $('#city').val(json.userData.city).attr('selected','selected');   


            if(json.userData.picture!='')
            {
              $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.picture+" width='200px' height='200px'/>");           
            }
            else
            {
              $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+'uploads/NEMT/thumb/img-dummy.jpg'+" width='200px' height='200px'/>");
            }
            jQuery('#userModal').modal('show', {backdrop: 'static'});
            if(json.userData.personal_doc!='')
            {
              $('#pdf').html("<a  target='_blank' href="+BASEURL+json.userData.personal_doc+">View</a>")           
            }
            var chk1=""; 
            
            jQuery('#userModal').modal('show', {backdrop: 'static'});
          }
          else{
            toastr.error(json.msg,"Error:");
          }
        });
}else{
  $("#user_key").val('');
  jQuery('#userModal').modal('show', {backdrop: 'static'});
}
}


// ----- show driver documents ---------
function getDriverDoc(dr_id)
{
  if($(dr_id).attr("data-row-id"))
  {
    $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Drivers/getDriverDoc",
      data: {"driver_id":$(dr_id).attr("data-row-id")},
    }).success(function (json) {
     if(json.status == "success")
     { 
      $(".driver_doc").html(json.response);        
      jQuery('#driverDocModal').modal('show', {backdrop: 'static'});
    }
    else
    {
      toastr.error(json.msg,"Error:");
    }
  });    
  }else
  {
    jQuery('#driverDocModal').modal('show', {backdrop: 'static'});
  }
}

//--------- change approval status----------
function changeApproval($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are you sure you want to change appoval status?'))
    {
      $.ajax({
        type: "POST",
        dataType: "json",
        url: BASEURL+"admin/Drivers/change_appoval",
        data: {"driver_id":$($this).attr("data-row-id")},
      }).success(function (json) {
        if(json.status == "success"){           
          // location.reload();
          listDrivers(0,'user','DESC')
          toastr.success(json.msg,"Success:");
        }else{
          toastr.error(json.msg,"Error:");
        }
      });
    } }else{
      $("#user_key").val('');   
    }
  }

function changeStatus($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are you sure you want to change the status?'))
    {
      $.ajax({
        type: "POST",
        dataType: "json",
        url: BASEURL+"admin/Drivers/change_status",
        data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        if(json.status == "success"){           
          location.reload();
          $("#user_key").val($($this).attr("data-row-id"));                
          $("#status").innerHTML=json.userData.status;           
          toastr.success(json.msg,"Success:");
        }else{
          toastr.error(json.msg,"Error:");
        }
      });
    } }else{
      $("#user_key").val('');   
    }
  }
  var column = 'user';
  var order = 'DESC';
  var ordercolumn = column;
  var orderby = order;
  listDrivers(0,column,order);

  $("#listDrivers th").click(function(){
    if($(this).hasClass("sorting")){
      $("#listDrivers th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listDrivers th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
       $(this).attr("data-order","DESC");
     }else{
      $(this).attr("data-order","ASC");
    }
    $(this).addClass("sorting_"+order.toLowerCase());
    listDrivers(0,column,order);
  }
});
  $("input[name=pimage]").change(function () {
    if (this.files && this.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        var img = $('<img>').attr('src', e.target.result);
        $('#theDiv').html(img);
        $('#pimage1').html(img);
      };
      reader.readAsDataURL(this.files[0]);                    
    }
  });

  $(document).ready(function()
  { 
    $("#userModal").on("hidden.bs.modal", function (){
      $('#userForm div').removeClass('form-group has-error has-feedback').addClass('form-group'); 
      $('#userForm span .glyphicon glyphicon-remove form-control-feedback').remove(); 
      $('#userForm p').remove(); 
      $('#userForm').find('input:text, input:password, select, textarea').val('');
      $('#userForm').find('input:radio, input:checkbox').prop('checked', false);
      $('#dateissue p.at-error').remove();
      $('#dateissue div').removeClass("has-error");
    });

    $("#driverDate").on("change",function (){ 
      $('#dateissue').removeClass('form-group has-error has-feedback').addClass('form-group has-success has-feedback'); 
      $('#dateissue span').removeClass('glyphicon glyphicon-remove form-control-feedback').addClass('glyphicon glyphicon-ok form-control-feedback'); 
      $('#driverDate').removeClass('form-control hasDatepicker at-required').addClass('form-control at-success');
      $('#dateissue p').remove(); //form-control at-successform-control at-successform-control at-success
      $('#dateissue p.at-error').remove();
      $('#dateissue div').removeClass("has-error");
    });

    $('#persoal_doc_view').hide();
    $('#vehicle_reg_veiw').hide();
    $("#DrvLiceED").hide();
    $("#panDrugTtED").hide();
    $("#crimBGED").hide();
    $("#motVehRecED").hide();
    $("#cleaED").hide();
    $("#vehiInspED").hide();
    $("#vehMaintRecED").hide();
    $("#vehInsurnED").hide();
    $("#vehRegED").hide();
    $("#first_form").show();
    $("#second_form").hide(); 
    $('#own_bussiness_show').hide();
    $('#company_vehicle_show').hide();

    $('#pers_doc').on('change',function(){
      var id = $('#pers_doc option:selected').val();   
      if(id == 1)
        $('#persoal_doc_view').show();
      else
        $('#persoal_doc_view').hide();    
    }); 

    $('#vehRegDropdown').on('change',function(){
      var id = $('#vehRegDropdown option:selected').val();    
      if(id == 1)
        $('#vehicle_reg_veiw').show();
      else
        $('#vehicle_reg_veiw').hide();
    });  

    $('#own_vehicle_dd').on('change',function(){
      var id = $('#own_vehicle_dd option:selected').val();    
      if(id == 1)
        $('#own_bussiness_show').show();
      else
        $('#own_bussiness_show').hide();
    });  

    $('#company_vehicle_dropdown').on('change',function(){
      var id = $('#company_vehicle_dropdown option:selected').val();    
      if(id == 1)
        $('#company_vehicle_show').show();
      else
        $('#company_vehicle_show').hide();
    }); 

    $("#drivrLicDoc").on('change',function(){
      if($(this).val() != '')
        $("#DrvLiceED").show();
    });

    $("#panelDrgTstResltDoc").on('change',function(){
      if($(this).val() != '')
        $("#panDrugTtED").show();   
    });

    $("#crimnlBGDoc").on('change',function(){
      if($(this).val() != '')
        $("#crimBGED").show(); 
    });

    $("#MotrVehRecDoc").on('change',function(){
      if($(this).val() != '')
        $("#motVehRecED").show(); 
    });

    $("#clearanceDoc").on('change',function(){
      if($(this).val() != '')
        $("#cleaED").show(); 
    });

    $("#vehInspDoc").on('change',function(){
      if($(this).val() != '')
        $("#vehiInspED").show(); 
    });

    $("#vehMaintRecDoc").on('change',function(){
      if($(this).val() != '')
        $("#vehMaintRecED").show(); 
    });

    $("#vehInsrnDoc").on('change',function(){
      if($(this).val() != '')
        $("#vehInsurnED").show(); 
    });

    $("#vehRegDoc").on('change',function(){
      if($(this).val() != '')
        $("#vehRegED").show(); 
    });

    $("#back").click(function(){  
      $("#first_form").show();
      $("#second_form").hide();          
    });
    var fnFlag=false;
    var dtFlag = false;
    var usrnFlag = false;
    var phFlag = false;
    var emlFlag = false;
    var ctryFlag = false;
    var stFlag = false;
    var ctFlag = false;
    var addFlag = false;
    var zipFlag = false;
    var warnignFlag= false;
    var errorFlag = false;
    var saveBtnFlag= false;
    var userExistCheck = false;
    var emailExistCheck = false;
    var divWarnignFlag = false;
    var divErrorFlag = false;

    $("#sumbitBtn").click(function(){
      saveBtnFlag = true;
    });

    $("#next").click(function(){  
      if($("#full_name").val()=="") fnFlag = true; else fnFlag = false;
      if($("#driverDate").val()=="") dtFlag = true; else dtFlag = false;
      if($("#username").val()=="") usrnFlag = true; else usrnFlag = false;
      if($("#phone").val()=="") phFlag = true; else phFlag = false;   
      if($("#email").val()=="") emlFlag = true; else emlFlag = false;
      if($("#country").val()=="") ctryFlag = true; else ctryFlag = false;
      if($("#state").val()=="") stFlag = true; else stFlag = false;
      if($("#city").val()=="") ctFlag = true; else ctFlag = false;
      if($("#address").val()=="") addFlag = true; else addFlag = false;
      if($("#zipcode").val()=="") zipFlag = true; else zipFlag = false;
      if($("#userModal").find('div.has-warning').text() != "") divWarnignFlag=true; else divWarnignFlag=false;
      if($("#userModal").find('div.has-error').text() != "") divErrorFlag=true; else divErrorFlag=false;
      if($("#userModal").find('p.at-warning').text() != "") warnignFlag=true; else warnignFlag=false;
      if($("#userModal").find('p.at-error').text() != "") errorFlag=true; else errorFlag=false;
      if($("#userModal").find('div.username1').text() != "") userExistCheck=true; else userExistCheck=false;
      if($("#userModal").find('div.email1').text() != "") emailExistCheck=true; else emailExistCheck=false;
    //alert(emailExistCheck +" "+ userExistCheck); return false;
    if(saveBtnFlag)
    {
      $(".error_message").remove();
      $("#first_form").hide();
      $("#second_form").show();
    }
    else{
      if(divWarnignFlag || divErrorFlag || fnFlag || dtFlag || usrnFlag || phFlag || emlFlag || ctryFlag || stFlag || ctFlag || addFlag || zipFlag || errorFlag || warnignFlag || emailExistCheck || userExistCheck)
      { 
        $(".error_message").remove();
        $("#error_message").parent().append("<div class='error_message' style='color:red;'>All fields are required or valid.</div>");
        $("#first_form").show();
        $("#second_form").hide();  
      }   
      else{
        $(".error_message").remove();
        $("#first_form").hide();
        $("#second_form").show();  
      }  
    }    
  });

    $('#country').on('change',function(){
      var countryID = $(this).val();         
      if(countryID){
        $.ajax({
          type:'POST',
          url:BASEURL+"admin/Drivers/get_state",
          data:{countryID:countryID},
          dataType:'json',
          success:function(json){                                     
           $('#state').html(json.state);
           $('#city').html('<option value="">Select state first</option>'); 
         }
       }); 
      }else{
        $('#state').html('<option value="">Select country first</option>');
        $('#city').html('<option value="">Select state first</option>'); 
      }
    });

    $('#state').on('change',function(){
      var stateID = $(this).val();
      if(stateID){
        $.ajax({
          type:'POST',
          url:BASEURL+'admin/Drivers/get_city',
          data:{'state_id':stateID},
          dataType:'json',
          success:function(json){
            $('#city').html(json.city);
          }
        }); 
      }else{
        $('#city').html('<option value="">Select state first</option>'); 
      }
    }); 
    $('#search').attr('disabled','disabled');
    $('#txt_search').keyup(function() {
      if($(this).val() != '') {
       $('#search').removeAttr('disabled');
     }
     else {
       $('#search').attr('disabled','disabled');
       listDrivers(0,'user','DESC'); 
     }
   });      
    $("#userForm").ajaxForm({
      dataType: 'json', 
      beforeSend: function() 
      {            
        $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {

      },
      success: function(json) 
    { //location.reload();
      if(json.status == "success"){           
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listDrivers(0,'user','DESC'); 
        }else{
          listDrivers(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else{
        toastr.error(json.msg,"Error:");
      }
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
      $("#userForm").find('[type="submit"]').removeAttr('disabled');
    },
    complete: function(json) 
    {
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
      $("#userForm").find('[type="submit"]').removeAttr('disabled');
    },
    error: function()
    {
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
      $("#userForm").find('[type="submit"]').removeAttr('disabled');
    }       
  });     
 //   }); 

 $(".sixMonthDate").datepicker({
  changeMonth:true,
  changeYear:true,
  minDate:0,
  maxDate: '+6m'    
});
 $( ".oneYearDate" ).datepicker({  
  changeMonth:true,
  changeYear:true,
  minDate:0,
  maxDate: '+1y'    
});

 $( ".fourYearDate" ).datepicker({   
  changeMonth:true,
  changeYear:true,
  minDate:0,
  maxDate: '+4y'    
});
});

function checkemail()
{
  var email = $("#email").val();
  var usertype = $("#usertype").val();
  $(".email").remove();
  $(".email1").remove();
  $("#userForm p.at-warning").remove();  
  if($("#email").val() != '')
  {
    var result = false;
    $.ajax({
      type:"POST",
      url:BASEURL+"checkemailAvailability",
      data:{'email':email,'usertype':usertype},
      dataType:"json",
      async:false,          
      success:function(response){    
        if(response.status == true)
        {
          $(".email1").remove(); 
          $("#email").parent().append("<div class='email1' id='email1' style='color:red;'>"+ response.message +"</div>");
          result = false;
          $("#userForm").find('[type="submit"]').attr("disabled", "disabled");
        }
        else
        {
          var username1 = $('#username1').attr('id'); 
          $(".email1").remove();    
          result = true;   
          if($("#userForm").find('[type="submit"]').is(':disabled') && typeof username1==="undefined")
            $("#userForm").find('[type="submit"]').removeAttr('disabled');
        } 
      }  
    });
    return result; 
  }
  else
  {
    return false;
  }
}


function checkusername()
{ 
  var username = $("#username").val();
  $(".username1").remove();
  $(".username").remove();
  var result = false;
  if($("#username").val() !='')
  {
    $.ajax({
      type:"POST",
      url:BASEURL+"frontend/Homecontroller/check_username_availability",
      data:{'username':username},
      dataType:"json",
      async:false,          
      success:function(response){
        if(response.status == true)
        {
          $(".username1").remove(); 
          $("#username").parent().append("<div class='username1' id='username1' style='color:red;'>"+ response.message +"</div>");
          $("#userForm").find('[type="submit"]').attr("disabled", "disabled");
          result = false;
        }
        else
        {
          var email1 = $('#email1').attr('id'); 
          result = true;
          $(".username1").remove();  
          if($("#userForm").find('[type="submit"]').is(':disabled') && typeof email1 ==="undefined" )   
            $("#userForm").find('[type="submit"]').removeAttr('disabled');
        }
      }   
    });  
    return result; 
  }
  else
  {
    return false;
  }  
}
