var pagenumber = 0;
var inprocess = null;
function list_of_notification(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Notification/list_of_notification",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {
    
      inprocess = null;
      if(json.status == "success"){
        $("#list_of_notification tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

$('#search').attr('disabled','disabled');
 $('#txt_search').keyup(function() {
    if($(this).val() != '') {
       $('#search').removeAttr('disabled');
    }
    else {
     $('#search').attr('disabled','disabled');
      list_of_notification(0,'user','DESC'); 
    }
 });


function get_notification($this){
  $("#notificationForm")[0].reset();
  $("#notificationForm div.has-error").removeClass("has-error");
  $("#notificationForm div.has-warning").removeClass("has-warning");
  $("#notificationForm div.has-success").removeClass("has-success");
  $("#notificationForm p.at-error").remove();
  $("#notificationForm span.glyphicon-remove").removeClass("glyphicon-remove");

  $('#notificationForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#notificationForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#notificationForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
  $("#notificationForm").find('[type="submit"]').prop("disabled", false);
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Notification/get_notification",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){            
            $("#user_key").val($($this).attr("data-row-id"));
            $("#notification_title").val(json.userData.n_title);  
            $("#notification_description").val(json.userData.n_full_description);          
            $("#user").val(json.userData.user_id);
            jQuery('#notificationModal').modal('show', {backdrop: 'static'});
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#notificationModal').modal('show', {backdrop: 'static'});
  }
}

function delete_notification($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to delete notification?'))
        {          
    $.ajax({
          type: "POST",
          dataType: "json",
         url: BASEURL+"admin/Notification/delete_notification",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
            $($this).parents("tr:first").remove();
            toastr.success(json.msg,"Success:");
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}   

var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
list_of_notification(0,column,order);

$("#list_of_notification th").click(function(){
  if($(this).hasClass("sorting")){
    $("#list_of_notification th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#list_of_notification th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      list_of_notification(0,column,order);
  }
});

$(document).ready(function()
{
  $("#notification_title").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });     

$("#notification_description").keydown(function (e) {      
  if (e.keyCode === 32 && !this.value.length)
    e.preventDefault();        
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,55,37,39,188,109,32,191]) !== -1 ||
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
        (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
            return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }        
});

  $("#notificationForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        $("#notificationForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    {
      if(json.status == "success"){   
      $("#notificationForm").find('[type="submit"]').prop("disabled", false);             
        jQuery('#notificationModal').modal('hide');
        if(json.action == "add"){
          list_of_notification(0,'user','DESC'); 
        }else{
          list_of_notification(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else{
        toastr.error(json.msg,"Error:");
      }
      $("#notificationForm").find('[type="submit"]').removeClass('sending').blur();
    },
    complete: function(json) 
    {
      $("#notificationForm").find('[type="submit"]').prop("disabled", false);
      $("#notificationForm").find('[type="submit"]').removeClass('sending').blur();
    },
    error: function()
    {
      $("#notificationForm").find('[type="submit"]').prop("disabled", false);
      $("#notificationForm").find('[type="submit"]').removeClass('sending').blur();
    }
  });
});
