var pagenumber = 0;
var inprocess = null;
function listUsers(page,column,order){
//alert("ghjghj");
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/users/listUsers",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {
    
      inprocess = null;
      if(json.status == "success"){
        $("#listUsers tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

// function getUser($this){
//   $("#userForm")[0].reset();
//   if($($this).attr("data-row-id")){
//     $.ajax({
//           type: "POST",
//           dataType: "json",
//           url: BASEURL+"users/getUser",
//           data: {"key":$($this).attr("data-row-id")},
//       }).success(function (json) {
//           if(json.status == "success"){
//             $("#user_key").val($($this).attr("data-row-id"));
//             $("#first_name").val(json.userData.first_name);
//             $("#last_name").val(json.userData.last_name);
//             jQuery('#userModal').modal('show', {backdrop: 'static'});
//             toastr.success(json.msg,"Success:");
//           }else{
//               toastr.error(json.msg,"Error:");
//           }
//       });
//   }else{
//     $("#user_key").val('');
//     jQuery('#userModal').modal('show', {backdrop: 'static'});
//   }
// }

// var column = 'user';
// var order = 'DESC';
// var ordercolumn = column;
// var orderby = order;
// listUsers(0,column,order);

// $("#listUsers th").click(function(){
//   if($(this).hasClass("sorting")){
//     $("#listUsers th").removeClass("sorting_asc").removeClass("sorting_desc");
//       column = $(this).attr("data-name");
//       order = $(this).attr("data-order");
//       ordercolumn = column;
//       orderby = order;
//       $("#listUsers th").attr("data-order","ASC");
//       if(order.toLowerCase() == 'asc'){
//          $(this).attr("data-order","DESC");
//       }else{
//           $(this).attr("data-order","ASC");
//       }
//       $(this).addClass("sorting_"+order.toLowerCase());
//       listUsers(0,column,order);
//   }
// });

function getUser($this){
   $("#userForm")[0].reset();
 // $("#userForm")[0].removeClass("error-block");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/users/getUser",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){            
            $("#user_key").val($($this).attr("data-row-id"));
            $("#first_name").val(json.userData.first_name);
            $("#last_name").val(json.userData.last_name);
            $("#address").val(json.userData.address);
            $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);           
                   
            var chk1=""; 
            if(json.userData.gender=="Male")
            {
              chk1='<input type="radio" name="gender" value="Male" checked> Male<input type="radio" name="gender" value="Female">Female<br>';
            }
            else
            {
              chk1='<input type="radio" name="gender" value="Male" > Male<input type="radio" name="gender" value="Female" checked>Female<br>';
            }
            $("#gender").html(chk1);           
            $('input[name=gender]:checked').val();
            $("#zipcode").val(json.userData.zipcode);
            jQuery('#userModal').modal('show', {backdrop: 'static'});

          //  toastr.success(json.msg,"Success:");
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}


  function changeStatus($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to change status?'))
        {
    $.ajax({
          type: "POST",
          dataType: "json",
         url: BASEURL+"admin/users/change_user_status",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
                  location.reload();
            //$("#status").val(json.userData.status);
            toastr.success(json.msg,"Success:");

          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}
    





var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listUsers(0,column,order);

$("#listUsers th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listUsers th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listUsers th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listUsers(0,column,order);
  }
});

$(document).ready(function()
{
  $("#userForm").validate({
      rules: {
        first_name: {
              required: true,
          },
        last_name: {
              required: true,
          },
           email: {
              required: true,
               
          },
        address: {
              required: true,
          },
        phone: {
              required: true,
          },
        gender: {
              required: true,
          }
       
      },
      errorClass: "error-block",
      errorElement: "span",
      messages: {
        first_name: "Please enter your first name",
        last_name: "Please enter your last name",
        email_address: "Please enter your email",
        address: "Please enter your address",
        gender: "Please enter  gender",
        phone: "Please enter your phone",
       
      }
  });
      
  $("#userForm").ajaxForm({

//console.log();
    dataType: 'json', 
    beforeSend: function() 
    {  
        $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    {


      if(json.status == "success"){           
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listUsers(0,'user','DESC'); 
        }else{
          listUsers(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else{
        toastr.error(json.msg,"Error:");
      }
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    },
    complete: function(json) 
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    },
    error: function()
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    }
  });
});
