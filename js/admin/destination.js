var pagenumber = 0;
var inprocess = null;


function listDestination(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Destination/listDestination",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
    console.log(json);
      inprocess = null;
      if(json.status == "success"){
          $("#listDestination tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}

function deleteDestination($this){
  if($($this).attr("data-row-id")){    
   if (confirm("Do you want to delete")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Destination/deleteDestination",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        setTimeout(function(){ location.reload(); }, 500);
           if(json.status == "success"){

            $("#user_key").val($($this).attr("data-row-id"));
            $($this).parents("tr:first").remove();            
            toastr.success(json.msg,"Success:");

          }else{
              toastr.error(json.msg,"Error:");
          }
      });
  }
  
  }
  else
  {
   return false;
  }
}

function getDestination($this){
  $("#serviceForm")[0].reset();  
//  $("#serviceForm span.error-block").css("display","none");
 // $("#serviceForm .form-control").removeClass("error-block");

$("#serviceForm p.at-error").remove();
$("#serviceForm p.at-error").removeClass("at-error");
  $("#serviceForm div.has-error").removeClass("has-error");
  $("#serviceForm div.has-success").removeClass("at-success");
  $("#serviceForm span.glyphicon-remove").removeClass("glyphicon-remove");
  //$('#dateissue').removeClass('has-success'); 
  $('#serviceForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#serviceForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#serviceForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');


  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Destination/getDestination",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){            
            $("#page_key").val($($this).attr("data-row-id"));
            $("#destination").val(json.pageData.destination);             
           jQuery('#serviceModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#page_key").val(''); 
    jQuery('#serviceModal').modal('show', {backdrop: 'static'});
  }
}

var column = 'fname';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listDestination(0,column,order);

$("#listDestination th").click(function(){

  if($(this).hasClass("sorting")){
    $("#listDestination th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listDestination th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listDestination(0,column,order);
  }
});

$(document).ready(function()
{
 $("#destination").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

 $('#search').attr('disabled','disabled');
     $('#txt_search').keyup(function() {
        if($(this).val() != '') {
           $('#search').removeAttr('disabled');
        }
        else {
         $('#search').attr('disabled','disabled');
          listDestination(0,'user','DESC'); 
        }
     });  
 
  $("#serviceForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#serviceForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
       $("#serviceForm").find('[type="submit"]').prop("disabled", false); 
          toastr.success(json.msg,'Success:');
          jQuery('#serviceModal').modal('hide');
          if(json.action == "add"){
            listDestination(0,'id','DESC'); 
          }else{
            listDestination(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
          $("#serviceForm").find('[type="submit"]').prop("disabled", false); 
        }
        $("#serviceForm").find('[type="submit"]').removeClass('sending').blur();
      },
      complete: function(json) 
      {
          $("#serviceForm").find('[type="submit"]').removeClass('sending').blur();
      },
      error: function()
      {
          $("#serviceForm").find('[type="submit"]').removeClass('sending').blur();
      }
  });
});
