/* Author : Ram K 
   Page : customer_care.js 
   Description:  Customer Care Details
   Date: 12 Feb 2018
*/
var column = 'first_name';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listOfCustomerCareDetails(0,column,order);

$("#listOfCustomerCareDetails th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listOfCustomerCareDetails th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listOfCustomerCareDetails th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listOfCustomerCareDetails(0,column,order);
  }
});


var pagenumber = 0;
var inprocess = null;
function listOfCustomerCareDetails(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Customer_care/listOfCustomerCareDetails",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {    
      inprocess = null;
      if(json.status == "success"){
        $("#listOfCustomerCareDetails tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}
function checkemail()
{
  var email = $("#email").val();
  $(".email").remove();
  $(".email1").remove();
  $("#userForm p.at-warning").remove();  
  if($("#email").val() != '')
  {
    var result = false;
    $.ajax({
      type:"POST",
      url:BASEURL+"admin/Customer_care/check_email_availability",
      data:{'email':email},
      dataType:"json",
      async:false,          
      success:function(response){    
        if(response.status == true)
        {
          $(".email1").remove(); 
          $("#email").parent().append("<div class='email1' id='email1' style='color:red;'>"+ response.message +"</div>");
          result = false;
          $("#userForm").find('[type="submit"]').attr("disabled", "disabled");
        }
        else
        {
          var username1 = $('#username1').attr('id'); 
          $(".email1").remove();    
          result = true;   
          if($("#userForm").find('[type="submit"]').is(':disabled') && typeof username1==="undefined")
          $("#userForm").find('[type="submit"]').removeAttr('disabled');
        } 
      }  
    });
    return result; 
    }
  else
  {
    return false;
  }
}


function checkusername()
{ 
  var username = $("#username").val();
  $(".username1").remove();
  $(".username").remove();
  var result = false;
  if($("#username").val() !='')
  {
    $.ajax({
      type:"POST",
      url:BASEURL+"admin/Customer_care/check_username_availability",
      data:{'username':username},
      dataType:"json",
      async:false,          
      success:function(response){
        if(response.status == true)
        {
          $(".username1").remove(); 
          $("#username").parent().append("<div class='username1' id='username1' style='color:red;'>"+ response.message +"</div>");
          $("#userForm").find('[type="submit"]').attr("disabled", "disabled");
          result = false;
        }
        else
        {
          var email1 = $('#email1').attr('id'); 
          result = true;
          $(".username1").remove();  
          if($("#userForm").find('[type="submit"]').is(':disabled') && typeof email1 ==="undefined" )   
          $("#userForm").find('[type="submit"]').removeAttr('disabled');
        }
      }   
    });  
    return result; 
  }
  else
  {
    return false;
  }  
}

function changeStatus($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are you sure you want to change the status?'))
        {
    $.ajax({
          type: "POST",
          dataType: "json",
         url: BASEURL+"admin/Customer_care/change_user_status",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){

            // location.reload();
            $("#user_key").val($($this).attr("data-row-id"));                
           $("#status").innerHTML=json.userData.status;           
            toastr.success(json.msg,"Success:");
            listOfCustomerCareDetails(0,column,order);
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');   
  }
}

function getDetails($this){
  $("#userForm")[0].reset();    
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Customer_care/getDetails",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
         console.log(json);
          if(json.status == "success"){ 
            $('#username').attr('disabled',true);

            $('#email').attr('disabled',true);
            $("#user_key").val($($this).attr("data-row-id"));
            $("#username").val(json.userData.username);
            $("#password").val(json.userData.actual_pwd);
            $("#first_name").val(json.userData.first_name);
            $("#last_name").val(json.userData.last_name);
            $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);      
            jQuery('#userModal').modal('show', {backdrop: 'static'});
            listOfCustomerCareDetails(0,column,order);          
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}

function deleteUser($this){
if($($this).attr("data-row-id")){   
   if(confirm("Do you want to delete")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Customer_care/deleteUser",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
           if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
            // $($this).parents("tr:first").remove();            
            toastr.success(json.msg,"Success:");
            listOfCustomerCareDetails(0,column,order);
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
    }  
  }
  else
  {
   return false;
  }
}

$(function() { //shorthand document.ready function
    $('#btn_submit').on('click', function(e) { //use on if jQuery 1.7+
         e.preventDefault();  //prevent form from submitting
        var data = $('#userForm').serialize();
        if($('#username').val() =='' || $('#password').val() =='' || $('#email').val() =='' || $('#first_name').val() =='' || $('#last_name').val() =='' || $('#phone').val() =='')
        {
          return false;
        }else{
              $.ajax({
              type: "POST",
              dataType: "json",
              url: BASEURL+"admin/Customer_care/save",
              data: {"formData":data},
          }).success(function (json) {
            //console.log(json);
              if(json.status == "success"){                            
                jQuery('#userModal').modal('hide');
                toastr.success(json.msg,"success:");
                listOfCustomerCareDetails(0,column,order); 
              }
              else{
                  toastr.error(json.msg,"Error:");
              }
          });
        }
         
    });





});





// function saveDetails($this){
//  alert($("#allocatedrideForm")[0].val());    
//   if($($this).attr("data-row-id")){
//     $.ajax({
//           type: "POST",
//           dataType: "json",
//           url: BASEURL+"admin/Nemt_allocated_ride/saveAllocatedRide",
//           data: {"key":$($this).attr("data-row-id")},
//       }).success(function (json) {
//          console.log(json);
//           if(json.status == "success"){ 
//             $("#user_key").val($($this).attr("data-row-id"));
//             $("#full_name").val(json.userData.full_name);
//             $("#free_ride").val(json.userData.free_ride);               
//             jQuery('#allocatedModal').modal('show', {backdrop: 'static'});          
//           }
//           else{
//               toastr.error(json.msg,"Error:");
//           }
//       });
//   }else{
//     $("#user_key").val('');
//     jQuery('#allocatedModal').modal('show', {backdrop: 'static'});
//   }
// }



// $(document).ready(function()
// {
//   $('#search').attr('disabled','disabled');
//    $('#txt_search').keyup(function() {
//       if($(this).val() != '') {
//          $('#search').removeAttr('disabled');
//       }
//       else {
//        $('#search').attr('disabled','disabled');
//         listOfCustomerCareDetails(0,'user','DESC'); 
//       }
//    }); 
//    $("#userForm").ajaxForm({
//           dataType: 'json', 
//           beforeSend: function() 
//           {            
//             $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
//           },
//           uploadProgress: function(event, position, total, percentComplete) 
//           {
              
//           },
//           success: function(json) 
//           { //location.reload();
//             if(json.status == "success"){           
//               jQuery('#userModal').modal('hide');
//               if(json.action == "add"){
//                 listPatients(0,'user','DESC'); 
//               }else{
//                 listPatients(pagenumber,ordercolumn,orderby);  
//               }
//               toastr.success(json.msg,"Success:");
//             }else{
//               toastr.error(json.msg,"Error:");
//             }
//             $("#userForm").find('[type="submit"]').removeClass('sending').blur();
//             $("#userForm").find('[type="submit"]').removeAttr('disabled');
//           },
//           complete: function(json) 
//           {
//               $("#userForm").find('[type="submit"]').removeClass('sending').blur();
//               $("#userForm").find('[type="submit"]').removeAttr('disabled');
//           },
//           error: function()
//           {
//               $("#userForm").find('[type="submit"]').removeClass('sending').blur();
//               $("#userForm").find('[type="submit"]').removeAttr('disabled');
//           }       
//         });   
// });


function addNew($this){
  $('#username').attr('disabled',false);
  $('#email').attr('disabled',false);
  $("#userForm")[0].reset();
  $("#userForm div.has-error").removeClass("has-error");
  $("#userForm div.has-warning").removeClass("has-warning");
  $("#userForm div.has-success").removeClass("has-success");
  $("#userForm span.glyphicon-remove").removeClass("glyphicon-remove");
  //$('#dateissue').removeClass('has-success'); 
  $('#userForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#userForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#userForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
  $('#userForm').find('div.username1').remove();
  $('#userForm').find('div.email1').remove();
  $("#userForm").find('[type="submit"]').removeAttr('disabled');
  $('.username1').remove();
  $('.email1').remove();
  //alert('hello 2');
//$('#fullnameissue').removeClass('has-warning');
var attr = $($this).attr("data-row-id");
if(typeof attr !== typeof undefined && attr !== false) {
$('#imgChange span').html('Change old image');  
}
else
{
  $('#imgChange span').html('Add new image');
}
//alert($($this).attr("data-row-id"));
//$('#imgChange span').html('Change old image');
  $('#insurance_provider').show();
  $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+"images/default.jpg"+" />");
  $('#pdf').html("<span></span>");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Patients/getPatient",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
         //console.log(json);
          if(json.status == "success"){ 
            $("#user_key").val($($this).attr("data-row-id"));
            $("#full_name").val(json.userData.full_name);
            $("#econtact_name").val(json.userData.emergency_contactname);
            $("#econtact_no").val(json.userData.emergency_contactno);             
            $("#street").val(json.userData.street);
            $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);
            $("#username").val(json.userData.username);             
            $("#county").val(json.userData.county); 
            $("#example1").val(json.userData.date_of_birth);
            $("#oldimg").val(json.userData.picture);            
            $("#oldpdf").val(json.userData.personal_doc);
            $("#insurance_id").val(json.userData.insurance_id); 
            $("#email").val(json.userData.email);
            $("#phone").val(json.userData.phone);  
            $("#signature").val(json.userData.signature);  
            $("#signature option:selected" ).text();
            $("#signature").val(json.userData.signature).attr('selected','selected');
            $("#country").val(json.userData.country).attr('selected','selected'); 
            $('#state').html(json.stateJSON); 
            $('#state').val(json.userData.state).attr('selected','selected'); 
            $('#city').html(json.cityJSON);
            $('#city').val(json.userData.city).attr('selected','selected');           
            if(json.userData.picture!='')
            {
              $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.picture+" width='200px' height='200px'/>");           
            }
            else
            {
              $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+'uploads/NEMT/thumb/img-dummy.jpg'+" width='200px' height='200px'/>");
            }
            jQuery('#userModal').modal('show', {backdrop: 'static'});
            if(json.userData.personal_doc!='')
            {
              $('#pdf').html("<a  target='_blank' href="+BASEURL+json.userData.personal_doc+">View</a>")           
            }
            var chk1=""; 
            $("#zipcode").val(json.userData.zipcode);
            jQuery('#userModal').modal('show', {backdrop: 'static'});
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}


