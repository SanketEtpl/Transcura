var pagenumber = 0;
var inprocess = null;
function listOfScheduleRide(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }  
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Ride_details/listOfScheduleRide",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {    
    console.log(json);
      inprocess = null;
      if(json.status == "success"){
          $("#listOfScheduleRide tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}

$('#search').attr('disabled','disabled');
 $('#txt_search').keyup(function() {
    if($(this).val() != '') {
       $('#search').removeAttr('disabled');
    }
    else {
     $('#search').attr('disabled','disabled');
      listOfScheduleRide(0,'user','DESC'); 
    }
 });

function getScheduleRide($this){  
  $('#testimg').html("");  
  $("#rideDetailsForm")[0].reset();  
  $("#rideDetailsForm span.error-block").css("display","none");
  $("#rideDetailsForm .form-control").removeClass("error-block");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Ride_details/getScheduleRide",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){ 
          $("#trip_id").text(json.pageData.sr_trip_id);
          $("#trip_date").text(json.pageData.sr_date);  
          $("#source_address").text(json.pageData.source_address);
          $("#destination_address").text(json.pageData.destination_address);  
          $("#duration").text(json.pageData.duration);
          $("#distance").text(json.pageData.sr_total_distance);  
          $("#pick_up_time").text(json.pageData.sr_pick_up_time);  
          $("#driver_pick_up_time").text(json.pageData.driver_pick_up_time);  
          $("#driver_drop_time").text(json.pageData.driver_drop_time);    
          if(json.pageData.sr_first_ride_status == "0")
            $("#first_ride").text("None");  
          else if(json.pageData.sr_first_ride_status == "1")   
            $("#first_ride").text("Running");
          else if(json.pageData.sr_first_ride_status == "2") 
            $("#first_ride").text("Completed");
          else
            $("#first_ride").text("Cancelled");

          if(json.pageData.sr_second_ride_status == "0")
            $("#second_ride").text("None");  
          else if(json.pageData.sr_second_ride_status == "1")   
            $("#second_ride").text("Running");
          else if(json.pageData.sr_first_ride_status == "2") 
            $("#second_ride").text("Completed");
          else
            $("#second_ride").text("Cancelled");

          if(json.pageData.driver_status == "0")
            $("#driver_status").text("Open ride");  
          else if(json.pageData.driver_status == "1")   
            $("#driver_status").text("Accepted ride");
          else if(json.pageData.driver_status == "2") 
            $("#driver_status").text("Closed ride");
          else
            $("#driver_status").text("Completed ride");
           
            jQuery('#rideDetailsModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#page_key").val(''); 
    jQuery('#rideDetailsModal').modal('show', {backdrop: 'static'});
  }
}
var column = 'id';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listOfScheduleRide(0,column,order);

$("#listOfScheduleRide th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listOfScheduleRide th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
    orderby = order;
      $("#listOfScheduleRide th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listOfScheduleRide(0,column,order);
  }
});

$(document).ready(function()
{ 
  $("#rideDetailsForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#rideDetailsForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
       $("#rideDetailsForm").find('[type="submit"]').prop("disabled", false); 
          toastr.success(json.msg,'Success:');
          jQuery('#rideDetailsModal').modal('hide');
          if(json.action == "add"){
            listOfScheduleRide(0,'id','DESC'); 
          }else{
            listOfScheduleRide(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
          $("#rideDetailsForm").find('[type="submit"]').prop("disabled", false); 
        }
        $("#rideDetailsForm").find('[type="submit"]').removeClass('sending').blur();
      },
      complete: function(json) 
      {
          $("#rideDetailsForm").find('[type="submit"]').removeClass('sending').blur();
      },
      error: function()
      {
          $("#rideDetailsForm").find('[type="submit"]').removeClass('sending').blur();
      }
  });
});
