var pagenumber = 0;
var inprocess = null;
function listOfScheduleRide(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }  
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Admin_ride_status/listOfScheduleRide",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {    
    console.log(json);
      inprocess = null;
      if(json.status == "success"){
          $("#listOfScheduleRide tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}

function getScheduleRide($this){    
  $("#adminRideReqForm")[0].reset();  
  $("#adminRideReqForm span.error-block").css("display","none");
  $("#adminRideReqForm .form-control").removeClass("error-block");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Admin_ride_status/getScheduleRide",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){            
            $("#user_key").val($($this).attr("data-row-id"));
            $("#sr_trip_id").text(json.pageData.sr_trip_id); 
            $("#sr_date").text(json.pageData.sr_date);
            $("#source_address").text(json.pageData.source_address);
            $("#destination_address").text(json.pageData.destination_address);
            $("#sr_pick_up_time").text(json.pageData.sr_pick_up_time);
            $("#duration").text(json.pageData.duration);
            $("#sr_total_distance").text(json.pageData.sr_total_distance);
            $("#admin_status").val(json.pageData.admin_status);
            if(json.pageData.sr_urgency_type == 1)
              $("#sr_urgency_type").text("Urgent");
            else
              $("#sr_urgency_type").text("Normal (Within 72 hours)");
            if(json.pageData.sr_trip_type == 1)
              $("#sr_trip_type").text("One way");
            else
              $("#sr_trip_type").text(" Round trip");           
            jQuery('#adminRideReqModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#page_key").val(''); 
    jQuery('#adminRideReqModal').modal('show', {backdrop: 'static'});
  }
}
var column = 'fname';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listOfScheduleRide(0,column,order);

$("#listOfScheduleRide th").click(function(){

  if($(this).hasClass("sorting")){
    $("#listOfScheduleRide th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
    orderby = order;
      $("#listOfScheduleRide th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listOfScheduleRide(0,column,order);
  }
});

$("input[name=tm_image]").change(function () {
    if (this.files && this.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var img = $('<img>').attr('src', e.target.result);
            $('#testimg').html(img);
             $('#tm_image1').html(img);
        };
        reader.readAsDataURL(this.files[0]);                    
    }
});


$('#search').attr('disabled','disabled');
     $('#txt_search').keyup(function() {
        if($(this).val() != '') {
           $('#search').removeAttr('disabled');
        }
        else {
         $('#search').attr('disabled','disabled');
          listOfScheduleRide(0,'user','DESC'); 
        }
     }); 

$(document).ready(function()
{ 
  $("#adminRideReqForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#adminRideReqForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
       $("#adminRideReqForm").find('[type="submit"]').prop("disabled", false); 
          toastr.success(json.msg,'Success:');
          jQuery('#adminRideReqModal').modal('hide');
          if(json.action == "add"){
            listOfScheduleRide(0,'id','DESC'); 
          }else{
            listOfScheduleRide(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
          $("#adminRideReqForm").find('[type="submit"]').prop("disabled", false); 
        }
        $("#adminRideReqForm").find('[type="submit"]').removeClass('sending').blur();
      },
      complete: function(json) 
      {
          $("#adminRideReqForm").find('[type="submit"]').removeClass('sending').blur();
      },
      error: function()
      {
          $("#adminRideReqForm").find('[type="submit"]').removeClass('sending').blur();
      }
  });
});
