var pagenumber = 0;
var inprocess = null;
CKEDITOR.replace('page_detail', {

     filebrowserBrowseUrl: BASEURL+'js/kcfinder/browse.php',
     filebrowserUploadUrl:  BASEURL+'js/kcfinder/upload.php'


});

var editorElement = CKEDITOR.document.getById( 'page_detail' );
CKEDITOR.instances.page_detail.on('change', function() {
         $("#page_detail").val(CKEDITOR.instances['page_detail'].getData());

      });
function listPages(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/pages/listPages",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
    console.log(json);
      inprocess = null;
      if(json.status == "success"){
          $("#listPages tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}

 $('#search').attr('disabled','disabled');
     $('#txt_search').keyup(function() {
        if($(this).val() != '') {
           $('#search').removeAttr('disabled');
        }
        else {
         $('#search').attr('disabled','disabled');
          listPages(0,'user','DESC'); 
        }
     });



function deletePage($this){

if($($this).attr("data-row-id")){
   // alert($($this).attr("data-row-id"));
   if (confirm("Do you want to delete")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/pages/deletePage",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
         location.reload();
           if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
                 $($this).parents("tr:first").remove();

            //$("#status").val(json.userData.status);
            toastr.success(json.msg,"Success:");
    
          }else{
              toastr.error(json.msg,"Error:");
          }
      });

  }
  
  }
  else
  {
   return false;

  }
}




function getPage($this){
  $("#pageForm")[0].reset();
  $("#pageForm div.has-error").removeClass("has-error");
  $("#pageForm div.has-warning").removeClass("has-warning");
  $("#pageForm div.has-success").removeClass("has-success");
  $("#pageForm p.at-error").remove();
  $("#pageForm span.glyphicon-remove").removeClass("glyphicon-remove");

  $('#pageForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#pageForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#pageForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');

 // $("#pageForm span.error-block").css("display","none");
 // $("#pageForm .form-control").removeClass("error-block");
  if($($this).attr("data-row-id")){
   //alert($($this).attr("data-row-id"));
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/pages/getPage",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            //alert(json.pageData.page_detail);
            $("#page_key").val($($this).attr("data-row-id"));
            $("#title").val(json.pageData.page_title);
             $("#page_detail").val(json.pageData.page_detail);
             $("#page_name").val(json.pageData.page_name); 
             $("#sub_title").val(json.pageData.sub_title);            
             CKEDITOR.instances['page_detail'].setData(json.pageData.page_detail);
           jQuery('#pageModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#page_key").val('');
    CKEDITOR.instances['page_detail'].setData();
  $("#title").val();
  $("#page_detail").val();
  $("#page_name").val();
    jQuery('#pageModal').modal('show', {backdrop: 'static'});
  }
}

var column = 'fname';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listPages(0,column,order);

$("#listPages th").click(function(){

  if($(this).hasClass("sorting")){
    $("#listPages th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
    orderby = order;
      $("#listPages th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listPages(0,column,order);
  }
});

$(document).ready(function()
{
  $("#title").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

  $("#sub_title").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

  $("#page_name").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9 || k == 173 ) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });

  $("#pageForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#pageForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
       $("#pageForm").find('[type="submit"]').prop("disabled", false); 
          toastr.success(json.msg,'Success:');
          jQuery('#pageModal').modal('hide');
          if(json.action == "add"){
            listPages(0,'page_id','DESC'); 
          }else{
            listPages(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
        $("#pageForm").find('[type="submit"]').prop("disabled", false); 
        }
        $("#pageForm").find('[type="submit"]').removeClass('sending').blur();


      },
      complete: function(json) 
      {
          $("#pageForm").find('[type="submit"]').removeClass('sending').blur();
      },
      error: function()
      {
          $("#pageForm").find('[type="submit"]').removeClass('sending').blur();
      }
  });
});
