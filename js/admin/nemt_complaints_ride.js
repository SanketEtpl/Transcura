var pagenumber = 0;
var inprocess = null;
function listOfNemtComplaintsRide(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Nemt_complaints_ride/listOfNemtComplaintsRide",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {    
      inprocess = null;
      if(json.status == "success"){
        $("#listOfNemtComplaintsRide tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

function getDetails($this){
  $("#complaintsRideForm")[0].reset();    
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Nemt_complaints_ride/getDetails",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
         console.log(json);
          if(json.status == "success"){ 
            //$("#user_key").val($($this).attr("data-row-id"));
            $("#full_name").text(json.userData.full_name);
            $("#src_driver_name").text(json.userData.src_driver_name);
            $("#src_date_of_service").text(json.userData.src_date_of_service);
            $("#src_name_of_transportation_provider").text(json.userData.src_name_of_transportation_provider);
            $("#src_description").text(json.userData.src_description);
            $("#ct_name").text(json.userData.ct_name);                              
            jQuery('#complaintsModal').modal('show', {backdrop: 'static'});          
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#complaintsModal').modal('show', {backdrop: 'static'});
  }
}

function delete_complaints_ride($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to delete complaints ride details ?'))
    {          
      $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Nemt_complaints_ride/delete_complaints_ride",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            $($this).parents("tr:first").remove(); 
            toastr.success(json.msg,"Success:");
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}  

var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listOfNemtComplaintsRide(0,column,order);

$("#listOfNemtComplaintsRide th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listOfNemtComplaintsRide th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listOfNemtComplaintsRide th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listOfNemtComplaintsRide(0,column,order);
  }
});

$(document).ready(function()
{
  $('#search').attr('disabled','disabled');
   $('#txt_search').keyup(function() {
      if($(this).val() != '') {
         $('#search').removeAttr('disabled');
      }
      else {
       $('#search').attr('disabled','disabled');
        listOfNemtComplaintsRide(0,'user','DESC'); 
      }
   });    
});
