var pagenumber = 0;
var inprocess = null;
function listServiceproviders(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
    type: "POST",
    dataType: "json",
    url: BASEURL+"admin/Doctors/listDoctors",
    data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},

  }).success(function (json) {

    inprocess = null;
    if(json.status == "success"){
      $("#listServiceproviders tbody").html(json.rows);
      $("#paginate_links").html(json.pagelinks);
      $("#paginate_entries").html(json.entries);
    }
  });
}
$('#search').attr('disabled','disabled');
$('#txt_search').keyup(function() {
  if($(this).val() != '') {
   $('#search').removeAttr('disabled');
 }
 else {
   $('#search').attr('disabled','disabled');
   listServiceproviders(0,'user','DESC'); 
 }
});

function deleteDoctor($this){
  if($($this).attr("data-row-id")){   
   if(confirm("Do you want to delete")){
    $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Doctors/deleteDoctor",
      data: {"key":$($this).attr("data-row-id")},
    }).success(function (json) {
     if(json.status == "success"){
      $("#user_key").val($($this).attr("data-row-id"));
      $($this).parents("tr:first").remove();            
      toastr.success(json.msg,"Success:");
    }else{
      toastr.error(json.msg,"Error:");
    }
  });
  }  
}
else
{
 return false;
}
}

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('#theDiv')
      .attr('src', e.target.result)
      .width(150)
      .height(200);
    };

    reader.readAsDataURL(input.files[0]);
  }
}


function getDoctor($this){
 $("#userForm")[0].reset();
  /*$("#userForm div.has-error").removeClass("has-error");
  $("#userForm span.glyphicon-remove").removeClass("glyphicon-remove");*/
  $("#userForm div.has-error").removeClass("has-error");
  $("#userForm div.has-warning").removeClass("has-warning");
  $("#userForm div.has-success").removeClass("has-success");
  $("#userForm span.glyphicon-remove").removeClass("glyphicon-remove");
  $('#userForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#userForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#userForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
  $('#insurance_provider').show();
  $('.username1').remove();
  $('.email1').remove();
  $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+"images/default.jpg"+" />");
  $('#pdf').html("<span></span>");
  var attr = $($this).attr("data-row-id");
  $("#userForm").find('[type="submit"]').removeAttr('disabled');
  if(typeof attr !== typeof undefined && attr !== false) {
    $('#imgChange span').html('Change profile picture');  
  }
  else
  {
    $('#imgChange span').html('Add new profile picture');
  }
  if($($this).attr("data-row-id")){
    $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Doctors/getDoctor",
      data: {"key":$($this).attr("data-row-id")},
    }).success(function (json) {
     console.log(json);
     if(json.status == "success"){ 
      $("#user_key").val($($this).attr("data-row-id"));
      $("#full_name").val(json.userData.full_name);
      $("#econtact_name").val(json.userData.emergency_contactname);
      $("#econtact_no").val(json.userData.emergency_contactno);             
      $("#address").val(json.userData.address);
      $("#street").val(json.userData.street);            
      $("#email").val(json.userData.email);
      $("#username").val(json.userData.username);
      $("#phone").val(json.userData.phone);  
      $("#example1").val(json.userData.date_of_birth);
      $("#oldimg").val(json.userData.picture);            
      $("#oldpdf").val(json.userData.personal_doc);
      $('#county').val(json.userData.county);
      $("#email").val(json.userData.email);
      $("#phone").val(json.userData.phone);  
      $("#signature").val(json.userData.signature);  
      $("#signature option:selected" ).text();
      $("#signature").val(json.userData.signature).attr('selected','selected');           
      $("#country").val(json.userData.country).attr('selected','selected'); 
      $('#state').html(json.stateJSON); 
      $('#state').val(json.userData.state).attr('selected','selected'); 
      $('#city').html(json.cityJSON);
      $('#city').val(json.userData.city).attr('selected','selected');
      if(json.userData.picture != '')
      {
       $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.picture+" width='200px' height='200px'/>");         
     }else{
       $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+'uploads/no-img.jpg'+" width='200px' height='200px'/>");         
     }		

     jQuery('#userModal').modal('show', {backdrop: 'static'});
     if(json.userData.personal_doc!='')
     {
      $('#pdf').html("<a  target='_blank' href="+BASEURL+json.userData.personal_doc+">View</a>")           
    }
    var chk1=""; 
    $("#zipcode").val(json.userData.zipcode);
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
  else{
    toastr.error(json.msg,"Error:");
  }
});
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}


function changeStatus($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to change status?'))
    {
      $.ajax({
        type: "POST",
        dataType: "json",
        url: BASEURL+"admin/Doctors/change_user_status",
        data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        if(json.status == "success"){
          $("#user_key").val($($this).attr("data-row-id"));
          location.reload();
            //$("#status").val(json.userData.status);
            toastr.success(json.msg,"Success:");

          }else{
            toastr.error(json.msg,"Error:");
          }
        });
    } }else{
      $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
 }
}






var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listServiceproviders(0,column,order);

$("#listServiceproviders th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listServiceproviders th").removeClass("sorting_asc").removeClass("sorting_desc");
    column = $(this).attr("data-name");
    order = $(this).attr("data-order");
    ordercolumn = column;
    orderby = order;
    $("#listServiceproviders th").attr("data-order","ASC");
    if(order.toLowerCase() == 'asc'){
     $(this).attr("data-order","DESC");
   }else{
    $(this).attr("data-order","ASC");
  }
  $(this).addClass("sorting_"+order.toLowerCase());
  listServiceproviders(0,column,order);
}
});

$("input[name=pimage]").change(function () {
  if (this.files && this.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      var img = $('<img>').attr('src', e.target.result);
      $('#theDiv').html(img);
      $('#pimage1').html(img);
    };
    reader.readAsDataURL(this.files[0]);                    
  }
});


$(document).ready(function()
{

  $("#userModal").on("hidden.bs.modal", function () 
  {
    $('#userForm div').removeClass('form-group has-error has-feedback').addClass('form-group'); 
    $('#userForm span .glyphicon glyphicon-remove form-control-feedback').remove(); 
    $('#userForm p').remove(); 
    $('#userForm').find('input:text, input:password, select, textarea').val('');
    $('#userForm').find('input:radio, input:checkbox').prop('checked', false);


  });
  $("#example1").on("change",function (){ 

    $('#dateissue').removeClass('form-group has-error has-feedback').addClass('form-group has-success has-feedback'); 
    $('#dateissue span').removeClass('glyphicon glyphicon-remove form-control-feedback').addClass('glyphicon glyphicon-ok form-control-feedback'); 
    $('#example1').removeClass('form-control hasDatepicker at-required').addClass('form-control at-success');
  $('#dateissue p').remove(); //form-control at-successform-control at-successform-control at-success

});

  $('#country').on('change',function(){
    var countryID = $(this).val();         
    if(countryID){
      $.ajax({
        type:'POST',
        url:BASEURL+"admin/Doctors/get_state",
        data:{countryID:countryID},
        dataType:'json',
        success:function(json){                                     
         $('#state').html(json.state);
         $('#city').html('<option value="">Select state first</option>'); 
       }
     }); 
    }else{
      $('#state').html('<option value="">Select country first</option>');
      $('#city').html('<option value="">Select state first</option>'); 
    }
  });
  $('#state').on('change',function(){
    var stateID = $(this).val();
    if(stateID){
      $.ajax({
        type:'POST',
        url:BASEURL+'admin/Doctors/get_city',
        data:{'state_id':stateID},
        dataType:'json',
        success:function(json){
          $('#city').html(json.city);
        }
      }); 
    }else{
      $('#city').html('<option value="">Select state first</option>'); 
    }
  });

  $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
      $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {

    },
    success: function(json) 
    {
      // location.reload();
      if(json.status == "success"){           
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listServiceproviders(0,'user','DESC'); 
        }else{
          listServiceproviders(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else{
        toastr.error(json.msg,"Error:");
      }
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
      $("#userForm").find('[type="submit"]').removeAttr('disabled');
    },
    complete: function(json) 
    {
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    },
    error: function()
    {
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
      $("#userForm").find('[type="submit"]').removeAttr('disabled');
    }
  });
});
function checkemail()
{
  var email = $("#email").val();
  var usertype = $("#usertype").val();
  $(".email").remove();
  $(".email1").remove();
  $("#userForm p.at-warning").remove();  
  if($("#email").val() != '')
  {
    var result = false;
    $.ajax({
      type:"POST",
      url:BASEURL+"checkemailAvailability",
      data:{'email':email,'usertype':usertype},
      dataType:"json",
      async:false,          
      success:function(response){    
        if(response.status == true)
        {
          $(".email1").remove(); 
          $("#email").parent().append("<div class='email1' id='email1' style='color:red;'>"+ response.message +"</div>");
          result = false;
          $("#userForm").find('[type="submit"]').attr("disabled", "disabled");
        }
        else
        {
          var username1 = $('#username1').attr('id');
          $(".email1").remove();    
          result = true;   
          if($("#userForm").find('[type="submit"]').is(':disabled') && typeof username1 ==="undefined")
            $("#userForm").find('[type="submit"]').removeAttr('disabled');
        } 
      }  
    });
    return result; 
  }
  else
  {
    return false;
  }
}

function checkusername()
{ 
  var username = $("#username").val();
  $(".username1").remove();
  $(".username").remove();
  var result = false;
  if($("#username").val() !=''){
    $.ajax({
      type:"POST",
      url:BASEURL+"frontend/Homecontroller/check_username_availability",
      data:{'username':username},
      dataType:"json",
      async:false,          
      success:function(response){     
        if(response.status == true)
        {
          $(".username1").remove(); 
          $("#username").parent().append("<div class='username1' id='username1' style='color:red;'>"+ response.message +"</div>");
          $("#userForm").find('[type="submit"]').attr("disabled", "disabled");
          result = false;

        }
        else
        {
          var email1 = $('#email1').attr('id');            
          result = true;
          $(".username1").remove();  
          if($("#userForm").find('[type="submit"]').is(':disabled') && typeof email1 === "undefined")   
            $("#userForm").find('[type="submit"]').removeAttr('disabled');
        }           
      }   
    });  
    return result; 
  }
  else
  {
    return false;
  }  
}
