var pagenumber = 0;
var inprocess = null;
function listDoctors_cat(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Doctors_cat/listdoctors_cat",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {
    
      inprocess = null;
      if(json.status == "success"){
        $("#listDoctors_cat tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

$('#search').attr('disabled','disabled');
     $('#txt_search').keyup(function() {
        if($(this).val() != '') {
           $('#search').removeAttr('disabled');
        }
        else {
         $('#search').attr('disabled','disabled');
          listDoctors_cat(0,'user','DESC'); 
        }
     });


function getDoctors_cat($this){
  $("#userForm")[0].reset();
  $("#userForm div.has-error").removeClass("has-error");
  $("#userForm div.has-warning").removeClass("has-warning");
  $("#userForm div.has-success").removeClass("has-success");
  $("#userForm p.at-error").remove();
  $("#userForm span.glyphicon-remove").removeClass("glyphicon-remove");

  $('#userForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#userForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#userForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
 // $("#userForm")[0].removeClass("error-block");
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Doctors_cat/getDoctors_cat",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){            
            $("#user_key").val($($this).attr("data-row-id"));
            $("#category_title").val(json.userData.cat_title);            
            jQuery('#userModal').modal('show', {backdrop: 'static'});

          //  toastr.success(json.msg,"Success:");
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}


  function changeStatus($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to change status?'))
        {
    $.ajax({
          type: "POST",
          dataType: "json",
         url: BASEURL+"admin/Service_providers_cat/change_status",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
              location.reload();            
            toastr.success(json.msg,"Success:");
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');   
  }
}
 function deleteDoctors_cat($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to delete category?'))
        {
    $.ajax({
          type: "POST",
          dataType: "json",
         url: BASEURL+"admin/Doctors_cat/deleteDoctors_cat",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
            listDoctors_cat(0,column,order);
                 // $($this).parents("tr:first").remove();
            //$("#status").val(json.userData.status);
            toastr.success(json.msg,"Success:");

          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}
   

var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listDoctors_cat(0,column,order);

$("#listDoctors_cat th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listDoctors_cat th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listDoctors_cat th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listDoctors_cat(0,column,order);
  }
});

$(document).ready(function()
{
  $("#category_title").keydown(function(event) {
    k = event.which;
    if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
      if ($(this).val().length == 100) {
        if (k == 8) {
          return true;
        } else {
          event.preventDefault();
          return false;
        }
      }
    } else {
      event.preventDefault();
      return false;
    }
    if (k === 32 && !this.value.length)
        event.preventDefault();
  });      
  $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    {
      if(json.status == "success"){   
 $("#userForm").find('[type="submit"]').prop("disabled", false);             
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listDoctors_cat(0,'user','DESC'); 
        }else{
          listDoctors_cat(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else{
        toastr.error(json.msg,"Error:");
      }
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    },
    complete: function(json) 
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    },
    error: function()
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
    }
  });
});
