var pagenumber = 0;
var inprocess = null;
function listAgents(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/agents/listAgents",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {
    
      inprocess = null;
      if(json.status == "success"){
        $("#listAgents tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}


function getAgent($this){
   $("#userForm")[0].reset();
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          // dataType: "json",


          url: BASEURL+"admin/agents/getAgent",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
   
        var response = JSON.parse(json);
          if(response.status == "success"){ 
       console.log(response);
            $("#user_key").val($($this).attr("data-row-id"));
            $("#first_name").val(response.userData.first_name);
            $("#last_name").val(response.userData.last_name);
            $("#address").val(response.userData.address);
            $("#email").val(response.userData.email);
            $("#phone").val(response.userData.phone);
        var abc = response.userData.agencies_id;
           
        $('#agencies').empty();
        $('#agencies').show();

         loc = response.agentData;
               
        for(var i=0 ; i<loc.length; i++)
        {
         // $("select option[value='xyz212323']").attr("selected","selected");
          var e = $("#agencies").append("<option value='"+loc[i].agencies_id+"' >"+loc[i].agencies_name+"</option>");
         $("select option").each(function(){
          if($(this).val()==abc)$(this).attr("selected","selected");
           });

        }

          var chk1=""; 
          

		//$('input[name="gender"][value="'+ response.userData.gender +'"]').prop('checked', true);

		if (response.userData.gender == 'female') {
		$('input:radio[name="gender"]').filter('[value="Female"]').attr('checked', true);
		}
		else
		{
		$('input:radio[name="gender"]').filter('[value="Male"]').attr('checked', true);
		}

		$("#zipcode").val(response.userData.zipcode);
		jQuery('#userModal').modal('show', {backdrop: 'static'});
            
          }else{
          toastr.error(response.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}


  function changeStatus($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to change status?'))
        {
    $.ajax({
          type: "POST",
          dataType: "json",
         url: BASEURL+"admin/agents/change_user_status",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            $("#user_key").val($($this).attr("data-row-id"));
                  location.reload();
            //$("#status").val(json.userData.status);
            toastr.success(json.msg,"Success:");

          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}
var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listAgents(0,column,order);

$("#listAgents th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listAgents th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listAgents th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listAgents(0,column,order);
  }
});

$(document).ready(function()
{
 
      
  $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
        $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    { //location.reload();
      if(json.status == "success"){           
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listAgents(0,'user','DESC'); 
        }else{
          listAgents(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else{
        toastr.error(json.msg,"Error:");
      }
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
      $("#userForm").find('[type="submit"]').removeAttr('disabled');
    },
    complete: function(json) 
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
       $("#userForm").find('[type="submit"]').removeAttr('disabled');
    },
    error: function()
    {
        $("#userForm").find('[type="submit"]').removeClass('sending').blur();
       $("#userForm").find('[type="submit"]').removeAttr('disabled');
    }
  });
});
