var pagenumber = 0;
var inprocess = null;

function listCities(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/cities/listCities",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
    //console.log(json);
      inprocess = null;
      if(json.status == "success"){
          $("#listCities tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}



function deleteCity($this){

if($($this).attr("data-row-id")){
   // alert($($this).attr("data-row-id"));
   if (confirm("Do you want to delete")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/cities/deleteCity",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
           // alert(json.pageData.title);
            window.location.reload();
          $("#banner_key").val($($this).attr("data-row-id"));            
          $("#listCities tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
          }
      });
  }
 // location.reload();
  }
  else
  {
   return false;

  }
}


function getCity($this){
	
  $("#bannerForm")[0].reset();
  $("#bannerForm span.error-block").css("display","none");
  $("#bannerForm .form-control").removeClass("error-block");

  if($($this).attr("data-row-id")){
    //alert($($this).attr("data-row-id"));
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/cities/getCity",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
			  console.log(json.imageData);
           // alert(json.pageData.title);
            $("#banner_key").val($($this).attr("data-row-id"));            
            $("#city").val(json.imageData.city);  
           $('#theDiv').html("<img name='userfile1' id='userfile1' src="+BASEURL+json.imageData.image+" width='150' height='100'/><input type='hidden' name='uploadfile1' value='"+json.imageData.image+"'/>")
      
           jQuery('#pageModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#banner_key").val('');
    jQuery('#pageModal').modal('show', {backdrop: 'static'});
  }
}

var column = 'city';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listCities(0,column,order);

$("#listCities th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listCities th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
    orderby = order;
      $("#listCities th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listCities(0,column,order);
  }
});

$(document).ready(function()
{

  $("#bannerForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#bannerForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
          toastr.success(json.msg,'Success:');
          jQuery('#pageModal').modal('hide');
          window.location.reload();
          if(json.action == "add"){
            listCities(0,'id','DESC'); 
          }else{
            listCities(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
        }
        $("#bannerForm").find('[type="submit"]').removeClass('sending').blur();
      },
      complete: function(json) 
      {
          $("#bannerForm").find('[type="submit"]').removeClass('sending').blur();
        //   window.location.reload();
      },
      error: function()
      {
          $("#bannerForm").find('[type="submit"]').removeClass('sending').blur();
         //  window.location.reload();
      }
  });
});
