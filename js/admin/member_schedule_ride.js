var pagenumber = 0;
var inprocess = null;
function listOfMemberScheduleRideDetails(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Member_schedule_ride/listOfMemberScheduleRideDetails",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {    
      inprocess = null;
      if(json.status == "success"){
        $("#listOfMemberScheduleRideDetails tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

function getDetails($this){
  $("#memberScheduleRideForm")[0].reset();    
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Member_schedule_ride/getDetails",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
         console.log(json);
          if(json.status == "success"){ 
            //$("#user_key").val($($this).attr("data-row-id"));
            $("#full_name").text(json.userData.full_name);
            $("#sr_trip_id").text(json.userData.sr_trip_id);
            $("#state_name").text(json.userData.state_name);
            $("#country_name").text(json.userData.country_name);
            $("#sr_date").text(json.userData.sr_date);
            $("#sr_pick_up_time").text(json.userData.sr_pick_up_time);                  
            $("#sr_appointment_time").text(json.userData.sr_appointment_time);
            $("#source_address").text(json.userData.source_address);
            $("#destination_address").text(json.userData.destination_address);         
            jQuery('#memberScheduleModal').modal('show', {backdrop: 'static'});          
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#memberScheduleModal').modal('show', {backdrop: 'static'});
  }
}

function delete_member_schedule_ride($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to delete member schedule ride details ?'))
    {          
      $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Member_schedule_ride/delete_member_schedule_ride",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
            $($this).parents("tr:first").remove();   
            toastr.success(json.msg,"Success:");
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}  

var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listOfMemberScheduleRideDetails(0,column,order);

$("#listOfMemberScheduleRideDetails th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listOfMemberScheduleRideDetails th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listOfMemberScheduleRideDetails th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listOfMemberScheduleRideDetails(0,column,order);
  }
});

$(document).ready(function()
{
  $('#search').attr('disabled','disabled');
   $('#txt_search').keyup(function() {
      if($(this).val() != '') {
         $('#search').removeAttr('disabled');
      }
      else {
       $('#search').attr('disabled','disabled');
        listOfMemberScheduleRideDetails(0,'user','DESC'); 
      }
   });    
});
