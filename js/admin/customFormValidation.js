 $("#zipcode").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
  $("#county").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,37,39,188,173,32,191]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90) ||( e.keyCode>=96 && e.keyCode<=105)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        
    });

	$("#phone").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
   $("#econtact_no").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $("#insurance_id").keydown(function (e) {        
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||             
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||              
            (e.keyCode >= 35 && e.keyCode <= 40)||( e.keyCode>=65 && e.keyCode<=90)) {                 
                 return;
        }        
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

	$(".fullname").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 222 || k == 189 || k == 173 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});
/*	$("#econtact_name").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 39 || k == 46 || k == 32 || k == 9) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});*/

	$("#street").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190,55,37,39,188,109,173,111,32,191,189,173]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        
    });

	
	$("#special_instruction").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105) || k == 9 || k == 173 || k == 109 || k == 8 || k == 55 || k == 37 || k == 39 || k == 46 || k == 32 || k == 190 || k == 110 || k == 191 || k == 111) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	  if (k === 32 && !this.value.length)
        event.preventDefault();
	});

	$("#username").keydown(function (e) {      
        if(e.keyCode === 32 && !this.value.length)
        e.preventDefault();
	    if ($.inArray(e.keyCode, [189,222,32,46, 8, 9, 27, 13, 110, 190,16,37,39,173,111,173,95,45,109]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        /* if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
     	 e.preventDefault();*/
     	 if($(this).val().indexOf('-') !== -1 && (e.keyCode == 109 ))
     	 e.preventDefault();
	    if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();
    });
  
	$("#city").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 9 || k == 39 || k == 46)  {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	});

	$(".name").keydown(function(event) {
	  k = event.which;
	  if ((k >= 65 && k <= 90) || k == 8 || k == 37 || k == 9 || k == 39 || k == 46)  {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        event.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    event.preventDefault();
	    return false;
	  }
	});

	$(".password").keydown(function(e) {		
	  k = e.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105 || k >= 48 && k<=57) || k == 32 || k == 9 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        e.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    e.preventDefault();
	    return false;
	  }
	    if($(this).val().indexOf(' ') !== -1 && (e.keyCode == 8))
        e.preventDefault();
    if (k === 32 && !this.value.length)
        e.preventDefault();
	});

	$("#pass").keydown(function(e) {		
	  k = e.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105 || k >= 48 && k<=57) || k == 32 || k == 9 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        e.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    e.preventDefault();
	    return false;
	  }
	    if($(this).val().indexOf(' ') !== -1 && (e.keyCode == 8))
        e.preventDefault();
    if (k === 32 && !this.value.length)
        e.preventDefault();
	});
	$("#login-password").keydown(function(e) {		
	  k = e.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105 || k >= 48 && k<=57) || k == 9 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        e.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    e.preventDefault();
	    return false;
	  }
	    if($(this).val().indexOf(' ') !== -1 && (e.keyCode == 8))
        e.preventDefault();
	});

	$("#conpass").keydown(function(e) {		
	  k = e.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105 || k >= 48 && k<=57) || k == 9 || k == 32 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        e.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    e.preventDefault();
	    return false;
	  }
	    if($(this).val().indexOf(' ') !== -1 && (e.keyCode == 8))
        e.preventDefault();
    if (k === 32 && !this.value.length)
        e.preventDefault();
	});

	/*$("#email").keydown(function(e) {		
	  k = e.which;
	  if ((k >= 65 && k <= 90 || k >= 96 && k <= 105) || k == 9 || k == 50 || k == 190 || k == 110 || k == 173  || k == 8 || k == 37 || k == 39 || k == 46) {
	    if ($(this).val().length == 100) {
	      if (k == 8) {
	        return true;
	      } else {
	        e.preventDefault();
	        return false;
	      }
	    }
	  } else {
	    e.preventDefault();
	    return false;
	  }
	  if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
      e.preventDefault();
  		if($(this).val().indexOf('_') !== -1 && (e.keyCode == 173 || e.keyCode == 95))
      e.preventDefault();

	    if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();
	});*/

	$("#email").keydown(function (e) {      
      if (e.keyCode === 32 && !this.value.length)
        e.preventDefault();
    	/* if(this.value.indexOf('@') !== -1 && (e.keyCode == 50))
     	 e.preventDefault(); */
     	if(this.value.indexOf('-') !== -1 && (e.keyCode == 189))
     	 e.preventDefault();
     	if(this.value.indexOf('__') !== -1 && (e.keyCode == 189))
     	 e.preventDefault();
  		if(this.value.indexOf('_') !== -1 && (e.keyCode == 173 || e.keyCode == 95))
      	e.preventDefault();
	    if(this.value.indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();

        if ($.inArray(e.keyCode, [190,50,189,46, 8, 9, 27, 13, 110, 190,16,37,39,173,111,173,95,45,109]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
         /*if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
     	 e.preventDefault();
     	 if($(this).val().indexOf('-') !== -1 && (e.keyCode == 109 ))
     	 e.preventDefault();
	    if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();*/
    });
	$("#title_name").keydown(function (e) {      
        if(e.keyCode === 32 && !this.value.length)
        e.preventDefault();
	    if ($.inArray(e.keyCode, [189,222,32,46, 8, 9, 27, 13, 110, 190,16,37,39,173,111,173,95,45,109,191]) !== -1 ||
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
            (e.keyCode >= 35 && e.keyCode <= 40) ||( e.keyCode>=65 && e.keyCode<=90)) {
                 return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        /* if($(this).val().indexOf('@') !== -1 && (e.keyCode == 50))
     	 e.preventDefault();*/
     	 if($(this).val().indexOf('-') !== -1 && (e.keyCode == 109 ))
     	 e.preventDefault();
	    if($(this).val().indexOf('..') !== -1 && (e.keyCode == 190 || e.keyCode == 110))
        e.preventDefault();
    });
	$("#description").keydown(function (e) {      
	if(e.keyCode === 32 && !this.value.length)
	e.preventDefault();
	});

