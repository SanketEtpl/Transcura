var pagenumber = 0;
var inprocess = null;
CKEDITOR.replace('banner_detail');

var editorElement = CKEDITOR.document.getById( 'banner_detail' );
CKEDITOR.instances.banner_detail.on('change', function() {
         $("#banner_detail").val(CKEDITOR.instances['banner_detail'].getData());
    //alert(CKEDITOR.instances['page_detail'].getData());
      });
function listBanners(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/banners/listBanners",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
    //console.log(json);
      inprocess = null;
      if(json.status == "success"){
          $("#listBanners tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}



function deleteBanners($this){

if($($this).attr("data-row-id")){
   // alert($($this).attr("data-row-id"));
   if (confirm("Do you want to delete")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/banners/deletePage",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
           // alert(json.pageData.title);
            window.location.reload();
          $("#banner_key").val($($this).attr("data-row-id"));            
          $("#listBanners tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
          }
      });
  }
 // location.reload();
  }
  else
  {
   return false;

  }
}


function getBanner($this){

  $("#bannerForm")[0].reset();
  $("#bannerForm span.error-block").css("display","none");
  $("#bannerForm .form-control").removeClass("error-block");

  if($($this).attr("data-row-id")){
    //alert($($this).attr("data-row-id"));
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/banners/getBanner",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){
//console.log(json.bannerData.thumbnail);
           // alert(json.pageData.title);
            $("#banner_key").val($($this).attr("data-row-id"));
            $("#title").val(json.bannerData.banner_title);
            $("#subtitle").val(json.bannerData.banner_subtitle);  
$('#theDiv').html("<img name='userfile1' id='userfile1' src="+BASEURL+json.bannerData.thumbnail+" width='150' height='100'/>")
       //   $("#theDiv").html("<img src='"+$(this).find("img").attr(BASEURL+json.bannerData.thumbnail)+"' class='imgPreview'>");

             $("#banner_detail").val(json.bannerData.banner_detail);
             CKEDITOR.instances['banner_detail'].setData(json.bannerData.banner_detail)
           jQuery('#pageModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#banner_key").val('');
    jQuery('#pageModal').modal('show', {backdrop: 'static'});
  }
}

var column = 'fname';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listBanners(0,column,order);

$("#listBanners th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listBanners th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
    orderby = order;
      $("#listBanners th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listBanners(0,column,order);
  }
});

$(document).ready(function()
{
  // $("#pageForm").validate({
  //     rules: {
  //       title: {
  //             required: true,
  //         },
  //       page_detail: {
  //             required: true,
  //         }
        
  //     },
  //     errorClass: "error-block",
  //     errorElement: "span",
  //     messages: {
  //       title: "Please enter content title  ",
  //       page_detail: "Please enter content details"
        
  //     }
  // });
  $("#bannerForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#bannerForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
          toastr.success(json.msg,'Success:');
          jQuery('#pageModal').modal('hide');
          window.location.reload();
          if(json.action == "add"){
            listBanners(0,'banner_id','DESC'); 
          }else{
            listBanners(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
        }
        $("#bannerForm").find('[type="submit"]').removeClass('sending').blur();
      },
      complete: function(json) 
      {
          $("#bannerForm").find('[type="submit"]').removeClass('sending').blur();
          // window.location.reload();
      },
      error: function()
      {
          $("#bannerForm").find('[type="submit"]').removeClass('sending').blur();
           //window.location.reload();
      }
  });
});
