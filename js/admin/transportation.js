var pagenumber = 0;
var inprocess = null;
// ------- listing  -----------
function listTranportation(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
    type: "POST",
    dataType: "json",
    url: BASEURL+"admin/Transportation/listTranportation",
    data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {    
    inprocess = null;
    if(json.status == "success"){
      $("#listTranportation tbody").html(json.rows);
      $("#paginate_links").html(json.pagelinks);
      $("#paginate_entries").html(json.entries);
    }
  });
}

var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listTranportation(0,column,order);

$("#listTranportation th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listTranportation th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listTranportation th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listTranportation(0,column,order);
  }
});


// ----------- get info for edit -----------
function getTransportation($this){
  $("#userForm")[0].reset();
  $("#userForm div.has-error").removeClass("has-error");
  $("#userForm div.has-warning").removeClass("has-warning");
  $("#userForm div.has-success").removeClass("has-success");
  $("#userForm span.glyphicon-remove").removeClass("glyphicon-remove");
  $('#userForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#userForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#userForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
  $('#insurance_provider').show();
  $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+"images/default.jpg"+" />");
  $('#pdf').html("<span></span>");
  $("#userForm").find('[type="submit"]').removeAttr('disabled');
  var attr = $($this).attr("data-row-id");
  if(typeof attr !== typeof undefined && attr !== false) {
    $('#imgChange span').html('Change Profile picture');  
  }
  else
  {
    $('#imgChange span').html('Add Profile picture');
  }
  if($($this).attr("data-row-id")){
    $.ajax({
        type: "POST",
        dataType: "json",
        url: BASEURL+"admin/Transportation/getTransportProv",
        data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
      console.log(json);
      if(json.status == "success"){ 
        $("#user_key").val($($this).attr("data-row-id"));
        $("#full_name").val(json.userData.full_name);
        $("#econtact_name").val(json.userData.emergency_contactname);
        $("#econtact_no").val(json.userData.emergency_contactno);             
        $("#address").val(json.userData.address);
        $("#street").val(json.userData.street);            
        $("#email").val(json.userData.email);
        $("#username").val(json.userData.username);
        $("#phone").val(json.userData.phone);  
        $("#example1").val(json.userData.date_of_birth);
        $("#oldimg").val(json.userData.picture);            
        // $("#oldpdf").val(json.userData.personal_doc);
        $('#county').val(json.userData.county);
        $("#email").val(json.userData.email);
        $("#phone").val(json.userData.phone);  
        $("#signature").val(json.userData.signature);  
        $("#signature option:selected" ).text();
        $("#signature").val(json.userData.signature).attr('selected','selected');           
        $("#country").val(json.userData.country).attr('selected','selected'); 
        $('#state').html(json.stateJSON); 
        $('#state').val(json.userData.state).attr('selected','selected'); 
        $('#city').html(json.cityJSON);
        $('#city').val(json.userData.city).attr('selected','selected');
        if(json.userData.picture != '')
        {
          $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+json.userData.picture+" width='200px' height='200px'/>");
        }
        else
        {
          $('#theDiv').html("<img name='pimage1' id='pimage1' src="+BASEURL+'uploads/NEMT/thumb/img-dummy.jpg'+" width='200px' height='200px'/>");
        }         
        jQuery('#userModal').modal('show', {backdrop: 'static'});
        if(json.userData.personal_doc!='')
        {
          $('#pdf').html("<a  target='_blank' href="+BASEURL+json.userData.personal_doc+">View</a>")           
        }
        var chk1=""; 
        $("#zipcode").val(json.userData.zipcode);
        jQuery('#userModal').modal('show', {backdrop: 'static'});
      }
      else{
        toastr.error(json.msg,"Error:");
      }
    });
  }else{
    $("#user_key").val('');
    jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}

//------- change status ----------
function changeStatus($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are you sure you want to change the status?'))
    {
      $.ajax({
        type: "POST",
        dataType: "json",
        url: BASEURL+"admin/Transportation/change_user_status",
        data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
        if(json.status == "success"){   
        listTranportation(0,column,order);    
          // $("#user_key").val($($this).attr("data-row-id"));                
         // $("#status").innerHTML=json.userData.status;           
          toastr.success(json.msg,"Success:");
        }else{
            toastr.error(json.msg,"Error:");
        }
      });
    }
  }else{
    $("#user_key").val('');   
  }
}


function readURL(input) {
  if(input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function (e) {
        $('#theDiv')
            .attr('src', e.target.result)
            .width(150)
            .height(200);
    };
    reader.readAsDataURL(input.files[0]);
  }
}





$("input[name=pimage]").change(function () {
  if (this.files && this.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          var img = $('<img>').attr('src', e.target.result);
          $('#theDiv').html(img);
           $('#pimage1').html(img);
      };
      reader.readAsDataURL(this.files[0]);                    
  }
});

$(document).ready(function()
{   
     $('#state').on('change',function(){
        var stateID = $(this).val();
        if(stateID){
            $.ajax({
                type:'POST',
                url:BASEURL+'admin/Member/get_city',
                data:{'state_id':stateID},
                dataType:'json',
                success:function(json){
                    $('#city').html(json.city);
                }
            }); 
        }else{
            $('#city').html('<option value="">Select state first</option>'); 
        }
    });

    $('#search').attr('disabled','disabled');
     $('#txt_search').keyup(function() {
        if($(this).val() != '') {
           $('#search').removeAttr('disabled');
        }
        else {
         $('#search').attr('disabled','disabled');
          listTranportation(0,'user','DESC'); 
        }
     });  

  $("#userForm").ajaxForm({
    dataType: 'json', 
    beforeSend: function() 
    {
      $("#userForm").find('[type="submit"]').toggleClass('sending').blur();
    },
    uploadProgress: function(event, position, total, percentComplete) 
    {
        
    },
    success: function(json) 
    { //location.reload();
      if(json.status == "success"){           
        jQuery('#userModal').modal('hide');
        if(json.action == "add"){
          listTranportation(0,'user','DESC'); 
        }else{
          listTranportation(pagenumber,ordercolumn,orderby);  
        }
        toastr.success(json.msg,"Success:");
      }else  if(json.status == "error"){
        toastr.error(json.msg,"Error:");
      }
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
      $("#userForm").find('[type="submit"]').removeAttr('disabled');
    },
    complete: function(json) 
    {
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
      $("#userForm").find('[type="submit"]').removeAttr('disabled');
    },
    error: function()
    {
      $("#userForm").find('[type="submit"]').removeClass('sending').blur();
      $("#userForm").find('[type="submit"]').removeAttr('disabled');
    }
  });
});
