var pagenumber = 0;
var inprocess = null;

function listAllSecondSectionData(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Second_section/listAllSecondSectionData",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
  }).success(function (json) {
    console.log(json);
      inprocess = null;
      if(json.status == "success"){
          $("#listAllSecondSectionData tbody").html(json.rows);
          $("#paginate_links").html(json.pagelinks);
          $("#paginate_entries").html(json.entries);
      }
  });
}



 $('#search').attr('disabled','disabled');
     $('#txt_search').keyup(function() {
        if($(this).val() != '') {
           $('#search').removeAttr('disabled');
        }
        else {
         $('#search').attr('disabled','disabled');
          listAllSecondSectionData(0,'user','DESC'); 
        }
     });
     
function deleteSecondSection($this){
  if($($this).attr("data-row-id")){    
   if (confirm("Do you want to delete")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Second_section/deleteSecondSection",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
           if(json.status == "success"){
            $("#page_key").val($($this).attr("data-row-id"));
            $($this).parents("tr:first").remove();            
            toastr.success(json.msg,"Success:");
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
    }  
  }
  else
  {
   return false;
  }
}

function readURL(input) {
  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          $('#theDiv')
              .attr('src', e.target.result)
              .width(150)
              .height(200);
      };
      reader.readAsDataURL(input.files[0]);
  }
}

function getSecondSection($this){
  $("#SSForm")[0].reset();  
  $("#SSForm span.error-block").css("display","none");
  $("#SSForm .form-control").removeClass("error-block");
  $('#theDiv').html("<img name='icon_image' id='icon_image' src="+BASEURL+"images/default.jpg"+" />");
  $("#SSForm div.has-error").removeClass("has-error");
  $("#SSForm div.has-warning").removeClass("has-warning");
  $("#SSForm div.has-success").removeClass("has-success");
  $("#SSForm span.glyphicon-remove").removeClass("glyphicon-remove");
  //$('#dateissue').removeClass('has-success'); 
  $('#SSForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
  $('#SSForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
  $('#SSForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
  $("oldimg").val();
var attr = $($this).attr("data-row-id");
  if(typeof attr !== typeof undefined && attr !== false) {
    $('#imgChange label').html('Change old Icon');  
  }
  else
  {
    $('#imgChange label').html('Add new Icon');
  }
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Second_section/getSecondSection",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){            
            $("#page_key").val($($this).attr("data-row-id"));
            $("#title_name").val(json.pageData.ss_title);
            $("#description").val(json.pageData.ss_description);
            $("#oldimg").val(json.pageData.ss_icon_image);
           if(json.pageData.ss_icon_image!='')
            {
              $('#theDiv').html("<img name='icon_image' id='icon_image' src="+BASEURL+json.pageData.ss_icon_image+" width='200px' height='200px'/>");           
            }
            else
            {
              $('#theDiv').html("<img name='icon_image' id='icon_image' src="+BASEURL+'uploads/NEMT/thumb/img-dummy.jpg'+" width='200px' height='200px'/>");
            }
          
           jQuery('#SSModal').modal('show', {backdrop: 'static'});
          }
      });
  }else{
    $("#page_key").val(''); 
    jQuery('#SSModal').modal('show', {backdrop: 'static'});
  }
}


  $("input[name=icon_image]").change(function () {
    if(this.files && this.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
          var img = $('<img>').attr('src', e.target.result);
          $('#theDiv').html(img); 
          $("#SSForm div.has-error").removeClass("has-error");
          $("#SSForm div.has-warning").removeClass("has-warning");
          $("#SSForm div.has-success").removeClass("has-success");
          $("#SSForm span.glyphicon-remove").removeClass("glyphicon-remove");
          //$('#dateissue').removeClass('has-success'); 
          $('#SSForm span').removeClass('glyphicon glyphicon-ok form-control-feedback'); 
          $('#SSForm span').removeClass('glyphicon glyphicon-warning-sign form-control-feedback'); 
          $('#SSForm span').removeClass('glyphicon glyphicon-remove form-control-feedback');
          $('#SSForm p.at-error').remove();
          $('#SSForm p.has-warning').remove();
      };
      reader.readAsDataURL(this.files[0]);                    
    }
});

var column = 'id';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listAllSecondSectionData(0,column,order);

$("#listAllSecondSectionData th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listAllSecondSectionData th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listAllSecondSectionData th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listAllSecondSectionData(0,column,order);
  }
});

$(document).ready(function()
{ 
  $("#SSForm").ajaxForm({
      dataType: 'json',
      beforeSend: function() 
      {
          $("#SSForm").find('[type="submit"]').toggleClass('sending').blur();
      },
      uploadProgress: function(event, position, total, percentComplete) 
      {
          
      },
      success: function(json) 
      {
        if(json.status == "success"){
       $("#SSForm").find('[type="submit"]').prop("disabled", false); 
          toastr.success(json.msg,'Success:');
          jQuery('#SSModal').modal('hide');
          if(json.action == "add"){
            listAllSecondSectionData(0,'id','DESC'); 
          }else{
            listAllSecondSectionData(pagenumber,ordercolumn,orderby);  
          }
        }else{
          toastr.error(json.msg,'Error:');
          $("#SSForm").find('[type="submit"]').prop("disabled", false); 
        }
        $("#SSForm").find('[type="submit"]').removeClass('sending').blur();
      },
      complete: function(json) 
      {
          $("#SSForm").find('[type="submit"]').removeClass('sending').blur();
      },
      error: function()
      {
          $("#SSForm").find('[type="submit"]').removeClass('sending').blur();
      }
  });
});
