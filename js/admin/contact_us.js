var pagenumber = 0;
var inprocess = null;
function listOfContactUs(page,column,order){
  pagenumber = page;
  if(inprocess){ 
    inprocess.abort();
  }
  inprocess = $.ajax({
      type: "POST",
      dataType: "json",
      url: BASEURL+"admin/Contact_us/listOfContactUs",
      data: {"page":page,"column":column,"order":order,"search":$("#txt_search").val()},
      
  }).success(function (json) {    
      inprocess = null;
      if(json.status == "success"){
        $("#listOfContactUs tbody").html(json.rows);
        $("#paginate_links").html(json.pagelinks);
        $("#paginate_entries").html(json.entries);
      }
  });
}

function getDetails($this){
  $("#contactUsForm")[0].reset();    
  if($($this).attr("data-row-id")){
    $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Contact_us/getDetails",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
         console.log(json);
          if(json.status == "success"){ 
            $("#full_name").text(json.userData.cu_first_name+' '+json.userData.cu_last_name);                  
            $("#cu_email").text(json.userData.cu_email);
            $("#cu_phone").text(json.userData.cu_phone);
            $("#cu_message").text(json.userData.cu_message);         
            jQuery('#contactUsModal').modal('show', {backdrop: 'static'});          
          }
          else{
              toastr.error(json.msg,"Error:");
          }
      });
  }else{
    $("#user_key").val('');
    jQuery('#contactUsModal').modal('show', {backdrop: 'static'});
  }
}

function delete_contact_us($this)
{
  if($($this).attr("data-row-id")){
    if(confirm('Are sure want to delete contact us ?'))
    {          
      $.ajax({
          type: "POST",
          dataType: "json",
          url: BASEURL+"admin/Contact_us/delete_contact_us",
          data: {"key":$($this).attr("data-row-id")},
      }).success(function (json) {
          if(json.status == "success"){                      
          $($this).parents("tr:first").remove();  
            toastr.success(json.msg,"Success:");
          }else{
              toastr.error(json.msg,"Error:");
          }
      });
 } }else{
    $("#user_key").val('');
   // jQuery('#userModal').modal('show', {backdrop: 'static'});
  }
}  

var column = 'user';
var order = 'DESC';
var ordercolumn = column;
var orderby = order;
listOfContactUs(0,column,order);

$("#listOfContactUs th").click(function(){
  if($(this).hasClass("sorting")){
    $("#listOfContactUs th").removeClass("sorting_asc").removeClass("sorting_desc");
      column = $(this).attr("data-name");
      order = $(this).attr("data-order");
      ordercolumn = column;
      orderby = order;
      $("#listOfContactUs th").attr("data-order","ASC");
      if(order.toLowerCase() == 'asc'){
         $(this).attr("data-order","DESC");
      }else{
          $(this).attr("data-order","ASC");
      }
      $(this).addClass("sorting_"+order.toLowerCase());
      listOfContactUs(0,column,order);
  }
});

$(document).ready(function()
{
  $('#search').attr('disabled','disabled');
   $('#txt_search').keyup(function() {
      if($(this).val() != '') {
         $('#search').removeAttr('disabled');
      }
      else {
       $('#search').attr('disabled','disabled');
        listOfContactUs(0,'user','DESC'); 
      }
   });    
});
