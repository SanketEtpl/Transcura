$(function() {

    // var owner = $('#owner');
    var cardNumber = $('#card_number');
    // var cardNumberField = $('#card-number-field');
    var CVV = $("#card_cvv");
    // var mastercard = $("#mastercard");
    // var confirmButton = $('#confirm-purchase');
    // var visa = $("#visa");
    // var amex = $("#amex");

    // Use the payform library to format and validate
    // the payment fields.

    cardNumber.payform('formatCardNumber');
    CVV.payform('formatCardCVC');


    cardNumber.keyup(function() {
        // amex.removeClass('transparent');
        // visa.removeClass('transparent');
        // mastercard.removeClass('transparent');

        if ($.payform.validateCardNumber(cardNumber.val()) == false) {
            $(".card_number").remove(); 
            $("#card_number").parent().append("<div class='card_number' style='color:red;'>Invalid Card Number.</div>");
            cardnumberFlag =false;
            // cardNumberField.addClass('has-error');
        } else {
            $(".card_number").remove();
            cardnumberFlag =true; 
            // cardNumberField.removeClass('has-error');
            // cardNumberField.addClass('has-success');
        }

        // if ($.payform.parseCardType(cardNumber.val()) == 'visa') {
        //     mastercard.addClass('transparent');
        //     amex.addClass('transparent');
        // } else if ($.payform.parseCardType(cardNumber.val()) == 'amex') {
        //     mastercard.addClass('transparent');
        //     visa.addClass('transparent');
        // } else if ($.payform.parseCardType(cardNumber.val()) == 'mastercard') {
        //     amex.addClass('transparent');
        //     visa.addClass('transparent');
        // }
    });

    $('#btnSubmit').click(function(e) {
        var cardnumberFlag = false;
        e.preventDefault();

        var isCardValid = $.payform.validateCardNumber(cardNumber.val());
        // var isCvvValid = $.payform.validateCardCVC(CVV.val());
        // console.log(isCardValid);
        if (!isCardValid) {
            $(".card_number").remove(); 
            $("#card_number").parent().append("<div class='card_number' style='color:red;'>Wrong Card Number.</div>");
             cardnumberFlag =false;
        }  else {
           $(".card_number").remove(); 
            cardnumberFlag =true;
       }
   });
});
