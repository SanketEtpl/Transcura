$(document).ready(function(){
	$('#seach_restaurant').click(function(){
		$("#schLoadingDiv").show();		
		$.ajax({
			type : "POST",
			url : base_url+"frontend/MemberController/searchRestaurant",
			data :
			{
				'location' : $('#location').val(),
				'query' : $('#search_query').val()
			},
			success : function(response)
			{
				$("#schLoadingDiv").hide();
				var obj = JSON.parse(response);
				if(obj.status == "success")
				{
					var data = '';
				$('#responseData').html('');
				if(obj.data.results.length < 1)
				{
					$('#responseData').html(response.msg);
				}else
				{

					for(i = 0;i< obj.data.results.length;i++ )
					{
						Id = obj.data.results[i].place_id;
						data += '<div class="list-group"><a href="'+base_url+'restaurant_details/'+Id+'" class="list-group-item"><h4 class="list-group-item-heading">'+ obj.data.results[i].name+'</h4><p class="list-group-item-text">'+ obj.data.results[i].formatted_address+'</p> </a></div>';

					}
					$('#responseData').html(data);
					$('#recordLength').html('<span>'+obj.data.results.length +' records found</span>');
				}
				}else if(obj.status == "error")
				{
					$('#responseData').html('<p>'+obj.msg+'</p>');
					$('#recordLength').html('');
				}
				

			// 	console.log(obj.data.restaurants[0].restaurant.name);
			// 	var data = '';

			// 			/*obj.data.restaurants.each(function(i){

			// 				data += '<div>'+ obj.data.restaurants[i].restaurant.name+' </div>';

			// 			});*/
			// 			$('#responseData').html('');
			// 			var len = obj.data.restaurants.length;
			// 			var myArr = [];
			// 			data = '';
			// 			for(i = 0;i< obj.data.restaurants.length;i++ )
			// 			{
			// 				Id = obj.data.restaurants[i].restaurant.id;							
			// 				//newArr = JSON.stringify(myArr);
			// 				//console.log(newArr);
			// 				data += '<div class="list-group"><a href="'+base_url+'restaurant_details/'+Id+'" class="list-group-item"><h4 class="list-group-item-heading">'+ obj.data.restaurants[i].restaurant.name+'</h4><p class="list-group-item-text">'+ obj.data.restaurants[i].restaurant.location.address+'</p> </a></div>';

			// 				/*<div class="list-group">
			//   <a href="#" class="list-group-item active">
			//     <h4 class="list-group-item-heading">List group item heading</h4>
			//     <p class="list-group-item-text">...</p>
			//   </a>
			//   </div>*/
			// }

			// console.log(data);

			// $('#responseData').html(data);
			// $('#recordLength').html('<span>'+len +' records found</span>');


		}

	});

	});

		// -------- get address and location data onclick get address -------------

		$("#getRestaAddress").on("change click",function(){
			var source = $("#source").val();
			var destination = $("#destination").val();
			var end_lat = $("#end_lat").val();
			var end_long = $("#end_long").val();

			if(source != "" && destination != ""){
				$("#schLoadingDiv").show();
				$("#rest_open_hours").html('');
				$.ajax({
					type:"POST",
					url:base_url+"frontend/MemberController/get_Restaddress",
					data:{'source':source,'destination':destination,'end_lat':end_lat,'end_long':end_long},
					dataType:"json",
					async:false,					
					success:function(response){
							/*var obj = JSON.parse(response.data);							
							console.log(obj);*/
							if(response.status == true){
								if(response.data.hasOwnProperty("opening_hours"))
								{
									var opn_hrs = response.data.opening_hours.weekday_text;	
									opn_hrs.forEach(function(element) {
										$("#rest_open_hours").append(element);
										$("#rest_open_hours").append("<br />");
									});
								}else
								{
									$("#rest_open_hours").html("Not available.");
								}

								var phone_no = (response.data.hasOwnProperty("formatted_phone_number")) ? response.data.formatted_phone_number : 'Not available';
								
								$("#schLoadingDiv").hide();
								$("#drop_point_details").show();
								$("#listAddress tbody").html(response.rows);
								

								$("#rest_name").text(response.data.name);
								$("#rest_address").text(response.data.formatted_address);
								$("#rest_mob_number").text(phone_no);
							// $("#rest_open_hours").text(response.data.opening_hours.weekday_text[2]);
							
							
							$("#source_point").val($('#source').val());
							$("#destination_point").val($('#destination').val());


							if(response.loc_data == null)	
							{

							} else
							{
								$("#start_lat").val(response.loc_data.start_lat);						
								$("#start_long").val(response.loc_data.start_long);						
								$("#distance").val(response.loc_data.distance);						
								$("#duration").val(response.loc_data.duration);
							}				

						}
						else
						{
							$("#schLoadingDiv").hide();
							//alert(response.message);				
						}

					}
				});
			}else
			{
				alert("Please enter source and destination point.");
			}
		});


	//---------- save schedule  -----------------

	$("#bookMemberRequest").on("change click",function(){	
		console.log(' Source ' +$('#memberSourceAddr').val());
		console.log(' End lat ' +$('#tot_distance').val());

		/*console.log(formData);

		return false;*/
		$.ajax({
			type:"POST",
			url:base_url+"frontend/MemberController/save_rest_schedule",
			data: 
			{
				'source' : $('#memberSourceAddr').val(),
				'destination' : $('#memberDestinationAddr').val(),
				'end_lat' : $('#end_lat').val(),
				'end_long' : $('#end_long').val(),
				'tot_distance' : $('#tot_distance').val(),
				'trip_duration' : $('#trip_duration').val(),
				'tot_amt' : $('#tot_amt').val()
			},	
			error: function (xhr, status)
			{
				// toastr.error(status, "Error:");
			},			
			success:function(response)
			{
				var obj = JSON.parse(response);
				//alert(obj.message);
				if(obj.status == "1")
				{
					$('#memberRideModal').modal('show');
					$('#responseMsg').html(obj.message);
					$('#rideTripId').html('Trip ID : '+obj.trip_id);

					// setTimeout(function(){
					// 	window.location.href = base_url+"member-my-ride";											
					// 	$('#memberRideModal').modal('hide');

					// },2000);
				}
				else if(obj.status == "0")
				{
					$('#memberRideModal').modal('show');
					$('#responseMsg').html(obj.message);
					$('#rideTripId').html('');
					
					setTimeout(function(){
						$('#memberRideModal').modal('hide');
					},2000);
				}
				else if(obj.status == "exist")
				{
					$('#memberRideModal').modal('show');
					$('#responseMsg').html(obj.message);
					$('#rideTripId').html('');
					
					// setTimeout(function(){
					// 	window.location.href = base_url+"member-my-ride";
					// 	$('#memberRideModal').modal('hide');
					// },2000);
				}

			}
		});
	});

	$("#schedule_ride_ok").click(function(){
		window.location.href = base_url+"member-my-ride";	
	});

	// --------- CONFIRM DRIVER -------------
	$("#btn_confirm_driver").on("click",function(e){
		e.preventDefault();
		var sched_id = $(this).attr("data-id");
		if (confirm("Do you really want to confirm driver?"))
		{
			$.ajax({
				type:"POST",
				url:baseURL+"frontend/MemberController/confirmDriver",
				data:{'sched_id':sched_id},
				dataType:"json",
			//async:false,					
			success:function(response){
				$("#btn_confirm_driver").hide();
				$("#btn_reject_driver").hide();
				if(response.status == "success"){	
					$("#success_message").text(response.message);
					setInterval(function(){
						location.reload();
					},2000);				
				}
				else
					if(response.status == "error"){
						$("#error_message").text(response.message);
						setInterval(function(){
							$("#error_message").text("");
						},3000);			
					}				
				}
			});	
		}			
	});

	//---------- REJECT DRIVER -------------
	$("#btn_reject_driver").on("click",function(e){
		e.preventDefault();
		var sched_id = $(this).attr("data-id");
		if (confirm("Do you really want to reject driver?"))
		{
			$.ajax({
				type:"POST",
				url:baseURL+"frontend/MemberController/rejectDriver",
				data:{'sched_id':sched_id},
				dataType:"json",
			//async:false,					
			success:function(response){
				$("#btn_confirm_driver").hide();
				$("#btn_reject_driver").hide();
				if(response.status == "success"){	
					$("#success_message").text(response.message);
					setInterval(function(){
						location.reload();
					},2000);				
				}
				else
					if(response.status == "error"){
						$("#error_message").text(response.message);
						setInterval(function(){
							$("#error_message").text("");
						},3000);			
					}				
				}
			});	
		}			
	});
	
});

	// function restaurantDetails(data)
	// {

	// 	$.ajax({

	// 		type : "POST",
	// 		url : base_url+"frontend/Homecontroller/getRestaurant",
	// 		data :
	// 		{
	// 			'id' : data
	// 		},
	// 		success : function(response)
	// 		{
	// 			var res = '';

	// 			var obj = JSON.parse(response);

	// 			res = '<h4>'+obj.data.name+'</h4><p>'+ obj.data.location.address+'</p><p>'+ obj.data.cuisines+'</p><p>'+ obj.data.location.locality_verbose+'</p><p>'+ obj.data.url+'</p>';

	// 			console.log(res);
	// 			$('#restContent').html(res);

	// 			$('#myModal').modal('show');
	// 		}
	// 	});
	// }

	function openPayModel(sched_id)
	{
		$.ajax({
			type : "POST",
			url : base_url+"frontend/MemberController/getScheduleDetails",
			data :
			{
				'sched_id' : sched_id
			},
			success : function(response)
			{

				var obj = JSON.parse(response);
				// console.log(obj);
				if(obj.status == "success")
				{
					var driver_name = (obj.data.full_name == null) ? "-" : obj.data.full_name;
					$("#tripid").html(obj.data.sr_trip_id);
					$("#source_address").html(obj.data.source_address);
					$("#dest_address").html(obj.data.destination_address);
					$("#driver_nm").html(driver_name);
					$("#tot_amt").html(obj.data.sr_total_cost+" USD");
					$("#ride_amt").val(obj.data.sr_total_cost);
					$("#ride_date").html(obj.data.trip_date+" "+obj.data.sr_pick_up_time);
					$("#tot_dist").html(obj.data.sr_total_distance+" miles");
				}else
				{
					alert("Something went wrong!!!")
				}
				$('#payModal').modal('show');
			}
		});
	}

	// $("#btn_pay_ride").on("change click",function(){
	// 	$("#pay_member_ride").submit();
	// });